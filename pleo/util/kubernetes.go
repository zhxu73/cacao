// Package util provides functions for creating Kubernetes resources required for
// running WorkflowDefinitions in a cluster
package util

import (
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// createNamespace is used to simply create a K8s Namespace
func createNamespace(name string) *core.Namespace {
	return &core.Namespace{
		ObjectMeta: meta.ObjectMeta{Name: name},
		Spec: core.NamespaceSpec{
			Finalizers: []core.FinalizerName{core.FinalizerKubernetes},
		},
	}
}

// createPodReaderRole creates a K8s Role allowing pods running in a cluster
// to access information about other pods running in that same cluster namespace
func createPodReaderRole(namespace string) *rbac.Role {
	return &rbac.Role{
		ObjectMeta: meta.ObjectMeta{Name: namespace + "-pod-reader"},
		Rules: []rbac.PolicyRule{
			{
				APIGroups: []string{""},
				Resources: []string{"pods", "pods/log"},
				Verbs:     []string{"get", "list", "watch"},
			},
		},
	}
}

// createPodReaderRoleBinding creates a K8s RoleBinding to bind a role allowing
// a Pod to access information about other Pods to the default ServiceAccount
func createPodReaderRoleBinding(roleName, namespace string) *rbac.RoleBinding {
	return &rbac.RoleBinding{
		ObjectMeta: meta.ObjectMeta{Name: roleName},
		Subjects: []rbac.Subject{
			{
				Kind:      "ServiceAccount",
				Name:      "default",
				Namespace: namespace,
			},
		},
		RoleRef: rbac.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "Role",
			Name:     roleName,
		},
	}
}

// createDeployment will use Containers defined by a user to create a K8s Deployment
// labeled with the app name and version
func createDeployment(containers []core.Container, id, name, version string) *apps.Deployment {
	return &apps.Deployment{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":      name,
				"version":  version,
				"cacao_id": id,
			},
		},
		Spec: apps.DeploymentSpec{
			Selector: &meta.LabelSelector{
				MatchLabels: map[string]string{
					"app":      name,
					"version":  version,
					"cacao_id": id,
				},
			},
			Template: core.PodTemplateSpec{
				ObjectMeta: meta.ObjectMeta{
					Name: name,
					Labels: map[string]string{
						"app":      name,
						"version":  version,
						"cacao_id": id,
					},
				},
				Spec: core.PodSpec{
					Containers: containers,
				},
			},
		},
	}
}

// createService creates a K8s Service to expose container ports defined by
// the user for Deployment labelled with app name and version
func createService(name, id, version string, ports []core.ServicePort) *core.Service {
	return &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":      name,
				"service":  name,
				"version":  version,
				"cacao_id": id,
			},
		},
		Spec: core.ServiceSpec{
			Ports: ports,
			Selector: map[string]string{
				"app":      name,
				"version":  version,
				"cacao_id": id,
			},
		},
	}
}
