package util

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/common/wfdefcommon"
	"gitlab.com/cyverse/cacao/phylax-service/phylax"
	"gitlab.com/cyverse/cacao/workflow-definition-service/workflowdefinition"
	"gitlab.com/cyverse/cacao/workflow-definition-service/workflowdefinition/types"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	security "istio.io/client-go/pkg/apis/security/v1beta1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

var (
	scaledObjectGVR = schema.GroupVersionResource{
		Group:    "keda.k8s.io",
		Version:  "v1alpha1",
		Resource: "scaledobjects",
	}
	adminKubeconfig = os.Getenv("ADMIN_KUBECONFIG")
)

const (
	istioNamespace = "istio-system"
	xidSliceLength = 8
)

// Request is a struct containing the necessary fields for all Pleo
// operations coming from the API
type Request struct {
	common.BasicRequest
	TCPPorts           []core.ServicePort                    `json:"tcp_ports,omitempty"`
	HTTPPorts          []core.ServicePort                    `json:"http_ports,omitempty"`
	AuthDisabled       bool                                  `json:"auth_disabled,omitempty"`
	Namespace          string                                `json:"namespace,omitempty"`
	WorkflowDefinition workflowdefinition.WorkflowDefinition `json:"data,omitempty"`
}

// Run is used to handle most of Pleo's functionality for running different resources
// on a user's Kubernetes cluster
func Run(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "util",
		"function": "Run",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("preparing resources needed to run workflow")
	cluster, ok := request.GetUser().Clusters[request.GetClusterID()]
	if !ok {
		err = fmt.Errorf("error getting cluster config '%s'", request.GetClusterID())
		logger.WithFields(log.Fields{"error": err}).Error("unable to run workflow without cluster config")
		return
	}
	// Set namespaceName if specified, otherwise use default for the Cluster
	namespaceName := request.Namespace
	if len(namespaceName) == 0 {
		namespaceName = cluster.DefaultNamespace
	}
	clientsets, err := wfdefcommon.NewUserClusterK8sClientsets(cluster.Config)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get dynamic clientset to run workflow")
		return
	}

	// Create Admin clientset for Istio namespaced resources
	adminClientsets, err := wfdefcommon.NewUserClusterK8sClientsets(adminKubeconfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get admin clientset")
		return
	}

	// Create user roles to allow pod reading
	role := createPodReaderRole(namespaceName)
	_, err = clientsets.CreateRole(role, namespaceName)
	if err != nil && err.Error() != fmt.Sprintf(`roles.rbac.authorization.k8s.io "%s" already exists`, role.Name) {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create pod reader role")
		return
	}
	rolebinding := createPodReaderRoleBinding(role.Name, namespaceName)
	_, err = clientsets.CreateRoleBinding(rolebinding, namespaceName)
	if err != nil && err.Error() != fmt.Sprintf(`rolebindings.rbac.authorization.k8s.io "%s" already exists`, rolebinding.Name) {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create pod reader rolebinding")
		return
	}

	// Create basic gateway on user cluster
	cacaoGateway := createCacaoGateway()
	_, err = clientsets.CreateGateway(cacaoGateway, namespaceName)
	if err != nil && err.Error() != `gateways.networking.istio.io "cacao-gateway" already exists` {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create basic Istio Gateway")
		return
	}

	if request.WorkflowDefinition.RawWorkflow == "" {
		err = getWorkflowDefinitionData(&request, natsInfo)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to retrieve WorkflowDefinition data")
			return
		}
	}

	// Now create necessary resources to proxy back to service cluster
	err = prepareAPIServiceProxy(clientsets, namespaceName)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to prepare API Service proxy")
		return
	}

	// If the request included ports, override those of the WFD
	if len(request.TCPPorts) > 0 {
		request.WorkflowDefinition.TCPPorts = request.TCPPorts
	}
	if len(request.HTTPPorts) > 0 {
		request.WorkflowDefinition.HTTPPorts = request.HTTPPorts
	}

	// Run the Workflow using WorkflowDefinition 'Run()'
	name := common.CreateResourceName(cluster.Slug, request.WorkflowDefinition.Name, request.GetRunID())
	workflow, err := types.ParseRawWorkflow(request.WorkflowDefinition.RawWorkflow, request.WorkflowDefinition.Type, request.WorkflowDefinition.Build)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse workflow")
		return
	}

	reqMap := map[string]interface{}{
		"id":            request.GetRunID(),
		"version":       request.WorkflowDefinition.Repository.Branch,
		"httpPorts":     request.WorkflowDefinition.HTTPPorts,
		"tcpPorts":      request.WorkflowDefinition.TCPPorts,
		"host":          cluster.Host,
		"resource_name": name,
		"namespace":     namespaceName,
	}

	// Run the Workflow
	err = workflow.Run(clientsets, reqMap, name, namespaceName)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to run workflow")
		return
	}

	// Configure routing for the Workflow
	err = workflow.SetupRouting(clientsets, adminClientsets, reqMap, name, namespaceName, cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to configure Workflow routing")
		return
	}

	// Now get the NodePort for the TCP services
	if len(request.WorkflowDefinition.TCPPorts) > 0 {
		ingressService, err := adminClientsets.GetService(name, istioNamespace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Errorf("unable to get Istio ingressgateway Service '%s'", name)
			return
		}
		request.TCPPorts = ingressService.Spec.Ports
		// Send update request to Phylax
		reqBytes, err := json.Marshal(request)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request to update Runner")
			return
		}
		common.StreamingPublish(reqBytes, natsInfo, "Phylax.UpdateRunner", "Pleo")
	}

	// Create Policy to link to Keycloak and AuthorizationPolicy to restrict access
	// to `/username`. If auth is disabled, create resources to exclude this from the rule
	var authenticationPolicy *security.RequestAuthentication
	var authPolicy *security.AuthorizationPolicy
	if request.AuthDisabled {
		// Since the "default" polciy created by createKeycloakPolicy() might already exist,
		// we need to explicitly create a new policy with an exclusion
		authenticationPolicy = createKeycloakPolicyExclude(name, request.WorkflowDefinition.Repository.Branch, request.GetRunID())
		authPolicy = createAuthorizationPolicyExclude(name, request.WorkflowDefinition.Repository.Branch, cluster.Host, request.GetRunID())
	} else {
		authenticationPolicy = createKeycloakPolicy()
		authPolicy = createAuthorizationPolicy(request.GetUser().Username, cluster.Host)
	}
	// If authenticationPolicy is nil, then Keycloak is not being used
	if authenticationPolicy != nil {
		_, err = adminClientsets.CreateRequestAuthentication(authenticationPolicy, istioNamespace)
		if err != nil && err.Error() != fmt.Sprintf(`requestauthentications.security.istio.io "%s" already exists`, authenticationPolicy.Name) {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create Keycloak Policy")
			return
		}
		_, err = adminClientsets.CreateAuthorizationPolicy(authPolicy, istioNamespace)
		if err != nil && err.Error() != fmt.Sprintf(`authorizationpolicies.security.istio.io "%s" already exists`, authPolicy.Name) {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create AuthorizationPolicy")
			return
		}
	} else {
		logger.Warn("not creating Keycloak Policy because KEYCLOAK_URL is undefined")
	}
}

// Delete is used to delete a WorkflowDefinition and all related resources from
// the user's Kubernetes cluster
func Delete(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "util",
		"function": "Delete",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("attempting to delete run")

	result, err := common.PublishRequest(&request, "Phylax.GetRunner", "Pleo", natsInfo["address"])
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get Runner")
		return
	}
	var runnerData struct {
		Error  common.Error   `json:"error,omitempty"`
		Runner *phylax.Runner `json:"runner"`
	}
	err = json.Unmarshal(result, &runnerData)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Runner")
		return
	}
	if len(runnerData.Error.Message) > 0 {
		err = fmt.Errorf(runnerData.Error.Message)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get Runner")
		return
	}

	runner := runnerData.Runner
	logRunner := runner.GetRedacted()
	logger = logger.WithFields(log.Fields{"runner": logRunner})
	logger.Info("successfully got Runner")

	request.SetWorkflowID(runner.WorkflowDefinitionID)

	result, err = common.PublishRequest(&request, "WorkflowDefinition.Get", "Pleo", natsInfo["address"])
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition")
		return
	}
	var wfdData Request
	err = json.Unmarshal(result, &wfdData)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into WorkflowDefinition")
		return
	}
	if len(wfdData.GetError().Message) > 0 {
		err = fmt.Errorf(wfdData.GetError().Message)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition")
		return
	}

	logger = logger.WithFields(log.Fields{"workflow_definition": wfdData.WorkflowDefinition})
	logger.Info("successfully got WorkflowDefinition")

	workflow, err := types.ParseRawWorkflow(
		wfdData.WorkflowDefinition.RawWorkflow,
		wfdData.WorkflowDefinition.Type,
		wfdData.WorkflowDefinition.Build,
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse workflow")
		return
	}

	cluster := runner.Cluster
	// Set namespace if specified, otherwise use default for the Cluster
	namespace := runner.Namespace
	if len(namespace) == 0 {
		namespace = cluster.DefaultNamespace
	}
	clientsets, err := wfdefcommon.NewUserClusterK8sClientsets(cluster.Config)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get dynamic clientset to run workflow")
		return
	}

	name := runner.ResourceName

	// Delete WorkflowType-specific resources
	err = workflow.Delete(clientsets, name, namespace)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Warn("unable to delete Workflow")
	}

	// Now delete admin resources
	adminClientset, err := wfdefcommon.NewUserClusterK8sClientsets(adminKubeconfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get admin clientset")
		return
	}
	// These resources are only created when auth is disabled
	if runner.AuthDisabled {
		err = adminClientset.DeleteAuthorizationPolicy(name, istioNamespace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to delete AuthorizationPolicy")
		}
		err = adminClientset.DeleteRequestAuthentication(name, istioNamespace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to delete Policy")
		}
	}
	if len(runner.TCPPorts) > 0 || len(wfdData.TCPPorts) > 0 || len(wfdData.WorkflowDefinition.TCPPorts) > 0 {
		err = adminClientset.DeleteService(name, istioNamespace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Warn("unable to delete Istio Service for TCP Ports")
		}
	}
}

// ScaleUp is used when a user attempts to access a Workflow that is no longer
// running. This will start up some new Pods for the user to use
func ScaleUp(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "util",
		"function": "ScaleUp",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("attempting to scale up workflow")

	cluster, ok := request.GetUser().Clusters[request.GetClusterID()]
	if !ok {
		err = fmt.Errorf("error getting cluster config '%s'", request.GetClusterID())
		logger.WithFields(log.Fields{"error": err}).Error("unable to scale up workflow without cluster config")
		return
	}
	// Set namespaceName if specified, otherwise use default for the Cluster
	namespaceName := request.Namespace
	if len(namespaceName) == 0 {
		namespaceName = cluster.DefaultNamespace
	}
	clientsets, err := wfdefcommon.NewUserClusterK8sClientsets(cluster.Config)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get dynamic clientset to scale up workflow")
		return
	}

	// If the request included ports, override those of the WFD
	if len(request.TCPPorts) > 0 {
		request.WorkflowDefinition.TCPPorts = request.TCPPorts
	}
	if len(request.HTTPPorts) > 0 {
		request.WorkflowDefinition.HTTPPorts = request.HTTPPorts
	}

	name := common.CreateResourceName(request.GetUser().Username, request.WorkflowDefinition.Name, request.GetRunID())
	workflow, err := types.ParseRawWorkflow(request.WorkflowDefinition.RawWorkflow, request.WorkflowDefinition.Type, request.WorkflowDefinition.Build)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse workflow")
		return
	}

	reqMap := map[string]interface{}{
		"id":            request.GetRunID(),
		"version":       request.WorkflowDefinition.Repository.Branch,
		"httpPorts":     request.WorkflowDefinition.HTTPPorts,
		"tcpPorts":      request.WorkflowDefinition.TCPPorts,
		"host":          cluster.Host,
		"resource_name": name,
		"namespace":     namespaceName,
	}

	err = workflow.ScaleFromZero(clientsets, reqMap, name, namespaceName)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to scale up workflow")
		return
	}

	// Create Admin clientset for Istio namespaced resources
	adminClientsets, err := wfdefcommon.NewUserClusterK8sClientsets(adminKubeconfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get admin clientset")
		return
	}

	// Configure routing for the Workflow
	err = workflow.SetupRouting(clientsets, adminClientsets, reqMap, name, namespaceName, cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to configure Workflow routing")
		return
	}

	// Now get the NodePort for the TCP services
	if len(request.WorkflowDefinition.TCPPorts) > 0 {
		ingressService, err := adminClientsets.GetService(name, istioNamespace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Errorf("unable to get Istio ingressgateway Service '%s'", name)
			return
		}
		request.TCPPorts = ingressService.Spec.Ports
		// Send update request to Phylax
		reqBytes, err := json.Marshal(request)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request to update Runner")
			return
		}
		common.StreamingPublish(reqBytes, natsInfo, "Phylax.UpdateRunner", "Pleo")
	}

	// Create Policy to link to Keycloak and AuthorizationPolicy to restrict access
	// to `/username`. If auth is disabled, create resources to exclude this from the rule
	var authenticationPolicy *security.RequestAuthentication
	var authPolicy *security.AuthorizationPolicy
	if request.AuthDisabled {
		// Since the "default" polciy created by createKeycloakPolicy() might already exist,
		// we need to explicitly create a new policy with an exclusion
		authenticationPolicy = createKeycloakPolicyExclude(name, request.WorkflowDefinition.Repository.Branch, request.GetRunID())
		authPolicy = createAuthorizationPolicyExclude(name, request.WorkflowDefinition.Repository.Branch, cluster.Host, request.GetRunID())
	} else {
		authenticationPolicy = createKeycloakPolicy()
		authPolicy = createAuthorizationPolicy(request.GetUser().Username, cluster.Host)
	}
	// If authenticationPolicy is nil, then Keycloak is not being used
	if authenticationPolicy != nil {
		_, err = adminClientsets.CreateRequestAuthentication(authenticationPolicy, istioNamespace)
		if err != nil && err.Error() != fmt.Sprintf(`requestauthentications.security.istio.io  "%s" already exists`, authenticationPolicy.Name) {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create Keycloak Policy")
			return
		}
		_, err = adminClientsets.CreateAuthorizationPolicy(authPolicy, istioNamespace)
		if err != nil && err.Error() != fmt.Sprintf(`authorizationpolicies.security.istio.io "%s" already exists`, authPolicy.Name) {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create AuthorizationPolicy")
			return
		}
	} else {
		logger.WithFields(log.Fields{"error": err}).Warn("not creating Keycloak Policy because KEYCLOAK_URL is undefined")
	}
}

// CreateUserClusterKubeconfig is a synchronous subscriber used to create new Key and Certificate to allow a user to access
// a Kubernetes cluster. It will create the new file and respond with it to the API Service to save in Vault
func CreateUserClusterKubeconfig(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "util",
		"function": "CreateUserClusterKubeconfig",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("creating new user cluster config for user")

	var errMsg string
	cluster := request.User.Clusters[request.User.Username]
	adminClientsets, err := wfdefcommon.NewUserClusterK8sClientsets(adminKubeconfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get admin clientset to create new cluster config")
		errMsg = err.Error()
	}

	err = createDefaultNamespace(adminClientsets, &cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create namespace for user")
		errMsg = err.Error()
	}

	userClusterConfig, err := createNewUserClusterConfig(cluster.Slug, adminClientsets)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create user cluster config")
		errMsg = err.Error()
	}

	cluster.Config = userClusterConfig
	data, err := json.Marshal(struct {
		Error   common.Error   `json:"error,omitempty"`
		Cluster common.Cluster `json:"cluster"`
	}{
		Error:   common.Error{Message: errMsg, Code: 500},
		Cluster: cluster,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
	}
	msg.Respond(data)
	// Delete CertificateSigningRequest now that it is complete
	err = adminClientsets.DeleteCertificateSigningRequest(cluster.Slug)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete CertificateSigningRequest")
		return
	}
}

// createDefaultNamespace attempts to create a namespace on the user cluster with a name that matches
// cluster.Slug.  If that namespace already exists, it appends a dash and the first xidSliceLength
// characters of cluster.ID.  In the highly unlikely event that that name is taken, it tries appending
// a dash and the next available substring of length xidSliceLength.
//
// The Slug, DefaultNamespace, and Host fields of cluster are updated in this function as well.
func createDefaultNamespace(adminClientsets *wfdefcommon.K8sClientsets, cluster *common.Cluster) (err error) {
	slug := cluster.Slug
	namespace := createNamespace(cluster.Slug)
	_, err = adminClientsets.CreateNamespace(namespace)
	if err != nil && err.Error() == fmt.Sprintf(`namespaces "%s" already exists`, cluster.Slug) {
		clusterIDLength := len(cluster.ID)
		if clusterIDLength < xidSliceLength {
			err = fmt.Errorf("xidSliceLength (%d) must be less than Cluster ID length (%d)", xidSliceLength, clusterIDLength)
		}
		for n := 0; n < clusterIDLength-xidSliceLength; n++ {
			slug = cluster.Slug + "-" + cluster.ID[n:n+xidSliceLength]
			namespace = createNamespace(slug)
			_, err = adminClientsets.CreateNamespace(namespace)
			if err == nil {
				break
			}
		}
	}

	cluster.Slug = slug
	cluster.DefaultNamespace = slug
	// cluster.Host comes from api.createUser with the value of userClusterHost
	cluster.Host = cluster.Slug + "." + cluster.Host

	return
}

func getWorkflowDefinitionData(request *Request, natsInfo map[string]string) error {
	msg, err := common.PublishRequest(
		request,
		"WorkflowDefinition.Get",
		"Pleo",
		natsInfo["address"],
	)
	if err == nil {
		err = json.Unmarshal(msg, request)
	}

	return err
}
