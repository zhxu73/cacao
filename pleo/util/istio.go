package util

import (
	"fmt"
	"os"

	"gitlab.com/cyverse/cacao/common/wfdefcommon"

	api "istio.io/api/networking/v1beta1"
	apisec "istio.io/api/security/v1beta1"
	istio "istio.io/client-go/pkg/apis/networking/v1beta1"
	security "istio.io/client-go/pkg/apis/security/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	defaultAPIHost = "ca.cyverse.org"
)

var (
	cacaoAPIHost string
)

func init() {
	cacaoAPIHost = os.Getenv("CACAO_API_HOST")
	if len(cacaoAPIHost) == 0 {
		cacaoAPIHost = defaultAPIHost
	}
}

func createIstioIngressService(name string, ports []core.ServicePort) *core.Service {
	return &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      name,
			Namespace: "istio-system",
			Labels: map[string]string{
				"app":                         "istio-ingressgateway",
				"istio":                       "ingressgateway",
				"operator.istio.io/component": "IngressGateways",
				"operator.istio.io/managed":   "Reconcile",
				"release":                     "istio",
			},
		},
		Spec: core.ServiceSpec{
			Ports: ports,
			Selector: map[string]string{
				"app":   "istio-ingressgateway",
				"istio": "ingressgateway",
			},
			Type: core.ServiceTypeLoadBalancer,
		},
	}
}

// createDestinationRule creates a new Istio DestinationRule to route to a K8s
// Service based on app name and version
func createDestinationRule(name, id, version string) *istio.DestinationRule {
	return &istio.DestinationRule{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":      name,
				"version":  version,
				"cacao_id": id,
			},
		},
		Spec: api.DestinationRule{
			Host: name,
			Subsets: []*api.Subset{
				{
					Name: version,
					Labels: map[string]string{
						"app":      name,
						"version":  version,
						"cacao_id": id,
					},
				},
			},
		},
	}
}

// createVirtualService creates an Istio VirtualService to control routing
// from an endpoint like "/user/workflow" to the correct K8s Service
func createVirtualService(name, host, version, uid string, httpPorts []core.ServicePort, tcpPorts []core.ServicePort) *istio.VirtualService {
	// Create HTTP routes for each port
	var routes []*api.HTTPRoute
	for _, p := range httpPorts {
		// If there is only one port in the slice, do not use port mapping
		path := fmt.Sprintf("/%s:%d", uid, p.Port)
		routes = append(routes, &api.HTTPRoute{
			Match: []*api.HTTPMatchRequest{
				{
					Uri: &api.StringMatch{
						MatchType: &api.StringMatch_Prefix{
							Prefix: path + "/",
						},
					},
				},
				{
					Uri: &api.StringMatch{
						MatchType: &api.StringMatch_Prefix{
							Prefix: path,
						},
					},
				},
			},
			Rewrite: &api.HTTPRewrite{Uri: "/"},
			Route: []*api.HTTPRouteDestination{
				{
					Destination: &api.Destination{
						Host:   name,
						Subset: version,
						Port:   &api.PortSelector{Number: uint32(p.Port)},
					},
				},
			},
		})
	}

	var tcpRoutes []*api.TCPRoute
	for _, p := range tcpPorts {
		tcpRoutes = append(tcpRoutes, &api.TCPRoute{
			Match: []*api.L4MatchAttributes{
				{Port: uint32(p.Port)},
			},
			Route: []*api.RouteDestination{
				{
					Destination: &api.Destination{
						Host:   name,
						Subset: version,
						Port:   &api.PortSelector{Number: uint32(p.Port)},
					},
				},
			},
		})
	}

	// Now create and return VirtualService
	return &istio.VirtualService{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":      name,
				"version":  version,
				"cacao_id": uid,
			},
		},
		Spec: api.VirtualService{
			Gateways: []string{"cacao-gateway", name},
			Hosts:    []string{host},
			Http:     routes,
			Tcp:      tcpRoutes,
		},
	}
}

// createVirtualServiceCacaoRoute creates an Istio VirtualService that will
// route to the Cacao Service Cluster ServiceEntry to enable routing back to
// Cacao API for scale-from-zero
func createVirtualServiceCacaoRoute(port int) *istio.VirtualService {
	return &istio.VirtualService{
		ObjectMeta: meta.ObjectMeta{
			Name: "cacao-api-service",
		},
		Spec: api.VirtualService{
			Gateways: []string{"cacao-gateway", "cacao-api-proxy"},
			Hosts:    []string{"*"},
			Http: []*api.HTTPRoute{
				{
					Match: []*api.HTTPMatchRequest{
						{
							Uri: &api.StringMatch{
								MatchType: &api.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
					},
					Route: []*api.HTTPRouteDestination{
						{
							Destination: &api.Destination{
								Host: cacaoAPIHost,
								Port: &api.PortSelector{Number: uint32(port)},
							},
						},
					},
				},
			},
		},
	}
}

// createServiceEntry creates an Istio ServiceEntry to allow routing to the
// external host that is the Cacao Service Cluster
func createServiceEntry(address string, port int) *istio.ServiceEntry {
	return &istio.ServiceEntry{
		ObjectMeta: meta.ObjectMeta{
			Name: "cacao-api-service",
		},
		Spec: api.ServiceEntry{
			Hosts:     []string{cacaoAPIHost},
			Addresses: []string{address + "/32"},
			Ports: []*api.Port{
				{
					Number:   uint32(port),
					Name:     "tls",
					Protocol: "TLS",
				},
			},
			Location:   api.ServiceEntry_MESH_EXTERNAL,
			Resolution: api.ServiceEntry_STATIC,
			Endpoints: []*api.WorkloadEntry{
				{
					Address: address,
				},
			},
		},
	}
}

// createCacaoGateway creates a simple Istio Gateway for Cacao routing
func createCacaoGateway() *istio.Gateway {
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: "cacao-gateway",
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*api.Server{
				{
					Hosts: []string{"*"},
					Port: &api.Port{
						Number:   uint32(80),
						Name:     "http",
						Protocol: "HTTP",
					},
				},
			},
		},
	}
}

// createTCPGateway ...
func createTCPGateway(name string, ports []core.ServicePort) *istio.Gateway {
	var servers []*api.Server
	for _, p := range ports {
		servers = append(servers, &api.Server{
			Hosts: []string{"*"},
			Port: &api.Port{
				Number:   uint32(p.Port),
				Name:     p.Name,
				Protocol: "TCP",
			},
		})
	}
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers:  servers,
		},
	}
}

// createProxyGateway creates an Istio Gateway for routing back to the Cacao
// Service Cluster
func createProxyGateway(port int) *istio.Gateway {
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: "cacao-api-service",
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*api.Server{
				{
					Hosts: []string{cacaoAPIHost},
					Port: &api.Port{
						Number:   uint32(port),
						Name:     "tls",
						Protocol: "TLS",
					},
					Tls: &api.ServerTLSSettings{
						Mode: api.ServerTLSSettings_PASSTHROUGH,
					},
				},
			},
		},
	}
}

// createAuthorizationPolicy creates an Istio AuthorizationPolicy that requires
// a JWT with 'preferred_username' claim matching the URI prefix so that only "UserA"
// can access things running on "/UserA/*" endpoints
func createAuthorizationPolicy(username, host string) *security.AuthorizationPolicy {
	return &security.AuthorizationPolicy{
		ObjectMeta: meta.ObjectMeta{
			Name: username,
		},
		Spec: apisec.AuthorizationPolicy{
			Rules: []*apisec.Rule{
				{
					To: []*apisec.Rule_To{
						{
							Operation: &apisec.Operation{
								Paths: []string{"/*"},
								Hosts: []string{host},
							},
						},
					},
					When: []*apisec.Condition{
						{
							Key:    "request.auth.claims[preferred_username]",
							Values: []string{username},
						},
					},
				},
			},
		},
	}
}

func createAuthorizationPolicyExclude(name, version, host, runID string) *security.AuthorizationPolicy {
	return &security.AuthorizationPolicy{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"cacao_id": runID,
			},
		},
		Spec: apisec.AuthorizationPolicy{
			Rules: []*apisec.Rule{
				{
					To: []*apisec.Rule_To{
						{
							Operation: &apisec.Operation{
								Hosts: []string{host},
								Paths: []string{fmt.Sprintf("/%s*", runID)},
							},
						},
					},
				},
			},
		},
	}
}

// createKeycloakPolicy creates an Istio Policy requiring a valid JWT for the
// Keycloak server
func createKeycloakPolicy() *security.RequestAuthentication {
	keycloakURL := os.Getenv("KEYCLOAK_URL")
	if len(keycloakURL) == 0 {
		return nil
	}
	return &security.RequestAuthentication{
		ObjectMeta: meta.ObjectMeta{Name: "default"},
		Spec: apisec.RequestAuthentication{
			JwtRules: []*apisec.JWTRule{
				{
					Issuer: keycloakURL,
					FromHeaders: []*apisec.JWTHeader{
						{
							Name: "Authorization",
						},
					},
					Audiences: []string{"cacao-client"},
					JwksUri:   keycloakURL + "/protocol/openid-connect/certs",
				},
			},
		},
	}
}

func createKeycloakPolicyExclude(name, version, uid string) *security.RequestAuthentication {
	keycloakURL := os.Getenv("KEYCLOAK_URL")
	if len(keycloakURL) == 0 {
		return nil
	}
	return &security.RequestAuthentication{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"cacao_id": uid,
			},
		},
		Spec: apisec.RequestAuthentication{
			JwtRules: []*apisec.JWTRule{
				{
					Issuer: keycloakURL,
					FromHeaders: []*apisec.JWTHeader{
						{
							Name: "Authorization",
						},
					},
					Audiences: []string{"cacao-client"},
					JwksUri:   keycloakURL + "/protocol/openid-connect/certs",

					// TODO: implement this in other ways
					// TriggerRule is no more supported
					/*
						TriggerRules: []*apiauth.Jwt_TriggerRule{
							&apiauth.Jwt_TriggerRule{
								ExcludedPaths: []*apiauth.StringMatch{
									{
										MatchType: &apiauth.StringMatch_Prefix{
											Prefix: fmt.Sprintf("/%s", uid),
										},
									},
								},
							},
						},
					*/
				},
			},
		},
	}
}

// getIngressInfo accesses the cluster that this is running on in order to get the
// Host and Port used to access the Istio Ingress Gateway
func getIngressInfo() (address string, port int, err error) {
	clientsets, err := wfdefcommon.NewServiceClusterK8sClientsets()
	if err != nil {
		return
	}
	podList, err := clientsets.ListPods(meta.ListOptions{LabelSelector: "istio=ingressgateway"}, "istio-system")
	if err != nil {
		return
	}
	pod := podList.Items[0]
	address = pod.Status.HostIP

	service, err := clientsets.GetService("istio-ingressgateway", "istio-system")
	if err != nil {
		return
	}
	for _, p := range service.Spec.Ports {
		if p.Name == "http2" {
			port = int(p.NodePort)
			break
		}
	}
	return
}

// prepareAPIServiceProxy creates the necessary Gateways, VirtualService, and ServiceEntry
// to the User cluster in order to proxy requests to non-running services back to the
// Cacao API
func prepareAPIServiceProxy(clientsets *wfdefcommon.K8sClientsets, namespace string) (err error) {
	address, port, err := getIngressInfo()
	if err != nil {
		return
	}

	// Create a standard gateway
	cacaoGateway := createCacaoGateway()
	_, err = clientsets.CreateGateway(cacaoGateway, namespace)
	if err != nil && err.Error() != `gateways.networking.istio.io "cacao-gateway" already exists` {
		return
	}
	// Create a Gateway to proxy back to Service Cluster
	proxyGateway := createProxyGateway(port)
	_, err = clientsets.CreateGateway(proxyGateway, namespace)
	if err != nil && err.Error() != `gateways.networking.istio.io "cacao-api-service" already exists` {
		return
	}
	// Create a ServiceEntry representing the external API Service
	serviceEntry := createServiceEntry(address, port)
	_, err = clientsets.CreateServiceEntry(serviceEntry, namespace)
	if err != nil && err.Error() != `serviceentries.networking.istio.io "cacao-api-service" already exists` {
		return
	}
	// Create a VirtualService defining path prefix to use for API Service
	virtualService := createVirtualServiceCacaoRoute(port)
	_, err = clientsets.CreateVirtualService(virtualService, namespace)
	if err != nil && err.Error() != `virtualservices.networking.istio.io "cacao-api-service" already exists` {
		return
	}
	return nil
}
