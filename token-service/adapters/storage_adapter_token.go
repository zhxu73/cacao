package adapters

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/types"
	"time"
)

// Revoke immediately expires a token
func (adapter *MongoAdapter) Revoke(actor string, tokenID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.Revoke",
	})
	if !tokenID.Validate() {
		errorMessage := "error validating token id during revoke"
		logger.Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}
	result := types.Token{}
	err := adapter.store.Get(adapter.config.TokenMongoDBCollectionName, tokenID.String(), &result)
	if err != nil {
		errorMessage := "error trying to get token to revoke"
		logger.WithError(err).Error(errorMessage)
		return err
	}

	if actor != result.Owner {
		errorMessage := "must own a token to revoke"
		logger.Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	result.Expiration = adapter.TimeSrc()
	updated, err := adapter.store.Update(adapter.config.TokenMongoDBCollectionName, tokenID.String(), result, []string{"expiration"})
	if !updated {
		errorMessage := fmt.Sprintf("unable to revoke the token")
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}
	return nil
}

// List returns tokens owned by user
func (adapter *MongoAdapter) List(user string) ([]types.Token, error) {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.List",
	})
	var results []types.Token
	err := adapter.store.ListForUser(adapter.config.TokenMongoDBCollectionName, user, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list tokens for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedTokens []types.Token, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}
	mock.On("ListForUser", adapter.config.TokenMongoDBCollectionName, user).Return(expectedTokens, expectedError)
	return nil
}

// Get retrieves the Token from the database
func (adapter *MongoAdapter) Get(tokenID cacao_common.ID) (types.Token, error) {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Token{}
	err := adapter.store.Get(adapter.config.TokenMongoDBCollectionName, tokenID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the token for id %s", tokenID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(tokenID cacao_common.ID, expectedToken types.Token, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}
	mock.On("Get", adapter.config.TokenMongoDBCollectionName, tokenID.String()).Return(expectedToken, expectedError)
	return nil
}

// Create inserts a new token into the database
func (adapter *MongoAdapter) Create(token types.Token) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.Create",
	})
	err := adapter.store.Insert(adapter.config.TokenMongoDBCollectionName, token)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a token because id %s conflicts", token.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a token with id %s", token.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(token types.Token, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.config.TokenMongoDBCollectionName).Return(expectedError)
	return nil
}

// TokenAccessed update the last access time of token to current time
func (adapter *MongoAdapter) TokenAccessed(tokenID cacao_common.ID) error {
	now := time.Now().UTC()
	_, err := adapter.store.Update(adapter.config.TokenMongoDBCollectionName, tokenID.String(), types.Token{
		LastAccessed: &now,
	}, []string{"last_accessed"})
	if err != nil {
		return err
	}
	return nil
}

// Update updates an existing Token in storage
func (adapter *MongoAdapter) Update(token types.Token, updateFieldNames []string, actor string) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// validate the update field names, this ensures there is no extra field being passed in
	if !adapter.validateUpdateFieldNames(updateFieldNames) {
		return cacao_common_service.NewCacaoInvalidParameterError("invalid value in update_field_names")
	}

	// get and check ownership
	result := types.Token{}

	err := adapter.store.Get(adapter.config.TokenMongoDBCollectionName, token.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the token for id %s", token.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != token.Owner && actor != cacao_common_service.ReservedCacaoSystemActor {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the token for id %s", token.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.store.Update(adapter.config.TokenMongoDBCollectionName, token.ID.String(), token, updateFieldNames)

	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the token for id %s", token.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the token for id %s", token.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(token types.Token, updates types.Token, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Update", adapter.config.TokenMongoDBCollectionName, token.ID.String()).Return(expectedResult, expectedError)
	mock.On("Get", adapter.config.TokenMongoDBCollectionName, token.ID.String()).Return(token, expectedError)
	return nil
}

var allowedUpdateFieldNames = map[string]struct{}{
	"name":        {},
	"description": {},
	"public":      {},
	"scopes":      {},
	"tags":        {},
}

func (adapter *MongoAdapter) validateUpdateFieldNames(updateFieldNames []string) bool {
	for _, name := range updateFieldNames {
		_, ok := allowedUpdateFieldNames[name]
		if !ok {
			return false
		}
	}
	return true
}

// Delete deletes an existing token from storage
func (adapter *MongoAdapter) Delete(user string, tokenID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Token{}

	err := adapter.store.Get(adapter.config.TokenMongoDBCollectionName, tokenID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the token for id %s", tokenID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the token for id %s", tokenID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// actually delete - revoke does the expiration=now
	deleted, err := adapter.store.Delete(adapter.config.TokenMongoDBCollectionName, tokenID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the token for id %s", tokenID.String())
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the token for id %s", tokenID.String())
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, tokenID cacao_common.ID, existingToken types.Token, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.config.TokenMongoDBCollectionName, tokenID.String()).Return(existingToken, expectedError)
	mock.On("Delete", adapter.config.TokenMongoDBCollectionName, tokenID.String()).Return(expectedResult, expectedError)
	return nil
}
