package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/ports"
	"sync"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	conn     messaging2.QueryConnection
	handlers ports.IncomingQueryHandlers
}

// SetHandlers sets the incoming query handlers for the query adapter
func (adapter QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	adapter.handlers = handlers
}

// NewQueryAdapter creates a new query adapter
func NewQueryAdapter(queryConn messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{
		conn: queryConn,
	}
}

// Start starts the adapter
func (adapter QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup, handlers ports.IncomingQueryHandlers) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")
	err := adapter.conn.Listen(ctx, map[cacao_common.QueryOp]messaging2.QueryHandlerFunc{
		cacao_common_service.TokenGetQueryOp:       queryHandlerWrapper(handlers.Get),
		cacao_common_service.TokenAuthCheckQueryOp: queryHandlerWrapper(handlers.AuthCheckToken),
		cacao_common_service.TokenListQueryOp:      queryHandlerWrapper(handlers.List),
		cacao_common_service.TokenTypesListQueryOp: queryHandlerWrapper(handlers.ListTokenTypes),
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

func queryHandlerWrapper[T1 any, T2 any](handler func(T1) T2) messaging2.QueryHandlerFunc {
	return func(ctx context.Context, ce cloudevents.Event, writer messaging2.ReplyWriter) {
		var request T1
		err := json.Unmarshal(ce.Data(), &request)
		if err != nil {
			return
		}
		reply := handler(request)
		ceReply, err := messaging2.CreateCloudEventWithAutoSource(reply, cacao_common.QueryOp(ce.Type()))
		if err != nil {
			return
		}
		err = writer.Write(ceReply)
		if err != nil {
			return
		}
	}
}

var _ ports.IncomingQueryPort = &QueryAdapter{}
