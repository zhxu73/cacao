package ports

import (
	"context"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/types"
	"sync"
)

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup, handlers IncomingQueryHandlers) error
	SetHandlers(handlers IncomingQueryHandlers)
}

// IncomingQueryHandlers represents the handlers for all query operation (e.g. read operations) this service supports, this is implemented by object in `domain` package
type IncomingQueryHandlers interface {
	List(session cacao_common_service.Session) cacao_common_service.TokenListModel
	AuthCheckToken(request cacao_common_service.TokenAuthCheckRequest) cacao_common_service.TokenModel
	Get(cacao_common_service.TokenModel) cacao_common_service.TokenModel
	ListTokenTypes(session cacao_common_service.Session) cacao_common_service.TokenTypeListModel
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup, handlers IncomingEventHandlers) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for all event operations (operations with side effects) this service supports, this is implemented by object in `domain` package
type IncomingEventHandlers interface {
	Create(cacao_common_service.TokenCreateModel, OutgoingEventPort)
	Update(cacao_common_service.TokenModel, OutgoingEventPort)
	Revoke(cacao_common_service.TokenModel, OutgoingEventPort)
	Delete(cacao_common_service.TokenModel, OutgoingEventPort)
	// TODO implement
	BulkCreate(model cacao_common_service.TokenCreateBulkModel, port OutgoingEventPort)
}

// OutgoingEventPort represents all events this service will publish, implementation of IncomingEventPort will construct this and pass to methods of IncomingEventHandlers.
type OutgoingEventPort interface {
	// Token Events
	TokenCreatedEvent(cacao_common_service.TokenCreateModel)
	TokenCreateFailedEvent(cacao_common_service.TokenCreateModel)
	TokenUpdatedEvent(cacao_common_service.TokenModel)
	TokenUpdateFailedEvent(cacao_common_service.TokenModel)
	TokenDeletedEvent(cacao_common_service.TokenModel)
	TokenDeleteFailedEvent(cacao_common_service.TokenModel)
	TokenRevokedEvent(cacao_common_service.TokenModel)
	TokenRevokeFailedEvent(cacao_common_service.TokenModel)
}

// PersistentStoragePort is an interface for Persistent Storage.
type PersistentStoragePort interface {
	Init(config *types.Config) error
	// List lists tokens
	List(user string) ([]types.Token, error)
	// Get gets a single token by xid
	Get(tokenID cacao_common.ID) (types.Token, error)
	// Create creates a token
	Create(token types.Token) error
	// TokenAccessed update the last access time of a token
	TokenAccessed(tokenID cacao_common.ID) error
	// Update updates a token
	Update(token types.Token, updateFieldNames []string, actor string) error
	// Delete real delete
	Delete(user string, tokenID cacao_common.ID) error
	// Revoke soft delete / expire now
	Revoke(user string, tokenID cacao_common.ID) error
}

// UserService is an interface for checking user admin status from the user microservice
type UserService interface {
	Init(config *types.Config) error
	// CheckForAdmin communicates with user service to check if a user is admin.
	// Return error if check fails or user is NOT admin.
	// This can be overridden with config to skip checks.
	CheckForAdmin(actor cacao_common_service.Actor) (actorIsAdmin bool, err error)
}
