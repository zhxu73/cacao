package domain

import (
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/ports"
	"gitlab.com/cyverse/cacao/token-service/ports/mocks"
	"gitlab.com/cyverse/cacao/token-service/types"
	"testing"
	"time"
)

// TODO implement Bulk Create functionality
//func TestEventHandlers_BulkCreate(t *testing.T) {
//	type fields struct {
//		storage  ports.PersistentStoragePort
//		userSvc  ports.UserService
//		TimeSrc  func() time.Time
//	}
//	type args struct {
//		model service.TokenCreateBulkModel
//		port  ports.OutgoingEventPort
//	}
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//	}{
//		// TODO: Add test cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			h := EventHandlers{
//				storage:  tt.fields.storage,
////				userSvc:  tt.fields.userSvc,
//				TimeSrc:  tt.fields.TimeSrc,
//			}
//			h.BulkCreate(tt.args.model, tt.args.port)
//		})
//	}
//}

func TestEventHandlers_Create(t *testing.T) {
	createdDate := time.Now().UTC() // time.Date(2023, 10, 31, 0, 0, 0, 0, time.UTC)
	expirationTime := createdDate.Add(time.Hour * 24)
	expirationTimeInPast := createdDate.Add(time.Hour * (-48))
	invalidStartTime := createdDate.Add(time.Hour * 96)
	tokenXID := xid.New()
	t.Logf("created date %s", createdDate.String())
	t.Logf("expiration date %s", expirationTime.String())
	t.Logf("invalid expiration date %s", expirationTimeInPast.String())
	t.Logf("invalid start date %s", invalidStartTime.String())
	validCreatedModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:          common.NewIDFromXID("token", tokenXID),
		Owner:       "test-username",
		Name:        "test-token-name",
		Description: "test-description-name",
		Public:      false,
		Expiration:  expirationTime,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdDate,
		UpdatedAt:   createdDate,
	}
	validCreatedToken, validCreatedHash, validCreatedSalt, _ := common.NewTokenID(string(validCreatedModel.Type), validCreatedModel.Owner, createdDate, tokenXID)

	invalidCreateModel := service.TokenCreateModel{}
	emptyExpirationModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:          common.NewIDFromXID("token", tokenXID),
		Owner:       "test-username",
		Name:        "test-token-name",
		Description: "test-description-name",
		Public:      false,
		Expiration:  time.Time{},
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdDate,
		UpdatedAt:   createdDate,
	}
	invalidExpirationModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:          common.NewIDFromXID("token", tokenXID),
		Owner:       "test-username",
		Name:        "test-token-name",
		Description: "test-description-name",
		Public:      false,
		Expiration:  expirationTimeInPast,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdDate,
		UpdatedAt:   createdDate,
	}
	invalidStartModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:          common.NewIDFromXID("token", tokenXID),
		Owner:       "test-username",
		Name:        "test-token-name",
		Description: "test-description-name",
		Public:      false,
		Expiration:  expirationTime,
		Start:       &invalidStartTime,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdDate,
		UpdatedAt:   createdDate,
	}
	invalidTypeModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:          common.NewIDFromXID("token", tokenXID),
		Owner:       "test-username",
		Name:        "test-token-name",
		Description: "test-description-name",
		Public:      false,
		Expiration:  expirationTime,
		Scopes:      "api",
		Type:        "invalid-type",
		CreatedAt:   createdDate,
		UpdatedAt:   createdDate,
	}
	validDelegatedModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:               common.NewIDFromXID("token", tokenXID),
		Owner:            "test-username",
		Name:             "test-token-name",
		Description:      "test-description-name",
		Public:           false,
		Expiration:       expirationTime,
		Scopes:           "api",
		Type:             "delegated",
		DelegatedContext: "something",
		CreatedAt:        createdDate,
		UpdatedAt:        createdDate,
	}
	validDelegatedToken, validDelegatedHash, validDelegatedSalt, _ := common.NewTokenID(string(validDelegatedModel.Type), validDelegatedModel.Owner, createdDate, tokenXID)
	missingDelegatedModel := service.TokenCreateModel{
		Session: service.Session{
			SessionActor: "test-username",
		},
		ID:          common.NewIDFromXID("token", tokenXID),
		Owner:       "test-username",
		Name:        "test-token-name",
		Description: "test-description-name",
		Public:      false,
		Expiration:  expirationTime,
		Scopes:      "api",
		Type:        "delegated",
		CreatedAt:   createdDate,
		UpdatedAt:   createdDate,
	}
	type fields struct {
		storage        *mocks.PersistentStoragePort
		userSvc        *mocks.UserService
		XIDGenerator   func() xid.ID
		TokenGenerator func(typeString, owner string, createdAt time.Time, tokenXid xid.ID) (id common.TokenID, hash string, salt string, err error)
		TimeSrc        func() time.Time
	}
	type args struct {
		request service.TokenCreateModel
		port    *mocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "token emulator",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: service.TokenCreateModel{
					Session: service.Session{
						SessionEmulator: "token-cl5el1u2au5ud19qunog",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreateFailedEvent", service.TokenCreateModel{
						Session: service.Session{
							SessionEmulator: "token-cl5el1u2au5ud19qunog",
							ServiceError:    service.NewCacaoUnauthorizedError("unable to create tokens while authorized with tokens").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "valid create",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               common.NewIDFromXID("token", tokenXID),
						Owner:            validCreatedModel.Owner,
						Name:             validCreatedModel.Name,
						Description:      validCreatedModel.Description,
						Public:           validCreatedModel.Public,
						Expiration:       validCreatedModel.Expiration,
						Start:            validCreatedModel.Start,
						Scopes:           validCreatedModel.Scopes,
						DelegatedContext: validCreatedModel.DelegatedContext,
						Type:             string(validCreatedModel.Type),
						Tags:             validCreatedModel.Tags,
						Hash:             validCreatedHash,
						Salt:             validCreatedSalt,
						CreatedAt:        createdDate,
						UpdatedAt:        createdDate,
					}).Return(nil).Once()
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: func(typeString, owner string, createdAt time.Time, tokenXid xid.ID) (id common.TokenID, hash string, salt string, err error) {
					return validCreatedToken, validCreatedHash, validCreatedSalt, nil
				},
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: validCreatedModel,
				port: func() *mocks.OutgoingEventPort {
					outgoingEventPort := &mocks.OutgoingEventPort{}
					outgoingEventPort.On("TokenCreatedEvent", service.TokenCreateModel{
						Session: service.CopySessionActors(validCreatedModel.Session),
						ID:      common.NewIDFromXID("token", tokenXID),
						Token:   validCreatedToken.String(),
					}).Return(validCreatedModel)
					return outgoingEventPort
				}(),
			},
		},
		{
			name: "empty actor",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               invalidCreateModel.ID,
						Owner:            invalidCreateModel.Owner,
						Name:             invalidCreateModel.Name,
						Description:      invalidCreateModel.Description,
						Public:           invalidCreateModel.Public,
						Expiration:       time.Time{}, // invalid expiration time
						Start:            invalidCreateModel.Start,
						Scopes:           invalidCreateModel.Scopes,
						DelegatedContext: invalidCreateModel.DelegatedContext,
						Type:             string(invalidCreateModel.Type),
						Tags:             invalidCreateModel.Tags,
						Hash:             "FIXME",
						Salt:             "FIXME",
						CreatedAt:        createdDate,
						UpdatedAt:        createdDate,
					}).Return(nil).Once()
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: invalidCreateModel,
				port: func() *mocks.OutgoingEventPort {
					outgoingEventPort := &mocks.OutgoingEventPort{}
					outgoingEventPort.On("TokenCreateFailedEvent", service.TokenCreateModel{
						Session: service.Session{
							ServiceError: service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
						ID:    invalidCreateModel.ID,
						Token: invalidCreateModel.Token,
					}).Return(invalidCreateModel)
					return outgoingEventPort
				}(),
			},
		},
		{
			name: "empty expiration date",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: emptyExpirationModel,
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreateFailedEvent", service.TokenCreateModel{
						ID:    emptyExpirationModel.ID,
						Owner: emptyExpirationModel.Owner,
						Name:  emptyExpirationModel.Name,
						Session: service.Session{
							SessionActor: emptyExpirationModel.SessionActor,
							ServiceError: service.CacaoErrorBase{
								StandardMessage:   "invalid parameter error",
								ContextualMessage: "expiration cannot be empty",
							},
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "expiration date in the past",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               invalidExpirationModel.ID,
						Owner:            invalidExpirationModel.Owner,
						Name:             invalidExpirationModel.Name,
						Description:      invalidExpirationModel.Description,
						Public:           invalidExpirationModel.Public,
						Expiration:       invalidExpirationModel.Expiration,
						Start:            invalidExpirationModel.Start,
						Scopes:           invalidExpirationModel.Scopes,
						DelegatedContext: invalidExpirationModel.DelegatedContext,
						Type:             string(invalidExpirationModel.Type),
						Tags:             invalidExpirationModel.Tags,
						Hash:             "FIXME",
						Salt:             "FIXME",
						CreatedAt:        invalidExpirationModel.CreatedAt,
						UpdatedAt:        invalidExpirationModel.UpdatedAt,
					}).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: invalidExpirationModel,
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreateFailedEvent", service.TokenCreateModel{
						ID:    invalidExpirationModel.ID,
						Owner: invalidExpirationModel.Owner,
						Name:  invalidExpirationModel.Name,
						Session: service.Session{
							SessionActor: invalidExpirationModel.SessionActor,
							ServiceError: service.CacaoErrorBase{
								StandardMessage:   "invalid parameter error",
								ContextualMessage: "expiration must occur in the future",
							},
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "start date > expiration date",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               invalidStartModel.ID,
						Owner:            invalidStartModel.Owner,
						Name:             invalidStartModel.Name,
						Description:      invalidStartModel.Description,
						Public:           invalidStartModel.Public,
						Expiration:       invalidStartModel.Expiration,
						Start:            invalidStartModel.Start,
						Scopes:           invalidStartModel.Scopes,
						DelegatedContext: invalidStartModel.DelegatedContext,
						Type:             string(invalidStartModel.Type),
						Tags:             invalidStartModel.Tags,
						Hash:             "FIXME",
						Salt:             "FIXME",
						CreatedAt:        invalidStartModel.CreatedAt,
						UpdatedAt:        invalidStartModel.UpdatedAt,
					}).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: invalidStartModel,
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreateFailedEvent", service.TokenCreateModel{
						ID:    invalidStartModel.ID,
						Owner: invalidStartModel.Owner,
						Name:  invalidStartModel.Name,
						Session: service.Session{
							SessionActor: invalidStartModel.SessionActor,
							ServiceError: service.CacaoErrorBase{
								StandardMessage:   "invalid parameter error",
								ContextualMessage: "start must occur before expiration",
							},
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "invalid token type",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               invalidTypeModel.ID,
						Owner:            invalidTypeModel.Owner,
						Name:             invalidTypeModel.Name,
						Description:      invalidTypeModel.Description,
						Public:           invalidTypeModel.Public,
						Expiration:       invalidTypeModel.Expiration,
						Start:            invalidTypeModel.Start,
						Scopes:           invalidTypeModel.Scopes,
						DelegatedContext: invalidTypeModel.DelegatedContext,
						Type:             string(invalidTypeModel.Type),
						Tags:             invalidTypeModel.Tags,
						Hash:             "FIXME",
						Salt:             "FIXME",
						CreatedAt:        invalidTypeModel.CreatedAt,
						UpdatedAt:        invalidTypeModel.UpdatedAt,
					}).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: invalidTypeModel,
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreateFailedEvent", service.TokenCreateModel{
						ID:    invalidTypeModel.ID,
						Owner: invalidTypeModel.Owner,
						Name:  invalidTypeModel.Name,
						Session: service.Session{
							SessionActor: invalidTypeModel.SessionActor,
							ServiceError: service.CacaoErrorBase{
								StandardMessage:   "invalid parameter error",
								ContextualMessage: "invalid token type",
							},
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "valid delegated token",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               validDelegatedModel.ID,
						Owner:            validDelegatedModel.Owner,
						Name:             validDelegatedModel.Name,
						Description:      validDelegatedModel.Description,
						Public:           validDelegatedModel.Public,
						Expiration:       validDelegatedModel.Expiration,
						Start:            validDelegatedModel.Start,
						Scopes:           validDelegatedModel.Scopes,
						DelegatedContext: validDelegatedModel.DelegatedContext,
						Type:             string(validDelegatedModel.Type),
						Tags:             validDelegatedModel.Tags,
						Hash:             validDelegatedHash,
						Salt:             validDelegatedSalt,
						CreatedAt:        validDelegatedModel.CreatedAt,
						UpdatedAt:        validDelegatedModel.UpdatedAt,
					}).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: func(typeString, owner string, createdAt time.Time, tokenXid xid.ID) (id common.TokenID, hash string, salt string, err error) {
					return validDelegatedToken, validDelegatedHash, validDelegatedSalt, nil
				},
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: validDelegatedModel,
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreatedEvent", service.TokenCreateModel{
						ID: validDelegatedModel.ID,
						Session: service.Session{
							SessionActor: validDelegatedModel.SessionActor,
						},
						Token: validDelegatedToken.String(),
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "missing delegated context",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Create", types.Token{
						ID:               missingDelegatedModel.ID,
						Owner:            missingDelegatedModel.Owner,
						Name:             missingDelegatedModel.Name,
						Description:      missingDelegatedModel.Description,
						Public:           missingDelegatedModel.Public,
						Expiration:       missingDelegatedModel.Expiration,
						Start:            missingDelegatedModel.Start,
						Scopes:           missingDelegatedModel.Scopes,
						DelegatedContext: missingDelegatedModel.DelegatedContext,
						Type:             string(missingDelegatedModel.Type),
						Tags:             missingDelegatedModel.Tags,
						Hash:             "FIXME",
						Salt:             "FIXME",
						CreatedAt:        missingDelegatedModel.CreatedAt,
						UpdatedAt:        missingDelegatedModel.UpdatedAt,
					}).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{
						Actor: "test-username",
					}).Return(false, nil)
					return userSvc
				}(),
				XIDGenerator: func() xid.ID {
					return tokenXID
				},
				TokenGenerator: nil,
				TimeSrc: func() time.Time {
					return createdDate
				},
			},
			args: args{
				request: missingDelegatedModel,
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenCreateFailedEvent", service.TokenCreateModel{
						ID:    missingDelegatedModel.ID,
						Owner: missingDelegatedModel.Owner,
						Name:  missingDelegatedModel.Name,
						Session: service.Session{
							SessionActor: missingDelegatedModel.SessionActor,
							ServiceError: service.CacaoErrorBase{
								StandardMessage:   "invalid parameter error",
								ContextualMessage: "delegated context cannot be empty",
							},
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		// TODO add more tests
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := EventHandlers{
				storage:        tt.fields.storage,
				userSvc:        tt.fields.userSvc,
				XIDGenerator:   tt.fields.XIDGenerator,
				TokenGenerator: tt.fields.TokenGenerator,
				TimeSrc:        tt.fields.TimeSrc,
			}
			h.Create(tt.args.request, tt.args.port)
		})
	}
}

func TestEventHandlers_Delete(t *testing.T) {
	deleteDate := time.Now()
	type fields struct {
		storage *mocks.PersistentStoragePort
		userSvc *mocks.UserService
		TimeSrc func() time.Time
	}
	type args struct {
		request service.TokenModel
		port    *mocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "token emulator",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return deleteDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{
						SessionEmulator: "token-cl5elnm2au5uvki4ic3g",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenDeleteFailedEvent", service.TokenModel{
						Session: service.Session{
							SessionEmulator: "token-cl5elnm2au5uvki4ic3g",
							ServiceError:    service.NewCacaoUnauthorizedError("unable to delete tokens while authorized with tokens").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "empty actor",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return deleteDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID: "token-cl5el1u2au5ud19qunog",
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator123",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenDeleteFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "emulator123",
							ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "missing ID",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return deleteDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{SessionActor: "actor123"},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenDeleteFailedEvent", service.TokenModel{
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoInvalidParameterError("input validation error: token id is empty").GetBase(),
						},
					})
					return port
				}(),
			},
		},
		{
			name: "trying to delete other user token",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{
						ID:    "token-cl5el1u2au5ud19qunog",
						Owner: "otheruser123",
						Name:  "token-other",
					}, nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return deleteDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{SessionActor: "actor123"},
					ID:      "token-cl5el1u2au5ud19qunog",
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenDeleteFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoUnauthorizedError("must be token owner or admin to delete").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "valid delete",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{
						ID:    "token-cl5el1u2au5ud19qunog",
						Owner: "actor123",
					}, nil)
					storage.On("Delete", "actor123", common.ID("token-cl5el1u2au5ud19qunog")).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return deleteDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{SessionActor: "actor123"},
					ID:      "token-cl5el1u2au5ud19qunog",
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenDeletedEvent", service.TokenModel{
						Session: service.Session{SessionActor: "actor123"},
						ID:      "token-cl5el1u2au5ud19qunog",
						Owner:   "actor123",
					}).Return(nil)
					return port
				}(),
			},
		},
		// TODO add more tests
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := EventHandlers{
				storage: tt.fields.storage,
				userSvc: tt.fields.userSvc,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Delete(tt.args.request, tt.args.port)
		})
	}
}

func TestEventHandlers_Revoke(t *testing.T) {
	revokeDate := time.Now()
	type fields struct {
		storage ports.PersistentStoragePort
		userSvc ports.UserService
		TimeSrc func() time.Time
	}
	type args struct {
		request service.TokenModel
		port    ports.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "token emulator",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return revokeDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{
						SessionEmulator: "token-cl5el1u2au5ud19qunog",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenRevokeFailedEvent", service.TokenModel{
						Session: service.Session{
							SessionEmulator: "token-cl5el1u2au5ud19qunog",
							ServiceError:    service.NewCacaoUnauthorizedError("unable to revoke tokens while authorized with tokens").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "empty actor",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return revokeDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID: "token-cl5el1u2au5ud19qunog",
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator123",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenRevokeFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "emulator123",
							ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "missing ID",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return revokeDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{SessionActor: "actor123"},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenRevokeFailedEvent", service.TokenModel{
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoInvalidParameterError("input validation error: token id is empty").GetBase(),
						},
					})
					return port
				}(),
			},
		},
		{
			name: "revoking other user token",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "some-other-user",
						Name:       "token-other",
						Public:     false,
						Expiration: revokeDate.Add(time.Hour * 48),
						Scopes:     "api",
						Type:       "personal",
					}, nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return revokeDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{SessionActor: "actor123"},
					ID:      "token-cl5el1u2au5ud19qunog",
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenRevokeFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoUnauthorizedError("must be token owner or admin to revoke").GetBase(),
						},
					})
					return port
				}(),
			},
		},
		{
			name: "valid revoke",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "actor123",
						Name:       "token-1",
						Public:     false,
						Expiration: time.Now().Add(time.Hour * 48),
						Scopes:     "api",
						Type:       "personal",
					}, nil).Once()
					storage.On("Revoke", "actor123", common.ID("token-cl5el1u2au5ud19qunog")).Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return revokeDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID: "token-cl5el1u2au5ud19qunog",
					Session: service.Session{
						SessionActor: "actor123",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenRevokedEvent", service.TokenModel{
						Session:    service.Session{SessionActor: "actor123"},
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "actor123",
						Name:       "token-1",
						Public:     false,
						Expiration: revokeDate,
						Scopes:     "api",
						Type:       "personal",
					}).Return(nil)
					return port
				}(),
			},
		},
		// TODO add more tests
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := EventHandlers{
				storage: tt.fields.storage,
				userSvc: tt.fields.userSvc,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Revoke(tt.args.request, tt.args.port)
		})
	}
}

func TestEventHandlers_Update(t *testing.T) {
	updateDate := time.Now()
	type fields struct {
		storage ports.PersistentStoragePort
		userSvc ports.UserService
		TimeSrc func() time.Time
	}
	type args struct {
		request service.TokenModel
		port    ports.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "token emulator",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return updateDate
				},
			},
			args: args{
				request: service.TokenModel{
					Session: service.Session{
						SessionEmulator: "token-cl5el1u2au5ud19qunog",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenUpdateFailedEvent", service.TokenModel{
						Session: service.Session{
							SessionEmulator: "token-cl5el1u2au5ud19qunog",
							ServiceError:    service.NewCacaoUnauthorizedError("unable to update tokens while authorized with tokens").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "empty actor",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return updateDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID: "token-cl5el1u2au5ud19qunog",
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator123",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenUpdateFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "emulator123",
							ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "trying to update non-existent token",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{}, service.NewCacaoNotFoundError("unable to get the token for id token-cl5el1u2au5ud19qunog"))
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return updateDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID:      "token-cl5el1u2au5ud19qunog",
					Session: service.Session{SessionActor: "actor123"},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenUpdateFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoNotFoundError("unable to get the token for id token-cl5el1u2au5ud19qunog").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "update other user's token by non-admin user",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "someotheruser123",
						Name:       "token-other",
						Public:     false,
						Expiration: updateDate,
						Scopes:     "api",
						Type:       "personal",
						CreatedAt:  updateDate,
						UpdatedAt:  updateDate,
					}, nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return updateDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID: "token-cl5el1u2au5ud19qunog",
					Session: service.Session{
						SessionActor: "actor123",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("TokenUpdateFailedEvent", service.TokenModel{
						ID: "token-cl5el1u2au5ud19qunog",
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoUnauthorizedError("must be token owner or admin to update").GetBase(),
						},
					}).Return(nil)
					return port
				}(),
			},
		},
		{
			name: "update other user's token by admin user",
			fields: fields{
				storage: func() *mocks.PersistentStoragePort {
					storage := &mocks.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl5el1u2au5ud19qunog")).Return(types.Token{
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "someotheruser123",
						Name:       "token-other",
						Public:     false,
						Expiration: updateDate,
						Scopes:     "api",
						Type:       "personal",
						CreatedAt:  updateDate,
						UpdatedAt:  updateDate,
					}, nil)
					storage.On("Update", types.Token{
						ID:    "token-cl5el1u2au5ud19qunog",
						Owner: "someotheruser123",
						Name:  "new-name-1",
					}, []string{"name", "description", "public", "scopes"}, "actor123").Return(nil)
					return storage
				}(),
				userSvc: func() *mocks.UserService {
					userSvc := &mocks.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(true, nil)
					return userSvc
				}(),
				TimeSrc: func() time.Time {
					return updateDate
				},
			},
			args: args{
				request: service.TokenModel{
					ID:   "token-cl5el1u2au5ud19qunog",
					Name: "new-name-1",
					Session: service.Session{
						SessionActor: "actor123",
					},
				},
				port: func() *mocks.OutgoingEventPort {
					port := &mocks.OutgoingEventPort{}
					port.On("Update", service.TokenModel{
						Session: service.Session{
							SessionActor: "actor123",
							ServiceError: service.NewCacaoUnauthorizedError("must be token owner or admin to update").GetBase(),
						},
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "someotheruser123",
						Name:       "token-other",
						Public:     false,
						Expiration: updateDate,
						Scopes:     "api",
						Type:       "personal",
						CreatedAt:  updateDate,
						UpdatedAt:  updateDate,
					}).Return(nil)
					port.On("TokenUpdatedEvent", service.TokenModel{
						Session: service.Session{
							SessionActor: "actor123",
						},
						ID:         "token-cl5el1u2au5ud19qunog",
						Owner:      "someotheruser123",
						Name:       "token-other",
						Public:     false,
						Expiration: updateDate,
						Scopes:     "api",
						Type:       "personal",
						CreatedAt:  updateDate,
						UpdatedAt:  updateDate,
					}).Return(nil)
					return port
				}(),
			},
		},
		// TODO add more tests
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := EventHandlers{
				storage: tt.fields.storage,
				userSvc: tt.fields.userSvc,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Update(tt.args.request, tt.args.port)
		})
	}
}
