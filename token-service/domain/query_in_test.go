package domain

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/ports"
	portsmock "gitlab.com/cyverse/cacao/token-service/ports/mocks"
	"gitlab.com/cyverse/cacao/token-service/types"
	"reflect"
	"testing"
	"time"
)

func TestQueryHandlers_Get(t *testing.T) {
	tokenID := common.ID("token-cl5elnm2au5uvki4ic3g")
	missingActorModel := service.TokenModel{
		ID: tokenID,
		Session: service.Session{
			SessionActor:    "",
			SessionEmulator: "emulator123",
		},
	}
	missingActor := service.Actor{
		Actor:    missingActorModel.GetSessionActor(),
		Emulator: missingActorModel.GetSessionEmulator(),
	}
	adminTokenModel := types.Token{
		ID:         tokenID,
		Owner:      "some-admin",
		Name:       "admin-token",
		Public:     false,
		Expiration: time.Now().Add(time.Hour * 24),
		Scopes:     "api",
		Type:       "internal-deploy",
	}
	nonAdminActor := service.Actor{
		Actor:    "some-user",
		Emulator: "",
	}
	type fields struct {
		storage *portsmock.PersistentStoragePort
		userSvc *portsmock.UserService
	}
	type args struct {
		request service.TokenModel
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.TokenModel
	}{
		{
			name: "missing token ID",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					return userSvc
				}(),
			},
			args: args{request: service.TokenModel{
				Session: service.Session{
					SessionActor: "actor123",
				},
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: "actor123",
					ServiceError: service.NewCacaoInvalidParameterError("input validation error: token ID is empty").GetBase(),
				},
			},
		},
		{
			name: "missing actor",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", tokenID).Return(types.ConvertFromModel(missingActorModel), nil)
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					userSvc.On("CheckForAdmin", missingActor).Return(false, errors.New("unauthorized access issue checking for admin status"))
					return userSvc
				}(),
			},
			args: args{
				request: missingActorModel,
			},
			want: service.TokenModel{
				Session: service.Session{
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoUnauthorizedError("issue checking for admin status").GetBase(),
				},
			},
		},
		{
			name: "non-admin getting internal-deploy tokens",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", tokenID).Return(adminTokenModel, nil)
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					userSvc.On("CheckForAdmin", nonAdminActor).Return(false, nil)
					return userSvc
				}(),
			},
			args: args{request: service.TokenModel{
				ID: tokenID,
				Session: service.Session{
					SessionActor:    nonAdminActor.Actor,
					SessionEmulator: nonAdminActor.Emulator,
				},
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor:    nonAdminActor.Actor,
					SessionEmulator: nonAdminActor.Emulator,
					ServiceError:    service.NewCacaoUnauthorizedError("must be an admin to use internal tokens").GetBase(),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := QueryHandlers{
				storage: tt.fields.storage,
				userSvc: tt.fields.userSvc,
			}
			if got := h.Get(tt.args.request); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQueryHandlers_List(t *testing.T) {
	expirationDate := time.Now().Add(time.Hour * 48)
	type fields struct {
		storage ports.PersistentStoragePort
		userSvc ports.UserService
	}
	type args struct {
		request service.Session
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.TokenListModel
	}{
		{
			name: "empty actor",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					return userSvc
				}(),
			},
			args: args{
				request: service.Session{
					SessionActor:    "",
					SessionEmulator: "emulator123",
				},
			},
			want: service.TokenListModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
				},
			},
		},
		{
			name: "empty token list",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("List", "actor123").Return([]types.Token{}, nil)
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					return userSvc
				}(),
			},
			args: args{
				request: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "",
				},
			},
			want: service.TokenListModel{
				Session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "",
				},
				Tokens: make([]service.TokenModel, 0),
			},
		},
		{
			name: "token list some",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("List", "actor123").Return([]types.Token{
						{ID: "token-1", Owner: "actor123", Name: "token-name-1", Expiration: expirationDate, Scopes: "api", Type: "personal"},
						{ID: "token-2", Owner: "actor123", Name: "token-name-2", Expiration: expirationDate, Scopes: "api", Type: "personal"},
					}, nil)
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					return userSvc
				}(),
			},
			args: args{
				request: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "",
				},
			},
			want: service.TokenListModel{
				Session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "",
				},
				Tokens: []service.TokenModel{
					{ID: "token-1", Owner: "actor123", Name: "token-name-1", Expiration: expirationDate, Scopes: "api", Type: "personal"},
					{ID: "token-2", Owner: "actor123", Name: "token-name-2", Expiration: expirationDate, Scopes: "api", Type: "personal"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := QueryHandlers{
				storage: tt.fields.storage,
				userSvc: tt.fields.userSvc,
			}
			if got := h.List(tt.args.request); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQueryHandlers_ListTokenTypes(t *testing.T) {
	type fields struct {
		storage ports.PersistentStoragePort
		userSvc ports.UserService
	}
	type args struct {
		request service.Session
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.TokenTypeListModel
	}{
		{
			name: "empty actor",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					return userSvc
				}(),
			},
			args: args{request: service.Session{
				SessionActor:    "",
				SessionEmulator: "emulator123",
			}},
			want: service.TokenTypeListModel{Session: service.Session{
				SessionEmulator: "emulator123",
				ServiceError:    service.NewCacaoInvalidParameterError("actor is empty").GetBase(),
			}},
		},
		{
			name: "non-admin list token types",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "actor123"}).Return(false, nil)
					return userSvc
				}(),
			},
			args: args{request: service.Session{
				SessionActor: "actor123",
			}},
			want: service.TokenTypeListModel{
				Session: service.Session{
					SessionActor: "actor123",
				},
				TokenTypes: []service.TokenType{"personal", "delegated"},
			},
		},
		{
			name: "admin list token types",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				userSvc: func() *portsmock.UserService {
					userSvc := &portsmock.UserService{}
					userSvc.On("CheckForAdmin", service.Actor{Actor: "admin-user"}).Return(true, nil)
					return userSvc
				}(),
			},
			args: args{request: service.Session{
				SessionActor: "admin-user",
			}},
			want: service.TokenTypeListModel{
				Session: service.Session{
					SessionActor: "admin-user",
				},
				TokenTypes: []service.TokenType{"personal", "delegated", "internal-deploy"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := QueryHandlers{
				storage: tt.fields.storage,
				userSvc: tt.fields.userSvc,
			}
			if got := h.ListTokenTypes(tt.args.request); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTokenTypes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQueryHandlers_AuthCheckToken(t *testing.T) {
	now := time.Now()
	//id1 := xid.NewWithTime(time.Date(2023, 11, 14, 0, 0, 0, 0, time.UTC))
	//tokenStr, tokenHash, tokenSalt, err := common.NewTokenID(service.PersonalTokenType.String(), "testuser123", now, id1)
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println(id1.String())
	//fmt.Println(tokenStr)
	//fmt.Println(tokenHash)
	//fmt.Println(tokenSalt)
	//return

	type fields struct {
		storage ports.PersistentStoragePort
		userSvc ports.UserService
		TimeSrc func() time.Time
	}
	type args struct {
		request service.TokenAuthCheckRequest
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.TokenModel
	}{
		{
			name: "success",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:               "token-cl9bh07uo8sjoeqnph50",
						Owner:            "testuser123",
						Name:             "token-name123",
						Description:      "desc-123",
						Public:           false,
						Expiration:       now.Add(time.Hour).UTC(),
						Start:            nil,
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfd507a111ec61457f183db8",
						Salt:             "acb9f0b5535edda05930d57d5b7d7273",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Minute),
						UpdatedAt:        now.Add(-1 * time.Minute),
					}, nil)
					storage.On("TokenAccessed", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "testuser123",
			},
		},
		{
			name: "success with start time",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:          "token-cl9bh07uo8sjoeqnph50",
						Owner:       "testuser123",
						Name:        "token-name123",
						Description: "desc-123",
						Public:      false,
						Expiration:  now.Add(time.Hour).UTC(),
						Start: func() *time.Time {
							start := now.Add(-1 * time.Second) // start being valid 1 sec ago
							return &start
						}(),
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfd507a111ec61457f183db8",
						Salt:             "acb9f0b5535edda05930d57d5b7d7273",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Minute),
						UpdatedAt:        now.Add(-1 * time.Minute),
					}, nil)
					storage.On("TokenAccessed", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "testuser123",
			},
		},
		{
			name: "non-system actor",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: "random-actor",
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: "random-actor",
					ServiceError: service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "emulator",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor:    service.ReservedCacaoSystemActor,
					SessionEmulator: "random-actor",
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor:    service.ReservedCacaoSystemActor,
					SessionEmulator: "random-actor",
					ServiceError:    service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "invalid token, no xid postfix",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-00000000000000000000",
				Owner: "",
			},
		},
		{
			name: "invalid token, empty string",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-00000000000000000000",
				Owner: "",
			},
		},
		{
			name: "token not found",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{}, service.NewCacaoNotFoundError("not found123"))
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "salt not hex encoding",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:               "token-cl9bh07uo8sjoeqnph50",
						Owner:            "testuser123",
						Name:             "token-name123",
						Description:      "desc-123",
						Public:           false,
						Expiration:       now.Add(time.Hour).UTC(),
						Start:            nil,
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfd507a111ec61457f183db8",
						Salt:             "acb9f0b5535edda05AAAA___AAAAAAAA",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Minute),
						UpdatedAt:        now.Add(-1 * time.Minute),
					}, nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "hash mismatch",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:               "token-cl9bh07uo8sjoeqnph50",
						Owner:            "testuser123",
						Name:             "token-name123",
						Description:      "desc-123",
						Public:           false,
						Expiration:       now.Add(time.Hour).UTC(),
						Start:            nil,
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfaaaaaaaaaaaaaaaaaaaaaa",
						Salt:             "acb9f0b5535edda05930d57d5b7d7273",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Minute),
						UpdatedAt:        now.Add(-1 * time.Minute),
					}, nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("invalid token").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "expired token",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:               "token-cl9bh07uo8sjoeqnph50",
						Owner:            "testuser123",
						Name:             "token-name123",
						Description:      "desc-123",
						Public:           false,
						Expiration:       now.Add(-1 * time.Second).UTC(), // expired 1 sec ago
						Start:            nil,
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfd507a111ec61457f183db8",
						Salt:             "acb9f0b5535edda05930d57d5b7d7273",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Hour),
						UpdatedAt:        now.Add(-1 * time.Hour),
					}, nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("token has expired").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "not yet valid",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:          "token-cl9bh07uo8sjoeqnph50",
						Owner:       "testuser123",
						Name:        "token-name123",
						Description: "desc-123",
						Public:      false,
						Expiration:  now.Add(time.Hour).UTC(),
						Start: func() *time.Time {
							start := now.Add(time.Second) // start 1 sec later
							return &start
						}(),
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfd507a111ec61457f183db8",
						Salt:             "acb9f0b5535edda05930d57d5b7d7273",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Minute),
						UpdatedAt:        now.Add(-1 * time.Minute),
					}, nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					ServiceError: service.NewCacaoUnauthorizedError("token has not started").GetBase(),
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "",
			},
		},
		{
			name: "fail to update access time",
			fields: fields{
				storage: func() *portsmock.PersistentStoragePort {
					storage := &portsmock.PersistentStoragePort{}
					storage.On("Get", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(types.Token{
						ID:               "token-cl9bh07uo8sjoeqnph50",
						Owner:            "testuser123",
						Name:             "token-name123",
						Description:      "desc-123",
						Public:           false,
						Expiration:       now.Add(time.Hour).UTC(),
						Start:            nil,
						Scopes:           "",
						DelegatedContext: "",
						Type:             string(service.PersonalTokenType),
						Tags:             nil,
						Hash:             "9635c4c3e5773a138f7dc4b1a4e4fbd29f1f3324cfd507a111ec61457f183db8",
						Salt:             "acb9f0b5535edda05930d57d5b7d7273",
						LastAccessed:     nil,
						CreatedAt:        now.Add(-2 * time.Minute),
						UpdatedAt:        now.Add(-1 * time.Minute),
					}, nil)
					storage.On("TokenAccessed", common.ID("token-cl9bh07uo8sjoeqnph50")).Return(fmt.Errorf("failed 123")) // no effect, should still return success reply with no error
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{request: service.TokenAuthCheckRequest{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
				},
				Token: "cptoken_7baf4a7bfd72a2fda3b2248638b348489afb8589b5b93d032ea432e541ec2530d5411a3a1c63386c6074294359f9924a567327003aa2ee812f1001b4424ce459_010000000edce6587339ffe0cafe5c_cl9bh07uo8sjoeqnph50",
			}},
			want: service.TokenModel{
				Session: service.Session{
					SessionActor: service.ReservedCacaoSystemActor,
					// success reply, no error
				},
				ID:    "token-cl9bh07uo8sjoeqnph50",
				Owner: "testuser123",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := QueryHandlers{
				storage: tt.fields.storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			got := h.AuthCheckToken(tt.args.request)
			if !assert.Equal(t, tt.want, got) {
				t.Errorf("AuthCheckToken() = %v, want %v", got, tt.want)
				return
			}
			tt.fields.storage.(*portsmock.PersistentStoragePort).AssertExpectations(t)
		})
	}
}
