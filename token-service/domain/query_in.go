package domain

import (
	"encoding/hex"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/ports"
	"gitlab.com/cyverse/cacao/token-service/types"
	"golang.org/x/crypto/sha3"
	"time"
)

// QueryHandlers implements ports.IncomingQueryHandlers
type QueryHandlers struct {
	storage ports.PersistentStoragePort
	userSvc ports.UserService
	TimeSrc func() time.Time
}

// ListTokenTypes lists the available types of tokens for the TokenModel.TokenType enum
func (h QueryHandlers) ListTokenTypes(request cacao_common_service.Session) cacao_common_service.TokenTypeListModel {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "QueryHandlers.ListTokenTypes",
	})
	logger.Info("List token types")

	if len(request.GetSessionActor()) <= 0 {
		errorMessage := "actor is empty"
		err := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		logger.WithError(err).Error(errorMessage)
		return h.listTokenTypesErrorReply(request, err)
	}

	sessionActor := cacao_common_service.Actor{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
	}
	actorIsAdmin, err := h.userSvc.CheckForAdmin(sessionActor)
	var tokenListReply cacao_common_service.TokenTypeListModel
	if err != nil {
		logger.WithError(err).Error("error checking user for admin")
		return h.listTokenTypesErrorReply(request, err)
	}
	if actorIsAdmin {
		tokenListReply = cacao_common_service.TokenTypeListModel{
			Session:    request,
			TokenTypes: []cacao_common_service.TokenType{cacao_common_service.PersonalTokenType, cacao_common_service.DelegatedTokenType, cacao_common_service.InternalDeployType},
		}
	} else {
		tokenListReply = cacao_common_service.TokenTypeListModel{
			Session:    request,
			TokenTypes: []cacao_common_service.TokenType{cacao_common_service.PersonalTokenType, cacao_common_service.DelegatedTokenType},
		}
	}
	return tokenListReply
}

// List lists the tokens that the session actor owns
func (h QueryHandlers) List(request cacao_common_service.Session) cacao_common_service.TokenListModel {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "QueryHandlers.List",
	})
	logger.Info("List tokens")

	if len(request.SessionActor) == 0 {
		return h.listErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty"))
	}

	tokenList, err := h.storage.List(request.SessionActor)
	if err != nil {
		logger.WithError(err).Error("fail to list tokens from storage")
		return h.listErrorReply(request, err)
	}
	logger.WithField("len", len(tokenList)).Info("Listed")

	var listReply cacao_common_service.TokenListModel
	listReply.Session = cacao_common_service.CopySessionActors(request)
	var models = make([]cacao_common_service.TokenModel, 0, len(tokenList))
	for _, token := range tokenList {
		model := types.ConvertToModel(cacao_common_service.Session{}, token)
		models = append(models, model)
	}
	listReply.Tokens = models
	return listReply
}

// AuthCheckToken checks if a token is valid or not (authN), check authorization (authZ) and query its owner if valid.
func (h QueryHandlers) AuthCheckToken(request cacao_common_service.TokenAuthCheckRequest) cacao_common_service.TokenModel {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "QueryHandlers.ValidateToken",
		"actor":    request.GetSessionActor(),
		"emulator": request.GetSessionEmulator(),
	})
	if request.GetSessionActor() != cacao_common_service.ReservedCacaoSystemActor || request.GetSessionEmulator() != "" {
		// system actor only, no emulator
		// there shouldn't be a need for normal user to check authN/authZ of a token.
		logger.Warn("someone is emitting query they are not supposed to, system actor only, no emulator")
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("invalid token")) // return the same error as invalid token
	}
	if !request.Token.Validate() {
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("invalid token"))
	}

	tokenID := common.NewIDFromXID("token", request.Token.XID())
	token, err := h.storage.Get(tokenID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch token from storage")
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("invalid token"))
	}
	logger = logger.WithField("id", token.ID)

	// validate hash
	saltBytes, err := hex.DecodeString(token.Salt)
	if err != nil {
		logger.Error("could not decode token salt")
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("invalid token"))
	}
	hash := sha3.New256()
	hash.Write([]byte(request.Token))
	hash.Write(saltBytes)
	if token.Hash != hex.EncodeToString(hash.Sum(nil)) {
		logger.Error("could not validate token hash")
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("invalid token"))
	}

	now := h.TimeSrc().UTC()
	// make sure token hasn't expired
	if token.Expiration.Before(now) {
		logger.Info("token expired")
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("token has expired"))
	}
	// make sure token has actually started if token.Start is set
	if token.Start != nil && token.Start.After(now) {
		logger.Info("token not yet take effect")
		return h.authCheckErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("token has not started"))
	}
	logger.WithField("tokenID", token.ID).Info("validated")

	// TODO check authorization of operation once we support scope and delegation, the request should contain operation identifier.

	err = h.storage.TokenAccessed(tokenID)
	if err != nil {
		logger.WithError(err).Error("fail to update access time of token")
		// log and continue
	}

	// return the minimal amount of info needed to grant authorization
	return cacao_common_service.TokenModel{
		Session: cacao_common_service.CopySessionActors(request.Session),
		ID:      token.ID,
		Owner:   token.Owner,
	}
}

// Get retrieves a single token based on ID
func (h QueryHandlers) Get(request cacao_common_service.TokenModel) cacao_common_service.TokenModel {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "QueryHandlers.Get",
	})
	if len(request.ID) == 0 {
		return h.getErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: token ID is empty"))
	}

	logger.Infof("Get token %s", request.ID.String())

	token, err := h.storage.Get(request.ID)

	if err != nil {
		logger.WithError(err).Error("fail to fetch token from storage")
		return h.getErrorReply(request, err)
	}

	if request.GetSessionActor() == cacao_common_service.ReservedCacaoSystemActor {
		request.SetSessionActor(token.Owner)
	}

	actorIsAdmin, err := h.userSvc.CheckForAdmin(cacao_common_service.Actor{Actor: request.GetSessionActor(), Emulator: request.GetSessionEmulator()})
	if err != nil {
		return h.getErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("issue checking for admin status"))
	}

	if token.Type == string(cacao_common_service.InternalDeployType) && !actorIsAdmin {
		return h.getErrorReply(request, cacao_common_service.NewCacaoUnauthorizedError("must be an admin to use internal tokens"))
	}

	return types.ConvertToModel(cacao_common_service.CopySessionActors(request.Session), token)
}

func (h QueryHandlers) listTokenTypesErrorReply(request cacao_common_service.Session, err error) cacao_common_service.TokenTypeListModel {
	var listTokenTypesReply cacao_common_service.TokenTypeListModel
	listTokenTypesReply.Session = cacao_common_service.CopySessionActors(request)
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		listTokenTypesReply.ServiceError = cerr.GetBase()
	} else {
		listTokenTypesReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return listTokenTypesReply
}

func (h QueryHandlers) listErrorReply(request cacao_common_service.Session, err error) cacao_common_service.TokenListModel {
	var listReply cacao_common_service.TokenListModel
	listReply.Session = cacao_common_service.CopySessionActors(request)
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		listReply.ServiceError = cerr.GetBase()
	} else {
		listReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return listReply
}

// error reply for get request
func (h *QueryHandlers) getErrorReply(request cacao_common_service.TokenModel, err error) cacao_common_service.TokenModel {
	var getReply cacao_common_service.TokenModel
	getReply.Session = cacao_common_service.CopySessionActors(request.Session)
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		getReply.ServiceError = cerr.GetBase()
	} else {
		getReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return getReply
}

func (h *QueryHandlers) authCheckErrorReply(request cacao_common_service.TokenAuthCheckRequest, err cacao_common_service.CacaoError) cacao_common_service.TokenModel {
	var getReply cacao_common_service.TokenModel
	getReply.Session = cacao_common_service.CopySessionActors(request.Session)
	getReply.ServiceError = err.GetBase()
	getReply.ID = common.NewIDFromXID("token", request.Token.XID())
	return getReply
}

var _ ports.IncomingQueryHandlers = &QueryHandlers{}
