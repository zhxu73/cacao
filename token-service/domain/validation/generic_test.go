package validation

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"strings"
	"testing"
	"time"
)

func Test_validateGenericToken(t *testing.T) {
	type args struct {
		token service.TokenModel
	}
	validStartTime := time.Now().Add(time.Hour * 12)
	invalidStartTime := time.Now().Add(time.Hour * 48)
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "valid without start",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "valid with start",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Start:       &validStartTime,
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "valid delegated type with delegated context",
			args: args{
				token: service.TokenModel{
					ID:               "token-0000-test",
					Owner:            "testuser0001",
					Name:             "testtoken0001",
					Description:      "test token description",
					Public:           true,
					Expiration:       time.Now().Add(time.Hour * 24),
					Start:            &validStartTime,
					Scopes:           "api",
					Type:             "delegated",
					DelegatedContext: "testcontext",
					CreatedAt:        time.Now(),
					UpdatedAt:        time.Now(),
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "invalid start time",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Start:       &invalidStartTime,
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		},
		{
			name: "missing expiration",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		},
		{
			name: "missing type",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		},
		{
			name: "unknown type",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "token-type123",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		},
		{
			name: "missing owner",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		}, {
			name: "missing name",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		}, {
			name: "description too long",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: strings.Repeat("this description is way too long ", 100),
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Start:       &validStartTime,
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		}, {
			name: "invalid scopes characters",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api\u0000",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		}, {
			name: "valid tags",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
					Tags:        map[string]string{"validkey": "validvalue"},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "invalid tag key characters",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
					Tags:        map[string]string{"invalid@key": "validvalue"},
				},
			},
			wantErr: assert.Error,
		}, {
			name: "invalid tag value characters",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "personal",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
					Tags:        map[string]string{"validkey": "valid#value"},
				},
			},
			wantErr: assert.Error,
		}, {
			name: "missing delegated context when type=delegated",
			args: args{
				token: service.TokenModel{
					ID:          "token-0000-test",
					Owner:       "testuser0001",
					Name:        "testtoken0001",
					Description: "test token description",
					Public:      true,
					Expiration:  time.Now().Add(time.Hour * 24),
					Scopes:      "api",
					Type:        "delegated",
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
				},
			},
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, validateGenericToken(tt.args.token), fmt.Sprintf("validateGenericToken(%+v)", tt.args.token))
		})
	}
}
