package validation

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"strings"
	"unicode"
)

const (
	maxTokenNameLength        = 100
	maxTokenOwnerLength       = 100
	maxTokenDescriptionLength = 1000
	maxTagCount               = 50 // max number of tag per token
	maxTagKeyLength           = 63
	maxTagValueLength         = 63
)

// TokenValidator validates token fields
type TokenValidator func(model service.TokenModel) error

// ValidateToken validates that a token's fields are valid
func ValidateToken(token service.TokenModel, isAdmin bool) error {
	err := validateToken(token, isAdmin)
	if err != nil {
		return service.NewCacaoInvalidParameterError(err.Error())
	}
	return nil
}

func validateToken(token service.TokenModel, isAdmin bool) error {
	var err error
	if err = validateGenericToken(token); err != nil {
		return err
	}
	switch token.Type {
	case "delegated":
		err = validateDelegatedToken(token)
	case "internal-deploy":
		err = validateInternalDeployToken(token, isAdmin)
	}
	return err
}

func validateInternalDeployToken(_ service.TokenModel, admin bool) error {
	if !admin {
		return fmt.Errorf("must be an administrator to create internal-deploy tokens")
	}
	return nil
}

func validateDelegatedToken(token service.TokenModel) error {
	if len(token.DelegatedContext) <= 0 {
		return fmt.Errorf("delegated context cannot be empty")
	}
	// TODO not entirely sure what a delegated context can contain...
	return nil
}

func validateGenericToken(token service.TokenModel) error {
	var err error
	if err = validateTokenTags(token.Tags); err != nil {
		return err
	}
	if err = validateTokenName(token.Name); err != nil {
		return err
	}
	if err = validateTokenOwner(token.Owner); err != nil {
		return err
	}
	if err = validateTokenType(token.Type); err != nil {
		return err
	}
	if err = validateTokenScopes(token.Scopes); err != nil {
		return err
	}
	if err = validateTokenDescriptionLength(token.Description); err != nil {
		return err
	}
	if err = validateExpirationTime(token); err != nil {
		return err
	}
	if err = validateTokenStartTime(token); err != nil {
		return err
	}
	if token.Type == service.DelegatedTokenType {
		err = validateDelegatedToken(token)
		if err != nil {
			return err
		}
	}
	return err
}

func validateExpirationTime(token service.TokenModel) error {
	if token.Expiration.IsZero() {
		return fmt.Errorf("expiration cannot be empty")
	}
	return nil
}

func validateTokenStartTime(token service.TokenModel) error {
	if token.Start != nil && !token.Start.Before(token.Expiration) {
		return fmt.Errorf("start must occur before expiration")
	}
	return nil
}

func validateTokenOwner(owner string) error {
	if len(owner) <= 0 {
		return fmt.Errorf("token owner cannot be empty")
	}
	if len(owner) > maxTokenOwnerLength {
		return fmt.Errorf("token owner is too long")
	}
	return nil
}

func validateTokenScopes(scopes string) error {
	// make sure each of the scopes have acceptable characters. we're expecting a space-delimited series of scopes.
	for _, subScope := range strings.Split(scopes, " ") {
		for i, scopeRune := range subScope {
			if !isAllowedScopeCharacters(scopeRune) {
				return fmt.Errorf("invalid character in token scope %s at index %d", subScope, i)
			}
		}
	}
	return nil
}

func validateTokenDescriptionLength(description string) error {
	if len(description) > maxTokenDescriptionLength {
		return fmt.Errorf("token description length too long")
	}
	return nil
}

func validateTokenType(tokenType service.TokenType) error {
	if len(tokenType) <= 0 {
		return fmt.Errorf("token type cannot be empty")
	}
	switch tokenType {
	case service.PersonalTokenType:
		return nil
	case service.DelegatedTokenType:
		return nil
	case service.InternalDeployType:
		return nil
	default:
		return fmt.Errorf("invalid token type")
	}
}

func validateTokenName(name string) error {
	if len(name) <= 0 {
		return fmt.Errorf("token name cannot be empty")
	}
	if len(name) > maxTokenNameLength {
		return fmt.Errorf("token name too long")
	}
	for i, r := range name {
		if r > unicode.MaxASCII {
			// only allow ASCII
			return fmt.Errorf("invalid character in token name at index %d", i)
		}
		if r < ' ' || r > '~' {
			// check if it is outside of range of normal ASCII characters (not digit, not letter, not punct, not space)
			return fmt.Errorf("invalid character in token name at index %d", i)
		}
	}
	return nil
}

func validateTokenTags(tokenTags map[string]string) error {
	var err error
	if len(tokenTags) > maxTagCount {
		return fmt.Errorf("too many tags")
	}
	for tagKey, tagVal := range tokenTags {
		if err = validateTagKey(tagKey); err != nil {
			return err
		}
		err = validateTagValue(tagVal)
		if err != nil {
			return err
		}
	}
	return nil

}

// Note: Restrictions on tag values is determined by the common denominator of AWS/GCP/Azure tags and GCP labels.
// This is so that if we decide to propagate the tags to resources in those cloud, we can do so.
func validateTagKey(tagKey string) error {
	if tagKey == "" {
		return fmt.Errorf("tag key cannot be empty")
	}
	if len(tagKey) > maxTagKeyLength {
		return fmt.Errorf("tag key too long")
	}
	for i, r := range tagKey {
		if i == 0 {
			// start with letter.
			// Note: GCP tags/labels only allow lower case for key, but to accommodate existing data (e.g. openstack project name), upper case is allowed as well.
			if !unicode.IsLetter(r) {
				return fmt.Errorf("invalid character in token tag '%s' at index %d", tagKey, i)
			}
		}
		if !isAllowedTagKeyCharacters(r) {
			return fmt.Errorf("invalid character in token tag '%s' at index %d", tagKey, i)
		}
	}
	return nil
}

func isAllowedTagKeyCharacters(r rune) bool {
	if r > unicode.MaxASCII {
		return false
	}
	// Note: GCP tags/labels only allow lower case for key, but to accommodate existing data (e.g. openstack project name), upper case is allowed as well.
	// ofc, another approach is to toLower when apply to GCP
	if unicode.IsLetter(r) {
		return true
	}
	if unicode.IsDigit(r) {
		return true
	}
	switch r {
	case '-':
		return true
	case '_':
		return true
	}
	return false
}

// Note: Restrictions on tag values is determined by the common denominator of AWS/GCP/Azure tags and GCP labels.
// This is so that if we decide to propagate the tags to resources in those cloud, we can do so.
func validateTagValue(tagVal string) error {
	if len(tagVal) > maxTagValueLength {
		return fmt.Errorf("tag value too long")
	}
	for i, r := range tagVal {
		if !isAllowedTagValueCharacters(r) {
			return fmt.Errorf("invalid character in token tag '%s' at index %d", tagVal, i)
		}
	}
	return nil
}

func isAllowedTagValueCharacters(r rune) bool {
	if r > unicode.MaxASCII {
		return false
	}
	// Note: GCP labels only allow lower case for value. But if we switch to key-value tags entirely (REST api && service(NATS) && storage),
	// some keys might become values, so we might still need to allow upper case.
	// e.g. "<openstack-project-name>": "" => "project": "<openstack-project-name>"
	// ofc, another approach is to toLower when apply to GCP
	if unicode.IsLetter(r) {
		return true
	}
	if unicode.IsDigit(r) {
		return true
	}
	switch r {
	case '-':
		return true
	case '_':
		return true
	case '@':
		return true
	case '=':
		return true
	case '+':
		return true
	case ':':
		return true
	}
	return false
}

func isAllowedScopeCharacters(r rune) bool {
	if r > unicode.MaxASCII {
		return false
	}
	if unicode.IsLetter(r) {
		return true
	}
	if unicode.IsDigit(r) {
		return true
	}
	switch r {
	case '-':
		return true
	case '_':
		return true
	case '.':
		return true
	case ':':
		return true
	case '/':
		return true
	}
	return false
}
