package domain

import (
	"context"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/token-service/ports"
	"gitlab.com/cyverse/cacao/token-service/types"
	"sync"
	"time"
)

// Domain is the base struct for the domain service
type Domain struct {
	config        *types.Config
	Storage       ports.PersistentStoragePort
	QueryIn       ports.IncomingQueryPort
	EventIn       ports.IncomingEventPort
	UserSvc       ports.UserService
	queryHandlers *QueryHandlers
	eventHandlers *EventHandlers
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) error {
	d.config = config
	err := d.Storage.Init(config)
	if err != nil {
		return err
	}
	err = d.UserSvc.Init(config)
	if err != nil {
		return err
	}
	d.queryHandlers = &QueryHandlers{storage: d.Storage, userSvc: d.UserSvc, TimeSrc: time.Now}
	d.eventHandlers = &EventHandlers{
		storage:        d.Storage,
		userSvc:        d.UserSvc,
		XIDGenerator:   xid.New,
		TokenGenerator: common.NewTokenID,
		TimeSrc: func() time.Time {
			return time.Now().UTC()
		},
	}

	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	d.QueryIn.SetHandlers(d.queryHandlers)
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg, d.queryHandlers)
	if err != nil {
		log.WithError(err).Panic()
		return err
	}

	d.EventIn.SetHandlers(d.eventHandlers)
	wg.Add(1)
	err = d.EventIn.Start(ctx, &wg, d.eventHandlers)
	if err != nil {
		log.WithError(err).Panic()
		return err
	}
	wg.Wait()
	return nil
}
