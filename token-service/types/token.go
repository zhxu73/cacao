package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"time"
)

// Token is a struct for storing Token information.
type Token struct {
	ID               common.ID         `bson:"_id" json:"id,omitempty"`
	Owner            string            `bson:"owner" json:"owner,omitempty"`
	Name             string            `bson:"name" json:"name,omitempty"`
	Description      string            `bson:"description" json:"description,omitempty"`
	Public           bool              `bson:"public" json:"public,omitempty"`
	Expiration       time.Time         `bson:"expiration" json:"expiration,omitempty"`
	Start            *time.Time        `bson:"start" json:"start,omitempty"`
	Scopes           string            `bson:"scopes" json:"scopes,omitempty"`
	DelegatedContext string            `bson:"delegated_context" json:"delegated_context,omitempty"`
	Type             string            `bson:"type" json:"type,omitempty"`
	Tags             map[string]string `bson:"tags" json:"tags,omitempty"`
	Hash             string            `bson:"hash" json:"hash,omitempty"`
	Salt             string            `bson:"salt" json:"salt,omitempty"`
	LastAccessed     *time.Time        `bson:"last_accessed" json:"last_accessed"`
	CreatedAt        time.Time         `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt        time.Time         `bson:"updated_at" json:"updated_at,omitempty"`
}

// ConvertFromModel converts from service model to internal Token model
func ConvertFromModel(model cacao_common_service.TokenModel) Token {
	token := Token{
		ID:               model.ID,
		Owner:            model.Owner,
		Name:             model.Name,
		Description:      model.Description,
		Public:           model.Public,
		Expiration:       model.Expiration,
		Start:            model.Start,
		Scopes:           model.Scopes,
		DelegatedContext: model.DelegatedContext,
		Type:             string(model.Type),
		Tags:             model.Tags,
		Hash:             "",
		Salt:             "",
		LastAccessed:     model.LastAccessed,
		CreatedAt:        model.CreatedAt,
		UpdatedAt:        model.UpdatedAt,
	}
	return token
}

// ConvertToModel converts to service model from internal Token model + Session
func ConvertToModel(session cacao_common_service.Session, token Token) cacao_common_service.TokenModel {
	return cacao_common_service.TokenModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ServiceError:    session.ServiceError,
		},
		ID:               token.ID,
		Owner:            token.Owner,
		Name:             token.Name,
		Description:      token.Description,
		Public:           token.Public,
		Expiration:       token.Expiration,
		Start:            token.Start,
		Scopes:           token.Scopes,
		DelegatedContext: token.DelegatedContext,
		Type:             cacao_common_service.TokenType(token.Type),
		Tags:             token.Tags,
		LastAccessed:     token.LastAccessed,
		CreatedAt:        token.CreatedAt,
		UpdatedAt:        token.UpdatedAt,
		UpdateFieldNames: nil,
	}
}
