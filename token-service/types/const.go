package types

const (
	// natsQueueGroup is the NATS Queue Group for this service
	natsQueueGroup = "token_queue_group"
	// natsWildcardSubject is the NATS subject to subscribe
	natsWildcardSubject = "cyverse.token.>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "token-service"

	// natsDurableName is the NATS Durable Name for this service
	natsDurableName = "token_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao-token"
	// DefaultTokenMongoDBCollectionName is a default MongoDB Collection Name
	DefaultTokenMongoDBCollectionName = "token"
	// DefaultCacaoAPIURL is the default cacao URL for the token API
	DefaultCacaoAPIURL = "/token"
)
