package types

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
	"time"
)

// This is to ensure that Token contains the same data field as
// service.TokenModel (data field meaning not service.Session). This makes
// sure that the JSON wire format does not change.
func TestTokenJSONConvert(t *testing.T) {
	// Token => service.TokenModel => Token
	startTime := time.Now().Add(time.Hour * 24)
	expirationTime := time.Now().Add(time.Hour * 48)
	var ws1 = Token{
		ID:               "token-test-1",
		Owner:            "testuser123",
		Name:             "testtoken123",
		Description:      "description",
		Expiration:       expirationTime,
		Start:            &startTime,
		Scopes:           "deployment:testdeployment1:read",
		DelegatedContext: "testuser321",
		Type:             "delegated",
		CreatedAt:        time.Now().Add(-time.Hour).UTC(),
		UpdatedAt:        time.Now().UTC(),
	}
	var ws2 service.TokenModel
	var ws3 Token

	marshal, err := json.Marshal(ws1)
	if !assert.NoError(t, err) {
		return
	}
	err = json.Unmarshal(marshal, &ws2)
	if !assert.NoError(t, err) {
		return
	}
	marshal, err = json.Marshal(ws2)
	if !assert.NoError(t, err) {
		return
	}
	err = json.Unmarshal(marshal, &ws3)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, ws2.ID, ws3.ID)
	assert.Equal(t, ws2.Owner, ws3.Owner)
	assert.Equal(t, ws2.Name, ws3.Name)
	assert.Equal(t, ws2.Description, ws3.Description)
	assert.WithinDuration(t, ws2.Expiration, ws3.Expiration, 0)
	assert.WithinDuration(t, *ws2.Start, *ws3.Start, 0)
	assert.Equal(t, ws2.Scopes, ws3.Scopes)
	assert.Equal(t, ws2.DelegatedContext, ws3.DelegatedContext)
	assert.Equal(t, string(ws2.Type), ws3.Type)
	assert.Equal(t, ws2.ID, ws3.ID)
	assert.WithinDuration(t, ws2.CreatedAt, ws3.CreatedAt, 0)
	assert.WithinDuration(t, ws2.UpdatedAt, ws3.UpdatedAt, 0)
}

func TestConvertFromModel(t *testing.T) {
	createdAt := time.Now()
	expirationDate := time.Now().Add(time.Hour * 24)
	token := service.TokenModel{
		Session: service.Session{
			SessionActor: "test-username1",
		},
		ID:          "token-cl5el1u2au5ud19qunog",
		Owner:       "test-username1",
		Name:        "test-token1",
		Description: "test token",
		Public:      false,
		Expiration:  expirationDate,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdAt,
		UpdatedAt:   createdAt,
	}
	rawToken := Token{
		ID:          "token-cl5el1u2au5ud19qunog",
		Owner:       "test-username1",
		Name:        "test-token1",
		Description: "test token",
		Public:      false,
		Expiration:  expirationDate,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdAt,
		UpdatedAt:   createdAt,
	}
	type args struct {
		model service.TokenModel
	}
	tests := []struct {
		name string
		args args
		want Token
	}{
		{name: "valid convert", args: args{token}, want: rawToken},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, ConvertFromModel(tt.args.model), "ConvertFromModel(%v)", tt.args.model)
		})
	}
}

func TestConvertToModel(t *testing.T) {
	createdAt := time.Now()
	expirationDate := time.Now().Add(time.Hour * 24)
	session := service.Session{
		SessionActor: "test-username1",
	}
	token := service.TokenModel{
		Session:     session,
		ID:          "token-cl5el1u2au5ud19qunog",
		Owner:       "test-username1",
		Name:        "test-token1",
		Description: "test token",
		Public:      false,
		Expiration:  expirationDate,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdAt,
		UpdatedAt:   createdAt,
	}
	rawToken := Token{
		ID:          "token-cl5el1u2au5ud19qunog",
		Owner:       "test-username1",
		Name:        "test-token1",
		Description: "test token",
		Public:      false,
		Expiration:  expirationDate,
		Scopes:      "api",
		Type:        "personal",
		CreatedAt:   createdAt,
		UpdatedAt:   createdAt,
	}
	type args struct {
		session service.Session
		token   Token
	}
	tests := []struct {
		name string
		args args
		want service.TokenModel
	}{
		{name: "valid convert", args: args{session: session, token: rawToken}, want: token},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, ConvertToModel(tt.args.session, tt.args.token), "ConvertToModel(%v, %v)", tt.args.session, tt.args.token)
		})
	}
}
