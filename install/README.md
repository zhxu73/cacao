# Install

### Table of Contents
[[_TOC_]]

## Getting Started

This directory is reserved for ansible and other resources needed to deploy the CACAO platform and user clusters.

### Supported Operating Systems:

Ubuntu 18.04 or 20.04 

## CACAO Deployment

CACAO deployment is done using the `deploy.sh` script in this directory.  Currently, there are four supported environments that are passed as arguments to this script: 
* `cacaodev`: local dev environment with support to access-ci
* `ci`: continuous integration environment
* `prod`: basic production deployment, but some setup will be required
* `local`: local dev environment that supports a local keycloak deployment

### Local Development using cacao-dev.cyverse.rocks

    NOTE: to get local development working, you may need to disable maintenance and interactive-sessions-service from `skaffold.yaml`

`cacao-dev.cyverse.rocks` was setup to provide local development with access-ci, complete with SSL. 

To properly setup a local cacao-dev.cyverse.rocks development environment, follow these steps:

0. Create an ACCESS account on the JS2 allocation (ping Edwin)
1. Setup k8s DNS
    1. Get the cluster ip for nginx ingress. Be sure to grab the `CLUSTER-IP`.
    ```bash
    # kubectl get svc ingress-nginx-controller
    NAME                       TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
    ingress-nginx-controller   LoadBalancer   10.43.23.139   10.0.183.9    80:30966/TCP,443:31052/TCP   23h
    ```
    2. edit the coredns for `cacao-dev.cyverse.rocks`. If your default editor is like mine, the default editor will be `vim`
    ```bash
    # kubectl edit cm -n kube-system coredns
    ```
    3. add a new line to the NodeHosts section. Note, use the ip address you copied in the previous step. Also, indentation is important:
    ```bash
    NodeHosts: |
        10.0.183.9 cacao-dev-test-master
        10.43.23.139 cacao-dev.cyverse.rocks
    ```
    4. exit from editor
    5. restart coredns
    ```bash
    kubectl rollout restart deployment -n kube-system coredns 
    ```
    
0. `cacao` repo should be checked out into /opt/cacao
1. clone the private `cacao-dev-cyverse-rocks` repo into `/opt`
2. `cd /opt/cacao/install`
3. `./deploy.sh cacaodev`
4. optional, if you're accessing a vm remotely configured with `cacao-dev.cyverse.rocks`, you will need to tunnel port 443 to your local system. To do this, you will need to allow ssh to create ports lower than 1024. This has been tested in linux:
```bash
# setcap 'cap_net_bind_service=+ep' /usr/bin/ssh
# ssh -L 443:localhost:443 user@my-vm-ip-address
```

### Local Development

If you pass `local` as the argument to `deploy.sh`, the script will first install gettext-base, python3-pip, ansible-base, ansible-core, jq, and curl if they are not already present.  Next, the script will prompt for the following:

```
$ ./deploy.sh local
Enter your cluster type, either k3s or k3d [k3d]: 
Enter the path to the cacao-edge-deployment source code directory [/home/ubuntu/cacao-edge-deployment]: 
Enter the path to the Cacao source code directory [/home/ubuntu/cacao]: 
Enter the Git branch you want to use [master]:
Enter the URL to the Git remote repo [https://gitlab.com/cyverse/cacao.git]: 
```

The script will then generate a configuration file called `config.local.yaml`, symbolically link it to `config.yaml`, configure an AWM key, install necessary roles from ansible-galaxy, and finally run the `playbook-cacao.yaml` Ansible playbook if ran as root.  If not root, it will add your user to the docker group and instruct you to "please log out, log back in, and re-run this script."  At that point it will run `playbook-cacao.yaml`.

playbook-cacao.yaml will install docker and k3s or k3d based on your response to the prompt above and provision two Kubernetes clusters, one for running the application itself (the "service cluster") and one for running the analyses (the "user cluster").  It will then deploy the CACAO application itself and any required dependencies to these clusters, including Vault, Keycloak, and MongoDB.

Once the script has finished, move to the project's root to run `skaffold dev --status-check=false` and deploy CACAO to your local Kubernetes cluster.

An entry will be added to your `/etc/hosts` file pointing the value of `API_DOMAIN` at the IP address of the NGINX ingress gateway.

### Production deployment

TODO
