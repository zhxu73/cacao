
{{- if .Values.maintenance.start_time }}
maintenance is starting or already started.
{{- end }}

image tag for cacao repo: {{ .Values.cacao.image_tag | default "latest" }}
image tag for ionosphere: {{ .Values.ion.image_tag | default "latest" }}
image tag for user service: {{ .Values.users_microservice.image_tag | default "latest" }}
image tag for credential service: {{ .Values.creds_microservice.image_tag | default "latest" }}
image tag for wiretap service: {{ .Values.wiretap_backend.image_tag | default "latest" }}
