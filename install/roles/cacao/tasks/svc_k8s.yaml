---
###
# SERVICE CLUSTER
###

- name: COMMAND; set context to service cluster
  ansible.builtin.command: "kubectl config use-context {{ CACAO_SERVICE_CONTEXT }}"
  when: not IS_DEV_ENVIRONMENT or (CLUSTER_TYPE is defined and CLUSTER_TYPE != "k3s")

- name: COMMAND; test if ingress-nginx was already added
  ansible.builtin.command: /snap/bin/helm status ingress-nginx
  ignore_errors: true
  changed_when: false
  register: ingress_nginx_added

- name: Install nginx ingress via helm
  block:
    - name: COMMAND; helm repo add ingress-nginx
      ansible.builtin.command: /snap/bin/helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
      register: result
      changed_when: "'already exists with the same configuration, skipping' in result.stdout"

    - name: COMMAND; helm repo update
      ansible.builtin.command: /snap/bin/helm repo update

    - name: COMMAND; helm install ingress-nginx
      ansible.builtin.command: /snap/bin/helm install ingress-nginx ingress-nginx/ingress-nginx --set controller.config.proxy-buffer-size=16k
  when: ingress_nginx_added is failed

- name: COMMAND; Get proxy buffer size for nginx ingress
  ansible.builtin.command: kubectl get cm ingress-nginx-controller -o jsonpath='{.data.proxy-buffer-size}'
  changed_when: false
  register: nginx_buffer_size

# increase proxy-buffer-size to 16k (default 4k) to account for large request header
# this is mainly to patch proxy-buffer-size for existing installation of nginx ingress
- name: COMMAND; Patch proxy buffer size for nginx
  ansible.builtin.command: kubectl patch configmap/ingress-nginx-controller --type merge -p '{"data":{"proxy-buffer-size":"16k"}}'
  when: nginx_buffer_size.stdout != "16k"

- name: COMMAND; wait for ingress-nginx-controller to be available
  ansible.builtin.command: "kubectl wait deployment/ingress-nginx-controller --for=condition=available --timeout={{ CACAO_POD_WAIT_MINUTES }}m"
  changed_when: false

- name: COMMAND; check if TLS secret exists
  ansible.builtin.command: "kubectl get secret {{ TLS_SECRET_NAME }} "
  ignore_errors: true
  register: get_tls_secret
  when: TLS_ENABLE is defined and TLS_ENABLE|bool

- name: COMMAND; create TLS secret for NGINX
  ansible.builtin.command: "kubectl create secret tls {{ TLS_SECRET_NAME }} --cert={{ TLS_SECRET_CERT }} --key={{ TLS_SECRET_KEY }}"
  when: TLS_ENABLE is defined and TLS_ENABLE|bool and get_tls_secret.rc != 0

# - name: IMPORT_TASKS; deployment_service.yaml
#   ansible.builtin.import_tasks: deployment_service.yaml

- name: IMPORT_TASKS; deployment_service.yaml
  ansible.builtin.import_tasks: generate_secrets.yaml

- name: IMPORT_TASKS; deployment_service.yaml
  ansible.builtin.import_tasks: cacao_helm.yaml

- name: COMMAND; wait until the NATS pod is in ready state; for now, just check nats-0 pod from the stateful set
  ansible.builtin.command: "kubectl wait pod -l statefulset.kubernetes.io/pod-name=nats-0 --for=condition=Ready --timeout={{ CACAO_POD_WAIT_MINUTES }}m"
  changed_when: false

# EJS - for now, this will be skaffold run
# - name: SHELL; create cacao resources
#   ansible.builtin.shell: "kubectl apply -f {{ CACAO_DEPLOY_DIR }}/{{ item }}"
#   register: result
#   changed_when: "'configured' in result.stdout or 'created' in result.stdout"
#   loop: "{{ CACAO_RESOURCE_FILES }}"
#   when: SKAFFOLD is defined and SKAFFOLD == false

- name: COMMAND; skaffold delete if SKAFFOLD_FORCE_DELETE is true
  ansible.builtin.command: skaffold delete
  args:
    chdir: "{{ CACAO_SRC_DIR }}"
  when: SKAFFOLD is defined and SKAFFOLD | bool and not IS_DEV_ENVIRONMENT and SKAFFOLD_FORCE_DELETE is defined and SKAFFOLD_FORCE_DELETE | bool

- name: COMMAND; run skaffold run for now
  ansible.builtin.command: skaffold run --build-concurrency=0
  args:
    chdir: "{{ CACAO_SRC_DIR }}"
  when: SKAFFOLD is defined and SKAFFOLD | bool and not IS_DEV_ENVIRONMENT

- name: COMMAND; obtain the SVC_ISTIO_INGRESS_IP output
  ansible.builtin.command: kubectl get ingress cacao-api-ingress -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
  changed_when: false
  register: nginx_ip

- name: LINEINFILE; add hosts entry for local deployment
  ansible.builtin.lineinfile:
    dest: /etc/hosts
    regexp: '.*{{ API_DOMAIN }}$'
    line: "{{ nginx_ip.stdout }} {{ API_DOMAIN }}"
    state: present
  when: IS_DEV_ENVIRONMENT
  become: true
