{{- if .Values.MONGODB_CONTAINER_ENABLED }}
mongodb is launched in container.
kubectl get service cacao-mongo
kubectl get deployment cacao-mongo

A k8s job is created in pre-seed test users.
kubectl get job mongodb-add-test-user-{{ .Release.Name }}

Following users are injected:
    {{- range $user := .Values.KEYCLOAK_USERS }}
        {{- $user.username }}
    {{- end }}           
{{- end }}