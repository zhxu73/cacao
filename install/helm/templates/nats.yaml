{{- if .Values.NATS_NUM_REPLICAS }}
  {{- if gt 0 (.Values.NATS_NUM_REPLICAS | int) }}
  {{ fail "NATS_NUM_REPLICAS must be postive" }}
  {{- end }}
  {{- if ne (mod .Values.NATS_NUM_REPLICAS 2) 1 }}
  {{ fail "NATS_NUM_REPLICAS must be odd number" }}
  {{- end }}
  {{- if gt (int .Values.NATS_NUM_REPLICAS) 1 }}
    {{- if not .Values.NATS_CLUSTER_PASSWORD }}
      {{ fail "NATS_CLUSTER_PASSWORD must be defined when replicas > 1" }}
    {{- end }}
  {{- end }}
{{- else }}
  {{ fail "NATS_NUM_REPLICAS must be defined" }}
{{- end }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    service: nats
    app: nats
  name: nats
spec:
  # create stable network ID with headless service https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#stable-network-id
  serviceName: nats-headless
  replicas: {{ .Values.NATS_NUM_REPLICAS }}
  selector:
    matchLabels:
      service: nats
  template:
    metadata:
      labels:
        service: nats
        app: nats
    spec:
      terminationGracePeriodSeconds: 30
      containers:
        - image: nats:2
          name: nats
          args:
            - "-js"
            - "-m"
            - "8222"
            - "-c"
            - "/etc/nats-config/nats.conf"
          ports:
            - containerPort: 4222
              name: "client"
            - containerPort: 6222
              name: "cluster"
            - containerPort: 8222
              name: "monitor"
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: CLUSTER_ADVERTISE
              value: $(POD_NAME).nats
{{- if gt (int .Values.NATS_NUM_REPLICAS) 1 }}
            - name: CLUSTER_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: nats-secrets
                  key: NATS_CLUSTER_PASSWORD
{{- end }}
          volumeMounts:
            - name: config-volume
              mountPath: /etc/nats-config
{{ if and .Values.LOCAL_PATH_PROVISIONER_ENABLE (and .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE (and (eq .Values.LOCAL_PATH_PROVISIONER_ENABLE true) (eq .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE true))) }}
            - name: nats-local-path
              mountPath: /data/jetstream
{{- end }}
          #resources:
          #  requests:
          #    cpu: 0
          livenessProbe:
            httpGet:
              path: /
              port: 8222
            initialDelaySeconds: 10
            timeoutSeconds: 5
        - name: metrics
          image: natsio/prometheus-nats-exporter:0.13.0
          args:
            - -connz
            - -routez
            - -subz
            - -varz
            - -channelz
            - -serverz
            - http://localhost:8222
          ports:
            - containerPort: 7777
              name: metrics
      volumes:
        - name: config-volume
          configMap:
            name: nats-config
{{ if and .Values.LOCAL_PATH_PROVISIONER_ENABLE (and .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE (and (eq .Values.LOCAL_PATH_PROVISIONER_ENABLE true) (eq .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE true))) }}
  volumeClaimTemplates:
    - metadata:
        name: nats-local-path
      spec:
        accessModes:
          - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: {{ .Values.LOCAL_PATH_PROVISIONER_NATS_SIZE }}
{{- end }}
---
apiVersion: v1
kind: Service
metadata:
  labels:
    service: nats
    app: nats
  name: nats
spec:
  ports:
    - name: "tcp"
      port: 4222
    - name: "cluster"
      port: 6222
    - name: "monitor"
      port: 8222
    - name: "metrics"
      port: 7777
  selector:
    service: nats
status:
  loadBalancer: { }
---
apiVersion: v1
kind: Service
metadata:
  name: nats-headless
spec:
  clusterIP: None
  clusterIPs:
    - None
  internalTrafficPolicy: Cluster
  ports:
    - appProtocol: tcp
      name: nats
      port: 4222
      protocol: TCP
      targetPort: nats
    - appProtocol: tcp
      name: cluster
      port: 6222
      protocol: TCP
      targetPort: cluster
    - appProtocol: http
      name: monitor
      port: 8222
      protocol: TCP
      targetPort: monitor
  publishNotReadyAddresses: true
  selector:
    service: nats
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: v1
kind: Secret
metadata:
  name: nats-secrets
stringData:
{{- if gt (int .Values.NATS_NUM_REPLICAS) 1 }}
  NATS_CLUSTER_PASSWORD: {{ .Values.NATS_CLUSTER_PASSWORD | quote }}
{{- end }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: nats-config
data:
  nats.conf: |
    server_name: $POD_NAME
    # http port for monitoring
    http_port: 8222

    jetstream {
        store_dir: /data/jetstream
        max_mem: 1G
        max_file: 100G
    }
{{- if gt (int .Values.NATS_NUM_REPLICAS) 1 }}
    cluster {
      name: cacao-cluster

      # port for inbound route connections from other server
      port: 6222
      routes [
{{- range $i := until (int .Values.NATS_NUM_REPLICAS) }}
        nats://route_user:{{ $.Values.NATS_CLUSTER_PASSWORD }}@nats-{{ $i }}.nats-headless:6222
{{- end }}
      ]
      cluster_advertise: $CLUSTER_ADVERTISE
      connect_retries: 10

      # Authorization for route connections
      authorization {
        user: route_user
        password: {{ .Values.NATS_CLUSTER_PASSWORD | quote }}
        timeout: 0.5
      }
    }
{{- end }}
{{- if .Values.NATS_TOKEN }}
    authorization {
      token: {{ .Values.NATS_TOKEN | quote }}
    }
{{- end }}

#
# STAN (NATS Streaming) as separate service
#
---
apiVersion: v1
kind: Service
metadata:
  labels:
    service: stan
    app: stan
  name: stan
spec:
  ports:
    - name: "tcp"
      port: 4222
    - name: "cluster"
      port: 6222
    - name: "monitor"
      port: 8222
    - name: "metrics"
      port: 7777
  selector:
    service: stan
status:
  loadBalancer: { }
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: stan-config
data:
  stan.conf: |
    http: 8222
    streaming {
      id: "cacao-cluster"
      store: file
      dir: /data/stan/store
      ft_group_name: "cacao-cluster"
      file_options {
          buffer_size: 32mb
          sync_on_flush: false
          slice_max_bytes: 512mb
          parallel_recovery: 64
      }
      store_limits {
          max_channels: 10
          max_msgs: 0
          max_bytes: 256gb
          max_age: 2160h
          max_subs: 128
      }
      # use external nats instead of the embedded one
      nats_server_url: "nats://nats:4222"
    }
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    service: stan
    app: stan
  name: stan
spec:
  serviceName: stan
  replicas: 1
  selector:
    matchLabels:
      service: stan
  template:
    metadata:
      labels:
        service: stan
        app: stan
    spec:
      terminationGracePeriodSeconds: 30
      containers:
        - image: nats-streaming
          name: stan
          args:
            - "-sc"
            - "/etc/stan-config/stan.conf"
          ports:
            - containerPort: 4222
              name: "client"
            - containerPort: 6222
              name: "cluster"
            - containerPort: 8222
              name: "monitor"
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: CLUSTER_ADVERTISE
              value: $(POD_NAME).nats
          volumeMounts:
            - name: config-volume
              mountPath: /etc/stan-config
{{ if and .Values.LOCAL_PATH_PROVISIONER_ENABLE (and .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE (and (eq .Values.LOCAL_PATH_PROVISIONER_ENABLE true) (eq .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE true))) }}
            - name: stan-volume
              mountPath: /data/stan
{{ end }}
          resources:
            requests:
              cpu: 200m
          livenessProbe:
            httpGet:
              path: /
              port: 8222
            initialDelaySeconds: 10
            timeoutSeconds: 5
        - name: metrics
          image: synadia/prometheus-nats-exporter:0.5.0
          args:
            - -connz
            - -routez
            - -subz
            - -varz
            - -channelz
            - -serverz
            - http://localhost:8222
          ports:
            - containerPort: 7777
              name: metrics
      volumes:
        - name: config-volume
          configMap:
            name: stan-config
{{ if and .Values.LOCAL_PATH_PROVISIONER_ENABLE (and .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE (and (eq .Values.LOCAL_PATH_PROVISIONER_ENABLE true) (eq .Values.LOCAL_PATH_PROVISIONER_NATS_ENABLE true))) }}
        - name: stan-volume
          persistentVolumeClaim:
            claimName: stan-local-path-pvc
{{ end }}

