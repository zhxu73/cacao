# cacao role breakdown
This is a break-down/summary of cacao ansible role

# main.yml - cacao role
- create dir
    - config dir
    - deploy dir
    - wiretap log dir
    - local path prov config dir
- set env_prefix
- change context to service cluster
- j2 template
    - cacao-logging-ns.yaml
    - mongodb-configmap.yaml
    - nats.yaml
    - vault-configmap.yaml
    - ion-configmap.yaml
    - cacao-configmap.yaml
    - globus-configmap.yaml
- import_tasks svc_k8s.yml

# svc_k8s.yml
- change context to service cluster
- helm check nginx ingress
- helm install nginx ingress
- kubectl wait nginx ingress
- nginx-ingress.yml.j2
    - j2 template
    - apply
- check tls secret exists
- create tls secret from files(cert & key file)
- import_tasks svc_k8s_mongodb.yml
- import_tasks svc_k8s_vault.yml
- import_tasks redis.yml
- import_tasks deployment_service.yml
- import_tasks svc_local_path_pvs.yml
- apply
    - everything in cacao config dir
- import_tasks keycloak.yml
- kubectl wait nats pod
- import_tasks guac-lite.yml
- skaffold delete
- skaffold run
- get nginx ingress ip
- add nginx ingress ip to /etc/hosts for local dev

# svc_k8s_mongodb.yml
- template & apply mongodb.yaml
- kubectl wait mongodb deployment
- add keycloak user if dev env
    - template mongodb js script
    - run js script with mongosh
        - k3s & k3d are different

# svc_k8s_vault.yml
- template & apply vault-postgres.yaml
- kubectl wait for vault postgres
- template & apply vault.yaml
- kubectl wait for vault

# redis.yml
- template & apply redis.yaml

# deployment_service.yml
- generate AWM key if not exists
- create k8s secret for AWM key

# svc_local_path_pvs.yml
- template & apply nats-local-path.yaml

# keycloak.yml
- ingress setup if local dev
    - context service cluster
    - get nginx ingress ip
    - get nginx ingress port
    - set NGINX_INGRESS_IP & NGINX_INGRESS_PORT
- template & apply keycloak.yaml
- kubectl wait keycloak if local keycloak pod
- keycloak_users.yaml for every KEYCLOAK_USERS if local keycloak

# keycloak_users.yaml
- get bearer token (login as `admin`)
- use bear token to:
    - create user
    - assign `cacao_admin` role to user

# guac-lite.yml
- template & apply guac-lite-pvc.yaml if local provisoner
- templaet & apply guac-lite.yaml

