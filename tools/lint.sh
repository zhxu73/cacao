#!/bin/bash

#Exit upon failure
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if ! command -v golint &> /dev/null ; then
    go get golang.org/x/lint/golint
    go install golang.org/x/lint/golint
fi

if ! command -v ineffassign &> /dev/null ; then
    go get github.com/gordonklaus/ineffassign
    go install github.com/gordonklaus/ineffassign
fi

if ! command -v misspell &> /dev/null ; then
    go get github.com/client9/misspell
    go install github.com/client9/misspell/cmd/misspell
fi


for SERVICE in $(ls "$SCRIPT_DIR/.." -1d -- */ | grep -Ev "(docs|tools|base|install|internal|kubernetes-manifests)")
do
    SERVICE_DIR="$SCRIPT_DIR/../$SERVICE"
    for dir in $(go list $SERVICE_DIR/... | grep -v argotypes); do golint $dir; done | tee /tmp/output.txt
    test $(cat /tmp/output.txt | wc -l) -eq 0
    ineffassign $SERVICE_DIR
    misspell -error $SERVICE_DIR
done
