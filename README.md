# cacao
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/cyverse/cacao?style=flat-square)](https://goreportcard.com/report/gitlab.com/cyverse/cacao)
[![Pipeline](https://gitlab.com/cyverse/cacao/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/cyverse/cacao/commits/master)
[![coverage](https://gitlab.com/cyverse/cacao/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/cyverse/cacao/commits/master)

CACAO(Cloud Automation & Continuous Analysis Orchestration) is a project enabling cloud automation & continuous analysis orchestration on multi-cloud.
It allows user to import templates defined in various templating language(e.g. terraform, ansible, argo workflow) from any git hosting solution (e.g. Github, Gitlab);
and deploy it to a cloud provider (e.g. OpenStack, AWS, K8S).

### Table of Contents
[[_TOC_]]


### [Installation](install/README.md)
Instructions for setting up CACAO using the provided deploy.sh script.


### [Developers](docs/getting_started_developers.md)
This document outlines the process for getting started as a CACAO developer, including local development setup and basic tasks for using CACAO.


### [Users](docs/getting_started_users.md)
This document has some basic information for new users, such as installing the CLI, basic actions to use CACAO with the CLI, and a link to WorkflowDefinition file documentation. Then there is also some information about building and running Workflows.


## [Developer Documentation](docs/developers/README.md)
This directory contains developer documentation for each service in addition to some other relevant information.

## License
See [LICENSE.txt](./LICENSE.txt)
