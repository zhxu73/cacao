package adapters

import (
	"time"

	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
)

// StanSink is a NATS Streaming event sink to publish events into.
type StanSink struct {
	conf     types.StanConfig
	sc       stan.Conn
	clientID string
	connLost chan struct{}
}

// NewStanSink creates new StanSink
func NewStanSink(clientID string) *StanSink {
	return &StanSink{clientID: clientID + xid.New().String(), connLost: make(chan struct{})}
}

// InitSink initialize StanSink, establish the connection.
func (sink *StanSink) InitSink(conf types.StanConfig) error {
	sink.conf = conf
	return sink.initSink()
}

func (sink *StanSink) initSink() error {
	sc, err := stanConnect(sink.conf, sink.clientID, sink.connLostHandler)
	if err != nil {
		return err
	}
	sink.sc = sc
	return nil
}

// Publish publish an event to the StanSink
func (sink StanSink) Publish(event types.Event) error {
	ce := eventToCloudEvent(event)
	msg, err := ce.MarshalJSON()
	if err != nil {
		return err
	}

	return retryConstBackoff(5, time.Second*3, func() error {
		return sink.sc.Publish(sink.conf.Subject, msg)
	})
}

func (sink StanSink) connLostHandler(conn stan.Conn, err error) {
	log.WithFields(log.Fields{
		"url": sink.conf.URL,
		"err": err.Error(),
	}).Errorf("STAN sink connection lost")

	err = conn.Close()
	if err != nil {
		log.WithError(err).Error("error while closing STAN connection")
	}

	sink.connLost <- struct{}{}
	close(sink.connLost)
}

// ConnLost returns the connection lost notifier channel
func (sink StanSink) ConnLost() <-chan struct{} {
	return sink.connLost
}

// Close performs a cleanup. NATS Streaming connection will be closed.
func (sink StanSink) Close() {
	if sink.sc != nil {
		err := sink.sc.Close()
		if err != nil {
			log.WithError(err).Error("error while closing STAN connection")
		}
	}
}

// StanSrc is a NATS Streaming event source to receive events from.
type StanSrc struct {
	// This is just to distinguish between multiple StanSrc
	name      string
	conf      types.StanConfig
	clientID  string
	sc        stan.Conn
	sub       stan.Subscription
	eventChan chan<- types.Event
}

// NewStanSrc creates a new StanSrc
func NewStanSrc(name, clientID string) *StanSrc {
	return &StanSrc{name: name, clientID: clientID}
}

// InitSrc initialize the StanSrc, establish the connection.
func (src *StanSrc) InitSrc(conf types.StanConfig) error {
	src.conf = conf
	return src.initSrc()
}

func (src *StanSrc) initSrc() error {
	sc, err := stanConnect(src.conf, src.clientID, src.connLostHandler)
	if err != nil {
		return err
	}
	log.WithField("event-src", src.name).Info("connected")
	src.sc = sc
	return nil
}

// perform cleanup, and notfiy the connection lost by closing eventChan
func (src *StanSrc) connLostHandler(conn stan.Conn, err error) {
	logger := log.WithField("event-src", src.name)
	logger.WithFields(log.Fields{
		"url": src.conf.URL,
		"err": err.Error(),
	}).Errorf("STAN src connection lost")

	src.Close()
	close(src.eventChan)
}

// InitChannel sets the channel that StanSrc will publish events into.
func (src *StanSrc) InitChannel(chann chan<- types.Event) {
	src.eventChan = chann
}

// Start starts the NATS Streaming subscription, and start pushing events
// received from NATS Streaming into the go channel.
func (src StanSrc) Start() error {
	logger := log.WithFields(log.Fields{
		"package":   "adapter",
		"function":  "StanSrc.Start",
		"event-src": src.name,
		"subject":   src.conf.Subject,
		"queue":     src.conf.QueueGroup,
	})

	if src.conf.Subject == "" {
		logger.Fatal("cannot subscribe with no receive subject")
	}
	if src.eventChan == nil {
		logger.Fatal("eventChan is nil")
	}

	callback := func(msg *stan.Msg) {
		msg.Ack()

		ce, err := getRequestFromCloudEvent(msg.Data)
		if err != nil {
			logger.WithError(err).Error("unable to get event from CloudEvent")
			return
		}
		logger.WithFields(log.Fields{"event": string(ce.Data())}).Trace("received message")

		event := cloudeventToEvent(*ce)
		logger.WithFields(log.Fields{"event": event, "event_type": event.EventType()}).Trace("event converted")
		src.eventChan <- event
	}

	subOptions := getStanOptions(src.conf.Subject)
	sub, err := src.sc.QueueSubscribe(src.conf.Subject, src.conf.QueueGroup, callback, subOptions...)
	if err != nil {
		return err
	}
	src.sub = sub
	log.WithField("event-src", src.name).Info("Start NATS Streaming subscriber")
	return nil
}

// Close performs a cleanup. The NATS Streaming connection will be closed, and
// the NATS Streaming subscription will be closed but not unsubscribed.
func (src StanSrc) Close() {
	var err error

	if src.sub != nil {
		// close but not unsubscribe
		err = src.sub.Close()
		if err != nil {
			log.WithError(err).Error("error while closing STAN subscription")
		}
	}

	if src.sc != nil {
		err := src.sc.Close()
		if err != nil {
			log.WithError(err).Error("error while closing STAN connection")
		}
	}
}
