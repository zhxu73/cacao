package adapters

import (
	"encoding/json"
	"reflect"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
)

// StanEvent implements types.Event
type StanEvent cloudevents.Event

// EventType returns the type of the event, implements types.EventType
func (event StanEvent) EventType() types.EventType {
	return types.EventType(cloudevents.Event(event).Type())
}

// establish a NATS Streaming connection
func stanConnect(conf types.StanConfig, clientID string, connLostHandler func(stan.Conn, error)) (stan.Conn, error) {
	var sc stan.Conn
	err := retry(10, 5*time.Second, func() error {
		var err error
		sc, err = stan.Connect(
			conf.ClusterID,
			clientID,
			stan.NatsURL(conf.URL),
			stan.ConnectWait(5*time.Second),
			stan.SetConnectionLostHandler(connLostHandler),
		)
		return err
	})

	if err != nil {
		return nil, err
	}

	return sc, nil
}

func getStanOptions(subject string) []stan.SubscriptionOption {
	aw, _ := time.ParseDuration("60s")
	return []stan.SubscriptionOption{
		stan.DurableName("Durable" + subject),
		stan.MaxInflight(25),
		stan.SetManualAckMode(),
		stan.AckWait(aw),
	}
}

func eventToCloudEvent(event types.Event) cloudevents.Event {
	se, ok := event.(StanEvent)
	if !ok {
		log.Errorf("cannot convert to stanEvent, %s", reflect.TypeOf(event))
	}
	return cloudevents.Event(se)
}

func cloudeventToEvent(ce cloudevents.Event) types.Event {
	se := StanEvent(ce)
	return types.Event(se)
}

// retry with expoential backoff (backoff X2 each time)
func retry(attempts int, sleep time.Duration, fn func() error) error {
	if err := fn(); err != nil {
		if attempts--; attempts > 0 {
			time.Sleep(sleep)
			return retry(attempts, 2*sleep, fn)
		}
		return err
	}
	return nil
}

// retry with constant backoff
func retryConstBackoff(attempts int, sleep time.Duration, fn func() error) error {
	if err := fn(); err != nil {
		if attempts--; attempts > 0 {
			time.Sleep(sleep)
			return retry(attempts, sleep, fn)
		}
		return err
	}
	return nil
}

// get cloudevents from msg payload
func getRequestFromCloudEvent(msgData []byte) (*cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msgData, &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
