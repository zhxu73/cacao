package adapters

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
)

func Test_retry(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		err := retry(10, time.Millisecond*100, func() error { return nil })
		if err != nil {
			t.Errorf("retry() error = %v, expect = %v", err, nil)
		}
	})
	t.Run("error", func(t *testing.T) {
		attempts := 5
		sleep := time.Millisecond * 100
		expectErr := fmt.Errorf("error")

		timeStart := time.Now()
		err := retry(attempts, sleep, func() error { return expectErr })
		timeStop := time.Now()
		if err == nil {
			t.Errorf("retry() error = %v, expect = %v", err, expectErr)
		}
		expectedMinStop := timeStart

		intervals := 1
		for i := 0; i < attempts; i++ {
			for j := 0; j < intervals; j++ {
				expectedMinStop = timeStart.Add(sleep)
			}
			intervals *= 2
		}
		if expectedMinStop.After(timeStop) {
			t.Errorf("retry() didnt sleep for enough time, expect = %v, has = %v", expectedMinStop.Sub(timeStart), timeStop.Sub(timeStart))
		}
	})
	t.Run("error-check-attempts", func(t *testing.T) {
		attempts := 10
		sleep := time.Millisecond * 10
		expectErr := fmt.Errorf("error")
		runCounter := 0

		err := retry(attempts, sleep, func() error { runCounter++; return fmt.Errorf("error") })
		if err == nil {
			t.Errorf("retry() error = %v, expect = %v", err, expectErr)
		}
		if runCounter != attempts {
			t.Errorf("retry() different number of attempts, expect = %v, has = %v", attempts, runCounter)
		}
	})
}

func Test_cloudeventToEvent(t *testing.T) {
	ce := cloudevents.NewEvent("1.0")
	event := cloudeventToEvent(ce)
	ce2 := eventToCloudEvent(event)
	if !reflect.DeepEqual(ce, ce2) {
		t.Errorf("cloudeventToEvent() = %v, want %v", ce, ce2)
	}
}
