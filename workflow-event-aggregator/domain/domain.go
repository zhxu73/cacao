package domain

import (
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/ports"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
)

// Domain is the entrypoint object for the service.
type Domain struct {
	sinkFactory   ports.EventSinkFactory
	srcFactory    ports.EventSrcFactory
	eventChan     chan types.Event
	receivers     map[string]EventReceiver
	sender        EventSender
	failureNotify chan string
	conf          types.Config
}

// NewDomain creates a new domain
func NewDomain(sinkFac ports.EventSinkFactory, srcFac ports.EventSrcFactory) Domain {
	domain := Domain{
		sinkFactory:   sinkFac,
		srcFactory:    srcFac,
		eventChan:     make(chan types.Event, 10),
		receivers:     make(map[string]EventReceiver),
		failureNotify: make(chan string),
	}

	return domain
}

// Init initialize Domain with configuration
func (domain *Domain) Init(conf types.Config) error {
	domain.conf = conf
	domain.failureNotify = make(chan string, len(domain.conf.Srcs))

	domain.sender = NewEventSender(domain.sinkFactory, domain.eventChan)
	err := domain.sender.Init(conf.Sink)
	if err != nil {
		return err
	}

	log.WithField("receiver_count", len(conf.Srcs)).Info()
	for name, srcConf := range domain.conf.Srcs {
		receiver := NewEventReceiver(name, domain.srcFactory, domain.eventChan, domain.failureNotify)
		err := receiver.Init(srcConf)
		if err != nil {
			return err
		}
		domain.receivers[name] = receiver
		log.Infof("initialized receiver '%s'", name)
	}
	return nil
}

// Start the domain service
func (domain *Domain) Start() {

	var wg sync.WaitGroup

	for _, receiver := range domain.receivers {
		go receiver.Start()

		wg.Add(1)
		go NewReceiverMonitor(domain).Monitor(&wg)
	}

	wg.Add(1)
	go domain.sender.Start()

	go domain.sender.Monitor()

	wg.Wait()
}
