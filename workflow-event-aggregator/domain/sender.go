package domain

import (
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/ports"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
)

// EventSender reads events from a channel and sends to EventSink.
type EventSender struct {
	// source to receive events from
	srcChan   <-chan types.Event
	eventSink ports.EventSink
}

// NewEventSender creates new EventSender
func NewEventSender(factory ports.EventSinkFactory, src <-chan types.Event) EventSender {
	sender := EventSender{
		srcChan:   src,
		eventSink: factory(),
	}
	return sender
}

// Init initialize EventSender, EventSink is initialized
func (sender *EventSender) Init(conf types.StanConfig) error {
	return sender.eventSink.InitSink(conf)
}

// Start starts receiving from channel.
func (sender *EventSender) Start() {
	var wg sync.WaitGroup

	wg.Add(1)
	go sender.sendEvents(&wg)
	wg.Wait()

}

func (sender *EventSender) sendEvents(wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "EventSender.sendEvents",
	})
	logger.Info("starting EventSender.sendEvents()")
	defer logger.Info("EventSender.sendEvents() exiting")
	defer wg.Done()

	for event := range sender.srcChan {
		err := sender.eventSink.Publish(event)
		if err != nil {
			logger.WithFields(log.Fields{
				"eventType": event.EventType(),
				"error":     err.Error(),
			}).Error("failed to send event")
		} else {
			logger.WithFields(log.Fields{"eventType": event.EventType()}).Info("event sent")
		}
	}
	return nil
}

// Monitor monitors the connection of sender, will terminate if connection is lost
func (sender *EventSender) Monitor() {
	for range sender.eventSink.ConnLost() {
		log.Fatal("sender connection lost")
	}
}
