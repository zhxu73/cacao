package ports

import (
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
)

// EventSrc is an adapter interface for incoming events
type EventSrc interface {
	InitSrc(types.StanConfig) error
	InitChannel(chan<- types.Event)
	Start() error
	Close()
}

// EventSrcFactory is a function that creates EventSrc.
// The name parameter is just for identification purpose.
type EventSrcFactory func(name string) EventSrc

// EventSink is an adapter interface for outgoing events
type EventSink interface {
	InitSink(types.StanConfig) error
	Publish(types.Event) error
	// return a channel that will be notified when connection is lost
	ConnLost() <-chan struct{}
	Close()
}

// EventSinkFactory is a function that creates EventSink.
// The name parameter is just for identification purpose.
type EventSinkFactory func() EventSink
