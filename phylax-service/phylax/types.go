package phylax

import (
	"fmt"
	"time"

	"gitlab.com/cyverse/cacao/common"

	"go.mongodb.org/mongo-driver/bson"
	core "k8s.io/api/core/v1"
)

// Database abstraction layer used throughout the package
var db *DB

// DB struct is a wrapper around the CRUD adapter for Phylax related types.
type DB struct {
	adapter common.CRUD
}

// Save ...
func (d DB) Save(document common.CacaoBase) error {
	return d.adapter.Replace(document.GetCollection(), document)
}

// Delete ...
func (d DB) Delete(document common.CacaoBase) error {
	return d.adapter.Delete(document.GetCollection(), document.GetID())
}

// GetRunner ...
func (d DB) GetRunner(ID, owner string) (*Runner, error) {
	var runner Runner
	err := db.getDocument(&runner, ID, owner)
	if err != nil {
		return &Runner{}, err
	}
	return &runner, nil
}

// GetBuild ...
func (d DB) GetBuild(ID, owner string) (*Build, error) {
	var build Build
	err := db.getDocument(&build, ID, owner)
	if err != nil {
		return &Build{}, err
	}
	return &build, nil
}

// GetRunnersForUser ...
func (d DB) GetRunnersForUser(owner string) (runners []Runner, err error) {
	run := new(Runner)
	documents, err := d.adapter.ReadForUser(run.GetCollection(), owner)
	if err != nil {
		return
	}

	for _, doc := range documents {
		var bsonBytes []byte
		bsonBytes, err = bson.Marshal(doc)
		if err != nil {
			return
		}
		var runner Runner
		err = bson.Unmarshal(bsonBytes, &runner)
		if err != nil {
			return
		}
		runners = append(runners, runner)
	}
	return
}

// GetBuildsForUser ...
func (d DB) GetBuildsForUser(owner string) (builds []Build, err error) {
	build := new(Build)
	documents, err := d.adapter.ReadForUser(build.GetCollection(), owner)
	if err != nil {
		return
	}
	for _, doc := range documents {
		var bsonBytes []byte
		bsonBytes, err = bson.Marshal(doc)
		if err != nil {
			return
		}
		var build Build
		err = bson.Unmarshal(bsonBytes, &build)
		if err != nil {
			return
		}
		builds = append(builds, build)
	}
	return
}

func (d DB) getDocument(document common.CacaoBase, ID, owner string) error {
	data, err := d.adapter.Read(document.GetCollection(), ID)

	if err != nil {
		return fmt.Errorf("Error reading from database: %v", err)
	}

	err = bson.Unmarshal(data, document)

	if document.GetOwner() != owner {
		return fmt.Errorf("User not authorized for this Runner")
	}

	if err != nil {
		return fmt.Errorf("Error unmarshalling BSON response: %v", err)
	}

	return nil
}

// InitMongoDB will initialize the connection to the database
func InitMongoDB(name, address string) error {
	var err error
	adapter, err := common.GetMongoDBAdapter(name, address)
	if err == nil {
		db = &DB{
			adapter: adapter,
		}
	}
	return err
}

// MonitoredResource comprises the fields that are common among resources that
// Phylax monitors (e.g. Runners, Builds)
type MonitoredResource struct {
	ID                   string         `bson:"_id" json:"id"`
	Owner                string         `bson:"owner" json:"owner"`
	StartDate            time.Time      `bson:"startdate" json:"startdate"`
	EndDate              time.Time      `bson:"enddate" json:"enddate"`
	WorkflowDefinitionID string         `bson:"workflowdefinitionid" json:"workflowdefinitionid"`
	ResourceName         string         `bson:"resourcename" json:"resourcename"`
	Cluster              common.Cluster `json:"cluster"`
	Namespace            string         `bson:"namespace" json:"namespace"`
}

// Populate accepts a phylax.Request and uses it to populate the fields of a MonitoredResource
func (m *MonitoredResource) Populate(request Request) error {
	m.WorkflowDefinitionID = request.GetWorkflowID()
	m.StartDate = time.Now()
	cluster, ok := request.GetUser().Clusters[request.GetClusterID()]
	if !ok {
		return fmt.Errorf("Error getting cluster config '%s'", request.GetClusterID())
	}
	m.Namespace = request.Namespace
	m.Cluster = cluster
	return nil
}

// Runner contains the necessary information to link objects in K8s to the WorkflowDefinition they
// originate from
type Runner struct {
	MonitoredResource `bson:",inline"`
	TCPPorts          []core.ServicePort `bson:"tcp_ports" json:"tcp_ports"`
	HTTPPorts         []core.ServicePort `bson:"http_ports" json:"http_ports"`
	AuthDisabled      bool               `bson:"auth_disabled" json:"auth_disabled"`
}

// GetCollection returns the MongoDB collection in which Runners should be stored.
func (r Runner) GetCollection() string {
	return fmt.Sprintf("%T", r)
}

// GetID ...
func (r Runner) GetID() string {
	return r.ID
}

// GetOwner ...
func (r Runner) GetOwner() string {
	return r.Owner
}

// GetRedacted will return a new Runner with the cluster config removed
func (r *Runner) GetRedacted() Runner {
	redactedRunner := *r
	redactedRunner.Cluster = redactedRunner.Cluster.GetRedacted()
	return redactedRunner
}

// NewRunner populates a Runner from a phylax.Request and returns a pointer to it
// and an error if anything goes wrong
func NewRunner(request Request) (runner *Runner, err error) {
	runner = new(Runner)
	runner.ID = request.GetRunID()
	runner.Owner = request.GetUser().Username
	err = runner.Populate(request)
	if err != nil {
		return
	}
	runner.ResourceName = common.CreateResourceName(request.GetUser().Username, request.WorkflowDefinition.Name, request.GetRunID())
	runner.AuthDisabled = request.AuthDisabled
	// If new TCPPorts were defined in the run request, save them. Otherwise save WFD TCPPorts
	if len(request.TCPPorts) > 0 {
		runner.TCPPorts = request.TCPPorts
	} else {
		runner.TCPPorts = request.WorkflowDefinition.TCPPorts
	}
	runner.HTTPPorts = request.HTTPPorts
	return
}

// BuildConditionType is a string that represents the state of a Build.
type BuildConditionType string

// These are valid conditions of a build.
const (
	// BuildComplete means the build has completed successfully.
	BuildComplete BuildConditionType = "Complete"
	// BuildFailed means the build has failed.
	BuildFailed BuildConditionType = "Failed"
	// BuildPending means the build is still in progress.
	BuildPending BuildConditionType = "Pending"
)

// Build represents a request to build the necessary Docker image(s) for a WorkflowDefinition
type Build struct {
	Condition         BuildConditionType `bson:"condition" json:"condition"`
	MonitoredResource `bson:",inline"`
	RunAfterBuild     bool `json:"run"`
	// RunID should only be populated if RunAfterBuild is true
	RunID string `bson:"runid" json:"runid"`
}

// GetCollection returns the MongoDB collection in which Builds should be stored.
func (b Build) GetCollection() string {
	return fmt.Sprintf("%T", b)
}

// GetID ...
func (b Build) GetID() string {
	return b.ID
}

// GetOwner ...
func (b Build) GetOwner() string {
	return b.Owner
}

// NewBuild accepts a Request and uses it to populate a Build.  The Condition field
// is set to BuildPending.
func NewBuild(request Request) (build *Build, err error) {
	build = new(Build)
	build.ID = request.GetBuildID()
	build.Owner = request.GetUser().Username
	build.Condition = BuildPending
	build.RunAfterBuild = request.RunAfterBuild
	if build.RunAfterBuild {
		build.RunID = request.GetRunID()
	}
	err = build.Populate(request)
	build.ResourceName = common.CreateResourceName(request.GetUser().Username, request.WorkflowDefinition.Name, request.GetBuildID())
	return
}
