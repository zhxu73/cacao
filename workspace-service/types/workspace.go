package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// Workspace is a struct for storing workspace information
type Workspace struct {
	ID                common.ID `bson:"_id" json:"id,omitempty"`
	Owner             string    `bson:"owner" json:"owner,omitempty"`
	Name              string    `bson:"name" json:"name,omitempty"`
	Description       string    `bson:"description" json:"description,omitempty"`
	DefaultProviderID common.ID `bson:"default_provider_id" json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt         time.Time `bson:"updated_at" json:"updated_at,omitempty"`
}

// ConvertFromModel converts WorkspaceModel to Workspace
func ConvertFromModel(model cacao_common_service.WorkspaceModel) Workspace {
	workspace := Workspace{
		ID:                model.ID,
		Owner:             model.Owner,
		Name:              model.Name,
		Description:       model.Description,
		DefaultProviderID: model.DefaultProviderID,
		CreatedAt:         model.CreatedAt,
		UpdatedAt:         model.UpdatedAt,
	}

	return workspace
}

// ConvertToModel converts Workspace to WorkspaceModel
func ConvertToModel(session cacao_common_service.Session, workspace Workspace) cacao_common_service.WorkspaceModel {
	return cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		ID:                workspace.ID,
		Owner:             workspace.Owner,
		Name:              workspace.Name,
		Description:       workspace.Description,
		DefaultProviderID: workspace.DefaultProviderID,
		CreatedAt:         workspace.CreatedAt,
		UpdatedAt:         workspace.UpdatedAt,
	}
}
