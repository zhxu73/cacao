package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

const (
	// natsQueueGroup is the NATS Queue Group for this service
	natsQueueGroup = "workspace_queue_group"
	// natsWildcardSubject is the NATS subject to subscribe
	natsWildcardSubject = string(cacao_common_service.NatsSubjectWorkspace) + ".>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "workspace-service"

	// natsDurableName is the NATS Durable Name for this service
	natsDurableName = "workspace_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao-workspace"
	// DefaultWorkspaceMongoDBCollectionName is a default MongoDB Collection Name
	DefaultWorkspaceMongoDBCollectionName = "workspace"
)
