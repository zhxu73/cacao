package types

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
	"time"
)

// This is to ensure that Workspace contains the same data field as
// service.WorkspaceModel (data field meaning not service.Session). This makes
// sure that the JSON wire format does not change.
func TestWorkspaceConvert(t *testing.T) {
	// Workspace => service.WorkspaceModel => Workspace
	var ws1 = Workspace{
		ID:                "workspace-d3jng0598850n9abiklg",
		Owner:             "testuser123",
		Name:              "testuser456",
		Description:       "description",
		DefaultProviderID: "provider-d17e30598850n9abikl0",
		CreatedAt:         time.Now().Add(-time.Hour).UTC(),
		UpdatedAt:         time.Now().UTC(),
	}
	var ws2 service.WorkspaceModel
	var ws3 Workspace

	marshal, err := json.Marshal(ws1)
	if !assert.NoError(t, err) {
		return
	}
	err = json.Unmarshal(marshal, &ws2)
	if !assert.NoError(t, err) {
		return
	}
	marshal, err = json.Marshal(ws2)
	if !assert.NoError(t, err) {
		return
	}
	err = json.Unmarshal(marshal, &ws3)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, ws1, ws3)
}
