package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		log.WithError(err).Fatal("unable to connect to MongoDB")
	}
	mongoAdapter := &MongoAdapter{
		config: &config,
		store:  store,
	}

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.store)

	mongoAdapter.Close()
}

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"

	expectedResults := []types.Workspace{
		{
			ID:                "0001",
			Owner:             testUser,
			Name:              "test_workspace1",
			Description:       "test_description1",
			DefaultProviderID: "test_default_provider1",
			CreatedAt:         testTime,
			UpdatedAt:         testTime,
		},
		{
			ID:                "0002",
			Owner:             testUser,
			Name:              "test_workspace2",
			Description:       "test_description2",
			DefaultProviderID: "test_default_provider2",
			CreatedAt:         testTime,
			UpdatedAt:         testTime,
		},
	}
	err := mongoAdapter.MockList(testUser, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.List(context.TODO(), testUser)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	mongoAdapter.Close()
}

func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	expectedResult := types.Workspace{
		ID:                testWorkspaceID,
		Owner:             testUser,
		Name:              "test_workspace1",
		Description:       "test_description1",
		DefaultProviderID: "test_default_provider1",
		CreatedAt:         testTime,
		UpdatedAt:         testTime,
	}
	err := mongoAdapter.MockGet(testUser, testWorkspaceID, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.Get(context.TODO(), testUser, testWorkspaceID)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Close()
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	testWorkspace := types.Workspace{
		ID:                testWorkspaceID,
		Owner:             testUser,
		Name:              "test_workspace1",
		Description:       "test_description1",
		DefaultProviderID: "test_default_provider1",
		CreatedAt:         testTime,
		UpdatedAt:         testTime,
	}

	err := mongoAdapter.MockCreate(testWorkspace, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Create(context.TODO(), testWorkspace)
	assert.NoError(t, err)

	mongoAdapter.Close()
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	testExistingWorkspace := types.Workspace{
		ID:                testWorkspaceID,
		Owner:             testUser,
		Name:              "test_workspace1",
		Description:       "test_description1",
		DefaultProviderID: "test_default_provider1",
		CreatedAt:         testTime,
		UpdatedAt:         testTime,
	}

	testUpdateWorkspace := types.Workspace{
		ID:                testWorkspaceID,
		Owner:             testUser,
		Description:       "test_description2",
		DefaultProviderID: "test_default_provider2",
	}

	err := mongoAdapter.MockUpdate(testExistingWorkspace, testUpdateWorkspace, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Update(context.TODO(), testUpdateWorkspace, []string{"description", "default_provider_id"})
	assert.NoError(t, err)

	mongoAdapter.Close()
}

func TestMongoAdapterDelete(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	testWorkspace := types.Workspace{
		ID:                testWorkspaceID,
		Owner:             testUser,
		Name:              "test_workspace1",
		Description:       "test_description1",
		DefaultProviderID: "test_default_provider1",
		CreatedAt:         testTime,
		UpdatedAt:         testTime,
	}

	err := mongoAdapter.MockDelete(testUser, testWorkspaceID, testWorkspace, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Delete(context.TODO(), testUser, testWorkspaceID)
	assert.NoError(t, err)

	mongoAdapter.Close()
}
