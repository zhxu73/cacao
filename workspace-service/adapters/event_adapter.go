package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
	"sync"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	handlers ports.IncomingEventHandlers
	// internal
	conn messaging2.EventConnection
}

var _ ports.IncomingEventPort = &EventAdapter{}

// NewEventAdapter creates a new EventAdapter from EventConnection
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{conn: conn}
}

// SetHandlers ...
func (adapter *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")
	return adapter.conn.Listen(ctx, map[cacao_common.EventType]messaging2.EventHandlerFunc{
		cacao_common_service.WorkspaceCreateRequestedEvent: handlerWrapper(adapter.handlers.Create),
		cacao_common_service.WorkspaceDeleteRequestedEvent: handlerWrapper(adapter.handlers.Delete),
		cacao_common_service.WorkspaceUpdateRequestedEvent: handlerWrapper(adapter.handlers.Update),
	}, wg)
}

func handlerWrapper[T any](handler func(context.Context, T, ports.OutgoingEventPort)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
			log.WithField("ceType", event.Type()).WithError(err).Error(errorMessage)
			return err
		}
		handler(ctx, request, eventOut{writer: writer})
		return nil
	}
}

/*
func (adapter *EventAdapter) handleWorkspaceCreateRequest(transactionID cacao_common.TransactionID, requestCe *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleWorkspaceCreateRequest",
	})

	var createRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(requestCe.Data(), &createRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	workspace := types.ConvertFromModel(createRequest)

	err = adapter.handlers.Create(createRequest.GetSessionActor(), createRequest.GetSessionEmulator(), workspace, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleWorkspaceDeleteRequest(transactionID cacao_common.TransactionID, requestCe *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleWorkspaceDeleteRequest",
	})

	var deleteRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(requestCe.Data(), &deleteRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	workspace := types.ConvertFromModel(deleteRequest)

	err = adapter.handlers.Delete(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), workspace, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleWorkspaceUpdateRequest(transactionID cacao_common.TransactionID, requestCe *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleWorkspaceUpdateRequest",
	})

	var updateRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(requestCe.Data(), &updateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	workspace := types.ConvertFromModel(updateRequest)

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "description", "default_provider_id")
	}

	err = adapter.handlers.Update(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), workspace, updateRequest.UpdateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}
*/

type eventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventPort = &eventOut{}

// WorkspaceCreatedEvent ...
func (e eventOut) WorkspaceCreatedEvent(model cacao_common_service.WorkspaceModel) {
	e.publish(model, cacao_common_service.WorkspaceCreatedEvent)
}

// WorkspaceCreateFailedEvent ...
func (e eventOut) WorkspaceCreateFailedEvent(model cacao_common_service.WorkspaceModel) {
	e.publish(model, cacao_common_service.WorkspaceCreateFailedEvent)
}

// WorkspaceUpdatedEvent ...
func (e eventOut) WorkspaceUpdatedEvent(model cacao_common_service.WorkspaceModel) {
	e.publish(model, cacao_common_service.WorkspaceUpdatedEvent)
}

// WorkspaceUpdateFailedEvent ...
func (e eventOut) WorkspaceUpdateFailedEvent(model cacao_common_service.WorkspaceModel) {
	e.publish(model, cacao_common_service.WorkspaceUpdateFailedEvent)
}

// WorkspaceDeletedEvent ...
func (e eventOut) WorkspaceDeletedEvent(model cacao_common_service.WorkspaceModel) {
	e.publish(model, cacao_common_service.WorkspaceDeletedEvent)
}

// WorkspaceDeleteFailedEvent ...
func (e eventOut) WorkspaceDeleteFailedEvent(model cacao_common_service.WorkspaceModel) {
	e.publish(model, cacao_common_service.WorkspaceDeleteFailedEvent)
}

func (e eventOut) publish(model interface{}, eventType cacao_common.EventType) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "eventOut.publish",
	})

	ce, err := messaging2.CreateCloudEventWithAutoSource(model, eventType)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", eventType)
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", eventType)
	}
}

// Created publishes cacao_common_service.WorkspaceCreatedEvent
func (adapter *EventAdapter) Created(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Created",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, workspace)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.WorkspaceCreatedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.WorkspaceCreatedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreatedEvent)
	}

	return nil
}

// CreateFailed publishes cacao_common_service.WorkspaceCreateFailedEvent
func (adapter *EventAdapter) CreateFailed(actor string, emulator string, workspace types.Workspace, creationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.CreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
	}

	if cerr, ok := creationError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(creationError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, workspace)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.WorkspaceCreateFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.WorkspaceCreateFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreateFailedEvent)
	}

	return nil
}

// Updated publishes cacao_common_service.WorkspaceUpdatedEvent.
func (adapter *EventAdapter) Updated(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Updated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, workspace)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.WorkspaceUpdatedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.WorkspaceUpdatedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdatedEvent)
	}

	return nil
}

// UpdateFailed publishes cacao_common_service.WorkspaceUpdateFailedEvent
func (adapter *EventAdapter) UpdateFailed(actor string, emulator string, workspace types.Workspace, updateError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
	}

	if cerr, ok := updateError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(updateError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, workspace)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.WorkspaceUpdateFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.WorkspaceUpdateFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdateFailedEvent)
	}

	return nil
}

// Deleted publishes cacao_common_service.WorkspaceDeletedEvent
func (adapter *EventAdapter) Deleted(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, workspace)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.WorkspaceDeletedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.WorkspaceDeletedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeletedEvent)
	}

	return nil
}

// DeleteFailed publishes cacao_common_service.WorkspaceDeleteFailedEvent
func (adapter *EventAdapter) DeleteFailed(actor string, emulator string, workspace types.Workspace, deletionError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
	}

	if cerr, ok := deletionError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(deletionError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, workspace)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.WorkspaceDeleteFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.WorkspaceDeleteFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeleteFailedEvent)
	}

	return nil
}
