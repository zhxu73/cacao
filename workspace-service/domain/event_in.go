package domain

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workspace-service/ports"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// EventHandlers implements IncomingEventPort
type EventHandlers struct {
	Storage  ports.PersistentStoragePort
	EventOut ports.OutgoingEventPort
	TimeSrc  func() time.Time
}

var _ ports.IncomingEventHandlers = &EventHandlers{}

// Create creates a workspace
func (h *EventHandlers) Create(ctx context.Context, request cacao_common_service.WorkspaceModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "EventHandlers.Create",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})
	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.WorkspaceCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		request.ID = cacao_common_service.NewWorkspaceID()
	} else if !request.ID.Validate() || request.ID.FullPrefix() != "workspace" {
		// if we are using ID supplied by client, then we need to validate it
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: ill-formed workspace id")
		sink.WorkspaceCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if len(request.Name) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace name is empty")
		logger.WithError(err).Error("bad request")
		sink.WorkspaceCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	now := h.TimeSrc()
	var workspace = types.Workspace{
		ID:                request.ID,
		Owner:             request.SessionActor,
		Name:              request.Name,
		Description:       request.Description,
		DefaultProviderID: request.DefaultProviderID,
		CreatedAt:         now,
		UpdatedAt:         now,
	}

	logger.Debugf("Creating workspace %s", workspace.ID.String())

	err := h.Storage.Create(ctx, workspace)
	if err != nil {
		logger.WithError(err).Error("fail to create workspace in storage")
		sink.WorkspaceCreateFailedEvent(h.creationErrorResponse(request, toCacaoError(err)))
		return
	}

	logger.Info("workspace created")
	sink.WorkspaceCreatedEvent(cacao_common_service.WorkspaceModel{
		Session:          cacao_common_service.CopySessionActors(request.Session),
		ID:               workspace.ID,
		Owner:            workspace.Owner,
		Name:             workspace.Name,
		CreatedAt:        workspace.CreatedAt,
		UpdatedAt:        time.Time{},
		UpdateFieldNames: nil,
	})
}

func (h *EventHandlers) creationErrorResponse(request cacao_common_service.WorkspaceModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.WorkspaceModel {
	return cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
		Name:  request.Name,
	}
}

// Update updates the workspace
func (h *EventHandlers) Update(ctx context.Context, request cacao_common_service.WorkspaceModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":          "workspace-service.domain",
		"function":         "EventHandlers.Update",
		"actor":            request.SessionActor,
		"emulator":         request.SessionEmulator,
		"workspace":        request.ID,
		"updateFieldNames": request.UpdateFieldNames,
	})
	logger.Infof("Updating workspace %s", request.ID.String())
	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.WorkspaceUpdateFailedEvent(h.updateErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace id is empty")
		logger.WithError(err).Error("bad request")
		sink.WorkspaceUpdateFailedEvent(h.updateErrorResponse(request, err))
		return
	}

	now := h.TimeSrc()

	var workspace = types.Workspace{
		ID:                request.ID,
		Owner:             request.SessionActor, // user can only them their own workspace
		Name:              request.Name,
		Description:       request.Description,
		DefaultProviderID: request.DefaultProviderID,
		UpdatedAt:         now,
	}

	if len(request.UpdateFieldNames) == 0 {
		// update all fields
		request.UpdateFieldNames = append(request.UpdateFieldNames, "name", "description", "default_provider_id")
	}

	err := h.Storage.Update(ctx, workspace, request.UpdateFieldNames)
	if err != nil {
		logger.WithError(err).Error("fail to update workspace in storage")
		sink.WorkspaceUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	// get the final result
	updatedWorkspace, err := h.Storage.Get(ctx, request.SessionActor, workspace.ID)
	if err != nil {
		logger.WithError(err).Error("fail to re-fetch workspace after update")
		sink.WorkspaceUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	var response = cacao_common_service.WorkspaceModel{
		Session:   cacao_common_service.CopySessionActors(request.Session),
		ID:        updatedWorkspace.ID,
		Owner:     updatedWorkspace.Owner,
		Name:      updatedWorkspace.Name,
		CreatedAt: updatedWorkspace.CreatedAt,
		UpdatedAt: updatedWorkspace.UpdatedAt,
	}
	sink.WorkspaceUpdatedEvent(response)
}

func (h *EventHandlers) updateErrorResponse(request cacao_common_service.WorkspaceModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.WorkspaceModel {
	return cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:               request.ID,
		Owner:            request.SessionActor,
		Name:             request.Name,
		UpdateFieldNames: request.UpdateFieldNames,
	}
}

// Delete deletes the workspace
func (h *EventHandlers) Delete(ctx context.Context, request cacao_common_service.WorkspaceModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":   "workspace-service.domain",
		"function":  "EventHandlers.Delete",
		"actor":     request.SessionActor,
		"emulator":  request.SessionEmulator,
		"workspace": request.ID,
	})

	logger.Infof("deleting workspace")
	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.WorkspaceDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace id is empty")
		logger.WithError(err).Error("bad request")
		sink.WorkspaceDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	// get the workspace first
	workspace, err := h.Storage.Get(ctx, request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch workspace from storage before deletion")
		sink.WorkspaceDeleteFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}

	err = h.Storage.Delete(ctx, request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete workspace in storage")
		sink.WorkspaceDeleteFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}
	logger.Infof("workspace deleted")

	sink.WorkspaceDeletedEvent(cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.CopySessionActors(request.Session),
		ID:      workspace.ID,
		Owner:   workspace.Owner,
		Name:    workspace.Name,
	})
}

func (h *EventHandlers) deletionErrorResponse(request cacao_common_service.WorkspaceModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.WorkspaceModel {
	return cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
	}
}

func toCacaoError(err error) cacao_common_service.CacaoError {
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		return cerr
	}
	return cacao_common_service.NewCacaoGeneralError(err.Error())
}
