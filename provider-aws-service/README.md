# provider-aws-service

This service provides access to some AWS operations.

## Queries
| query subject  | description                                                    |
|----------------|----------------------------------------------------------------|
| "images.list"  | List available EC2 images in the AWS region.                   |
| "images.get"   | Get the details for a single image in the AWS region.          |
| "flavors.list" | List the available AWS EC2 instance types.                     |
| "flavors.get"  | Get the details for a AWC EC2 instance type in the AWS region. |
| "regions.list" | List the available AWS regions.                                |


## Building

To build this microservice, you'll need to have a reasonably up-to-date version
of Go installed. Then run the following in the top-level directory containing
the service code:

```
go build
```

The service should build with that single step. If that is not the case, please
file an issue.

This service uses Go modules.

## Configuration

The service is configured through a set of environment variables. Here is the
important subset of the variables:

| env name                      | description                                                                              | default          |
|-------------------------------|------------------------------------------------------------------------------------------|------------------|
| `DEFAULT_CHANNEL_BUFFER_SIZE` | default channel buffer size, the number of messages(event & query) the is kept in buffer | 1                |
| `QUERY_WORKER_COUNT`          | query worker count (number of go routine listens for query request)                      | 10               |
| `POD_NAME`                    | name of the pod, used as NATS client ID if set                                           |                  |
| `LOG_LEVEL`                   | log level                                                                                | debug            |
| `NATS_URL`                    | -                                                                                        | nats://nats:4222 |
| `NATS_QUEUE_GROUP`            | -                                                                                        |                  |
| `NATS_WILDCARD_SUBJECT`       | this is set to `cyverse.providers.aws.>`                                                 |                  |
| `NATS_CLIENT_ID`              | -                                                                                        |                  |
| `NATS_MAX_RECONNECTS`         | -                                                                                        | -1               |
| `NATS_RECONNECT_WAIT`         | -                                                                                        | -1               |
| `NATS_REQUEST_TIMEOUT`        | -                                                                                        | -1               |


## Credentials Access

AWS operations requires authentication. The credential should be specified as a Credential object in the request.

Credential ID refers to the ID of a credential object stored in credential service. Its value field must be a JSON string similar to the following examples:

example of credential:
```json
{
  "AWS_ACCESS_KEY_ID" : "AXXXXXXXXXXXXXXXXXXXXXXX",
  "AWS_SECRET_ACCESS_KEY" : "SXXXXXXXXXXXXXXXXXXXXXXX"
}
```

The credential ID used by an operation will be passed in as part of the request.
If the credential ID is empty (or not provided, which defaults to empty string in Golang),
then the service will attempt a series of lookups in the credentials that user(actor)
owns. If no credential is selected, then the operation will error out.
