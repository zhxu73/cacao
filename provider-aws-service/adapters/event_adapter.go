package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"sync"
)

// EventAdapter communicates to IncomingEventPort
type EventAdapter struct {
	conn     messaging2.EventConnection
	handlers ports.IncomingEventHandlers
}

var _ ports.IncomingEventPort = (*EventAdapter)(nil)

// NewEventAdapter ...
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{
		conn:     conn,
		handlers: nil,
	}
}

// SetHandlers sets the mapping of event type to event handlers
func (adapter *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("Starting Event Adapter")

	err := adapter.conn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		service.EventCredentialAdded: eventHandlerWrapper(adapter.handlers.HandleCredentialAdded),
	}, wg)
	if err != nil {
		log.WithError(err).Error()
		return err
	}
	return nil
}

// this is a type set that contain the event body struct for all incoming events
type eventRequestStructTypeSet interface {
	service.CredentialCreateResponse | service.CredentialUpdateResponse
}

func eventHandlerWrapper[T eventRequestStructTypeSet](handler func(request T, sink ports.OutgoingEvents)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			return err
		}
		handler(request, EventOut{writer: writer})
		return nil
	}
}

// EventOut implements ports.OutgoingEvents, this will be pass as argument to ports.IncomingEventHandlers
type EventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEvents = (*EventOut)(nil)

// EventApplicationCredentialsCreated ...
func (e EventOut) EventApplicationCredentialsCreated(response providers.CreateApplicationCredentialResponse) {
	e.publish(response, providers.EventApplicationCredentialsCreated)
}

// EventApplicationCredentialsCreateFailed ...
func (e EventOut) EventApplicationCredentialsCreateFailed(response providers.CreateApplicationCredentialResponse) {
	e.publish(response, providers.EventApplicationCredentialsCreateFailed)
}

// EventApplicationCredentialsDeleted ...
func (e EventOut) EventApplicationCredentialsDeleted(response providers.DeleteApplicationCredentialResponse) {
	e.publish(response, providers.EventApplicationCredentialsDeleted)
}

// EventApplicationCredentialsDeleteFailed ...
func (e EventOut) EventApplicationCredentialsDeleteFailed(response providers.DeleteApplicationCredentialResponse) {
	e.publish(response, providers.EventApplicationCredentialsDeleteFailed)
}

func (e EventOut) publish(response interface{}, ceType common.EventType) {
	ce, err := messaging2.CreateCloudEvent(response, ceType, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		log.WithField("ceType", ceType).WithError(err).Error("fail to create cloudevent")
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		log.WithField("ceType", ceType).WithError(err).Error("fail to write response event")
		return
	}
}

// Nothing is used to explicitly denote that no events will be published by a handler
func (e EventOut) Nothing() {
}
