package adapters

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/ec2"
	ec2types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/aws/smithy-go/middleware"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/service/providers"
	awsmocks "gitlab.com/cyverse/cacao/provider-aws-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

var testContext = context.Background()

func TestAWSAdapter_GetImage(t *testing.T) {
	type fields struct {
		createClient func(region string) AWSSDK
	}
	type args struct {
		ctx        context.Context
		credential types.Credential
		id         string
		region     string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *providers.AWSImage
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdk := &awsmocks.AWSSDK{}
					sdk.On("DescribeImages", mockCredentialInCtx(t), &ec2.DescribeImagesInput{
						DryRun:            nil,
						ExecutableUsers:   nil,
						Filters:           nil,
						ImageIds:          []string{"ami-aaaaaaaaaaaaaaaaa"},
						IncludeDeprecated: nil,
						Owners:            nil,
					}).Return(&ec2.DescribeImagesOutput{
						Images: []ec2types.Image{
							{
								ImageId: func() *string {
									id := "ami-aaaaaaaaaaaaaaaaa"
									return &id
								}(),
								Name: func() *string {
									name := "amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2"
									return &name
								}(),
								State: "available",
							},
						},
						ResultMetadata: middleware.Metadata{},
					}, nil)
					return sdk
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				id:     "ami-aaaaaaaaaaaaaaaaa",
				region: "us-east-1",
			},
			want: &providers.AWSImage{
				Image: providers.Image{
					ID:     "ami-aaaaaaaaaaaaaaaaa",
					Name:   "amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2",
					Status: "available",
				},
			},
			wantErr: false,
		},
		{
			name: "failed",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdk := &awsmocks.AWSSDK{}
					sdk.On("DescribeImages", mockCredentialInCtx(t), &ec2.DescribeImagesInput{
						DryRun:            nil,
						ExecutableUsers:   nil,
						Filters:           nil,
						ImageIds:          []string{"ami-aaaaaaaaaaaaaaaaa"},
						IncludeDeprecated: nil,
						Owners:            nil,
					}).Return(&ec2.DescribeImagesOutput{}, fmt.Errorf("failed"))
					return sdk
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				id:     "ami-aaaaaaaaaaaaaaaaa",
				region: "us-east-1",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var sdkMocks AWSSDK
			o := &awsAdapter{
				createClient: func(region string) AWSSDK {
					assert.Equal(t, tt.args.region, region)
					sdkMocks = tt.fields.createClient(region) // capture the mock for assertion
					return sdkMocks
				},
			}
			got, err := o.GetImage(tt.args.ctx, tt.args.credential, tt.args.id, tt.args.region)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "GetImage() got = %v, want %v", got, tt.want)
			sdkMocks.(*awsmocks.AWSSDK).AssertExpectations(t)
		})
	}
}

func TestAWSAdapter_ListImages(t *testing.T) {
	type fields struct {
		createClient func(region string) AWSSDK
	}
	type args struct {
		ctx        context.Context
		credential types.Credential
		region     string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []providers.AWSImage
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeImages", mockCredentialInCtx(t), &ec2.DescribeImagesInput{
						DryRun:          nil,
						ExecutableUsers: nil,
						Filters: []ec2types.Filter{
							{
								Name:   aws.String("name"),
								Values: []string{"al2023*"},
							},
							{
								Name:   aws.String("state"),
								Values: []string{"available"},
							},
							{
								Name:   aws.String("architecture"),
								Values: []string{"x86_64"},
							},
						},
						ImageIds:          nil,
						IncludeDeprecated: nil,
						Owners:            []string{selfOwner, amazonLinuxOwnerID},
					}).Return(&ec2.DescribeImagesOutput{
						Images: []ec2types.Image{
							{
								ImageId: aws.String("ami-aaaaaaaaaaaaaaaaa"),
								Name:    aws.String("amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2"),
								State:   "available",
								OwnerId: aws.String(amazonLinuxOwnerID),
							},
						},
						ResultMetadata: middleware.Metadata{},
					}, nil).Once()
					sdkMocks.On("DescribeImages", mockCredentialInCtx(t), &ec2.DescribeImagesInput{
						DryRun:          nil,
						ExecutableUsers: nil,
						Filters: []ec2types.Filter{
							{
								Name:   aws.String("name"),
								Values: []string{"ubuntu-minimal/images/*/ubuntu-jammy-*", "ubuntu/images/*/ubuntu-jammy-*"},
							},
							{
								Name:   aws.String("state"),
								Values: []string{"available"},
							},
							{
								Name:   aws.String("architecture"),
								Values: []string{"x86_64"},
							},
						},
						ImageIds:          nil,
						IncludeDeprecated: nil,
						Owners:            []string{canonicalOwnerID},
					}).Return(&ec2.DescribeImagesOutput{
						Images: []ec2types.Image{
							{
								ImageId: aws.String("ami-bbbbbbbbbbbbbbbbb"),
								Name:    aws.String("ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912"),
								State:   "available",
								OwnerId: aws.String(canonicalOwnerID),
							},
						},
						ResultMetadata: middleware.Metadata{},
					}, nil).Once()
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				region: "us-east-1",
			},
			want: []providers.AWSImage{
				{
					Image: providers.Image{
						ID:     "ami-aaaaaaaaaaaaaaaaa",
						Name:   "amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2",
						Status: "available",
					},
				},
				{
					Image: providers.Image{
						ID:     "ami-bbbbbbbbbbbbbbbbb",
						Name:   "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912",
						Status: "available",
					},
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.NoError(t, err)
				return true
			},
		},
		{
			name: "failed",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeImages", mockCredentialInCtx(t), &ec2.DescribeImagesInput{
						DryRun:          nil,
						ExecutableUsers: nil,
						Filters: []ec2types.Filter{
							{
								Name:   aws.String("name"),
								Values: []string{"al2023*"},
							},
							{
								Name:   aws.String("state"),
								Values: []string{"available"},
							},
							{
								Name:   aws.String("architecture"),
								Values: []string{"x86_64"},
							},
						},
						ImageIds:          nil,
						IncludeDeprecated: nil,
						Owners:            []string{selfOwner, amazonLinuxOwnerID},
					}).Return(nil, fmt.Errorf("failed"))
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				region: "us-east-1",
			},
			want: []providers.AWSImage{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return true
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var sdkMocks AWSSDK
			o := &awsAdapter{
				createClient: func(region string) AWSSDK {
					assert.Equal(t, tt.args.region, region)
					sdkMocks = tt.fields.createClient(region) // capture the mock for assertion
					return sdkMocks
				},
			}
			got, err := o.ListImages(tt.args.ctx, tt.args.credential, tt.args.region)
			if !tt.wantErr(t, err, fmt.Sprintf("ListImages(%v, %v)", tt.args.ctx, tt.args.region)) {
				return
			}
			assert.ElementsMatchf(t, tt.want, got, "ListImages(%v, %v)", tt.args.ctx, tt.args.region)
			sdkMocks.(*awsmocks.AWSSDK).AssertExpectations(t)
		})
	}
}

func TestAWSAdapter_GetFlavor(t *testing.T) {
	type fields struct {
		createClient func(region string) AWSSDK
	}
	type args struct {
		ctx        context.Context
		credential types.Credential
		id         string
		region     string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *providers.Flavor
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeInstanceTypes", mockCredentialInCtx(t), &ec2.DescribeInstanceTypesInput{
						DryRun:        nil,
						Filters:       nil,
						InstanceTypes: []ec2types.InstanceType{"foo.bar"},
						MaxResults:    nil,
						NextToken:     nil,
					}).Return(&ec2.DescribeInstanceTypesOutput{
						InstanceTypes: []ec2types.InstanceTypeInfo{
							{
								InstanceStorageInfo: &ec2types.InstanceStorageInfo{
									Disks:             nil,
									EncryptionSupport: "",
									NvmeSupport:       "",
									TotalSizeInGB: func() *int64 {
										var disk = int64(1111)
										return &disk
									}(),
								},
								InstanceStorageSupported: func() *bool {
									var supported = true
									return &supported
								}(),
								InstanceType: "foo.bar",
								MemoryInfo: &ec2types.MemoryInfo{
									SizeInMiB: func() *int64 {
										var mem = int64(12345)
										return &mem
									}(),
								},
								VCpuInfo: &ec2types.VCpuInfo{
									DefaultCores:          nil,
									DefaultThreadsPerCore: nil,
									DefaultVCpus: func() *int32 {
										var vcpu int32 = 123
										return &vcpu
									}(),
									ValidCores:          nil,
									ValidThreadsPerCore: nil,
								},
							},
						},
						NextToken:      nil,
						ResultMetadata: middleware.Metadata{},
					}, nil)
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				id:     "foo.bar",
				region: "us-west-1",
			},
			want: &providers.Flavor{
				ID:        "foo.bar",
				Name:      "foo.bar",
				RAM:       12345,
				Ephemeral: 0,
				VCPUs:     123,
				IsPublic:  true,
				Disk:      1111,
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "failed",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeInstanceTypes", mockCredentialInCtx(t), &ec2.DescribeInstanceTypesInput{
						DryRun:        nil,
						Filters:       nil,
						InstanceTypes: []ec2types.InstanceType{"foo.bar"},
						MaxResults:    nil,
						NextToken:     nil,
					}).Return(nil, fmt.Errorf("failed"))
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				id:     "foo.bar",
				region: "us-east-1",
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var sdkMocks AWSSDK
			o := &awsAdapter{
				createClient: func(region string) AWSSDK {
					assert.Equal(t, tt.args.region, region)
					sdkMocks = tt.fields.createClient(region) // capture the mock for assertion
					return sdkMocks
				},
			}
			got, err := o.GetFlavor(tt.args.ctx, tt.args.credential, tt.args.id, tt.args.region)
			if !tt.wantErr(t, err, fmt.Sprintf("GetFlavor(%v, %v, %v)", tt.args.ctx, tt.args.id, tt.args.region)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetFlavor(%v, %v, %v)", tt.args.ctx, tt.args.id, tt.args.region)
			sdkMocks.(*awsmocks.AWSSDK).AssertExpectations(t)
		})
	}
}

func TestAWSAdapter_ListFlavors(t *testing.T) {
	type fields struct {
		createClient func(region string) AWSSDK
	}
	type args struct {
		ctx        context.Context
		credential types.Credential
		region     string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []providers.Flavor
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeInstanceTypes", mockCredentialInCtx(t), &ec2.DescribeInstanceTypesInput{
						DryRun: nil,
						Filters: []ec2types.Filter{
							{
								Name:   aws.String("processor-info.supported-architecture"),
								Values: []string{"x86_64"},
							},
						},
						InstanceTypes: nil,
						MaxResults:    aws.Int32(100),
						NextToken:     nil,
					}).Return(&ec2.DescribeInstanceTypesOutput{
						InstanceTypes: []ec2types.InstanceTypeInfo{
							{
								InstanceStorageInfo: &ec2types.InstanceStorageInfo{
									Disks:             nil,
									EncryptionSupport: "",
									NvmeSupport:       "",
									TotalSizeInGB: func() *int64 {
										var disk = int64(1111)
										return &disk
									}(),
								},
								InstanceStorageSupported: func() *bool {
									var supported = true
									return &supported
								}(),
								InstanceType: "foo.bar",
								MemoryInfo: &ec2types.MemoryInfo{
									SizeInMiB: func() *int64 {
										var mem = int64(12345)
										return &mem
									}(),
								},
								VCpuInfo: &ec2types.VCpuInfo{
									DefaultCores:          nil,
									DefaultThreadsPerCore: nil,
									DefaultVCpus: func() *int32 {
										var vcpu int32 = 123
										return &vcpu
									}(),
									ValidCores:          nil,
									ValidThreadsPerCore: nil,
								},
							},
						},
						NextToken:      nil,
						ResultMetadata: middleware.Metadata{},
					}, nil).Once()
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				region: "us-west-1",
			},
			want: []providers.Flavor{
				{
					ID:        "foo.bar",
					Name:      "foo.bar",
					RAM:       12345,
					Ephemeral: 0,
					VCPUs:     123,
					IsPublic:  true,
					Disk:      1111,
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "failed",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeInstanceTypes", mockCredentialInCtx(t), &ec2.DescribeInstanceTypesInput{
						DryRun: nil,
						Filters: []ec2types.Filter{
							{
								Name:   aws.String("processor-info.supported-architecture"),
								Values: []string{"x86_64"},
							},
						},
						InstanceTypes: nil,
						MaxResults:    aws.Int32(100),
						NextToken:     nil,
					}).Return(nil, fmt.Errorf("failed"))
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
				region: "us-east-1",
			},
			want: []providers.Flavor{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var sdkMocks AWSSDK
			o := &awsAdapter{
				createClient: func(region string) AWSSDK {
					assert.Equal(t, tt.args.region, region)
					sdkMocks = tt.fields.createClient(region) // capture the mock for assertion
					return sdkMocks
				},
			}
			got, err := o.ListFlavors(tt.args.ctx, tt.args.credential, tt.args.region)
			if !tt.wantErr(t, err, fmt.Sprintf("ListFlavors(%v, %v)", tt.args.ctx, tt.args.region)) {
				return
			}
			assert.ElementsMatchf(t, tt.want, got, "ListFlavors(%v, %v)", tt.args.ctx, tt.args.region)
			sdkMocks.(*awsmocks.AWSSDK).AssertExpectations(t)
		})
	}
}

func TestAWSAdapter_ListRegions(t *testing.T) {
	type fields struct {
		createClient func(region string) AWSSDK
	}
	type args struct {
		ctx        context.Context
		credential types.Credential
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []providers.Region
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeRegions", mockCredentialInCtx(t), &ec2.DescribeRegionsInput{
						AllRegions:  nil,
						DryRun:      nil,
						Filters:     nil,
						RegionNames: nil,
					}).Return(&ec2.DescribeRegionsOutput{
						Regions: []ec2types.Region{
							{
								Endpoint:    nil,
								OptInStatus: nil,
								RegionName: func() *string {
									var name = "us-east-1"
									return &name
								}(),
							},
							{
								Endpoint:    nil,
								OptInStatus: nil,
								RegionName: func() *string {
									var name = "eu-west-1"
									return &name
								}(),
							},
							{
								Endpoint:    nil,
								OptInStatus: nil,
								RegionName: func() *string {
									var name = "us-west-2"
									return &name
								}(),
							},
						},
						ResultMetadata: middleware.Metadata{},
					}, nil)
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
			},
			want: []providers.Region{
				{
					ID:   "us-east-1",
					Name: "us-east-1",
				},
				{
					ID:   "us-west-2",
					Name: "us-west-2",
				},
				{
					ID:   "eu-west-1",
					Name: "eu-west-1",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "failed",
			fields: fields{
				createClient: func(region string) AWSSDK {
					sdkMocks := &awsmocks.AWSSDK{}
					sdkMocks.On("DescribeRegions", mockCredentialInCtx(t), &ec2.DescribeRegionsInput{
						AllRegions:  nil,
						DryRun:      nil,
						Filters:     nil,
						RegionNames: nil,
					}).Return(nil, fmt.Errorf("failed"))
					return sdkMocks
				},
			},
			args: args{
				ctx: testContext,
				credential: types.Credential{
					ID:        "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Cred: types.AWSAccessKeyCredential{
						AccessKey: "foo",
						SecretKey: "bar",
					},
				},
			},
			want: []providers.Region{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var sdkMocks AWSSDK
			o := &awsAdapter{
				createClient: func(region string) AWSSDK {
					assert.Equal(t, "", region)
					sdkMocks = tt.fields.createClient(region) // capture the mock for assertion
					return sdkMocks
				},
			}
			got, err := o.ListRegions(tt.args.ctx, tt.args.credential)
			if !tt.wantErr(t, err, fmt.Sprintf("ListRegions(%v)", tt.args.ctx)) {
				return
			}
			assert.ElementsMatch(t, tt.want, got, "ListRegions(%v)", tt.args.ctx)
			sdkMocks.(*awsmocks.AWSSDK).AssertExpectations(t)
		})
	}
}

func mockCredentialInCtx(t *testing.T) interface{} {
	return mock.MatchedBy(func(ctx context.Context) bool {
		if !assert.NotNil(t, ctx) {
			return false
		}
		awsCred := awsCredFromCtx(ctx)
		if !assert.NotEmpty(t, awsCred.AccessKey) {
			return false
		}
		return assert.NotEmpty(t, awsCred.SecretKey)
	})
}
