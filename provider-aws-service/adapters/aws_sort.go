package adapters

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	ec2types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"strings"
)

type sortAWSRegion []ec2types.Region

// Len implements sort.Interface
func (s sortAWSRegion) Len() int {
	return len(s)
}

// Less implements sort.Interface
func (s sortAWSRegion) Less(i, j int) bool {
	if s[i].RegionName == nil && s[j].RegionName != nil {
		return false
	} else if s[i].RegionName != nil && s[j].RegionName == nil {
		return true
	} else if s[i].RegionName == nil && s[j].RegionName == nil {
		return true
	}
	// sort US region first
	iUS := strings.HasPrefix(*s[i].RegionName, "us-")
	jUS := strings.HasPrefix(*s[j].RegionName, "us-")
	if iUS && !jUS {
		return true
	}
	if !iUS && jUS {
		return false
	}
	return strings.Compare(*s[i].RegionName, *s[j].RegionName) < 0
}

// Swap implements sort.Interface
func (s sortAWSRegion) Swap(i, j int) {
	temp := s[i]
	s[i] = s[j]
	s[j] = temp
}

// sortInstanceTypes sort the free-tier first, then sort by alphabet order
type sortInstanceTypes []ec2types.InstanceTypeInfo

// Len implements sort.Interface
func (s sortInstanceTypes) Len() int {
	return len(s)
}

// Less implements sort.Interface
func (s sortInstanceTypes) Less(i, j int) bool {
	iFree := s[i].FreeTierEligible != nil && *s[i].FreeTierEligible
	jFree := s[j].FreeTierEligible != nil && *s[j].FreeTierEligible
	if iFree && !jFree {
		return true
	}
	if !iFree && jFree {
		return false
	}
	return strings.Compare(string(s[i].InstanceType), string(s[j].InstanceType)) < 0
}

// Swap implements sort.Interface
func (s sortInstanceTypes) Swap(i, j int) {
	temp := s[i]
	s[i] = s[j]
	s[j] = temp
}

// sort image in reverse alphabetical order, so that newer image of the same series appear first.
//
// e.g.
// al2023-ami-minimal-2023.1.20230719.0-kernel-6.1-x86_64
// al2023-ami-minimal-2023.1.20230705.0-kernel-6.1-x86_64
// al2023-ami-2023.1.20230719.0-kernel-6.1-x86_64
// al2023-ami-2023.1.20230705.0-kernel-6.1-x86_64
type sortAWSImages []ec2types.Image

// Len implements sort.Interface
func (s sortAWSImages) Len() int {
	return len(s)
}

// Less implements sort.Interface
func (s sortAWSImages) Less(i, j int) bool {
	return strings.Compare(aws.ToString(s[i].Name), aws.ToString(s[j].Name)) > 0
}

// Swap implements sort.Interface
func (s sortAWSImages) Swap(i, j int) {
	temp := s[i]
	s[i] = s[j]
	s[j] = temp
}
