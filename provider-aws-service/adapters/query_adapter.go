package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
	"sync"
)

// QueryAdapter implements ports.IncomingQueryPort & ports.OutgoingQueryPort
type QueryAdapter struct {
	closeQueryChan sync.Once
	queryChan      chan queryWrapper
	nc             *messaging2.NatsConnection
	handlers       ports.IncomingQueryHandlers
}

type queryWrapper struct {
	request cloudevents.Event
	writer  messaging2.ReplyWriter
}

// Init perform initialization on the adapter
func (adapter *QueryAdapter) Init(conf types.Configuration) {

	natsConn, err := conf.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Panic("fail to connect to NATS")
		return
	}
	adapter.nc = &natsConn
	adapter.queryChan = make(chan queryWrapper, conf.DefaultChannelBufferSize)
	log.Info("connected to NATS")
}

// SetHandlers ...
func (adapter *QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	adapter.handlers = handlers
}

// Start starts the queue subscriber
func (adapter *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	if adapter.nc == nil {
		return fmt.Errorf("nats connection is nil")
	}
	// using a separate WaitGroup for Listen(), this is necessary because we want to
	// close the channel after the listening stopped to shut down the query worker go
	// routines
	listenerWG := &sync.WaitGroup{}
	listenerWG.Add(1)
	err := adapter.nc.Listen(ctx, map[common.QueryOp]messaging2.QueryHandlerFunc{
		providers.AWSQueryPrefix + providers.CredentialListOp: adapter.sentToChannel,
		providers.AWSQueryPrefix + providers.RegionsListOp:    adapter.sentToChannel,
		providers.AWSQueryPrefix + providers.ImagesGetOp:      adapter.sentToChannel,
		providers.AWSQueryPrefix + providers.ImagesListOp:     adapter.sentToChannel,
		providers.AWSQueryPrefix + providers.FlavorsGetOp:     adapter.sentToChannel,
		providers.AWSQueryPrefix + providers.FlavorsListOp:    adapter.sentToChannel,
	}, listenerWG)
	if err != nil {
		return err
	}
	go adapter.spawnQueryWorkers(wg)
	go func() {
		listenerWG.Wait()
		adapter.closeQueryChan.Do(func() {
			close(adapter.queryChan)
		})
		wg.Done() // this is necessary because WaitGroup should be +1 before calling Start()
		log.Info("query adapter shutting down")
	}()
	log.Info("start listening to queries")
	return nil
}

func (adapter *QueryAdapter) sentToChannel(ctx context.Context, request cloudevents.Event, writer messaging2.ReplyWriter) {
	adapter.queryChan <- queryWrapper{
		request: request,
		writer:  writer,
	}
}

const queryWorkerCount = 10

func (adapter *QueryAdapter) spawnQueryWorkers(wg *sync.WaitGroup) {
	for i := 0; i < queryWorkerCount; i++ {
		wg.Add(1)
		go adapter.queryWorker(wg)
	}
}

func (adapter *QueryAdapter) queryWorker(wg *sync.WaitGroup) {
	defer wg.Done()
	mapping := map[common.QueryOp]messaging2.QueryHandlerFunc{
		providers.AWSQueryPrefix + providers.CredentialListOp:     handlerWrapper(adapter.handlers.CredentialListOp),
		providers.AWSQueryPrefix + providers.RegionsListOp:        handlerWrapper(adapter.handlers.RegionsListOp),
		providers.AWSQueryPrefix + providers.ImagesGetOp:          handlerWrapper(adapter.handlers.ImagesGetOp),
		providers.AWSQueryPrefix + providers.ImagesListOp:         handlerWrapper(adapter.handlers.ImagesListOp),
		providers.AWSQueryPrefix + providers.FlavorsGetOp:         handlerWrapper(adapter.handlers.FlavorsGetOp),
		providers.AWSQueryPrefix + providers.FlavorsListOp:        handlerWrapper(adapter.handlers.FlavorsListOp),
		providers.AWSQueryPrefix + providers.AuthenticationTestOp: handlerWrapper(adapter.handlers.AuthenticationTest),
	}
	for query := range adapter.queryChan {
		handler := mapping[common.QueryOp(query.request.Type())]
		handler(context.TODO(), query.request, query.writer)
	}
}

func handlerWrapper[T1, T2 any](handler func(ctx context.Context, request T1) T2) messaging2.QueryHandlerFunc {
	return func(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
		var request T1
		err := json.Unmarshal(requestCe.Data(), &request)
		if err != nil {
			log.WithField("ceType", requestCe.Type()).WithError(err).Error("fail to unmarshal query request")
			return
		}
		reply := handler(ctx, request)
		replyCe, err := messaging2.CreateCloudEvent(reply, common.QueryOp(requestCe.Type()), messaging2.AutoPopulateCloudEventSource)
		if err != nil {
			log.WithField("ceType", requestCe.Type()).WithError(err).Error("fail to create cloudevent for query reply")
			return
		}
		err = writer.Write(replyCe)
		if err != nil {
			log.WithField("ceType", requestCe.Type()).WithError(err).Error("fail to write query reply")
			return
		}
	}
}

// Close ...
func (adapter *QueryAdapter) Close() error {
	return adapter.nc.Close()
}
