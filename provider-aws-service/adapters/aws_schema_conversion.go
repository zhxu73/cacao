package adapters

// Conversion between AWS schema and cacao's schema

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	ec2types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

type imageAWS ec2types.Image

// Convert ...
func (img imageAWS) Convert() providers.AWSImage {
	return providers.AWSImage{
		Image: providers.Image{
			ID:     aws.ToString(img.ImageId),
			Name:   aws.ToString(img.Name),
			Status: string(img.State),
		},
	}
}

type flavorAWS ec2types.InstanceTypeInfo

// Convert ...
func (fl flavorAWS) Convert() providers.Flavor {

	var disk int64 = 0
	if aws.ToBool(fl.InstanceStorageSupported) {
		disk = aws.ToInt64(fl.InstanceStorageInfo.TotalSizeInGB)
	}

	return providers.Flavor{
		ID:       string(fl.InstanceType),
		Name:     string(fl.InstanceType),
		RAM:      aws.ToInt64(fl.MemoryInfo.SizeInMiB),
		VCPUs:    int64(aws.ToInt32(fl.VCpuInfo.DefaultVCpus)),
		IsPublic: true,
		Disk:     disk,
	}
}

type regionAWS ec2types.Region

// Convert ...
func (reg regionAWS) Convert() providers.Region {
	return providers.Region{
		ID:   aws.ToString(reg.RegionName),
		Name: aws.ToString(reg.RegionName),
	}
}
