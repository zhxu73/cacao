package adapters

import (
	"encoding/json"
	"reflect"
	"testing"

	"gitlab.com/cyverse/cacao-common/service/providers"
)

func Test_imageAWS_Convert(t *testing.T) {
	tests := []struct {
		name    string
		imgJSON string
		img     imageAWS
		want    providers.AWSImage
	}{
		{
			name: "amazon linux",
			imgJSON: `
{
  "Architecture": "x86_64",
  "BlockDeviceMappings": [
    {
      "DeviceName": "/dev/xvda",
      "Ebs": {
        "DeleteOnTermination": true,
        "Encrypted": false,
        "Iops": null,
        "KmsKeyId": null,
        "OutpostArn": null,
        "SnapshotId": "snap-0526cfc815bec45ca",
        "Throughput": null,
        "VolumeSize": 2,
        "VolumeType": "gp2"
      },
      "NoDevice": null,
      "VirtualName": null
    }
  ],
  "BootMode": "",
  "CreationDate": "2022-03-16T03:44:18.000Z",
  "DeprecationTime": "2024-03-16T03:44:18.000Z",
  "Description": "Amazon Linux 2 SELinux Enforcing AMI 2.0.20220316.0 x86_64 Minimal HVM gp2",
  "EnaSupport": true,
  "Hypervisor": "xen",
  "ImageId": "ami-059cd2be9c27a0e81",
  "ImageLocation": "amazon/amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2",
  "ImageOwnerAlias": "amazon",
  "ImageType": "machine",
  "KernelId": null,
  "Name": "amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2",
  "OwnerId": "137112412989",
  "Platform": "",
  "PlatformDetails": "Linux/UNIX",
  "ProductCodes": null,
  "Public": true,
  "RamdiskId": null,
  "RootDeviceName": "/dev/xvda",
  "RootDeviceType": "ebs",
  "SriovNetSupport": "simple",
  "State": "available",
  "StateReason": null,
  "Tags": null,
  "TpmSupport": "",
  "UsageOperation": "RunInstances",
  "VirtualizationType": "hvm"
}
`,
			img: imageAWS{},
			want: providers.AWSImage{
				Image: providers.Image{
					ID:     "ami-059cd2be9c27a0e81",
					Name:   "amzn2-ami-minimal-selinux-enforcing-hvm-2.0.20220316.0-x86_64-gp2",
					Status: "available",
				},
			},
		},
		{
			name: "ubuntu 22",
			imgJSON: `

{
  "Architecture": "x86_64",
  "BlockDeviceMappings": [
    {
      "DeviceName": "/dev/sda1",
      "Ebs": {
        "DeleteOnTermination": true,
        "Encrypted": false,
        "Iops": null,
        "KmsKeyId": null,
        "OutpostArn": null,
        "SnapshotId": "snap-0999f893e0b6a1b4e",
        "Throughput": null,
        "VolumeSize": 8,
        "VolumeType": "gp2"
      },
      "NoDevice": null,
      "VirtualName": null
    },
    {
      "DeviceName": "/dev/sdb",
      "Ebs": null,
      "NoDevice": null,
      "VirtualName": "ephemeral0"
    },
    {
      "DeviceName": "/dev/sdc",
      "Ebs": null,
      "NoDevice": null,
      "VirtualName": "ephemeral1"
    }
  ],
  "BootMode": "",
  "CreationDate": "2022-09-12T07:00:15.000Z",
  "DeprecationTime": "2024-09-12T07:00:15.000Z",
  "Description": "Canonical, Ubuntu, 22.04 LTS, amd64 jammy image build on 2022-09-12",
  "EnaSupport": true,
  "Hypervisor": "xen",
  "ImageId": "ami-08c40ec9ead489470",
  "ImageLocation": "amazon/ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912",
  "ImageOwnerAlias": "amazon",
  "ImageType": "machine",
  "KernelId": null,
  "Name": "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912",
  "OwnerId": "099720109477",
  "Platform": "",
  "PlatformDetails": "Linux/UNIX",
  "ProductCodes": null,
  "Public": true,
  "RamdiskId": null,
  "RootDeviceName": "/dev/sda1",
  "RootDeviceType": "ebs",
  "SriovNetSupport": "simple",
  "State": "available",
  "StateReason": null,
  "Tags": null,
  "TpmSupport": "",
  "UsageOperation": "RunInstances",
  "VirtualizationType": "hvm"
}
`,
			img: imageAWS{},
			want: providers.AWSImage{
				Image: providers.Image{
					ID:     "ami-08c40ec9ead489470",
					Name:   "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912",
					Status: "available",
				},
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var img imageAWS
			if tt.imgJSON == "" {
				img = tt.img
			} else {
				err := json.Unmarshal([]byte(tt.imgJSON), &img)
				if err != nil {
					panic("imgJSON must be parsed successfully")
				}
			}

			if got := img.Convert(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_flavorAWS_Convert(t *testing.T) {
	tests := []struct {
		name       string
		flavorJSON string
		fl         flavorAWS
		want       providers.Flavor
	}{
		{
			name: "t2.micro",
			flavorJSON: `
{
  "AutoRecoverySupported": true,
  "BareMetal": false,
  "BurstablePerformanceSupported": true,
  "CurrentGeneration": true,
  "DedicatedHostsSupported": false,
  "EbsInfo": {
    "EbsOptimizedInfo": null,
    "EbsOptimizedSupport": "unsupported",
    "EncryptionSupport": "supported",
    "NvmeSupport": "unsupported"
  },
  "FpgaInfo": null,
  "FreeTierEligible": true,
  "GpuInfo": null,
  "HibernationSupported": true,
  "Hypervisor": "xen",
  "InferenceAcceleratorInfo": null,
  "InstanceStorageInfo": null,
  "InstanceStorageSupported": false,
  "InstanceType": "t2.micro",
  "MemoryInfo": {
    "SizeInMiB": 1024
  },
  "NetworkInfo": {
    "DefaultNetworkCardIndex": 0,
    "EfaInfo": null,
    "EfaSupported": false,
    "EnaSupport": "unsupported",
    "EncryptionInTransitSupported": false,
    "Ipv4AddressesPerInterface": 2,
    "Ipv6AddressesPerInterface": 2,
    "Ipv6Supported": true,
    "MaximumNetworkCards": 1,
    "MaximumNetworkInterfaces": 2,
    "NetworkCards": [
      {
        "MaximumNetworkInterfaces": 2,
        "NetworkCardIndex": 0,
        "NetworkPerformance": "Low to Moderate"
      }
    ],
    "NetworkPerformance": "Low to Moderate"
  },
  "PlacementGroupInfo": {
    "SupportedStrategies": [
      "partition",
      "spread"
    ]
  },
  "ProcessorInfo": {
    "SupportedArchitectures": [
      "i386",
      "x86_64"
    ],
    "SustainedClockSpeedInGhz": 2.5
  },
  "SupportedBootModes": [
    "legacy-bios"
  ],
  "SupportedRootDeviceTypes": [
    "ebs"
  ],
  "SupportedUsageClasses": [
    "on-demand",
    "spot"
  ],
  "SupportedVirtualizationTypes": [
    "hvm"
  ],
  "VCpuInfo": {
    "DefaultCores": 1,
    "DefaultThreadsPerCore": 1,
    "DefaultVCpus": 1,
    "ValidCores": null,
    "ValidThreadsPerCore": null
  }
}
`,
			fl: flavorAWS{},
			want: providers.Flavor{
				ID:        "t2.micro",
				Name:      "t2.micro",
				RAM:       1024,
				Ephemeral: 0,
				VCPUs:     1,
				IsPublic:  true,
				Disk:      0,
			},
		},
		{
			name: "t3.micro",
			flavorJSON: `
{
  "AutoRecoverySupported": true,
  "BareMetal": false,
  "BurstablePerformanceSupported": true,
  "CurrentGeneration": true,
  "DedicatedHostsSupported": true,
  "EbsInfo": {
    "EbsOptimizedInfo": {
      "BaselineBandwidthInMbps": 87,
      "BaselineIops": 500,
      "BaselineThroughputInMBps": 10.875,
      "MaximumBandwidthInMbps": 2085,
      "MaximumIops": 11800,
      "MaximumThroughputInMBps": 260.625
    },
    "EbsOptimizedSupport": "default",
    "EncryptionSupport": "supported",
    "NvmeSupport": "required"
  },
  "FpgaInfo": null,
  "FreeTierEligible": false,
  "GpuInfo": null,
  "HibernationSupported": true,
  "Hypervisor": "nitro",
  "InferenceAcceleratorInfo": null,
  "InstanceStorageInfo": null,
  "InstanceStorageSupported": false,
  "InstanceType": "t3.micro",
  "MemoryInfo": {
    "SizeInMiB": 1024
  },
  "NetworkInfo": {
    "DefaultNetworkCardIndex": 0,
    "EfaInfo": null,
    "EfaSupported": false,
    "EnaSupport": "required",
    "EncryptionInTransitSupported": false,
    "Ipv4AddressesPerInterface": 2,
    "Ipv6AddressesPerInterface": 2,
    "Ipv6Supported": true,
    "MaximumNetworkCards": 1,
    "MaximumNetworkInterfaces": 2,
    "NetworkCards": [
      {
        "MaximumNetworkInterfaces": 2,
        "NetworkCardIndex": 0,
        "NetworkPerformance": "Up to 5 Gigabit"
      }
    ],
    "NetworkPerformance": "Up to 5 Gigabit"
  },
  "PlacementGroupInfo": {
    "SupportedStrategies": [
      "partition",
      "spread"
    ]
  },
  "ProcessorInfo": {
    "SupportedArchitectures": [
      "x86_64"
    ],
    "SustainedClockSpeedInGhz": 2.5
  },
  "SupportedBootModes": [
    "legacy-bios",
    "uefi"
  ],
  "SupportedRootDeviceTypes": [
    "ebs"
  ],
  "SupportedUsageClasses": [
    "on-demand",
    "spot"
  ],
  "SupportedVirtualizationTypes": [
    "hvm"
  ],
  "VCpuInfo": {
    "DefaultCores": 1,
    "DefaultThreadsPerCore": 2,
    "DefaultVCpus": 2,
    "ValidCores": [
      1
    ],
    "ValidThreadsPerCore": [
      1,
      2
    ]
  }
}
`,
			fl: flavorAWS{},
			want: providers.Flavor{
				ID:        "t3.micro",
				Name:      "t3.micro",
				RAM:       1024,
				Ephemeral: 0,
				VCPUs:     2,
				IsPublic:  true,
				Disk:      0,
			},
		},
		{
			name: "t3.large",
			flavorJSON: `
{
  "AutoRecoverySupported": true,
  "BareMetal": false,
  "BurstablePerformanceSupported": true,
  "CurrentGeneration": true,
  "DedicatedHostsSupported": true,
  "EbsInfo": {
    "EbsOptimizedInfo": {
      "BaselineBandwidthInMbps": 695,
      "BaselineIops": 4000,
      "BaselineThroughputInMBps": 86.875,
      "MaximumBandwidthInMbps": 2780,
      "MaximumIops": 15700,
      "MaximumThroughputInMBps": 347.5
    },
    "EbsOptimizedSupport": "default",
    "EncryptionSupport": "supported",
    "NvmeSupport": "required"
  },
  "FpgaInfo": null,
  "FreeTierEligible": false,
  "GpuInfo": null,
  "HibernationSupported": true,
  "Hypervisor": "nitro",
  "InferenceAcceleratorInfo": null,
  "InstanceStorageInfo": null,
  "InstanceStorageSupported": false,
  "InstanceType": "t3.large",
  "MemoryInfo": {
    "SizeInMiB": 8192
  },
  "NetworkInfo": {
    "DefaultNetworkCardIndex": 0,
    "EfaInfo": null,
    "EfaSupported": false,
    "EnaSupport": "required",
    "EncryptionInTransitSupported": false,
    "Ipv4AddressesPerInterface": 12,
    "Ipv6AddressesPerInterface": 12,
    "Ipv6Supported": true,
    "MaximumNetworkCards": 1,
    "MaximumNetworkInterfaces": 3,
    "NetworkCards": [
      {
        "MaximumNetworkInterfaces": 3,
        "NetworkCardIndex": 0,
        "NetworkPerformance": "Up to 5 Gigabit"
      }
    ],
    "NetworkPerformance": "Up to 5 Gigabit"
  },
  "PlacementGroupInfo": {
    "SupportedStrategies": [
      "partition",
      "spread"
    ]
  },
  "ProcessorInfo": {
    "SupportedArchitectures": [
      "x86_64"
    ],
    "SustainedClockSpeedInGhz": 2.5
  },
  "SupportedBootModes": [
    "legacy-bios",
    "uefi"
  ],
  "SupportedRootDeviceTypes": [
    "ebs"
  ],
  "SupportedUsageClasses": [
    "on-demand",
    "spot"
  ],
  "SupportedVirtualizationTypes": [
    "hvm"
  ],
  "VCpuInfo": {
    "DefaultCores": 1,
    "DefaultThreadsPerCore": 2,
    "DefaultVCpus": 2,
    "ValidCores": [
      1
    ],
    "ValidThreadsPerCore": [
      1,
      2
    ]
  }
}
`,
			fl: flavorAWS{},
			want: providers.Flavor{
				ID:        "t3.large",
				Name:      "t3.large",
				RAM:       8192,
				Ephemeral: 0,
				VCPUs:     2,
				IsPublic:  true,
				Disk:      0,
			},
		},
		{
			name: "m5ad.24xlarge",
			flavorJSON: `
{
  "AutoRecoverySupported": false,
  "BareMetal": false,
  "BurstablePerformanceSupported": false,
  "CurrentGeneration": true,
  "DedicatedHostsSupported": false,
  "EbsInfo": {
    "EbsOptimizedInfo": {
      "BaselineBandwidthInMbps": 13750,
      "BaselineIops": 60000,
      "BaselineThroughputInMBps": 1718.75,
      "MaximumBandwidthInMbps": 13750,
      "MaximumIops": 60000,
      "MaximumThroughputInMBps": 1718.75
    },
    "EbsOptimizedSupport": "default",
    "EncryptionSupport": "supported",
    "NvmeSupport": "required"
  },
  "FpgaInfo": null,
  "FreeTierEligible": false,
  "GpuInfo": null,
  "HibernationSupported": false,
  "Hypervisor": "nitro",
  "InferenceAcceleratorInfo": null,
  "InstanceStorageInfo": {
    "Disks": [
      {
        "Count": 4,
        "SizeInGB": 900,
        "Type": "ssd"
      }
    ],
    "EncryptionSupport": "required",
    "NvmeSupport": "required",
    "TotalSizeInGB": 3600
  },
  "InstanceStorageSupported": true,
  "InstanceType": "m5ad.24xlarge",
  "MemoryInfo": {
    "SizeInMiB": 393216
  },
  "NetworkInfo": {
    "DefaultNetworkCardIndex": 0,
    "EfaInfo": null,
    "EfaSupported": false,
    "EnaSupport": "required",
    "EncryptionInTransitSupported": false,
    "Ipv4AddressesPerInterface": 50,
    "Ipv6AddressesPerInterface": 50,
    "Ipv6Supported": true,
    "MaximumNetworkCards": 1,
    "MaximumNetworkInterfaces": 15,
    "NetworkCards": [
      {
        "MaximumNetworkInterfaces": 15,
        "NetworkCardIndex": 0,
        "NetworkPerformance": "20 Gigabit"
      }
    ],
    "NetworkPerformance": "20 Gigabit"
  },
  "PlacementGroupInfo": {
    "SupportedStrategies": [
      "cluster",
      "partition",
      "spread"
    ]
  },
  "ProcessorInfo": {
    "SupportedArchitectures": [
      "x86_64"
    ],
    "SustainedClockSpeedInGhz": 2.2
  },
  "SupportedBootModes": [
    "legacy-bios",
    "uefi"
  ],
  "SupportedRootDeviceTypes": [
    "ebs"
  ],
  "SupportedUsageClasses": [
    "on-demand",
    "spot"
  ],
  "SupportedVirtualizationTypes": [
    "hvm"
  ],
  "VCpuInfo": {
    "DefaultCores": 48,
    "DefaultThreadsPerCore": 2,
    "DefaultVCpus": 96,
    "ValidCores": [
      12,
      18,
      24,
      36,
      48
    ],
    "ValidThreadsPerCore": [
      1,
      2
    ]
  }
}
`,
			fl: flavorAWS{},
			want: providers.Flavor{
				ID:        "m5ad.24xlarge",
				Name:      "m5ad.24xlarge",
				RAM:       393216,
				Ephemeral: 0,
				VCPUs:     96,
				IsPublic:  true,
				Disk:      3600,
			},
		},
		{
			name: "p3.2xlarge",
			flavorJSON: `
{
  "AutoRecoverySupported": true,
  "BareMetal": false,
  "BurstablePerformanceSupported": false,
  "CurrentGeneration": true,
  "DedicatedHostsSupported": true,
  "EbsInfo": {
    "EbsOptimizedInfo": {
      "BaselineBandwidthInMbps": 1750,
      "BaselineIops": 10000,
      "BaselineThroughputInMBps": 218.75,
      "MaximumBandwidthInMbps": 1750,
      "MaximumIops": 10000,
      "MaximumThroughputInMBps": 218.75
    },
    "EbsOptimizedSupport": "default",
    "EncryptionSupport": "supported",
    "NvmeSupport": "unsupported"
  },
  "FpgaInfo": null,
  "FreeTierEligible": false,
  "GpuInfo": {
    "Gpus": [
      {
        "Count": 1,
        "Manufacturer": "NVIDIA",
        "MemoryInfo": {
          "SizeInMiB": 16384
        },
        "Name": "V100"
      }
    ],
    "TotalGpuMemoryInMiB": 16384
  },
  "HibernationSupported": false,
  "Hypervisor": "xen",
  "InferenceAcceleratorInfo": null,
  "InstanceStorageInfo": null,
  "InstanceStorageSupported": false,
  "InstanceType": "p3.2xlarge",
  "MemoryInfo": {
    "SizeInMiB": 62464
  },
  "NetworkInfo": {
    "DefaultNetworkCardIndex": 0,
    "EfaInfo": null,
    "EfaSupported": false,
    "EnaSupport": "supported",
    "EncryptionInTransitSupported": false,
    "Ipv4AddressesPerInterface": 15,
    "Ipv6AddressesPerInterface": 15,
    "Ipv6Supported": true,
    "MaximumNetworkCards": 1,
    "MaximumNetworkInterfaces": 4,
    "NetworkCards": [
      {
        "MaximumNetworkInterfaces": 4,
        "NetworkCardIndex": 0,
        "NetworkPerformance": "Up to 10 Gigabit"
      }
    ],
    "NetworkPerformance": "Up to 10 Gigabit"
  },
  "PlacementGroupInfo": {
    "SupportedStrategies": [
      "cluster",
      "partition",
      "spread"
    ]
  },
  "ProcessorInfo": {
    "SupportedArchitectures": [
      "x86_64"
    ],
    "SustainedClockSpeedInGhz": 2.7
  },
  "SupportedBootModes": [
    "legacy-bios"
  ],
  "SupportedRootDeviceTypes": [
    "ebs"
  ],
  "SupportedUsageClasses": [
    "on-demand",
    "spot"
  ],
  "SupportedVirtualizationTypes": [
    "hvm"
  ],
  "VCpuInfo": {
    "DefaultCores": 4,
    "DefaultThreadsPerCore": 2,
    "DefaultVCpus": 8,
    "ValidCores": [
      1,
      2,
      3,
      4
    ],
    "ValidThreadsPerCore": [
      1,
      2
    ]
  }
}
`,
			fl: flavorAWS{},
			want: providers.Flavor{
				ID:        "p3.2xlarge",
				Name:      "p3.2xlarge",
				RAM:       62464,
				Ephemeral: 0,
				VCPUs:     8,
				IsPublic:  true,
				Disk:      0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var fl flavorAWS
			if tt.flavorJSON == "" {
				fl = tt.fl
			} else {
				err := json.Unmarshal([]byte(tt.flavorJSON), &fl)
				if err != nil {
					panic("flavorJSON must be parsed successfully")
				}
			}
			if got := fl.Convert(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_regionAWS_Convert(t *testing.T) {
	tests := []struct {
		name       string
		regionJSON string
		reg        regionAWS
		want       providers.Region
	}{
		{
			name: "eu-west-1",
			regionJSON: `
{
  "Endpoint": "ec2.eu-west-1.amazonaws.com",
  "OptInStatus": "opt-in-not-required",
  "RegionName": "eu-west-1"
}
`,
			reg: regionAWS{},
			want: providers.Region{
				ID:   "eu-west-1",
				Name: "eu-west-1",
			},
		},
		{
			name: "us-east-1",
			regionJSON: `
{
  "Endpoint": "ec2.us-east-1.amazonaws.com",
  "OptInStatus": "opt-in-not-required",
  "RegionName": "us-east-1"
}
`,
			reg: regionAWS{},
			want: providers.Region{
				ID:   "us-east-1",
				Name: "us-east-1",
			},
		},
		{
			name: "us-west-2",
			regionJSON: `
{
  "Endpoint": "ec2.us-west-2.amazonaws.com",
  "OptInStatus": "opt-in-not-required",
  "RegionName": "us-west-2"
}
`,
			reg: regionAWS{},
			want: providers.Region{
				ID:   "us-west-2",
				Name: "us-west-2",
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var reg regionAWS
			if tt.regionJSON == "" {
				reg = tt.reg
			} else {
				err := json.Unmarshal([]byte(tt.regionJSON), &reg)
				if err != nil {
					panic("regionJSON must be parsed successfully")
				}
			}
			if got := reg.Convert(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() = %v, want %v", got, tt.want)
			}
		})
	}
}
