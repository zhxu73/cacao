package adapters

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"time"
)

// WorkspaceMicroservice ...
type WorkspaceMicroservice struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewWorkspaceMicroservice ...
func NewWorkspaceMicroservice(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) WorkspaceMicroservice {
	return WorkspaceMicroservice{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}

// defaultWorkspaceName is the name for default workspace for a provider
const defaultWorkspaceName = "Default Workspace"

// CreateWorkspaceForProviderAsync creates workspace for a user for a provider.
// The creation is async, it only sends the request, it does not wait for the
// response.
//
// Creating workspace using the default name, since the combination of (owner,
// name, default_provider_id) is unique (checked by the DB index), so if the
// workspace already exists, new workspace won't be created, this makes the
// operation idempotent.
func (svc WorkspaceMicroservice) CreateWorkspaceForProviderAsync(ctx context.Context, actor, emulator string, providerID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "WorkspaceMicroservice.CreateWorkspaceForProviderAsync",
		"actor":      actor,
		"emulator":   emulator,
		"providerID": providerID,
	})
	client, err := service.NewNatsWorkspaceClientFromConn(svc.queryConn, svc.eventConn)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	promise, err := client.CreateAsync(ctx, service.Actor{Actor: actor, Emulator: emulator}, service.WorkspaceModel{
		Session:           service.Session{},
		ID:                "",
		Owner:             actor,
		Name:              defaultWorkspaceName,
		Description:       fmt.Sprintf("Default workspace for provider %s", providerID),
		DefaultProviderID: providerID,
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdateFieldNames:  nil,
	})
	if err != nil {
		return err
	}
	_ = promise.Close()
	return nil
}
