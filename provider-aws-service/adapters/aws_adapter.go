package adapters

import (
	"context"
	"crypto/sha1"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	ec2types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/aws/aws-sdk-go-v2/service/sts"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
	"io"
	"sort"
	"time"
)

// awsAdapter is used to access medata from AWS
type awsAdapter struct {
	createClient func(region string) AWSSDK
}

// NewAWSAdapter creates a new AWS adapter
func NewAWSAdapter() ports.AWS {
	return &awsAdapter{
		createClient: createClient,
	}
}

// AWSSDK is an interface that has all the methods that will be used by awsAdapter. ec2.Client implements this interface.
// This interface is created to separate the adapter from the AWS sdk, thus allow mocking the AWS sdk for unit testing.
type AWSSDK interface {
	DescribeImages(ctx context.Context, param *ec2.DescribeImagesInput, optFn ...func(*ec2.Options)) (*ec2.DescribeImagesOutput, error)
	DescribeInstanceTypes(ctx context.Context, param *ec2.DescribeInstanceTypesInput, optFn ...func(*ec2.Options)) (*ec2.DescribeInstanceTypesOutput, error)
	DescribeRegions(ctx context.Context, param *ec2.DescribeRegionsInput, optFn ...func(*ec2.Options)) (*ec2.DescribeRegionsOutput, error)
	GetCallerIdentity(ctx context.Context, params *sts.GetCallerIdentityInput, optFns ...func(*sts.Options)) (*sts.GetCallerIdentityOutput, error)
}

// implements AWSSDK, combine multiple AWS service client together.
type awsSDK struct {
	ec2Client *ec2.Client
	stsClient *sts.Client
}

var _ AWSSDK = (*awsSDK)(nil)

// DescribeImages ...
func (sdk awsSDK) DescribeImages(ctx context.Context, param *ec2.DescribeImagesInput, optFn ...func(*ec2.Options)) (*ec2.DescribeImagesOutput, error) {
	return sdk.ec2Client.DescribeImages(ctx, param, optFn...)
}

// DescribeInstanceTypes ...
func (sdk awsSDK) DescribeInstanceTypes(ctx context.Context, param *ec2.DescribeInstanceTypesInput, optFn ...func(*ec2.Options)) (*ec2.DescribeInstanceTypesOutput, error) {
	return sdk.ec2Client.DescribeInstanceTypes(ctx, param, optFn...)
}

// DescribeRegions ...
func (sdk awsSDK) DescribeRegions(ctx context.Context, param *ec2.DescribeRegionsInput, optFn ...func(*ec2.Options)) (*ec2.DescribeRegionsOutput, error) {
	return sdk.ec2Client.DescribeRegions(ctx, param, optFn...)
}

// GetCallerIdentity is used for inspecting credential (ensure that the credential is at least a valid one), no permission is required for this operation.
func (sdk awsSDK) GetCallerIdentity(ctx context.Context, params *sts.GetCallerIdentityInput, optFns ...func(*sts.Options)) (*sts.GetCallerIdentityOutput, error) {
	return sdk.stsClient.GetCallerIdentity(ctx, params, optFns...)
}

type awsCtxKey int

// ONLY use withAWSCred to inject credential into context.
// the context value awsCtxKey constants should be unexported to avoid collision.
const (
	accessKeyIDCtxKey awsCtxKey = iota
	secretAccessKeyCtxKey
)

// ONLY use withAWSCred to inject credential into context.
func withAWSCred(parent context.Context, credential types.Credential) context.Context {
	ctx := context.WithValue(parent, accessKeyIDCtxKey, credential.Cred.AccessKey)
	return context.WithValue(ctx, secretAccessKeyCtxKey, credential.Cred.SecretKey)
}

func awsCredFromCtx(ctx context.Context) types.AWSAccessKeyCredential {
	accessKey := ctx.Value(accessKeyIDCtxKey)
	secretKey := ctx.Value(secretAccessKeyCtxKey)
	// because credential is injected by withAWSCred(), there is no need to check for nil or
	// the type of the value, we can just do type assertion.
	// if this panics then there is a bug somewhere.
	return types.AWSAccessKeyCredential{
		AccessKey: accessKey.(string),
		SecretKey: secretKey.(string),
	}
}

// retrieve is used to retrieve credentials from the context.
// this has the same function signature as aws.CredentialsProviderFunc.
func retrieve(ctx context.Context) (aws.Credentials, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "retrieve",
	})

	awsKey := awsCredFromCtx(ctx)
	credentials := aws.Credentials{
		AccessKeyID:     awsKey.AccessKey,
		SecretAccessKey: awsKey.SecretKey,
	}
	logger.WithFields(log.Fields{
		"accessKey": hashSecretForLogging(credentials.AccessKeyID),
		"secretKey": hashSecretForLogging(credentials.SecretAccessKey),
	}).Debug("credentials retrieved")
	return credentials, nil
}

func hashSecretForLogging(secret string) string {
	sha1.New()
	h := sha1.New()
	_, _ = io.WriteString(h, secret)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func createClient(region string) AWSSDK {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "createClient",
	})
	const defaultRegion = "us-east-1"

	if region == "" {
		log.WithField("region", region).Warn("region not specified, using default")
		region = defaultRegion
	}
	cfg := aws.Config{
		Credentials: aws.CredentialsProviderFunc(retrieve),
		Region:      region,
	}
	logger.WithField("region", region).Info("creating client")
	return awsSDK{
		ec2Client: ec2.NewFromConfig(
			cfg,
		),
		stsClient: sts.NewFromConfig(cfg),
	}
}

// GetImage attempts to retrieve a specific AWS image by ID.
func (a *awsAdapter) GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.AWSImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.GetImage",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	imageOutput, err := client.DescribeImages(withAWSCred(ctx, credential), &ec2.DescribeImagesInput{
		ImageIds: []string{id},
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch aws image")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to fetch aws image, %s", err.Error()))
	}

	if len(imageOutput.Images) == 0 {
		// check length just in case
		return nil, service.NewCacaoGeneralError("fail to fetch aws image, but no error from AWS")
	}
	image := imageAWS(imageOutput.Images[0]).Convert()
	logger.WithField("image", image.ID).Info("image fetched")
	return &image, err
}

// GetFlavor returns a list of instances types from AWS
func (a *awsAdapter) GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.GetFlavor",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	flavorOutput, err := client.DescribeInstanceTypes(withAWSCred(ctx, credential), &ec2.DescribeInstanceTypesInput{
		InstanceTypes: []ec2types.InstanceType{ec2types.InstanceType(id)},
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch aws instance type")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to fetch aws instance type, %s", err.Error()))
	}
	if len(flavorOutput.InstanceTypes) == 0 {
		// check length just in case
		return nil, service.NewCacaoGeneralError("fail to fetch aws instance types, but no error from AWS")
	}
	flavor := flavorAWS(flavorOutput.InstanceTypes[0]).Convert()
	logger.WithField("flavor", flavor.ID).Info("flavor fetched")
	return &flavor, err
}

const (
	// owner ID of amazon linux images.
	// filter on this as owner will return ~600 images
	amazonLinuxOwnerID = "137112412989"
	canonicalOwnerID   = "099720109477"
	// images owned by user
	selfOwner = "self"
)

// ListImages returns a list of images from AWS.
//
// - x86_64
// - amazon-linux-2023 or ubuntu-22
// - available
// - created within 3 month
func (a *awsAdapter) ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.AWSImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.ListImages",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	alImgOutput, err := client.DescribeImages(withAWSCred(ctx, credential), &ec2.DescribeImagesInput{
		Filters: []ec2types.Filter{
			filterByAmazonLinux2023(),
			filterByImageState("available"),
			filterImageByX64(),
		},
		Owners: []string{selfOwner, amazonLinuxOwnerID},
	})
	if err != nil {
		logger.WithError(err).Error("fail to list aws images")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws images, %s", err.Error()))
	}
	sort.Sort(sortAWSImages(alImgOutput.Images))

	ubuntuImgOutput, err := client.DescribeImages(withAWSCred(ctx, credential), &ec2.DescribeImagesInput{
		Filters: []ec2types.Filter{
			filterUbuntuImages(),
			filterByImageState("available"),
			filterImageByX64(),
		},
		Owners: []string{canonicalOwnerID},
	})
	if err != nil {
		logger.WithError(err).Error("fail to list aws images")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws images, %s", err.Error()))
	}
	sort.Sort(sortAWSImages(ubuntuImgOutput.Images))

	logger.WithField("length", len(alImgOutput.Images)+len(ubuntuImgOutput.Images)).Info("images listed")
	return a.convertImageList(append(alImgOutput.Images, ubuntuImgOutput.Images...)), err
}

// Amazon Linux 2023 EOL on 2028.
// Amazon Linux 2 EOL on 2025.
// So filter on 2023 only for now.
func filterByAmazonLinux2023() ec2types.Filter {
	var fieldName = "name"
	return ec2types.Filter{
		Name:   &fieldName,
		Values: []string{"al2023*"}, // TODO update this when next version comes out, e.g. al2025
	}
}

func filterUbuntuImages() ec2types.Filter {
	var fieldName = "name"
	return ec2types.Filter{
		Name:   &fieldName,
		Values: []string{"ubuntu-minimal/images/*/ubuntu-jammy-*", "ubuntu/images/*/ubuntu-jammy-*"},
	}
}

// available | pending | failed
func filterByImageState(state string) ec2types.Filter {
	var fieldName = "state"
	return ec2types.Filter{
		Name:   &fieldName,
		Values: []string{state},
	}
}

func filterImageByX64() ec2types.Filter {
	var fieldName = "architecture"
	return ec2types.Filter{
		Name:   &fieldName,
		Values: []string{"x86_64"},
	}
}

// convertImageList converts a list of imageAWS to a list of providers.AWSImage
func (a *awsAdapter) convertImageList(imageList []ec2types.Image) []providers.AWSImage {
	if len(imageList) == 0 {
		return []providers.AWSImage{}
	}
	const month = time.Hour * 24 * 30
	now := time.Now()
	var result []providers.AWSImage
	for _, image := range imageList {
		if image.CreationDate == nil || aws.ToString(image.OwnerId) == selfOwner {
			result = append(result, imageAWS(image).Convert())
			continue
		}
		// for Amazon Linux, filter out image older than 3 month
		parsedTime, err := time.Parse(time.RFC3339, *image.CreationDate)
		if err != nil {
			continue
		}
		if now.Sub(parsedTime) > month*3 {
			continue
		}
		result = append(result, imageAWS(image).Convert())
	}
	return result
}

// ListFlavors returns a list of instances types from AWS
func (a *awsAdapter) ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.ListFlavors",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	// list all instance types(x86_64) with pagination, max 100 per page
	var reqOpts = &ec2.DescribeInstanceTypesInput{
		Filters: []ec2types.Filter{
			filterInstanceTypeByX64(),
		},
		MaxResults: aws.Int32(100),
	}
	var result []ec2types.InstanceTypeInfo
	output, err := client.DescribeInstanceTypes(withAWSCred(ctx, credential), reqOpts)
	if err != nil {
		logger.WithError(err).Error("fail to list aws instance types")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws instance types, %s", err.Error()))
	}
	result = output.InstanceTypes

	for output.NextToken != nil {
		reqOpts.NextToken = output.NextToken
		output, err = client.DescribeInstanceTypes(withAWSCred(ctx, credential), reqOpts)
		if err != nil {
			logger.WithError(err).Error("fail to list aws instance types")
			return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws instance types, %s", err.Error()))
		}
		result = append(result, output.InstanceTypes...)
	}
	sort.Sort(sortInstanceTypes(result))
	logger.WithField("length", len(result)).Info("flavors listed")
	return a.convertFlavorList(result), nil
}

func filterInstanceTypeByX64() ec2types.Filter {
	var fieldName = "processor-info.supported-architecture"
	return ec2types.Filter{
		Name:   &fieldName,
		Values: []string{"x86_64"},
	}
}

// convertFlavorList converts a list of flavorAWS to a list of providers.Flavor
func (a *awsAdapter) convertFlavorList(flavorList []ec2types.InstanceTypeInfo) []providers.Flavor {
	var result []providers.Flavor
	for _, flavor := range flavorList {
		result = append(result, flavorAWS(flavor).Convert())

	}
	return result
}

// ListRegions returns a list of regions from AWS
func (a *awsAdapter) ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.ListRegions",
		"actor":    types.UsernameFromCtx(ctx),
	})

	client := a.createClient("")
	regionsOutput, err := client.DescribeRegions(withAWSCred(ctx, credential), &ec2.DescribeRegionsInput{})
	if err != nil {
		logger.WithError(err).Error("fail to list aws regions")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws regions, %s", err.Error()))
	}
	sort.Sort(sortAWSRegion(regionsOutput.Regions))

	logger.WithField("length", len(regionsOutput.Regions)).Info("Regions listed")
	return a.convertRegionList(regionsOutput.Regions), nil
}

// AuthenticationTest performs an authentication test on a credential to check if credential is valid.
func (a *awsAdapter) AuthenticationTest(ctx context.Context, credential types.Credential) (types.CallerIdentity, error) {
	client := a.createClient("")
	output, err := client.GetCallerIdentity(withAWSCred(ctx, credential), &sts.GetCallerIdentityInput{})
	if err != nil {
		return types.CallerIdentity{}, err
	}
	var result types.CallerIdentity
	if output.Account != nil {
		result.Account = *output.Account
	}
	if output.Arn != nil {
		result.Arn = *output.Arn
	}
	if output.UserId != nil {
		result.UserID = *output.UserId
	}
	return result, nil
}

// convertRegionList converts a list of regionAWS to a list of providers.Region
func (a *awsAdapter) convertRegionList(regionsList []ec2types.Region) []providers.Region {
	var result []providers.Region
	for _, region := range regionsList {
		result = append(result, regionAWS(region).Convert())
	}
	return result
}
