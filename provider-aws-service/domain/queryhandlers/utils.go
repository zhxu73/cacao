package queryhandlers

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

func errorReply(request providers.ProviderRequest, err error) providers.BaseProviderReply {
	var svcErr service.CacaoErrorBase
	if svcErr1, ok := err.(service.CacaoError); ok {
		svcErr = svcErr1.GetBase()
	} else {
		svcErr = service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return providers.BaseProviderReply{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    svcErr,
		},
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}

func requestToBaseReply(request providers.ProviderRequest) providers.BaseProviderReply {
	return providers.BaseProviderReply{
		Session:   request.Session,
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}
