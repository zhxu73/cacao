package domain

import (
	"context"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/eventhandlers"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/queryhandlers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
	"sync"
)

// Domain define the functions that a Domain type must support.
type Domain interface {
	Init(c types.Configuration)
	Start()
}

// AWSDomain ...
type AWSDomain struct {
	config      *types.Configuration
	QueryIn     ports.IncomingQueryPort
	EventIn     ports.IncomingEventPort
	AWS         ports.AWS
	CredMS      ports.CredentialMS
	ProviderMS  ports.ProviderMetadataMS
	WorkspaceMS ports.WorkspaceMS
	credFac     credsrc.CredentialFactory
}

// NewAWSDomain ...
func NewAWSDomain(c *types.Configuration, queryIn ports.IncomingQueryPort, eventIn ports.IncomingEventPort, aws ports.AWS, credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS, workspaceMS ports.WorkspaceMS) *AWSDomain {
	return &AWSDomain{
		config:      c,
		QueryIn:     queryIn,
		EventIn:     eventIn,
		AWS:         aws,
		CredMS:      credMS,
		ProviderMS:  providerMS,
		WorkspaceMS: workspaceMS,
		credFac:     credsrc.NewCredentialFactory(credMS, providerMS),
	}
}

// Start ...
func (domain *AWSDomain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	wg.Add(1)
	domain.QueryIn.SetHandlers(domain)
	err := domain.QueryIn.Start(ctx, &wg)
	if err != nil {
		return err
	}

	wg.Add(1)
	domain.EventIn.SetHandlers(domain)
	err = domain.EventIn.Start(ctx, &wg)
	if err != nil {
		return err
	}

	wg.Wait()
	return nil
}

var _ ports.IncomingQueryHandlers = (*AWSDomain)(nil)

// CredentialListOp ...
func (domain *AWSDomain) CredentialListOp(ctx context.Context, request providers.ProviderRequest) providers.CredentialListReply {
	return queryhandlers.CredentialHandler{CredentialMS: domain.CredMS}.Handle(ctx, request)
}

// RegionsListOp ...
func (domain *AWSDomain) RegionsListOp(ctx context.Context, request providers.ProviderRequest) providers.RegionListReply {
	return queryhandlers.RegionListHandler{AWS: domain.AWS, CredFac: domain.credFac}.Handle(ctx, request)
}

// ImagesGetOp ...
func (domain *AWSDomain) ImagesGetOp(ctx context.Context, request providers.ProviderRequest) providers.GetImageReply[providers.AWSImage] {
	return queryhandlers.ImageGetHandler{AWS: domain.AWS, CredFac: domain.credFac}.Handle(ctx, request)
}

// ImagesListOp ...
func (domain *AWSDomain) ImagesListOp(ctx context.Context, request providers.ProviderRequest) providers.ImageListReply[providers.AWSImage] {
	return queryhandlers.ImageListHandler{AWS: domain.AWS, CredFac: domain.credFac}.Handle(ctx, request)
}

// FlavorsGetOp ...
func (domain *AWSDomain) FlavorsGetOp(ctx context.Context, request providers.ProviderRequest) providers.GetFlavorReply {
	return queryhandlers.FlavorGetHandler{AWS: domain.AWS, CredFac: domain.credFac}.Handle(ctx, request)
}

// FlavorsListOp ...
func (domain *AWSDomain) FlavorsListOp(ctx context.Context, request providers.ProviderRequest) providers.FlavorListReply {
	return queryhandlers.FlavorListHandler{AWS: domain.AWS, CredFac: domain.credFac}.Handle(ctx, request)
}

// AuthenticationTest ...
func (domain *AWSDomain) AuthenticationTest(ctx context.Context, request providers.ProviderRequest) providers.AuthenticationTestReply {
	return queryhandlers.AuthenticateTestHandler{AWS: domain.AWS}.Handle(ctx, request)
}

// HandleCredentialAdded ...
func (domain *AWSDomain) HandleCredentialAdded(request service.CredentialCreateResponse, sink ports.OutgoingEvents) {
	eventhandlers.CredentialAddedHandler{
		ProviderMS:  domain.ProviderMS,
		CredMS:      domain.CredMS,
		AWSClient:   domain.AWS,
		WorkspaceMS: domain.WorkspaceMS,
	}.Handle(context.TODO(), request, sink)
}
