package eventhandlers

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
)

// CredentialAddedHandler ...
type CredentialAddedHandler struct {
	ProviderMS  ports.ProviderMetadataMS
	CredMS      ports.CredentialMS
	AWSClient   ports.AWS
	WorkspaceMS ports.WorkspaceMS
}

// Handle handles CredentialAdded event. It tests the AWS credential and disables
// the credential if the test fails; It creates workspaces for all the AWS
// providers that user has access to.
func (h CredentialAddedHandler) Handle(ctx context.Context, incoming service.CredentialCreateResponse, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandlers",
		"function": "CredentialCreatedHandler.Handle",
		"actor":    incoming.SessionActor,
		"emulator": incoming.SessionEmulator,
		"credID":   incoming.ID,
	})
	if incoming.Username == "" || incoming.ID == "" || incoming.Type != service.AWSCredentialType {
		return // skip
	}
	credential, err := h.CredMS.GetCredential(ctx, incoming.GetSessionActor(), incoming.GetSessionEmulator(), incoming.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch AWS credential")
		return
	}
	if credential == nil {
		return
	}
	testResult, err := h.AWSClient.AuthenticationTest(ctx, *credential)
	if err != nil {
		logger.WithError(err).Error("AWS credential test failed, invalid credential")

		// If the auth test failed, we disabled the credential. Though technically
		// failing the auth test does not mean the credential is invalid, there could be
		// the possibility of connectivity issue to AWS.
		err = h.CredMS.DisableCredential(ctx, incoming.SessionActor, incoming.SessionEmulator, incoming.ID)
		if err != nil {
			logger.WithError(err).Error("fail to disable AWS credential")
			return
		}

		return
	}
	logger.WithField("awsUserID", testResult.UserID).Info()

	// list aws provider
	awsProviders, err := h.ProviderMS.ListAWSProviders(ctx, incoming.SessionActor, incoming.SessionEmulator)
	if err != nil {
		logger.WithError(err).Error("fail to list AWS providers")
		return
	}
	for i := range awsProviders {
		// create workspace for provider
		err = h.WorkspaceMS.CreateWorkspaceForProviderAsync(ctx, incoming.SessionActor, incoming.SessionEmulator, awsProviders[i].ID)
		if err != nil {
			logger.WithField("providerID", awsProviders[i].ID).WithError(err).Error("fail to create workspace")
			continue
		}
	}
}
