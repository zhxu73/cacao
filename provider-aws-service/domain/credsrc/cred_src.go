package credsrc

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// CredentialFactory creates credential based on what's specified in the request
type CredentialFactory interface {
	GetCredential(ctx context.Context, request providers.ProviderRequest) (types.Credential, error)
}

// credentialFactory implements CredentialFactory
type credentialFactory struct {
	CredSelector credentialSelector
}

// NewCredentialFactory ...
func NewCredentialFactory(credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS) CredentialFactory {
	return credentialFactory{
		CredSelector: credentialSelector{
			credMS: credMS,
		},
	}
}

// GetCredential construct credential based on credential specified in the request
func (src credentialFactory) GetCredential(ctx context.Context, request providers.ProviderRequest) (types.Credential, error) {
	if ctx == nil {
		return types.Credential{}, fmt.Errorf("CredentialFactory: context is nil")
	}

	if request.Credential.Type != providers.ProviderCredentialID {
		return types.Credential{}, fmt.Errorf("unknown credential type")
	}
	return src.getCredViaID(ctx, request)

}

// getCredViaID fetches credential based on ID
func (src credentialFactory) getCredViaID(ctx context.Context, request providers.ProviderRequest) (types.Credential, error) {
	cred, err := src.CredSelector.SelectCredential(ctx, request)
	if err != nil {
		return types.Credential{}, err
	}
	if cred == nil {
		log.WithFields(log.Fields{
			"function": "credentialFactory.getCredViaID",
			"cred":     request.Credential,
		}).Error("credential is nil, this is a BUG")
		return types.Credential{}, fmt.Errorf("credential is nil, fail to fetch credential for unknown reason")
	}
	return *cred, nil
}
