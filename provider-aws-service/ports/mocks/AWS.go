// Code generated by mockery v2.35.4. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	providers "gitlab.com/cyverse/cacao-common/service/providers"

	types "gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// AWS is an autogenerated mock type for the AWS type
type AWS struct {
	mock.Mock
}

// AuthenticationTest provides a mock function with given fields: ctx, credential
func (_m *AWS) AuthenticationTest(ctx context.Context, credential types.Credential) (types.CallerIdentity, error) {
	ret := _m.Called(ctx, credential)

	var r0 types.CallerIdentity
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential) (types.CallerIdentity, error)); ok {
		return rf(ctx, credential)
	}
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential) types.CallerIdentity); ok {
		r0 = rf(ctx, credential)
	} else {
		r0 = ret.Get(0).(types.CallerIdentity)
	}

	if rf, ok := ret.Get(1).(func(context.Context, types.Credential) error); ok {
		r1 = rf(ctx, credential)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetFlavor provides a mock function with given fields: ctx, credential, id, region
func (_m *AWS) GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error) {
	ret := _m.Called(ctx, credential, id, region)

	var r0 *providers.Flavor
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string, string) (*providers.Flavor, error)); ok {
		return rf(ctx, credential, id, region)
	}
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string, string) *providers.Flavor); ok {
		r0 = rf(ctx, credential, id, region)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*providers.Flavor)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, types.Credential, string, string) error); ok {
		r1 = rf(ctx, credential, id, region)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetImage provides a mock function with given fields: ctx, credential, id, region
func (_m *AWS) GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.AWSImage, error) {
	ret := _m.Called(ctx, credential, id, region)

	var r0 *providers.AWSImage
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string, string) (*providers.AWSImage, error)); ok {
		return rf(ctx, credential, id, region)
	}
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string, string) *providers.AWSImage); ok {
		r0 = rf(ctx, credential, id, region)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*providers.AWSImage)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, types.Credential, string, string) error); ok {
		r1 = rf(ctx, credential, id, region)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListFlavors provides a mock function with given fields: ctx, credential, region
func (_m *AWS) ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error) {
	ret := _m.Called(ctx, credential, region)

	var r0 []providers.Flavor
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string) ([]providers.Flavor, error)); ok {
		return rf(ctx, credential, region)
	}
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string) []providers.Flavor); ok {
		r0 = rf(ctx, credential, region)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.Flavor)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, types.Credential, string) error); ok {
		r1 = rf(ctx, credential, region)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListImages provides a mock function with given fields: ctx, credential, region
func (_m *AWS) ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.AWSImage, error) {
	ret := _m.Called(ctx, credential, region)

	var r0 []providers.AWSImage
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string) ([]providers.AWSImage, error)); ok {
		return rf(ctx, credential, region)
	}
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential, string) []providers.AWSImage); ok {
		r0 = rf(ctx, credential, region)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.AWSImage)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, types.Credential, string) error); ok {
		r1 = rf(ctx, credential, region)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListRegions provides a mock function with given fields: ctx, credential
func (_m *AWS) ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error) {
	ret := _m.Called(ctx, credential)

	var r0 []providers.Region
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential) ([]providers.Region, error)); ok {
		return rf(ctx, credential)
	}
	if rf, ok := ret.Get(0).(func(context.Context, types.Credential) []providers.Region); ok {
		r0 = rf(ctx, credential)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.Region)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, types.Credential) error); ok {
		r1 = rf(ctx, credential)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewAWS creates a new instance of AWS. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewAWS(t interface {
	mock.TestingT
	Cleanup(func())
}) *AWS {
	mock := &AWS{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
