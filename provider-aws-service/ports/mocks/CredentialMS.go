// Code generated by mockery v2.33.3. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	service "gitlab.com/cyverse/cacao-common/service"

	types "gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// CredentialMS is an autogenerated mock type for the CredentialMS type
type CredentialMS struct {
	mock.Mock
}

// DisableCredential provides a mock function with given fields: ctx, actor, emulator, ID
func (_m *CredentialMS) DisableCredential(ctx context.Context, actor string, emulator string, ID string) error {
	ret := _m.Called(ctx, actor, emulator, ID)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) error); ok {
		r0 = rf(ctx, actor, emulator, ID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCredential provides a mock function with given fields: ctx, actor, emulator, ID
func (_m *CredentialMS) GetCredential(ctx context.Context, actor string, emulator string, ID string) (*types.Credential, error) {
	ret := _m.Called(ctx, actor, emulator, ID)

	var r0 *types.Credential
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) (*types.Credential, error)); ok {
		return rf(ctx, actor, emulator, ID)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) *types.Credential); ok {
		r0 = rf(ctx, actor, emulator, ID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*types.Credential)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string, string, string) error); ok {
		r1 = rf(ctx, actor, emulator, ID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListCredentials provides a mock function with given fields: ctx, actor, emulator
func (_m *CredentialMS) ListCredentials(ctx context.Context, actor string, emulator string) ([]service.CredentialModel, error) {
	ret := _m.Called(ctx, actor, emulator)

	var r0 []service.CredentialModel
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string) ([]service.CredentialModel, error)); ok {
		return rf(ctx, actor, emulator)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string, string) []service.CredentialModel); ok {
		r0 = rf(ctx, actor, emulator)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]service.CredentialModel)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, actor, emulator)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListCredentialsByTag provides a mock function with given fields: ctx, actor, emulator, tagName
func (_m *CredentialMS) ListCredentialsByTag(ctx context.Context, actor string, emulator string, tagName string) ([]types.Credential, error) {
	ret := _m.Called(ctx, actor, emulator, tagName)

	var r0 []types.Credential
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) ([]types.Credential, error)); ok {
		return rf(ctx, actor, emulator, tagName)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) []types.Credential); ok {
		r0 = rf(ctx, actor, emulator, tagName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]types.Credential)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string, string, string) error); ok {
		r1 = rf(ctx, actor, emulator, tagName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewCredentialMS creates a new instance of CredentialMS. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewCredentialMS(t interface {
	mock.TestingT
	Cleanup(func())
}) *CredentialMS {
	mock := &CredentialMS{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
