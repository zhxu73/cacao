package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
	"sync"
)

// IncomingQueryPort is an interface for queries.
// Incoming queries are pushed into a channel by the adapter that implement this interface,
// and domain will be popping queries off from the channel to process them.
// Domain will then reply to the query by calling types.CloudEventRequest.Replyer.Reply().
type IncomingQueryPort interface {
	Init(config types.Configuration)
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingQueryHandlers)
}

// IncomingQueryHandlers ...
type IncomingQueryHandlers interface {
	CredentialListOp(ctx context.Context, request providers.ProviderRequest) providers.CredentialListReply
	RegionsListOp(ctx context.Context, request providers.ProviderRequest) providers.RegionListReply
	ImagesGetOp(ctx context.Context, request providers.ProviderRequest) providers.GetImageReply[providers.AWSImage]
	ImagesListOp(ctx context.Context, request providers.ProviderRequest) providers.ImageListReply[providers.AWSImage]
	FlavorsGetOp(ctx context.Context, request providers.ProviderRequest) providers.GetFlavorReply
	FlavorsListOp(ctx context.Context, request providers.ProviderRequest) providers.FlavorListReply
	AuthenticationTest(ctx context.Context, request providers.ProviderRequest) providers.AuthenticationTestReply
}

// IncomingEventPort ...
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for the event operations that this service supports
type IncomingEventHandlers interface {
	HandleCredentialAdded(request service.CredentialCreateResponse, sink OutgoingEvents)
}

// OutgoingEvents ...
type OutgoingEvents interface {
}

// ProviderMetadataMS is an interface to interact with provider metadata service.
type ProviderMetadataMS interface {
	GetMetadata(ctx context.Context, actor, emulator string, providerID common.ID) (map[string]interface{}, error)
	ListAWSProviders(ctx context.Context, actor, emulator string) ([]service.ProviderModel, error)
}

// CredentialMS ...
type CredentialMS interface {
	GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error)
	ListCredentials(ctx context.Context, actor, emulator string) ([]service.CredentialModel, error)
	ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error)
	DisableCredential(ctx context.Context, actor, emulator, ID string) error
}

// WorkspaceMS ...
type WorkspaceMS interface {
	CreateWorkspaceForProviderAsync(ctx context.Context, actor, emulator string, providerID common.ID) error
}

// AWS ...
type AWS interface {
	GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error)
	GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.AWSImage, error)
	ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error)
	ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error)
	ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.AWSImage, error)
	AuthenticationTest(ctx context.Context, credential types.Credential) (types.CallerIdentity, error)
}
