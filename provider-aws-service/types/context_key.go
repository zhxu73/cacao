package types

import (
	"context"
)

// the context value key type should be unexported to avoid collision.
type key int

// the context value key constants should be unexported to avoid collision.
const (
	// usernameKey is what you should use as a key for the username in context.Context.
	usernameKey key = iota
)

// WithUsername inject username into context
func WithUsername(parent context.Context, username string) context.Context {
	return context.WithValue(parent, usernameKey, username)
}

// UsernameFromCtx extracts username from context, return "' if key not exists or value not string
func UsernameFromCtx(ctx context.Context) string {
	usernameRaw := ctx.Value(usernameKey)
	if usernameRaw == nil {
		return ""
	}
	username, ok := usernameRaw.(string)
	if !ok {
		return ""
	}
	return username
}
