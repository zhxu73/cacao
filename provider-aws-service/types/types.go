package types

import (
	"errors"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/ec2"
)

// Credential ...
type Credential struct {
	ID        string
	CreatedAt time.Time
	UpdatedAt time.Time
	Cred      AWSAccessKeyCredential
}

// AWSAccessKeyCredential is the format of AWS credential that this service accepts.
// The Value field in the credential we get from credential service should be a JSON string of this struct.
type AWSAccessKeyCredential struct {
	AccessKey string `json:"AWS_ACCESS_KEY_ID" mapstructure:"AWS_ACCESS_KEY_ID"`
	SecretKey string `json:"AWS_SECRET_ACCESS_KEY" mapstructure:"AWS_SECRET_ACCESS_KEY"`
}

// AWSClient ...
type AWSClient struct {
	*ec2.Client
}

// Configuration contains the settings needed to configure this microservice.
type Configuration struct {
	Messaging                messaging2.NatsStanMsgConfig
	DefaultChannelBufferSize int    `envconfig:"DEFAULT_CHANNEL_BUFFER_SIZE" default:"1"`
	QueryWorkerCount         int    `envconfig:"QUERY_WORKER_COUNT" default:"10"`
	PodName                  string `envconfig:"POD_NAME"`
	LogLevel                 string `envconfig:"LOG_LEVEL" default:"trace"`
}

// Validate returns an error if there's something wrong with the configuration.
func (c *Configuration) Validate() error {
	if c.Messaging.ClientID == "" {
		return errors.New("NATS_CLIENT_ID environment variable must be set")
	}

	if c.QueryWorkerCount <= 0 {
		return errors.New("QUERY_WORKER_COUNT cannot be zero or negative")
	}

	return nil
}

// Override some config values
func (c *Configuration) Override() {
	c.Messaging.QueueGroup = "provider-aws-service"
	c.Messaging.DurableName = "provider-aws-service"
	c.Messaging.WildcardSubject = "cyverse.providers.aws.>"
}

// CallerIdentity is the result we get back from authentication test a credential
type CallerIdentity struct {
	// The Amazon Web Services account ID number
	Account string
	Arn     string
	// The unique identifier of the calling entity.
	UserID string
}
