package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/adapters"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/domain"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

var envConf types.EnvConfig
var config types.Config

func init() {
	loadConfig()
}

func main() {
	natsConn, err := config.MessagingConfig.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
	}
	stanConn, err := config.MessagingConfig.ConnectStan()
	if err != nil {
		log.WithError(err).Panic()
	}
	svc := newService(envConf, config, &natsConn, &stanConn)

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)
	if err := svc.Init(envConf, config); err != nil {
		log.WithError(err).Panic(err)
	}
	if err := svc.Start(serviceCtx); err != nil {
		log.WithError(err).Panic(err)
	}
}

func loadConfig() {
	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatal(err)
	}
	lvl, err := log.ParseLevel(envConf.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(lvl)

	err = envconfig.Process("", &config.MessagingConfig)
	if err != nil {
		log.Fatal(err)
	}
	// set NATS client ID if missing
	if config.MessagingConfig.ClientID == "" {
		if envConf.PodName != "" {
			config.MessagingConfig.ClientID = envConf.PodName
		} else {
			config.MessagingConfig.ClientID = "deployment-metadata-" + xid.New().String()
		}
	}
	err = envconfig.Process("", &config.MongoConfig)
	if err != nil {
		log.Fatal(err)
	}
	config.Override()
	log.Info("envvar config loaded")
}

func newService(envConf types.EnvConfig, conf types.Config, queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) domain.Domain {
	natsAdapter := adapters.NewNATSAdapter()
	stanAdapter := adapters.NewStanAdapter(eventConn)
	svc := domain.Domain{
		NatsSrc:     natsAdapter,
		NatsSink:    natsAdapter,
		EventSrc:    stanAdapter,
		Storage:     adapters.NewMongoDeploymentStorage(conf.MongoConfig),
		RunStorage:  adapters.NewMongoDeploymentRunStorage(conf.MongoConfig),
		TimeSrc:     adapters.UTCTimeSrc{},
		WorkspaceMS: adapters.NewWorkspaceMicroservice(queryConn),
		TemplateMS:  adapters.NewTemplateMicroservice(queryConn),
		ProviderMS:  adapters.NewProviderMicroservice(queryConn),
		CredMS:      adapters.NewCredentialMicroservice(queryConn),
	}
	return svc
}
