package adapters

// this file contains simple wrapper for microservice adapters from internal/msadapters package

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// WorkspaceMicroservice is a client to interact with Workspace microservice
type WorkspaceMicroservice struct {
	client service.WorkspaceClient
}

// NewWorkspaceMicroservice ...
func NewWorkspaceMicroservice(queryConn messaging2.QueryConnection) *WorkspaceMicroservice {
	client, err := service.NewNatsWorkspaceClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
	}
	return &WorkspaceMicroservice{
		client: client,
	}
}

// Get a workspace
func (w WorkspaceMicroservice) Get(actor types.Actor, id common.ID) (*service.WorkspaceModel, error) {
	return w.client.Get(context.TODO(), actor, id)
}

// ProviderMicroservice is a client to provider microservice
type ProviderMicroservice struct {
	client service.ProviderClient
}

// NewProviderMicroservice ...
func NewProviderMicroservice(queryConn messaging2.QueryConnection) *ProviderMicroservice {
	client, err := service.NewNatsProviderClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
	}
	return &ProviderMicroservice{client: client}
}

// Get fetch a provider by ID
func (p ProviderMicroservice) Get(actor types.Actor, id common.ID) (*service.ProviderModel, error) {
	return p.client.Get(context.TODO(), actor, id)
}

// List fetches a list of provider
func (p ProviderMicroservice) List(actor types.Actor) ([]service.ProviderModel, error) {
	return p.client.List(context.TODO(), actor)
}

// TemplateMicroservice is a client for template microservice
type TemplateMicroservice struct {
	client service.TemplateClient
}

// NewTemplateMicroservice ...
func NewTemplateMicroservice(queryConn messaging2.QueryConnection) *TemplateMicroservice {
	client, err := service.NewNatsTemplateClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
	}
	return &TemplateMicroservice{client: client}
}

// Get fetches a template by ID
func (t TemplateMicroservice) Get(actor types.Actor, id common.ID) (service.Template, error) {
	return t.client.Get(context.TODO(), actor, id)
}

// GetTemplateType fetches a template type by name
func (t TemplateMicroservice) GetTemplateType(actor types.Actor, templateTypeName service.TemplateTypeName) (service.TemplateType, error) {
	return t.client.GetType(context.TODO(), actor, templateTypeName)
}

// GetTemplateVersion ...
func (t TemplateMicroservice) GetTemplateVersion(actor types.Actor, id common.ID) (*service.TemplateVersion, error) {
	return t.client.GetTemplateVersion(context.TODO(), actor, id)
}

// CredentialMicroservice ...
type CredentialMicroservice struct {
	client service.CredentialClient
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(queryConn messaging2.QueryConnection) *CredentialMicroservice {
	client, err := service.NewNatsCredentialClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
	}
	return &CredentialMicroservice{client: client}
}

// Get a credential by ID from credential microservice.
// Note: the combination of username and cred ID is unique. The ID alone is Not.
func (c CredentialMicroservice) Get(actor types.Actor, owner, id string) (*service.CredentialModel, error) {
	if actor.Actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	return c.client.Get(context.TODO(), actor, id)
}

// List returns a list of all credentials owned by a user.
func (c CredentialMicroservice) List(actor types.Actor, owner string) ([]service.CredentialModel, error) {
	if actor.Actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	return c.client.List(context.TODO(), actor, service.CredentialListFilter{Username: owner})
}
