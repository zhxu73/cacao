package adapters

import (
	"container/list"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"sync"
)

// NatsMock is mock for NATS
type NatsMock struct {
	replies  map[string]*list.List
	incoming list.List
	channel  chan<- types.Query
	lock     sync.Mutex
}

// NewNatsMock ...
func NewNatsMock() *NatsMock {
	return &NatsMock{
		replies:  make(map[string]*list.List),
		incoming: *list.New(),
	}
}

// Init ...
func (n *NatsMock) Init(messaging2.NatsConfig) error {
	return nil
}

// Reply ...
func (n *NatsMock) Reply(subject string, reply cloudevents.Event) error {
	_, ok := n.replies[subject]
	if !ok {
		n.replies[subject] = list.New()
	}
	n.replies[subject].PushBack(reply)
	return nil
}

// InitChannel ...
func (n *NatsMock) InitChannel(channel chan<- types.Query) {
	n.channel = channel
}

// Start ...
func (n *NatsMock) Start() error {
	for {
		n.lock.Lock()
		if n.incoming.Len() == 0 {
			n.lock.Unlock()
			break
		}
		query := n.incoming.Front().Value.(Query)
		n.incoming.Remove(n.incoming.Front())
		n.lock.Unlock()

		n.channel <- query
	}
	return nil
}

// PushMsg push msg to incoming, which will be pushed to channel in Start()
func (n *NatsMock) PushMsg(ce cloudevents.Event) {
	n.lock.Lock()
	defer n.lock.Unlock()
	subject := xid.New().String()
	n.incoming.PushBack(Query{subject, ce})
}

// GetReply will fetch a reply with the specified reply subject.
func (n *NatsMock) GetReply(subject string) *cloudevents.Event {
	lst, ok := n.replies[subject]
	if !ok {
		return nil
	}
	if lst.Len() == 0 {
		return nil
	}
	reply := lst.Front().Value.(cloudevents.Event)
	return &reply
}
