package queryhandler

import (
	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	domainmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/domain/mocks"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	typesmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/types/mocks"
	"math/rand"
	"testing"
)

func TestListRunHandler_HandleStream(t *testing.T) {
	t.Run("existing runs", testListRunHandlerExistingRun)
	// FIXME
	t.Run("empty list", testListRunHandlerEmptyList)
	t.Run("streaming in multiple replies", testListRunHandlerStreamingList)
	t.Run("pagination", testListRunHandlerPagination)
	t.Run("not authorized", testListRunHandlerNotAuthorized)
}

func testListRunHandlerExistingRun(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	runs := make([]types.DeploymentRun, 0)
	for i := 0; i < 10; i++ {
		runs = append(runs, types.DeploymentRun{
			ID:         common.NewID(types.RunIDPrefix),
			Deployment: listRunQuery.Deployment,
		})
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		types.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	).Return(runs, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", listRunQuery.SessionActor, listRunQuery.Deployment).Return(true, nil)

	h := ListRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.NoError(t, listRunReply.GetServiceError())
	assert.Len(t, listRunReply.Runs, len(runs))
	assert.Equal(t, 1, listRunReply.ReplyListStream.TotalReplies)
	assert.Equal(t, 0, listRunReply.ReplyListStream.ReplyIndex)
	for i := range listRunReply.Runs {
		assert.Equal(t, runs[i].ID, listRunReply.Runs[i].ID)
		assert.Equal(t, runs[i].Deployment, listRunReply.Runs[i].Deployment)
		assert.Equal(t, runs[i].ConvertToExternal(), listRunReply.Runs[i])
	}
}

func testListRunHandlerEmptyList(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		types.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	).Return(nil, service.NewCacaoNotFoundError("deployment run not found")) // empty list

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", listRunQuery.SessionActor, listRunQuery.Deployment).Return(true, nil)

	h := ListRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.IsType(t, &service.CacaoNotFoundError{}, listRunReply.GetServiceError())
	assert.Len(t, listRunReply.Runs, 0)
	assert.Equal(t, 1, listRunReply.ReplyListStream.TotalReplies)
	assert.Equal(t, 0, listRunReply.ReplyListStream.ReplyIndex)
}

func testListRunHandlerStreamingList(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	runs := make([]types.DeploymentRun, 0)
	runCount := types.DeploymentRunStreamThreshold*5 + rand.Intn(100) + 3
	for i := 0; i < runCount; i++ {
		runs = append(runs, types.DeploymentRun{
			ID:         common.NewID(types.RunIDPrefix),
			Deployment: listRunQuery.Deployment,
		})
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		types.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	).Return(runs, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", listRunQuery.SessionActor, listRunQuery.Deployment).Return(true, nil)

	h := ListRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	expectedRunCount := len(runs) / types.DeploymentRunStreamThreshold
	if len(runs)%types.DeploymentRunStreamThreshold > 0 {
		expectedRunCount++
	}
	if !assert.Len(t, replies, expectedRunCount) {
		return
	}
	for replyIndex, reply := range replies {
		assert.IsType(t, service.DeploymentListRunReply{}, reply)
		listRunReply := reply.(service.DeploymentListRunReply)
		assert.NoError(t, listRunReply.GetServiceError())
		assert.Equal(t, expectedRunCount, listRunReply.ReplyListStream.TotalReplies)
		assert.Equal(t, replyIndex, listRunReply.ReplyListStream.ReplyIndex)
		if replyIndex >= len(replies)-1 {
			assert.Len(t, listRunReply.Runs, len(runs)%types.DeploymentRunStreamThreshold)
		} else {
			assert.Len(t, listRunReply.Runs, types.DeploymentRunStreamThreshold)
		}
	}
}

func testListRunHandlerPagination(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        5,
			PageSizeLimit: 5,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	runs := make([]types.DeploymentRun, 0)
	for i := 0; i < 20; i++ {
		runs = append(runs, types.DeploymentRun{
			ID:         common.NewID(types.RunIDPrefix),
			Deployment: listRunQuery.Deployment,
		})
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		types.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        listRunQuery.Offset,
			PageSizeLimit: listRunQuery.PageSizeLimit,
		},
	).Return(runs[listRunQuery.Offset:listRunQuery.PageSizeLimit+listRunQuery.Offset], nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", listRunQuery.SessionActor, listRunQuery.Deployment).Return(true, nil)

	h := ListRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.NoError(t, listRunReply.GetServiceError())
	assert.Equal(t, listRunReply.Offset, listRunReply.Offset)
	assert.Equal(t, len(listRunReply.Runs), listRunReply.Count)
	assert.Equal(t, listRunQuery.PageSizeLimit, listRunReply.Count)
	assert.Equal(t, 1, listRunReply.ReplyListStream.TotalReplies)
	assert.Equal(t, 0, listRunReply.ReplyListStream.ReplyIndex)
	for i := range listRunReply.Runs {
		assert.Equal(t, runs[i+listRunQuery.Offset].ID, listRunReply.Runs[i].ID)
		assert.Equal(t, runs[i+listRunQuery.Offset].Deployment, listRunReply.Runs[i].Deployment)
		assert.Equal(t, runs[i+listRunQuery.Offset].ConvertToExternal(), listRunReply.Runs[i])
	}
}

func testListRunHandlerNotAuthorized(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        5,
			PageSizeLimit: 5,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var runStorage = &portsmocks.DeploymentRunStorage{}

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", listRunQuery.SessionActor, listRunQuery.Deployment).Return(false, nil)

	h := ListRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.NotNil(t, listRunReply.GetServiceError())
	assert.IsType(t, &service.CacaoUnauthorizedError{}, listRunReply.GetServiceError())
	assert.Equal(t, 1, listRunReply.ReplyListStream.TotalReplies)
	assert.Equal(t, 0, listRunReply.ReplyListStream.ReplyIndex)
	assert.Empty(t, listRunReply.Runs)
}
