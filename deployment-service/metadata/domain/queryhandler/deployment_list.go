package queryhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// ListQueryHandler is handler for list query
type ListQueryHandler struct {
	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	HandlerCommon
}

// NewListQueryHandler ...
func NewListQueryHandler(
	storage ports.DeploymentStorage,
	runStorage ports.DeploymentRunStorage,
	commonDependencies HandlerCommon,
) ListQueryHandler {
	return ListQueryHandler{
		storage:       storage,
		runStorage:    runStorage,
		HandlerCommon: commonDependencies,
	}
}

// QueryOp ...
func (h ListQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentListQueryType
}

// Handle handles the query
func (h ListQueryHandler) Handle(query types.Query) Reply {
	panic("not supported")
}

// HandleStream handles the query
func (h ListQueryHandler) HandleStream(query types.Query) []Reply {
	logger := log.WithFields(log.Fields{
		"query": "list",
	})
	var listQuery service.DeploymentListQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &listQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentListQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}
	logger = logger.WithFields(log.Fields{
		"filters":  listQuery.Filters,
		"actor":    listQuery.SessionActor,
		"emulator": listQuery.SessionEmulator,
	})

	sort, err := types.FromExternalDeploymentSort(listQuery.SortBy, listQuery.SortDirection)
	if err != nil {
		errorMessage := "invalid sort option in request"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoInvalidParameterError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}
	deployments, err := h.storage.List(
		h.convertFilter(listQuery),
		int64(listQuery.Offset),
		int64(listQuery.Limit),
		sort,
	)
	if err != nil {
		if _, ok := err.(*service.CacaoNotFoundError); ok {
			return []Reply{h.createEmptyListReply()}
		}

		return h.createErrorReply(err)
	}
	logger.WithField("len", len(deployments)).Debug()

	var lastRunsMap map[common.ID]types.DeploymentRun
	if listQuery.FullRunObject {
		// only fetch last runs if the FullRunObject flag is set
		lastRunsMap, err = h.fetchLastRuns(deployments)
		if err != nil {
			return h.createErrorReply(err)
		}
	}

	extDeployments, err := h.convertDeploymentListToExternal(deployments, lastRunsMap)
	if err != nil {
		return h.createErrorReply(err)
	}
	return h.createReplies(listQuery.Offset, extDeployments)
}

func (h ListQueryHandler) convertFilter(listQuery service.DeploymentListQuery) types.DeploymentFilter {
	filters := types.DeploymentFilter{
		// Only return deployments created by user
		Creator:              listQuery.GetSessionActor(),
		Template:             listQuery.Filters.Template,
		TemplateSet:          listQuery.Filters.TemplateSet,
		Workspace:            listQuery.Filters.Workspace,
		PrimaryCloudProvider: listQuery.Filters.PrimaryCloudProvider,
		// TODO LastRunStatus cannot be filtered in deployment storage, it should be passed to run storage (DeploymentID in set{deployment IDs from other deployment filters} && Status == LastRunStatus)
		//LastRunStatus:        listQuery.Filters.LastRunStatus,
		Credential: deploymentcommon.CredentialID(listQuery.Filters.CredentialID),
	}
	if !listQuery.Filters.AllCurrentStatus {
		if listQuery.Filters.CurrentStatus.Valid() {
			filters.CurrentStatus = []service.DeploymentStatus{listQuery.Filters.CurrentStatus}
		} else {
			// By default, hides deleted deployments
			filters.CurrentStatus = []service.DeploymentStatus{
				service.DeploymentStatusNone,
				service.DeploymentStatusActive,
				service.DeploymentStatusCreationErrored,
				service.DeploymentStatusDeletionErrored,
			}
		}
	}
	if listQuery.Filters.PendingStatus.Valid() {
		filters.PendingStatus = []service.DeploymentPendingStatus{listQuery.Filters.PendingStatus}
	}

	return filters
}

// fetch last run for every deployment in the list if the deployment has a last run, returns a deploymentID-DeploymentRun map
func (h ListQueryHandler) fetchLastRuns(deployments []types.Deployment) (map[common.ID]types.DeploymentRun, error) {
	lastRunIDs := make([]common.ID, 0, len(deployments))
	for _, deployment := range deployments {
		if deployment.HasLastRun() {
			lastRunIDs = append(lastRunIDs, *deployment.LastRun)
		}
	}
	lastRunList, err := h.runStorage.BatchGet(lastRunIDs)
	if err != nil {
		return nil, err
	}

	// map[deploymentID]DeploymentRun
	deploymentLastRuns := make(map[common.ID]types.DeploymentRun, len(deployments))
	for _, run := range lastRunList {
		deploymentLastRuns[run.Deployment] = run
	}
	return deploymentLastRuns, nil
}

func (h ListQueryHandler) createErrorReply(err error) []Reply {
	var reply service.DeploymentListReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return []Reply{reply}
}

func (h ListQueryHandler) createReplies(
	initialOffset int,
	deployments []service.Deployment,
) []Reply {
	if len(deployments) == 0 {
		return []Reply{h.createEmptyListReply()}
	} else if len(deployments) < types.DeploymentStreamThreshold {
		return []Reply{h.singleReply(initialOffset, deployments)}
	} else {
		return h.multipleReplies(initialOffset, deployments)
	}
}

func (h ListQueryHandler) createEmptyListReply() service.DeploymentListReply {
	return service.DeploymentListReply{
		Session:     service.Session{},
		Offset:      0,
		Count:       0,
		Deployments: make([]service.Deployment, 0),
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListQueryHandler) singleReply(initialOffset int, deployments []service.Deployment) service.DeploymentListReply {
	return service.DeploymentListReply{
		Session:     service.Session{},
		Offset:      initialOffset,
		Count:       len(deployments),
		Deployments: deployments,
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListQueryHandler) multipleReplies(initialOffset int, deployments []service.Deployment) []Reply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ListQueryHandler.multipleReplies",
	})
	allReplies := make([]Reply, 0)

	allChunks := h.divideList(deployments)
	offsetCounter := initialOffset
	for chunkIndex, chunk := range allChunks {
		reply := service.DeploymentListReply{
			Session:     service.Session{},
			Offset:      offsetCounter,
			Count:       len(chunk),
			Deployments: chunk,
			ReplyListStream: service.ReplyListStream{
				TotalReplies: len(allChunks),
				ReplyIndex:   chunkIndex,
			},
		}
		allReplies = append(allReplies, reply)
	}
	logger.WithField("totalReplies", len(allReplies)).Debug("large deployment list divided into multiple replies")
	return allReplies
}

func (h ListQueryHandler) divideList(deployments []service.Deployment) [][]service.Deployment {
	if len(deployments) <= types.DeploymentStreamThreshold {
		return [][]service.Deployment{deployments}
	}
	var allChunks = make([][]service.Deployment, 0)
	var chunk []service.Deployment
	var start = 0
	var end = types.DeploymentStreamThreshold
	for start < len(deployments) {
		chunk = deployments[start:end]
		allChunks = append(allChunks, chunk)
		start = end
		if start+types.DeploymentStreamThreshold >= len(deployments) {
			end = len(deployments)
		} else {
			end = start + types.DeploymentStreamThreshold
		}
	}
	return allChunks
}

// convert a list of deployments
func (h ListQueryHandler) convertDeploymentListToExternal(
	deployments []types.Deployment,
	lastRunsMap map[common.ID]types.DeploymentRun,
) ([]service.Deployment, error) {
	extDeployments := make([]service.Deployment, 0, len(deployments))
	for _, deployment := range deployments {
		external, err := h.convertDeploymentToExternal(deployment, lastRunsMap)
		if err != nil {
			return nil, err
		}
		extDeployments = append(extDeployments, external)
	}
	return extDeployments, nil
}

// convert a single deployment
func (h ListQueryHandler) convertDeploymentToExternal(
	deployment types.Deployment,
	lastRunsMap map[common.ID]types.DeploymentRun,
) (service.Deployment, error) {
	var lastRun types.DeploymentRun
	var err error
	if deployment.HasLastRun() {
		if lastRunsMap == nil {
			// if lastRunsMap is not provided, then only include the Run ID in lastRun
			lastRun = types.DeploymentRun{ID: *deployment.LastRun}
		} else {
			lastRun, err = h.lookupLastRun(lastRunsMap, &deployment)
			if err != nil {
				return service.Deployment{}, err
			}
		}

	}
	return deploymentConvertToExternal(deployment, lastRun), nil
}

func (h ListQueryHandler) lookupLastRun(
	lastRunsMap map[common.ID]types.DeploymentRun,
	deployment *types.Deployment,
) (types.DeploymentRun, error) {
	var lastRun types.DeploymentRun
	var ok bool
	if deployment.HasLastRun() {
		lastRun, ok = lastRunsMap[deployment.ID]
		if !ok {
			// this should not happen, for deployment that has a last run, the run should be in the lastRunsMap
			log.WithFields(log.Fields{
				"package":    "domain",
				"function":   "ListQueryHandler.convertDeploymentListToExternal",
				"deployment": deployment.ID,
			}).Errorf("last run for deployment is missing from last run map")
			return lastRun, service.NewCacaoGeneralError(fmt.Sprintf("last run missing from the map for deployment %s", deployment.ID))
		}
	}
	return lastRun, nil
}
