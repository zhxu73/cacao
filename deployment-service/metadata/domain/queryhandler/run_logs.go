package queryhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// GetLogsQueryHandler handles GetLogs query
type GetLogsQueryHandler struct {
	storage ports.DeploymentRunStorage
	HandlerCommon
}

// NewGetLogsQueryHandler ...
func NewGetLogsQueryHandler(
	runStorage ports.DeploymentRunStorage,
	commonDependencies HandlerCommon,
) GetLogsQueryHandler {
	return GetLogsQueryHandler{
		storage:       runStorage,
		HandlerCommon: commonDependencies,
	}
}

// QueryOp ...
func (h GetLogsQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetLogsQueryType
}

// Handle handles the query
func (h GetLogsQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "GetLogsQueryHandler.Handle",
	})
	var getLogsQuery service.DeploymentGetLogsQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getLogsQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetLogsQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}

	run, err := h.storage.Get(getLogsQuery.Run)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(err)
	}

	ok, err := h.Perm.DeploymentAccessByID(getLogsQuery.GetSessionActor(), run.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	if !run.HasLogs() {
		return h.createErrorReply(service.NewCacaoNotFoundError("deployment logs not available"))
	}

	// FIXME logs needs to be fetched from else where (not storage of this microservice)
	panic("FIXME")
	//return h.createReply(*run.Logs)
}

// HandleStream handles the query
func (h GetLogsQueryHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

func (h GetLogsQueryHandler) createErrorReply(err error) service.DeploymentGetLogsReply {
	var reply service.DeploymentGetLogsReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return reply
}

func (h GetLogsQueryHandler) createReply(logs string) service.DeploymentGetLogsReply {
	return service.DeploymentGetLogsReply{
		Session: service.Session{},
		Logs:    logs,
	}
}
