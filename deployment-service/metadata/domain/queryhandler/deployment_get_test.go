package queryhandler

import (
	"testing"
	"time"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	domainmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/domain/mocks"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	typesmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/types/mocks"
)

// NonDeletedStatus are status that is not deleted.
var NonDeletedStatus = []service.DeploymentStatus{
	service.DeploymentStatusNone,
	service.DeploymentStatusActive,
	service.DeploymentStatusCreationErrored,
	service.DeploymentStatusDeletionErrored,
}

func TestGetQueryHandler_Handle(t *testing.T) {
	t.Run("existing deployment w/o last run", testGetHandlerExistingDeployment)
	t.Run("existing deployment with last run", testGetHandlerExistingDeploymentWithLastRun)
	t.Run("deployment not found", testGetHandlerNotFound)
	t.Run("not authorized", testGetHandlerNotAuthorized)
	t.Run("empty request", testGetHandlerEmptyRequest)
}

func testGetHandlerExistingDeployment(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployment := types.Deployment{
		ID: getQuery.ID,
		CreatedBy: deploymentcommon.Creator{
			User:     getQuery.GetSessionActor(),
			Emulator: "",
		},
		Template:  common.NewID("template"),
		Workspace: common.NewID("workspace"),
		LastRun:   nil,
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", getQuery.ID).Return(deployment, nil)

	var runStorage = &portsmocks.DeploymentRunStorage{}

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccess", getQuery.GetSessionActor(), deployment).Return(true, nil)

	h := GetQueryHandler{
		storage:    deploymentStorage,
		runStorage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.NoError(t, getReply.GetServiceError())
	assert.Equal(t, deployment.ID, getReply.Deployment.ID)
	assert.Equal(t, deployment.Template, getReply.Deployment.Template)
	assert.Equal(t, deployment.Workspace, getReply.Deployment.Workspace)
}

func testGetHandlerExistingDeploymentWithLastRun(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	lastRun := types.DeploymentRun{
		ID:         common.NewID(service.RunIDPrefix),
		Deployment: getQuery.ID,
		CreatedBy: deploymentcommon.Creator{
			User:     getQuery.GetSessionActor(),
			Emulator: "",
		},
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           deploymentcommon.DeploymentRunPreflight,
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
	}

	deployment := types.Deployment{
		ID: getQuery.ID,
		CreatedBy: deploymentcommon.Creator{
			User:     getQuery.GetSessionActor(),
			Emulator: "",
		},
		Template:  common.NewID("template"),
		Workspace: common.NewID("workspace"),
		LastRun:   &lastRun.ID,
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", getQuery.ID).Return(deployment, nil)

	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", lastRun.ID).Return(lastRun, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccess", getQuery.GetSessionActor(), deployment).Return(true, nil)

	h := GetQueryHandler{
		storage:    deploymentStorage,
		runStorage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.NoError(t, getReply.GetServiceError())
	assert.Equal(t, deployment.ID, getReply.Deployment.ID)
	assert.Equal(t, deployment.Template, getReply.Deployment.Template)
	assert.Equal(t, deployment.Workspace, getReply.Deployment.Workspace)
}

func testGetHandlerNotFound(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", getQuery.ID).Return(types.Deployment{}, service.NewCacaoNotFoundError("deployment not found"))

	var runStorage = &portsmocks.DeploymentRunStorage{}

	var perm = &domainmocks.PermissionChecker{}

	h := GetQueryHandler{
		storage:    deploymentStorage,
		runStorage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Error(t, getReply.GetServiceError())
	assert.IsType(t, &service.CacaoNotFoundError{}, getReply.GetServiceError())
}

func testGetHandlerNotAuthorized(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployment := types.Deployment{
		ID: getQuery.ID,
		CreatedBy: deploymentcommon.Creator{
			User:     getQuery.GetSessionActor(),
			Emulator: "",
		},
		Template:  common.NewID("template"),
		Workspace: common.NewID("workspace"),
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", deployment.ID).Return(deployment, nil)

	var runStorage = &portsmocks.DeploymentRunStorage{}

	var perm = &domainmocks.PermissionChecker{}
	perm.On(
		"DeploymentAccess",
		getQuery.GetSessionActor(),
		deployment,
	).Return(false, nil) // not authorized

	h := GetQueryHandler{
		storage:    deploymentStorage,
		runStorage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Error(t, getReply.GetServiceError())
	assert.IsType(t, &service.CacaoUnauthorizedError{}, getReply.GetServiceError())
}

func testGetHandlerEmptyRequest(t *testing.T) {
	getQuery := service.DeploymentGetQuery{} // empty request
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", common.ID("")).Return(types.Deployment{}, service.NewCacaoNotFoundError("deployment not found"))

	var runStorage = &portsmocks.DeploymentRunStorage{}

	var perm = &domainmocks.PermissionChecker{}

	h := GetQueryHandler{
		storage:    deploymentStorage,
		runStorage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Error(t, getReply.GetServiceError())
	assert.IsType(t, &service.CacaoNotFoundError{}, getReply.GetServiceError())
}
