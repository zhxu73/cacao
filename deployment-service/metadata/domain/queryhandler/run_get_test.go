package queryhandler

import (
	"testing"
	"time"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	domainmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/domain/mocks"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	typesmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/types/mocks"
)

func TestGetRunHandler_Handle(t *testing.T) {
	t.Run("existing run", testGetRunHandlerExistingRun)
	t.Run("not found", testGetRunHandlerRunNotFound)
	t.Run("not authorized", testGetRunHandlerNotAuthorized)
}

func testGetRunHandlerExistingRun(t *testing.T) {
	getRunQuery := service.DeploymentGetRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := types.DeploymentRun{
		ID:               getRunQuery.Run,
		Deployment:       getRunQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRunQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", getRunQuery.GetSessionActor(), run.Deployment).Return(true, nil)

	h := GetRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRunReply{}, reply)
	getRunReply := reply.(service.DeploymentGetRunReply)
	assert.NoError(t, getRunReply.GetServiceError())
	assert.Equal(t, run.ID, getRunReply.Run.ID)
	assert.Equal(t, run.Deployment, getRunReply.Deployment)
	assert.Equal(t, run.Deployment, getRunReply.Run.Deployment)
	assert.Equal(t, run.ConvertToExternal(), getRunReply.Run)
}

func testGetRunHandlerRunNotFound(t *testing.T) {
	getRunQuery := service.DeploymentGetRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRunQuery.Run).Return(types.DeploymentRun{}, service.NewCacaoNotFoundError("deployment run not found"))

	var perm = &domainmocks.PermissionChecker{}

	h := GetRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRunReply{}, reply)
	getRunReply := reply.(service.DeploymentGetRunReply)
	assert.IsType(t, &service.CacaoNotFoundError{}, getRunReply.GetServiceError())
}

func testGetRunHandlerNotAuthorized(t *testing.T) {
	getRunQuery := service.DeploymentGetRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := types.DeploymentRun{
		ID:               getRunQuery.Run,
		Deployment:       getRunQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRunQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On(
		"DeploymentAccessByID",
		getRunQuery.GetSessionActor(),
		run.Deployment,
	).Return(false, nil) // not authorized

	h := GetRunHandler{
		storage: runStorage,
		HandlerCommon: HandlerCommon{
			Perm:     perm,
			ClientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRunReply{}, reply)
	getRunReply := reply.(service.DeploymentGetRunReply)
	assert.IsType(t, &service.CacaoUnauthorizedError{}, getRunReply.GetServiceError())
}
