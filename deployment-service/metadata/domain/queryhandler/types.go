package queryhandler

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// Reply is reply to query
type Reply interface {
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// QueryHandler handles a particular type of query, and generate a replies for query
type QueryHandler interface {
	Handle(query types.Query) Reply
	HandleStream(query types.Query) []Reply
	QueryOp() common.QueryOp
}

// HandlerCommon common dependencies for query handler
type HandlerCommon struct {
	Perm     PermissionChecker
	ClientID string
}

// PermissionChecker checks if user has permission to access certain object
type PermissionChecker interface {
	// check if a user has access to a deployment
	DeploymentAccess(user string, d types.Deployment) (bool, error)
	// check if a user has access to a deployment
	DeploymentAccessByID(user string, deployment common.ID) (bool, error)
}

type permChecker struct {
	storage ports.DeploymentStorage
}

// NewPermissionCheck creates a new permission checker
func NewPermissionCheck(storage ports.DeploymentStorage) PermissionChecker {
	return permChecker{storage: storage}
}

// DeploymentAccess checks for permission on deployment access
func (c permChecker) DeploymentAccess(user string, d types.Deployment) (bool, error) {
	// TODO creator of deployment are not necessarily always able to access deployment.
	// e.g. they could be removed from accessing the underlying workspace.
	if user == d.CreatedBy.User {
		return true, nil
	}
	return false, nil
	//return c.WorkspaceAccess(user, deployment.Workspace)
}

// DeploymentAccessByID checks for permission on deployment access
func (c permChecker) DeploymentAccessByID(user string, deploymentID common.ID) (bool, error) {
	deployment, err := c.storage.Get(deploymentID)
	if err != nil {
		return false, err
	}
	return c.DeploymentAccess(user, deployment)
}
