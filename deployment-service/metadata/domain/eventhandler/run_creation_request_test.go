package eventhandler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"reflect"
	"testing"
	"time"
)

func Test_runFactory_CreateRun1(t *testing.T) {
	type args struct {
		runID   common.ID
		request service.DeploymentCreateRunRequest
		data    runCreationPrerequisiteData
		now     time.Time
	}
	tests := []struct {
		name    string
		args    args
		want    types.DeploymentRun
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{ // output should use Source from TemplateVersion instead of Template
							Type: "git",
							URI:  "github.com/cyverse/bbbbbbbbbbbbbbbbb",
							AccessParameters: map[string]interface{}{
								"branch": "bbbbbbbbbbbbbbbbb",
								"path":   "/bbbbbbbbbbbbbbbbb",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want: types.DeploymentRun{
				ID:         "run-d8caa0598850n9abikmg",
				Deployment: "deployment-d600t0598850n9abikm0",
				CreatedBy: deploymentcommon.Creator{
					User:     "testuser123",
					Emulator: "",
				},
				CreatedAt: time.Unix(1600000000, 0),
				EndsAt:    time.Time{},
				TemplateSnapshot: deploymentcommon.TemplateSnapshot{
					TemplateID:        "template-cnm8f0598850n9abikj0",
					TemplateVersionID: "templateversion-cur4m0598850n9abikkg",
					UpstreamTracked: deploymentcommon.GitUpstream{
						Branch: "main",
					},
					GitURL:     "github.com/cyverse/foobar",
					CommitHash: "",
					SubPath:    "/foo",
				},
				RequestParameters: map[string]interface{}{
					"key1": "val1",
					"key2": 222,
				},
				Status:    deploymentcommon.DeploymentRunPendingCreation,
				StatusMsg: "",
			},
			wantErr: false,
		},
		{
			name: "deployment missing workspace",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "", // missing
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "inconsistent template ID",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cq2hs0598850n9abikjg",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "inconsistent provider ID",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-co1g2rt9885fie78ffj0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "diff creator, no permission",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "diff_user123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "bad status",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusDeleted,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "template missing source type",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "diff_user123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "", // missing
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "template missing URI",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "diff_user123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "", // missing
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "template missing access param",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "diff_user123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type:             "git",
							URI:              "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{}, // missing
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "deleted template",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Deleted: true, // template is soft-deleted
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/bbbbbbbbbbbbbbbbb",
							AccessParameters: map[string]interface{}{
								"branch": "bbbbbbbbbbbbbbbbb",
								"path":   "/bbbbbbbbbbbbbbbbb",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "disabled template version",
			args: args{
				runID: "run-d8caa0598850n9abikmg",
				request: service.DeploymentCreateRunRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					CreateParam: service.DeploymentRunCreateParam{
						Deployment: "deployment-d600t0598850n9abikm0",
						ParamValues: map[string]interface{}{
							"key1": "val1",
							"key2": 222,
						},
					},
				},
				data: runCreationPrerequisiteData{
					Deployment: types.Deployment{
						ID:        "deployment-d600t0598850n9abikm0",
						Name:      "",
						Workspace: "workspace-d3jng0598850n9abiklg",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						Template:             "template-cnm8f0598850n9abikj0",
						PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
						CurrentStatus:        service.DeploymentStatusNone,
						PendingStatus:        service.DeploymentStatusNoPending,
					},
					Provider: &service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
					Template: &service.TemplateModel{
						Session: service.Session{},
						ID:      "template-cnm8f0598850n9abikj0",
						Public:  true,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/bbbbbbbbbbbbbbbbb",
							AccessParameters: map[string]interface{}{
								"branch": "bbbbbbbbbbbbbbbbb",
								"path":   "/bbbbbbbbbbbbbbbbb",
							},
						},
					},
					LatestTemplateVersion: service.TemplateVersion{
						ID:         "templateversion-cur4m0598850n9abikkg",
						TemplateID: "template-cnm8f0598850n9abikj0",
						Disabled:   true, // the template version is disabled
						Source: service.TemplateSource{
							Type: "git",
							URI:  "github.com/cyverse/foobar",
							AccessParameters: map[string]interface{}{
								"branch": "main",
								"path":   "/foo",
							},
						},
					},
				},
				now: time.Unix(1600000000, 0),
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &runFactory{}
			got, err := s.CreateRun(tt.args.runID, tt.args.request, tt.args.data, tt.args.now)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateRun() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateRun() got = %v, want %v", got, tt.want)
			}
		})
	}
}
