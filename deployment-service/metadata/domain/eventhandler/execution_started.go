package eventhandler

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// ExecutionStartedHandler handles when then execution stage has started
type ExecutionStartedHandler struct {
	runStorage ports.DeploymentRunStorage
	timeSrc    ports.TimeSrc
}

// NewExecutionStartedHandler ...
func NewExecutionStartedHandler(portsDependency ports.Ports) ExecutionStartedHandler {
	return ExecutionStartedHandler{
		runStorage: portsDependency.RunStorage,
		timeSrc:    portsDependency.TimeSrc,
	}
}

// Handle ...
func (h ExecutionStartedHandler) Handle(result deploymentevents.RunPreflightResult, sink ports.OutgoingEventSink) {
	updatedEventBody := h.handle(result)
	if updatedEventBody != nil {
		sink.EventDeploymentRunStatusUpdated(*updatedEventBody)
	}
}

func (h ExecutionStartedHandler) handle(event deploymentevents.RunPreflightResult) *deploymentevents.DeploymentRunStatusUpdated {
	logger := log.WithFields(log.Fields{
		"package":      "eventhandler",
		"function":     "ExecutionStartedHandler.handle",
		"deployment":   event.Deployment,
		"templateType": event.TemplateType,
		"run":          event.Run,
	})
	oldStatus, err := h.getOldStatus(event)
	if err != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err).Errorf("fail to fetch run %s to get its status, this does not prevent emitting %s event", deploymentcommon.DeploymentRunRunning, deploymentevents.EventDeploymentRunStatusUpdated)
	} else if oldStatus != deploymentcommon.DeploymentRunPreflight {
		logger.WithField("runStatus", oldStatus).Warn("current status of run is not preflight when execution started")
	}
	err2 := h.updateRunStatus(event)
	if err2 != nil {
		logger.WithError(err2).Errorf("fail to update run status to %s", deploymentcommon.DeploymentRunRunning)
		// for the sake of consistency (run status in storage, and event gateway), skip emitting the DeploymentRunStatusUpdated event.
		// since DeploymentRunRunning is not a terminal status for run, it is okay to skip.
		return nil
	}
	eventBody := h.runStatusUpdatedEvent(event, oldStatus, err)
	return &eventBody
}

func (h ExecutionStartedHandler) getOldStatus(event deploymentevents.RunPreflightResult) (deploymentcommon.DeploymentRunStatus, error) {
	run, err := h.runStorage.Get(event.Run)
	if err != nil {
		// Note that the "" status will be used in the DeploymentRunStatusUpdated event when fail to fetch run
		return "", err
	}
	return run.Status, nil
}

func (h ExecutionStartedHandler) updateRunStatus(event deploymentevents.RunPreflightResult) error {
	newStatus := deploymentcommon.DeploymentRunRunning
	runUpdate := types.DeploymentRunUpdate{Status: &newStatus}
	updated, err := h.runStorage.Update(event.Run, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find run %s in storage, fail to update status", event.Run)
	}
	return nil
}

func (h ExecutionStartedHandler) runStatusUpdatedEvent(incoming deploymentevents.RunPreflightResult, oldStatus deploymentcommon.DeploymentRunStatus, err error) deploymentevents.DeploymentRunStatusUpdated {
	var svcErr service.CacaoErrorBase
	if err != nil {
		svcErr = types.ErrorToServiceError(err).GetBase()
	}
	eventBody := deploymentevents.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    incoming.SessionActor,
			SessionEmulator: incoming.SessionEmulator,
			ServiceError:    svcErr,
		},
		Deployment: incoming.Deployment,
		Run:        incoming.Run,
		Status:     deploymentcommon.DeploymentRunRunning,
		OldStatus:  oldStatus,
	}
	return eventBody
}
