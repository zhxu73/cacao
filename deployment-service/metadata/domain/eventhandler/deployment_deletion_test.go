package eventhandler

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"reflect"
	"testing"
	"time"
)

// IncomingEvent ...
type IncomingEvent struct {
	ce cloudevents.Event
}

// NewIncomingEvent ...
func NewIncomingEvent(eventType common.EventType, event interface{}) IncomingEvent {
	ce, err := messaging.CreateCloudEventWithTransactionID(event, string(eventType), "test", messaging.NewTransactionID())
	if err != nil {
		panic(err)
	}
	return IncomingEvent{ce: ce}
}

func (i IncomingEvent) EventType() common.EventType {
	return common.EventType(i.ce.Type())
}

func (i IncomingEvent) Transaction() common.TransactionID {
	return messaging.GetTransactionID(&i.ce)
}

func (i IncomingEvent) CloudEvent() cloudevents.Event {
	return i.ce
}

type TestTimeSrc struct {
	Timestamp time.Time
}

func (t TestTimeSrc) Now() time.Time {
	return t.Timestamp
}

func TestDeletionHandler_Handle(t *testing.T) {
	//t.Run("no run", testDeleteStandAloneDeployment) // TODO need workaround for mock after pending status update, basically DeploymentStorage mocks need to return a different Deployment for Get() after pending status update
	t.Run("not authorized", testDeleteDeploymentNotAuthorized)
	t.Run("conflict status", testDeleteDeploymentDifferentConflictStatus)
	//t.Run("with run & resource", testDeleteDeploymentWithRunAndResource) // TODO need workaround for mock after pending status update
}

func testDeleteStandAloneDeployment(t *testing.T) {
	var req = service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    "testuser123",
			SessionEmulator: "",
		},
		ID: common.NewID("deployment"),
	}
	var deployment = types.Deployment{
		ID:                   req.ID,
		CreatedBy:            deploymentcommon.Creator{User: "testuser123"},
		Template:             common.NewID("template"),
		TemplateType:         "terraform_openstack",
		PrimaryCloudProvider: common.NewID("provider"),
		CurrentStatus:        service.DeploymentStatusActive,
		PendingStatus:        service.DeploymentStatusNoPending,
		LastRun:              nil,
	}

	timeSrc := TestTimeSrc{time.Now()}
	deploymentStorage := &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", req.ID).Return(deployment, nil)
	update, filter := types.NewDeploymentStatusUpdater(timeSrc.Now()).PendingStatusDeleting()
	deploymentStorage.On("Update", req.ID,
		mock.MatchedBy(func(param types.DeploymentUpdate) bool { return reflect.DeepEqual(update, param) }),
		mock.MatchedBy(func(param types.DeploymentFilter) bool { return reflect.DeepEqual(filter, param) }),
	).Return(true, nil)

	update2, filter2 := types.NewDeploymentStatusUpdater(timeSrc.Now()).StatusDeleted()
	deploymentStorage.On("Update", req.ID,
		mock.MatchedBy(func(param types.DeploymentUpdate) bool { return reflect.DeepEqual(update2, param) }),
		mock.MatchedBy(func(param types.DeploymentFilter) bool { return reflect.DeepEqual(filter2, param) }),
	).Return(true, nil)

	runStorage := &portsmocks.DeploymentRunStorage{}

	testDeletionHandler(t,
		DeletionHandler{
			deploymentStorage: deploymentStorage,
			runStorage:        runStorage,
			timeSrc:           timeSrc,
		},
		req,
		service.DeploymentDeleted,
		nil,
	)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
}

func testDeleteDeploymentNotAuthorized(t *testing.T) {
	var req = service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    "testuser123",
			SessionEmulator: "",
		},
		ID: common.NewID("deployment"),
	}
	var deployment = types.Deployment{
		ID:                   req.ID,
		CreatedBy:            deploymentcommon.Creator{User: "DifferentUser"}, // different user, not authorized
		Template:             common.NewID("template"),
		TemplateType:         "terraform_openstack",
		PrimaryCloudProvider: common.NewID("provider"),
		CurrentStatus:        service.DeploymentStatusActive,
		PendingStatus:        service.DeploymentStatusNoPending,
		LastRun:              nil,
	}

	timeSrc := TestTimeSrc{time.Now()}
	deploymentStorage := &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", req.ID).Return(deployment, nil)

	runStorage := &portsmocks.DeploymentRunStorage{}

	testDeletionHandler(t,
		DeletionHandler{
			deploymentStorage: deploymentStorage,
			runStorage:        runStorage,
			timeSrc:           timeSrc,
		},
		req,
		service.DeploymentDeleteFailed,
		service.NewCacaoUnauthorizedError("unauthorized access to deployment"),
	)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
}

func testDeleteDeploymentDifferentConflictStatus(t *testing.T) {
	t.Run("active&creating", func(t *testing.T) {
		testDeleteDeploymentConflictStatus(t, service.DeploymentStatusActive, service.DeploymentStatusCreating, service.NewCacaoInvalidParameterError("deployment cannot be deleted when pending status is not none"))
	})
	t.Run("active&deleting", func(t *testing.T) {
		testDeleteDeploymentConflictStatus(t, service.DeploymentStatusActive, service.DeploymentStatusDeleting, service.NewCacaoInvalidParameterError("deployment cannot be deleted when pending status is not none"))
	})
	t.Run("deleted&none", func(t *testing.T) {
		testDeleteDeploymentConflictStatus(t, service.DeploymentStatusDeleted, service.DeploymentStatusNoPending, service.NewCacaoInvalidParameterError("current status does not allow deletion"))
	})
	t.Run("none&deleting", func(t *testing.T) {
		testDeleteDeploymentConflictStatus(t, service.DeploymentStatusNone, service.DeploymentStatusDeleting, service.NewCacaoInvalidParameterError("deployment cannot be deleted when pending status is not none"))
	})
	t.Run("none&creating", func(t *testing.T) {
		testDeleteDeploymentConflictStatus(t, service.DeploymentStatusNone, service.DeploymentStatusCreating, service.NewCacaoInvalidParameterError("deployment cannot be deleted when pending status is not none"))
	})
	t.Run("creation_errored&deleting", func(t *testing.T) {
		testDeleteDeploymentConflictStatus(t, service.DeploymentStatusCreationErrored, service.DeploymentStatusDeleting, service.NewCacaoInvalidParameterError("deployment cannot be deleted when pending status is not none"))
	})
}

func testDeleteDeploymentConflictStatus(t *testing.T, currStatus service.DeploymentStatus, pendingStatus service.DeploymentPendingStatus, cacaoError service.CacaoError) {
	var req = service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    "testuser123",
			SessionEmulator: "",
		},
		ID: common.NewID("deployment"),
	}
	var deployment = types.Deployment{
		ID:                   req.ID,
		CreatedBy:            deploymentcommon.Creator{User: "testuser123"},
		Template:             common.NewID("template"),
		TemplateType:         "terraform_openstack",
		PrimaryCloudProvider: common.NewID("provider"),
		CurrentStatus:        currStatus,
		PendingStatus:        pendingStatus,
		LastRun:              nil,
	}

	timeSrc := TestTimeSrc{time.Now()}
	deploymentStorage := &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", req.ID).Return(deployment, nil)

	runStorage := &portsmocks.DeploymentRunStorage{}

	testDeletionHandler(t,
		DeletionHandler{
			deploymentStorage: deploymentStorage,
			runStorage:        runStorage,
			timeSrc:           timeSrc,
		},
		req,
		service.DeploymentDeleteFailed,
		cacaoError,
	)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
}

func testDeleteDeploymentWithRunAndResource(t *testing.T) {
	var req = service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    "testuser123",
			SessionEmulator: "",
		},
		ID: common.NewID("deployment"),
	}
	var runID = common.NewID("run")
	var deployment = types.Deployment{
		ID:                   req.ID,
		CreatedBy:            deploymentcommon.Creator{User: "testuser123"},
		Template:             common.NewID("template"),
		TemplateType:         "terraform_openstack",
		PrimaryCloudProvider: common.NewID("provider"),
		CurrentStatus:        service.DeploymentStatusActive,
		PendingStatus:        service.DeploymentStatusNoPending,
		LastRun:              &runID,
	}
	var run = types.DeploymentRun{
		ID:         runID,
		Deployment: deployment.ID,
		LastState: deploymentcommon.DeploymentStateView{
			Resources: []service.DeploymentResource{
				{
					ID: "foobar",
				},
			},
		},
	}

	timeSrc := TestTimeSrc{time.Now()}
	deploymentStorage := &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", req.ID).Return(deployment, nil)
	update, filter := types.NewDeploymentStatusUpdater(timeSrc.Now()).PendingStatusDeleting()
	deploymentStorage.On("Update", req.ID,
		mock.MatchedBy(func(param types.DeploymentUpdate) bool { return reflect.DeepEqual(update, param) }),
		mock.MatchedBy(func(param types.DeploymentFilter) bool { return reflect.DeepEqual(filter, param) }),
	).Return(true, nil)

	runStorage := &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", runID).Return(run, nil)

	testDeletionHandler(t,
		DeletionHandler{
			deploymentStorage: deploymentStorage,
			runStorage:        runStorage,
			timeSrc:           timeSrc,
		},
		req,
		service.DeploymentDeletionStarted,
		nil,
	)
	deploymentStorage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
}

func testDeletionHandler(t *testing.T, h DeletionHandler, req service.DeploymentDeletionRequest, respEventType common.EventType, cacaoError service.CacaoError) {
	out := &portsmocks.OutgoingEventSink{}
	switch respEventType {
	case service.DeploymentDeletionStarted:
		out.On("DeploymentDeletionStarted", service.DeploymentDeletionResult{
			Session: service.Session{
				SessionActor:    req.SessionActor,
				SessionEmulator: req.SessionEmulator,
			},
			ID:           req.ID,
			TemplateType: "",
		}).Once()
	case service.DeploymentDeleteFailed:
		out.On("DeploymentDeleteFailed", service.DeploymentDeletionResult{
			Session: service.Session{
				SessionActor:    req.SessionActor,
				SessionEmulator: req.SessionEmulator,
				ServiceError:    cacaoError.GetBase(),
			},
			ID:           req.ID,
			TemplateType: "",
		}).Once()
	case service.DeploymentDeleted:
		out.On("DeploymentDeleted", service.DeploymentDeletionResult{
			Session: service.Session{
				SessionActor:    req.SessionActor,
				SessionEmulator: req.SessionEmulator,
			},
			ID:           req.ID,
			TemplateType: "",
		}).Once()
	}

	h.Handle(req, out)
	out.AssertExpectations(t)
}

func Test_deletionChecker_Setter(t *testing.T) {
	t.Run("request", func(t *testing.T) {
		var checker deletionChecker
		request := service.DeploymentDeletionRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			ID: "deployment-d600t0598850n9abikm0",
		}
		assert.NoError(t, checker.SetRequest(request))
		assert.Equal(t, request, checker.request)
	})
	t.Run("request emulator", func(t *testing.T) {
		var checker deletionChecker
		request := service.DeploymentDeletionRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "emulator123",
			},
			ID: "deployment-d600t0598850n9abikm0",
		}
		assert.NoError(t, checker.SetRequest(request))
		assert.Equal(t, request, checker.request)
	})
	t.Run("request no actor", func(t *testing.T) {
		var checker deletionChecker
		request := service.DeploymentDeletionRequest{
			ID: "deployment-d600t0598850n9abikm0",
		}
		assert.Error(t, checker.SetRequest(request))
		assert.Equal(t, service.DeploymentDeletionRequest{}, checker.request)
	})
	t.Run("request no ID", func(t *testing.T) {
		var checker deletionChecker
		request := service.DeploymentDeletionRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
		}
		assert.Error(t, checker.SetRequest(request))
		assert.Equal(t, service.DeploymentDeletionRequest{}, checker.request)
	})
	t.Run("request bad ID", func(t *testing.T) {
		var checker deletionChecker
		request := service.DeploymentDeletionRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			ID: common.NewID("badID"),
		}
		assert.Error(t, checker.SetRequest(request))
		assert.Equal(t, service.DeploymentDeletionRequest{}, checker.request)
	})

	t.Run("deployment", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID: "deployment-d600t0598850n9abikm0",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.NoError(t, checker.SetDeployment(deployment))
		assert.Equal(t, deployment, checker.deployment)
	})
	t.Run("deployment bad ID", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        common.NewID("badID"),
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment no creator", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment bad template ID", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             common.NewID("badID"),
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment no template type", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment bad provider ID", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: common.NewID("badID"),
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment empty current status", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        "",
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment current status deleted", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusDeleted,
			PendingStatus:        service.DeploymentStatusNoPending,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment empty pending status", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        "",
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment pending status creating", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusCreating,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})
	t.Run("deployment pending status deleting", func(t *testing.T) {
		var checker deletionChecker
		deployment := types.Deployment{
			ID:        "deployment-d600t0598850n9abikm0",
			Workspace: "",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			Template:             "template-cnm8f0598850n9abikj0",
			TemplateType:         "terraform_openstack",
			PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
			CurrentStatus:        service.DeploymentStatusActive,
			PendingStatus:        service.DeploymentStatusDeleting,
			CloudCredentials:     nil,
			GitCredential:        "",
			LastRun:              nil,
		}
		assert.Error(t, checker.SetDeployment(deployment))
		assert.Equal(t, types.Deployment{}, checker.deployment)
	})

	t.Run("run", func(t *testing.T) {
		var checker deletionChecker
		run := types.DeploymentRun{
			ID:         "run-d8caa0598850n9abikmg",
			Deployment: "deployment-d600t0598850n9abikm0",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
			Parameters:       nil,
			Status:           "",
			StatusMsg:        "",
			LastState:        deploymentcommon.DeploymentStateView{},
			StateUpdatedAt:   time.Time{},
		}
		assert.NoError(t, checker.AddRun(run))
		assert.Equal(t, checker.runs, []types.DeploymentRun{run})
	})
	t.Run("run bad ID", func(t *testing.T) {
		var checker deletionChecker
		run := types.DeploymentRun{
			ID:         common.NewID("badID"),
			Deployment: "deployment-d600t0598850n9abikm0",
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
			Parameters:       nil,
			Status:           "",
			StatusMsg:        "",
			LastState:        deploymentcommon.DeploymentStateView{},
			StateUpdatedAt:   time.Time{},
		}
		assert.Error(t, checker.AddRun(run))
		assert.Nil(t, checker.runs)
	})
	t.Run("run err", func(t *testing.T) {
		var checker deletionChecker
		run := types.DeploymentRun{
			ID:         "run-d8caa0598850n9abikmg",
			Deployment: common.NewID("badID"),
			CreatedBy: deploymentcommon.Creator{
				User: "testuser123",
			},
			TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
			Parameters:       nil,
			Status:           "",
			StatusMsg:        "",
			LastState:        deploymentcommon.DeploymentStateView{},
			StateUpdatedAt:   time.Time{},
		}
		assert.Error(t, checker.AddRun(run))
		assert.Nil(t, checker.runs)
	})
}

func Test_deletionChecker_Check(t *testing.T) {
	runID := common.ID("run-d8caa0598850n9abikmg")

	type fields struct {
		deployment types.Deployment
		request    service.DeploymentDeletionRequest
		runs       []types.DeploymentRun
	}
	tests := []struct {
		name                  string
		fields                fields
		wantNeedCleanup       bool
		errMatchedExpectation assert.ErrorAssertionFunc
	}{
		{
			name: "normal no last run",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              nil,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: nil,
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.NoError(t, err)
				return err == nil
			},
		},
		{
			name: "normal last run w/ no state",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              &runID,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: []types.DeploymentRun{
					{
						ID:         "run-d8caa0598850n9abikmg",
						Deployment: "deployment-d600t0598850n9abikm0",
						LastState:  deploymentcommon.DeploymentStateView{},
					},
				},
			},
			wantNeedCleanup: true, // perform a cleanup even if deployment shows to have no resources in state view
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.NoError(t, err)
				return err == nil
			},
		},
		{
			name: "normal last run w/ state",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              &runID,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: []types.DeploymentRun{
					{
						ID:         "run-d8caa0598850n9abikmg",
						Deployment: "deployment-d600t0598850n9abikm0",
						LastState: deploymentcommon.DeploymentStateView{
							Resources: []service.DeploymentResource{
								{
									ID:                  "foobar123",
									Type:                "foobar",
									ProviderType:        "foobar",
									Provider:            "provider-d17e30598850n9abikl0",
									Attributes:          nil,
									SensitiveAttributes: nil,
									AvailableActions:    nil,
								},
							},
						},
					},
				},
			},
			wantNeedCleanup: true,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.NoError(t, err)
				return err == nil
			},
		},
		{
			name: "creator is diff from actor, no permission",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              nil,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "DifferentUser",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: nil,
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "deployment not set",
			fields: fields{
				deployment: types.Deployment{},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: nil,
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "request not set",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              nil,
				},
				request: service.DeploymentDeletionRequest{},
				runs:    nil,
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "runs not set when deployment has last run",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              &runID,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: nil,
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "runs set when deployment has no last run",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              nil,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: []types.DeploymentRun{
					{
						ID:         "run-d8caa0598850n9abikmg",
						Deployment: "deployment-bbbbbbbbbbbbbbbbbbbb",
						LastState: deploymentcommon.DeploymentStateView{
							Resources: nil,
						},
					},
				},
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "inconsistent deployment ID between deployment and request",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              nil,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-bbbbbbbbbbbbbbbbbbbb",
				},
				runs: nil,
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "inconsistent deployment ID between runs and deployment",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              &runID,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: []types.DeploymentRun{
					{
						ID:         "run-d8caa0598850n9abikmg",
						Deployment: "deployment-bbbbbbbbbbbbbbbbbbbb",
						LastState: deploymentcommon.DeploymentStateView{
							Resources: nil,
						},
					},
				},
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "last run not in runs",
			fields: fields{
				deployment: types.Deployment{
					ID:        "deployment-d600t0598850n9abikm0",
					Workspace: "workspace-d3jng0598850n9abiklg",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					Template:             "template-cnm8f0598850n9abikj0",
					TemplateType:         "",
					PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
					CurrentStatus:        service.DeploymentStatusActive,
					PendingStatus:        service.DeploymentStatusNoPending,
					CloudCredentials:     nil,
					GitCredential:        "",
					LastRun:              &runID,
				},
				request: service.DeploymentDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID: "deployment-d600t0598850n9abikm0",
				},
				runs: []types.DeploymentRun{
					{
						ID:         "run-co1g2rt9885fie78ffkg",
						Deployment: "deployment-d600t0598850n9abikm0",
						LastState: deploymentcommon.DeploymentStateView{
							Resources: nil,
						},
					},
				},
			},
			wantNeedCleanup: false,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := deletionChecker{
				deployment: tt.fields.deployment,
				request:    tt.fields.request,
				runs:       tt.fields.runs,
			}
			gotNeedCleanup, err := dc.Check()
			if !tt.errMatchedExpectation(t, err, "Check()") {
				return
			}
			assert.Equalf(t, tt.wantNeedCleanup, gotNeedCleanup, "Check()")
		})
	}
}
