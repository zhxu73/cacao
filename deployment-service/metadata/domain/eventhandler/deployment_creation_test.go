package eventhandler

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

func Test_deploymentFactory_Setter(t *testing.T) {
	t.Run("request", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.NoError(t, fac.SetRequest(req))
		assert.Equal(t, req, fac.request)
	})
	t.Run("request emulator", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "user123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.NoError(t, fac.SetRequest(req))
		assert.Equal(t, req, fac.request)
	})
	t.Run("request no actor", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request no workspace", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request no template", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request no provider", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace: "workspace-d3jng0598850n9abiklg",
				Template:  "template-cnm8f0598850n9abikj0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request no cloud cred", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request cloud cred bad provider ID", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request cloud cred, provider ID not in list", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-co1g2rt9885fie78ffj0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request cloud cred bad cred ID", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("request long description", func(t *testing.T) {
		var fac deploymentFactory
		req := service.DeploymentCreationRequest{
			Session: service.Session{
				SessionActor: "testuser123",
			},
			CreateParam: service.DeploymentCreateParam{
				Description:  strings.Repeat("a", 256),
				Workspace:    "workspace-d3jng0598850n9abiklg",
				Template:     "template-cnm8f0598850n9abikj0",
				PrimaryCloud: "provider-d17e30598850n9abikl0",
				CloudCredentials: map[string]common.ID{
					"cred123": "provider-d17e30598850n9abikl0",
				},
			},
		}
		assert.Error(t, fac.SetRequest(req))
		assert.Equal(t, service.DeploymentCreationRequest{}, fac.request)
	})
	t.Run("workspace", func(t *testing.T) {
		var fac deploymentFactory
		ws := &service.WorkspaceModel{
			ID:    common.NewID("workspace"),
			Owner: "foo",
		}
		err := fac.SetWorkspace(ws)
		assert.NoError(t, err)
		assert.Equal(t, ws, fac.workspace)
	})
	t.Run("workspace bad ID", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetWorkspace(&service.WorkspaceModel{
			ID:    common.NewID("badID"),
			Owner: "foo",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.workspace)
	})
	t.Run("workspace no owner", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetWorkspace(&service.WorkspaceModel{
			ID:    common.NewID("workspace"),
			Owner: "",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.workspace)
	})
	t.Run("template", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.NoError(t, fac.SetTemplate(template))
		assert.Equal(t, template, fac.template)
	})
	t.Run("template bad ID", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("badID"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no owner", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template bad source type", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no source URI", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no branch nor tag", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"path": "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no path", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no metadata name", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "",
				TemplateType: "templateType",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no metadata template type name", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "",
				Purpose:      "purpose",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("template no metadata purpose", func(t *testing.T) {
		var fac deploymentFactory
		template := &service.TemplateModel{
			ID:    common.NewID("template"),
			Owner: "foo",
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:         "name",
				TemplateType: "templateType",
				Purpose:      "",
			},
		}
		assert.Error(t, fac.SetTemplate(template))
		assert.Nil(t, fac.template)
	})
	t.Run("primary provider", func(t *testing.T) {
		var fac deploymentFactory
		provider := &service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "cyverse.org",
		}
		err := fac.SetPrimaryProvider(provider)
		assert.NoError(t, err)
		assert.Equal(t, provider, fac.primary)
		assert.Nil(t, fac.providers)
	})
	t.Run("primary provider bad ID", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetPrimaryProvider(&service.ProviderModel{
			ID:   common.NewID("badID"),
			Type: "openstack",
			URL:  "cyverse.org",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.primary)
		assert.Nil(t, fac.providers)
	})
	t.Run("primary provider no type", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetPrimaryProvider(&service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "",
			URL:  "cyverse.org",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.primary)
		assert.Nil(t, fac.providers)
	})
	t.Run("primary provider no URL", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetPrimaryProvider(&service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.primary)
		assert.Nil(t, fac.providers)
	})
	t.Run("provider", func(t *testing.T) {
		var fac deploymentFactory
		provider := service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "cyverse.org",
		}
		err := fac.AddProvider(&provider)
		assert.NoError(t, err)
		assert.Equal(t, []service.ProviderModel{provider}, fac.providers)
		assert.Nil(t, fac.primary)
	})
	t.Run("provider 2", func(t *testing.T) {
		var fac deploymentFactory
		provider := service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "foo.cyverse.org",
		}
		provider2 := service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "bar.cyverse.org",
		}
		assert.NoError(t, fac.AddProvider(&provider))
		assert.Equal(t, []service.ProviderModel{provider}, fac.providers)
		assert.Nil(t, fac.primary)
		assert.NoError(t, fac.AddProvider(&provider2))
		assert.ElementsMatch(t, []service.ProviderModel{provider, provider2}, fac.providers)
		assert.Nil(t, fac.primary)
	})
	t.Run("provider badID", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddProvider(&service.ProviderModel{
			ID:   common.NewID("badID"),
			Type: "openstack",
			URL:  "cyverse.org",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.providers)
		assert.Nil(t, fac.primary)
	})
	t.Run("provider no type", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddProvider(&service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "",
			URL:  "cyverse.org",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.providers)
		assert.Nil(t, fac.primary)
	})
	t.Run("provider no URL", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddProvider(&service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.providers)
		assert.Nil(t, fac.primary)
	})
	t.Run("provider duplicate", func(t *testing.T) {
		var fac deploymentFactory
		provider := &service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "cyverse.org",
		}
		err := fac.AddProvider(provider)
		assert.NoError(t, err)
		assert.NotNil(t, fac.providers)
		assert.Len(t, fac.providers, 1)
		err2 := fac.AddProvider(provider)
		assert.Error(t, err2)
		assert.Len(t, fac.providers, 1)
	})
	t.Run("provider duplicate w/ primary", func(t *testing.T) {
		var fac deploymentFactory
		provider := &service.ProviderModel{
			ID:   common.NewID("provider"),
			Type: "openstack",
			URL:  "cyverse.org",
		}
		err := fac.SetPrimaryProvider(provider)
		assert.NoError(t, err)
		err2 := fac.AddProvider(provider)
		assert.Error(t, err2)
		assert.Nil(t, fac.providers)
		assert.NotNil(t, fac.primary)
	})
	t.Run("cloud credential", func(t *testing.T) {
		var fac deploymentFactory
		providerID := common.ID("provider-d17e30598850n9abikl0")
		cred := service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "openstack",
			ID:       "cred123",
		}
		err := fac.AddCloudCredential(providerID, &cred)
		assert.NoError(t, err)
		assert.NotNil(t, fac.cloudCreds)
		assert.Len(t, fac.cloudCreds, 1)
		assert.Equal(t, []credProviderIDPair{
			{
				Credential: cred,
				ProviderID: providerID,
			},
		}, fac.cloudCreds)
	})
	t.Run("cloud credential bad provider ID", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddCloudCredential("badID", &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "openstack",
			ID:       "cred123",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.cloudCreds)
	})
	t.Run("cloud credential no username", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddCloudCredential("provider-d17e30598850n9abikl0", &service.CredentialModel{
			Username: "",
			Value:    "{}",
			Type:     "openstack",
			ID:       "cred123",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.cloudCreds)
	})
	t.Run("cloud credential empty value", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddCloudCredential("provider-d17e30598850n9abikl0", &service.CredentialModel{
			Username: "testuser123",
			Value:    "",
			Type:     "openstack",
			ID:       "cred123",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.cloudCreds)
	})
	t.Run("cloud credential no type", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddCloudCredential("provider-d17e30598850n9abikl0", &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "",
			ID:       "cred123",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.cloudCreds)
	})
	t.Run("cloud credential bad ID", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.AddCloudCredential("provider-d17e30598850n9abikl0", &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "openstack",
			ID:       "",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.cloudCreds)
	})
	t.Run("git credential", func(t *testing.T) {
		var fac deploymentFactory
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "git",
			ID:       "cred123",
		}
		err := fac.SetGitCredential(cred)
		assert.NoError(t, err)
		assert.Equal(t, cred, fac.gitCred)
	})
	t.Run("git credential bad ID", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetGitCredential(&service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "git",
			ID:       "",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.gitCred)
	})
	t.Run("git credential no username", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetGitCredential(&service.CredentialModel{
			Username: "",
			Value:    "{}",
			Type:     "git",
			ID:       "cred123",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.gitCred)
	})
	t.Run("git credential empty value", func(t *testing.T) {
		var fac deploymentFactory
		err := fac.SetGitCredential(&service.CredentialModel{
			Username: "testuser123",
			Value:    "",
			Type:     "git",
			ID:       "cred123",
		})
		assert.Error(t, err)
		assert.Nil(t, fac.gitCred)
	})
	t.Run("creation timestamp", func(t *testing.T) {
		var fac deploymentFactory
		now := time.Now()
		assert.NoError(t, fac.SetCreationTimestamp(now))
		assert.Equal(t, now, fac.createdAt)
	})
	t.Run("creation timestamp zero value", func(t *testing.T) {
		var fac deploymentFactory
		assert.Error(t, fac.SetCreationTimestamp(time.Time{}))
		assert.Equal(t, time.Time{}, fac.createdAt)
	})
}

func Test_deploymentFactory_Create(t *testing.T) {

	simpleRequest := service.DeploymentCreationRequest{
		Session: service.Session{
			SessionActor: "testuser123",
		},
		CreateParam: service.DeploymentCreateParam{
			Name:           "name123",
			Description:    "description123",
			Workspace:      "workspace-d3jng0598850n9abiklg",
			Template:       "template-cnm8f0598850n9abikj0",
			PrimaryCloud:   "provider-d17e30598850n9abikl0",
			CloudProviders: nil,
			CloudCredentials: map[string]common.ID{
				"cred123": "provider-d17e30598850n9abikl0",
			},
			GitCredential: "",
		},
	}
	now := time.Now()

	type fields struct {
		request    service.DeploymentCreationRequest
		workspace  *service.WorkspaceModel
		template   service.Template
		primary    *service.ProviderModel
		providers  []service.ProviderModel
		cloudCreds []credProviderIDPair
		gitCred    *service.CredentialModel
		createdAt  time.Time
	}
	type args struct {
		deploymentID common.ID
	}
	tests := []struct {
		name                  string
		fields                fields
		args                  args
		want                  *types.Deployment
		errMatchedExpectation assert.ErrorAssertionFunc // return true if returned error (could be nil) matched expectation
	}{
		{
			name: "normal",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: &types.Deployment{
				ID:          "deployment-d600t0598850n9abikm0",
				Name:        "name123",
				Description: "description123",
				CreatedAt:   now,
				UpdatedAt:   now,
				Workspace:   "workspace-d3jng0598850n9abiklg",
				CreatedBy: deploymentcommon.Creator{
					User:     "testuser123",
					Emulator: "",
				},
				Template:             "template-cnm8f0598850n9abikj0",
				TemplateType:         "terraform_openstack",
				PrimaryCloudProvider: "provider-d17e30598850n9abikl0",
				CurrentStatus:        service.DeploymentStatusNone,
				PendingStatus:        service.DeploymentStatusNoPending,
				StatusMsg:            "",
				CloudCredentials: deploymentcommon.ProviderCredentialMappings{
					{
						Credential: "cred123",
						Provider:   "provider-d17e30598850n9abikl0",
					},
				},
				GitCredential: "",
				LastRun:       nil,
			},
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.NoError(t, err)
				return err == nil
			},
		},
		{
			name: "bad deployment name",
			fields: fields{
				request: service.DeploymentCreationRequest{
					Session: service.Session{
						SessionActor: "testuser123",
					},
					CreateParam: service.DeploymentCreateParam{
						Name:           "_Bad-Name_\U0001f60a",
						Description:    "description123",
						Workspace:      "workspace-d3jng0598850n9abiklg",
						Template:       "template-cnm8f0598850n9abikj0",
						PrimaryCloud:   "provider-d17e30598850n9abikl0",
						CloudProviders: nil,
						CloudCredentials: map[string]common.ID{
							"cred123": "provider-d17e30598850n9abikl0",
						},
						GitCredential: "",
					},
				},
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "no workspace set",
			fields: fields{
				request:   simpleRequest,
				workspace: nil,
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "no template set",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: nil,
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "no primary provider set",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary:   nil,
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "no cloud cred set",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers:  nil,
				cloudCreds: nil,
				gitCred:    nil,
				createdAt:  now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "no cloud cred for primary provider",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-co1g2rt9885fie78ffj0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "cloud cred for other provider",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
					{
						Credential: service.CredentialModel{
							ID: "cred345",
						},
						ProviderID: "provider-co1g2rt9885fie78ffj0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "createdAt zero time",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: time.Time{},
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "private template but no git cred",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPrivate,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-co1g2rt9885fie78ffj0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "provider ID (in credential pairs) not in deployment",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-co1g2rt9885fie78ffj0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "inconsistent provider ID in credential pairs",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-co1g2rt9885fie78ffj0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
		{
			name: "no workspace permission",
			fields: fields{
				request: simpleRequest,
				workspace: &service.WorkspaceModel{
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "DifferentUser",
				},
				template: &service.TemplateModel{
					ID: "template-cnm8f0598850n9abikj0",
					Source: service.TemplateSource{
						Visibility: service.TemplateSourceVisibilityPublic,
					},
					Metadata: service.TemplateMetadata{
						Name:         "foo",
						TemplateType: "terraform_openstack",
						Purpose:      "general",
					},
				},
				primary: &service.ProviderModel{
					ID: "provider-d17e30598850n9abikl0",
				},
				providers: nil,
				cloudCreds: []credProviderIDPair{
					{
						Credential: service.CredentialModel{
							ID: "cred123",
						},
						ProviderID: "provider-d17e30598850n9abikl0",
					},
				},
				gitCred:   nil,
				createdAt: now,
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
			},
			want: nil,
			errMatchedExpectation: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				return err != nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fac := deploymentFactory{
				request:    tt.fields.request,
				workspace:  tt.fields.workspace,
				template:   tt.fields.template,
				primary:    tt.fields.primary,
				providers:  tt.fields.providers,
				cloudCreds: tt.fields.cloudCreds,
				gitCred:    tt.fields.gitCred,
				createdAt:  tt.fields.createdAt,
			}
			got, err := fac.Create(tt.args.deploymentID)
			if !tt.errMatchedExpectation(t, err, fmt.Sprintf("Create(%v)", tt.args.deploymentID)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Create(%v)", tt.args.deploymentID)
		})
	}
}
