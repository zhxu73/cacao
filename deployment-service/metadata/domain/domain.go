package domain

import (
	"context"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/domain/eventhandler"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/domain/queryhandler"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"sync"
)

const queryChannDepth = 5
const workerCount = 5

// Domain is the entrypoint object of this microservice
type Domain struct {
	NatsSrc   ports.QuerySrc
	NatsSink  ports.ReplySink
	queryChan chan types.Query

	EventSrc ports.EventSrc

	Storage    ports.DeploymentStorage
	RunStorage ports.DeploymentRunStorage
	TimeSrc    ports.TimeSrc

	WorkspaceMS ports.WorkspaceMicroservice
	TemplateMS  ports.TemplateMicroservice
	ProviderMS  ports.ProviderMicroservice
	CredMS      ports.CredentialMicroservice

	conf types.Config
}

// Init initialize all adapters, and domain internals
func (svc *Domain) Init(envConf types.EnvConfig, conf types.Config) error {

	svc.conf = conf

	if err := svc.Storage.Init(); err != nil {
		return err
	}

	if err := svc.RunStorage.Init(); err != nil {
		return err
	}

	if err := svc.NatsSrc.Init(conf.MessagingConfig.NatsConfig); err != nil {
		return err
	}

	svc.queryChan = make(chan types.Query, queryChannDepth)
	svc.NatsSrc.InitChannel(svc.queryChan)

	if err := svc.EventSrc.Init(conf.MessagingConfig); err != nil {
		return err
	}
	svc.EventSrc.SetHandlers(eventhandler.NewEventHandlers(ports.Ports{
		DeploymentStorage: svc.Storage,
		RunStorage:        svc.RunStorage,
		TimeSrc:           svc.TimeSrc,
		TemplateMS:        svc.TemplateMS,
		WorkspaceMS:       svc.WorkspaceMS,
		CredentialMS:      svc.CredMS,
		ProviderMS:        svc.ProviderMS,
	}))
	return nil
}

// Start starts accepting incoming queries and processing them
func (svc Domain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	err := svc.startQuery(ctx, &wg)
	if err != nil {
		return err
	}

	wg.Add(1)
	err = svc.EventSrc.Start(ctx, &wg)
	if err != nil {
		return err
	}
	wg.Wait()
	return nil
}

func (svc Domain) startQuery(ctx context.Context, wg *sync.WaitGroup) error {
	err := svc.NatsSrc.Start(ctx)
	if err != nil {
		return err
	}

	svc.spawnQueryWorker(wg, workerCount)
	return nil
}

func (svc Domain) spawnQueryWorker(wg *sync.WaitGroup, workerCount int) {
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		worker := queryhandler.NewQueryWorker(svc.conf.MessagingConfig.ClientID, svc.NatsSink, svc.Storage, svc.RunStorage)
		go worker.Run(wg, svc.queryChan)
	}
}
