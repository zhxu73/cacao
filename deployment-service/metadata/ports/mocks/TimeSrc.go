package mocks

import (
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"time"
)

// IncrementingTimeSrc returns a time.Time that is incrementing each time Now() is called.
// This is useful when testing creation and update timestamp.
type IncrementingTimeSrc struct {
	Start time.Time
	curr  time.Time
}

// TimeSrcIncrementStep ...
const TimeSrcIncrementStep = time.Second

var _ ports.TimeSrc = (*IncrementingTimeSrc)(nil)

// Now ...
func (t *IncrementingTimeSrc) Now() time.Time {
	if t.curr.IsZero() {
		t.curr = t.Start
		return t.Start
	}
	t.curr = t.curr.Add(TimeSrcIncrementStep)
	return t.curr
}
