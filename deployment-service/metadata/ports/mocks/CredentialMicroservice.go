// Code generated by mockery v2.22.1. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"

	service "gitlab.com/cyverse/cacao-common/service"
)

// CredentialMicroservice is an autogenerated mock type for the CredentialMicroservice type
type CredentialMicroservice struct {
	mock.Mock
}

// Get provides a mock function with given fields: actor, owner, id
func (_m *CredentialMicroservice) Get(actor service.Actor, owner string, id string) (*service.CredentialModel, error) {
	ret := _m.Called(actor, owner, id)

	var r0 *service.CredentialModel
	var r1 error
	if rf, ok := ret.Get(0).(func(service.Actor, string, string) (*service.CredentialModel, error)); ok {
		return rf(actor, owner, id)
	}
	if rf, ok := ret.Get(0).(func(service.Actor, string, string) *service.CredentialModel); ok {
		r0 = rf(actor, owner, id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*service.CredentialModel)
		}
	}

	if rf, ok := ret.Get(1).(func(service.Actor, string, string) error); ok {
		r1 = rf(actor, owner, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// List provides a mock function with given fields: actor, owner
func (_m *CredentialMicroservice) List(actor service.Actor, owner string) ([]service.CredentialModel, error) {
	ret := _m.Called(actor, owner)

	var r0 []service.CredentialModel
	var r1 error
	if rf, ok := ret.Get(0).(func(service.Actor, string) ([]service.CredentialModel, error)); ok {
		return rf(actor, owner)
	}
	if rf, ok := ret.Get(0).(func(service.Actor, string) []service.CredentialModel); ok {
		r0 = rf(actor, owner)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]service.CredentialModel)
		}
	}

	if rf, ok := ret.Get(1).(func(service.Actor, string) error); ok {
		r1 = rf(actor, owner)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewCredentialMicroservice interface {
	mock.TestingT
	Cleanup(func())
}

// NewCredentialMicroservice creates a new instance of CredentialMicroservice. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewCredentialMicroservice(t mockConstructorTestingTNewCredentialMicroservice) *CredentialMicroservice {
	mock := &CredentialMicroservice{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
