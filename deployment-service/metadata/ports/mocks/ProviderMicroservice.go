// Code generated by mockery v2.22.1. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	common "gitlab.com/cyverse/cacao-common/common"

	service "gitlab.com/cyverse/cacao-common/service"
)

// ProviderMicroservice is an autogenerated mock type for the ProviderMicroservice type
type ProviderMicroservice struct {
	mock.Mock
}

// Get provides a mock function with given fields: actor, id
func (_m *ProviderMicroservice) Get(actor service.Actor, id common.ID) (*service.ProviderModel, error) {
	ret := _m.Called(actor, id)

	var r0 *service.ProviderModel
	var r1 error
	if rf, ok := ret.Get(0).(func(service.Actor, common.ID) (*service.ProviderModel, error)); ok {
		return rf(actor, id)
	}
	if rf, ok := ret.Get(0).(func(service.Actor, common.ID) *service.ProviderModel); ok {
		r0 = rf(actor, id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*service.ProviderModel)
		}
	}

	if rf, ok := ret.Get(1).(func(service.Actor, common.ID) error); ok {
		r1 = rf(actor, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// List provides a mock function with given fields: actor
func (_m *ProviderMicroservice) List(actor service.Actor) ([]service.ProviderModel, error) {
	ret := _m.Called(actor)

	var r0 []service.ProviderModel
	var r1 error
	if rf, ok := ret.Get(0).(func(service.Actor) ([]service.ProviderModel, error)); ok {
		return rf(actor)
	}
	if rf, ok := ret.Get(0).(func(service.Actor) []service.ProviderModel); ok {
		r0 = rf(actor)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]service.ProviderModel)
		}
	}

	if rf, ok := ret.Get(1).(func(service.Actor) error); ok {
		r1 = rf(actor)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewProviderMicroservice interface {
	mock.TestingT
	Cleanup(func())
}

// NewProviderMicroservice creates a new instance of ProviderMicroservice. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewProviderMicroservice(t mockConstructorTestingTNewProviderMicroservice) *ProviderMicroservice {
	mock := &ProviderMicroservice{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
