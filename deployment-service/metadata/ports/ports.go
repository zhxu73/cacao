package ports

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"sync"
	"time"
)

// ReplySink is a sink to send the query reply into
type ReplySink interface {
	Init(messaging2.NatsConfig) error
	Reply(subject string, reply cloudevents.Event) error
}

// QuerySrc is a source of incoming query to be processed
type QuerySrc interface {
	Init(messaging2.NatsConfig) error
	InitChannel(chan<- types.Query)
	Start(ctx context.Context) error
}

// EventSrc is a sink to publish events into
type EventSrc interface {
	Init(config messaging2.NatsStanMsgConfig) error
	SetHandlers(EventHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// EventHandlers represents the handlers for event operation that this service supports
type EventHandlers interface {
	DeploymentCreationRequested(service.DeploymentCreationRequest, OutgoingEventSink)
	DeploymentCreateRunRequested(service.DeploymentCreateRunRequest, OutgoingEventSink)
	DeploymentDeletionRequested(service.DeploymentDeletionRequest, OutgoingEventSink)
	EventRunPreflightStarted(deploymentevents.RunPreflightStarted, OutgoingEventSink)
	EventRunPreflightStartFailed(deploymentevents.RunPreflightStartFailed, OutgoingEventSink)
	EventRunPreflightFailed(deploymentevents.RunPreflightResult, OutgoingEventSink)
	EventRunExecutionStarted(deploymentevents.RunPreflightResult, OutgoingEventSink)
	EventRunExecutionSucceeded(deploymentevents.RunExecutionSucceeded, OutgoingEventSink)
	EventRunExecutionFailed(deploymentevents.RunExecutionFailed, OutgoingEventSink)
	DeploymentUpdateRequested(service.DeploymentUpdateRequest, OutgoingEventSink)
	EventDeploymentDeletionCleanupSucceeded(deploymentevents.DeploymentDeletionCleanupResult, OutgoingEventSink)
	EventDeploymentDeletionCleanupFailed(deploymentevents.DeploymentDeletionCleanupResult, OutgoingEventSink)
}

// OutgoingEventSink represents all events this service will publish in its event handlers
type OutgoingEventSink interface {
	DeploymentCreated(service.DeploymentCreationResult)      // service.DeploymentCreated
	DeploymentCreateFailed(service.DeploymentCreationResult) // service.DeploymentCreateFailed

	DeploymentUpdated(service.DeploymentUpdateResult)      //service.DeploymentUpdated
	DeploymentUpdateFailed(service.DeploymentUpdateResult) // service.DeploymentUpdateFailed

	EventStartRunRequested(deploymentevents.StartRunRequest)                     // deploymentevents.EventStartRunRequested
	DeploymentRunCreated(service.DeploymentCreateRunResult)                      // service.DeploymentRunCreated
	DeploymentRunCreateFailed(service.DeploymentCreateRunResult)                 // service.DeploymentRunCreateFailed
	EventDeploymentRunStatusUpdated(deploymentevents.DeploymentRunStatusUpdated) // deploymentevents.EventDeploymentRunStatusUpdated

	DeploymentDeleted(service.DeploymentDeletionResult)         // service.DeploymentDeleted
	DeploymentDeleteFailed(service.DeploymentDeletionResult)    // service.DeploymentDeleteFailed
	DeploymentDeletionStarted(service.DeploymentDeletionResult) // service.DeploymentDeletionStarted
}

// DeploymentStorage is storage for Deployment objects
type DeploymentStorage interface {
	Init() error
	Create(types.Deployment) error
	Get(common.ID) (types.Deployment, error)
	List(filters types.DeploymentFilter, offset int64, limit int64, sort types.DeploymentSort) ([]types.Deployment, error)
	Update(id common.ID, deployment types.DeploymentUpdate, filter types.DeploymentFilter) (bool, error)
	Delete(common.ID) error
}

// DeploymentBuildStorage is storage for DeploymentBuild
type DeploymentBuildStorage interface {
	// TODO implement later
}

// DeploymentRunStorage is storage for DeploymentRun objects
type DeploymentRunStorage interface {
	Init() error
	Get(runID common.ID) (types.DeploymentRun, error)
	// GetLatestRun returns the latest run of a deployment if any.
	GetLatestRun(deployment common.ID) (*types.DeploymentRun, error)
	List(filter types.DeploymentRunFilter, pagination service.RequestPagination) ([]types.DeploymentRun, error)
	BatchGet(runIDs []common.ID) ([]types.DeploymentRun, error)
	Create(run types.DeploymentRun) (runID common.ID, err error)
	Update(id common.ID, run types.DeploymentRunUpdate, filter types.DeploymentRunFilter) (bool, error)
	// DeletePlaceholder deletes a placeholder record for a pending run creation request
	DeletePlaceholder(id common.ID) (bool, error)
}

// WorkspaceMicroservice is an interface to interact with Workspace service
type WorkspaceMicroservice interface {
	Get(actor types.Actor, id common.ID) (*service.WorkspaceModel, error)
}

// TemplateMicroservice is an interface to interact with Template service
type TemplateMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Template, error)
	GetTemplateType(actor types.Actor, templateTypeName service.TemplateTypeName) (service.TemplateType, error)
	GetTemplateVersion(actor types.Actor, id common.ID) (*service.TemplateVersion, error)
}

// ProviderMicroservice is an interface to interact with Provider service
type ProviderMicroservice interface {
	Get(actor types.Actor, id common.ID) (*service.ProviderModel, error)
	List(actor types.Actor) ([]service.ProviderModel, error)
}

// CredentialMicroservice is an interface to interact with Credential service
type CredentialMicroservice interface {
	Get(actor types.Actor, owner, id string) (*service.CredentialModel, error)
	// List return a list of credential ID
	List(actor types.Actor, owner string) ([]service.CredentialModel, error)
}

// TimeSrc is source of current time
type TimeSrc interface {
	Now() time.Time
}

// Ports contains a ref to most ports, this struct exist to ease passing dependency as parameter.
type Ports struct {
	DeploymentStorage DeploymentStorage
	RunStorage        DeploymentRunStorage
	TimeSrc           TimeSrc

	TemplateMS   TemplateMicroservice
	WorkspaceMS  WorkspaceMicroservice
	CredentialMS CredentialMicroservice
	ProviderMS   ProviderMicroservice
}
