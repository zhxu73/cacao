package types

import (
	"errors"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"reflect"
	"testing"
)

func TestErrorToServiceError(t *testing.T) {
	type args struct {
		err error
	}
	tests := []struct {
		name string
		args args
		want service.CacaoError
	}{
		{
			name: "errors.New",
			args: args{
				err: errors.New("failed"),
			},
			want: service.NewCacaoGeneralError("failed"),
		},
		{
			name: "invalidParam svc err",
			args: args{
				err: service.NewCacaoInvalidParameterError("failed"),
			},
			want: service.NewCacaoInvalidParameterError("failed"),
		},
		{
			name: "nil",
			args: args{
				err: nil,
			},
			want: &service.CacaoErrorBase{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ErrorToServiceError(tt.args.err); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ErrorToServiceError() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMergeServiceErrorAndError(t *testing.T) {
	type args struct {
		session service.Session
		err     error
	}
	tests := []struct {
		name string
		args args
		want service.CacaoErrorBase
	}{
		{
			name: "no svc err & no err",
			args: args{
				session: service.Session{},
				err:     nil,
			},
			want: service.CacaoErrorBase{},
		},
		{
			name: "no svc err & has err",
			args: args{
				session: service.Session{
					ServiceError: service.CacaoErrorBase{
						StandardMessage:   "",
						ContextualMessage: "",
						Options: service.CacaoErrorOptions{
							Temporary: false,
						},
					},
				},
				err: errors.New("failed"),
			},
			want: service.NewCacaoGeneralError("failed").GetBase(),
		},
		{
			name: "has svc err & no err",
			args: args{
				session: service.Session{
					ServiceError: service.NewCacaoInvalidParameterError("failed").GetBase(),
				},
				err: nil,
			},
			want: service.NewCacaoInvalidParameterError("failed").GetBase(),
		},
		{
			name: "has svc err & has err",
			args: args{
				session: service.Session{
					ServiceError: service.NewCacaoInvalidParameterError("svcErrfailed").GetBase(),
				},
				err: errors.New("failed"),
			},
			want: ErrorToServiceError(fmt.Errorf("error occurred while handling failure %w, original err: %s",
				errors.New("failed"),
				service.NewCacaoInvalidParameterError("svcErrfailed").Error(),
			)).GetBase(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeServiceErrorAndError(tt.args.session, tt.args.err); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MergeServiceErrorAndError() = %v, want %v", got, tt.want)
			}
		})
	}
}
