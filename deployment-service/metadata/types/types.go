package types

import (
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// Query is the the query type that is passed in between domain and storage via ports
type Query interface {
	QueryType() common.QueryOp
	CloudEvent() cloudevents.Event
	ReplySubject() string
}

// ID prefix
const (
	// DeploymentIDPrefix is prefix for ID for deployment objects
	DeploymentIDPrefix = "deployment"
	// RunIDPrefix is prefix for ID for deployment run objects
	RunIDPrefix = "run"
)

// EnvConfig is configuration via environment variable
type EnvConfig struct {
	PodName  string `envconfig:"POD_NAME"`
	LogLevel string `envconfig:"LOG_LEVEL" default:"debug"`
}

// Config is configuration via config file
type Config struct {
	MessagingConfig messaging2.NatsStanMsgConfig
	MongoConfig     db.MongoDBConfig
}

// Override ...
func (conf *Config) Override() {
	conf.MessagingConfig.WildcardSubject = string(DeploymentWildcardSubject)
	conf.MessagingConfig.QueueGroup = "deployment-metadata"
	conf.MessagingConfig.DurableName = "deployment-metadata"
}

// DeploymentWildcardSubject is the wildcard subject this microservice subcribe to for all incoming queries
const DeploymentWildcardSubject common.QueryOp = "cyverse.deployment.*"

const (
	// DeploymentStreamThreshold is a threshold for streaming deployments in multiple replies.
	// If num of deployments in the list exceeds this threshold, start streaming result in multiple replies.
	// 220 is after accounting for the LastRun field in the deployment object.
	DeploymentStreamThreshold = 220
	// DeploymentRunStreamThreshold is a threshold for streaming runs in multiple replies.
	// If num of runs in the list exceeds this threshold, start streaming result in multiple replies.
	DeploymentRunStreamThreshold = 1000
)

// Actor is the user that performs certain action.
// FIXME remove this alias, once migration is done
type Actor = service.Actor

// DeploymentType computes deployment type from template type and provider type. DeploymentType is used for determine
// which execution service handles operation on the deployment and run.
// FIXME move to deploymentcommon
func DeploymentType(templateType service.TemplateType, provider service.ProviderModel) string {
	if templateType.GetEngine() == "" {
		return ""
	}
	if provider.Type == "" {
		return ""
	}
	return fmt.Sprintf("%s_%s", templateType.GetEngine(), provider.Type)
}

// ErrorToServiceError converts built-in error object into service error that can be transported in events.
func ErrorToServiceError(err error) service.CacaoError {
	if err == nil {
		return &service.CacaoErrorBase{}
	}
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}
	return serviceError
}

// MergeServiceErrorAndError merge a new error into a service error
func MergeServiceErrorAndError(session service.Session, err error) service.CacaoErrorBase {
	noSvcErr := session.GetServiceError() == nil
	var svcErr service.CacaoErrorBase
	if err != nil && noSvcErr {
		svcErr = ErrorToServiceError(err).GetBase()
	} else if err == nil && !noSvcErr {
		svcErr = session.ServiceError
	} else if err != nil && !noSvcErr {
		newErr := fmt.Errorf("error occurred while handling failure %w, original err: %s", err, session.GetServiceError().Error())
		svcErr = ErrorToServiceError(newErr).GetBase()
	}
	// else {
	// no service error and no err
	//}
	return svcErr
}
