package deploymentevents

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// EventDeploymentDeletionCleanupStarted is event type that indicates the cleanup for deletion has started.
const EventDeploymentDeletionCleanupStarted common.EventType = common.EventTypePrefix + "DeploymentDeletionCleanupStarted"

// DeploymentDeletionCleanupStarted is the event body for EventDeploymentDeletionCleanupStarted
type DeploymentDeletionCleanupStarted struct {
	service.Session `json:",inline"`
	TemplateType    service.TemplateTypeName `json:"template_type"`
	Deployment      common.ID                `json:"deployment"`
}

// EventDeploymentDeletionCleanupSucceeded is event type that indicates the cleanup for deletion has succeeded.
const EventDeploymentDeletionCleanupSucceeded common.EventType = common.EventTypePrefix + "DeploymentDeletionCleanupSucceeded"

// EventDeploymentDeletionCleanupFailed is event type that indicates the cleanup for deletion has failed.
const EventDeploymentDeletionCleanupFailed common.EventType = common.EventTypePrefix + "DeploymentDeletionCleanupFailed"

// DeploymentDeletionCleanupResult is the event body for EventDeploymentDeletionCleanupFailed and EventDeploymentDeletionCleanupSucceeded.
type DeploymentDeletionCleanupResult struct {
	service.Session `json:",inline"`
	TemplateType    service.TemplateTypeName `json:"template_type"`
	Deployment      common.ID                `json:"deployment"`
}
