package deploymentevents

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"time"
)

// EventStartRunRequested is the event type for StartRunRequest
const EventStartRunRequested common.EventType = common.EventTypePrefix + "StartRunRequested"

// StartRunRequest is a request to start a run.
type StartRunRequest struct {
	service.Session `json:",inline"`
	Deployment      Deployment `json:"deployment"`
	Run             Run        `json:"run"`
}

// Deployment is deployment in the context/perspective of starting a run.
type Deployment struct {
	ID               common.ID                       `json:"id"`
	CreatedBy        deploymentcommon.Creator        `json:",inline"`
	TemplateType     service.TemplateTypeName        `json:"type"`
	Workspace        common.ID                       `json:"workspace"`
	PrimaryProvider  common.ID                       `json:"primary_provider"`
	CurrentStatus    service.DeploymentStatus        `json:"current_status"`
	PendingStatus    service.DeploymentPendingStatus `json:"pending_status"`
	CloudCredentials []ProviderCredentialPair        `json:"cloud_creds"`
	GitCredentialID  string                          `json:"git_cred"`
}

// ProviderCredentialPair is an association between a cloud credential and a provider
type ProviderCredentialPair struct {
	Provider     common.ID `json:"provider"`
	CredentialID string    `json:"credential"`
}

// Run is run in the context/perspective of starting it.
type Run struct {
	ID                common.ID                            `json:"id"`
	CreatedBy         deploymentcommon.Creator             `json:",inline"`
	CreatedAt         time.Time                            `json:"created_at"`
	TemplateID        common.ID                            `json:"template"`
	TemplateVersionID common.ID                            `json:"template_version"`
	RequestParameters service.DeploymentParameterValues    `json:"parameters"`
	Status            deploymentcommon.DeploymentRunStatus `json:"status"`
}

// EventRunPreflightStarted is event that indicates the preflight stage has started.
const EventRunPreflightStarted common.EventType = common.EventTypePrefix + "RunPreflightStarted"

// EventRunPreflightStartFailed is even type that indicates the preflight stage of a run cannot be started or failed to be started.
const EventRunPreflightStartFailed common.EventType = common.EventTypePrefix + "RunPreflightStartFailed"

// RunPreflightStarted  is an event body for EventRunPreflightStarted.
type RunPreflightStarted struct {
	service.Session `json:",inline"`
	TemplateType    service.TemplateTypeName              `json:"template_type"`
	Deployment      common.ID                             `json:"deployment"`
	Run             common.ID                             `json:"run"`
	Parameters      deploymentcommon.DeploymentParameters `json:"parameters"`
}

// RunPreflightStartFailed is an event body for EventRunPreflightStartFailed.
type RunPreflightStartFailed commonRunResultEventBody

// EventRunPreflightSucceeded is event type that indicate preflight stage of the run has succeeded
const EventRunPreflightSucceeded common.EventType = common.EventTypePrefix + "RunPreflightSucceeded"

// EventRunPreflightFailed is event type that indicate preflight stage of the run has failed
const EventRunPreflightFailed common.EventType = common.EventTypePrefix + "RunPreflightFailed"

// RunPreflightResult is an event that indicates the preflight stage has completed, it is the event body for EventRunPreflightSucceeded or EventRunPreflightFailed.
type RunPreflightResult commonRunResultEventBody

// EventRunExecutionRequested is event type to request for starting the execution stage.
// This is currently not used, since EventRunPreflightSucceeded will trigger the start of execution-stage.
// TODO this is reserved for situation when one needed to manually trigger the execution stage.
const EventRunExecutionRequested common.EventType = common.EventTypePrefix + "RunExecutionRequested"

// RunExecutionRequested is a request to start the execution stage of a run, it is the event body for EventRunExecutionRequested.
type RunExecutionRequested commonRunResultEventBody

// EventRunExecutionStarted is event type that is a request for starting execution stage.
const EventRunExecutionStarted common.EventType = common.EventTypePrefix + "RunExecutionStarted"

// RunExecutionStarted is the event body for EventRunExecutionStarted
type RunExecutionStarted commonRunResultEventBody

// EventRunExecutionSucceeded is event type that indicates the execution stage has succeeded
const EventRunExecutionSucceeded common.EventType = common.EventTypePrefix + "RunExecutionSucceeded"

// RunExecutionSucceeded is the event body for EventRunExecutionSucceeded.
type RunExecutionSucceeded struct {
	service.Session `json:",inline"`
	TemplateType    service.TemplateTypeName             `json:"template_type"`
	Deployment      common.ID                            `json:"deployment"`
	Run             common.ID                            `json:"run"`
	StateView       deploymentcommon.DeploymentStateView `json:"state_view"`
}

// EventRunExecutionFailed is event type that indicates the execution stage of run has failed, OR fail to start the execution stage.
const EventRunExecutionFailed common.EventType = common.EventTypePrefix + "RunExecutionFailed"

// RunExecutionFailed is the event body for EventRunExecutionFailed.
type RunExecutionFailed struct {
	service.Session `json:",inline"`
	TemplateType    service.TemplateTypeName             `json:"template_type"`
	Deployment      common.ID                            `json:"deployment"`
	Run             common.ID                            `json:"run"`
	StateView       deploymentcommon.DeploymentStateView `json:"state_view"`
}

// a common event body for various result(Success/Fail) events for different stages of run
type commonRunResultEventBody struct {
	service.Session `json:",inline"`
	TemplateType    service.TemplateTypeName `json:"template_type"`
	Deployment      common.ID                `json:"deployment"`
	Run             common.ID                `json:"run"`
}

// EventDeploymentRunStatusUpdated ...
const EventDeploymentRunStatusUpdated common.EventType = common.EventTypePrefix + "DeploymentRunStatusUpdated"

// DeploymentRunStatusUpdated is event emitted when the status of a deployment run is updated.
type DeploymentRunStatusUpdated struct {
	service.Session `json:",inline"`
	Deployment      common.ID                            `json:"deployment"`
	Run             common.ID                            `json:"run"`
	Status          deploymentcommon.DeploymentRunStatus `json:"status"`
	OldStatus       deploymentcommon.DeploymentRunStatus `json:"old_status"`
	// For outgoing purpose
	TransactionID common.TransactionID `json:"-"`
}

// EventType ...
func (d DeploymentRunStatusUpdated) EventType() common.EventType {
	return EventDeploymentRunStatusUpdated
}

// Transaction ...
func (d DeploymentRunStatusUpdated) Transaction() common.TransactionID {
	return d.TransactionID
}

// ToCloudEvent ...
func (d DeploymentRunStatusUpdated) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging2.CreateCloudEventWithTransactionID(d, EventDeploymentRunStatusUpdated, source, d.TransactionID)
}
