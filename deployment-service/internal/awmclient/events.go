package awmclient

import (
	"gitlab.com/cyverse/cacao-common/common"
)

// WorkflowCreateRequestedEvent is the type name of WorkflowCreate request
const WorkflowCreateRequestedEvent common.EventType = common.EventTypePrefix + "WorkflowCreateRequested"

// WorkflowCreate is an event that request the creation of a workflow.
type WorkflowCreate struct {
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// username of the user, null if not user specific
	Username *string `json:"username"`

	// filename of the workflow definition
	WorkflowFilename string `json:"workflow_type"`

	// workflow data, parameters and other metadata used to create the workflow
	WfDat map[string]interface{} `json:"workflow_data"`
}

// WorkflowCreatedEvent is the cloudevent name of WorkflowCreated defined below
const WorkflowCreatedEvent common.EventType = common.EventTypePrefix + "WorkflowCreated"

// WorkflowCreated is an event emitted when a Workflow is created.
type WorkflowCreated struct {
	// ID of the request event (WorkflowCreate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// The provider of which the workflow is created in
	Provider AWMProvider `json:"provider"`

	// Name of the created workflow
	WorkflowName string `json:"workflow_name"`
}

// WorkflowCreateFailedEvent is the event name of WorkflowCreateFailed
const WorkflowCreateFailedEvent common.EventType = common.EventTypePrefix + "WorkflowCreateFailed"

// WorkflowCreateFailed is an event emitted when failure occurred during
// workflow creation.
// This event is emitted in response to a WorkflowCreate event.
type WorkflowCreateFailed struct {
	// ID of the request event (WorkflowCreate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// The provider that the workflow is attempted to be created in
	Provider AWMProvider `json:"provider"`

	// Type of the error occurred that cause the creation to fail
	Error string `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// WorkflowTerminateRequestedEvent is the type name of WorkflowTerminate request
const WorkflowTerminateRequestedEvent common.EventType = common.EventTypePrefix + "WorkflowTerminateRequested"

// WorkflowTerminate is an event that request the terminatation of a workflow.
type WorkflowTerminate struct {
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`
}

// WorkflowTerminatedEvent is the name of the WorkflowTerminated
const WorkflowTerminatedEvent common.EventType = common.EventTypePrefix + "WorkflowTerminated"

// WorkflowTerminated is an event emitted when a workflow is terminated via a WorkflowTerminateCmd event.
type WorkflowTerminated struct {
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`

	// status of the workflow
	WorkflowStatus string `json:"workflow_status"`
}

// WorkflowTerminateFailedEvent is the name of the WorkflowTerminateFailed
const WorkflowTerminateFailedEvent common.EventType = common.EventTypePrefix + "WorkflowTerminateFailed"

// WorkflowTerminateFailed is an event emitted when fail to terminate a workflow.
type WorkflowTerminateFailed struct {
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`

	// Type of the error occurred that cause the termination to fail
	Error string `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// WorkflowResubmitRequestedEvent is the type name of WorkflowResubmit request
const WorkflowResubmitRequestedEvent common.EventType = common.EventTypePrefix + "WorkflowResubmitRequested"

// WorkflowResubmit is an event that request the resubmission of a workflow.
type WorkflowResubmit struct {
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`
}

// WorkflowResubmittedEvent is the name of the WorkflowResubmitted event
const WorkflowResubmittedEvent common.EventType = common.EventTypePrefix + "WorkflowResubmitted"

// WorkflowResubmitted is an event that is emitted when workflow resubmission succeeded.
type WorkflowResubmitted struct {
	// ID of the request event (WorkflowResubmit) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow that is requested to be resubmitted
	WorkflowName string `json:"workflow_name"`

	// name of the newly created workflow as the result of the resubmission
	NewWorkflowName string `json:"new_workflow_name"`
}

// WorkflowResubmitFailedEvent is the name of the WorkflowResubmitFailed event
const WorkflowResubmitFailedEvent common.EventType = common.EventTypePrefix + "WorkflowResubmitFailed"

// WorkflowResubmitFailed is an event that is emitted when workflow resubmission failed.
type WorkflowResubmitFailed struct {

	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow that is requested to be resubmitted
	WorkflowName string `json:"workflow_name"`

	// Type of the error occurred that cause the resubmission to fail
	Error string `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// WorkflowSucceededEvent is the event type of WorkflowSucceeded
const WorkflowSucceededEvent common.EventType = common.EventTypePrefix + "WorkflowSucceeded"

// WorkflowFailedEvent is the event type of WorkflowFailed
const WorkflowFailedEvent common.EventType = common.EventTypePrefix + "WorkflowFailed"

// WorkflowSucceeded is an event emitted when workflow succeeded
type WorkflowSucceeded struct {
	// provider that the workflow operates on
	Provider common.ID `json:"provider,omitempty"`
	// name of the workflow
	WorkflowName string `json:"workflow_name"`
	// status of the Workflow
	Status WorkflowStatus `json:"status"`
	// assume workflow output is a json object
	WfOutputs map[string]interface{} `json:"workflow_outputs"`
	Metadata  map[string]interface{} `json:"metadata"`
}

// WorkflowFailed is an event emitted when workflow failed or errored
type WorkflowFailed struct {
	// provider that the workflow operates on
	Provider common.ID `json:"provider,omitempty"`
	// name of the workflow
	WorkflowName string `json:"workflow_name"`
	// status of the Workflow
	Status WorkflowStatus `json:"status"`
	// status of the failed node
	NodeStatus map[string]NodeStatus `json:"node_status"`
	// assume workflow output is a json object
	WfOutputs map[string]interface{} `json:"workflow_outputs"`
	Metadata  map[string]interface{} `json:"metadata"`
}

// WorkflowStatus is the status of workflow
type WorkflowStatus string

// Workflow statuses
const (
	WfPending   WorkflowStatus = "Pending"
	WfRunning   WorkflowStatus = "Running"
	WfSucceeded WorkflowStatus = "Succeeded"
	WfSkipped   WorkflowStatus = "Skipped"
	WfFailed    WorkflowStatus = "Failed"
	WfError     WorkflowStatus = "Error"
	WfOmitted   WorkflowStatus = "Omitted"
)

// NodeStatus is status of a node in workflow
type NodeStatus string

// Node statuses
const (
	NodePending   NodeStatus = "Pending"
	NodeRunning   NodeStatus = "Running"
	NodeSucceeded NodeStatus = "Succeeded"
	NodeSkipped   NodeStatus = "Skipped"
	NodeFailed    NodeStatus = "Failed"
	NodeError     NodeStatus = "Error"
	NodeOmitted   NodeStatus = "Omitted"
)
