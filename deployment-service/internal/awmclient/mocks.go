package awmclient

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
)

// ArgoWorkflowMediatorMock is a mock client for Argo Workflow Mediator.
// This mock is written because mockery does not support function as argument.
type ArgoWorkflowMediatorMock struct {
	workflowName string
	err          error

	// ensure only called once
	Called bool

	Provider         common.ID
	Username         string
	WorkflowFilename string
	TFWfData         TerraformWorkflowData
}

// NewArgoWorkflowMediatorMock ...
func NewArgoWorkflowMediatorMock(returnWorkflowName string, returnError error) *ArgoWorkflowMediatorMock {
	return &ArgoWorkflowMediatorMock{workflowName: returnWorkflowName, err: returnError}
}

// Create ...
func (awm *ArgoWorkflowMediatorMock) Create(
	ctx context.Context,
	provider common.ID,
	username string,
	workflowFilename string,
	opts ...AWMWorkflowCreateOpt,
) (wfName string, err error) {
	if awm.Called {
		panic("already called")
	}
	awm.Provider = provider
	awm.Username = username
	awm.WorkflowFilename = workflowFilename
	var options awmWorkflowCreateOptions
	for _, opt := range opts {
		opt(&options)
	}
	awm.TFWfData = options.tfWfData
	awm.Called = true
	return awm.workflowName, awm.err
}

// CreateAsync is not implemented
func (awm *ArgoWorkflowMediatorMock) CreateAsync(
	provider common.ID,
	username string,
	workflowFilename string,
	opts ...AWMWorkflowCreateOpt,
) error {
	panic("not implemented")
}
