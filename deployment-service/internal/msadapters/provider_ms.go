package msadapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// ProviderMicroservice is a client to provider microservice
type ProviderMicroservice struct {
	queryConn messaging2.QueryConnection
}

// NewProviderMicroservice ...
func NewProviderMicroservice(queryConn messaging2.QueryConnection) ProviderMicroservice {
	return ProviderMicroservice{
		queryConn: queryConn,
	}
}

// Get fetch a provider by ID
func (p ProviderMicroservice) Get(actor service.Actor, id common.ID) (*service.ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "ProviderMicroservice.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"provider": id,
	})
	client, err := service.NewNatsProviderClientFromConn(p.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create provider svc client")
		return nil, err
	}
	provider, err := client.Get(context.TODO(), actor, id)
	if err != nil {
		logger.WithError(err).Error("fail to get provider")
		return nil, err
	}
	logger.WithFields(log.Fields{
		"name": provider.Name,
		"type": provider.Type,
		"url":  provider.URL,
	}).Trace("got provider from provider ms")
	return provider, nil
}

// List fetches a list of provider
func (p ProviderMicroservice) List(actor service.Actor) ([]service.ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderMicroservice.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})
	client, err := service.NewNatsProviderClientFromConn(p.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create provider svc client")
		return nil, err
	}
	providerList, err := client.List(context.TODO(), actor)
	if err != nil {
		logger.WithError(err).Error("fail to list providers")
		return nil, err
	}
	logger.WithFields(log.Fields{
		"len": len(providerList),
	}).Trace("got provider list from provider ms")
	return providerList, nil
}
