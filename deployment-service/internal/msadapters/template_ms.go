package msadapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging2"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// TemplateMicroservice is a client for template microservice
type TemplateMicroservice struct {
	queryConn messaging2.QueryConnection
}

// NewTemplateMicroservice ...
func NewTemplateMicroservice(queryConn messaging2.QueryConnection) TemplateMicroservice {
	return TemplateMicroservice{
		queryConn: queryConn,
	}
}

// Get fetches a template by ID
func (t TemplateMicroservice) Get(actor service.Actor, id common.ID) (service.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "TemplateMicroservice.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"template": id,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	client, err := service.NewNatsTemplateClientFromConn(t.queryConn, nil)
	if err != nil {
		errorMessage := "fail to create template svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	template, err := client.Get(context.TODO(), actor, id)
	if err != nil {
		errorMessage := "fail to get template"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got template from template ms")
	return template, nil
}

// GetTemplateType fetches a template type by name
func (t TemplateMicroservice) GetTemplateType(actor service.Actor, templateTypeName service.TemplateTypeName) (service.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":      "msadapters",
		"function":     "TemplateMicroservice.GetTemplateType",
		"actor":        actor.Actor,
		"emulator":     actor.Emulator,
		"templateType": templateTypeName,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	client, err := service.NewNatsTemplateClientFromConn(t.queryConn, nil)
	if err != nil {
		errorMessage := "fail to create template svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	templateType, err := client.GetType(context.TODO(), actor, templateTypeName)
	if err != nil {
		errorMessage := "fail to get template type"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got template type from template ms")
	return templateType, nil
}
