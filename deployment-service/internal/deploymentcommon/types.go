package deploymentcommon

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Creator is the creator of deployments
type Creator struct {
	User     string `bson:"creator"`
	Emulator string `bson:"creator_emulator"`
}

// CredentialID is the ID of credential
type CredentialID string

// String ...
func (c CredentialID) String() string {
	return string(c)
}

// ProviderCredentialPair is a mapping between a cloud provider and a cloud credential.
type ProviderCredentialPair struct {
	Credential CredentialID `bson:"credential"`
	Provider   common.ID    `bson:"provider"`
}

// ProviderCredentialMappings is a list of mappings between cloud providers and cloud credentials.
type ProviderCredentialMappings []ProviderCredentialPair

// ConvertToExternal converts to map[credID]providerID
func (mapping ProviderCredentialMappings) ConvertToExternal() map[string]common.ID {
	var result = map[string]common.ID{}
	for _, pair := range mapping {
		result[pair.Credential.String()] = pair.Provider
	}
	return result
}

const (
	// DeploymentNameMaxLength is max length for name
	DeploymentNameMaxLength = 100
	// DeploymentDescriptionMaxLength is max length for description
	DeploymentDescriptionMaxLength = 255
	// DeploymentStatusMsgMaxLength is max length for status msg
	DeploymentStatusMsgMaxLength = 255
)

// DeploymentRunStatus is the status of a deployment run
type DeploymentRunStatus string

// String ...
func (status DeploymentRunStatus) String() string {
	return string(status)
}

const (
	// DeploymentRunPendingCreation is an internal status for run request that is pending. This will be used for placeholder record.
	DeploymentRunPendingCreation DeploymentRunStatus = "pending"
	// DeploymentRunPreflight ...
	DeploymentRunPreflight DeploymentRunStatus = "pre-flight"
	// DeploymentRunRunning ...
	DeploymentRunRunning DeploymentRunStatus = "running"
	// DeploymentRunActive ...
	DeploymentRunActive DeploymentRunStatus = "active"
	// DeploymentRunErrored ...
	DeploymentRunErrored DeploymentRunStatus = "errored"
)

// DeploymentParameters ...
type DeploymentParameters []DeploymentParameter

// ConvertToExternal converts to external representation
func (dp DeploymentParameters) ConvertToExternal() []service.DeploymentParameter {
	external := make([]service.DeploymentParameter, 0)
	for _, param := range dp {
		external = append(external, param.ConvertToExternal())
	}
	return external
}

// ToParameterValues convert parameters to parameter values
func (dp DeploymentParameters) ToParameterValues() service.DeploymentParameterValues {
	if len(dp) == 0 {
		return map[string]interface{}{}
	}
	var paramValues = make(map[string]interface{})
	for _, param := range []DeploymentParameter(dp) {
		paramValues[param.Name] = param.Value
	}
	return paramValues
}

// DeploymentParameter contain the type and value of a parameter to deployment
type DeploymentParameter struct {
	Name  string      `bson:"name"`
	Type  string      `bson:"type"` // int,string,bool,float,uuid,url,secret, etc.
	Value interface{} `bson:"value"`
}

// ConvertToExternal converts to external representation
func (dp DeploymentParameter) ConvertToExternal() service.DeploymentParameter {
	return service.DeploymentParameter{
		Name:  dp.Name,
		Type:  dp.Type,
		Value: dp.Value,
	}
}

// RawStateType is type of raw state
type RawStateType string

// RawDeploymentState is the raw state of a deployment (run)
type RawDeploymentState struct {
	Type    RawStateType    `bson:"type"`
	TFState *TerraformState `bson:"terraform"`
	//State   interface{}     `bson:"state,omitempty"`
}

// GitUpstream is a git upstream source
type GitUpstream struct {
	Branch string `bson:"branch"`
	Tag    string `bson:"tag"`
}

// TemplateSnapshot is a snapshot in time of a template.
// It contains info that can be used to identify a specific instance template.
type TemplateSnapshot struct {
	TemplateID        common.ID   `bson:"template_id"`
	TemplateVersionID common.ID   `bson:"template_version_id"`
	UpstreamTracked   GitUpstream `bson:",inline"`
	GitURL            string      `bson:"git_url"`
	CommitHash        string      `bson:"git_commit"`
	// Path within the git repo, empty means root dir of repo
	SubPath string `bson:"sub_path"`
}

// GetTemplateSnapshot get a snapshot of a template
func GetTemplateSnapshot(templateVersion service.TemplateVersion) (TemplateSnapshot, error) {
	var gitBranch string
	var gitTag string
	var gitCommit string
	var subPath string
	_, ok := templateVersion.Source.AccessParameters["branch"]
	if ok {
		gitBranch = templateVersion.Source.AccessParameters["branch"].(string)
	}
	_, ok = templateVersion.Source.AccessParameters["tag"]
	if ok {
		gitTag = templateVersion.Source.AccessParameters["tag"].(string)
	}
	_, ok = templateVersion.Source.AccessParameters["commit"]
	if ok {
		gitCommit = templateVersion.Source.AccessParameters["commit"].(string)
	}
	_, ok = templateVersion.Source.AccessParameters["path"]
	if ok {
		subPath = templateVersion.Source.AccessParameters["path"].(string)
	}
	return TemplateSnapshot{
		TemplateID:        templateVersion.TemplateID,
		TemplateVersionID: templateVersion.ID,
		UpstreamTracked: GitUpstream{
			Branch: gitBranch,
			Tag:    gitTag,
		},
		GitURL:     templateVersion.Source.URI,
		CommitHash: gitCommit,
		SubPath:    subPath,
	}, nil
}

// DeploymentStateView is a higher level view of raw state
type DeploymentStateView service.DeploymentStateView

// ConvertToExternal converts to external representation
func (sv DeploymentStateView) ConvertToExternal() service.DeploymentStateView {
	return service.DeploymentStateView(sv)
}

// DeploymentStateViewFromExternal converts external representation of state view to internal one.
func DeploymentStateViewFromExternal(state service.DeploymentStateView) DeploymentStateView {
	return DeploymentStateView(state)
}
