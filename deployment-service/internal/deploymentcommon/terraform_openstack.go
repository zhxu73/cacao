package deploymentcommon

const openstackComputeInstanceV2 = "openstack_compute_instance_v2"

// OpenstackComputeInstance openstack_compute_instance_v2
type OpenstackComputeInstance struct {
	AccessIPV4  string `json:"access_ip_v4" mapstructure:"access_ip_v4"`
	AccessIPV6  string `json:"access_ip_v6" mapstructure:"access_ip_v6"`
	AdminPass   string `json:"admin_pass" mapstructure:"admin_pass"`
	BlockDevice []struct {
		SourceType          string `json:"source_type" mapstructure:"source_type"`
		UUID                string `json:"uuid" mapstructure:"uuid"`
		VolumeSize          int    `json:"volume_size" mapstructure:"volume_size"`
		DestinationType     string `json:"destination_type" mapstructure:"destination_type"`
		BootIndex           int    `json:"boot_index" mapstructure:"boot_index"`
		DeleteOnTermination *bool  `json:"delete_on_termination" mapstructure:"delete_on_termination"`
		GuestFormat         string `json:"guest_format" mapstructure:"guest_format"`
		VolumeType          string `json:"volume_type" mapstructure:"volume_type"`
		DeviceType          string `json:"device_type" mapstructure:"device_type"`
		DiskBus             string `json:"disk_bus" mapstructure:"disk_bus"`
	} `json:"block_device" mapstructure:"block_device"`
	ID        string `json:"id" mapstructure:"id"`
	ImageID   string `json:"image_id" mapstructure:"image_id"`
	ImageName string `json:"image_name" mapstructure:"image_name"`
	KeyPair   string `json:"key_pair" mapstructure:"key_pair"`
	Name      string `json:"name" mapstructure:"name"`
	Network   []struct {
		IPV4 string `json:"fixed_ip_v4" mapstructure:"fixed_ip_v4"`
		IPV6 string `json:"fixed_ip_v6" mapstructure:"fixed_ip_v6"`
		Mac  string `json:"mac" mapstructure:"mac"`
		Name string `json:"name" mapstructure:"name"`
		UUID string `json:"uuid" mapstructure:"uuid"`
		Port string `json:"port" mapstructure:"port"`
	} `json:"network" mapstructure:"network"`
	PowerState    string   `json:"power_state" mapstructure:"power_state"`
	Region        string   `json:"region" mapstructure:"region"`
	SecurityGroup []string `json:"security_groups" mapstructure:"security_groups"`
	UserData      string   `json:"user_data" mapstructure:"user_data"`
	Volume        []struct {
		ID       string `json:"id" mapstructure:"id"`
		VolumeID string `json:"volume_id" mapstructure:"volume_id"`
		Device   string `json:"device" mapstructure:"device"`
	} `json:"volume" mapstructure:"volume"`
	SchedulerHints []SchedulerHint `json:"scheduler_hints" mapstructure:"scheduler_hints"`
}

const openstackComputeFloatingIPAssociate = "openstack_compute_floatingip_associate_v2"

// OpenstackComputeFloatingIPAssociate openstack_compute_floatingip_associate_v2
type OpenstackComputeFloatingIPAssociate struct {
	FixedIP             string      `json:"fixed_ip" mapstructure:"fixed_ip"`
	FloatingIP          string      `json:"floating_ip" mapstructure:"floating_ip"`
	ID                  string      `json:"id" mapstructure:"id"`                   // "111.111.111.111/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/"
	InstanceID          string      `json:"instance_id" mapstructure:"instance_id"` // uuid
	Region              string      `json:"region" mapstructure:"region"`
	Timeouts            interface{} `json:"timeouts" mapstructure:"timeouts"` // FIXME null
	WaitUntilAssociated *bool       `json:"wait_until_associated" mapstructure:"wait_until_associated"`
}

const openstackNetworkFloatingIPAssociate = "openstack_networking_floatingip_associate_v2"

// OpenstackNetworkFloatingIPAssociate openstack_networking_floatingip_associate_v2
type OpenstackNetworkFloatingIPAssociate struct {
	FixedIP    string `json:"fixed_ip" mapstructure:"fixed_ip"`
	FloatingIP string `json:"floating_ip" mapstructure:"floating_ip"`
	ID         string `json:"id" mapstructure:"id"` // "111.111.111.111/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/"
	PortID     string `json:"port_id" mapstructure:"port_id"`
	Region     string `json:"region" mapstructure:"region"`
}

const openstackNetworkingFloatingipV2 = "openstack_networking_floatingip_v2"

// OpenstackNetworkingFloatingIP openstack_networking_floatingip_v2
type OpenstackNetworkingFloatingIP struct {
	Address     string      `json:"address" mapstructure:"address"`
	AllTags     []string    `json:"all_tags" mapstructure:"all_tags"`
	Description string      `json:"description" mapstructure:"description"`
	DNSDomain   string      `json:"dns_domain" mapstructure:"dns_domain"`
	DNSName     string      `json:"dns_name" mapstructure:"dns_name"`
	FixedIP     string      `json:"fixed_ip" mapstructure:"fixed_ip"`
	ID          string      `json:"id" mapstructure:"id"`     // uuid
	Pool        string      `json:"pool" mapstructure:"pool"` // "ext-net"
	PortID      string      `json:"port_id" mapstructure:"port_id"`
	Region      string      `json:"region" mapstructure:"region"`       // "RegionOne"
	SubnetID    string      `json:"subnet_id" mapstructure:"subnet_id"` // uuid
	Tags        []string    `json:"tags" mapstructure:"tags"`
	TenantID    string      `json:"tenant_id" mapstructure:"tenant_id"` // uuid
	Timeouts    interface{} `json:"timeouts" mapstructure:"timeouts"`   // FIXME null
	ValueSpecs  []string    `json:"value_specs" mapstructure:"value_specs"`
}

const openstackBlockstorageVolumeV3 = "openstack_blockstorage_volume_v3"

// OpenstackBlockStorageVolume openstack_blockstorage_volume_v3
type OpenstackBlockStorageVolume struct {
	ID                 string                 `json:"id" mapstructure:"id"` // FIXME verify that volume has this field
	Region             string                 `json:"region" mapstructure:"region"`
	Size               int                    `json:"size" mapstructure:"size"`
	EnableOnlineResize *bool                  `json:"enable_online_resize" mapstructure:"enable_online_resize"`
	Name               string                 `json:"name" mapstructure:"name"`
	Description        string                 `json:"Description" mapstructure:"Description"`
	AvailabilityZone   string                 `json:"availability_zone" mapstructure:"availability_zone"`
	Metadata           map[string]interface{} `json:"metadata" mapstructure:"metadata"`
	SnapshotID         string                 `json:"snapshot_id" mapstructure:"snapshot_id"`
	SourceVolID        string                 `json:"source_vol_id" mapstructure:"source_vol_id"`
	ImageID            string                 `json:"image_id" mapstructure:"image_id"`
	VolumeType         string                 `json:"volume_type" mapstructure:"volume_type"`
	ConsistencyGroupID string                 `json:"consistency_group_id" mapstructure:"consistency_group_id"`
	SourceReplica      string                 `json:"source_replica" mapstructure:"source_replica"`
	Multiattach        *bool                  `json:"multiattach" mapstructure:"multiattach"`
	Attachment         []struct {
		ID         string `json:"id" mapstructure:"id"`
		InstanceID string `json:"instance_id" mapstructure:"instance_id"`
		Device     string `json:"device" mapstructure:"device"`
	} `json:"attachment" mapstructure:"attachment"`
	SchedulerHints []SchedulerHint `json:"scheduler_hints" mapstructure:"scheduler_hints"`
}

// SchedulerHint is the scheduler_hints field shared by many openstack resource
type SchedulerHint struct {
	DifferentHost        []string               `json:"different_host" mapstructure:"different_host"`
	SameHost             []string               `json:"same_host" mapstructure:"same_host"`
	Query                string                 `json:"query" mapstructure:"query"`
	LocalToInstance      string                 `json:"local_to_instance" mapstructure:"local_to_instance"`
	AdditionalProperties map[string]interface{} `json:"additional_properties" mapstructure:"additional_properties"`
}
