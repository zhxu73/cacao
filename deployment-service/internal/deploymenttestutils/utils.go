package deploymenttestutils

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"reflect"
	"testing"
)

/*
// AssertRunEqual assert 2 runs being equal except for certain fields
func AssertRunEqual(t *testing.T, r1 deploymentcommon.DeploymentRun, r2 deploymentcommon.DeploymentRun) {
	run1 := reflect.ValueOf(r1)
	run1Type := reflect.TypeOf(r1)
	run2 := reflect.ValueOf(r2)
	for i := 0; i < run1.NumField(); i++ {
		switch run1Type.Field(i).Name {
		case "CreatedAt":
		case "EndsAt":
		case "StateUpdatedAt":
		case "Parameters":
			assert.ElementsMatch(t, run1.Field(i).Interface(), run2.Field(i).Interface())
		default:
			assert.Equal(t, run1.Field(i).Interface(), run2.Field(i).Interface())
		}
	}
}
*/

// AssertDeploymentEqual assert 2 deployments being equal except for certain fields
func AssertDeploymentEqual(t *testing.T, d1 types.Deployment, d2 types.Deployment) {
	deployment1 := reflect.ValueOf(d1)
	deployment1Type := reflect.TypeOf(d1)
	deployment2 := reflect.ValueOf(d2)
	for i := 0; i < deployment1.NumField(); i++ {
		switch deployment1Type.Field(i).Name {
		case "CreatedAt":
		case "UpdatedAt":
		case "CloudCredentials":
			assert.ElementsMatch(t, deployment1.Field(i).Interface(), deployment2.Field(i).Interface())
		default:
			assert.Equal(t, deployment1.Field(i).Interface(), deployment2.Field(i).Interface())
		}
	}
}
