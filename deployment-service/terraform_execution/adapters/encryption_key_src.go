package adapters

import (
	"encoding/base64"
	log "github.com/sirupsen/logrus"
	"os"
)

// EncryptionKeyFile reads encryption key from file.
type EncryptionKeyFile struct {
	key []byte
}

// NewEncryptionKeyFile ...
func NewEncryptionKeyFile(filename string) *EncryptionKeyFile {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "NewEncryptionKeyFile",
	})
	fileContent, err := os.ReadFile(filename)
	if err != nil {
		logger.Fatalf("fail to read key file %s", filename)
	}
	decoded, err := base64.StdEncoding.DecodeString(string(fileContent))
	if err != nil {
		logger.WithError(err).Fatal(err)
	}
	return &EncryptionKeyFile{key: decoded}
}

// GetKey ...
func (e EncryptionKeyFile) GetKey() []byte {
	return e.key
}

// EncryptionKeyEnvVar reads base64 encoded encryption key from env var.
type EncryptionKeyEnvVar struct {
	key []byte
}

// NewEncryptionKeyEnvVar ...
func NewEncryptionKeyEnvVar(envVar string) *EncryptionKeyEnvVar {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "NewEncryptionKeyEnvVar",
	})
	keyBase64, ok := os.LookupEnv(envVar)
	if !ok {
		logger.Fatalf("encryption key is not present in env var %s", envVar)
	}
	decoded, err := base64.StdEncoding.DecodeString(keyBase64)
	if err != nil {
		logger.WithError(err).Fatal("key from env var is not valid base64 string")
	}
	return &EncryptionKeyEnvVar{key: decoded}
}

// GetKey ...
func (e EncryptionKeyEnvVar) GetKey() []byte {
	return e.key
}
