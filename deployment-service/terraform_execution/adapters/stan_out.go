package adapters

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
)

// StanAdapterOutgoing ...
type StanAdapterOutgoing struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventSink = StanAdapterOutgoing{}

// EventRunPreflightStarted ...
func (s StanAdapterOutgoing) EventRunPreflightStarted(started deploymentevents.RunPreflightStarted) {
	s.publish(deploymentevents.EventRunPreflightStarted, started)
}

// EventRunPreflightStartFailed ...
func (s StanAdapterOutgoing) EventRunPreflightStartFailed(failed deploymentevents.RunPreflightStartFailed) {
	s.publish(deploymentevents.EventRunPreflightStartFailed, failed)
}

// EventRunExecutionFailed ...
func (s StanAdapterOutgoing) EventRunExecutionFailed(failed deploymentevents.RunExecutionFailed) {
	s.publish(deploymentevents.EventRunExecutionFailed, failed)
}

// EventRunExecutionStarted ...
func (s StanAdapterOutgoing) EventRunExecutionStarted(started deploymentevents.RunExecutionStarted) {
	s.publish(deploymentevents.EventRunExecutionStarted, started)
}

// EventDeploymentDeletionCleanupFailed ...
func (s StanAdapterOutgoing) EventDeploymentDeletionCleanupFailed(result deploymentevents.DeploymentDeletionCleanupResult) {
	s.publish(deploymentevents.EventDeploymentDeletionCleanupFailed, result)
}

// EventDeploymentDeletionCleanupStarted ...
func (s StanAdapterOutgoing) EventDeploymentDeletionCleanupStarted(started deploymentevents.DeploymentDeletionCleanupStarted) {
	s.publish(deploymentevents.EventDeploymentDeletionCleanupStarted, started)
}

// EventRunPreflightSucceeded ...
func (s StanAdapterOutgoing) EventRunPreflightSucceeded(result deploymentevents.RunPreflightResult) {
	s.publish(deploymentevents.EventRunPreflightSucceeded, result)
}

// EventRunPreflightFailed ...
func (s StanAdapterOutgoing) EventRunPreflightFailed(result deploymentevents.RunPreflightResult) {
	s.publish(deploymentevents.EventRunPreflightFailed, result)
}

// EventDeploymentDeletionCleanupSucceeded ...
func (s StanAdapterOutgoing) EventDeploymentDeletionCleanupSucceeded(result deploymentevents.DeploymentDeletionCleanupResult) {
	s.publish(deploymentevents.EventDeploymentDeletionCleanupSucceeded, result)
}

// EventRunExecutionSucceeded ...
func (s StanAdapterOutgoing) EventRunExecutionSucceeded(succeeded deploymentevents.RunExecutionSucceeded) {
	s.publish(deploymentevents.EventRunExecutionSucceeded, succeeded)
}

func (s StanAdapterOutgoing) publish(eventType common.EventType, eventBody interface{}) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapterOutgoing.publish",
		"ceType":   eventType,
	})
	ce, err := messaging2.CreateCloudEventWithAutoSource(eventBody, eventType)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return
	}
	err = s.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Error("fail to write events")
		return
	}
}
