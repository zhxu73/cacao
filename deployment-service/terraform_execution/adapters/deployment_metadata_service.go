package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// DeploymentMetadataService is a client to interact with deployment metadata service
type DeploymentMetadataService struct {
	client service.DeploymentClient
}

// NewDeploymentMetadataService ...
func NewDeploymentMetadataService(conn messaging2.QueryConnection) DeploymentMetadataService {
	client, err := service.NewDeploymentClientFromConn(conn, nil)
	if err != nil {
		log.WithError(err).Panic()
	}
	return DeploymentMetadataService{
		client: client,
	}
}

// Get fetches a deployment by ID
func (svc DeploymentMetadataService) Get(actor types.Actor, id common.ID) (service.Deployment, error) {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "DeploymentMetadataService.Get",
		"actor":      actor.Actor,
		"emulator":   actor.Emulator,
		"deployment": id,
	})
	deployment, err := svc.client.Get(context.TODO(), actor, id)
	if err != nil {
		logger.WithError(err).Error("fail to get deployment")
		return service.Deployment{}, err
	}
	logger.Debug("got deployment from deployment metadata ms")
	return *deployment, nil
}
