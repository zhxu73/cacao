package adapters

// SSHKeySrc stores the master SSH key that will be injected into users' VMs to provide CACAO access to users' VMs.
type SSHKeySrc struct {
	SSHKey string
}

// GetPublicSSHKey ...
func (s SSHKeySrc) GetPublicSSHKey() string {
	return s.SSHKey
}
