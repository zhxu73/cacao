package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"sync"
)

// QueryAdapter is an adapter for NATS, implements ports.ReplySink and ports.QuerySrc
type QueryAdapter struct {
	conf messaging2.NatsConfig
	conn messaging2.QueryConnection
}

// NewQueryAdapter ...
func NewQueryAdapter(qc messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{
		conn: qc,
	}
}

// Start ...
func (a *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup, handlers ports.QueryHandlers) error {
	err := a.conn.Listen(ctx, map[common.QueryOp]messaging2.QueryHandlerFunc{
		service.DeploymentGetLogsLocationQueryType: func(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
			logger := log.WithField("querOp", service.DeploymentGetLogsLocationQueryType)
			var request service.DeploymentLogsLocationQuery
			err := json.Unmarshal(requestCe.Data(), &request)
			if err != nil {
				logger.WithError(err).Errorf("fail to unmarshal request")
				replyCe, err1 := a.unmarshalErrReply()
				if err1 != nil {
					logger.WithError(err1).Errorf("fail to create ce for unmarshal err reply")
					return
				}
				err1 = writer.Write(replyCe)
				if err1 != nil {
					logger.WithError(err1).Errorf("fail to write reply")
					return
				}
				return
			}
			reply := handlers.ListLogsLocation(request)
			replyCe, err := reply.ToCloudEvent(messaging2.AutoPopulateCloudEventSource)
			if err != nil {
				logger.WithError(err).Errorf("fail to convert reply to cloudevent")
				return
			}
			err1 := writer.Write(replyCe)
			if err1 != nil {
				logger.WithError(err1).Errorf("fail to write reply")
				return
			}
			logger.Debug("query reply-ed")
		},
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

func (a *QueryAdapter) unmarshalErrReply() (cloudevents.Event, error) {
	return messaging2.CreateCloudEventWithAutoSource(service.DeploymentLogsLocationReply{
		Session: service.Session{
			SessionActor:    "",
			SessionEmulator: "",
			ServiceError:    service.NewCacaoMarshalError("fail to unmarshal request into DeploymentLogsLocationQuery struct").GetBase(),
		},
	}, service.DeploymentGetLogsLocationQueryType)
}
