package adapters

import (
	"errors"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// TFRunCollectionName is mongodb collection name for Terraform Run
const TFRunCollectionName = "deploymentRunTF"

// TFRunMongoStorage is mongodb storage for Terraform Run
type TFRunMongoStorage struct {
	conf db.MongoDBConfig
	conn *db.MongoDBConnection
}

// NewTFRunMongoStorage ...
func NewTFRunMongoStorage(conf db.MongoDBConfig) *TFRunMongoStorage {
	return &TFRunMongoStorage{conf: conf}
}

// Init initialize the mongo connection
func (storage *TFRunMongoStorage) Init() error {
	conn, err := db.NewMongoDBConnection(&storage.conf)
	if err != nil {
		return err
	}
	storage.conn = conn
	return nil
}

// Get fetches a run based on ID
func (storage TFRunMongoStorage) Get(runID common.ID) (types.DeploymentRun, error) {
	if !runID.Validate() || runID.FullPrefix() != service.RunIDPrefix {
		return types.DeploymentRun{}, errors.New("bad run ID")
	}
	var run types.DeploymentRun
	filter := map[string]interface{}{
		"_id": runID.String(),
	}
	err := storage.conn.Get(TFRunCollectionName, filter, &run)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return types.DeploymentRun{}, service.NewCacaoNotFoundError("tf run not found")
		}
		return types.DeploymentRun{}, err
	}
	return run, nil
}

// Create inserts a run
func (storage TFRunMongoStorage) Create(run types.DeploymentRun) error {
	err := storage.validateBeforeInsert(run)
	if err != nil {
		return err
	}
	err = storage.conn.Insert(TFRunCollectionName, run)
	if err != nil {
		return err
	}
	return nil
}

func (storage TFRunMongoStorage) validateBeforeInsert(run types.DeploymentRun) error {
	if !run.ID.Validate() || run.ID.FullPrefix() != service.RunIDPrefix {
		return errors.New("bad run ID")
	}
	//run.Deployment
	//run.Status
	return nil
}

// Update ...
func (storage TFRunMongoStorage) Update(runID common.ID, update types.DeploymentRunUpdate, filter types.DeploymentRunFilter) (updated bool, err error) {
	filter.ID = runID // ensure the ID is set in filter
	ok, err := storage.conn.Update(TFRunCollectionName, filter.ToBSON(), bson.M{"$set": update.ToBSON()})
	if err != nil {
		return ok, err
	}
	return ok, err
}

// Search ...
func (storage TFRunMongoStorage) Search(filter types.DeploymentRunFilter) ([]types.DeploymentRun, error) {
	var result []types.DeploymentRun
	err := storage.conn.List(TFRunCollectionName, filter.ToBSON(), result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Close ...
func (storage *TFRunMongoStorage) Close() error {
	if storage.conn == nil {
		return nil
	}
	return storage.conn.Disconnect()
}
