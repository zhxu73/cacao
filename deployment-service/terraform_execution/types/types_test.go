package types

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEnvConfig_validateCIDR(t *testing.T) {
	type fields struct {
		PodName            string
		LogLevel           string
		CacaoSSHKey        string
		WhitelistCIDRs     []string
		LogsStorageBaseURL string
		LogsJWTKeyBase64   string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "IP",
			fields: fields{
				WhitelistCIDRs: []string{"8.8.8.8"},
			},
			wantErr: assert.Error,
		},
		{
			name: "mix of CIDR and IP",
			fields: fields{
				WhitelistCIDRs: []string{"8.8.8.8/32", "127.0.0.1", "127.0.0.1/24"},
			},
			wantErr: assert.Error,
		},
		{
			name: "CIDR /32",
			fields: fields{
				WhitelistCIDRs: []string{"127.0.0.1/32"},
			},
			wantErr: assert.NoError,
		},
		{
			name: "CIDR",
			fields: fields{
				WhitelistCIDRs: []string{"18.34.0.0/19"},
			},
			wantErr: assert.NoError,
		},
		{
			name: "CIDR ipv6",
			fields: fields{
				WhitelistCIDRs: []string{"2600:1ff0:8000::/39", "2600:9000:a310::/48"},
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			envConf := MiscConfig{
				PodName:            tt.fields.PodName,
				LogLevel:           tt.fields.LogLevel,
				CacaoSSHKey:        tt.fields.CacaoSSHKey,
				WhitelistCIDRs:     tt.fields.WhitelistCIDRs,
				LogsStorageBaseURL: tt.fields.LogsStorageBaseURL,
				LogsJWTKeyBase64:   tt.fields.LogsJWTKeyBase64,
			}
			tt.wantErr(t, envConf.validateCIDR(), fmt.Sprintf("Validate()"))
		})
	}
}
