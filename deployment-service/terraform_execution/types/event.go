package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// DoNotSent this constant is for event that should not be sent, but has to return an event
//
// FIXME consider making return response event optional, not all cases have a valid response event.
const DoNotSent common.EventType = "DoNotSentThis"

// ResponseEvent is response event
type ResponseEvent struct {
	eventType   common.EventType
	transaction common.TransactionID
	event       interface{}
}

// NewEvent creates a new response event
func NewEvent(eventType common.EventType, transaction common.TransactionID, event interface{}) ResponseEvent {
	return ResponseEvent{eventType: eventType, transaction: transaction, event: event}
}

// EventType returns event type
func (e ResponseEvent) EventType() common.EventType {
	return e.eventType
}

// Transaction ...
func (e ResponseEvent) Transaction() common.TransactionID {
	return e.transaction
}

// ToCloudEvent converts event to cloudevent
func (e ResponseEvent) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging2.CreateCloudEventWithTransactionID(e.event, e.eventType, source, e.transaction)
}

// NewDoNotSentEvent creates a dummy event that will not be sent
func NewDoNotSentEvent() ResponseEvent {
	return ResponseEvent{eventType: DoNotSent}
}

// EventHandler is handler for events
type EventHandler interface {
	Handle(IncomingEvent) ResponseEvent
}
