package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"time"
)

// WorkflowPurpose is the purpose of which the workflow is intended to achieve
type WorkflowPurpose string

const (
	// PreflightPurpose is a workflow launched during preflight stage, e.g. to apply prerequisite template.
	PreflightPurpose WorkflowPurpose = "preflight"
	// ExecutionPurpose is a workflow launched during execution stage, e.g. to execute the actual template.
	ExecutionPurpose WorkflowPurpose = "execution"
	// DeletionPurpose is a workflow launched to delete the resources provisioned by a deployment.
	DeletionPurpose WorkflowPurpose = "deletion"
	// PrerequisiteDeletionPurpose is a workflow launched to delete the resources provisioned by prerequisite template on the primary provider of a deployment.
	PrerequisiteDeletionPurpose WorkflowPurpose = "prerequisite_deletion"
)

// DeploymentWorkflow is record of workflow launched for a deployment run or any deployment actions.
// The unique index should be the combination of AWMProvider and WorkflowName.
type DeploymentWorkflow struct {
	WorkflowName string                   `bson:"wf_name"`
	AWMProvider  common.ID                `bson:"provider"`
	Purpose      WorkflowPurpose          `bson:"purpose"`
	Deployment   common.ID                `bson:"deployment"`
	TemplateType service.TemplateTypeName `bson:"deployment_type"`
	Run          common.ID                `bson:"run"`
	CreatedAt    time.Time                `bson:"created_at"`
	EndedAt      time.Time                `bson:"ended_at"`
	Status       string                   `bson:"status"`
	Logs         *string                  `bson:"logs"`
}
