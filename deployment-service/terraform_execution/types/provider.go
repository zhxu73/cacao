package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// OpenStackProviderMetadata contains portions of provider metadata that deployment svc needs for OpenStack provider.
type OpenStackProviderMetadata struct {
	PreRequisiteTemplate PreRequisiteTemplate `json:"prerequisite_template" mapstructure:"prerequisite_template"`
	// public ssh key to be injected into instances
	PublicSSHKey        string `json:"public_ssh_key" mapstructure:"public_ssh_key"`
	ExternalNetworkName string `json:"external_network_name" mapstructure:"external_network_name"`
	// OpenStack External Network UUID
	ExternalNetworkUUID string `json:"external_network_uuid" mapstructure:"external_network_uuid"`
	// a list of OpenStack subnets UUID that belongs to the external network
	ExternalSubnetUUIDs []string `json:"external_subnet_uuids" mapstructure:"external_subnet_uuids"`
}

// Validate returns true if the metadata is valid.
func (m OpenStackProviderMetadata) Validate() bool {
	if len(m.PreRequisiteTemplate.TemplateID) == 0 {
		return false
	}
	if len(m.PublicSSHKey) == 0 {
		return false
	}
	if len(m.ExternalNetworkName) == 0 {
		return false
	}
	if len(m.ExternalNetworkUUID) == 0 {
		return false
	}
	if len(m.ExternalSubnetUUIDs) == 0 {
		return false
	}
	return true
}

// OpenStackPreRequisiteTemplateParameterInjection is parameters to be injected into PreRequisiteTemplateParameter for OpenStack provider.
type OpenStackPreRequisiteTemplateParameterInjection struct {
	Username            string `json:"username" mapstructure:"username"`
	KeyPairName         string `json:"keypair_name" mapstructure:"keypair_name"`
	PublicSSHKey        string `json:"public_ssh_key" mapstructure:"public_ssh_key"`
	ExternalNetworkUUID string `json:"external_network_uuid" mapstructure:"external_network_uuid"`
}

// PreRequisiteTemplate is the template that is the pre-requisite for deployments.
// Note: this is provider specific, thus specified by provider metadata.
type PreRequisiteTemplate struct {
	TemplateID common.ID `json:"template_id" mapstructure:"template_id"`
	// This includes the parameter values that are static, e.g. specific to cloud provider; but not to users, or deployments.
	// param vals that are dynamic (e.g. username) will be injected at runtime.
	ParamValue service.DeploymentParameterValues `json:"param_value" mapstructure:"param_value"`
}
