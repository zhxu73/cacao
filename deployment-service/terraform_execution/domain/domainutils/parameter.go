package domainutils

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strings"
	"unicode"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
)

const (
	templateStringType = "string"
	// TemplateUsernameType is CACAO username.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateUsernameType = "cacao_username"
	// TemplateUnixUsernameType is a version of the CACAO username that is converted to be used as unix username (e.g. username to ssh into VM instances).
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateUnixUsernameType      = "cacao_username_unix"
	templateProviderFlavorType    = "cacao_provider_flavor"
	templateProviderImageType     = "cacao_provider_image"
	templateProviderImageNameType = "cacao_provider_image_name"
	templateProviderProjectType   = "cacao_provider_project"
	// TemplateProviderRegionType is the name of the region.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderRegionType = "cacao_provider_region"
	// TemplateProviderKeyPairType is name of the ssh key pair used by provider.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderKeyPairType = "cacao_provider_key_pair"
	// TemplateProviderExternalNetworkType is name of external network.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderExternalNetworkType = "cacao_provider_external_network"
	// TemplateProviderExternalSubnetType is uuid of subnet of external network.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderExternalSubnetType = "cacao_provider_external_subnet"
	// TemplateCloudInitType is cloud-init script to be passed to instance.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateCloudInitType = "cacao_cloud_init"
	// TemplateUserAllSSHKeyJSONType are all public ssh key user stored as credential in CACAO, the keys will be encoded in JSON (list of strings).
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateUserAllSSHKeyJSONType = "cacao_user_ssh_key_all_json"
	// TemplateCACAOSSHKeyType is the public ssh key of cacao. This is another way to give CACAO ssh access to provisioned hosts other that cloud-init.
	// Note: the resulting value of the parameter is not always the same, CACAO may rotate key from time to time.
	// TODO: later on, we might want to swap this for a per-deployment key that CACAO manages.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateCACAOSSHKeyType = "cacao_ssh_key"
	// TemplateCACAOWhiteListCIDRJSONType is the list of IP ranges that template should whitelist to allow CACAO to access the hosts provisioned.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateCACAOWhiteListCIDRJSONType = "cacao_white_list_cidr_json"

	templateNumberType  = "number"
	templateIntegerType = "integer"
	templateFloatType   = "float"
	templateBoolType    = "bool"
	templateListType    = "list"
	templateMapType     = "map"
)

// https://www.terraform.io/docs/language/expressions/types.html#types
const (
	tfStringType = "string"
	tfNumberType = "number"
	tfBoolType   = "bool"
	tfListType   = "list"
	tfTupleType  = "tuple"
	tfMapType    = "map"
	tfObjectType = "object"
)
const tfModuleName = "cacao_module"

// ParamConstraint is constraints on a parameter
type ParamConstraint struct {
	Type    string        `mapstructure:"type"`
	Default interface{}   `mapstructure:"default"`
	Enum    []interface{} `mapstructure:"enum"`
}

// ParameterConverter converts external representation of deployment parameter and
// template parameter definition to internal parameter representation.
type ParameterConverter struct {
	option parameterConverterOption
}

type parameterConverterOption struct {
	// override parameter value for certain parameter types (as defined in the template param def), map[paramType]paramValue
	paramOverride map[string]interface{}
}

// ParameterConverterOption option function for converting parameter
type ParameterConverterOption func(option *parameterConverterOption)

// OverrideValueForTypes override parameter value for certain parameter types (as defined in the template param def), map[paramType]paramValue
func OverrideValueForTypes(typeValueMap map[string]interface{}) ParameterConverterOption {
	return func(option *parameterConverterOption) {
		option.paramOverride = typeValueMap
	}
}

// Convert converts external representation of deployment parameter and
// template parameter definition to internal parameter representation.
func (pc *ParameterConverter) Convert(
	templateParams []service.TemplateParameter,
	paramValues service.DeploymentParameterValues,
	opts ...ParameterConverterOption) ([]deploymentcommon.DeploymentParameter, error) {

	var allResultParam = make([]deploymentcommon.DeploymentParameter, 0, len(templateParams))

	pc.option = pc.aggregateOptions(opts...)

	for _, templateParam := range templateParams {
		paramVal, ok := paramValues[templateParam.Name]
		if !ok {
			paramVal = nil
		}
		resultParam, err := pc.parseParamValue(templateParam, paramVal)
		if err != nil {
			return nil, err
		}
		allResultParam = append(allResultParam, resultParam)
	}
	return allResultParam, nil
}

// combine all option functions
func (pc ParameterConverter) aggregateOptions(opts ...ParameterConverterOption) parameterConverterOption {
	var option parameterConverterOption
	if len(opts) == 0 {
		return option
	}
	for _, opt := range opts {
		opt(&option)
	}
	return option
}

// parse a single parameter value
func (pc ParameterConverter) parseParamValue(templateParam service.TemplateParameter, paramValue interface{}) (deploymentcommon.DeploymentParameter, error) {
	paramValue, _ = pc.overrideParamValue(templateParam, paramValue)

	var result deploymentcommon.DeploymentParameter

	err := templateParam.CorrectDataType()
	if err != nil {
		return result, err
	}

	_, paramValueChecked, err := templateParam.CheckValue(paramValue)
	if err != nil {
		return result, err
	}

	result.Name = templateParam.Name
	result.Type = templateParam.Type
	result.Value = paramValueChecked
	return result, nil
}

// override the parameter value based on the option.
// return the resulting parameter value, and a flag indicating whether the value is overridden (true=overridden).
func (pc ParameterConverter) overrideParamValue(templateParam service.TemplateParameter, paramValue interface{}) (interface{}, bool) {
	if pc.option.paramOverride == nil || len(pc.option.paramOverride) == 0 {
		return paramValue, false
	}
	switch templateParam.Type {
	case TemplateUnixUsernameType:
		username, ok := pc.option.paramOverride[TemplateUsernameType] // re-use override value for cacao_username
		if ok {
			return unixUsername(username.(string)), true
		}
	case TemplateUsernameType:
		fallthrough
	case TemplateProviderRegionType:
		fallthrough
	case TemplateProviderKeyPairType:
		fallthrough
	case TemplateProviderExternalNetworkType:
		fallthrough
	case TemplateProviderExternalSubnetType:
		fallthrough
	case TemplateCloudInitType:
		fallthrough
	case TemplateUserAllSSHKeyJSONType:
		fallthrough
	case TemplateCACAOSSHKeyType:
		fallthrough
	case TemplateCACAOWhiteListCIDRJSONType:
		valueToOverride, ok := pc.option.paramOverride[templateParam.Type]
		if ok {
			return valueToOverride, true
		}
	}
	return paramValue, false
}

// unixUsername convert the full username to one that is suit-able to be used as
// username for unix system (e.g. ssh into linux VM).
//
// check man page of useradd cmd for reference on username convention.
func unixUsername(fullUsername string) string {
	index := 0
	fullUsername = strings.ToLower(fullUsername)
	for i, c := range fullUsername {
		index = i
		if c > unicode.MaxASCII {
			break
		}
		if i == 0 && !unicode.IsLetter(c) {
			// if first letter is not letter
			break
		}
		if unicode.IsLetter(c) {
			continue
		}
		if unicode.IsDigit(c) {
			continue
		}
		if c == '_' || c == '-' {
			continue
		}
		break
	}
	if index == 0 {
		// If the first character is not letter, then fallback to hashing the username (first 8 char of hex encoded sha256).
		// To get the username on cmd line, you can run the following cmd and enter the username:
		// cat | tr -d '\n' | sha256sum | head -c 8 | xargs printf "user-%s" && echo ""
		hash := sha256.Sum256([]byte(fullUsername))
		return "user-" + hex.EncodeToString(hash[:])[:8]
	}
	// 32 limit is from useradd cmd
	if index > 32 {
		index = 32
	}
	if index == len(fullUsername)-1 {
		return fullUsername
	}
	return fullUsername[:index]
}

// EmptyParameters populate with empty (nil) as the value for parameters
func (pc ParameterConverter) EmptyParameters(
	templateParam map[string]interface{}) ([]deploymentcommon.DeploymentParameter, error) {

	var allResultParam = make([]deploymentcommon.DeploymentParameter, 0, len(templateParam))

	for paramName, paramConstraintRaw := range templateParam {
		var constraint ParamConstraint
		err := mapstructure.Decode(paramConstraintRaw, &constraint)
		if err != nil {
			return nil, err
		}
		var resultParam = deploymentcommon.DeploymentParameter{
			Name:  paramName,
			Type:  constraint.Type,
			Value: nil,
		}
		allResultParam = append(allResultParam, resultParam)
	}
	return allResultParam, nil
}

// ParamToAnsibleVars convert parameter to ansible variables
func ParamToAnsibleVars(parameters []deploymentcommon.DeploymentParameter) (map[string]interface{}, error) {
	var ansibleVars = make(map[string]interface{})
	ansibleVars["TF_MODULE"] = tfModuleName
	inputValues := map[string]interface{}{}
	inputVars := map[string]interface{}{}
	for _, param := range parameters {
		tfType, err := convertTemplateParamTypeToTerraformType(param.Type)
		if err != nil {
			return nil, err
		}
		inputVars[param.Name] = map[string]string{
			"type": tfType,
		}
		inputValues[param.Name] = param.Value
	}
	ansibleVars["TF_INPUT_VALUES"] = inputValues
	ansibleVars["TF_INPUT_VARS"] = inputVars
	return ansibleVars, nil
}

func convertTemplateParamTypeToTerraformType(paramType string) (string, error) {
	switch paramType {
	case templateStringType:
		return tfStringType, nil
	case templateNumberType:
		return tfNumberType, nil
	case templateBoolType:
		return tfBoolType, nil
	case templateListType:
		return tfListType, nil
	case templateMapType:
		return tfMapType, nil
	case TemplateUsernameType:
		return tfStringType, nil
	case TemplateUnixUsernameType:
		return tfStringType, nil
	case templateProviderFlavorType:
		return tfStringType, nil
	case templateProviderImageType:
		return tfStringType, nil
	case templateProviderImageNameType:
		return tfStringType, nil
	case templateProviderProjectType:
		return tfStringType, nil
	case TemplateProviderRegionType:
		return tfStringType, nil
	case TemplateProviderKeyPairType:
		return tfStringType, nil
	case TemplateProviderExternalNetworkType:
		return tfStringType, nil
	case TemplateProviderExternalSubnetType:
		return tfStringType, nil
	case TemplateCloudInitType:
		return tfStringType, nil
	case TemplateUserAllSSHKeyJSONType:
		return tfStringType, nil
	case TemplateCACAOSSHKeyType:
		return tfStringType, nil
	case TemplateCACAOWhiteListCIDRJSONType:
		return tfStringType, nil
	case templateIntegerType:
		return tfNumberType, nil
	default:
		return "", fmt.Errorf("unknown param type, %s", paramType)
	}
}

// TemplateParameterDefHasType checks if there is a parameter that has the specified type in the template parameter definition.
func TemplateParameterDefHasType(templateParams []service.TemplateParameter, paramType string) bool {
	for _, templateParam := range templateParams {
		if templateParam.Type == paramType {
			return true
		}
	}
	return false
}

// KeyPairName generates the keypair name based on trimmed username.
func KeyPairName(username string) string {
	return "keypair-" + TrimUsername(username)
}
