package prerequisitewfdata

import (
	"errors"
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// PrerequisiteTemplateTFStateKey is the key to raw state in storage for Terraform State (e.g. tf workspace name in
// psql tf backend) for applying prerequisite template.
// Currently, the Terraform state for the prerequisite template is associated with the combination of user and primary
// cloud provider (rather than being associated with the deployment itself). Therefore, the key is unique for all
// deployment runs for a user on a specific provider.
func PrerequisiteTemplateTFStateKey(provider common.ID, deploymentOwner string) string {
	return provider.String() + "-" + deploymentOwner
}

// Input ...
type Input struct {
	Username   string
	Deployment common.ID
	// We need the parameters for execution template because we could use some parameters values for execution template
	// to override the parameter values for prerequisite template.
	RequestParametersForExecutionTemplate map[string]interface{}
	ExecutionTemplateMetadata             service.TemplateMetadata
	Template                              service.Template
	TemplateVersion                       service.TemplateVersion
	Provider                              *service.ProviderModel
	CloudCreds                            []types.ProviderCredentialPair
	// No Git Credential for prerequisite template
	TerraformOperation string
}

// WorkflowData is workflow data for prerequisite template
type WorkflowData awmclient.TerraformWorkflowData

// TerraformWorkflowData ...
func (d WorkflowData) TerraformWorkflowData() awmclient.TerraformWorkflowData {
	return awmclient.TerraformWorkflowData(d)
}

// GenerateWorkflowData generates workflow data for prerequisite template
func GenerateWorkflowData(data Input, keySrc ports.KeySrc) (WorkflowData, error) {
	var err error
	var wfData = WorkflowData{
		Username:          data.Username,
		Deployment:        data.Deployment,
		TemplateID:        "",
		TemplateTypeName:  "",
		TerraformStateKey: PrerequisiteTemplateTFStateKey(data.Provider.ID, data.Username),
		GitURL:            "",
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{},
		Path:                  "",
		CloudCredID:           "",
		AnsibleVars:           nil,
		CloudCredentialBase64: "",
		GitCredID:             "", // no git credential for prerequisite template
		GitCredentialBase64:   "",
	}
	err = wfData.prepareTemplate(data)
	if err != nil {
		return WorkflowData{}, err
	}
	err = wfData.prepareAnsibleVars(data)
	if err != nil {
		return WorkflowData{}, err
	}
	err = wfData.prepareCloudCredential(data, keySrc)
	if err != nil {
		return WorkflowData{}, err
	}

	return wfData, nil
}

// prepare things related to prerequisite template
func (d *WorkflowData) prepareTemplate(data Input) error {
	snapshot, err := deploymentcommon.GetTemplateSnapshot(data.TemplateVersion)
	if err != nil {
		return err
	}

	d.TemplateID = data.Template.GetID()
	d.TemplateTypeName = data.Template.GetMetadata().TemplateType
	d.GitURL = snapshot.GitURL
	d.GitTrackedUpStream = struct {
		Branch string `mapstructure:"branch"`
		Tag    string `mapstructure:"tag"`
		Commit string `mapstructure:"commit"`
	}{
		Branch: snapshot.UpstreamTracked.Branch,
		Tag:    snapshot.UpstreamTracked.Tag,
		Commit: snapshot.CommitHash,
	}
	d.Path = snapshot.SubPath
	return nil
}

func (d *WorkflowData) prepareAnsibleVars(data Input) error {
	logger := log.WithFields(log.Fields{
		"package":  "prerequisitewfdata",
		"function": "WorkflowData.prepareAnsibleVars",
	})
	var providerMetadata types.OpenStackProviderMetadata
	err := mapstructure.Decode(data.Provider.Metadata, &providerMetadata)
	if err != nil {
		logger.WithError(err).Error("fail to parse openstack provider metadata")
		return err
	}
	paramValues := generateTemplateParameterValues(data.Username, providerMetadata)

	paramConverter := domainutils.ParameterConverter{}
	param, err := paramConverter.Convert(
		data.Template.GetMetadata().Parameters,
		paramValues,
		domainutils.OverrideValueForTypes(d.prepareOverrideValues(data)),
	)
	if err != nil {
		logger.WithError(err).Error("fail to convert parameter for prerequisite template")
		return err
	}
	logger.Debugf("parameters converted")

	ansibleVars, err := domainutils.ParamToAnsibleVars(param)
	if err != nil {
		logger.WithError(err).Error("fail to construct ansible vars for AWM request for prerequisite template")
		return err
	}
	tfOp, err := tfOperation(data)
	if err != nil {
		return err
	}
	ansibleVars["TF_OPERATION"] = tfOp

	logger.Infof("ansible variables generated for workflow data")
	d.AnsibleVars = ansibleVars
	return nil
}

func (d *WorkflowData) prepareOverrideValues(data Input) map[string]interface{} {
	var overrides = make(map[string]interface{})
	if len(data.Username) > 0 {
		overrides[domainutils.TemplateUsernameType] = data.Username
	}
	if len(data.RequestParametersForExecutionTemplate) == 0 {
		return overrides
	}
	// if region is provided in the execution template, then use the same value to override parameter for the prerequisite template
	regionParamName := findParamNameForRegionInExecutionTemplate(data)
	if regionParamName != "" {
		if val, ok := data.RequestParametersForExecutionTemplate[regionParamName]; ok {
			overrides[domainutils.TemplateProviderRegionType] = val
		}
	}
	return overrides
}

// find the name of parameter that has type domainutils.TemplateProviderRegionType in the execution template metadata.
func findParamNameForRegionInExecutionTemplate(data Input) string {
	for _, param := range data.ExecutionTemplateMetadata.Parameters {
		if param.Type == domainutils.TemplateProviderRegionType {
			return param.Name
		}
	}

	return ""
}

func (d *WorkflowData) prepareCloudCredential(data Input, keySrc ports.KeySrc) error {
	credential := d.findPrimaryCloudCredential(data)
	if credential == nil {
		return errors.New("credential not found")
	}
	cloudCredBase64, err := domainutils.EncodeCloudCred(credential, keySrc)
	if err != nil {
		return err
	}
	d.CloudCredID = credential.ID
	d.CloudCredentialBase64 = cloudCredBase64
	return nil
}

func (d WorkflowData) findPrimaryCloudCredential(data Input) *service.CredentialModel {
	for _, pair := range data.CloudCreds {
		if pair.Provider == data.Provider.ID {
			return pair.Credential
		}
	}
	return nil
}

// this generates TemplateParameterValues for prerequisite template
func generateTemplateParameterValues(username string, providerMetadata types.OpenStackProviderMetadata) (paramVal service.DeploymentParameterValues) {
	// prepare extra param values to be injected
	paramInjection := types.OpenStackPreRequisiteTemplateParameterInjection{
		Username: username,
	}
	paramInjection.KeyPairName = domainutils.KeyPairName(username)
	if providerMetadata.PublicSSHKey != "" {
		paramInjection.PublicSSHKey = providerMetadata.PublicSSHKey
	}
	if len(providerMetadata.ExternalNetworkUUID) > 0 {
		paramInjection.ExternalNetworkUUID = providerMetadata.ExternalNetworkUUID
	}

	paramVal = make(map[string]interface{})

	// use param values from metadata as a basis
	if providerMetadata.PreRequisiteTemplate.ParamValue != nil {
		for key, val := range providerMetadata.PreRequisiteTemplate.ParamValue {
			paramVal[key] = val
		}
	}

	// inject extra param values
	err := mapstructure.Decode(paramInjection, &paramVal)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "prerequisitewfdata",
			"function": "generateTemplateParameterValues",
		}).WithError(err).Error("failed to inject param value for prerequisite template")
		return paramVal
	}
	return
}

func tfOperation(data Input) (string, error) {
	switch data.TerraformOperation {
	default:
		return "", fmt.Errorf("bad terraform operation, %s", data.TerraformOperation)
	case "apply":
	case "destroy":
	}
	return data.TerraformOperation, nil
}
