package prerequisitewfdata

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

func Test_generateTemplateParameterValues(t *testing.T) {
	type args struct {
		username         string
		providerMetadata types.OpenStackProviderMetadata
	}
	tests := []struct {
		name         string
		args         args
		wantParamVal service.DeploymentParameterValues
	}{
		{
			name: "normal",
			args: args{
				username: "testuser123",
				providerMetadata: types.OpenStackProviderMetadata{
					PreRequisiteTemplate: types.PreRequisiteTemplate{
						TemplateID: common.NewID("template"),
						ParamValue: nil,
					},
					PublicSSHKey:        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					ExternalNetworkName: "public",
					ExternalNetworkUUID: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					ExternalSubnetUUIDs: []string{"ffffffff-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "ffffffff-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
				},
			},
			wantParamVal: map[string]interface{}{
				"username":              "testuser123",
				"keypair_name":          "keypair-testuser123",
				"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			},
		},
		{
			name: "email username",
			args: args{
				username: "testuser123@cyverse.org",
				providerMetadata: types.OpenStackProviderMetadata{
					PreRequisiteTemplate: types.PreRequisiteTemplate{
						TemplateID: common.NewID("template"),
						ParamValue: nil,
					},
					PublicSSHKey:        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					ExternalNetworkName: "public",
					ExternalNetworkUUID: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					ExternalSubnetUUIDs: []string{"ffffffff-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "ffffffff-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
				},
			},
			wantParamVal: map[string]interface{}{
				"username":              "testuser123@cyverse.org",
				"keypair_name":          "keypair-testuser123",
				"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotParamVal := generateTemplateParameterValues(tt.args.username, tt.args.providerMetadata); !reflect.DeepEqual(gotParamVal, tt.wantParamVal) {
				t.Errorf("generateTemplateParameterValues() = %v, want %v", gotParamVal, tt.wantParamVal)
			}
		})
	}
}

func Test_findParamNameForRegionInExecutionTemplate(t *testing.T) {
	type args struct {
		data Input
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "parameters nil",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters:     nil,
					},
				},
			},
			want: "",
		},
		{
			name: "parameters empty",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters:     []service.TemplateParameter{},
					},
				},
			},
			want: "",
		},
		{
			name: "parameter has no definition",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
							},
						},
					},
				},
			},
			want: "",
		},
		{
			name: "parameters has other type",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: "random_type_foobar",
							},
						},
					},
				},
			},
			want: "",
		},
		{
			name: "parameters has special username type",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: domainutils.TemplateUsernameType,
							},
						},
					},
				},
			},
			want: "",
		},
		{
			name: "parameters has special region type, is named region",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters: []service.TemplateParameter{
							{
								Name: "region",
								Type: domainutils.TemplateProviderRegionType,
							},
						},
					},
				},
			},
			want: "region",
		},
		{
			name: "parameters has special region type, not named region",
			args: args{
				data: Input{
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Name:           "",
						Author:         "",
						AuthorEmail:    "",
						Description:    "",
						TemplateType:   "",
						Purpose:        "",
						CacaoPostTasks: nil,
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: domainutils.TemplateProviderRegionType,
							},
						},
					},
				},
			},
			want: "foo",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findParamNameForRegionInExecutionTemplate(tt.args.data); got != tt.want {
				t.Errorf("findParamNameForRegionInExecutionTemplate() = %v, want %v", got, tt.want)
			}
		})
	}
	t.Run("parameters multiple param with special region type", func(t *testing.T) {
		testInput := Input{
			ExecutionTemplateMetadata: service.TemplateMetadata{
				Name:           "",
				Author:         "",
				AuthorEmail:    "",
				Description:    "",
				TemplateType:   "",
				Purpose:        "",
				CacaoPostTasks: nil,
				Parameters: []service.TemplateParameter{
					{
						Name: "bar",
						Type: domainutils.TemplateProviderRegionType,
					},
					{
						Name: "foo",
						Type: domainutils.TemplateProviderRegionType,
					},
				},
			},
		}
		got := findParamNameForRegionInExecutionTemplate(testInput)
		assert.Contains(t, []string{"bar", "foo"}, got) // could be either one of the parameter, not necessarily the first found.
	})
}

func TestWorkflowData_prepareOverrideValues(t *testing.T) {
	type args struct {
		data Input
	}
	tests := []struct {
		name string
		d    WorkflowData
		args args
		want map[string]interface{}
	}{
		{
			name: "empty username, empty request param values for execution template, no region param in execution template",
			d:    WorkflowData{},
			args: args{
				data: Input{
					Username:                              "", // Username should not be empty
					RequestParametersForExecutionTemplate: map[string]interface{}{},
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: "bar",
							},
						},
					},
				},
			},
			want: map[string]interface{}{},
		},
		{
			name: "empty request param values for execution template, no region param in execution template",
			d:    WorkflowData{},
			args: args{
				data: Input{
					Username:                              "testuser123",
					RequestParametersForExecutionTemplate: map[string]interface{}{},
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: "bar",
							},
						},
					},
				},
			},
			want: map[string]interface{}{
				domainutils.TemplateUsernameType: "testuser123",
			},
		},
		{
			name: "empty request param values for execution template, region param in execution template",
			d:    WorkflowData{},
			args: args{
				data: Input{
					Username:                              "testuser123",
					RequestParametersForExecutionTemplate: map[string]interface{}{},
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: domainutils.TemplateProviderRegionType,
							},
						},
					},
				},
			},
			want: map[string]interface{}{
				domainutils.TemplateUsernameType: "testuser123",
			},
		},
		{
			name: "region param in execution template, but no matching request param values for execution template",
			d:    WorkflowData{},
			args: args{
				data: Input{
					Username: "testuser123",
					RequestParametersForExecutionTemplate: map[string]interface{}{
						"bar": "string_value_123",
					},
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: domainutils.TemplateProviderRegionType,
							},
							{
								Name: "bar",
								Type: "string",
							},
						},
					},
				},
			},
			want: map[string]interface{}{
				domainutils.TemplateUsernameType: "testuser123",
			},
		},
		{
			name: "region param in execution template, and matching request param values for execution template",
			d:    WorkflowData{},
			args: args{
				data: Input{
					Username: "testuser123",
					RequestParametersForExecutionTemplate: map[string]interface{}{
						"foo": "region_123",
					},
					ExecutionTemplateMetadata: service.TemplateMetadata{
						Parameters: []service.TemplateParameter{
							{
								Name: "foo",
								Type: domainutils.TemplateProviderRegionType,
							},
							{
								Name: "bar",
								Type: "string",
							},
						},
					},
				},
			},
			want: map[string]interface{}{
				domainutils.TemplateUsernameType:       "testuser123",
				domainutils.TemplateProviderRegionType: "region_123",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.d.prepareOverrideValues(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("prepareOverrideValues() = %v, want %v", got, tt.want)
			}
		})
	}
}
