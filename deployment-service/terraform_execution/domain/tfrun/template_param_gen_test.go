package tfrun

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

func Test_generateParameters(t *testing.T) {
	type args struct {
		data   PrerequisiteData
		credMS func() *portsmocks.CredentialMicroservice
	}
	tests := []struct {
		name    string
		args    args
		want    []deploymentcommon.DeploymentParameter
		wantErr bool
		check   func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string)
	}{

		{
			name: "normal without CACAO ssh key",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "int1",
									Type: "integer",
								},
								{
									Name:    "int2",
									Type:    "integer",
									Default: 100,
								},
								{
									Name: "str1",
									Type: "string",
								},
								{
									Name: "username",
									Type: domainutils.TemplateUsernameType,
								},
								{
									Name: "keypair",
									Type: domainutils.TemplateProviderKeyPairType,
								},
								{
									Name: "ext_net_name",
									Type: domainutils.TemplateProviderExternalNetworkType,
								},
								{
									Name: "cloud_init",
									Type: domainutils.TemplateCloudInitType,
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					var actor = types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa FFFFF",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "int1",
					Type:  "integer",
					Value: int64(123),
				},
				{
					Name:  "int2",
					Type:  "integer",
					Value: int64(100),
				},
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "username",
					Type:  domainutils.TemplateUsernameType,
					Value: "testuser123",
				},
				{
					Name:  "keypair",
					Type:  domainutils.TemplateProviderKeyPairType,
					Value: "keypair-testuser123",
				},
				{
					Name:  "ext_net_name",
					Type:  domainutils.TemplateProviderExternalNetworkType,
					Value: "public",
				},
				{
					Name:  "cloud_init",
					Type:  domainutils.TemplateCloudInitType,
					Value: "#cloud-config\nusers:\n  - default\n  - name: testuser123\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa FFFFF\n",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "normal with CACAO ssh key",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "int1",
									Type: "integer",
								},
								{
									Name:    "int2",
									Type:    "integer",
									Default: 100,
								},
								{
									Name: "str1",
									Type: "string",
								},
								{
									Name: "username",
									Type: domainutils.TemplateUsernameType,
								},
								{
									Name: "keypair",
									Type: domainutils.TemplateProviderKeyPairType,
								},
								{
									Name: "ext_net_name",
									Type: domainutils.TemplateProviderExternalNetworkType,
								},
								{
									Name: "cloud_init",
									Type: domainutils.TemplateCloudInitType,
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
					CACAOSSHKey: "ssh-rsa cacao123",
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					var actor = types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa FFFFF",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "int1",
					Type:  "integer",
					Value: int64(123),
				},
				{
					Name:  "int2",
					Type:  "integer",
					Value: int64(100),
				},
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "username",
					Type:  domainutils.TemplateUsernameType,
					Value: "testuser123",
				},
				{
					Name:  "keypair",
					Type:  domainutils.TemplateProviderKeyPairType,
					Value: "keypair-testuser123",
				},
				{
					Name:  "ext_net_name",
					Type:  domainutils.TemplateProviderExternalNetworkType,
					Value: "public",
				},
				{
					Name:  "cloud_init",
					Type:  domainutils.TemplateCloudInitType,
					Value: "#cloud-config\nusers:\n  - default\n  - name: testuser123\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa FFFFF\n      - ssh-rsa cacao123\n",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},

		{
			name: "missing parameter, no cloud init",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								//"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "int1",
									Type: "integer",
								},
								{
									Name:    "int2",
									Type:    "integer",
									Default: 100,
								},
								{
									Name: "str1",
									Type: "string",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want:    nil,
			wantErr: true,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "extra parameter, no cloud init",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								"str1": "foobar",
								"str2": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "int1",
									Type: "integer",
								},
								{
									Name: "str1",
									Type: "string",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "int1",
					Type:  "integer",
					Value: int64(123),
				},
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "cacao_ssh_key parameter",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "str1",
									Type: "string",
								},
								{
									Name: "key1",
									Type: "cacao_ssh_key",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
					CACAOSSHKey: "ssh-rsa FFFFF",
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "key1",
					Type:  "cacao_ssh_key",
					Value: "ssh-rsa FFFFF",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "cacao_user_ssh_key_all parameter",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "str1",
									Type: "string",
								},
								{
									Name: "key_list1",
									Type: "cacao_user_ssh_key_all_json",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					var actor = types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "cred-foobar",
							},
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "cred-foobarbaz",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "cred-foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa FFFFF",
							Type:     "ssh",
							ID:       "cred-foobar",
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "cred-foobarbaz").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa EEEEE",
							Type:     "ssh",
							ID:       "cred-foobarbaz",
						},
						nil,
					)
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name: "key_list1",
					Type: "cacao_user_ssh_key_all_json",
					Value: `["ssh-rsa FFFFF","ssh-rsa EEEEE"]
`,
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "cacao_white_list_cidr_json parameter",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "ip_list1",
									Type: "cacao_white_list_cidr_json",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
					WhitelistCIDRs: []string{"8.8.8.8/32"},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "ip_list1",
					Type:  "cacao_white_list_cidr_json",
					Value: `["8.8.8.8/32"]`,
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "cacao_white_list_cidr_json parameter empty",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-cnm8f0598850n9abikj0",
						Metadata: service.TemplateMetadata{
							Name:           "",
							TemplateType:   "",
							Purpose:        "",
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name: "ip_list1",
									Type: "cacao_white_list_cidr_json",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-d17e30598850n9abikl0",
						Type:     service.OpenStackProviderType,
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "ip_list1",
					Type:  "cacao_white_list_cidr_json",
					Value: `[]`,
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credMS := tt.args.credMS()
			got, err := generateParameters(tt.args.data, credMS)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			tt.check(t, err, credMS, fmt.Sprintf("generateParameters(%v, %v)", tt.args.data, credMS))
			assert.ElementsMatchf(t, tt.want, got, "generateParameters(%v, %v)", tt.args.data, credMS)
		})
	}
}

func Test_generateCloudInitScript(t *testing.T) {
	var actor = types.Actor{Actor: "testuser123", Emulator: ""}
	var request = deploymentevents.StartRunRequest{
		Session: service.Session{
			SessionActor:    actor.Actor,
			SessionEmulator: actor.Emulator,
		},
	}
	var credMS = &portsmocks.CredentialMicroservice{}
	credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
		[]service.CredentialModel{
			{
				Username: "testuser123",
				Value:    "",
				Type:     "ssh",
				ID:       "foobar",
			},
		},
		nil,
	)
	credMS.On("Get", actor, actor.Actor, "foobar").Return(
		&service.CredentialModel{
			Username: "testuser123",
			Value:    "ssh-rsa FFFFF",
			Type:     "ssh",
			ID:       "foobar",
		},
		nil,
	)
	cloudInit, err := generateCloudInitScript(request, "ssh-rsa foobar", credMS)
	assert.NoError(t, err)
	assert.Greater(t, len(cloudInit), 0)
	expected := "#cloud-config\nusers:\n  - default\n  - name: testuser123\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa FFFFF\n      - ssh-rsa foobar\n"
	assert.Equal(t, expected, cloudInit)
}

func Test_listAllSSHCredentials(t *testing.T) {
	type args struct {
		credMS func() *portsmocks.CredentialMicroservice
		actor  types.Actor
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
		check   func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string)
	}{
		{
			name: "normal",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa AAAAA",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{"ssh-rsa AAAAA"},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "2 ssh keys",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar1",
							},
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar2",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar1").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa AAAAA",
							Type:     "ssh",
							ID:       "foobar1",
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar2").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa BBBBB",
							Type:     "ssh",
							ID:       "foobar2",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{"ssh-rsa AAAAA", "ssh-rsa BBBBB"},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "no cred",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "empty cred value",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "credMS list err",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(nil, errors.New("failed"))
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    nil,
			wantErr: true,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "credMS get err",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("ListPublicSSHKeys", actor, "testuser123").Return(
						[]service.CredentialModel{
							{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(nil, errors.New("failed"))
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    nil,
			wantErr: true,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credMS := tt.args.credMS()
			got, err := listAllSSHCredentials(credMS, tt.args.actor)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			tt.check(t, err, credMS, fmt.Sprintf("listAllSSHCredentials(%v, %v)", credMS, tt.args.actor))
			assert.Equalf(t, tt.want, got, "listAllSSHCredentials(%v, %v)", credMS, tt.args.actor)
		})
	}
}
