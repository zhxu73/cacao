package domain

import (
	"context"
	"encoding/base64"
	"fmt"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/eventhandler"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/queryhandlers"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"sync"
)

// Domain is the entrypoint object for this service
type Domain struct {
	QuerySrc        ports.QuerySrc
	EventSrc        ports.EventSrc
	TFRunStorage    ports.TFRunStorage
	WorkflowStorage ports.DeploymentWorkflowStorage
	KeySrc          ports.KeySrc
	TimeSrc         ports.TimeSrc
	SSHKeySrc       ports.SSHKeySrc

	MetadataSvc  ports.DeploymentMetadataService
	WorkspaceMS  ports.WorkspaceMicroservice
	TemplateMS   ports.TemplateMicroservice
	CredentialMS ports.CredentialMicroservice
	ProviderMS   ports.ProviderMicroservice
	AWM          ports.ArgoWorkflowMediator

	logsHandler queryhandlers.LogsLocationHandler

	WhitelistCIDRs []string
}

// Init ...
func (svc *Domain) Init(conf types.Config) error {
	var err error
	err = svc.TFRunStorage.Init()
	if err != nil {
		return err
	}
	err = svc.WorkflowStorage.Init()
	if err != nil {
		return err
	}
	jwtKey, err := base64.StdEncoding.DecodeString(conf.MiscConfig.LogsJWTKeyBase64)
	if err != nil {
		return fmt.Errorf("fail to decode JWT key, %w", err)
	}
	logsHandler, err := queryhandlers.NewLogsLocationHandler(conf.MiscConfig.LogsStorageBaseURL, jwtKey, svc.WorkflowStorage, svc.TFRunStorage, svc.MetadataSvc)
	if err != nil {
		return err
	}
	svc.logsHandler = logsHandler
	return nil
}

// Start starts receiving and processing events
func (svc *Domain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	svc.EventSrc.SetHandlers(eventhandler.NewEventHandlers(svc.commonDependencies(), svc.WhitelistCIDRs))
	wg.Add(1)
	err := svc.EventSrc.Start(ctx, &wg)
	if err != nil {
		return err
	}

	wg.Add(1)
	err = svc.QuerySrc.Start(ctx, &wg, svc.logsHandler)
	if err != nil {
		return err
	}

	wg.Wait()
	return nil
}

func (svc Domain) commonDependencies() ports.Ports {
	return ports.Ports{
		TFRunStorage:    svc.TFRunStorage,
		WorkflowStorage: svc.WorkflowStorage,
		KeySrc:          svc.KeySrc,
		TimeSrc:         svc.TimeSrc,
		SSHKeySrc:       svc.SSHKeySrc,
		MetadataSvc:     svc.MetadataSvc,
		TemplateMS:      svc.TemplateMS,
		WorkspaceMS:     svc.WorkspaceMS,
		CredentialMS:    svc.CredentialMS,
		ProviderMS:      svc.ProviderMS,
		AWM:             svc.AWM,
	}
}
