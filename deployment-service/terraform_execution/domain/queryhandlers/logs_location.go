package queryhandlers

import (
	"fmt"
	jwt "github.com/golang-jwt/jwt/v5"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"net/url"
	"time"
)

// LogsLocationHandler is handler for query request that asks for the location of deployment/deploymentRun logs
type LogsLocationHandler struct {
	jwtKey       []byte
	baseURL      *url.URL
	wfStorage    ports.DeploymentWorkflowStorage
	tfRunStorage ports.TFRunStorage
	metadataSvc  ports.DeploymentMetadataService
}

var _ ports.QueryHandlers = (*LogsLocationHandler)(nil)

// NewLogsLocationHandler ...
func NewLogsLocationHandler(baseURLStr string, jwtKey []byte, wfStorage ports.DeploymentWorkflowStorage, tfRunStorage ports.TFRunStorage, metadataSvc ports.DeploymentMetadataService) (LogsLocationHandler, error) {
	baseURL, err := url.Parse(baseURLStr)
	if err != nil {
		return LogsLocationHandler{}, err
	}
	if len(jwtKey) <= 32 {
		return LogsLocationHandler{}, fmt.Errorf("JWT key is too short, length %d <= 32", len(jwtKey))
	}
	return LogsLocationHandler{
		jwtKey:       jwtKey,
		baseURL:      baseURL,
		wfStorage:    wfStorage,
		tfRunStorage: tfRunStorage,
		metadataSvc:  metadataSvc,
	}, nil
}

// ListLogsLocation ...
func (h LogsLocationHandler) ListLogsLocation(request service.DeploymentLogsLocationQuery) service.DeploymentLogsLocationReply {
	logger := log.WithFields(log.Fields{
		"function":   "LogsLocationHandler.ListLogsLocation",
		"deployment": request.Deployment,
		"run":        request.Run,
		"actor":      request.SessionActor,
		"emulator":   request.SessionEmulator,
	})
	if request.Run == "" {
		if cerr := h.checkDeploymentExists(request); cerr != nil {
			logger.WithError(cerr).Error("fail to fetch location for deployment deletion logs")
			return h.errorReply(request, cerr)
		}
		reply := h.getDeploymentDeletionLogs(request)
		logger.WithFields(log.Fields{
			"len": len(reply.LogsLocations),
		}).Info("fetched locations")
		return reply
	}
	if cerr := h.checkRunExists(request); cerr != nil {
		logger.WithError(cerr).Error("fail to fetch location for run logs")
		return h.errorReply(request, cerr)
	}
	reply := h.getDeploymentRunLogs(request)
	logger.WithFields(log.Fields{
		"len": len(reply.LogsLocations),
	}).Info("fetched locations")
	return reply
}

func (h LogsLocationHandler) checkDeploymentExists(request service.DeploymentLogsLocationQuery) service.CacaoError {
	// fetch deployment to check ownership.
	_, err := h.metadataSvc.Get(service.ActorFromSession(request.Session), request.Deployment)
	if err != nil {
		return service.ToCacaoError(err)
	}
	return nil
}

func (h LogsLocationHandler) checkRunExists(request service.DeploymentLogsLocationQuery) service.CacaoError {
	run, err := h.tfRunStorage.Get(request.Run)
	if err != nil {
		return service.ToCacaoError(err)
	}
	if run.CreatedBy.User != request.SessionActor {
		return service.NewCacaoNotFoundError("tf run not found")
	}
	return nil
}

func (h LogsLocationHandler) getDeploymentDeletionLogs(request service.DeploymentLogsLocationQuery) service.DeploymentLogsLocationReply {
	workflows, err := h.wfStorage.ListDeploymentDeletionWorkflows(request.Deployment)
	if err != nil {
		return h.errorReply(request, err)
	}
	if len(workflows) == 0 {
		return h.errorReply(request, service.NewCacaoNotFoundError("no logs for the deployment deletion"))
	}

	// Only return the last deletion workflow. Technically, there can be multiple
	// deletion workflows for a deployment, because there can be cases where
	// deployment status is manually reset, this usually happens when manually debugging.
	wfName := workflows[len(workflows)-1].WorkflowName

	var token string
	token, err = h.generateJWTToken(wfName)
	if err != nil {
		log.WithError(err).Error("fail to generate token")
		return h.errorReply(request, service.NewCacaoGeneralError("fail to generate token for fetching logs"))
	}

	log.WithField("len", 1).Info("fetched location for deletion logs for deployment")
	return service.DeploymentLogsLocationReply{
		Session:      service.CopySessionActors(request.Session),
		Deployment:   request.Deployment,
		TemplateType: request.TemplateType,
		Run:          request.Run,
		LogsLocations: []service.LogsLocation{
			{
				DeploymentStatus: service.DeploymentStatusDeleted,
				RunStatus:        "", // leave this empty, since this is not logs for a deployment run
				LogsID:           wfName,
				LogsURL:          h.logsLocation(wfName),
				AuthHeader:       "Bearer " + token,
			},
		},
	}
}

func (h LogsLocationHandler) getDeploymentRunLogs(request service.DeploymentLogsLocationQuery) service.DeploymentLogsLocationReply {
	workflows, err := h.wfStorage.ListDeploymentRunWorkflows(request.Deployment, request.Run)
	if err != nil {
		return h.errorReply(request, err)
	}
	if len(workflows) == 0 {
		return h.errorReply(request, service.NewCacaoNotFoundError("no logs for the run"))
	}
	var locationList []service.LogsLocation
	for i := range workflows {
		wfName := workflows[i].WorkflowName
		var runStatus deploymentcommon.DeploymentRunStatus
		switch workflows[i].Purpose {
		case types.PreflightPurpose:
			runStatus = deploymentcommon.DeploymentRunPreflight
		case types.ExecutionPurpose:
			runStatus = deploymentcommon.DeploymentRunRunning
		default:
			// ignore this workflow
			continue
		}
		var token string
		token, err = h.generateJWTToken(wfName)
		if err != nil {
			log.WithError(err).Error("fail to generate token")
			return h.errorReply(request, service.NewCacaoGeneralError("fail to generate token for fetching logs"))
		}
		locationList = append(locationList, service.LogsLocation{
			DeploymentStatus: "", // leave this empty since this is not logs for deployment, but for deployment run.
			RunStatus:        runStatus.String(),
			LogsID:           wfName,
			LogsURL:          h.logsLocation(wfName),
			AuthHeader:       "Bearer " + token,
		})
	}
	log.WithField("len", len(locationList)).Info("fetched locations for run logs")
	return service.DeploymentLogsLocationReply{
		Session:       service.CopySessionActors(request.Session),
		Deployment:    request.Deployment,
		TemplateType:  request.TemplateType,
		Run:           request.Run,
		LogsLocations: locationList,
	}
}

// check workflow-logs-storage in cacao-edge-deployment for the URL.
// See `openapi.yaml` for the workflow logs storage for the endpoint api.
func (h LogsLocationHandler) logsLocation(workflowName string) string {
	// JoinPath modify url.URL, so make a copy of it
	var copyURL url.URL
	copyURL = *h.baseURL
	logsURL := copyURL.JoinPath("v1", "logs", workflowName)
	return logsURL.String()
}

func (h LogsLocationHandler) errorReply(request service.DeploymentLogsLocationQuery, err error) service.DeploymentLogsLocationReply {
	return service.DeploymentLogsLocationReply{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.ToCacaoError(err).GetBase(),
		},
		Deployment:    request.Deployment,
		TemplateType:  request.TemplateType,
		Run:           request.Run,
		LogsLocations: nil,
	}
}

func (h LogsLocationHandler) generateJWTToken(workflowName string) (string, error) {
	now := time.Now()
	const tokenLifetime = time.Minute * 5
	claims := jwt.RegisteredClaims{
		Issuer:    "terraform_execution_service",
		Subject:   workflowName,
		Audience:  nil,
		ExpiresAt: jwt.NewNumericDate(now.Add(tokenLifetime)),
		NotBefore: jwt.NewNumericDate(now),
		IssuedAt:  jwt.NewNumericDate(now),
		ID:        xid.New().String(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(h.jwtKey)
	if err != nil {
		return "", err
	}
	log.WithFields(log.Fields{"jwtID": claims.ID, "workflow": workflowName}).Info("generated jwt for fetching logs")
	return tokenStr, nil
}
