package preflightstage

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
	"time"
)

func TestWorkflowHandler_HandleWorkflowSuccess(t *testing.T) {
	type fields struct {
		runStorage func() *portsmocks.TFRunStorage
	}
	type args struct {
		tid      common.TransactionID
		event    awmclient.WorkflowSucceeded
		workflow types.DeploymentWorkflow
		sink     *portsmocks.OutgoingEventSink
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantNil     bool
		checkReturn func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage)
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{
						ID:           "run-d8caa0598850n9abikmg",
						Deployment:   "deployment-d600t0598850n9abikm0",
						TemplateType: types.TerraformOpenStackDeploymentType,
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-d17e30598850n9abikl0",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-d17e30598850n9abikl0",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs: map[string]interface{}{
						"version":           0,
						"terraform_version": "",
						"serial":            0,
						"lineage":           "",
						"outputs":           map[string]interface{}{},
						"resources":         []interface{}{},
					},
					Metadata: nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-d17e30598850n9abikl0",
					Purpose:      "",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventRunPreflightSucceeded", deploymentevents.RunPreflightResult{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						TemplateType: types.TerraformOpenStackDeploymentType,
						Deployment:   "deployment-d600t0598850n9abikm0",
						Run:          "run-d8caa0598850n9abikmg",
					})
					return sink
				}(),
			},
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightSucceeded, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, deploymentevents.RunPreflightResult{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					TemplateType: types.TerraformOpenStackDeploymentType,
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				}, eventBody)
				runStorage.AssertExpectations(t)
			},
		},
		{
			name: "empty tid in args",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{
						ID:           "run-d8caa0598850n9abikmg",
						Deployment:   "deployment-d600t0598850n9abikm0",
						TemplateType: types.TerraformOpenStackDeploymentType,
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-d17e30598850n9abikl0",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
			},
			args: args{
				tid: "", // empty TID
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-d17e30598850n9abikl0",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs: map[string]interface{}{
						"version":           0,
						"terraform_version": "",
						"serial":            0,
						"lineage":           "",
						"outputs":           map[string]interface{}{},
						"resources":         []interface{}{},
					},
					Metadata: nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-d17e30598850n9abikl0",
					Purpose:      "",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventRunPreflightSucceeded", deploymentevents.RunPreflightResult{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						TemplateType: types.TerraformOpenStackDeploymentType,
						Deployment:   "deployment-d600t0598850n9abikm0",
						Run:          "run-d8caa0598850n9abikmg",
					})
					return sink
				}(),
			},
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightSucceeded, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, deploymentevents.RunPreflightResult{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					TemplateType: types.TerraformOpenStackDeploymentType,
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				}, eventBody)
				runStorage.AssertExpectations(t)
			},
		},
		{
			name: "bad wf output",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{
						ID:           "run-d8caa0598850n9abikmg",
						Deployment:   "deployment-d600t0598850n9abikm0",
						TemplateType: types.TerraformOpenStackDeploymentType,
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-d17e30598850n9abikl0",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-d17e30598850n9abikl0",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs:    nil, // bad wf outputs
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-d17e30598850n9abikl0",
					Purpose:      "",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventRunPreflightFailed", deploymentevents.RunPreflightResult{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("workflow bbbbbbbbbbbbbbbbbbbb succeeded but error occuried while handling workflow result, invalid parameter error - workflow output is nil").GetBase(),
						},
						TemplateType: types.TerraformOpenStackDeploymentType,
						Deployment:   "deployment-d600t0598850n9abikm0",
						Run:          "run-d8caa0598850n9abikmg",
					})
					return sink
				}(),
			},
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.NotEqual(t, "", eventBody.ServiceError.StandardMessage)
				assert.NotEqual(t, "", eventBody.ServiceError.ContextualMessage)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)

				assert.Equal(t, "deployment-d600t0598850n9abikm0", eventBody.Deployment.String())
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.TemplateType)
				assert.Equal(t, "run-d8caa0598850n9abikmg", eventBody.Run.String())

				runStorage.AssertExpectations(t)
			},
		},
		{
			// Note, that it should be rare that WorkflowStorage succeeded in fetching workflow, but RunStorage failed
			// to fetch run.
			name: "run storage error",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{}, errors.New("failed123"))
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-d17e30598850n9abikl0",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs: map[string]interface{}{
						"version":           0,
						"terraform_version": "",
						"serial":            0,
						"lineage":           "",
						"outputs":           map[string]interface{}{},
						"resources":         []interface{}{},
					},
					Metadata: nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-d17e30598850n9abikl0",
					Purpose:      "",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventRunPreflightFailed", deploymentevents.RunPreflightResult{
						Session: service.Session{
							SessionActor:    "", // Since failed to fetch run, there is nowhere to get actor from (event does not have it)
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("workflow bbbbbbbbbbbbbbbbbbbb succeeded but error occuried while handling workflow result, failed123").GetBase(),
						},
						TemplateType: "",
						Deployment:   "deployment-d600t0598850n9abikm0",
						Run:          "run-d8caa0598850n9abikmg",
					})
					return sink
				}(),
			},
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equalf(t, "", eventBody.SessionActor, "Since failed to fetch run, there is nowhere to get actor from (event does not have it)")
				assert.NotEqual(t, "", eventBody.ServiceError.StandardMessage)
				assert.NotEqual(t, "", eventBody.ServiceError.ContextualMessage)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)

				assert.Equal(t, "deployment-d600t0598850n9abikm0", eventBody.Deployment.String())
				assert.Equal(t, service.TemplateTypeName(""), eventBody.TemplateType)
				assert.Equal(t, "run-d8caa0598850n9abikmg", eventBody.Run.String())

				runStorage.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			h := WorkflowHandler{
				runStorage: runStorage,
			}
			h.HandleWorkflowSuccess(tt.args.tid, tt.args.event, tt.args.workflow, tt.args.sink)
			runStorage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestWorkflowHandler_HandleWorkflowFailure(t *testing.T) {
	type fields struct {
		runStorage func() *portsmocks.TFRunStorage
	}
	type args struct {
		tid      common.TransactionID
		event    awmclient.WorkflowFailed
		workflow types.DeploymentWorkflow
		sink     *portsmocks.OutgoingEventSink
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantNil     bool
		checkReturn func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage)
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{
						ID:           "run-d8caa0598850n9abikmg",
						Deployment:   "deployment-d600t0598850n9abikm0",
						TemplateType: types.TerraformOpenStackDeploymentType,
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-d17e30598850n9abikl0",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					newStatus := deploymentcommon.DeploymentRunErrored
					runStorage.On("Update", common.ID("run-d8caa0598850n9abikmg"), types.DeploymentRunUpdate{
						Status: &newStatus,
					}, types.DeploymentRunFilter{}).Return(true, nil)
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowFailed{
					Provider:     "provider-d17e30598850n9abikl0",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "Failed",
					NodeStatus:   nil,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-d17e30598850n9abikl0",
					Purpose:      "",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventRunPreflightFailed", deploymentevents.RunPreflightResult{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("preflight workflow bbbbbbbbbbbbbbbbbbbb failed").GetBase(),
						},
						TemplateType: types.TerraformOpenStackDeploymentType,
						Deployment:   "deployment-d600t0598850n9abikm0",
						Run:          "run-d8caa0598850n9abikmg",
					})
					return sink
				}(),
			},
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.Equal(t, "", eventBody.SessionEmulator)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.TemplateType)
				assert.Equal(t, "deployment-d600t0598850n9abikm0", eventBody.Deployment.String())
				assert.Equal(t, "run-d8caa0598850n9abikmg", eventBody.Run.String())
				runStorage.AssertExpectations(t)
			},
		},
		{
			name: "run storage Get() err",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{}, errors.New("failed123"))
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowFailed{
					Provider:     "provider-d17e30598850n9abikl0",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "Failed",
					NodeStatus:   nil,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-d17e30598850n9abikl0",
					Purpose:      "",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventRunPreflightFailed", deploymentevents.RunPreflightResult{
						Session: service.Session{
							SessionActor:    "", // since we cannot get run with Get(), we dont know who created the run
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("workflow bbbbbbbbbbbbbbbbbbbb failed and error occuried while handling workflow result, failed123").GetBase(),
						},
						TemplateType: "",
						Deployment:   "deployment-d600t0598850n9abikm0",
						Run:          "run-d8caa0598850n9abikmg",
					})
					return sink
				}(),
			},
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, "", eventBody.SessionActor)
				assert.Equal(t, "", eventBody.SessionEmulator)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Equal(t, service.TemplateTypeName(""), eventBody.TemplateType)
				assert.Equal(t, "deployment-d600t0598850n9abikm0", eventBody.Deployment.String())
				assert.Equal(t, "run-d8caa0598850n9abikmg", eventBody.Run.String())
				runStorage.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			h := WorkflowHandler{
				runStorage: runStorage,
			}
			h.HandleWorkflowFailure(tt.args.tid, tt.args.event, tt.args.workflow, tt.args.sink)
			runStorage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}
