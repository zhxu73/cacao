package resourcedeletion

import (
	"errors"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"reflect"
	"testing"
)

func TestPrerequisiteGatherer_getLastRun(t *testing.T) {
	type fields struct {
		runStorage  func() *portsmocks.TFRunStorage
		credMS      ports.CredentialMicroservice
		metadataSvc func() *portsmocks.DeploymentMetadataService
	}
	type args struct {
		deploymentID common.ID
		actor        types.Actor
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    types.DeploymentRun
		wantErr bool
	}{
		{
			name: "normal",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{Actor: "testuser123", Emulator: ""},
						common.ID("deployment-d600t0598850n9abikm0"),
					).Return(service.Deployment{
						ID: "deployment-d600t0598850n9abikm0",
						LastRun: &service.DeploymentRun{
							ID: "run-d8caa0598850n9abikmg",
						},
					}, nil)
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(
						types.DeploymentRun{
							ID:         "run-d8caa0598850n9abikmg",
							Deployment: "deployment-d600t0598850n9abikm0",
						}, nil)
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want: types.DeploymentRun{
				ID:         "run-d8caa0598850n9abikmg",
				Deployment: "deployment-d600t0598850n9abikm0",
			},
			wantErr: false,
		},
		{
			name: "metadata svc Get() err",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{Actor: "testuser123", Emulator: ""},
						common.ID("deployment-d600t0598850n9abikm0"),
					).Return(service.Deployment{}, errors.New("failed"))
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "deployment no last run",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{
							Actor:    "testuser123",
							Emulator: "",
						},
						common.ID("deployment-d600t0598850n9abikm0"),
					).Return(service.Deployment{
						ID:      "deployment-d600t0598850n9abikm0",
						LastRun: nil, // no last run
					}, nil)
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "run storage Get() err",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{
							Actor:    "testuser123",
							Emulator: "",
						},
						common.ID("deployment-d600t0598850n9abikm0"),
					).Return(service.Deployment{
						ID: "deployment-d600t0598850n9abikm0",
						LastRun: &service.DeploymentRun{
							ID: "run-d8caa0598850n9abikmg",
						},
					}, nil)
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(
						types.DeploymentRun{}, errors.New("failed"))
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-d600t0598850n9abikm0",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			metadataSvc := tt.fields.metadataSvc()
			gatherer := PrerequisiteGatherer{
				runStorage:  runStorage,
				credMS:      tt.fields.credMS,
				metadataSvc: metadataSvc,
			}
			got, err := gatherer.getLastRun(tt.args.deploymentID, tt.args.actor)
			if (err != nil) != tt.wantErr {
				t.Errorf("getLastRun() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getLastRun() got = %v, want %v", got, tt.want)
			}
			runStorage.AssertExpectations(t)
			metadataSvc.AssertExpectations(t)
		})
	}
}
