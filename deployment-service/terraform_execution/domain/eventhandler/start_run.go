package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/tfrun"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// startRunResult is the result of StartRunHandler
type startRunResult struct {
	Deployment common.ID `json:"deployment"`
	Run        common.ID `json:"run"`
	Parameters deploymentcommon.DeploymentParameters
	// If Error is nil, then preflight has started, else fail to start preflight
	Error error
}

// StartRunHandler handles the StartRunRequest.
// It validates the request to see if the run can be started.
// If the run CAN be started, then it proceeds to the preflight stage, for which a preflight workflow will be launched.
// If run CANNOT be started (that is if any error arise in the handler), then run is fail to start,
// which means from the perspective of outside of deployment context (outside of all deployment services),
// the run is not created and does not exists.
type StartRunHandler struct {
	runStorage     ports.TFRunStorage
	wfStorage      ports.DeploymentWorkflowStorage
	awm            ports.ArgoWorkflowMediator
	providerMS     ports.ProviderMicroservice
	templateMS     ports.TemplateMicroservice
	credMS         ports.CredentialMicroservice
	keySrc         ports.KeySrc
	sshKeySrc      ports.SSHKeySrc
	whitelistCIDRs []string
}

// NewStartRunHandler ...
func NewStartRunHandler(portsDependency ports.Ports, whitelistCIDRs []string) StartRunHandler {
	return StartRunHandler{
		runStorage:     portsDependency.TFRunStorage,
		wfStorage:      portsDependency.WorkflowStorage,
		awm:            portsDependency.AWM,
		providerMS:     portsDependency.ProviderMS,
		templateMS:     portsDependency.TemplateMS,
		credMS:         portsDependency.CredentialMS,
		keySrc:         portsDependency.KeySrc,
		sshKeySrc:      portsDependency.SSHKeySrc,
		whitelistCIDRs: whitelistCIDRs,
	}
}

func (h StartRunHandler) handle(tid common.TransactionID, request deploymentevents.StartRunRequest) startRunResult {
	logger := log.WithFields(log.Fields{
		"package":    "eventhandler",
		"function":   "StartRunHandler.handle",
		"deployment": request.Deployment.ID,
		"run":        request.Run.ID,
	})
	gatherer := tfrun.NewPrerequisiteGatherer(h.templateMS, h.providerMS, h.credMS, h.sshKeySrc, h.whitelistCIDRs)
	prerequisite, err := gatherer.Gather(request)
	if err != nil {
		logger.WithError(err).Error("fail to gather prerequisite for starting the run")
		return h.failedToStartRun(request, err)
	}

	factory := tfrun.NewTFRunFactory()
	tfRun, err := factory.NewRun(tid, prerequisite, tfrun.NewTemplateParameterGenerator(h.credMS))
	if err != nil {
		logger.WithError(err).Error("fail to generate a terraform run object from request and prerequisite")
		return h.failedToStartRun(request, err)
	}
	logger.Info("new terraform run object created")

	awmProvider, wfName, err := preflightstage.ExecuteTemplate(h.awm, h.keySrc, prerequisite)
	if err != nil {
		logger.WithError(err).Error("fail to launch workflow for prerequisite template")
		return h.failedToStartRun(request, err)
	}
	logger.WithFields(log.Fields{
		"awmProvider": awmProvider,
		"wfName":      wfName,
	}).Info("preflight workflow launched")

	err = h.saveWorkflow(request, awmProvider, wfName)
	if err != nil {
		logger.WithError(err).Error("fail to save workflow to storage")
		return h.failedToStartRun(request, err)
	}

	// Note that run does not yet exist (for parties outside of the deployment context) until preflight workflow is successfully started, thus the insertion happens after the creation of workflow.
	err = h.runStorage.Create(tfRun)
	if err != nil {
		logger.WithError(err).Error("fail to insert terraform run into storage")
		return h.failedToStartRun(request, err)
	}
	logger.Info("terraform run started")

	return startRunResult{
		Deployment: request.Deployment.ID,
		Run:        request.Run.ID,
		Parameters: tfRun.Parameters,
		Error:      nil,
	}
}

func (h StartRunHandler) saveWorkflow(request deploymentevents.StartRunRequest, awmProvider awmclient.AWMProvider, wfName string) error {
	err := h.wfStorage.CreateDeploymentPreflightWorkflow(request.Deployment.ID, request.Deployment.TemplateType, request.Run.ID, awmProvider, wfName)
	if err != nil {
		return err
	}
	return nil
}

func (h StartRunHandler) failedToStartRun(request deploymentevents.StartRunRequest, err error) startRunResult {
	return startRunResult{
		Deployment: request.Deployment.ID,
		Run:        request.Run.ID,
		Error:      err,
	}
}

func (h StartRunHandler) toRespEvent(tid common.TransactionID, request deploymentevents.StartRunRequest, result startRunResult) types.ResponseEvent {
	session := types.CopySessionActors(request.Session)
	if result.Error != nil {
		session.ServiceError = types.ErrorToServiceError(result.Error).GetBase()

		return types.NewEvent(deploymentevents.EventRunPreflightStartFailed, tid, deploymentevents.RunPreflightStartFailed{
			Session:      session,
			TemplateType: request.Deployment.TemplateType,
			Deployment:   request.Deployment.ID,
			Run:          request.Run.ID,
		})
	}
	return types.NewEvent(deploymentevents.EventRunPreflightStarted, tid, deploymentevents.RunPreflightStarted{
		Session:      session,
		TemplateType: request.Deployment.TemplateType,
		Deployment:   request.Deployment.ID,
		Run:          request.Run.ID,
		Parameters:   result.Parameters,
	})
}
