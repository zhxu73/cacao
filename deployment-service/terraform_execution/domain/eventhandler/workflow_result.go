package eventhandler

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/resourcedeletion"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowSucceededHandler handles workflow succeeded event, and dispatch it to another handler based on its purpose
type WorkflowSucceededHandler struct {
	wfStorage ports.DeploymentWorkflowStorage
	timeSrc   ports.TimeSrc
	handlers  specificWorkflowPurposeHandlers
}

// NewWorkflowSucceededHandler ...
func NewWorkflowSucceededHandler(dependencies ports.Ports) WorkflowSucceededHandler {
	return WorkflowSucceededHandler{
		wfStorage: dependencies.WorkflowStorage,
		timeSrc:   dependencies.TimeSrc,
		handlers:  newSpecificWorkflowPurposeHandlers(dependencies),
	}
}

// Handle ...
func (h WorkflowSucceededHandler) Handle(tid common.TransactionID, event awmclient.WorkflowSucceeded, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowSucceededHandler.Handle",
	})
	workflow, err := lookupWorkflow(h.wfStorage, awmclient.AWMProvider(event.Provider), event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to look up workflow in storage")
		return
	}
	logger.Info("workflow record found in storage")
	err = h.wfStorage.WorkflowSucceededAt(awmclient.AWMProvider(workflow.AWMProvider), workflow.WorkflowName, h.timeSrc.Now())
	if err != nil {
		logger.WithError(err).Error("failed to update workflow status in storage")
		return
	}
	handler := dispatchBasedOnPurpose(h.handlers, workflow.Purpose)
	if handler == nil {
		err = fmt.Errorf("unknown workflow purpose %s from WorkflowFailed event, %s", workflow.Purpose, event.WorkflowName)
		logger.WithError(err).Error()
		return
	}
	handler.HandleWorkflowSuccess(tid, event, workflow, sink)
}

// WorkflowFailedHandler handles workflow failed event, and dispatch it to another handler based on its purpose
type WorkflowFailedHandler struct {
	wfStorage ports.DeploymentWorkflowStorage
	timeSrc   ports.TimeSrc
	handlers  specificWorkflowPurposeHandlers
}

// NewWorkflowFailedHandler ...
func NewWorkflowFailedHandler(dependencies ports.Ports) WorkflowFailedHandler {
	return WorkflowFailedHandler{
		wfStorage: dependencies.WorkflowStorage,
		timeSrc:   dependencies.TimeSrc,
		handlers:  newSpecificWorkflowPurposeHandlers(dependencies),
	}
}

// Handle ...
func (h WorkflowFailedHandler) Handle(tid common.TransactionID, event awmclient.WorkflowFailed, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowFailedHandler.Handle",
	})
	workflow, err := lookupWorkflow(h.wfStorage, awmclient.AWMProvider(event.Provider), event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to look up workflow in storage")
		return
	}
	logger.Info("workflow record found in storage")
	err = h.wfStorage.WorkflowFailedAt(awmclient.AWMProvider(workflow.AWMProvider), workflow.WorkflowName, h.timeSrc.Now())
	if err != nil {
		logger.WithError(err).Error("failed to update workflow status in storage")
		return
	}
	handler := dispatchBasedOnPurpose(h.handlers, workflow.Purpose)
	if handler == nil {
		err = fmt.Errorf("unknown workflow purpose %s from WorkflowFailed event, %s", workflow.Purpose, event.WorkflowName)
		logger.WithError(err).Error()
		return
	}
	handler.HandleWorkflowFailure(tid, event, workflow, sink)
}

func lookupWorkflow(wfStorage ports.DeploymentWorkflowStorage, provider awmclient.AWMProvider, workflowName string) (types.DeploymentWorkflow, error) {
	workflow, err := wfStorage.Search(provider, workflowName)
	if err != nil {
		return workflow, err
	}
	if workflow.Status != "" {
		// workflow already has completed and has its status updated, this could be a workflow retry or a collision on workflow name
		return workflow, fmt.Errorf("workflow already completed with status %s at %s", workflow.Status, workflow.EndedAt.String())
	}
	return workflow, err
}

type specificWorkflowPurposeHandlers struct {
	preflight preflightstage.WorkflowHandler
	execution executionstage.WorkflowHandler
	deletion  resourcedeletion.WorkflowHandler
}

func newSpecificWorkflowPurposeHandlers(dependencies ports.Ports) specificWorkflowPurposeHandlers {
	return specificWorkflowPurposeHandlers{
		preflight: preflightstage.NewWorkflowHandler(dependencies),
		execution: executionstage.NewWorkflowHandler(dependencies),
		deletion:  resourcedeletion.NewWorkflowHandler(dependencies),
	}
}

// SpecificPurposeWorkflowHandler is handler that handles workflow of a specific purpose.
// Note the handler will return nil when there is no response event generated.
type SpecificPurposeWorkflowHandler interface {
	HandleWorkflowSuccess(tid common.TransactionID, event awmclient.WorkflowSucceeded, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink)
	HandleWorkflowFailure(tid common.TransactionID, event awmclient.WorkflowFailed, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink)
}

// this may return nil
func dispatchBasedOnPurpose(handlers specificWorkflowPurposeHandlers, purpose types.WorkflowPurpose) SpecificPurposeWorkflowHandler {
	switch purpose {
	case types.PreflightPurpose:
		return handlers.preflight
	case types.ExecutionPurpose:
		return handlers.execution
	case types.DeletionPurpose:
		return handlers.deletion
	default:
		// unknown workflow purpose
		return nil
	}
}
