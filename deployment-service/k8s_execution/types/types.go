package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// IncomingEvent is a incoming event to be handled
type IncomingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	CloudEvent() cloudevents.Event
}

// OutgoingEvent is a outgoing event to be published
type OutgoingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// Config ...
type Config struct {
	LogLevel    string `envconfig:"LOG_LEVEL" default:"debug"`
	CacaoSSHKey string `envconfig:"CACAO_SSH_KEY"` // CACAO private ssh key

	MessagingConfig messaging2.NatsStanMsgConfig
	MongoConfig     db.MongoDBConfig
}

// Override ...
func (c *Config) Override() {
	c.MessagingConfig.DurableName = "deployment_k8s"
	c.MessagingConfig.QueueGroup = "deployment_k8s"
}

// TemplateFile is one file from template
type TemplateFile struct {
	// path of the file relative to the sub-path specified in template source
	Path string
	// content of the file
	Content []byte
}

// ResponseEvent ...
type ResponseEvent struct {
	eventType   common.EventType
	transaction common.TransactionID
	event       interface{}
}

// NewEvent creates a new response event
func NewEvent(eventType common.EventType, transaction common.TransactionID, event interface{}) ResponseEvent {
	return ResponseEvent{eventType: eventType, transaction: transaction, event: event}
}

// EventType returns event type
func (e ResponseEvent) EventType() common.EventType {
	return e.eventType
}

// Transaction ...
func (e ResponseEvent) Transaction() common.TransactionID {
	return e.transaction
}

// ToCloudEvent converts event to cloudevent
func (e ResponseEvent) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEventWithTransactionID(e.event, string(e.eventType), source, e.transaction)
}
