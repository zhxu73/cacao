package adapters

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"io"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/yaml"
	pkgtypes "k8s.io/apimachinery/pkg/types"
	yamlutil "k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/discovery/cached/memory"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/restmapper"
)

// CACAOFieldManager is a field manager constant that we use to indicate changes are made by CACAO
const CACAOFieldManager = "cacao-k8s-execution"

// K8S is an adapter that apply/delete k8s resources defined in template files.
type K8S struct{}

// Ping pings the k8s api by query for server version.
func (k8s *K8S) Ping(ctx context.Context, cfg *rest.Config) error {
	clientset, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		log.WithError(err).Error("fail to create k8s client")
		return err
	}
	version, err := clientset.ServerVersion()
	if err != nil {
		log.WithError(err).Error("fail to fetch server version")
		return err
	}
	log.WithFields(log.Fields{
		"version": version.Major,
	}).Debug("pinged k8s cluster")

	return nil
}

var decUnstructured = yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme)

// Apply ...
func (k8s *K8S) Apply(ctx context.Context, cfg *rest.Config, templateFiles []types.TemplateFile) error {
	// Prepare a RESTMapper to find GVR
	discoverClient, err := discovery.NewDiscoveryClientForConfig(cfg)
	if err != nil {
		return err
	}
	mapper := restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(discoverClient)) // use a cached client to avoid repeat query

	dynamicClient, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return err
	}

	for _, file := range templateFiles {
		var objList []k8sObject
		objList, err = fileBytesToObjectList(file.Content)
		if err != nil {
			return err
		}
		for i := range objList {
			err = k8s.serverSideApplyOne(ctx, dynamicClient, mapper, objList[i])
			if err != nil {
				log.WithError(err).Errorf("fail to apply %s in %s", objList[i].object.GetName(), file.Path)
				return err
			}
		}
	}
	return nil
}

func (k8s *K8S) serverSideApplyOne(ctx context.Context, dynamicClient dynamic.Interface, mapper *restmapper.DeferredDiscoveryRESTMapper, obj k8sObject) error {
	// Find GVR
	mapping, err := mapper.RESTMapping(obj.gvk.GroupKind(), obj.gvk.Version)
	if err != nil {
		return err
	}

	// Obtain REST interface for the GVR
	var dri dynamic.ResourceInterface
	if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
		// namespaced resources should specify the namespace
		dri = dynamicClient.Resource(mapping.Resource).Namespace(obj.object.GetNamespace())
	} else {
		// for cluster-wide resources
		dri = dynamicClient.Resource(mapping.Resource)
	}

	// Marshal object into JSON
	data, err := json.Marshal(obj.object.Object)
	if err != nil {
		return err
	}

	// Server side apply
	_, err = dri.Patch(ctx, obj.object.GetName(), pkgtypes.ApplyPatchType, data, metav1.PatchOptions{
		FieldManager: CACAOFieldManager, // use field manager to indicate that changes is made by CACAO
	})

	return err
}

// Delete ...
func (k8s *K8S) Delete(ctx context.Context, cfg *rest.Config, templateFiles []types.TemplateFile) error {
	// Prepare a RESTMapper to find GVR
	discoverClient, err := discovery.NewDiscoveryClientForConfig(cfg)
	if err != nil {
		return err
	}
	mapper := restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(discoverClient)) // use a cached client to avoid repeat query

	dynamicClient, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return err
	}

	for _, file := range templateFiles {
		var objList []k8sObject
		objList, err = fileBytesToObjectList(file.Content)
		if err != nil {
			return err
		}
		for i := range objList {
			err = k8s.deleteOne(ctx, dynamicClient, mapper, objList[i])
			if err != nil {
				log.WithError(err).Errorf("fail to delete %s in %s", objList[i].object.GetName(), file.Path)
				return err
			}
		}
	}
	return nil
}

func (k8s *K8S) deleteOne(ctx context.Context, dynamicClient dynamic.Interface, mapper *restmapper.DeferredDiscoveryRESTMapper, obj k8sObject) error {

	// Find GVR
	mapping, err := mapper.RESTMapping(obj.gvk.GroupKind(), obj.gvk.Version)
	if err != nil {
		return err
	}

	// Obtain REST interface for the GVR
	var dri dynamic.ResourceInterface
	if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
		// namespaced resources should specify the namespace
		dri = dynamicClient.Resource(mapping.Resource).Namespace(obj.object.GetNamespace())
	} else {
		// for cluster-wide resources
		dri = dynamicClient.Resource(mapping.Resource)
	}

	err = dri.Delete(ctx, obj.object.GetName(), metav1.DeleteOptions{})
	if err != nil {
		if statusError, ok := err.(*apierrors.StatusError); ok {
			if statusError.Status().Code == 404 {
				// 404 NotFound means that the resource does not exist, we will take that as a success and return nil
				log.WithError(err).Info("resource no longer exist, deletion success")
				return nil
			}
		}
		return err
	}
	return nil
}

// this represents a parsed unstructured K8S object
type k8sObject struct {
	object unstructured.Unstructured
	gvk    schema.GroupVersionKind
}

// parse object(s) from the content of a file.
// YAML file allows multiple object to be defined in it.
func fileBytesToObjectList(fileContent []byte) ([]k8sObject, error) {
	decoder := yamlutil.NewYAMLOrJSONDecoder(bytes.NewReader(fileContent), 256)
	var result []k8sObject
	for {
		var rawObj runtime.RawExtension
		err := decoder.Decode(&rawObj)
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return nil, err
		}

		var obj unstructured.Unstructured
		_, gvk, err := decUnstructured.Decode(rawObj.Raw, nil, &obj)
		if err != nil {
			return nil, err
		}
		result = append(result, k8sObject{
			object: obj,
			gvk:    *gvk,
		})
	}
	return result, nil
}
