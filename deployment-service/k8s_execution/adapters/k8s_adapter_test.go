package adapters

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"testing"
)

func Test_fileBytesToObjectList(t *testing.T) {
	type args struct {
		fileContent []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []k8sObject
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "yaml_deployment",
			args: args{
				fileContent: []byte(`
apiVersion: apps/v1
kind: Deployment
metadata:
  name: foobar
`),
			},
			want: []k8sObject{
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "apps/v1",
							"kind":       "Deployment",
							"metadata": map[string]interface{}{
								"name": "foobar",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "apps",
						Version: "v1",
						Kind:    "Deployment",
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "yaml_deployment_divider",
			args: args{
				fileContent: []byte(`
apiVersion: apps/v1
kind: Deployment
metadata:
  name: foobar
---
`),
			},
			want: []k8sObject{
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "apps/v1",
							"kind":       "Deployment",
							"metadata": map[string]interface{}{
								"name": "foobar",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "apps",
						Version: "v1",
						Kind:    "Deployment",
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "yaml_deployment_&_service",
			args: args{
				fileContent: []byte(`
apiVersion: apps/v1
kind: Deployment
metadata:
  name: foobar
---
apiVersion: v1
kind: Service
metadata:
  name: foobar-svc
`),
			},
			want: []k8sObject{
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "apps/v1",
							"kind":       "Deployment",
							"metadata": map[string]interface{}{
								"name": "foobar",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "apps",
						Version: "v1",
						Kind:    "Deployment",
					},
				},
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "v1",
							"kind":       "Service",
							"metadata": map[string]interface{}{
								"name": "foobar-svc",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "",
						Version: "v1",
						Kind:    "Service",
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "json_deployment",
			args: args{
				fileContent: []byte(`
{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "foobar"
  }
}
`),
			},
			want: []k8sObject{
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "apps/v1",
							"kind":       "Deployment",
							"metadata": map[string]interface{}{
								"name": "foobar",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "apps",
						Version: "v1",
						Kind:    "Deployment",
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "json_array_deployment_svc",
			args: args{
				fileContent: []byte(`
[
{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "foobar"
  }
},
{
  "apiVersion": "v1",
  "kind": "Service",
  "metadata": {
    "name": "foobar-svc"
  }
}
]
`),
			},
			want:    nil,
			wantErr: assert.Error, // json file cannot have multiple objects
		},
		{
			name: "json_deployment_svc",
			args: args{
				fileContent: []byte(`
{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "foobar"
  }
}
{
  "apiVersion": "v1",
  "kind": "Service",
  "metadata": {
    "name": "foobar-svc"
  }
}
`),
			},
			want: []k8sObject{
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "apps/v1",
							"kind":       "Deployment",
							"metadata": map[string]interface{}{
								"name": "foobar",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "apps",
						Version: "v1",
						Kind:    "Deployment",
					},
				},
				{
					object: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "v1",
							"kind":       "Service",
							"metadata": map[string]interface{}{
								"name": "foobar-svc",
							},
						},
					},
					gvk: schema.GroupVersionKind{
						Group:   "",
						Version: "v1",
						Kind:    "Service",
					},
				},
			},
			wantErr: assert.NoError,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fileBytesToObjectList(tt.args.fileContent)
			if !tt.wantErr(t, err, fmt.Sprintf("fileBytesToObjectList(%v)", tt.args.fileContent)) {
				return
			}
			assert.Equalf(t, tt.want, got, "fileBytesToObjectList(%v)", tt.args.fileContent)
		})
	}
}
