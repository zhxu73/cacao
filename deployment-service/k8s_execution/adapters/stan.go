package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"sync"
)

// Event implements types.IncomingEvent & types.OutgoingEvent
type Event cloudevents.Event

// EventType returns the event type
func (e Event) EventType() common.EventType {
	return common.EventType(cloudevents.Event(e).Type())
}

// Transaction returns the transaction ID
func (e Event) Transaction() common.TransactionID {
	return messaging.GetTransactionID((*cloudevents.Event)(&e))
}

// CloudEvent returns the event as a cloudevent
func (e Event) CloudEvent() cloudevents.Event {
	return cloudevents.Event(e)
}

// ToCloudEvent returns the event as a cloudevent with the specified source
func (e Event) ToCloudEvent(source string) (cloudevents.Event, error) {
	ce := cloudevents.Event(e)
	ce.SetSource(source)
	return ce, nil
}

// StanAdapter is an adapter for STAN that does both publishing and subscription
type StanAdapter struct {
	conn messaging2.EventConnection
	conf types.Config
}

// NewStanAdapter ...
func NewStanAdapter(conn messaging2.EventConnection) *StanAdapter {
	return &StanAdapter{conn: conn}
}

// Init establish a STAN connection
func (src *StanAdapter) Init(conf types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Init",
	})
	src.conf = conf
	logger.Info("Connect to STAN")
	return nil
}

// Start starts STAN subscription and start receiving events
func (src *StanAdapter) Start(ctx context.Context, wg *sync.WaitGroup, handlers ports.IncomingEventHandlers) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Start",
	})
	err := src.conn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		deploymentevents.EventStartRunRequested: func(ctx context.Context, ce cloudevents.Event, writer messaging2.EventResponseWriter) error {
			var request deploymentevents.StartRunRequest
			err := json.Unmarshal(ce.Data(), &request)
			if err != nil {
				logger.WithError(err).Error("fail to unmarshal cloudevent data into struct")
				return err
			}
			handlers.EventStartRunRequested(request, stanOut{writer: writer})
			return nil
		},
		deploymentevents.EventRunPreflightSucceeded: func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
			return nil
		},
		service.DeploymentDeletionStarted: func(ctx context.Context, ce cloudevents.Event, writer messaging2.EventResponseWriter) error {
			var request service.DeploymentDeletionResult
			err := json.Unmarshal(ce.Data(), &request)
			if err != nil {
				logger.WithError(err).Error("fail to unmarshal cloudevent data into struct")
				return err
			}
			handlers.DeploymentDeletionStarted(request, stanOut{writer: writer})
			return nil
		},
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

type stanOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEvents = (*stanOut)(nil)

func (out stanOut) publish(eventType common.EventType, eventBody any) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "stanOut.publish",
	})
	logger.Trace()
	ce, err := messaging2.CreateCloudEventWithAutoSource(eventBody, eventType)
	if err != nil {
		logger.WithError(err).Error("fail to convert event to cloudevent")
		return
	}
	err = out.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Error("fail to publish event to STAN")
		return
	}
	logger.WithFields(log.Fields{
		"eventType":   eventType,
		"transaction": messaging2.GetTransactionID(&ce),
	}).Info("event published")
}

// EventRunPreflightStarted ...
func (out stanOut) EventRunPreflightStarted(started deploymentevents.RunPreflightStarted) {
	out.publish(deploymentevents.EventRunPreflightStarted, started)
}

// EventRunPreflightStartFailed ...
func (out stanOut) EventRunPreflightStartFailed(failed deploymentevents.RunPreflightStartFailed) {
	out.publish(deploymentevents.EventRunPreflightStartFailed, failed)
}

// EventRunExecutionSucceeded ...
func (out stanOut) EventRunExecutionSucceeded(succeeded deploymentevents.RunExecutionSucceeded) {
	out.publish(deploymentevents.EventRunExecutionSucceeded, succeeded)
}

// EventRunExecutionFailed ...
func (out stanOut) EventRunExecutionFailed(failed deploymentevents.RunExecutionFailed) {
	out.publish(deploymentevents.EventRunExecutionFailed, failed)
}

// EventDeploymentDeletionCleanupSucceeded ...
func (out stanOut) EventDeploymentDeletionCleanupSucceeded(result deploymentevents.DeploymentDeletionCleanupResult) {
	out.publish(deploymentevents.EventDeploymentDeletionCleanupSucceeded, result)
}

// EventDeploymentDeletionCleanupFailed ...
func (out stanOut) EventDeploymentDeletionCleanupFailed(result deploymentevents.DeploymentDeletionCleanupResult) {
	out.publish(deploymentevents.EventDeploymentDeletionCleanupFailed, result)
}

// EventDeploymentDeletionCleanupStarted ...
func (out stanOut) EventDeploymentDeletionCleanupStarted(started deploymentevents.DeploymentDeletionCleanupStarted) {
	out.publish(deploymentevents.EventDeploymentDeletionCleanupStarted, started)
}
