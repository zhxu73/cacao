package adapters

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"reflect"
	"testing"
)

func TestGitAdapter_Clone(t *testing.T) {
	type args struct {
		templateSrc   service.TemplateSource
		gitCredential *service.CredentialModel
	}
	tests := []struct {
		name    string
		args    args
		want1   []types.TemplateFile
		want2   string
		wantErr bool
	}{
		{
			name: "dir path",
			args: args{
				templateSrc: service.TemplateSource{
					Type: service.TemplateSourceTypeGit,
					URI:  "https://gitlab.com/zhxu73/test-k8s-template", // FIXME use a repo under cyverse group
					AccessParameters: map[string]interface{}{
						"branch": "main",
						"path":   "just-manifests",
					},
					Visibility: service.TemplateSourceVisibilityPublic,
				},
				gitCredential: nil,
			},
			want1: []types.TemplateFile{
				{
					Path:    "nginx-test.yaml",
					Content: []byte("non-empty"),
				},
			},
			want2:   "2cb4aeb9c88c8a4a4ce255e90407bd456e447d59",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := GitAdapter{}
			got, got1, err := g.Clone(tt.args.templateSrc, tt.args.gitCredential)
			if (err != nil) != tt.wantErr {
				t.Errorf("Clone() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for i := range got {
				if len(got[i].Content) > 0 {
					got[i].Content = []byte("non-empty") // only check if content is non-empty
				}
			}
			if !reflect.DeepEqual(got, tt.want1) {
				t.Errorf("Clone() got = %v, want1 %v", got, tt.want1)
			}
			if got1 != tt.want2 {
				t.Errorf("Clone() got1 = %v, want1 %v", got1, tt.want2)
			}
		})
	}
}
