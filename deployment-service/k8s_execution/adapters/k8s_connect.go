package adapters

import (
	"context"
	"crypto/tls"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"golang.org/x/crypto/ssh"
	"io"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
	"net"
	"net/http"
	"time"
)

// K8SConnector builds a "connection" to k8s cluster in the form of a REST config,
// this allows us to tunnel the connection via other protocol (e.g. SSH)
type K8SConnector struct {
}

// Ping builds a "connection" to test if config works.
func (kc K8SConnector) Ping(config types.K8SConnectionConfig) error {
	client, err := connectToSSH(config)
	if err != nil {
		return err
	}
	defer client.Close()
	log.WithFields(log.Fields{
		"sshServerVersion": client.ServerVersion(),
	}).Info()
	return nil
}

// BuildConnection builds a connection to k8s api
func (kc K8SConnector) BuildConnection(config types.K8SConnectionConfig) (*rest.Config, io.Closer, error) {
	logger := log.WithFields(log.Fields{
		"function": "K8SConnector.BuildConnection",
		"sshHost":  config.SSHTunnelHost,
		"sshPort":  config.SSHTunnelPort,
	})
	restConfig, err := clientcmd.BuildConfigFromKubeconfigGetter("", func() (*clientcmdapi.Config, error) {
		return config.KubeConfig, nil
	})
	if err != nil {
		logger.WithError(err).Error("fail to build rest config from kubeconfig")
		return nil, nil, err
	}
	if config.SSHTunnel {
		sshRoundTripper, sshClient, err := SSHTunneledK8SConnection(config)
		if err != nil {
			logger.WithError(err).Error("fail to build an ssh tunnel for k8s api access")
			return nil, nil, err
		}
		restConfig.Transport = sshRoundTripper
		restConfig.TLSClientConfig = rest.TLSClientConfig{}

		// test connection
		err = testRESTConfig(restConfig)
		if err != nil {
			sshClient.Close()
			logger.WithError(err).Error("testing rest config, fail to reach k8s api")
			return nil, nil, err
		}

		return restConfig, sshClient, nil
	}
	return restConfig, nil, nil
}

func testRESTConfig(restConfig *rest.Config) error {
	clientset, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		log.WithError(err).Error("fail to create k8s client from rest config for testing connection")
		return err
	}
	version, err := clientset.ServerVersion()
	if err != nil {
		log.WithError(err).Error("fail to get k8s server version for testing connection")
		return err
	}
	log.WithField("serverVersion", version.String()).Info("k8s server version")
	return nil
}

// this serves as comparison for what the default transport is
func normalK8SConnection(config types.K8SConnectionConfig) (http.RoundTripper, io.Closer, error) {
	return http.DefaultTransport, nil, nil
}

func connectToSSH(config types.K8SConnectionConfig) (*ssh.Client, error) {
	signer, err := ssh.ParsePrivateKey(config.SSHPrivateKey)
	if err != nil {
		return nil, err
	}

	var expectedPublicKey ssh.PublicKey
	if len(config.SSHHostKey) > 0 {
		expectedPublicKey, err = ssh.ParsePublicKey(config.SSHHostKey)
		if err != nil {
			return nil, err
		}
	}

	sshConfig := &ssh.ClientConfig{
		User: config.SSHTunnelUsername,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			if expectedPublicKey != nil {
				return ssh.FixedHostKey(expectedPublicKey)(hostname, remote, key)
			}
			log.WithFields(log.Fields{"sshHost": config.SSHTunnelHost, "sshHostKey": key}).Info("skip verify ssh host key")
			return nil
		},
	}
	return ssh.Dial("tcp", fmt.Sprintf("%s:%d", config.SSHTunnelHost, config.SSHTunnelPort), sshConfig)
}

// SSHTunneledK8SConnection connect to host via SSH and create a http.RoundTripper based on SSH tunnel.
// The SSH tunnel is returned as io.Closer, caller need to close it to avoid connection leaks.
func SSHTunneledK8SConnection(config types.K8SConnectionConfig) (http.RoundTripper, io.Closer, error) {
	restConfig, err := clientcmd.BuildConfigFromKubeconfigGetter("", func() (*clientcmdapi.Config, error) {
		return config.KubeConfig, nil
	})
	if err != nil {
		return nil, nil, err
	}
	transportConfig, err := restConfig.TransportConfig()
	if err != nil {
		return nil, nil, err
	}
	var tlsClientConf *tls.Config
	if transportConfig.HasCertAuth() {
		// When using cert as the authentication methods for k8s api, we need to set the client cert in the ssh transport.
		// to do that, we use function from client-go to build a http transport, and then copy the TLSClientConfig from it.

		// build a http transport from rest.Config
		roundTripperInterface, err := rest.TransportFor(restConfig)
		if err != nil {
			return nil, nil, err
		}
		rt, ok := roundTripperInterface.(*http.Transport)
		if !ok {
			// corresponding unit test should fail if this panic
			panic("RoundTripper built from rest config is not *http.Transport")
		}

		// copy the TLS config from the transport
		tlsClientConf = rt.TLSClientConfig
		log.WithFields(log.Fields{"sshHost": config.SSHTunnelHost, "sshHostPort": config.SSHTunnelPort}).Info("cert auth")
	} else {
		// when using non-cert auth (e.g. token, password), use default
		tlsClientConf = &tls.Config{}
		log.WithFields(log.Fields{"sshHost": config.SSHTunnelHost, "sshHostPort": config.SSHTunnelPort}).Info("not cert auth")
	}

	client, err := connectToSSH(config)
	if err != nil {
		return nil, nil, err
	}

	var sshTransport http.RoundTripper = &http.Transport{
		Proxy:                  nil,
		OnProxyConnectResponse: nil,
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			return client.Dial(network, addr)
		},
		DialTLSContext:         nil,
		TLSClientConfig:        tlsClientConf,
		TLSHandshakeTimeout:    10 * time.Second,
		DisableKeepAlives:      false,
		DisableCompression:     false,
		MaxIdleConns:           3,
		MaxIdleConnsPerHost:    0,
		MaxConnsPerHost:        5,
		IdleConnTimeout:        30 * time.Second,
		ResponseHeaderTimeout:  0,
		ExpectContinueTimeout:  1 * time.Second,
		TLSNextProto:           nil,
		ProxyConnectHeader:     nil,
		GetProxyConnectHeader:  nil,
		MaxResponseHeaderBytes: 0,
		WriteBufferSize:        0,
		ReadBufferSize:         0,
		ForceAttemptHTTP2:      true,
	}
	// return ssh.Client as io.Closer so caller of this function can close it
	return sshTransport, client, nil
}
