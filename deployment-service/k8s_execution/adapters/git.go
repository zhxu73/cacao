package adapters

import (
	"encoding/json"
	"github.com/go-git/go-billy/v5"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/git"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"io"
	"os"
	"path"
	"sigs.k8s.io/yaml"
	"strings"
)

// GitAdapter ...
type GitAdapter struct{}

var _ ports.Git = (*GitAdapter)(nil)

// Clone clones the git repo. This returns a list of template files in the specified directory and commit hash of the HEAD.
func (g GitAdapter) Clone(templateSrc service.TemplateSource, gitCredential *service.CredentialModel) ([]types.TemplateFile, string, error) {
	repoConfig, err := g.generateRepoConfig(templateSrc, gitCredential)
	if err != nil {
		return nil, "", err
	}
	repo, err := git.NewGitCloneToRAM(&repoConfig, &git.RAMStorageOption{
		SizeLimit: 10 << 20, // 10 MB
	})
	if err != nil {
		return nil, "", err
	}
	head, err := repo.Repository.Head()
	if err != nil {
		return nil, "", err
	}
	fileContents, err := g.readFiles(repo, templateSrc)
	if err != nil {
		return nil, "", err
	}
	return fileContents, head.Hash().String(), err
}

func (g GitAdapter) generateRepoConfig(templateSrc service.TemplateSource, gitCredential *service.CredentialModel) (git.RepositoryConfig, error) {

	var conf = git.RepositoryConfig{
		URL:           templateSrc.URI,
		Username:      "",
		Password:      "",
		ReferenceType: "",
		ReferenceName: "",
	}
	if gitCredential != nil {
		var gitCred struct {
			Username string `json:"GIT_USERNAME"`
			Password string `json:"GIT_PASSWORD"`
		}
		err := json.Unmarshal([]byte(gitCredential.Value), &gitCred)
		if err != nil {
			return git.RepositoryConfig{}, err
		}
		conf.Username = gitCred.Username
		conf.Password = gitCred.Password
	}
	branchRaw, ok := templateSrc.AccessParameters["branch"]
	if ok {
		conf.ReferenceType = git.BranchReferenceType
		branch, ok := branchRaw.(string)
		if !ok {
			return git.RepositoryConfig{}, service.NewCacaoInvalidParameterError("branch in template access parameters should be string")
		}
		conf.ReferenceName = branch
		return conf, nil
	}
	tagRaw, ok := templateSrc.AccessParameters["tag"]
	if ok {
		conf.ReferenceType = git.TagReferenceType
		tag, ok := tagRaw.(string)
		if !ok {
			return git.RepositoryConfig{}, service.NewCacaoInvalidParameterError("tag in template access parameters should be string")
		}
		conf.ReferenceName = tag
		return conf, nil
	}
	return git.RepositoryConfig{}, service.NewCacaoInvalidParameterError("bad access parameters in template")
}

func (g GitAdapter) getBaseDirectoryPath(templateSrc service.TemplateSource) (string, error) {
	var dirPath string
	pathRaw, ok := templateSrc.AccessParameters["path"]
	if ok {
		dirPath, ok = pathRaw.(string)
		if !ok {
			return "", service.NewCacaoInvalidParameterError("path access parameters for template is not string")
		}
	} else {
		dirPath = "."
	}
	return dirPath, nil
}

// TODO consider implement recursive read (all yaml files in the directory tree, not just in the directory).
func (g GitAdapter) readFiles(repo *git.Clone, templateSrc service.TemplateSource) ([]types.TemplateFile, error) {
	baseDirPath, err := g.getBaseDirectoryPath(templateSrc)
	if err != nil {
		return nil, err
	}
	fileInfosInDir, err := repo.GetFileSystem().ReadDir(baseDirPath)
	if err != nil {
		return nil, err
	}
	var fileContentList = make([]types.TemplateFile, 0)
	for _, fileInfo := range fileInfosInDir {
		filePath := path.Join(baseDirPath, fileInfo.Name())

		if fileInfo.IsDir() {
			continue
		}
		if !strings.HasSuffix(fileInfo.Name(), ".yaml") && !strings.HasSuffix(fileInfo.Name(), ".yml") {
			continue
		}
		log.Debug(filePath)

		var file billy.File
		file, err = repo.GetFileSystem().OpenFile(filePath, os.O_RDONLY, 0600)
		if err != nil {
			return nil, err
		}

		var fileContent []byte
		fileContent, err = io.ReadAll(file)
		if err != nil {
			file.Close()
			return nil, err
		}
		file.Close()
		err = g.parseManifestFile(fileContent)
		if err != nil {
			return nil, err
		}
		fileContentList = append(fileContentList, types.TemplateFile{
			Path:    fileInfo.Name(), // use the file path relative to base dir, since we are not doing recursive yet, this is just the filename
			Content: fileContent,
		})
	}
	return fileContentList, nil
}

// check if is valid yaml
func (g GitAdapter) parseManifestFile(content []byte) error {
	var obj interface{}
	return yaml.Unmarshal(content, &obj)
}
