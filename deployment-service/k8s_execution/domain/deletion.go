package domain

import (
	"context"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
	"time"
)

// DeletionHandler handles deployment deletion request.
type DeletionHandler struct {
	providerMS           ports.ProviderMicroservice
	templateMS           ports.TemplateMicroservice
	credMS               ports.CredentialMicroservice
	deploymentMetadataMS ports.DeploymentMetadataService
	git                  ports.Git
	k8s                  ports.K8S
	k8sConnector         ports.K8SConnector
}

// Handle ...
func (h DeletionHandler) Handle(req service.DeploymentDeletionResult, sink ports.OutgoingEvents) {
	if h.shouldIgnoreRequest(req) {
		return
	}
	err := h.handle(req)
	if err != nil {
		log.WithError(err).Error("fail to unmarshal event as deletion request")
		sink.EventDeploymentDeletionCleanupFailed(deploymentevents.DeploymentDeletionCleanupResult{
			Session: service.Session{
				SessionActor:    req.SessionActor,
				SessionEmulator: req.SessionEmulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
			TemplateType: req.TemplateType,
			Deployment:   req.ID,
		})
	} else {
		sink.EventDeploymentDeletionCleanupSucceeded(deploymentevents.DeploymentDeletionCleanupResult{
			Session:      service.CopySessionActors(req.Session),
			TemplateType: req.TemplateType,
			Deployment:   req.ID,
		})
	}
}

// filter out deployment that is not k8s related (e.g. terraform_openstack)
func (h DeletionHandler) shouldIgnoreRequest(req service.DeploymentDeletionResult) bool {
	switch req.TemplateType {
	case "kubernetes":
		return false
	//case "helm":
	//	return false
	default:
		return true
	}
}

// DeletionData ...
type DeletionData struct {
	providerMetadata types.K8SProviderMetadata
	template         service.Template
	k8sConnConfig    types.K8SConnectionConfig
	gitCred          *service.CredentialModel
}

func (h DeletionHandler) handle(req service.DeploymentDeletionResult) error {
	// fetch data
	prerequisiteData, err := h.gather(req)
	if err != nil {
		return err
	}

	k8sRESTConfig, connectionCloser, err := h.k8sConnector.BuildConnection(prerequisiteData.k8sConnConfig)
	if err != nil {
		return err
	}
	if connectionCloser != nil {
		defer connectionCloser.Close()
	}

	// clone template
	templateFiles, _, err := h.git.Clone(prerequisiteData.template.GetSource(), prerequisiteData.gitCred)
	if err != nil {
		return err
	}

	// delete
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Minute*5)
	err = h.k8s.Delete(ctx, k8sRESTConfig, templateFiles)
	cancelFunc()
	if err != nil {
		return err
	}

	return nil
}

func (h DeletionHandler) gather(req service.DeploymentDeletionResult) (DeletionData, error) {
	deployment, err := h.deploymentMetadataMS.Get(service.ActorFromSession(req.Session), req.ID)
	if err != nil {
		return DeletionData{}, err
	}
	provider, err := h.providerMS.Get(service.ActorFromSession(req.Session), deployment.PrimaryCloud)
	if err != nil {
		return DeletionData{}, err
	}

	if len(deployment.CloudCredentials) == 0 {
		return DeletionData{}, service.NewCacaoInvalidParameterError("no cloud credential specified for deployment")
	}
	var firstCredID string
	for credID := range deployment.CloudCredentials {
		firstCredID = credID
		break
	}
	cloudCred, err := h.credMS.Get(service.ActorFromSession(req.Session), req.GetSessionActor(), firstCredID)
	if err != nil {
		log.WithError(err).WithField("cred", firstCredID).Error("fail to fetch cloud cred")
		return DeletionData{}, err
	}

	var k8sProviderMetadata types.K8SProviderMetadata
	err = mapstructure.Decode(provider.Metadata, &k8sProviderMetadata)
	if err != nil {
		log.WithError(err).Error("fail to decode k8s provider metadata")
		return DeletionData{}, err
	}
	var k8sConnConfig types.K8SConnectionConfig
	if k8sProviderMetadata.K8SApiSSHTunnel {
		sshCred, err := h.credMS.Get(service.ActorFromSession(req.Session), req.SessionActor, k8sProviderMetadata.K8SApiSSHTunnelConfig.SSHKeyCredID)
		if err != nil {
			return DeletionData{}, err
		}
		k8sConnConfig, err = getK8SConnectionConfig(provider, cloudCred, sshCred)
		if err != nil {
			return DeletionData{}, err
		}
	} else {
		k8sConnConfig, err = getK8SConnectionConfig(provider, cloudCred, nil)
		if err != nil {
			return DeletionData{}, err
		}
	}

	template, err := h.templateMS.Get(service.ActorFromSession(req.Session), deployment.Template)
	if err != nil {
		return DeletionData{}, err
	}

	var gitCred *service.CredentialModel
	if len(deployment.GitCredential) > 0 {
		gitCred, err = h.credMS.Get(service.ActorFromSession(req.Session), req.GetSessionActor(), deployment.GitCredential)
		if err != nil {
			return DeletionData{}, err
		}
	}
	return DeletionData{
		providerMetadata: k8sProviderMetadata,
		template:         template,
		k8sConnConfig:    k8sConnConfig,
		gitCred:          gitCred,
	}, nil
}

func getK8SConnectionConfig(provider *service.ProviderModel, kubeConfigCred *service.CredentialModel, sshCred *service.CredentialModel) (types.K8SConnectionConfig, error) {
	providerMetadata := types.K8SProviderMetadata{}
	err := mapstructure.Decode(provider.Metadata, &providerMetadata)
	if err != nil {
		log.WithError(err).Error("fail to decode k8s provider metadata")
		return types.K8SConnectionConfig{}, err
	}
	var kubeconfig *clientcmdapi.Config
	kubeconfig, err = clientcmd.Load([]byte(kubeConfigCred.Value))
	if err != nil {
		log.WithError(err).WithField("cred", kubeConfigCred.ID).Error("fail to parse credential as kubeconfig")
		return types.K8SConnectionConfig{}, err
	}
	if sshCred != nil {
		if sshCred.Type != service.PrivateSSHKeyCredentialType {
			err = service.NewCacaoInvalidParameterError("ssh credential specified in provider metadata is not private ssh key type")
			log.WithError(err).Error("ssh private key cred for ssh tunneling is wrong type")
			return types.K8SConnectionConfig{}, err
		}
		return types.K8SConnectionConfig{
			KubeConfig: kubeconfig,
			SSHTunnelConfig: types.SSHTunnelConfig{
				SSHTunnel:         true,
				SSHTunnelHost:     providerMetadata.K8SApiSSHTunnelConfig.Host,
				SSHTunnelPort:     providerMetadata.K8SApiSSHTunnelConfig.Port,
				SSHTunnelUsername: providerMetadata.K8SApiSSHTunnelConfig.Username,
				SSHPrivateKey:     []byte(sshCred.Value),
				SSHHostKey:        []byte(providerMetadata.K8SApiSSHTunnelConfig.SSHHostKey),
			},
		}, nil
	}
	return types.K8SConnectionConfig{
		KubeConfig: kubeconfig,
		SSHTunnelConfig: types.SSHTunnelConfig{
			SSHTunnel: false,
		},
	}, nil
}
