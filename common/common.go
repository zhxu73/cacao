// Package common provides functions and structs used by multiple Cacao services
// for interacting with Kubernetes clusters, NATS queues, and other services
package common

import (
	"github.com/rs/xid"
	"os"
	"regexp"
	"strings"
)

// GetNATSInfo returns a map containing necessary NATS connection variables. It is
// used to get this information from environment variables or use the defaults
func GetNATSInfo(defaultClusterID string, defaultClientID string, defaultConnAddress string) map[string]string {
	natsInfo := make(map[string]string)
	natsInfo["cluster_id"] = os.Getenv("NATS_CLUSTER_ID")
	if len(natsInfo["cluster_id"]) == 0 {
		natsInfo["cluster_id"] = defaultClusterID
	}
	natsInfo["client_id"] = os.Getenv("NATS_CLIENT_ID")
	if len(natsInfo["client_id"]) == 0 {
		natsInfo["client_id"] = defaultClientID
	}

	// Append xid to client ID to allow scaling
	natsInfo["client_id"] += "-" + xid.New().String()
	natsInfo["address"] = os.Getenv("NATS_ADDRESS")
	if len(natsInfo["address"]) == 0 {
		natsInfo["address"] = defaultConnAddress
	}
	return natsInfo
}

// FixName will make a string K8s-valid by making it lowercase and hyphenated
func FixName(name string) string {
	re := regexp.MustCompile(`[/:\.]`)
	return strings.ToLower(re.ReplaceAllString(name, "-"))
}

// CreateResourceName is used to create a K8s resource name that is both unique
// and informative to users:
//
//	<username:limit to 8>-<name:limit to 32 chars>-<xid>
func CreateResourceName(username, name, id string) string {
	if len(username) > 8 {
		username = username[0:8]
	}
	if len(name) > 32 {
		name = name[0:32]
	}
	return username + "-" + name + "-" + id
}

// PtrOf return a pointer to the data.
//
// Useful for converting constants and literal,
// e.g. PtrOf(true), PtrOf("foobar")
func PtrOf[T any](data T) *T {
	return &data
}
