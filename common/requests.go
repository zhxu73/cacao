package common

// Request is an interface that allows for reading common HTTP request fields and
// can be extended by service-specific types for reading additional fields
type Request interface {
	GetClusterID() string
	GetRegistrySecretID() string
	GetGitSecretID() string
	GetWorkflowID() string
	GetBuildID() string
	GetRunID() string
	GetUser() User
	GetError() Error

	SetClusterID(value string)
	SetRegistrySecretID(value string)
	SetGitSecretID(value string)
	SetWorkflowID(value string)
	SetBuildID(value string)
	SetRunID(value string)
	SetUser(value User)
}

// BasicRequest is the simplest implementation of the Request interface and is intended
// to be included as an embedded/anonymous struct in extending types
type BasicRequest struct {
	User             User   `json:"user,omitempty"`
	ClusterID        string `json:"cluster_id,omitempty"`
	RegistrySecretID string `json:"registry_secret_id,omitempty"`
	GitSecretID      string `json:"git_secret_id,omitempty"`
	WorkflowID       string `json:"workflow_id,omitempty"`
	BuildID          string `json:"build_id,omitempty"`
	RunID            string `json:"run_id,omitempty"`

	Error Error `json:"error,omitempty"`
}

// GetRedacted will return a new BasicRequest with the confidential fields removed from
// the User's Secrets and Clusters
func (r *BasicRequest) GetRedacted() BasicRequest {
	redactedRequest := *r
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

// GetClusterID ...
func (r *BasicRequest) GetClusterID() string {
	return r.ClusterID
}

// GetRegistrySecretID ...
func (r *BasicRequest) GetRegistrySecretID() string {
	return r.RegistrySecretID
}

// GetGitSecretID ...
func (r *BasicRequest) GetGitSecretID() string {
	return r.GitSecretID
}

// GetWorkflowID ...
func (r *BasicRequest) GetWorkflowID() string {
	return r.WorkflowID
}

// GetBuildID ...
func (r *BasicRequest) GetBuildID() string {
	return r.BuildID
}

// GetRunID ...
func (r *BasicRequest) GetRunID() string {
	return r.RunID
}

// GetUser ...
func (r *BasicRequest) GetUser() User {
	return r.User
}

// GetError ...
func (r *BasicRequest) GetError() Error {
	return r.Error
}

// SetClusterID ...
func (r *BasicRequest) SetClusterID(value string) {
	r.ClusterID = value
}

// SetRegistrySecretID ...
func (r *BasicRequest) SetRegistrySecretID(value string) {
	r.RegistrySecretID = value
}

// SetGitSecretID ...
func (r *BasicRequest) SetGitSecretID(value string) {
	r.GitSecretID = value
}

// SetWorkflowID ...
func (r *BasicRequest) SetWorkflowID(value string) {
	r.WorkflowID = value
}

// SetBuildID ...
func (r *BasicRequest) SetBuildID(value string) {
	r.BuildID = value
}

// SetRunID ...
func (r *BasicRequest) SetRunID(value string) {
	r.RunID = value
}

// SetUser ...
func (r *BasicRequest) SetUser(value User) {
	r.User = value
}
