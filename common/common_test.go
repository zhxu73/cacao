package common

import (
	"os"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetNATSInfoDefaults(t *testing.T) {
	natsInfo := GetNATSInfo("defaultClusterID", "defaultClientID", "defaultConnAddress")
	assert.Equal(t, "defaultClusterID", natsInfo["cluster_id"])
	assert.Regexp(t, regexp.MustCompile("defaultClientID-.{20}"), natsInfo["client_id"])
	assert.Equal(t, "defaultConnAddress", natsInfo["address"])
}

func TestGetNATSInfoEnv(t *testing.T) {
	os.Setenv("NATS_CLUSTER_ID", "envClusterID")
	os.Setenv("NATS_CLIENT_ID", "envClientID")
	os.Setenv("NATS_ADDRESS", "envAddress")

	natsInfo := GetNATSInfo("defaultClusterID", "defaultClientID", "defaultConnAddress")
	assert.Equal(t, "envClusterID", natsInfo["cluster_id"])
	assert.Regexp(t, regexp.MustCompile("envClientID-.{20}"), natsInfo["client_id"])
	assert.Equal(t, "envAddress", natsInfo["address"])
}

func TestFixName(t *testing.T) {
	cases := map[string]string{
		"test":        "test",
		"TEST":        "test",
		"TEST:latest": "test-latest",
		"test/test":   "test-test",
		"test.test":   "test-test",
	}

	for input, expected := range cases {
		assert.Equal(t, expected, FixName(input))
	}
}

func TestCreateResourceName(t *testing.T) {
	assert.Equal(t, "test-use-workflow-name-aaaaaaaaaaaaaaaaaaaa", CreateResourceName("test-user", "workflow-name", "aaaaaaaaaaaaaaaaaaaa"))
	assert.Equal(t, "test-use-this-is-a-very-long-workflow-nam-aaaaaaaaaaaaaaaaaaaa", CreateResourceName("test-user", "this-is-a-very-long-workflow-name-that-will-be-shortened", "aaaaaaaaaaaaaaaaaaaa"))
}
