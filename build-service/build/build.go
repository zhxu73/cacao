// Package build provides functions for creating Kubernetes resources required for
// building and uploading a container image
package build

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/common/wfdefcommon"

	batch "k8s.io/api/batch/v1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// createRegistrySecret will create a K8s Secret containing necessary information
// for uploading an image to a container registry
func createRegistrySecret(id string, secret common.Secret) (k8Secret *core.Secret, err error) {
	var dockerConfig string
	var gcrKey []byte
	if secret.Type == common.DockerhubSecret {
		dockerConfig = fmt.Sprintf(
			`{"auths": {"https://index.docker.io/v1/": {"auth": "%s"}}}`,
			base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", secret.Username, secret.Value))),
		)
	} else if secret.Type == common.GCRSecret {
		gcrKey, err = base64.StdEncoding.DecodeString(secret.Value)
		if err != nil {
			return
		}
	}
	k8Secret = &core.Secret{
		TypeMeta:   meta.TypeMeta{Kind: "Secret", APIVersion: "v1"},
		ObjectMeta: meta.ObjectMeta{Name: id},
		StringData: map[string]string{
			"config.json":        dockerConfig,
			"kaniko-secret.json": string(gcrKey),
		},
		Type: core.SecretTypeOpaque,
	}
	return
}

// createJob creates the K8s Job used to generate Pods to build and upload the image
func createJob(name, id string, steps []*common.BuildStep, secret common.Secret, url string, branch string) *batch.Job {
	url = strings.TrimLeft(strings.TrimLeft(url, "https://"), "http://")
	// Get correct secret values if needed
	var authString string
	if len(secret.Value) > 0 {
		authString = fmt.Sprintf("oauth2:%s@", secret.Value)
	}
	context := fmt.Sprintf("git://%s%s.git#refs/heads/%s", authString, url, branch)

	// Prepare list of containers for doing all builds in one Job
	var containers []core.Container
	for i, step := range steps {
		// Prepare Container fields
		args := []string{
			"--dockerfile=" + step.Dockerfile,
			"--context=" + context,
			"--destination=" + step.Image,
		}
		for _, arg := range createBuildArgs(step.Args) {
			args = append(args, arg)
		}
		var envVar core.EnvVar
		if step.Image[:3] == "gcr" {
			envVar = core.EnvVar{Name: "GOOGLE_APPLICATION_CREDENTIALS", Value: "/secret/kaniko-secret.json"}
		} else {
			envVar = core.EnvVar{Name: "DOCKER_CONFIG", Value: "/secret"}
		}

		// Add new container to slice
		containers = append(containers, core.Container{
			Name:  fmt.Sprintf("kaniko-%d", i),
			Image: "gcr.io/kaniko-project/executor:latest",
			Args:  args,
			VolumeMounts: []core.VolumeMount{
				{Name: name, MountPath: "/secret"},
			},
			Env: []core.EnvVar{envVar},
		})
	}
	backoffLimit := int32(1)
	return &batch.Job{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"cacao_id": id,
			},
		},
		Spec: batch.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: core.PodTemplateSpec{
				Spec: core.PodSpec{
					Containers:    containers,
					RestartPolicy: core.RestartPolicyOnFailure,
					Volumes: []core.Volume{
						{
							Name: name,
							VolumeSource: core.VolumeSource{
								Secret: &core.SecretVolumeSource{SecretName: name},
							},
						},
					},
				},
			},
		},
	}
}

// build will use the create functions to define K8s resources before using the
// clients to actually deploy the resources on a cluster
func build(request Request, natsInfo map[string]string) (err error) {
	name := common.CreateResourceName(request.GetUser().Username, request.WorkflowDefinition.Name, request.GetBuildID())

	secret, ok := request.GetUser().Secrets[request.GetRegistrySecretID()]
	if !ok {
		err = fmt.Errorf("error getting container registry secret '%s'", request.GetRegistrySecretID())
		return
	}

	registrySecret, err := createRegistrySecret(name, secret)
	if err != nil {
		err = fmt.Errorf("error creating registry secret '%s': %v", request.GetRegistrySecretID(), err)
		return
	}

	// Git secret is not required so failure is ok
	gitSecret, ok := request.GetUser().Secrets[request.GetGitSecretID()]
	if !ok && len(request.GetGitSecretID()) != 0 {
		err = fmt.Errorf("error getting git secret '%s'", request.GetGitSecretID())
		return
	}
	job := createJob(name, request.GetBuildID(), request.WorkflowDefinition.Build, gitSecret, request.WorkflowDefinition.Repository.URL, request.WorkflowDefinition.Repository.Branch)

	cluster, ok := request.GetUser().Clusters[request.GetClusterID()]
	if !ok {
		err = fmt.Errorf("error getting cluster config '%s'", request.GetClusterID())
		return
	}
	// Set namespaceName if specified, otherwise use default for the Cluster
	namespaceName := request.Namespace
	if len(namespaceName) == 0 {
		namespaceName = cluster.DefaultNamespace
	}
	clientsets, err := wfdefcommon.NewUserClusterK8sClientsets(cluster.Config)
	if err != nil {
		err = fmt.Errorf("error getting user cluster clienetsets for cluster '%s': %v", cluster.ID, err)
		return
	}
	// Create image registry Secret
	_, err = clientsets.CreateSecret(registrySecret, namespaceName)
	if err != nil && err.Error() == fmt.Sprintf(`secrets "%s" already exists`, registrySecret.Name) {
		_, err = clientsets.UpdateSecret(registrySecret, namespaceName)
	}
	if err != nil {
		err = fmt.Errorf("error creating registry secret '%s': %v", registrySecret.Name, err)
		return
	}
	// Create Job for build
	_, err = clientsets.CreateJob(job, namespaceName)
	if err != nil && err.Error() == fmt.Sprintf(`job "%s" already exists`, job.Name) {
		_, err = clientsets.UpdateJob(job, namespaceName)
	}
	if err != nil {
		err = fmt.Errorf("error creating build job '%s': %v", job.Name, err)
		return
	}

	msg, err := json.Marshal(request)
	if err != nil {
		return err
	}
	common.StreamingPublish(msg, natsInfo, "Build.Job", "Build")
	return
}

// createBuildArgs is used to create an array of build-args to use as input to
// the Build Step container
func createBuildArgs(args []core.EnvVar) (result []string) {
	for _, arg := range args {
		result = append(result, "--build-arg")
		result = append(result, fmt.Sprintf("%s=%s", arg.Name, arg.Value))
	}
	return
}
