# Build Service
> Legacy service, currently not used

The Build Service is responsible for building container images from a git repository and uploading to a container registry such as Dockerhub.

[Read more...](../docs/developers/build-service.md)
