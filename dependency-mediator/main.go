package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/dependency-mediator/adapters"
	"gitlab.com/cyverse/cacao/dependency-mediator/domain"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"io"
)

var config types.Config

func init() {
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Fatal("fail to load config from env var")
	}
	config.Override()
	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)
}

func main() {
	natsConn, err := config.MsgConfig.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&natsConn)
	stanConn, err := config.MsgConfig.ConnectStan()
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&stanConn)
	stan := adapters.NewStanAdapter(&stanConn)
	credSvc := adapters.NewCredentialService(&natsConn, &stanConn)
	templateSvc := adapters.NewTemplateService(&natsConn, &stanConn)
	providerSvc := adapters.NewProviderService(&natsConn, &stanConn)
	workspaceSvc := adapters.NewWorkspaceService(&natsConn, &stanConn)
	deploymentSvc := adapters.NewDeploymentService(&natsConn, &stanConn)
	providerOpenStackSvc := adapters.NewProviderOpenStackService(&natsConn, &stanConn)

	ctx, cancelFunc := context.WithCancel(context.Background())
	common.CancelWhenSignaled(cancelFunc)
	defer cancelFunc()

	svc := domain.NewService(stan, credSvc, templateSvc, providerSvc, providerOpenStackSvc, workspaceSvc, deploymentSvc)
	err = svc.Start(ctx)
	if err != nil {
		log.WithError(err).Panic("fail to start service")
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error("fail to close during cleanup")
		return
	}
}
