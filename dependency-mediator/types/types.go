package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// Config is config for the service, this can be imported from env var using envconfig.Process()
type Config struct {
	LogLevel  string `envconfig:"LOG_LEVEL" default:"debug"`
	MsgConfig messaging2.NatsStanMsgConfig
}

// Override ...
func (c *Config) Override() {
	c.MsgConfig.QueueGroup = "dependency-mediator"
	c.MsgConfig.DurableName = "dependency-mediator"
}

// Session is user who perform a certain actions.
type Session struct {
	Actor    string
	Emulator string
	TID      common.TransactionID
}

// ToServiceActor converts to service.Actor
func (s Session) ToServiceActor() service.Actor {
	return service.Actor{
		Actor:    s.Actor,
		Emulator: s.Emulator,
	}
}

// DependentType is type of dependent
type DependentType int

// String returns the string representation
func (t DependentType) String() string {
	switch t {
	case DeploymentDependent:
		return "deployment"
	case TemplateDependent:
		return "template"
	case ProviderDependent:
		return "provider"
	case WorkspaceDependent:
		return "workspace"
	case CredentialDependent:
		return "credential"
	default:
		return "unknown"
	}
}

// constants for dependent types
const (
	_ DependentType = iota
	DeploymentDependent
	TemplateDependent
	ProviderDependent
	WorkspaceDependent
	CredentialDependent
)

// DependencyResult is result of checking dependencies of a type of objects (DependentType) on another object.
type DependencyResult struct {
	dependentType DependentType
	Depended      bool
	// a list of identifier for the dependents, use interface to take account of different type of identifiers
	Dependents []interface{}
	// whether error occurred during the check
	Err error
}

// Type ...
func (result DependencyResult) Type() DependentType {
	return result.dependentType
}

// Errored return true if there is error
func (result DependencyResult) Errored() bool {
	return result.Err != nil
}

// ErrWhenCheckingDependency construct a result with an error
func ErrWhenCheckingDependency(dependentType DependentType, err error) DependencyResult {
	return DependencyResult{
		dependentType: dependentType,
		Err:           err,
	}
}

// Depended construct a result that indicates there is/are dependent(s) of given type.
// In other words, there is at least 1 object of given type depends on the object being checked.
// Note: this does not specify the dependent object(s).
func Depended(dependentType DependentType) DependencyResult {
	return DependencyResult{
		dependentType: dependentType,
		Depended:      true,
	}
}

// NoneDepended construct a result that indicates there is no dependents of given type.
// In other words, no object of given type depends on the object being checked.
func NoneDepended(dependentType DependentType) DependencyResult {
	return DependencyResult{
		dependentType: dependentType,
		Depended:      false,
	}
}

// DependentsList construct a result that indicates there is/are dependent(s) of given type, and also specified the list of dependents.
func DependentsList(dependentType DependentType, list []interface{}) DependencyResult {
	return DependencyResult{
		dependentType: dependentType,
		Depended:      true,
		Dependents:    list,
	}
}

// DeletionRequest is event body for deletion request with dependent checking.
// Note: the type of object to be deleted is not specified by fields in this struct, but by the event type.
type DeletionRequest = service.DepCheckDeletionRequest

// DeletionResponse is event body for response to deletion request with dependent checking.
// Note: the type of object to be deleted is not specified by fields in this struct, but by the event type.
type DeletionResponse = service.DepCheckDeletionResponse

// DeletionEventHandler ...
type DeletionEventHandler func(request DeletionRequest, tid common.TransactionID) DeletionResponse
