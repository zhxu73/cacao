# Dependency Mediator Service

This service mediate the dependency relationship between different objects.
e.g. A deployment object depends on a template, so the template should not be deleted unless all deployments using it are deleted.

Currently, this service only mediate the delete operation on objects, not the update operation.
