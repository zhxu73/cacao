package domain

import (
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"time"
)

// wait for a certain number of result, return early (with error) if a result has error or declare dependency.
func waitDependencyResults(resultChan chan types.DependencyResult, numResult int) (finalResult types.DependencyResult, timedout bool) {
	timeout := time.After(time.Second * 10)
	for i := 0; i < numResult; i++ {
		select {
		case result := <-resultChan:
			if result.Errored() {
				return result, false
			}
			if result.Depended {
				//return true, fmt.Errorf("There is %s depend on the entity, deletion failed", result.Type().String())
				return result, false
			}
		case <-timeout:
			return types.DependencyResult{}, true
		}
	}
	// no dependency
	return types.NoneDepended(0), false
}
