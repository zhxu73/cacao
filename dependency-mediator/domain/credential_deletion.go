package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"strings"
)

// CredentialDeletionHandler handles credential deletion.
// Credential is depdended by deployment.
// Note that even though credential is used to import/sync template, it is specified every time it is used (import/sync),
// so there isn't a reference to a credential that needs to be maintained. Therefore, we don't need to check for template
// when deleting credential.
type CredentialDeletionHandler struct {
	credSvc              ports.CredentialService
	templateSvc          ports.TemplateService
	deploymentSvc        ports.DeploymentService
	openstackProviderSvc ports.ProviderOpenStackService
}

// Handle ...
func (h CredentialDeletionHandler) Handle(ctx context.Context, req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
	return h.handle(
		ctx,
		types.Session{
			Actor:    req.SessionActor,
			Emulator: req.SessionEmulator,
			TID:      tid,
		},
		req.ID,
	)
}

func (h CredentialDeletionHandler) handle(ctx context.Context, session types.Session, credID string) types.DeletionResponse {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "CredentialDeletionHandler.handle",
	})
	// check credential exists and accessible
	cred, err := h.credSvc.Get(ctx, session, credID)
	if err != nil {
		return errResponse(session, credID, err)
	}

	result, timeout := h.checkDependent(ctx, session, credID)
	if result.Errored() {
		return errResponse(session, credID, result.Err)
	}
	if timeout {
		return timeoutResponse(session, credID)
	}
	if result.Depended {
		return hasDependentResponse(session, credID)
	}

	err = h.preDeletionCleanup(ctx, session, cred)
	if err != nil {
		logger.WithError(err).Error("fail to perform pre-deletion cleanup for credential")
		return errResponse(session, credID, err)
	}

	err = h.credSvc.Delete(ctx, session, credID)
	if err != nil {
		return errResponse(session, credID, err)
	}

	return deletedResponse(session, credID)
}

func (h CredentialDeletionHandler) checkDependent(ctx context.Context, actor types.Session, credID string) (result types.DependencyResult, timeout bool) {
	const numResults = 1
	resultChan := make(chan types.DependencyResult, numResults)

	go func() { resultChan <- h.checkDeployment(ctx, actor, credID) }()

	return waitDependencyResults(resultChan, numResults)
}

func (h CredentialDeletionHandler) checkDeployment(ctx context.Context, actor types.Session, credID string) types.DependencyResult {
	depended, err := h.deploymentSvc.CheckDependOnCredential(ctx, actor, credID)
	if err != nil {
		return types.ErrWhenCheckingDependency(types.DeploymentDependent, err)
	}
	if depended {
		return types.Depended(types.DeploymentDependent)
	}
	return types.NoneDepended(types.DeploymentDependent)
}

func (h CredentialDeletionHandler) preDeletionCleanup(ctx context.Context, actor types.Session, credential *service.CredentialModel) error {
	switch credential.Type {
	case "openstack":
		return h.cleanupOpenStackCredential(ctx, actor, credential)
	default:
		return nil
	}
}

func (h CredentialDeletionHandler) cleanupOpenStackCredential(ctx context.Context, actor types.Session, credential *service.CredentialModel) error {
	providerID, err := providerIDFromTags(credential)
	if err != nil {
		return err
	}
	err = h.openstackProviderSvc.DeleteApplicationCredential(ctx, actor, providerID, credential.ID)
	if err != nil {
		return err
	}
	return nil
}

func providerIDFromTags(credential *service.CredentialModel) (common.ID, error) {
	// cacao_provider takes priority over provider-xxxxxxxxxxxxxxxxxxxx
	for key, value := range credential.Tags {
		if key == "cacao_provider" {
			id := common.ID(value)
			if id.Validate() {
				return id, nil
			}
		}
	}
	for key := range credential.Tags {
		if strings.HasPrefix(key, "provider-") {
			id := common.ID(key)
			if id.Validate() {
				return id, nil
			}
		}
	}
	return "", service.NewCacaoGeneralError("credential is not associate with any provider ID")
}
