package domain

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/dependency-mediator/ports/mocks"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"testing"
)

func TestTemplateDeletionHandler_Handle(t *testing.T) {
	type fields struct {
		templateService   *portsmocks.TemplateService
		deploymentService *portsmocks.DeploymentService
		providerService   *portsmocks.ProviderService
	}
	type args struct {
		req types.DeletionRequest
		tid common.TransactionID
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   types.DeletionResponse
	}{
		{
			name: "no dependent",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(
						&service.TemplateModel{
							ID: "template-cnm8f0598850n9abikj0",
						}, nil)
					svc.On("Delete",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(nil)
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil)
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.CacaoErrorBase{},
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      true,
				HasDependent: false,
			},
		},
		{
			name: "has deployment dependent",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(
						&service.TemplateModel{
							ID: "template-cnm8f0598850n9abikj0",
						}, nil)
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(true, nil)
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil).Maybe()
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.CacaoErrorBase{},
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      false,
				HasDependent: true,
			},
		},
		{
			name: "has provider dependent",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(
						&service.TemplateModel{
							ID: "template-cnm8f0598850n9abikj0",
						}, nil)
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil).Maybe()
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(true, nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.CacaoErrorBase{},
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      false,
				HasDependent: true,
			},
		},
		{
			name: "fail to fetch template",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(nil, fmt.Errorf("failed"))
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      false,
				HasDependent: false,
			},
		},
		{
			name: "fail to check deployment dependent",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(
						&service.TemplateModel{
							ID: "template-cnm8f0598850n9abikj0",
						}, nil)
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, fmt.Errorf("failed"))
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil).Maybe()
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      false,
				HasDependent: false,
			},
		},
		{
			name: "fail to check provider dependent",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(
						&service.TemplateModel{
							ID: "template-cnm8f0598850n9abikj0",
						}, nil)
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil).Maybe()
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, fmt.Errorf("failed"))
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      false,
				HasDependent: false,
			},
		},
		{
			name: "fail to delete template",
			fields: fields{
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("Get",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(
						&service.TemplateModel{
							ID: "template-cnm8f0598850n9abikj0",
						}, nil)
					svc.On("Delete",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(fmt.Errorf("failed"))
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil)
					return svc
				}(),
				providerService: func() *portsmocks.ProviderService {
					svc := &portsmocks.ProviderService{}
					svc.On("CheckDependOnTemplate",
						context.TODO(),
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						}, common.ID("template-cnm8f0598850n9abikj0")).Return(false, nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "template-cnm8f0598850n9abikj0",
				Deleted:      false,
				HasDependent: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			templateService := tt.fields.templateService
			deploymentService := tt.fields.deploymentService
			providerService := tt.fields.providerService
			h := TemplateDeletionHandler{
				templateSvc:   templateService,
				deploymentSvc: deploymentService,
				providerSvc:   providerService,
			}
			if got := h.Handle(context.TODO(), tt.args.req, tt.args.tid); !assert.Equal(t, tt.want, got) {
				return
			}

			templateService.AssertExpectations(t)
			deploymentService.AssertExpectations(t)
			providerService.AssertExpectations(t)
		})
	}
}
