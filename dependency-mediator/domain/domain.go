package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"sync"
	"time"
)

// Domain entry point object of the service
type Domain struct {
	eventSource ports.IncomingEventSource

	credentialSvc        ports.CredentialService
	templateSvc          ports.TemplateService
	providerSvc          ports.ProviderService
	openstackProviderSvc ports.ProviderOpenStackService
	workspaceSvc         ports.WorkspaceService
	deploymentSvc        ports.DeploymentService

	credHandler      *CredentialDeletionHandler
	templateHandler  *TemplateDeletionHandler
	workspaceHandler *WorkspaceDeletionHandler
}

// NewService ...
func NewService(
	eventSource ports.IncomingEventSource,
	credentialSvc ports.CredentialService,
	templateSvc ports.TemplateService,
	providerSvc ports.ProviderService,
	openstackProviderSvc ports.ProviderOpenStackService,
	workspaceSvc ports.WorkspaceService,
	deploymentSvc ports.DeploymentService,
) Domain {
	return Domain{
		eventSource:          eventSource,
		credentialSvc:        credentialSvc,
		templateSvc:          templateSvc,
		providerSvc:          providerSvc,
		openstackProviderSvc: openstackProviderSvc,
		workspaceSvc:         workspaceSvc,
		deploymentSvc:        deploymentSvc,
	}
}

// Start starts the service
func (svc Domain) Start(ctx context.Context) error {

	svc.credHandler = &CredentialDeletionHandler{
		credSvc:              svc.credentialSvc,
		templateSvc:          svc.templateSvc,
		deploymentSvc:        svc.deploymentSvc,
		openstackProviderSvc: svc.openstackProviderSvc,
	}
	svc.templateHandler = &TemplateDeletionHandler{
		templateSvc:   svc.templateSvc,
		deploymentSvc: svc.deploymentSvc,
		providerSvc:   svc.providerSvc,
	}
	svc.workspaceHandler = &WorkspaceDeletionHandler{
		workspaceSvc:  svc.workspaceSvc,
		deploymentSvc: svc.deploymentSvc,
	}

	var wg sync.WaitGroup
	err := svc.eventSource.Start(ctx, svc, &wg)
	if err != nil {
		return err
	}
	log.Infof("event source started")
	<-ctx.Done()
	wg.Done()
	return nil
}

// DeleteCredential implements ports.EventHandlers
func (svc Domain) DeleteCredential(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
	// this might need to do additional cleanup, so longer timeout
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*15)
	defer cancelFunc()
	return svc.credHandler.Handle(ctx, req, tid)
}

// DeleteTemplate implements ports.EventHandlers
func (svc Domain) DeleteTemplate(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
	ctx, cancelFunc := svc.handlerContext()
	defer cancelFunc()
	return svc.templateHandler.Handle(ctx, req, tid)
}

// DeleteWorkspace implements ports.EventHandlers
func (svc Domain) DeleteWorkspace(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {

	ctx, cancelFunc := svc.handlerContext()
	defer cancelFunc()
	return svc.workspaceHandler.Handle(ctx, req, tid)
}

func (svc Domain) handlerContext() (context.Context, context.CancelFunc) {
	// use Background as parent context, this is because we want handler to have
	// separate context than the main App Context so that, they don't get cancelled
	// at the same time.
	return context.WithTimeout(context.Background(), time.Second*5)
}
