package domain

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// TemplateDeletionHandler handles template deletion.
// template is depended by deployment or provider (prerequisite template in metadata).
type TemplateDeletionHandler struct {
	templateSvc   ports.TemplateService
	deploymentSvc ports.DeploymentService
	providerSvc   ports.ProviderService
}

// Handle ...
func (h TemplateDeletionHandler) Handle(ctx context.Context, req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
	return h.handle(
		ctx,
		types.Session{
			Actor:    req.GetSessionActor(),
			Emulator: req.GetSessionEmulator(),
			TID:      tid,
		},
		common.ID(req.ID),
	)
}

func (h TemplateDeletionHandler) handle(ctx context.Context, session types.Session, templateID common.ID) types.DeletionResponse {
	// check template exists and accessible
	_, err := h.templateSvc.Get(ctx, session, templateID)
	if err != nil {
		return h.errResponse(session, templateID, err)
	}
	result, timeout := h.checkDependent(ctx, session, templateID)
	if result.Errored() {
		return h.errResponse(session, templateID, result.Err)
	}
	if timeout {
		return h.timeoutResponse(session, templateID)
	}
	if result.Depended {
		return h.hasDependentResponse(session, templateID)
	}

	err = h.templateSvc.Delete(ctx, session, templateID)
	if err != nil {
		return h.errResponse(session, templateID, err)
	}
	return h.deletedResponse(session, templateID)
}

func (h TemplateDeletionHandler) checkDependent(ctx context.Context, session types.Session, template common.ID) (result types.DependencyResult, timeout bool) {
	const numResults = 2
	resultChan := make(chan types.DependencyResult, numResults)

	// perform 2 checks concurrently
	go func() { resultChan <- h.checkProvider(ctx, session, template) }()
	go func() { resultChan <- h.checkDeployment(ctx, session, template) }()

	return waitDependencyResults(resultChan, numResults)
}

func (h TemplateDeletionHandler) checkProvider(ctx context.Context, session types.Session, template common.ID) types.DependencyResult {
	depended, err := h.providerSvc.CheckDependOnTemplate(ctx, session, template)
	if err != nil {
		return types.ErrWhenCheckingDependency(types.ProviderDependent, err)
	}
	if depended {
		return types.Depended(types.ProviderDependent)
	}
	return types.NoneDepended(types.ProviderDependent)
}

func (h TemplateDeletionHandler) checkDeployment(ctx context.Context, session types.Session, template common.ID) types.DependencyResult {
	depended, err := h.deploymentSvc.CheckDependOnTemplate(ctx, session, template)
	if err != nil {
		return types.ErrWhenCheckingDependency(types.DeploymentDependent, err)
	}
	if depended {
		return types.Depended(types.DeploymentDependent)
	}
	return types.NoneDepended(types.DeploymentDependent)
}

func (h TemplateDeletionHandler) errResponse(session types.Session, template common.ID, err error) types.DeletionResponse {
	return errResponse(session, template.String(), err)
}

func (h TemplateDeletionHandler) timeoutResponse(session types.Session, template common.ID) types.DeletionResponse {
	return timeoutResponse(session, template.String())
}

func (h TemplateDeletionHandler) hasDependentResponse(session types.Session, template common.ID) types.DeletionResponse {
	return hasDependentResponse(session, template.String())
}

func (h TemplateDeletionHandler) deletedResponse(session types.Session, template common.ID) types.DeletionResponse {
	return deletedResponse(session, template.String())
}
