package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"sync"
)

// StanAdapter ...
type StanAdapter struct {
	conn messaging2.EventConnection
}

// NewStanAdapter ...
func NewStanAdapter(conn messaging2.EventConnection) *StanAdapter {
	return &StanAdapter{
		conn: conn,
	}
}

// Start ...
func (s *StanAdapter) Start(ctx context.Context, handlers ports.EventHandlers, wg *sync.WaitGroup) error {
	var handlerMap = map[common.EventType]messaging2.EventHandlerFunc{
		service.EventDepMedDeleteCredential: deletionHandlerWrapper(handlers.DeleteCredential),
		service.EventDepMedDeleteTemplate:   deletionHandlerWrapper(handlers.DeleteTemplate),
		service.EventDepMedDeleteWorkspace:  deletionHandlerWrapper(handlers.DeleteWorkspace),
	}
	err := s.conn.Listen(ctx, handlerMap, wg)
	if err != nil {
		return err
	}
	return nil
}

// wrap deletion handler into a cloudevent handler
func deletionHandlerWrapper(handler types.DeletionEventHandler) messaging2.EventHandlerFunc {
	return func(ctx context.Context, requestCe cloudevents.Event, writer messaging2.EventResponseWriter) error {
		logger := log.WithFields(log.Fields{
			"package":  "adapters",
			"function": "StanAdapter.deletionHandlerWrapper",
		})

		var request types.DeletionRequest
		err := json.Unmarshal(requestCe.Data(), &request)
		if err != nil {
			return err
		}
		logger = logger.WithFields(log.Fields{
			"requestType": requestCe.Type(),
			"objID":       request.ID,
		})
		logger.Info("request received")

		tid := messaging2.GetTransactionID(&requestCe)
		response := handler(request, tid)

		responseCe, err := createDeletionCloudEventResponse(&requestCe, response)
		if err != nil {
			logger.WithError(err).Error("fail to create cloudevent for deletion response")
			return nil
		}
		logger = logger.WithField("responseType", responseCe.Type())
		err = writer.Write(responseCe)
		if err != nil {
			logger.WithError(err).Error("fail to publish deletion response")
			return nil
		}
		logger.Info("response published")
		return nil
	}
}

func createDeletionCloudEventResponse(requestCe *cloudevents.Event, response types.DeletionResponse) (cloudevents.Event, error) {
	respEventType := getResponseEventType(common.EventType(requestCe.Type()), response)
	responseCe, err := messaging2.CreateCloudEventWithTransactionID(response, respEventType, "dependency-mediator", messaging2.GetTransactionID(requestCe))
	if err != nil {
		return cloudevents.Event{}, err
	}
	return responseCe, nil
}

func getResponseEventType(reqEventType common.EventType, response types.DeletionResponse) common.EventType {
	if response.Errored() {
		return responseEventTypeMapping[reqEventType][responseDeleteErrored]
	}
	if response.Deleted {
		return responseEventTypeMapping[reqEventType][responseDeleted]
	}
	if response.HasDependent {
		return responseEventTypeMapping[reqEventType][responseHasDependent]
	}
	return ""
}

type responseType int

const (
	responseDeleted       responseType = 0
	responseDeleteErrored responseType = 1
	responseHasDependent  responseType = 2
)

var responseEventTypeMapping = map[common.EventType]map[responseType]common.EventType{
	service.EventDepMedDeleteProvider: {
		responseDeleted:       service.EventDepMedProviderDeleted,
		responseDeleteErrored: service.EventDepMedProviderDeletionErrored,
		responseHasDependent:  service.EventDepMedProviderHasDependent,
	},
	service.EventDepMedDeleteTemplate: {
		responseDeleted:       service.EventDepMedTemplateDeleted,
		responseDeleteErrored: service.EventDepMedTemplateDeletionErrored,
		responseHasDependent:  service.EventDepMedTemplateHasDependent,
	},
	service.EventDepMedDeleteWorkspace: {
		responseDeleted:       service.EventDepMedWorkspaceDeleted,
		responseDeleteErrored: service.EventDepMedWorkspaceDeletionErrored,
		responseHasDependent:  service.EventDepMedWorkspaceHasDependent,
	},
	service.EventDepMedDeleteCredential: {
		responseDeleted:       service.EventDepMedCredentialDeleted,
		responseDeleteErrored: service.EventDepMedCredentialDeletionErrored,
		responseHasDependent:  service.EventDepMedCredentialHasDependent,
	},
}
