package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// DeploymentSvcClient is redefinition of service.DeploymentClient, it is to allow mocks to be generated locally in this repo.
type DeploymentSvcClient interface {
	service.DeploymentClient
}

type deploymentSvcClientCreator func() service.DeploymentClient

// DeploymentService implements ports.DeploymentMS
type DeploymentService struct {
	svcClientCreator deploymentSvcClientCreator
}

// NewDeploymentService ...
func NewDeploymentService(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) DeploymentService {
	client, err := service.NewDeploymentClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return DeploymentService{
		svcClientCreator: func() service.DeploymentClient {
			return client
		},
	}
}

// Get fetches deployment
func (d DeploymentService) Get(ctx context.Context, session types.Session, id common.ID) (*service.Deployment, error) {
	return d.svcClientCreator().Get(ctx, session.ToServiceActor(), id)
}

// CheckDependOnCredential checks if there is any deployment depends on the specified credential.
func (d DeploymentService) CheckDependOnCredential(ctx context.Context, session types.Session, credID string) (bool, error) {
	result, err := d.svcClientCreator().Search(ctx, session.ToServiceActor(), service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			CredentialID: credID,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// CheckDependOnTemplate checks if there is any deployment depends on the specified template.
func (d DeploymentService) CheckDependOnTemplate(ctx context.Context, session types.Session, template common.ID) (bool, error) {
	result, err := d.svcClientCreator().Search(ctx, session.ToServiceActor(), service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			Template: template,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// CheckDependOnProvider checks if there is any deployment depends on the specified provider
func (d DeploymentService) CheckDependOnProvider(ctx context.Context, session types.Session, provider common.ID) (bool, error) {
	result, err := d.svcClientCreator().Search(ctx, session.ToServiceActor(), service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			PrimaryCloudProvider: provider,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// CheckDependOnWorkspace checks if there is any deployment depends on the specified workspace.
func (d DeploymentService) CheckDependOnWorkspace(ctx context.Context, session types.Session, workspace common.ID) (bool, error) {
	result, err := d.svcClientCreator().Search(ctx, session.ToServiceActor(), service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			Workspace: workspace,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// Delete deletes the deployment. Note that deployment deletion is async, so this will simply starts the deletion process.
func (d DeploymentService) Delete(ctx context.Context, session types.Session, id common.ID) error {
	deleted, err := d.svcClientCreator().Delete(ctx, session.ToServiceActor(), session.TID, id)
	if err != nil {
		return err
	}
	log.WithField("deleted", deleted).Info()
	return nil
}
