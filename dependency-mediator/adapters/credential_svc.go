package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// CredentialService implements ports.CredentialService
type CredentialService struct {
	credentialSvcClientCreator credentialSvcClientCreator
}

// NewCredentialService ...
func NewCredentialService(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) ports.CredentialService {
	client, err := service.NewNatsCredentialClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return CredentialService{
		credentialSvcClientCreator: func() service.CredentialClient {
			return client
		},
	}
}

// Get fetches a credential
func (c CredentialService) Get(ctx context.Context, session types.Session, id string) (*service.CredentialModel, error) {
	client := c.credentialSvcClientCreator()
	cred, err := client.Get(ctx, session.ToServiceActor(), id)
	if err != nil {
		return nil, err
	}
	return cred, nil
}

// Delete deletes a credential
func (c CredentialService) Delete(ctx context.Context, session types.Session, id string) error {
	client := c.credentialSvcClientCreator()
	err := client.Delete(ctx, session.ToServiceActor(), id)
	return err
}

// CredentialSvcClient is redefinition of service.CredentialClient, it is to allow mocks to be generated locally in this repo.
type CredentialSvcClient interface {
	service.CredentialClient
}

type credentialSvcClientCreator func() service.CredentialClient
