package adapters

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"testing"
)

// test if the wrapper return the correct response event type
func Test_deletionHandlerWrapper(t *testing.T) {
	type args struct {
		conn    MockCloudEventPublisher
		handler types.DeletionEventHandler
	}
	tests := []struct {
		name                   string
		args                   args
		requestCloudEventType  common.EventType
		expectedCloudEventType common.EventType
	}{
		{
			name: "credential deleted",
			args: args{
				conn: MockCloudEventPublisher{
					returnErr: nil,
				},
				handler: func(request types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
					return types.DeletionResponse{
						Session:      service.Session{},
						ID:           "",
						Deleted:      true,
						HasDependent: false,
					}
				},
			},
			requestCloudEventType:  service.EventDepMedDeleteCredential,
			expectedCloudEventType: service.EventDepMedCredentialDeleted,
		},
		{
			name: "credential delete failed",
			args: args{
				conn: MockCloudEventPublisher{
					returnErr: nil,
				},
				handler: func(request types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
					return types.DeletionResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
						},
						ID:           "",
						Deleted:      false,
						HasDependent: false,
					}
				},
			},
			requestCloudEventType:  service.EventDepMedDeleteCredential,
			expectedCloudEventType: service.EventDepMedCredentialDeletionErrored,
		},
		{
			name: "credential has dependent",
			args: args{
				conn: MockCloudEventPublisher{
					returnErr: nil,
				},
				handler: func(request types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
					return types.DeletionResponse{
						Session:      service.Session{},
						ID:           "",
						Deleted:      false,
						HasDependent: true,
					}
				},
			},
			requestCloudEventType:  service.EventDepMedDeleteCredential,
			expectedCloudEventType: service.EventDepMedCredentialHasDependent,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			requestCe, err := messaging2.CreateCloudEvent(nil, tt.requestCloudEventType, "foobar")
			if err != nil {
				panic(err)
			}

			wrapper := deletionHandlerWrapper(tt.args.handler)
			err = wrapper(context.TODO(), requestCe, &tt.args.conn)

			assert.NoError(t, err)
			if !assert.NotNil(t, tt.args.conn.receivedCe) {
				return
			}
			assert.Equal(t, string(tt.expectedCloudEventType), tt.args.conn.receivedCe.Type())
		})
	}
}

type MockCloudEventPublisher struct {
	returnErr  error
	receivedCe *cloudevents.Event
}

func (m *MockCloudEventPublisher) Write(ce cloudevents.Event) error {
	m.receivedCe = &ce
	return m.returnErr
}

func (m *MockCloudEventPublisher) NoResponse() {
}
