package adapters

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/adapters/mocks"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"reflect"
	"testing"
)

func TestDeploymentService_Get(t *testing.T) {
	type fields struct {
		svcClientCreator func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator
	}
	type args struct {
		session types.Session
		id      common.ID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *service.Deployment
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator {
					return func() service.DeploymentClient {
						assert.Equal(t, "actor-123", session.Actor)
						assert.Equal(t, "emulator-123", session.Emulator)
						svcClient.On("Get", context.TODO(), session.ToServiceActor(), common.ID("deployment-d600t0598850n9abikm0")).Return(&service.Deployment{
							ID:   "deployment-d600t0598850n9abikm0",
							Name: "foobar",
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				id: common.ID("deployment-d600t0598850n9abikm0"),
			},
			want: &service.Deployment{
				ID:   "deployment-d600t0598850n9abikm0",
				Name: "foobar",
			},
			wantErr: false,
		},
		{
			name: "errored",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator {
					return func() service.DeploymentClient {
						assert.Equal(t, "actor-123", session.Actor)
						assert.Equal(t, "emulator-123", session.Emulator)
						svcClient.On("Get", context.TODO(), session.ToServiceActor(), common.ID("deployment-d600t0598850n9abikm0")).Return(nil, fmt.Errorf("failed"))
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				id: common.ID("deployment-d600t0598850n9abikm0"),
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockSvcClient := &mocks.DeploymentSvcClient{}
			d := DeploymentService{
				svcClientCreator: tt.fields.svcClientCreator(t, mockSvcClient, tt.args.session),
			}
			got, err := d.Get(context.TODO(), tt.args.session, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
			mockSvcClient.AssertExpectations(t)
		})
	}
}

func TestDeploymentService_CheckDependOnCredential(t *testing.T) {
	type fields struct {
		svcClientCreator func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator
	}
	type args struct {
		session types.Session
		credID  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "success - 1 dependent",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator {
					return func() service.DeploymentClient {
						assert.Equal(t, "actor-123", session.Actor)
						assert.Equal(t, "emulator-123", session.Emulator)
						svcClient.On("Search",
							context.TODO(),
							session.ToServiceActor(),
							service.DeploymentListOption{
								Filter: service.DeploymentFilter{
									CredentialID: "cred-id-123",
								},
							}).Return(mockDeploymentList{
							{
								ID: "deployment-d600t0598850n9abikm0",
								CloudCredentials: map[string]common.ID{
									"cred-id-123": "provider-d17e30598850n9abikl0",
								},
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				credID: "cred-id-123",
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "success - 0 dependent",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator {
					return func() service.DeploymentClient {
						assert.Equal(t, "actor-123", session.Actor)
						assert.Equal(t, "emulator-123", session.Emulator)
						svcClient.On("Search",
							context.TODO(),
							session.ToServiceActor(),
							service.DeploymentListOption{
								Filter: service.DeploymentFilter{
									CredentialID: "cred-id-123",
								},
							}).Return(mockDeploymentList{}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				credID: "cred-id-123",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "errored",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient, session types.Session) deploymentSvcClientCreator {
					return func() service.DeploymentClient {
						assert.Equal(t, "actor-123", session.Actor)
						assert.Equal(t, "emulator-123", session.Emulator)
						svcClient.On("Search", context.TODO(), session.ToServiceActor(), service.DeploymentListOption{
							Filter: service.DeploymentFilter{
								CredentialID: "cred-id-123",
							},
						}).Return(nil, fmt.Errorf("failed"))
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				credID: "cred-id-123",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockSvcClient := &mocks.DeploymentSvcClient{}
			d := DeploymentService{
				svcClientCreator: tt.fields.svcClientCreator(t, mockSvcClient, tt.args.session),
			}
			got, err := d.CheckDependOnCredential(context.TODO(), tt.args.session, tt.args.credID)
			if (err != nil) != tt.wantErr {
				t.Errorf("CheckDependOnCredential() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equalf(t, tt.want, got, "CheckDependOnCredential(%v, %v)", tt.args.session, tt.args.credID)
			mockSvcClient.AssertExpectations(t)
		})
	}
}

type mockDeploymentList []service.Deployment

func (m mockDeploymentList) GetOptions() service.DeploymentListOption {
	panic("not implemented")
}

func (m mockDeploymentList) GetDeployments() []service.Deployment {
	return []service.Deployment(m)
}

func (m mockDeploymentList) GetStart() int {
	panic("not implemented")
}

func (m mockDeploymentList) GetSize() int {
	return len(m)
}

func (m mockDeploymentList) GetNextStart() int {
	panic("not implemented")
}
