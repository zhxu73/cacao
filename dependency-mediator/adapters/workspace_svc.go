package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type workspaceSvcClientCreator func() service.WorkspaceClient

// WorkspaceService implements ports.ProviderMS
type WorkspaceService struct {
	svcClientCreator workspaceSvcClientCreator
}

// NewWorkspaceService ...
func NewWorkspaceService(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) ports.WorkspaceService {
	client, err := service.NewNatsWorkspaceClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return WorkspaceService{
		svcClientCreator: func() service.WorkspaceClient {
			return client
		},
	}
}

// Get fetches provider
func (w WorkspaceService) Get(ctx context.Context, session types.Session, id common.ID) (*service.WorkspaceModel, error) {
	svcClient := w.svcClientCreator()
	return svcClient.Get(ctx, session.ToServiceActor(), id)
}

// Delete deletes the workspace
func (w WorkspaceService) Delete(ctx context.Context, session types.Session, workspace common.ID) error {
	svcClient := w.svcClientCreator()
	return svcClient.Delete(ctx, session.ToServiceActor(), workspace)
}
