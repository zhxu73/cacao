package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/adapters/mocks"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"testing"
	"time"
)

func TestProviderService_CheckDependOnTemplate(t *testing.T) {

	type fields struct {
		svcClientCreator1 func(session types.Session) providerSvcClientCreator
	}
	type args struct {
		session  types.Session
		template common.ID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "no provider",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session:  types.Session{Actor: "actor-123", Emulator: "emulator-123", TID: "tid-aaaaaaaaaaaaaaaaaaaa"},
				template: "",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, has dependent",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						metadataStr := `{
									    "prerequisite_template": {
									        "template_id": "template-cnm8f0598850n9abikj0"
									    }
									}`
						var metadata map[string]interface{}
						_ = json.Unmarshal([]byte(metadataStr), &metadata)
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{
							{
								ID:        "provider-d17e30598850n9abikl0",
								Name:      "",
								Type:      "openstack",
								URL:       "",
								CreatedAt: time.Time{},
								UpdatedAt: time.Time{},
								Metadata:  metadata,
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "one openstack provider, no dependent",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						metadataStr := `{
									    "prerequisite_template": {
									        "template_id": "template-bbbbbbbbbbbbbbbbbbbb"
									    }
									}`
						var metadata map[string]interface{}
						_ = json.Unmarshal([]byte(metadataStr), &metadata)
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{
							{
								ID:        "provider-d17e30598850n9abikl0",
								Name:      "",
								Type:      "openstack",
								URL:       "",
								CreatedAt: time.Time{},
								UpdatedAt: time.Time{},
								Metadata:  metadata,
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, no prerequisite template",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						metadataStr := `{
										"foo": "bar"
									}`
						var metadata map[string]interface{}
						_ = json.Unmarshal([]byte(metadataStr), &metadata)
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{
							{
								ID:        "provider-d17e30598850n9abikl0",
								Name:      "",
								Type:      "openstack",
								URL:       "",
								CreatedAt: time.Time{},
								UpdatedAt: time.Time{},
								Metadata:  metadata,
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, empty prerequisite template",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						metadataStr := `{
									    "prerequisite_template": {
									        "template_id": ""
									    }
									}`
						var metadata map[string]interface{}
						_ = json.Unmarshal([]byte(metadataStr), &metadata)
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{
							{
								ID:        "provider-d17e30598850n9abikl0",
								Name:      "",
								Type:      "openstack",
								URL:       "",
								CreatedAt: time.Time{},
								UpdatedAt: time.Time{},
								Metadata:  metadata,
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, no metadata",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{
							{
								ID:        "provider-d17e30598850n9abikl0",
								Name:      "",
								Type:      "openstack",
								URL:       "",
								CreatedAt: time.Time{},
								UpdatedAt: time.Time{},
								Metadata:  nil,
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "non-openstack provider",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return([]service.ProviderModel{
							{
								ID:        "provider-d17e30598850n9abikl0",
								Name:      "",
								Type:      "not-openstack",
								URL:       "",
								CreatedAt: time.Time{},
								UpdatedAt: time.Time{},
								Metadata:  nil,
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session:  types.Session{Actor: "actor-123", Emulator: "emulator-123", TID: "tid-aaaaaaaaaaaaaaaaaaaa"},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "list provider failed",
			fields: fields{
				svcClientCreator1: func(session types.Session) providerSvcClientCreator {
					return func() service.ProviderClient {
						svcClient := &mocks.ProviderSvcClient{}
						svcClient.On("List", context.TODO(), session.ToServiceActor()).Return(nil, fmt.Errorf("failed"))
						return svcClient
					}
				},
			},
			args: args{
				session:  types.Session{Actor: "actor-123", Emulator: "emulator-123", TID: "tid-aaaaaaaaaaaaaaaaaaaa"},
				template: "template-cnm8f0598850n9abikj0",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := ProviderService{
				svcClientCreator: tt.fields.svcClientCreator1(tt.args.session),
			}
			got, err := p.CheckDependOnTemplate(context.TODO(), tt.args.session, tt.args.template)
			fmtStr := fmt.Sprintf("CheckDependOnTemplate(%v, %v)", tt.args.session, tt.args.template)
			if tt.wantErr {
				assert.Errorf(t, err, fmtStr)
			} else {
				assert.NoErrorf(t, err, fmtStr)
			}
			assert.Equalf(t, tt.want, got, fmtStr)
		})
	}
}
