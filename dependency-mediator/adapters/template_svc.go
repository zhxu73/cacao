package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type templateSvcClientCreator func() service.TemplateClient

// TemplateService implements ports.TemplateService
type TemplateService struct {
	svcClientCreator templateSvcClientCreator
}

// NewTemplateService ...
func NewTemplateService(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) ports.TemplateService {
	client, err := service.NewNatsTemplateClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return TemplateService{
		svcClientCreator: func() service.TemplateClient {
			return client
		},
	}
}

// Get fetches template
func (t TemplateService) Get(ctx context.Context, session types.Session, template common.ID) (service.Template, error) {
	svcClient := t.svcClientCreator()
	return svcClient.Get(ctx, session.ToServiceActor(), template)
}

// Delete deletes template
func (t TemplateService) Delete(ctx context.Context, session types.Session, template common.ID) error {
	svcClient := t.svcClientCreator()
	return svcClient.Delete(ctx, session.ToServiceActor(), template)
}
