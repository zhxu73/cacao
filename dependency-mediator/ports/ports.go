package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"sync"
)

// IncomingEventSource is source of incoming events
type IncomingEventSource interface {
	Start(ctx context.Context, handlers EventHandlers, wg *sync.WaitGroup) error
}

// EventHandlers represents the handlers for the event operations that this service supports.
// This is implemented by object in `domain` package.
type EventHandlers interface {
	DeleteCredential(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse
	DeleteTemplate(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse
	DeleteWorkspace(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse
}

// TemplateService provides access to template microservice
type TemplateService interface {
	Get(context.Context, types.Session, common.ID) (service.Template, error)
	Delete(context.Context, types.Session, common.ID) error
}

// ProviderService provides access to provider metadata service
type ProviderService interface {
	Get(context.Context, types.Session, common.ID) (*service.ProviderModel, error)
	CheckDependOnTemplate(ctx context.Context, session types.Session, template common.ID) (bool, error)
	Delete(context.Context, types.Session, common.ID) error
}

// WorkspaceService provides access to workspace microservice
type WorkspaceService interface {
	Get(context.Context, types.Session, common.ID) (*service.WorkspaceModel, error)
	Delete(context.Context, types.Session, common.ID) error
}

// CredentialService provides access to credential microservice
type CredentialService interface {
	Get(ctx context.Context, session types.Session, id string) (*service.CredentialModel, error)
	Delete(ctx context.Context, session types.Session, id string) error
}

// DeploymentService provides access to deployment microservice
type DeploymentService interface {
	Get(context.Context, types.Session, common.ID) (*service.Deployment, error)
	CheckDependOnCredential(ctx context.Context, session types.Session, credID string) (bool, error)
	CheckDependOnTemplate(ctx context.Context, session types.Session, template common.ID) (bool, error)
	CheckDependOnProvider(ctx context.Context, session types.Session, provider common.ID) (bool, error)
	CheckDependOnWorkspace(ctx context.Context, session types.Session, workspace common.ID) (bool, error)
	Delete(context.Context, types.Session, common.ID) error
}

// ProviderOpenStackService provides access to provider openstack service
type ProviderOpenStackService interface {
	DeleteApplicationCredential(ctx context.Context, session types.Session, providerID common.ID, credID string) error
}
