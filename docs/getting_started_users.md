# Getting Started

This document outlines getting started with CACAO as a user. This assumed you have an account and access to a running CACAO server and an OpenStack credential.


### Table of Contents
[[_TOC_]]


### Prepare CACAO CLI
The CACAO CLI is the simplest way to interact with the CACAO API.

#### Download from Gitlab
We have pre-built executable for common platforms that you can just download and use.
https://gitlab.com/cyverse/cacao/-/packages
> Download the most recent one if you don't know which one to use.

Remember to download the one that fits your Operating System (windows, linux, darwin/macOS) and CPU architecture (amd64, arm64).

#### Build from source
These are the instructions to build the CLI from source:
> make sure you have Golang installed before you precedes,
> see Golang's official doc for how to install, https://go.dev/doc/install

1. Clone or download the repository from GitLab
	```bash
	git clone https://gitlab.com/cyverse/cacao.git
	# or download it without git:
	wget https://gitlab.com/cyverse/cacao/-/archive/master/cacao-master.zip
	unzip cacao.zip
	```
2. Build the `cacao` binary:
	```bash
	cd cacao/cmd
	go build -o cacao
	```
3. Move this to somewhere in your `$PATH`, or add this directory to your path:
	```bash
	sudo mv cacao /usr/local/bin/cacao
	# or
	export PATH=$PATH:$(pwd)
	```


### First Steps

In order to use the CACAO CLI, you need to log in:

We recommend you log in using CACAO's API token.
> This requires you to have an CACAO API token ready, which you need to create beforehand, you can create it in
> the credential page in Web UI. (For Jetstream 2, go to https://cacao.jetstream-cloud.org/credentials, click `"ADD CREDENTIAL"` button and select `"API TOKEN"`)
```bash
$ cacao login
```


If you wish to log in with auth token (the token you get from auth provider like Keycloak, ACCESS-CI, Globus), you can use the `--auth-token` to specify the token directly on the command line or use `--browser` flag to guide you through login via browser to get the auth token.
```bash
$ cacao login --auth-token <your-auth-token-goes-here>
# or
$ cacao login --browser
```

If you are a CACAO developer that is targeting a local development version of CACAO, use the `--keycloak` flag to login.


If this is your first time logging in, the CACAO CLI will prompt you for the address of the CACAO API.
```bash
Please provide address of Cacao API.
Format Should be: http://<hostname>:<port>	  or    <hostname>:<port> 
(Developers: this should match the value of API_DOMAIN in install/config.yaml followed by "/api", e.g. http://ca.cyverse.local/api)
Cacao API address (https://cacao.jetstream-cloud.org/api):
```

To access the production deployment of CACAO, enter `https://cacao.jetstream-cloud.org/api` (this is the default value, you can just press `<ENTER>` to use default).  For local development environments, enter `http://ca.cyverse.local/api` (or `localhost` if you run services manually from cmd line).

> Note: If you wish to specify the api address and token non-interactively (say you are using CACAO CLI in a bash script),
> use the `--api-addr` flag for the API address and `--cacao-token` or `--auth-token` for the token.

### User Actions
Now you have everything you need to interact with your user's resources in CACAO, specifically the OpenStack resources.

```bash
cacao get user ${username}

# list all providers
cacao provider get
# get a specific provider
cacao provider get ${providerID}
# create a new provider
cacao provider create ${providerName} ${providerURL} ${providerType} --metadata ${metadataJSONString}
# create a new provider with metadata from file
cacao provider create ${providerName} ${providerURL} ${providerType} --metadata-file ${metadataJSONFilename}
# create a new provider from JSON file
cacao provider create -f provider.json
# update a provider
cacao provider update ${providerID} --name ${providerName} --type ${providerType} --url ${providerURL} --metadata ${metadataJSONString}
# delete a provider
cacao provider delete ${providerID}

# list all templates
cacao template get
# get a specific template
cacao template get ${templateID}
# create/import a new template (git repo)
cacao template create git ${sourceURL} ${templateName} --public --branch ${gitBranch} --path ${subPath}
# create/import a new template with a credential (private git repo)
cacao template create git ${sourceURL} ${templateName} --public --branch ${gitBranch} --path ${subPath} --credential-id ${gitCredentialID}
# create/import a new template from JSON file
cacao template create -f template.json
# sync a template with its source
cacao template sync ${templateID}
# delete a template
cacao template delete ${templateID}

# list all user actions
cacao user_action get
# get a specific user action
cacao user_action get ${userActionID}
# create a new user action from url
cacao user_action create instance_execution script ${sourceURL} --data-type ${scriptDataType} --description ${description} --public ${userActionName}
# create a new user action from a file
cacao user_action create instance_execution script ${sourcePath} --data-type ${scriptDataType} --description ${description} --public ${userActionName} 
# create/import a new user action from JSON file
cacao user_action create -f user_action.json
# update a user action
cacao user_action update ${userActionID} --name ${userActionName} --description ${description} --public ${isPublic} 
# delete a user action
cacao user_action delete ${userActionID}

# list all workspaces
cacao workspace get
# get a specific workspace
cacao workspace get ${workspaceID}
# create a new workspace
cacao workspace create ${workspaceName} --default-provider-id ${defaultProviderID}
# create a new workspace from JSON file
cacao workspace create -f workspace.json
# update a workspace
cacao workspace update ${workspaceID} --name ${workspaceName} --default-provider-id ${defaultProviderID} 
# delete a workspace
cacao workspace delete ${workspaceID}

# list all credentials
cacao credential get
# get a specific credential
cacao credential get ${credentialID}
# create a new credential (example types: openstack, git, dockerhub, ssh, etc.)
cacao credential create ${credentialName} ${type} --value ${secret_string}
# create a new credential from JSON file
cacao credential create -f credential.json
# delete a credential
cacao credential delete ${credentialID}

# list all deployments
cacao deployment get
# get a specific deployment
cacao deployment get ${deploymentID}
# create a new deployment
cacao deployment create ${workspaceID} ${templateID} ${providerID} --cloud-creds ${credentialID}
# create a new deployment
cacao deployment create ${workspaceID} ${templateID} ${providerID} --cloud-creds ${credentialID} --git-cred ${gitCredentialID}
# delete a deployment
cacao deployment delete ${deploymentID}

# list all runs in a deployment
cacao run get ${deploymentID}
# get a specific run
cacao run get ${deploymentID} ${runID}
# create a new run
cacao run create ${deploymentID} --params k1=v1,k2=v2 --cloud-cred ${credentialID}
# create a new run
cacao run create ${deploymentID} --params '{"k1":"v1","k2":"v2"}' --cloud-cred ${credentialID}
# create a new run from template from private git repo
cacao run create ${deploymentID} --params k1=v1,k2=v2 --cloud-cred ${credentialID} --git-cred ${gitCredentialID}
# create a new run from JSON file
cacao run create ${deploymentID} -f run.json
```

### Create a prerequisite template
> Performed by Admin

First import a prerequisite template, use this template as an example https://gitlab.com/cyverse/cacao-tf-os-ops/-/blob/main/prerequisite/metadata.json .
Prerequisite template is a template that will be executed before normal template (hence prerequisite), and it is specific to provider.
```bash
cacao template create git https://gitlab.com/cyverse/cacao-tf-os-ops.git openstack-prerequisite \
	--public \
	--branch main \
	--path prerequisite
```
Save the template ID generated from the output (e.g. `template-xxxxxxxxxxxxxxxxxxxx`)

### Create an OpenStack provider
> Performed by Admin

Create a metadata JSON file, use the one below as an example. Note that you need to populate the fields in the JSON object.

`provider_metadata.json`
```json
{
    "prerequisite_template": {
        "template_id": "<prerequisite-template-ID-from-previous-step>"
    },
    "public_ssh_key": "<your-public-ssh-key>",
    "external_network_name": "<openstack-external-network-name>",
	"external_network_uuid": "<openstack-external-network-uuid>",
	"external_subnet_uuids": [
		"<a-subnet-under-external-network>"
	],
	"project_domain_uuid": "<project_domain_uuid>",
	"OS_AUTH_URL": "<auth-url>",
    "OS_IDENTITY_API_VERSION": "3",
    "OS_INTERFACE": "<interface>",
    "OS_PROJECT_DOMAIN_ID": "<domain-id>",
    "OS_REGION_NAME": "<region-name>",
    "OS_USER_DOMAIN_NAME": "<domain-name>"
}
```

Then, create the provider
```bash
cacao provider create my-provider https://example.com openstack --metadata-file provider_metadata.json
```

Save the provider ID generated from the output (e.g. `provider-xxxxxxxxxxxxxxxxxxxx`)

### Create a template
> Performed by Admin
Import a template. Use this single-image template as an example https://gitlab.com/cyverse/cacao-tf-os-ops/-/blob/main/single-image/metadata.json .
```bash
cacao template create git https://gitlab.com/cyverse/cacao-tf-os-ops.git openstack-single-image \
	--public \
	--branch main \
	--path single-image
```

Save the template ID generated from the output (e.g. `template-xxxxxxxxxxxxxxxxxxxx`)

### Create a workspace
Create a workspace with the default provider ID set to the one from previous step.
```bash
cacao workspace create my-workspace --default-provider-id ${defaultProviderID}
```

Save the workspace ID generated from the output (e.g. `workspace-xxxxxxxxxxxxxxxxxxxx`)

### Create a credential
Construct a JSON file that contains your openstack credential, use the example below as a guide. Note that the structure is very similar to the `.openrc` file the openstack generated.

`openstack_credential.json`
```json
{
  "OS_REGION_NAME": "os_region_name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "os_interface",
  "OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "os_user_domain_name",
  "OS_PROJECT_NAME": "os_project_name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://os-auth-url",
  "OS_AUTH_TYPE": "",
  "OS_USERNAME": "os_username",
  "OS_PASSWORD": "os_password"
}
```

`openstack_credential.json` with OpenStack application credential
```json
{
  "OS_REGION_NAME": "os_region_name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "os_interface",
  "OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "os_user_domain_name",
  "OS_PROJECT_NAME": "os_project_name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://os-auth-url",
  "OS_AUTH_TYPE": "v3applicationcredential",
  "OS_APPLICATION_CREDENTIAL_ID": "os_application_credential_id",
  "OS_APPLICATION_CREDENTIAL_SECRET": "os_application_credential_secret"
}
```

Create credential with the JSON file.
```bash
cacao credential create ${providerID} openstack --value-file openstack_credential.json
```

> Note, here we use `${providerID}` as the name of the credential, you are not required to name it this way.

Save the credential ID generated from the output (e.g. `cred-xxxxxxxxxxxxxxxxxxxx`)

### Create a deployment
Create a deployment with the workspace, template, and provider from previous steps
```bash
cacao deployment create ${workspaceID} ${templateID} ${providerID} --cloud-creds ${credentialID} --name my-deployment-name
```

Save the deployment ID generated from the output (e.g. `deployment-xxxxxxxxxxxxxxxxxxxx`)

### Run the deployment
First, create a JSON file for the request. 

`run.json`
```json
{
	"deployment_id": "<deployment-ID-from-above>",
    "cloud_credentials": ["<credential-ID-from-above>"],
    "git_credential_id": "",
    "parameters": [
        {
            "key": "flavor",
            "value": "<openstack-instance-size>"
        },
        {
            "key": "image_name",
            "value": "<openstack-image-name>"
        },
        {
            "key": "instance_count",
            "value": "1"
        },
        {
            "key": "instance_name",
            "value": "my-cacao-instance"
        },
        {
            "key": "power_state",
            "value": "active"
        }
    ]
}

```

Run the deployment
```bash
cacao run create -f run.json
```
> We are using the `-f` option to import from a file, this is because there can be a lot of parameters when creating a run, which can be annoying to type on the command line.

Save the run ID generated from the output (e.g. `run-xxxxxxxxxxxxxxxxxxxx`)

### Check status of the deployment & run

```bash
cacao run get ${deploymentID} ${runID} | jq -r .status
```
