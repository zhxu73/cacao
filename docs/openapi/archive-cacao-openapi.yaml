openapi: 3.0.0


info:
  title: Cacao API Gateway
  description: This is the Cacao API Gateway
  contact:
    email: calvinmclean@cyverse.org
  license:
    name: BSD
    url: https://www.cyverse.org/license
  version: 1.0.0


servers:
- url: /


tags:
- name: developers
  description: standard developer operations
- name: admins
  description: administrator operations
- name: oauth2
  description: oauth2 operations


paths:
  /users:
    get:
      tags:
      - developers
      - admins
      summary: Gets an array of users. Non-administrative users will only get one user, self.
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
    post:
      tags:
      - admins
      summary: Create a new user
      parameters:
        - in: query
          name: username
          description: the username of the new user to create
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/UserCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
        description: User to add

  /users/{username}:
    get:
      tags:
      - developers
      summary: Get existing user information
      description: |
        Get information about the user. This will exclude any secrets. If username = 'mine', this will return the current user object.
      parameters:
      - name: username
        in: path
        description: username of user to get
        required: true
        schema:
          type: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - developers
      - admins
      summary: Update existing user; admins can modify any user, but developers can only change their own user
      parameters:
      - name: username
        in: path
        description: username of user to update
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      responses:
        '201':
          $ref: '#/components/responses/UserUpdated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
    delete:
      tags:
      - admins
      summary: Delete an existing user
      parameters:
      - name: username
        in: path
        description: username of user to update
        required: true
        schema:
          type: string
      responses:
        '204':
          $ref: '#/components/responses/RequestAccepted'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'

  /users/{username}/secrets:
    get:
      tags:
      - developers
      summary: Get a users set of credentials
      description: |
        Get collection of credentials owned by the user.
      parameters:
      - name: username
        in: path
        description: username of user to get
        required: true
        schema:
          type: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Secret'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    post:
      tags:
      - developers
      summary: Create a new secret for a user
      parameters:
        - in: path
          name: username
          description: the username who owns the secret
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/SecretCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Secret'
        description: User to add

  /users/{username}/secrets/{secretID}:
    get:
      tags:
      - developers
      summary: Get existing secret item owned by a user
      description: |
        Get information about a secret owned by a user
      parameters:
        - name: username
          in: path
          description: username of user to get
          required: true
          schema:
            type: string
        - name: secretID
          in: path
          description: id of secret
          required: true
          schema:
            type: string
      responses:
        '200':
           description: success
           content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/Secret'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - developers
      summary: Update existing user secret, given a secretID
      parameters:
      - name: username
        in: path
        description: username of user to update
        required: true
        schema:
          type: string
      - name: secretID
        in: path
        description: id of secret
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Secret'
      responses:
        '201':
          $ref: '#/components/responses/SecretUpdated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
    delete:
      tags:
      - developers
      summary: Delete an existing user secret, given a secretID
      parameters:
      - name: username
        in: path
        description: username of user to update
        required: true
        schema:
          type: string
      - name: secretID
        in: path
        description: id of secret
        required: true
        schema:
          type: string
      responses:
        '204':
          $ref: '#/components/responses/RequestAccepted'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'


  /user/login:
    get:
      tags:
      - oauth2
      summary: Login by redirect to Keycloak SSO
      responses:
        "200":
          description: success

  /user/login/callback:
    get:
      tags:
      - oauth2
      summary: Callback for Keycloak login
      responses:
        "200":
          description: success

  /workflows:
    get:
      tags:
      - developers
      - admins
      summary: Gets all the workflows for the current user
      description: Use this method to get all the workflows for the current user. Administrators may query for specific user
      parameters:
        - name: username
          in: query
          description: filter by username (admin only operation)
          required: false
          schema:
            type: string
        - name: workflowType
          in: query
          description: filter by workflow type
          required: false
          schema:
            type: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Workflow'
        '400':
          $ref: '#/components/responses/InvalidRequest'
    post:
      tags:
      - developers
      summary: Create a new workflow
      parameters:
        - in: query
          name: username
          description: the username who owns the workflow
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/WorkflowCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Workflow'
        description: Workflow to add

  /workflows/{workflowID}:
    get:
      tags:
      - developers
      summary: Gets workflow by workflowID
      description: Use this method to get information about a specific workflow
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
      responses:
        '200':
           description: success
           content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/Workflow'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - developers
      summary: Modify workflow
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
        - in: query
          name: username
          description: the username who owns the workflow
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/WorkflowUpdated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
    delete:
      tags:
      - developers
      summary: delete a workflow
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
      responses:
        '204':
          $ref: '#/components/responses/RequestAccepted'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'

  /workflows/{workflowID}/builds:
    get:
      tags:
      - developers
      summary: Gets all the builds for a workflow
      description: Use this method to get all builds for a workflow
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/WorkflowBuild'
        '400':
          $ref: '#/components/responses/InvalidRequest'
    post:
      tags:
      - developers
      summary: Create a new build
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/WorkflowBuildCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/WorkflowBuild'
        description: workflow build object

  /workflows/{workflowID}/builds/{buildID}:
    get:
      tags:
      - developers
      summary: Gets all a specific build for a workflow
      description: Use this method to get a specific builds for a workflow
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
        - name: buildID
          in: path
          description: id of build
          required: true
          schema:
            type: string
            format: xid
      responses:
        '200':
           description: success
           content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/WorkflowBuild'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - notallowed
      summary: Method PUT not allowed for workflow build object
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
        - name: buildID
          in: path
          description: id of build
          required: true
          schema:
            type: string
            format: xid
      responses:
        '405':
          $ref: '#/components/responses/MethodNotAllowed'
    delete:
      tags:
      - notallowed
      summary: Method DELETE not allowed for workflow build object
      parameters:
        - name: workflowID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
        - name: buildID
          in: path
          description: id of build
          required: true
          schema:
            type: string
            format: xid
      responses:
        '405':
          $ref: '#/components/responses/MethodNotAllowed'

  /runs:
    get:
      tags:
      - developers
      summary: Gets all the runs
      description: Use this method to get all runs for a workflow that are owned by the user as well as runs that have public visibility
      parameters:
        - name: workflowID
          in: query
          description: id of workflow to filter
          required: false
          schema:
            type: string
        - name: username
          in: query
          description: username to filter
          required: false
          schema:
            type: string
            format: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/WorkflowRun'
        '400':
          $ref: '#/components/responses/InvalidRequest'
    post:
      tags:
      - developers
      summary: Create a new run
      parameters:
        - name: workflowID
          in: query
          description: id of workflow
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/WorkflowRunCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/WorkflowRun'
        description: workflow run object

  /runs/{runID}:
    get:
      tags:
      - developers
      summary: Gets a specific run for a workflow
      description: Use this method to get a specific run for a workflow
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
            format: xid
      responses:
        '200':
           description: success
           content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/WorkflowRun'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - developers
      summary: Modify workflow run
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/WorkflowRunUpdated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
    delete:
      tags:
      - developers
      summary: Delete a workflow run
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
      responses:
        '204':
          $ref: '#/components/responses/RequestAccepted'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'

  /runs/{runID}/triggers:
    get:
      tags:
      - developers
      summary: Gets all triggers for a specific run for a workflow
      description: Gets all triggers for a specific run for a workflow
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
            format: xid
        - name: eventtype
          in: query
          description: eventtype to filter
          required: false
          schema:
            type: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Trigger'
        '400':
          $ref: '#/components/responses/InvalidRequest'
    post:
      tags:
      - developers
      summary: Create a new trigger for a run
      parameters:
        - name: runID
          in: path
          description: id of workflow
          required: true
          schema:
            type: string
        - name: eventtype
          in: query
          description: eventtype of the trigger
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/TriggerCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Trigger'
        description: trigger object

  /runs/{runID}/triggers/{triggerID}:
    get:
      tags:
      - developers
      summary: Gets a specific trigger for a specific run for a workflow
      description: Gets a specific trigger for a specific run for a workflow
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
            format: xid
        - name: triggerID
          in: path
          description: id of trigger for a workflow run
          required: true
          schema:
            type: string
            format: xid
      responses:
        '200':
           description: success
           content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/Trigger'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - developers
      summary: Modify trigger
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
        - name: triggerID
          in: path
          description: id of trigger for a workflow run
          required: true
          schema:
            type: string
            format: xid
      responses:
        '201':
          $ref: '#/components/responses/TriggerUpdated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
    delete:
      tags:
      - developers
      summary: Delete a workflow run
      parameters:
        - name: runID
          in: path
          description: id of workflow run
          required: true
          schema:
            type: string
        - name: triggerID
          in: path
          description: id of trigger for a workflow run
          required: true
          schema:
            type: string
            format: xid
      responses:
        '204':
          $ref: '#/components/responses/RequestAccepted'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'

  /clusters:
    get:
      tags:
      - developers
      - admins
      summary: Gets all the kubernetes clusters for the current user
      description: Use this method to get all the workflows for the current user. Administrators may query for specific user
      parameters:
        - name: username
          in: query
          description: filter by username (admin only operation)
          required: false
          schema:
            type: string
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Cluster'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    post:
      tags:
      - developers
      summary: Create a new kubernetes cluster
      parameters:
        - name: username
          in: query
          description: user to create a cluster
          required: false
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/ClusterCreated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Cluster'
        description: cluster object

  /clusters/{clusterID}:
    get:
      tags:
      - developers
      summary: Gets a specific kubernetes cluster
      parameters:
        - name: clusterID
          in: path
          description: id of cluster
          required: true
          schema:
            type: string
            format: xid
      responses:
        '200':
           description: success
           content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/Cluster'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
    put:
      tags:
      - developers
      summary: Modify kubernetes cluster
      parameters:
        - name: clusterID
          in: path
          description: id of kubernetes cluster
          required: true
          schema:
            type: string
      responses:
        '201':
          $ref: '#/components/responses/ClusterUpdated'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
    delete:
      tags:
      - developers
      summary: Delete a kubernetes cluster
      parameters:
        - name: clusterID
          in: path
          description: id of kubernetes cluster
          required: true
          schema:
            type: string
      responses:
        '204':
          $ref: '#/components/responses/RequestAccepted'
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'

  /eventtypes:
    get:
      tags:
      - developers
      summary: Gets all the event types
      description: event types are necessary to define triggers for workflow run+s. 
        Currently, runs can only be activated by hitting the activation endpoint via http/https ("onHttp").
        Other triggers will be supported in the future, like onDataChange, onPort, onQueue, etc. Future
        implementation will expand event type resource.
      responses:
        "200":
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                  enum: ["onHttp"]
    post:
      tags:
      - notallowed
      summary: Method POST not allowed for event types resource
      responses:
        '405':
          $ref: '#/components/responses/MethodNotAllowed'


components:
  schemas:
    User:
      type: object
      required:
        - username
      properties:
        username:
          type: string
          readOnly: true
          format: string
          example: edwin
    Secret:
      type: object
      required:
        - id
        - name
        - type
        - owner
        - userOrEmail
      properties:
        id:
          type: string
          readOnly: true
          format: string
          description: id of secret
          example: xid
        name:
          type: string
          format: string
          description: name of secret
          example: "my secret"
        type:
          type: string
          format: string
          description: type of secret
          example: "github"
        owner:
          type: string
          readOnly: true
          format: string
          description: owner of secret
          example: edwin
        userOrEmail:
          type: string
          format: string
          description: user[name] or email for the secret
          example: "edwin@cyverse.org"
        uri:
          type: string
          format: string
          description: uri of the service and resource, if applicable
          example:  https://www.github.com
    Workflow:
      type: object
      required:
        - name
      properties:
        name:
          type: string
          format: string
          example: my workflow
    WorkflowBuild:
      type: object
      required:
        - name
      properties:
        name:
          type: string
          format: string
          example: my workflow build
    WorkflowRun:
      type: object
      required:
        - name
      properties:
        name:
          type: string
          format: string
          example: my workflow run
    Trigger:
      type: object
      required:
        - name
      properties:
        name:
          type: string
          format: string
          example: my trigger

    Cluster:
      type: object
      required:
        - name
      properties:
        name:
          type: string
          format: string
          example: my kubernetes cluster
    Error:
      type: object
      required:
        - timestamp
        - status
        - error
        - message
        - path
      properties:
        timestamp:
          type: string
          readOnly: true
          format: date-time
          example: "2017-07-21T17:32:28Z"
        status:
          type: string
          readOnly: true
          format: integer
          example: 401
        error:
          type: string
          readOnly: true
          format: string
          example: "Your request is unauthorized"
        message:
          type: string
          readOnly: true
          format: string
          example: "You are not authorized to create a new user"
        path:
          type: string
          readOnly: true
          format: string
          example: "/users"
        verb:
          type: string
          readOnly: true
          format: string
          example: POST


  responses:
    UserCreated:
      description: "User Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/User'
    UserUpdated:
      description: "User Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/User'
    SecretCreated:
      description: "Secret Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Secret'
    SecretUpdated:
      description: "Secret Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Secret'
    WorkflowCreated:
      description: "Workflow Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Workflow'
    WorkflowUpdated:
      description: "Workflow Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Workflow'
    WorkflowBuildCreated:
      description: "Workflow Build Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/WorkflowBuild'
    WorkflowBuildUpdated:
      description: "Workflow Build Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/WorkflowBuild'
    WorkflowRunCreated:
      description: "Workflow Run Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/WorkflowRun'
    WorkflowRunUpdated:
      description: "Workflow Run Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/WorkflowRun'
    TriggerCreated:
      description: "Trigger Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Trigger'
    TriggerUpdated:
      description: "Trigger Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Trigger'
    ClusterCreated:
      description: "Cluster Created"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Cluster'
    ClusterUpdated:
      description: "Cluster Updated"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Cluster'
    RequestAccepted:
      description: "Request to create or update entity accepted"
    Unauthorized:
      description: "Your request is unauthorized"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Error'
    NotFound:
      description: "Entity not found"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Error'
    InvalidRequest:
      description: "Invalid request"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Error'
    GeneralError:
      description: "General unspecified error"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Error'
    Conflict:
      description: "Conflict occurred while attempting to create your resource"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Error'
    MethodNotAllowed:
      description: "Method on this resource is not allowed"
      content:
        "application/json":
          schema:
            type: object
            $ref: '#/components/schemas/Error'
