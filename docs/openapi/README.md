This directory contains the OpenAPI docs for cacao. The primary file will be `openapi.yaml`.

Editors that work with OpenAPI v3+ include:
* vscode with OpenAPI (Swagger) Editor plugin
* https://editor.swagger.io/

The openapi.yaml file can be used with Stoplight Prism mock api server. There are two ways to use Prism:
* docker compose
1. install docker and docker-compose
2. `export BASE_DIR=/opt`
3. `cd $BASE_DIR`
4. `git clone https://gitlab.com/cyverse/cacao`
5. `cd $BASE_DIR/cacao/docs/openapi`
6. `docker-compose up`
* kubernetes (k3s, minikube, etc -- note: k3d requires additional work to mount the a volume at the time of cluster creation and is not covered by these steps)
1. install k3s
2. `export BASE_DIR=/opt`
3. `cd $BASE_DIR`
4. `git clone https://gitlab.com/cyverse/cacao`
5. `cd $BASE_DIR/cacao/docs/openapi`
6. IMPORTANT: if cacao is NOT deployed to /opt/cacao, you will need to change the volume hostpath to point to the full path of openapi.yaml
7. `kubectl apply -f k8s-deploy.yaml`
8. `kubectl get service prism-service` to get the CLUSTER-IP
9. `curl <CLUSTER-IP>:4010/<path>` (you can also use the browser)


If you want to use a UI to view the OpenAPIs locally rather than through gitlab, you can use swagger ui in a docker container. For example, let's say you want to install the open api in /opt and assuming the current user has docker access:
* `export BASE_DIR=/opt`
* `cd $BASE_DIR`
* `git clone https://gitlab.com/cyverse/cacao`
* `docker pull swaggerapi/swagger-ui`
* `docker run -p9080:8080 --rm -e URL=http://localhost:9080/openapi/openapi.yaml -v ${BASE_DIR}/cacao/docs/openapi/:/usr/share/nginx/html/openapi/ swaggerapi/swagger-ui`
* open browser to http://localhost:9080/

Note: you can change 9080 to any port that is available on your host system. 
