# Keycloak

Keycloak is used for authentication in CACAO and some setup is required if you want to use it for local development. Alternatively, you can use a username/password for local development by simply skipping these steps and using the password as `CACAO_TOKEN`


### Table of Contents
[[_TOC_]]


## Introduction
Keycloak is a single sign-on service that acts as an authentication provider for CACAO (and CACAO is a client of Keycloak). When a user goes to login, CACAO will redirect the user to Keycloak's login page to enter their credentials. After entering credentials, the user will be presented with a response containing an ID Token and some other information.


## How it works
CACAO implements a `Client` struct that is a wrapper around a few other structs from OAuth2 and OpenID Connect imports so that it can easily use those functions to complete the authentication flow.


### Getting a Token
Authentication with Keycloak works using the OpenID Connect "authorization code flow." This consists of a few steps:
  1. Redirect user to Keycloak login page
      - This redirect will include a "state" parameter which is an HMAC secret
  2. User logs in to Keycloak and is redirected back to CACAO with an authorization code
      - When Keycloak does this redirect, it will include the original "state" parameter so that CACAO can verify that the state is unchanged
  3. Once CACAO receives the authorization code, it will exchange it with the Keycloak server for the actual tokens that will be presented to the user


### Authentication with Token
Once the user has a token, they can include this in any requests to CACAO as the `Authorization` header. CACAO will simply use the Keycloak Client to verify the token against the Keycloak server. The verification response will be used to get the username and admin status of the requesting user.


### Admin Enforcement
Admin users are verified by Keycloak, which will include the `cacao_admin` field if the user is an admin. An authentication middleware sits in front of all admin-only endpoints and verifies this field before allowing the request to continue.


## Setup instructions

### Local Development
For local development, Keycloak setup is handled automatically by the ansible playbook. These variables are required in `config.yaml` for a successful local deployment:
```yaml
CACAO_AUTH_DRIVER: "keycloak"
KEYCLOAK_LOCAL: true
KEYCLOAK_SECRET: "top-secret-info"
KEYCLOAK_REALM: "cacao"
KEYCLOAK_BASE_URL: "http://{{ NGINX_INGRESS_IP }}"
KEYCLOAK_URL: "{{ KEYCLOAK_BASE_URL }}/auth/realms/{{ KEYCLOAK_REALM }}"
KEYCLOAK_REDIRECT_URL: "http://{{ API_DOMAIN }}/api/user/login/callback"
KEYCLOAK_USERS:
  - username: cacao-user
    password: cacao-user-password
    admin: false
  - username: cacao-admin
    password: cacao-admin-password
    admin: true
# You can also add your own users here
```
#### keycloak admin console
Assuming the default setup handled by the ansible playbook, the admin console will be available at http://ca.cyverse.local/auth/admin/master/console.
You can login with username `admin` and password `admin`.

#### openid configuration
Assuming the default setup handled by the ansible playbook, the openid configuration for `cacao` realm will be available at
http://ca.cyverse.local/auth/realms/cacao/.well-known/openid-configuration

### Production
Production setup will require some additional steps to make sure the client has everything it needs to be compatible with CACAO.

1. Login to the Keycloak admin page
2. Click `Clients` from the left-side menu, then click `Create` on the right side. Name the client `cacao-client`
3. Set `Access Type` to `Confidential`
4. Add `http://$CACAO_API/user/login/callback` as a Valid Redirect URI
5. Save
6. After saving, select the `Credentials` tab and copy the `Secret` to the `KEYCLOAK_SECRET` env var in `install/config.yaml`
7. Set other variables in `install/config.yaml`:
    ```yaml
    KEYCLOAK_URL: http://$KEYCLOAK_URL/auth/realms/cacao
    KEYCLOAK_REDIRECT_URL: http://$CACAO_API/user/login/callback
    ```
8. Now setup the `cacao_admin` mapper by going to the `Mappers` tab and click "add". Make sure to set the following fields:
    - Client ID: `cacao-client`
    - Multivalued: `OFF`
    - Token Claim Name: `cacao_admin`
    - Claim JSON Type: `string`
    - Add to ID token: `ON`
    - Add to access token: `ON`
    - Add to userinfo: `ON`
