# Authentication
The api service is the api gateway for CACAO backend, it handles the user authentication for the CACAO backend.
Authentication happens on each HTTP request to api service. Authenticator will check the header of the request to determine if the request should be authenticated.
All 4 authenticators currently implemented will check the token in `Authorization` header.

There are 4 authenticators:
- keycloak
- Globus
- CILogon
- Simple token
  - a simple plain text token set via environment variable
  - for local testing only


## [Keycloak](./keycloak.md)
## [Globus](./globus.md)
## CILogon
CILogon is an identity federation service. It has a OIDC interface just like Globus.
For the initial implementation, we are targeting the ACCESS-CI identity provider, but we could also use other identity provider with CILogon (e.g. UofA) if the configuration is in place.

Doc for OIDC
https://www.cilogon.org/oidc

CILogon OIDC Configuration
https://cilogon.org/.well-known/openid-configuration

### Configuration
In order to configure the CILogon auth driver, you need to specify the following 4 environment variables.
- `API_AUTH_DRIVER`
  - `API_AUTH_DRIVER=cilogon`
- `API_OAUTH2_CLIENT_SECRET`
- `API_OAUTH2_CLIENT_ID`
- `API_OAUTH2_REDIRECT_URL`

If you want to require user to have identity from a specific identity provider, then you can also specify the id of the identity provider (`API_REQUIRED_IDENTITY_PROVIDER`).
The "id of the identity provider" we refer to is the `"idp"` field (not `"idp_name"`) in userinfo response. By default, this authenticator requires identity from ACCESS-CI.

### Token introspection
The token introspection endpoint is used to validate the access token.
From the response, we can only determine if the token is still active(`active` claim), and if the token is within its lifetime (`exp` & `nbf` claim).
The `username` claim is the internal representation from CILogon, not the username from the identity provider.

### Userinfo
The userinfo endpoint will return the relevant information about the identity from the identity provider.
This is where we can get the username from the identity provider, (e.g. for ACCESS-CI, `foobar@access-ci.org`) and other information like email, first/last name.

## Token cache
For KeyCloak, Globus and CILogon auth drivers, there is an access token cache.

We cache the access token, and the user profile associated with it. Conceptually, it is basically a map like `map[accessToken]UserProfile`.

Caching access token provide benefit on 2 fronts:
1. better performance
If there is no caching, api-service would need to make external calls to verify the token and obtain user info.
This can be very costly, since frontend(Ion) could make multiple call to api-service to render a single page. This means
we could make multiple duplicate calls to verify token for a single page render, which is VERY bad for latency.

2. The ability to extend user session beyond the access token lifetime.
If the lifetime of the access token we get from OIDC provider is short (shorter than a typical user session). Then we could
increase the cache TTL to compensate. This way, even if the token has expired, user session will not be interrupted (no need to re-login or refresh token).

The downside of caching beyond token lifetime is that we will not respect token revocation which is a relative minor issue compared to frequent login.

> Note that due to the way cache is implemented, if there are multiple concurrent requests with the same token, there could be more than 1 cache miss.
> This is because we did a naive lookup->verify->save, rather than some kind of compare-and-swap or lock.
> e.g. if 2nd request lookup the cache before 1st request save to cache, then 2nd request will have a cache miss, and will do a verification itself.
