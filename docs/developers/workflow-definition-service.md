# Workflow Definition Service

> This service is currently not used

The WorkflowDefintion Service is responsible for reading a YAML file from a git repository in able to gather information for Build and Run. It acts as a mediator between the user and these other services since they rely on information from this service.

## Table of Contents
[[_TOC_]]


## Environment

| Variable name   | Required | Default                     | Description                                |
| :-------------- | :------- | :-------------------------- | :----------------------------------------- |
| MONGODB_NAME    | yes      | `workflow_definition`       | name of the database/collection in MongoDB |
| MONGODB_ADDRESS | yes      | `mongodb://localhost:27017` | address of MongoDB server                  |
| NATS_CLUSTER_ID | yes      | `cacao-cluster`             | ID of the cluster to connect to            |
| NATS_CLIENT_ID  | yes      | `workflow-definition`       | ID of _this_ client                        |
| NATS_ADDRESS    | yes      | `nats://localhost:4222`     | address of the NATS Streaming server       |


## Storage Back-end  

MongoDB is used as a storage back-end because it can easily store these YAML "documents" without too much difficulty.


## Communication

This service will react to events/messages published to NATS and NATS Streaming.


### NATS Streaming

On NATS Streaming, this service subscribes to 5 separate subjects/channels:
  - `Create`: this Subscriber is used to create new WorkflowDefintions from a git URL and branch. The resulting WorkflowDefintion is saved in MongoDB

  - `Delete`: this Subscriber simply deletes a WorkflowDefintion

  - `Update`: this Subscriber allows a user to edit a WorkflowDefintion object and push those changes back to the git repository. The repository URL and branch cannot be changed so a new WorkflowDefintion should be created instead. If no changes are included in the request, this will just update from the git repository


### NATS

On NATS, it subscribes to 2 subjects to offer synchronous responses:
  - `Get`: this will return all of the information on a WorkflowDefintion
  - `GetForuser`: this will return all WorkflowDefintions owned by a specific user


## Adding Workflow Types

CACAO uses a `CACAOWorkflow` interface to facilitate the addition of new Workflow Types. The interface defines functions used to create, deploy, and delete resources on a Kubernetes cluster.

### Developing a Workflow Type
1. Create a new file in [`cacao/workflow-definition-service/workflowdefinition/types`](../workflow-definition-service/workflowdefinition/types) with a descriptive name like `my_workflow.go`
2. Create your new type with a name similar to your file, like `MyWorkflow`
3. Create a constructor to create this type from a `[]byte` (which will come from a string in `wfd.yaml`) called `NewMyWorkflow`:
  ```go
  func NewMyWorkflow(workflowString []byte) (CACAOWorkflow, error){}
  ```
4. Find the `WorkflowFactory` function in [`cacao/workflow-definition-service/workflowdefinition/types/workflow_types.go`](../workflow-definition-service/workflowdefinition/types/workflow_types.go) and add a string to the case match (this will be used as the `type` value in `wfd.yaml`) that will run the function from (3) and return the results
  ```go
  func WorkflowFactory(workflowType string, workflowBytes []byte) (CACAOWorkflow, error) {
  	switch workflowType {
  	case "argo":
  		return NewArgoWorkflow(workflowBytes)
  	case "mock":
  		return NewMockWorkflow(workflowBytes)
  	case "container", "":
  		return NewContainerWorkflow(workflowBytes)
  	default:
  		return nil, fmt.Errorf("no constructor for Workflow Type '%s'", workflowType)
  	}
  }
  ```
5. Create additional functions according the the `CACAOWorkflow` interface


### CACAOWorkflow Interface

#### Type
Simply returns a string with the name of this Workflow Type

---

#### Run
This will create resources and deploy to Kubernetes.

Please include a list of all created Kubernetes resources in the function comment. Also, please add a label like this:
```go
Labels: map[string]string{
  "app":        name,
  "version":    version,
  "cacao_id": id,
}
```

##### Parameters

| Name         | Type                     | Description                                                          |
| :----------- | :----------------------- | :------------------------------------------------------------------- |
| `clientsets` | `common.K8sClientsets`   | contains config used to deploy resources to cluster                  |
| `request`    | `map[string]interface{}` | the request used to initiate this action, containing additional info |
| `name`       | `string`                 | name to use for the resources, generated in Pleo before calling this |
| `namespace`  | `string`                 | name of the namespace to deploy resources to                         |

##### Return values

| Name     | Type                                 | Description                               |
| :------- | :----------------------------------- | :---------------------------------------- |
| `err`    | `error`                              | error                                     |

---

#### SetupRouting
This will create the K8s/Istio resources related to routing HTTP requests to the Workflow. This consists primarily of Istio's `VirtualService` and `DestinationRule` resources. There is a function, `standardIstioRouting`, that sets up some of this basic routing and is used for Container Workflows and Argo Workflows at this time.

##### Parameters

| Name              | Type                     | Description                                                                |
| :---------------- | :----------------------- | :------------------------------------------------------------------------- |
| `clientsets`      | `common.K8sClientsets`   | contains config used to deploy resources to cluster                        |
| `adminClientsets` | `common.K8sClientsets`   | contains config used to deploy resources in the `istio-system` namespace   |
| `request`         | `map[string]interface{}` | the request used to initiate this action, containing additional info       |
| `name`            | `string`                 | name to use for the resources, generated in Pleo before calling this       |
| `namespace`       | `string`                 | name of the namespace to deploy resources to                               |
| `cluster`         | `common.Cluster`         | this is the cluster config being used. This has the important `host` field |

##### Return values

| Name     | Type                                 | Description                               |
| :------- | :----------------------------------- | :---------------------------------------- |
| `err`    | `error`                              | error                                     |

---

#### Delete
This will delete resources created by the `Run` function from the cluster.

##### Parameters

| Name         | Type                   | Description                                                          |
| :----------- | :--------------------- | :------------------------------------------------------------------- |
| `clientsets` | `common.K8sClientsets` | contains config used to delete resources from cluster                |
| `name`       | `string`               | name to use for the resources, generated in Pleo before calling this |
| `namespace`  | `string`               | name of the namespace to delete resources from                       |

##### Return values

| Name     | Type                                 | Description                               |
| :------- | :----------------------------------- | :---------------------------------------- |
| `err`    | `error`                              | error                                     |

---

#### ScaleFromZero
This is very similar to `Run` and in some cases may just use `Run`. If something is able to be scaled up without re-creating, that is preferred but in some cases new resources will need to be created.

##### Parameters

| Name         | Type                     | Description                                                          |
| :----------- | :----------------------- | :------------------------------------------------------------------- |
| `clientsets` | `common.K8sClientsets`   | contains config used to deploy resources to cluster                  |
| `request`    | `map[string]interface{}` | the request used to initiate this action, containing additional info |
| `name`       | `string`                 | name to use for the resources, generated in Pleo before calling this |
| `namespace`  | `string`                 | name of the namespace to deploy resources to                         |

##### Return values

| Name     | Type                                 | Description                               |
| :------- | :----------------------------------- | :---------------------------------------- |
| `err`    | `error`                              | error  
