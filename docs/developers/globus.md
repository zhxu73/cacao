# Globus

Globus can be used for authentication in CACAO, and some configuration is required to get it to work correctly. Since
Globus is an external identity provider, the OAuth2 client must be defined outside of CACAO itself. Please see the
[Globus Auth API Documentation][1] for instructions on creating the client.


### Table of Contents
[[_TOC_]]


## Introduction
Globus is an online identity provider that uses InCommon to authenticate users. Please see the [Globus Auth API
Documentation][1] for details.


## How it works
CACAO implements a `Client` struct that is a wrapper around a few other structs from the OAuth2 import so that it can
easily use those functions to complete the authentication flow.


### Getting a Token
Authentication with Keycloak works using the OAuth2 "authorization code flow." This consists of a few steps:
  1. Redirect user to Globus login page
      - This redirect will include a "state" parameter which is an HMAC secret
  2. User logs in to Globus and is redirected back to CACAO with an authorization code
      - When Globus does this redirect, it will include the original "state" parameter so that CACAO can verify that
        the state is unchanged
  3. Once CACAO receives the authorization code, it will exchange it with the Globus server for the actual tokens that
     will be presented to the user


### Authentication with Token
Once the user has a token, they can include this in any requests to CACAO as the `Authorization` header. CACAO will
call the token introspection endpoint in the Globus Authentication API in order to verify that the token is valid.


### Admin Enforcement
Admin users are verified by calling the user microservice. The `X-Cacao-Admin` header will be set to `true` if the
user is an admin. Otherwise, it will be set to `false`.

## Setup instructions

### OAuth Client Definition

In order to use Globus authentication, it is necessary to create an OAuth client for the API. This OAuth client should
have a custom scope associated with it. The custom scope allows other services to request access to the API on behalf
of their users. Here's an example definition:

``` json
{
  "allows_refresh_token": true,
  "required_domains": [],
  "scope_string": "https://auth.globus.org/scopes/b184f54f-4df1-45cd-8618-f6704bf1147c/all",
  "advertised": true,
  "name": "urn:globus:auth:scope:cyverse-cacao",
  "id": "537d849f-dc46-4bca-b453-73752de5ef12",
  "description": "All Operations on the API",
  "client": "b184f54f-4df1-45cd-8618-f6704bf1147c",
  "dependent_scopes": [
    {
      "scope": "245cd2d7-4831-4ee4-a81d-f8892625e865",
      "optional": false,
      "requires_refresh_token": true
    },
    {
      "scope": "03942612-0f39-4279-b823-02c332aa0fce",
      "optional": false,
      "requires_refresh_token": true
    },
    {
      "scope": "027b9e24-2902-4937-ab07-58ab5a9ec7a3",
      "optional": false,
      "requires_refresh_token": true
    }
  ]
}
```

Note: The dependent scopes don't seem to be required, but they were defined for the scope that was used for initial
testing.

It's important that this scope is attached to the OAuth client that is used for the API service. Here's an example
service definition:

``` json
{
  "visibility": "private",
  "name": "CACAO Development Testing - Sarah",
  "preselect_idp": null,
  "id": "b184f54f-4df1-45cd-8618-f6704bf1147c",
  "grant_types": [
    "authorization_code",
    "client_credentials",
    "refresh_token",
    "urn:globus:auth:grant_type:dependent_token"
  ],
  "public_client": false,
  "parent_client": null,
  "links": {
    "privacy_policy": null,
    "terms_and_conditions": null
  },
  "redirect_uris": [
    "http://localhost:8080/user/login/callback"
  ],
  "scopes": [
    "537d849f-dc46-4bca-b453-73752de5ef12"
  ],
  "project": "48ef9b6e-1099-4ecb-be59-d08257da39be",
  "required_idp": null,
  "fqdns": []
}
```

### Calling the API

If you have an app that needs to call the CACAO API, it will be necessary to create another Globus OAuth client for
the client app to use. This client needs no special definition. Here's an example:

``` json
{
  "visibility": "private",
  "links": {
    "privacy_policy": null,
    "terms_and_conditions": null
  },
  "required_idp": null,
  "preselect_idp": null,
  "redirect_uris": [
    "http://localhost:5000/login"
  ],
  "grant_types": [
    "authorization_code",
    "client_credentials",
    "refresh_token",
    "urn:globus:auth:grant_type:dependent_token"
  ],
  "public_client": false,
  "project": "48ef9b6e-1099-4ecb-be59-d08257da39be",
  "fqdns": [],
  "id": "0bc8d934-6fc9-4638-9a04-0c74cd202d49",
  "parent_client": null,
  "scopes": [],
  "name": "cacao-confidential-client"
}
```

When requesting authorization to make calls on behalf of the user, the client app must request the scope associated
with the API's OAuth client. The scope string associated with the example API client is
`https://auth.globus.org/scopes/b184f54f-4df1-45cd-8618-f6704bf1147c/all`, so this string must be included in the
`scope` query parameter when redirecting the user to Globus's authorization endpoint. Please see the [Globus OAuth
Documentation][1] for details.

Once the app has gone through the authentication flow and exchanged the authorization code for access tokens, the
token response body should contain multiple access tokens, for example:

``` json
{
  "access_token": "REDACTED_1",
  "scope": "profile openid email",
  "expires_in": 172800,
  "token_type": "Bearer",
  "resource_server": "auth.globus.org",
  "state": "_default",
  "refresh_token": "REDACTED_2",
  "other_tokens": [
    {
      "access_token": "REDACTED_3",
      "scope": "https://auth.globus.org/scopes/b184f54f-4df1-45cd-8618-f6704bf1147c/all",
      "expires_in": 172800,
      "token_type": "Bearer",
      "resource_server": "b184f54f-4df1-45cd-8618-f6704bf1147c",
      "state": "_default",
      "refresh_token": "REDACTED_4"
    }
  ],
  "id_token": "REDACTED_5"
}
```

In this example, the access token that can be validated by the client app is `REDACTED_1`, although it's unneccessary
to validate an access token that has just been recived from Globus. The access token that can be used to call the API
is `REDACTED_3`. In cases where there's more than one token underneath `other_tokens`, the client app can determine
which token to use by looking at the `resource_server` parameter. This identifier will be equal to the identifier of
the OAuth client used by the API (`b184f54f-4df1-45cd-8618-f6704bf1147c`).

### Local Development
For local development, it's currently necessary to create a config map then modify the API service deployment
definition found in `kubernetes-manifests/skaffold/api-service.yaml`. The config map definition should look something
like this:

``` yaml
apiVersion: v1
data:
  api-auth-driver: "globus"
  oauth2-redirect-url: "http://localhost:8080/api/user/login/callback"
  oauth2-client-id: "some-client-id"
  oauth2-client-secret: "some-client-secret"
kind: ConfigMap
metadata:
  labels:
    service: cacao-config
  name: cacao-config
```

The client ID and client secret can be obtained from Globus when you configure the OAuth2 client. Once the config map
definition has been created, you can add it to Kubernetes using `kubectl apply -f`. The API service deployment
definition can then be modified to obtain its environment variable values from the secret you just defined. To do
this, add the following environment variables to the deployment specification:

``` yaml
            - name: API_AUTH_DRIVER
              valueFrom:
                configMapKeyRef:
                  optional: true
                  key: api-auth-driver
                  name: cacao-config
            - name: API_OAUTH2_REDIRECT_URL
              valueFrom:
                configMapKeyRef:
                  optional: true
                  key: oauth2-redirect-url
                  name: cacao-config
            - name: API_OAUTH2_CLIENT_ID
              valueFrom:
                configMapKeyRef:
                  optional: true
                  key: oauth2-client-id
                  name: cacao-config
            - name: API_OAUTH2_CLIENT_SECRET
              valueFrom:
                configMapKeyRef:
                  optional: true
                  key: oauth2-client-secret
                  name: cacao-config
```

If desired, you can also remove any Keycloak environment variable definitions that may be present in the deployment
definition.

### Production
Production setup will be very similar to development setup except that the callback URL must use HTTPS.

[1]: https://docs.globus.org/api/auth/
