# Hexagonal Architecture
Hexagonal architecture is a pattern on how to structure your code. Hexagonal architecture split code into domain, ports and adapters.
Domain is where the core business logic happens. Adapters implement the code that interfaces with external components,
e.g. external API, other microservices, databases, certain 3rd party libraries.
Ports define the interface between domain adapter, thus abstract away the implementation details of adapter, makes domain code simpler.

> Except for API service, all services (and new services being implemented) follows hexagonal architecture.

## Folder structure
```
service-root
    |__ adapters/ # adapters go here. each adapter is its own file (unless they are tiny, and there is a reasonable way to group them). uou can create sub-folder if necessary.
    |__ domain/ # domain logic goes here. you can create sub-folder if necessary.
    |__ ports/
    |    |___ ports.go # port definition goes in this file.
    |__ types/ # any type definition or util functions that are shared among adapters & domain & ports. you can create sub-folder if necessary.
    |__ main.go
```

## Example (user service, database)
Use database for user service as an example:
User service needs to do CRUD operation on records of user. And it needs a persistence layer. For the domain logic, it
does not care what the persistence layer is, just the fact that it exists and support CRUD operation on users.
If we want, we can switch to another database anytime, just implement a new adapter for the new database; no need to change the domain and port.

Assume user object looks like this:
```go
type User struct {
	Username string `json:"username"`
	Email string `json:"email"`
	IsAdmin bool `json:"is_admin"`
}
```

### Port
The port will look like the following, it provides the necessary methods for CRUD access, but does not make any assumptions about how those methods will be implemented.
```go
type UserStorage interface {
    Get(username string) (User, error)
    List() ([]User, error)
    Create(user User) error
	Update(user User) error
    Delete(username string) error
}
```

### Adapter
Your mongodb adapter that implement the port can look like this:
```go
type MongodbUserStorage struct {
	conn *mongo.Conn
}

const mongoUserCollectionName = "users"

func NewUserStorage(config MongoConfig) *MongodbUserStorage{
	conn := NewMongoConnection(config)
	return &MongodbUserStorage{conn: conn}
}

func (storage *MongodbUserStorage) Get(username string) (User, error){
    user, err := storage.conn.Get(mongoUserCollectionName, map[string]interface{"_id": username})	
	if err != nil {
		return User{}, error
    }
	return convertMongoResultToUser(user)
}

func (storage *MongodbUserStorage) List() ([]User, error) {
	userList, err := storage.conn.List(mongoUserCollectionName)	
	if err != nil {
		return nil, error
    }
	var result []User
	for _, user := result {
		converted, err := convertMongoResultToUser(user)
		if err != nil {
		    return nil, error
        }
		result = append(result, converted)
    }
	return result, nil
}

func (storage *MongodbUserStorage) Create(user User) error {
	if err := checkUser(user); err != nil {
		return err
    }
    return storage.conn.Insert(mongoUserCollectionName, user)	
}

func (storage *MongodbUserStorage) Update(user User) error {
	if err := checkUser(user); err != nil {
		return err
    }
    return storage.conn.Update(mongoUserCollectionName, map[string]interface{"_id": user.Username}, user)	
}

func (storage *MongodbUserStorage) Delete(username string) error {
    return storage.conn.Delete(mongoUserCollectionName, map[string]interface{"_id": username})	
}

func (storage *MongodbUserStorage) Close() {
	storage.conn.Close()
}
```
The adapter takes care of the connection creation, MongoDB collection name and all of the other implementation details that is specific to MongoDB.

### Domain
In the domain, you could have a handler for each operation, the one for create user operation will look like this:
```go
type UserCreationHandler struct {
	storage UserStorage
}

func (h UserCreationHandler) CreateUser(actor, emulator, username, email string, isAdmin bool) error {
	actorUser, err := h.storage.Get(actor)
	if err != nil {
		return err
    }
	if err := h.checkPermission(actorUser); err != nil {
		return err
    }
	return h.storage.Create(User{
		Username: username,
		Email: email,
		IsAdmin: isAdmin,
    })
}
func (h UserCreationHandler) checkPermission(user User) error {
	if user.IsAdmin {
		return nil
    }
	return fmt.Errorf("non-admin cannot create new user")
}
```
Notice that the domain code does not have to worry about any of the implementation details specific to MongoDB, no connection setup,
no need to worry about the specific function call to perform the fetch and insert operation in MongoDB.


### main.go
The entrypoint is where you would connect the domain and the adapter, simply assign the adapter to the field of handler.
```go
func main() {
    // ...
	userStorage := adapters.NewUserStorage(mongoConfig)
	handler := domain.UserCreationHandler{storage: userStorage}
	// ...
}
```
