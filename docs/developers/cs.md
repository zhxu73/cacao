# Container Security Service

> This service is currently not used

The Container Security Service(CS Service) serves as a background operation that scans and develops vulnerability reports on Docker images provided in a workflow definition. This service acts as an intermediary between CACAO and Anchore, a container/image analysis tool.

## Container Security Service Architecture
![](cs-architecture.png)


## Environment

| Variable name         | Required | Default                                    | Description                                |
| :-------------------- | :------- | :----------------------------------------- | :----------------------------------------- |
| NATS_CLUSTER_ID       | yes      | `cacao-cluster`                            | ID of the cluster to connect to            |
| NATS_CLIENT_ID        | yes      | `api`                                      | ID of _this_ client                        |
| NATS_ADDRESS          | yes      | `nats://localhost:4222`                    | address of the NATS Streaming server       |
| ANCHORE_URL           | yes      | `http://anchore-anchore-engine-api.default.svc.cluster.local:8228/v1/` | URL to Anchore Engine |
| ANCHORE_USER				  | yes      | `admin` | default user to utilize anchore-cli |
| ANCHORE_PASSWORD      | yes      | `foobar` | default password to utilize anchore-cli |



## Storage Back-end  

List of vulnerabilities is found on the Anchore-Engine Kubernetes cluster.


## Communication

This service listens to and publishes messages to NATS and NATS Streaming.


### NATS Streaming

On NATS Streaming, this service subscribes to the subject:
  - `WorlkflowDefinition.Finished`: this Subscriber is used to notify the container security service that the workflow definition has been created. The resulting WorkflowDefinition is sent to cs-service for image analysis.

### NATS

On NATS, it subscribes to `Anchore.Get` to offer a synchronous response:
  - `Anchore.Get`: this will return the vulnerability report(s) for the Docker image(s) in the workflow definition.  

## Usage
The easiest way to receive vulnerability reports on Docker images is to first run a workflow with cacao.
Here's an example command:
`cacao create wfd -url https://gitlab.com/cyverse/cacao-simple-http-counter`

Once the workflow has been created, copy the generated workflow ID. This is needed for the next command.

If CACAO has just launched, it may take up to an hour for Anchore to synchronize it's security feed. This may initially produce empty reports.

With a workflow ID, run `cacao get reports <workflow-ID>`
What will be generated is a list of found vulnerabilities in the form of a json object.

### Anchore Functions
Below are the main functions that are used by the cs-service to work with the Anchore-Engine.

Here are some of the important functions defined in `common.go`:
- `AnchoreAction(workFlows WorkflowResponse, CurrentWFDIndex int, action string)`
	-	This function is used when a workflow is created.
	- It will use the `anchore-cli` to add an image(s) to the Anchore-Engine for analysis.
	- The function is currently used to perform image actions `add` and `wait`.    
- `GetReport(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string)`
	-	This function is used when a user runs `cacao get reports <workflow-ID>`
	- A Nats message is sent on the subject `WorkflowDefinition.GetForUser`
		-	The message reply will then be used to get the list of Docker images used in the workflow.
	- If there is one Docker image to analyze, it will use `GetImageAnalysis`, if multiple it will use `GetAllImageAnalysis`
		- Vulnerability reports in the form of JSON objects will be generated and sent to the user.
- `GetImageAnalysis(Images []string) []ImageReport)`
	-	This function and `GetAllImageAnalysis` are used when retrieving image analyzes from Anchore via the `anchore-cli`.
	- Vulnerability reports in the form of JSON objects will be generated and sent to the user.
