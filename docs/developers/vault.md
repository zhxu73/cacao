# Hashicorp Vault

> This is a legacy document describing how vault is used by API serivce, vault access has been separated out to credential service, api service no longer has access to vault.

Vault is used by CACAO to store users' secret credentials such as git and dockerhub tokens and Kubeconfigs. Some auxillary user information is also stored in Vault.


### Table of Contents
[[_TOC_]]


## Introduction
These are the main parts of Vault used by CACAO:
  - [Key/Value Secrets Engine](https://www.vaultproject.io/docs/secrets/kv/index.html): used to store secrets in Vault's backend storage
  - [Userpass Auth Method](https://www.vaultproject.io/docs/auth/userpass.html): this allows a user to authenticate with Vault using a simple username and password. This is only used for "mock auth" when running locally
  - [JWT Auth Method](https://www.vaultproject.io/docs/auth/jwt): this authentication method is used to connect to Keycloak and verify users with their Keycloak ID Token (JWT)
  - [Policies](https://www.vaultproject.io/docs/concepts/policies.html): used to restrict access to key/value paths to specific users so only someone logged in as `username` will have access to `cacao/username/secret`


## How it works

### When a user is created, CACAO will:
1. Create a new key/value path with the user's name so they can store secrets at `cacao/username/`.
2. Create a policy allowing CRUD access to `cacao/username/`:
    ```
    path "cacao/username/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
    }

    # if user is NOT an admin, this additional policy is created to prevent
    # users from making themselves an admin:
    path "cacao/username/info" {
        capabilities = ["create", "read", "update", "delete", "list"]
        denied_parameters = {"is_admin" = [true, "true"]}
    }
    ```
3. Create JWT role that will link the previously-created Policy to users that bear a JWT with a matching `preferred_username` from the `cacao-client`
4. Create secrets on `cacao/username/secrets` and clusters on `cacao/username/clusters`


### Then, in order to use CACAO:
A user will supply a JWT from Keycloak when making any API requests. Then, this token will be passed to Vault which will then verify it against Keycloak and return a Vault token. This token is used to read information from the Vault.


### Admins
An admin Policy is created during initialization of the API Service to allow access to everything at `cacao/*`:
```
path "cacao/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}
```


## Example of Organization Within Vault
- List all secrets
    ```bash
    $ vault secrets list
    Path                     Type         Accessor              Description
    ----                     ----         --------              -----------
    cubbyhole/               cubbyhole    cubbyhole_ac570bf2    per-token private secret storage
    identity/                identity     identity_338619a2     identity store
    cacao/username/        kv           kv_28712297           Secrets for username
    secret/                  kv           kv_439e726f           key/value secret storage
    sys/                     system       system_d1ab9634       system endpoints used for control, policy and debugging
    ```

- View all KVs owned by a user
    ```bash
    $ vault kv list cacao/username
    Keys
    ----
    clusters/
    info
    secrets/
    ```
    - Notice that `clusters/` and `secrets/` have the trailing slash (`/`), but `info` does not. This is because we can further list the clusters and secrets, but cannot list the info since it contains the actual KV pairs (not additional paths). Here is an example of trying to list info:
        ```bash
        $ vault kv list cacao/username/info
        No value found at cacao/username/info/
        ```

- View user info
    ```bash
    $ vault kv get cacao/username/info
    ====== Data ======
    Key         Value
    ---         -----
    is_admin    true
    username    username
    ```

- Now, we can list all of the user's secrets/clusters
    ```bash
    $ vault kv list cacao/username/secrets
    Keys
    ----
    bshjf22k8hnelg2a9vtg
    bshjf22k8hnelg2a9vug
    ```

- Then, to see the actual contents (KV pairs) of a cluster/secret
    ```bash
    $ vault kv get cacao/username/secrets/bshjf22k8hnelg2a9vtg
    ====== Data ======
    Key         Value
    ---         -----
    type        dockerhub
    username    dockerhub_username
    value       dockerhub_token
    ```

Something very important to understand here is the nesting of these KV pairs. In these examples, `cacao/username` is the KV Mount and can only contain more paths, not any KV pairs. If we attempt to do `vault kv put cacao/username foo=bar`, we will encounter an error. However, we can write KV pairs to `cacao/username/secrets` even though that is primarily used to hold additional paths.

Here is the Vault setup represented as a directory structure:
```bash
cacao/username
├── clusters/
│   └── bshjf22k8hnelg2a9vv0
│       ├── config=YXBpVmVyc2lvbjogdjEKY2x1c3...
│       ├── default_namespace=username
│       ├── host=username.ca-run.cyverse.org
│       ├── name=cyverse-user-cluster
│       └── slug=username
├── info
│   ├── is_admin=false
│   └── username=username
└── secrets/
    ├── bshjf22k8hnelg2a9vtg
    │   ├── type=dockerhub
    │   ├── username=dockerhub_username
    │   └── value=dockerhub_token
    └── bshjf22k8hnelg2a9vug
        ├── type=git
        ├── username=git_username
        └── value=git_token
```


## Developers
This section outlines some of the additional background to Vault that will be useful for developers.

The `Client` implemented by Vault has a few internal clients that can be gotten by calling functions. The most useful ones are `Logical()` and `Sys()`. The first will be used for logical-backend API calls such as reading/writing KV pairs. The latter is used for system-related API calls such as creating Policies, mounting KV paths, and enabling authentication methods.

An unexported function, `write`, uses the Vault API's `Logical().Write()` function to write specific data, as a `map[string]interface{}`, to a specified path. Then, more specific functions use this function to write Clusters, Secrets, or user info.

When you are running Vault locally, you might sometimes want to connect to the Pod and interact with Vault directly to troubleshoot or debug some issues. Once you have a shell in the Pod (using `kubectl exec -ti`), export these environment variables:
```bash
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=root_token
```
Now you can interact with Vault using the `vault` command (see [this section above](#example-of-organization-within-vault) for some examples).
