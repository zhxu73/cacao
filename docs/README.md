# CACAO

## [Getting Started (Users)](./getting_started_users.md)
Examples for creating templates, user-action, providers, credentials, workspaces, and deployments using the CLI
## [Getting Started (Developers)](./getting_started_developers.md)
Instructions for logging in, creating users, and configuring authentication with or without Keycloak
## [Developer Documentation](./developers)
Documentation for the API Service, the Build Service, the Workflow Definition Service, Debugging, Keycloak, Logging, Phylax, and Vault
## [OpenAPI](./openapi)
the CACAO API in OpenAPI format
## [Workflow Definition](./workflowdefinition)
Reference documentation for Workflows
## [Common Tasks and Problems](./common-tasks-and-problems.md)
Information for dealing with common problems like a Workflow not building

<!---
## Extra Info

Use these commands to get important environment variables when using Kind for local development:

```bash
# with service-cluster context:
export KEYCLOAK_URL=$(kubectl get --context=kind-service-cluster po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster get service keycloak -o jsonpath='{.spec.ports[?(@.name=="keycloak")].nodePort}')
export SERVICE_GATEWAY_URL=$(kubectl --context=kind-service-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export CACAO_API=$SERVICE_GATEWAY_URL

# with user-cluster context:
export USER_GATEWAY_URL=$(kubectl --context=kind-user-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-user-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
```
-->
