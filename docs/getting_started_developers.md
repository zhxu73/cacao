# Getting Started

This document will outline how to perform a basic test of all the services to make sure they are all working as expected. This assumes you already have the CACAO CLI built and your development clusters up and running by following the [instructions here](../install/README.md).

Mode developer documentation can be found [here](./developers/README.md).

### Table of Contents
[[_TOC_]]


### Development Tools

For local development, we prefer to use [Skaffold](https://skaffold.dev/) which provides automatic rebuild and redeploy of containers in Kubernetes when it detects code changes.

We use [`Go modules`](https://golang.org/ref/mod) for dependency management.  Additionally, we are using [`imports-gen`](https://github.com/edwarnicke/imports-gen) to dynamically generate a Go file (`internal/imports/imports.go`) that contains an `import` statement for all the dependencies used in this project.  This file is copied into the build environment Dockerfile (see `base/Dockerfile`) and built with `go build`.  This has the effect of building all the dependencies for the project once and storing them in a Docker layer, which can be cached and helps to speed up subsequent builds.

> :warning: **Before you update dependencies or run any operations with `go mod` that make changes to `go.mod` or `go.sum` (like `go mod tidy`), make sure to delete `internal/imports/imports.go` and then regenerate it with `go generate ./internal/imports` after you've updates your dependencies or run your command.**

In order to pass the GitLab CI Pipeline for a Merge Request, your branch will need to pass various checks including dependency check, format check, and linting:
  - A new copy of `internal/imports/imports.go` is generated and compared against the old copy to ensure that dependencies in the build environment are in sync.  A build is also done to make sure that all the dependencies work.
  - `gofmt -s` is used to format code. The `-s` flag is used to simplify. It is recommended to make this an automatic step handled by your IDE/editor whenever you save a file
  - `golint` is used for linting


### First Steps

To use Skaffold for local development, start it up from the root of the `cacao` directory:
```bash
skaffold dev --status-check=false
```

The `--status-check=false` flag prevents Skaffold from deleting the deployment if it needs to restart a pod.  Currently, there's an ongoing issue causing pods to get restarted once or twice when you first run `skaffold dev` because they aren't able to connect to NATS right away.

### Keycloak Management
If you need to access the Keycloak administrative interface, you can retrieve the URL by running:

```bash
export KEYCLOAK_URL=$(kubectl --context=k3d-service-cluster get ingress cacao-api-ingress -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):$(kubectl --context=k3d-service-cluster get service keycloak -o jsonpath='{.spec.ports[?(@.name=="keycloak")].nodePort}')
```
(If you have changed the value of `CACAO_SERVICE_CONTEXT` in `config.yaml`, replace `k3d-service-cluster` in the command above with the correct value.)

Visit that address in your browser and login with user `admin` and password `admin`.

You can create new users and manage administrative settings here.  If you want to add a new CACAO admin user, make sure to add the `cacao_admin` role in `Role Mappings`.

### Logging in and creating a user

By default, running `./deploy.sh local` in the `install/` directory will create two users in Keycloak:
* `cacao-user` (password: `cacao-user-password`)
* `cacao-admin` (password: `cacao-admin-password`)

These 2 users will be created in Keycloak AND in CACAO (by pre-seeding the mongo db).

Log in as `cacao-admin` using the CACAO CLI:

```bash
$ cacao login --keycloak
Please provide address of Cacao API.
Format Should be: http://<hostname>:<port>	  or    <hostname>:<port> 
(Developers: this should match the value of API_DOMAIN in install/config.yaml followed by "/api", e.g. http://ca.cyverse.local/api)
Cacao API address (https://cacao.jetstream-cloud.org/api): http://ca.cyverse.local/api
Username: cacao-admin
Password:
Successfully saved id_token at /home/will/.cacao/config.json
```

### Using SimpleToken Authentication

You can disable Keycloak authentication entirely by setting the following environment variables before running the `api-service/main.go` (if you're executing the api microservice on the shell directly):

```bash
export API_AUTO_CREATE_USER=true
export API_AUTH_DRIVER=simpletoken
export API_SIMPLE_TOKEN_USERNAME="testuser"
export API_SIMPLE_TOKEN_TOKEN="whatevertextyouwanthere"
export API_SIMPLE_TOKEN_STANDALONE=true
```

Note, `API_SIMPLE_TOKEN_STANDALONE` allows the api service to run without the need for the users microservice, if you want this, and is useful for functionally testing pieces of the api. 

If you're using skaffold or using kubectl to start your microservices, you'll want to first add these environment variables in the `kubernetes-manifests/skaffold/api-service.yaml` file:

```yaml
        - name: API_AUTO_CREATE_USER
          value: "true"
        - name: API_AUTH_DRIVER
          value: "simpletoken"
        - name: API_SIMPLE_TOKEN_USERNAME
          value: "testuser"
        - name: API_SIMPLE_TOKEN_TOKEN
          value: "whatevertextyouwanthere"
        - name: API_SIMPLE_TOKEN_STANDALONE
          value: "true"
```

If all goes well, then you can use your token as the authorization header to obtain your user. For example:

```bash
curl -H "Authorization: whatevertextyouwanthere" http://localhost:8080/users/cacao-admin
# or
curl -H "Authorization: whatevertextyouwanthere" http://ca.cyverse.local/api/users/cacao-admin
```

### Next Steps
Now you have everything you need to interact with your user's resources in CACAO, specifically the K8s cluster info and secrets. Check out the [Getting Started (users) doc](./getting_started_users.md) to learn more about interacting with CACAO using the CLI.


### TL;DR
```bash
# From the root of cacao directory
cd install/
./deploy.sh local

cacao login --keycloak
```
