# Interactive-Session Service

The Interactive-Session Microservice is responsible for maintaining interactive sessions (e.g., VNC).


## Configuration

The service is configured through a set of environment variables.

| env name                        | description                                                                                                                                                 | example                              |
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `ISESSION_MASTER_SSH_KEY`       | SSH Key to access instances. Be weary of `\n` and `\n\r` characters.                                                                                        |
| `ISESSION_GUAC_SERVERS`         | List of guacamole servers in JSON string. Each item is an array of two strings, `CloudID` and `hostname` of guacamole server. Do not use single quote here. | `[["TestLocal2", "127.0.0.1:3000"]]` |
| `ISESSION_GUAC_ENCRYPTION_KEY`  | Encryption key set in guacamole servers.                                                                                                                    |
| `ISESSION_GUAC_AUTH_KEY`        | Access key set in guacamole servers.                                                                                                                        |
| `ISESSION_VNC_DEFAULT_PASSWORD` | Default VNC password.                                                                                                                                       |
| `ISESSION_GUAC_FORCE_HTTP`      | Use HTTP instead of HTTPS for guac-lite.                                                                                                                    |
| `NATS_URL`                      | -                                                                                                                                                           |                                      |
| `NATS_QUEUE_GROUP`              | -                                                                                                                                                           |
| `NATS_WILDCARD_SUBJECT`         | -                                                                                                                                                           |
| `NATS_CLIENT_ID`                | -                                                                                                                                                           |
| `NATS_MAX_RECONNECTS`           | -                                                                                                                                                           |
| `NATS_RECONNECT_WAIT`           | -                                                                                                                                                           |
| `NATS_REQUEST_TIMEOUT`          | -                                                                                                                                                           |
| `MONGODB_URL`                   | -                                                                                                                                                           |
| `MONGODB_DB_NAME`               | -                                                                                                                                                           |
