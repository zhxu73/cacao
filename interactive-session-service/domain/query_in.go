package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// QueryHandlers implements IncomingQueryPort
type QueryHandlers struct {
	Storage      ports.PersistentStoragePort
	SessionSetup ports.SessionSetupPort
}

// List retrieves active/creating interactive sessions of the user
func (h *QueryHandlers) List(actor string, emulator string) ([]types.InteractiveSession, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	return h.listInteractiveSessions(actor, emulator)
}

// Get retrieves the interactive session
func (h *QueryHandlers) Get(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
	if len(actor) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(interactiveSessionID) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: interactive session ID is empty")
	}

	return h.getInteractiveSession(actor, emulator, interactiveSessionID)
}

// GetByInstanceID retrieves the interactive session by InstanceID
func (h *QueryHandlers) GetByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
	if len(actor) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(instanceID) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance ID is empty")
	}

	return h.getInteractiveSessionByInstanceID(actor, emulator, instanceID)
}

// GetByInstanceAddress retrieves the interactive session by Instance Address
func (h *QueryHandlers) GetByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
	if len(actor) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(instanceAddress) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance address is empty")
	}

	return h.getInteractiveSessionByInstanceAddress(actor, emulator, instanceAddress)
}

// CheckPrerequisites checks if prerequisites for the interactive session are met
func (h *QueryHandlers) CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	if len(protocol) == 0 {
		return false, cacao_common_service.NewCacaoInvalidParameterError("input validation error: protocol is empty")
	}

	if len(instanceAddress) == 0 {
		return false, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance address is empty")
	}

	if len(instanceAdminUsername) == 0 {
		return false, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance admin username is empty")
	}

	return h.checkPrerequisitesForInteractiveSession(protocol, instanceAddress, instanceAdminUsername)
}

// listInteractiveSessions returns interactive sessions owned by a user
func (h *QueryHandlers) listInteractiveSessions(actor string, emulator string) ([]types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.listInteractiveSessions",
	})

	logger.Info("List interactive sessions")

	interactiveSessions, err := h.Storage.List(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return interactiveSessions, nil
}

// getInteractiveSession returns the interactive session with the ID
func (h *QueryHandlers) getInteractiveSession(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.getInteractiveSession",
	})

	logger.Infof("Get an interactive session %s", interactiveSessionID.String())

	interactiveSession, err := h.Storage.Get(actor, interactiveSessionID)
	if err != nil {
		logger.Error(err)
		return types.InteractiveSession{}, err
	}

	return interactiveSession, nil
}

// getInteractiveSessionByInstanceID returns the interactive session with the InstanceID
func (h *QueryHandlers) getInteractiveSessionByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.getInteractiveSessionByInstanceID",
	})

	logger.Infof("Get an interactive session with instanceID %s", instanceID)

	interactiveSession, err := h.Storage.GetByInstanceID(actor, instanceID)
	if err != nil {
		logger.Error(err)
		return types.InteractiveSession{}, err
	}

	return interactiveSession, nil
}

// getInteractiveSessionByInstanceAddress returns the interactive session with the Instance Address
func (h *QueryHandlers) getInteractiveSessionByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.getInteractiveSessionByInstanceAddress",
	})

	logger.Infof("Get an interactive session with instance address %s", instanceAddress)

	interactiveSession, err := h.Storage.GetByInstanceAddress(actor, instanceAddress)
	if err != nil {
		logger.Error(err)
		return types.InteractiveSession{}, err
	}

	return interactiveSession, nil
}

// checkPrerequisitesForInteractiveSession checks if prerequisites for setting up given protocol
func (h *QueryHandlers) checkPrerequisitesForInteractiveSession(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.checkPrerequisitesForInteractiveSession",
	})

	logger.Infof("Check prerequisites for setting up %s interactive session on instance address %s, user %s", protocol, instanceAddress, instanceAdminUsername)

	check, err := h.SessionSetup.CheckPrerequisites(protocol, instanceAddress, instanceAdminUsername)
	if err != nil {
		logger.Error(err)
		return false, err
	}

	return check, err
}
