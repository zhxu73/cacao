package domain

import (
	"context"
	"net"
	"net/netip"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// EventHandlers implements IncomingEventHandlers
type EventHandlers struct {
	Storage      ports.PersistentStoragePort
	SessionSetup ports.SessionSetupPort
	TimeSrc      func() time.Time
	IDGenerator  func() cacao_common.ID
}

var _ ports.IncomingEventHandlers = &EventHandlers{}

// Create creates an interactive session
func (h *EventHandlers) Create(request cacao_common_service.InteractiveSessionModel, sink ports.OutgoingEventPort) {
	cacaoErr := h.validateCreationRequest(request)
	if cacaoErr != nil {
		sink.CreateFailed(cacao_common_service.InteractiveSessionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(cacaoErr).GetBase(),
			},
			ID:                    request.ID,
			Owner:                 request.SessionActor,
			InstanceID:            request.InstanceID,
			InstanceAddress:       "",
			InstanceAdminUsername: "",
			CloudID:               "",
			Protocol:              request.Protocol,
			RedirectURL:           "",
			State:                 "",
		})
		return
	}

	if len(request.ID) == 0 {
		request.ID = h.IDGenerator()
	}

	h.createInteractiveSession(request, sink)
}

// createInteractiveSession creates an interactive session
func (h *EventHandlers) createInteractiveSession(request cacao_common_service.InteractiveSessionModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":         "interactive-session-service.domain",
		"function":        "EventHandlers.createInteractiveSession",
		"actor":           request.SessionActor,
		"emulator":        request.SessionEmulator,
		"id":              request.ID,
		"instanceAddress": request.InstanceAddress,
		"protocol":        request.Protocol,
	})
	logger.Infof("Create an interactive session %s for instanceID %s with protocol %s", request.ID.String(), request.InstanceID, request.Protocol)

	owner := request.SessionActor

	// deactivate all existing interactive sessions with the same instance id
	_, err := h.Storage.DeactivateAllByInstanceID(owner, request.InstanceID)
	if err != nil {
		logger.WithError(err).Error("fail to deactivate all sessions by instance ID")
		sink.CreateFailed(cacao_common_service.InteractiveSessionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			ID:         request.ID,
			Owner:      owner,
			InstanceID: request.InstanceID,
			Protocol:   request.Protocol,
		})
		return
	}

	// put creating state
	nowTime := h.TimeSrc()

	interactiveSession := types.InteractiveSession{
		ID:                    request.ID,
		Owner:                 owner,
		InstanceID:            request.InstanceID,
		InstanceAddress:       request.InstanceAddress,
		InstanceAdminUsername: request.InstanceAdminUsername,
		CloudID:               request.CloudID,
		Protocol:              request.Protocol,
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateCreating,
		CreatedAt:             nowTime,
		UpdatedAt:             nowTime,
	}

	err = h.Storage.Create(interactiveSession)
	if err != nil {
		logger.WithError(err).Error("fail to insert interactive session into storage")
		sink.CreateFailed(
			types.ConvertToModel(
				cacao_common_service.Session{
					SessionActor:    request.SessionActor,
					SessionEmulator: request.SessionEmulator,
					ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
				},
				types.InteractiveSession{
					ID:         interactiveSession.ID,
					Owner:      owner,
					InstanceID: interactiveSession.InstanceID,
					Protocol:   interactiveSession.Protocol,
				},
			),
		)
		return
	}
	logger.Debug("interactive session inserted into storage, about to start setup")
	sink.CreateStarted(cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID:                    request.ID,
		Owner:                 owner,
		InstanceID:            interactiveSession.InstanceID,
		InstanceAddress:       interactiveSession.InstanceAddress,
		InstanceAdminUsername: interactiveSession.InstanceAdminUsername,
		CloudID:               interactiveSession.CloudID,
		Protocol:              interactiveSession.Protocol,
		RedirectURL:           "",
		State:                 interactiveSession.State,
		CreatedAt:             interactiveSession.CreatedAt,
		UpdatedAt:             interactiveSession.UpdatedAt,
	})

	// setup
	redirectURL, err := h.SessionSetup.Setup(owner, interactiveSession.CloudID, interactiveSession.Protocol, interactiveSession.InstanceAddress, interactiveSession.InstanceAdminUsername)
	if err != nil {
		logger.WithError(err).Error("fail to update session state to failed")
		// update record, change state to failed
		nowTime = h.TimeSrc()
		interactiveSession.State = cacao_common_service.InteractiveSessionStateFailed
		interactiveSession.UpdatedAt = nowTime

		err2 := h.Storage.Update(interactiveSession, []string{"state", "updated_at"})
		if err2 != nil {
			logger.WithError(err).Error("fail to update session state to failed after setup errored out")
			// log and ignore
		}

		sink.CreateFailed(types.ConvertToModel(cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
		}, interactiveSession))
		return
	}

	// update record, change state to active
	// fill missing fields
	nowTime = h.TimeSrc()

	interactiveSession.RedirectURL = redirectURL
	interactiveSession.State = cacao_common_service.InteractiveSessionStateActive
	interactiveSession.UpdatedAt = nowTime

	err = h.Storage.Update(interactiveSession, []string{"redirect_url", "state", "updated_at"})
	if err != nil {
		logger.WithError(err).Error("fail to update session state to active")
		sink.CreateFailed(types.ConvertToModel(cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
		}, interactiveSession))
		return
	}

	sink.Created(types.ConvertToModel(cacao_common_service.CopySessionActors(request.Session), interactiveSession))
	logger.Infof("interactive session created")
}

func (h *EventHandlers) validateCreationRequest(request cacao_common_service.InteractiveSessionModel) cacao_common_service.CacaoError {
	if len(request.GetSessionActor()) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(request.CloudID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: cloud id is empty")
	}

	if len(request.InstanceID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance id is empty")
	}

	if len(request.Protocol) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: protocol is empty")
	}

	if len(request.InstanceAddress) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance address is empty")
	}
	if !isIPorHostname(request.InstanceAddress) {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: invalid instance address")
	}

	if len(request.InstanceAdminUsername) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance admin username is empty")
	}

	return nil
}

func isIPorHostname(instanceAddress string) bool {
	// for simplicity's sake, ascii only
	for _, r := range instanceAddress {
		if r > unicode.MaxASCII {
			return false
		}
	}
	ip, err := netip.ParseAddr(instanceAddress)
	if err == nil {
		if !isValidIP(ip) {
			return false
		}
		return true
	}

	// do a DNS lookup of the host/domain, since we are about to run setup on the host, the host/domain needs to exist.
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second)
	hostList, err := net.DefaultResolver.LookupHost(ctx, instanceAddress)
	cancelFunc()
	if err != nil {
		return false
	}
	for _, host := range hostList {
		ip, err := netip.ParseAddr(host)
		if err != nil {
			log.Infof("host ip fail to parse")
			return false
		}
		if !isValidIP(ip) {
			log.Infof("host ip bad")
			return false
		}
	}
	return true
}

func isValidIP(ip netip.Addr) bool {
	if !ip.IsValid() {
		return false
	}
	if ip.IsPrivate() {
		return false
	}
	if ip.IsLoopback() {
		return false
	}
	return true
}

// Deactivate deactivates an interactive session
func (h *EventHandlers) Deactivate(request cacao_common_service.InteractiveSessionModel, sink ports.OutgoingEventPort) {
	err := h.validateDeactivateRequest(request)
	if err != nil {
		sink.DeactivateFailed(cacao_common_service.InteractiveSessionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			ID:    request.ID,
			Owner: request.SessionActor,
		})
		return
	}

	h.deactivateInteractiveSession(request, sink)
}

func (h *EventHandlers) validateDeactivateRequest(request cacao_common_service.InteractiveSessionModel) cacao_common_service.CacaoError {
	if len(request.GetSessionActor()) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}
	if request.ID == "" {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: interactive session ID is empty")
	}
	return nil
}

// deactivateInteractiveSession deactivates an interactive session
func (h *EventHandlers) deactivateInteractiveSession(request cacao_common_service.InteractiveSessionModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "EventHandlers.deactivateInteractiveSession",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"id":       request.ID,
	})

	logger.Infof("Deactivate an interactive session %s", request.ID.String())

	existingInteractiveSession, err := h.Storage.Get(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch session from storage")
		sink.Deactivated(cacao_common_service.InteractiveSessionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			ID:         request.ID,
			Owner:      request.SessionActor,
			InstanceID: request.InstanceID,
		})
		return
	}

	interactiveSession := types.InteractiveSession{
		ID:          request.ID,
		Owner:       request.GetSessionActor(),
		RedirectURL: "",
		State:       cacao_common_service.InteractiveSessionStateInactive,
		CreatedAt:   time.Time{},
		UpdatedAt:   h.TimeSrc(),
	}

	err = h.Storage.Update(interactiveSession, []string{"redirect_url", "state", "updated_at"})
	if err != nil {
		logger.WithError(err).Error("fail to update session state to inactive")
		sink.Deactivated(cacao_common_service.InteractiveSessionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			ID:         request.ID,
			Owner:      request.SessionActor,
			InstanceID: existingInteractiveSession.InstanceID,
			Protocol:   existingInteractiveSession.Protocol,
		})
		return
	}

	sink.Deactivated(cacao_common_service.InteractiveSessionModel{
		Session:               cacao_common_service.CopySessionActors(request.Session),
		ID:                    interactiveSession.ID,
		Owner:                 request.SessionActor,
		InstanceID:            existingInteractiveSession.InstanceID,
		InstanceAddress:       existingInteractiveSession.InstanceAddress,
		InstanceAdminUsername: "",
		CloudID:               "",
		Protocol:              existingInteractiveSession.Protocol,
		RedirectURL:           "",
		State:                 interactiveSession.State,
		CreatedAt:             existingInteractiveSession.CreatedAt,
		UpdatedAt:             interactiveSession.UpdatedAt,
	})
	logger.Infof("interactive session deactivated")
}
