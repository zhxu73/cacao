package domain

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/interactive-session-service/ports/mocks"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
	"testing"
	"time"
)

// return a time source that will increment the timestamp it returns each time it is called.
// this is useful to distinguish between creation and update timestamp.
func getIncrementingTimeSrc(start time.Time) func() time.Time {
	const incrementStep = time.Second
	var counter = new(uint)
	return func() time.Time {
		*counter++
		return start.Add(time.Duration(*counter-1) * incrementStep)
	}
}

func TestEventHandlers_Create(t *testing.T) {
	now := time.Now()
	type fields struct {
		Storage      *portsmocks.PersistentStoragePort
		SessionSetup *portsmocks.SessionSetupPort
		TimeSrc      func() time.Time
		IDGenerator  func() common.ID
	}
	type args struct {
		request cacao_common_service.InteractiveSessionModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty request",
			fields: fields{
				Storage:      &portsmocks.PersistentStoragePort{},
				SessionSetup: &portsmocks.SessionSetupPort{},
				TimeSrc:      getIncrementingTimeSrc(now),
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("CreateFailed", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "empty InteractiveSession",
			fields: fields{
				Storage:      &portsmocks.PersistentStoragePort{},
				SessionSetup: &portsmocks.SessionSetupPort{},
				TimeSrc:      getIncrementingTimeSrc(now),
				IDGenerator:  cacao_common_service.NewInteractiveSessionID,
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{
					Session: cacao_common_service.Session{
						SessionActor: "testuser123",
					},
					ID:                    "",
					Owner:                 "",
					InstanceID:            "",
					InstanceAddress:       "",
					InstanceAdminUsername: "",
					CloudID:               "",
					Protocol:              "",
					RedirectURL:           "",
					State:                 "",
					CreatedAt:             time.Time{},
					UpdatedAt:             time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("CreateFailed", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: cloud id is empty").GetBase(),
						},
						Owner: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "normal new creation",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("DeactivateAllByInstanceID", "testuser123", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa").Return(int64(0), nil)
					storage.On("Create",
						types.InteractiveSession{
							ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
							Owner:                 "testuser123",
							InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
							InstanceAddress:       "8.8.8.8",
							InstanceAdminUsername: "InstanceAdminUsername",
							CloudID:               "provider-d17e30598850n9abikl0",
							Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
							RedirectURL:           "",
							State:                 cacao_common_service.InteractiveSessionStateCreating,
							CreatedAt:             now,
							UpdatedAt:             now,
						}).Return(nil).Once()
					storage.On("Update",
						types.InteractiveSession{
							ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
							Owner:                 "testuser123",
							InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
							InstanceAddress:       "8.8.8.8",
							InstanceAdminUsername: "InstanceAdminUsername",
							CloudID:               "provider-d17e30598850n9abikl0",
							Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
							RedirectURL:           "https://cyverse.org",
							State:                 cacao_common_service.InteractiveSessionStateActive,
							CreatedAt:             now,
							UpdatedAt:             now.Add(time.Second),
						},
						[]string{"redirect_url", "state", "updated_at"},
					).Return(nil).Once()
					return storage
				}(),
				SessionSetup: func() *portsmocks.SessionSetupPort {
					setup := &portsmocks.SessionSetupPort{}
					setup.On("Setup",
						"testuser123", "provider-d17e30598850n9abikl0", cacao_common_service.InteractiveSessionProtocolSSH, "8.8.8.8", "InstanceAdminUsername",
					).Return("https://cyverse.org", nil).Once()
					return setup
				}(),
				TimeSrc:     getIncrementingTimeSrc(now),
				IDGenerator: cacao_common_service.NewInteractiveSessionID,
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{
					Session: cacao_common_service.Session{
						SessionActor: "testuser123",
					},
					ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
					Owner:                 "testuser123",
					InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					InstanceAddress:       "8.8.8.8",
					InstanceAdminUsername: "InstanceAdminUsername",
					CloudID:               "provider-d17e30598850n9abikl0",
					Protocol:              "ssh",
					RedirectURL:           "",
					State:                 "",
					CreatedAt:             time.Time{},
					UpdatedAt:             time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("CreateStarted", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
						Owner:                 "testuser123",
						InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						InstanceAddress:       "8.8.8.8",
						InstanceAdminUsername: "InstanceAdminUsername",
						CloudID:               "provider-d17e30598850n9abikl0",
						Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
						RedirectURL:           "",
						State:                 cacao_common_service.InteractiveSessionStateCreating,
						CreatedAt:             now,
						UpdatedAt:             now,
					}).Once()
					out.On("Created", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
						Owner:                 "testuser123",
						InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						InstanceAddress:       "8.8.8.8",
						InstanceAdminUsername: "InstanceAdminUsername",
						CloudID:               "provider-d17e30598850n9abikl0",
						Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
						RedirectURL:           "https://cyverse.org",
						State:                 "active",
						CreatedAt:             now,
						UpdatedAt:             now.Add(time.Second),
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "setup failed",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("DeactivateAllByInstanceID", "testuser123", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa").Return(int64(0), nil)
					storage.On("Create",
						types.InteractiveSession{
							ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
							Owner:                 "testuser123",
							InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
							InstanceAddress:       "8.8.8.8",
							InstanceAdminUsername: "InstanceAdminUsername",
							CloudID:               "provider-d17e30598850n9abikl0",
							Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
							RedirectURL:           "",
							State:                 cacao_common_service.InteractiveSessionStateCreating,
							CreatedAt:             now,
							UpdatedAt:             now,
						}).Return(nil).Once()
					storage.On("Update",
						types.InteractiveSession{
							ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
							Owner:                 "testuser123",
							InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
							InstanceAddress:       "8.8.8.8",
							InstanceAdminUsername: "InstanceAdminUsername",
							CloudID:               "provider-d17e30598850n9abikl0",
							Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
							RedirectURL:           "",
							State:                 cacao_common_service.InteractiveSessionStateFailed,
							CreatedAt:             now,
							UpdatedAt:             now.Add(time.Second),
						},
						[]string{"state", "updated_at"},
					).Return(nil).Once()
					return storage
				}(),
				SessionSetup: func() *portsmocks.SessionSetupPort {
					setup := &portsmocks.SessionSetupPort{}
					setup.On("Setup",
						"testuser123", "provider-d17e30598850n9abikl0", cacao_common_service.InteractiveSessionProtocolSSH, "8.8.8.8", "InstanceAdminUsername",
					).Return("", fmt.Errorf("errored123")).Once()
					return setup
				}(),
				TimeSrc:     getIncrementingTimeSrc(now),
				IDGenerator: cacao_common_service.NewInteractiveSessionID,
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{
					Session: cacao_common_service.Session{
						SessionActor: "testuser123",
					},
					ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
					Owner:                 "testuser123",
					InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					InstanceAddress:       "8.8.8.8",
					InstanceAdminUsername: "InstanceAdminUsername",
					CloudID:               "provider-d17e30598850n9abikl0",
					Protocol:              "ssh",
					RedirectURL:           "",
					State:                 "",
					CreatedAt:             time.Time{},
					UpdatedAt:             time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("CreateStarted", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
						Owner:                 "testuser123",
						InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						InstanceAddress:       "8.8.8.8",
						InstanceAdminUsername: "InstanceAdminUsername",
						CloudID:               "provider-d17e30598850n9abikl0",
						Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
						RedirectURL:           "",
						State:                 cacao_common_service.InteractiveSessionStateCreating,
						CreatedAt:             now,
						UpdatedAt:             now,
					}).Once()
					out.On("CreateFailed", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoGeneralError("errored123").GetBase(),
						},
						ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
						Owner:                 "testuser123",
						InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						InstanceAddress:       "8.8.8.8",
						InstanceAdminUsername: "InstanceAdminUsername",
						CloudID:               "provider-d17e30598850n9abikl0",
						Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
						RedirectURL:           "",
						State:                 "failed",
						CreatedAt:             now,
						UpdatedAt:             now.Add(time.Second),
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &EventHandlers{
				Storage:      tt.fields.Storage,
				SessionSetup: tt.fields.SessionSetup,
				TimeSrc:      tt.fields.TimeSrc,
				IDGenerator:  tt.fields.IDGenerator,
			}
			d.Create(tt.args.request, tt.args.sink)

			tt.fields.Storage.AssertExpectations(t)
			tt.fields.SessionSetup.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Deactivate(t *testing.T) {
	now := time.Now()
	type fields struct {
		Storage      *portsmocks.PersistentStoragePort
		SessionSetup *portsmocks.SessionSetupPort
		TimeSrc      func() time.Time
		IDGenerator  func() common.ID
	}
	type args struct {
		request cacao_common_service.InteractiveSessionModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "all args empty",
			fields: fields{
				Storage:      &portsmocks.PersistentStoragePort{},
				SessionSetup: &portsmocks.SessionSetupPort{},
				TimeSrc:      getIncrementingTimeSrc(now),
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeactivateFailed", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "empty InteractiveSession",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				SessionSetup: &portsmocks.SessionSetupPort{},
				TimeSrc:      getIncrementingTimeSrc(now),
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{
					Session: cacao_common_service.Session{SessionActor: "testuser123"},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeactivateFailed", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: interactive session ID is empty").GetBase(),
						},
						Owner: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "success",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", "testuser123", common.ID("interactivesession-aaaaaaaaaaaaaaaaaaaa")).Return(types.InteractiveSession{
						ID:                    "interactivesession-aaaaaaaaaaaaaaaaaaaa",
						Owner:                 "testuser123",
						InstanceID:            "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						InstanceAddress:       "8.8.8.8",
						InstanceAdminUsername: "InstanceAdminUsername",
						CloudID:               "provider-d17e30598850n9abikl0",
						Protocol:              cacao_common_service.InteractiveSessionProtocolSSH,
						RedirectURL:           "https://cyverse.org",
						State:                 cacao_common_service.InteractiveSessionStateActive,
						CreatedAt:             now.Add(-1 * time.Hour),
						UpdatedAt:             now.Add(-1 * time.Hour),
					}, nil).Once()
					storage.On("Update",
						types.InteractiveSession{
							ID:          "interactivesession-aaaaaaaaaaaaaaaaaaaa",
							Owner:       "testuser123",
							RedirectURL: "",
							State:       cacao_common_service.InteractiveSessionStateInactive,
							UpdatedAt:   now,
						},
						[]string{"redirect_url", "state", "updated_at"},
					).Return(nil).Once()
					return storage
				}(),
				SessionSetup: &portsmocks.SessionSetupPort{},
				TimeSrc:      getIncrementingTimeSrc(now),
			},
			args: args{
				request: cacao_common_service.InteractiveSessionModel{
					Session: cacao_common_service.Session{SessionActor: "testuser123"},
					ID:      "interactivesession-aaaaaaaaaaaaaaaaaaaa",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Deactivated", cacao_common_service.InteractiveSessionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						ID:              "interactivesession-aaaaaaaaaaaaaaaaaaaa",
						Owner:           "testuser123",
						InstanceID:      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						InstanceAddress: "8.8.8.8",
						//InstanceAdminUsername: "InstanceAdminUsername",
						//CloudID:               "provider-d17e30598850n9abikl0",
						Protocol:    cacao_common_service.InteractiveSessionProtocolSSH,
						RedirectURL: "",
						State:       cacao_common_service.InteractiveSessionStateInactive,
						CreatedAt:   now.Add(-1 * time.Hour),
						UpdatedAt:   now,
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage:      tt.fields.Storage,
				SessionSetup: tt.fields.SessionSetup,
				TimeSrc:      tt.fields.TimeSrc,
				IDGenerator:  tt.fields.IDGenerator,
			}
			h.Deactivate(tt.args.request, tt.args.sink)

			tt.fields.Storage.AssertExpectations(t)
			tt.fields.SessionSetup.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func Test_isIPorHostname(t *testing.T) {
	type args struct {
		instanceAddress string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "empty_string",
			args: args{
				instanceAddress: "",
			},
			want: false,
		},
		{
			name: "127.0.0.1",
			args: args{
				instanceAddress: "127.0.0.1",
			},
			want: false,
		},
		{
			name: "0.0.0.0",
			args: args{
				instanceAddress: "0.0.0.0",
			},
			want: true,
		},
		{
			name: "1.1.1.1",
			args: args{
				instanceAddress: "1.1.1.1",
			},
			want: true,
		},
		{
			name: "1",
			args: args{
				instanceAddress: "1",
			},
			want: false,
		},
		{
			name: "1.1",
			args: args{
				instanceAddress: "1.1",
			},
			want: false,
		},
		{
			name: "1.1.1.1.1",
			args: args{
				instanceAddress: "1.1.1.1.1",
			},
			want: false,
		},
		{
			name: "localhost",
			args: args{
				instanceAddress: "localhost",
			},
			want: false,
		},
		{
			name: "cyverse.org",
			args: args{
				instanceAddress: "cyverse.org",
			},
			want: true,
		},
		{
			name: "aws.amazon.com",
			args: args{
				instanceAddress: "aws.amazon.com",
			},
			want: true,
		},
		{
			name: "org",
			args: args{
				instanceAddress: "org",
			},
			want: false,
		},
		{
			name: "com",
			args: args{
				instanceAddress: "com",
			},
			want: false,
		},
		{
			name: "aws.amazon.com/foo",
			args: args{
				instanceAddress: "aws.amazon.com/foo",
			},
			want: false,
		},
		{
			name: "https://aws.amazon.com",
			args: args{
				instanceAddress: "https://aws.amazon.com",
			},
			want: false,
		},
		{
			name: "ws://aws.amazon.com",
			args: args{
				instanceAddress: "ws://aws.amazon.com",
			},
			want: false,
		},
		{
			name: "non-ascii, utf8 emoji",
			args: args{
				instanceAddress: "\U0001F600.aws.amazon.com",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isIPorHostname(tt.args.instanceAddress); got != tt.want {
				t.Errorf("isIPorHostname() = %v, want %v", got, tt.want)
			}
		})
	}
}
