package domain

import (
	"context"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
	"sync"
	"time"
)

// Domain is the base struct for the domain service
type Domain struct {
	config       *types.Config
	Storage      ports.PersistentStoragePort
	QueryIn      ports.IncomingQueryPort
	EventIn      ports.IncomingEventPort
	SessionSetup ports.SessionSetupPort
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.config = config
	d.Storage.Init(config)
	d.SessionSetup.Init(config)
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	d.QueryIn.SetHandlers(&QueryHandlers{
		Storage:      d.Storage,
		SessionSetup: d.SessionSetup,
	})
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg)
	if err != nil {
		return err
	}

	d.EventIn.SetHandlers(&EventHandlers{
		Storage:      d.Storage,
		SessionSetup: d.SessionSetup,
		TimeSrc:      func() time.Time { return time.Now().UTC() },
		IDGenerator:  service.NewInteractiveSessionID,
	})
	wg.Add(1)
	err = d.EventIn.Start(ctx, &wg)
	if err != nil {
		return err
	}

	wg.Wait()
	return nil
}
