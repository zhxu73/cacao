package types

// GuacamoleLiteConnectionWrapper ...
type GuacamoleLiteConnectionWrapper struct {
	Connection GuacamoleLiteConnection `json:"connection"`
}

// GuacamoleLiteConnection ...
type GuacamoleLiteConnection struct {
	Type     string                `json:"type"`
	Settings GuacamoleLiteSettings `json:"settings"`
}

// GuacamoleLiteSettings ...
type GuacamoleLiteSettings struct {
	Hostname            string `json:"hostname"`
	Port                string `json:"port,omitempty"`
	Username            string `json:"username"`
	InstanceUsername    string `json:"instance_username"`
	Password            string `json:"password,omitempty"`
	PrivateKey          string `json:"private-key,omitempty"`
	Security            string `json:"security"`
	IgnoreCert          bool   `json:"ignore-cert"`
	EnableSFTP          bool   `json:"enable-sftp"`
	DisableSFTPDownload int    `json:"sftp-disable-download"`
	DisableSFTPUpload   int    `json:"sftp-disable-upload"`
	SFTPRootDirectory   string `json:"sftp-root-directory"`
}

// GuacamoleLiteTokenObject ...
type GuacamoleLiteTokenObject struct {
	Iv    string `json:"iv"`
	Value string `json:"value"`
}
