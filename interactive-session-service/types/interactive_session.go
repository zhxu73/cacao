package types

import (
	"time"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// InteractiveSession is a struct for storing interactive session information
type InteractiveSession struct {
	ID                    cacao_common.ID                                 `bson:"_id" json:"id,omitempty"`
	Owner                 string                                          `bson:"owner" json:"owner,omitempty"`
	InstanceID            string                                          `bson:"instance_id" json:"instance_id,omitempty"`
	InstanceAddress       string                                          `bson:"instance_address" json:"instance_address,omitempty"`
	InstanceAdminUsername string                                          `bson:"instance_admin_username" json:"instance_admin_username,omitempty"`
	CloudID               string                                          `bson:"cloud_id" json:"cloud_id,omitempty"`
	Protocol              cacao_common_service.InteractiveSessionProtocol `bson:"protocol" json:"protocol,omitempty"`
	RedirectURL           string                                          `bson:"redirect_url" json:"redirect_url,omitempty"`
	State                 cacao_common_service.InteractiveSessionState    `bson:"state" json:"state,omitempty"`
	CreatedAt             time.Time                                       `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt             time.Time                                       `bson:"updated_at" json:"updated_at,omitempty"`
}

// GuacamoleServer is an interface used to represent a Guacamole Server
type GuacamoleServer struct {
	CloudID string `json:"cloud_id"`
	Address string `json:"address"`
}
