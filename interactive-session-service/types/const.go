package types

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

const (
	// DefaultNatsURL is a default NATS URL
	DefaultNatsURL = "nats://nats:4222"
	// DefaultNatsMaxReconnect is a default NATS Max Reconnect Count
	DefaultNatsMaxReconnect = 6
	// DefaultNatsReconnectWait is a default NATS Reconnect Wait Time in Seconds
	DefaultNatsReconnectWait = 10
	// DefaultNatsRequestTimeout is a default NATS Request Timeout in Seconds
	DefaultNatsRequestTimeout = 10

	// DefaultNatsQueueGroup is a default NATS Queue Group
	DefaultNatsQueueGroup = "interactivesession_queue_group"
	// DefaultNatsWildcardSubject is a default NATS subject to subscribe
	DefaultNatsWildcardSubject = string(cacao_common_service.NatsSubjectInteractiveSession) + ".>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "interactivesession-service"
	// DefaultNatsClusterID is a default NATS Cluster ID
	DefaultNatsClusterID = "cacao-cluster"

	// DefaultNatsDurableName is a default NATS Durable Name
	DefaultNatsDurableName = "interactivesession_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao-interactivesession"
	// DefaultInteractiveSessionMongoDBCollectionName is a default MongoDB Collection Name
	DefaultInteractiveSessionMongoDBCollectionName = "interactivesession"
)

const (
	// DefaultChannelBufferSize is a default buffer size for a channel used between adapters and domain
	DefaultChannelBufferSize = 100
)

// InteractiveSessionCreateStartedEvent is event type that indicate the interactive session is created (persisted), but has not finished (success or failed).
//
// TODO move to cacao-common/service
const InteractiveSessionCreateStartedEvent = cacao_common.EventTypePrefix + "InteractiveSession.CreateStarted"
