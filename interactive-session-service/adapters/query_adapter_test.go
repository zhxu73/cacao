package adapters

import (
	"encoding/json"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	portsmocks "gitlab.com/cyverse/cacao/interactive-session-service/ports/mocks"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

func createTestQueryAdapter(handlers ports.IncomingQueryHandlers) *QueryAdapter {
	return &QueryAdapter{
		handlers: handlers,
		channel:  nil,
		conn:     nil,
		wg:       sync.WaitGroup{},
	}
}

func TestInteractiveSessionListQuery(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	testTime := time.Now().UTC()

	expectedResults := []types.InteractiveSession{
		{
			ID:                    "0001",
			Owner:                 testUser,
			InstanceID:            "test_instance1",
			InstanceAddress:       "127.0.0.1",
			InstanceAdminUsername: "ubuntu",
			CloudID:               "test_cloud1",
			Protocol:              "vnc",
			RedirectURL:           "",
			State:                 cacao_common_service.InteractiveSessionStateActive,
			CreatedAt:             testTime,
			UpdatedAt:             testTime,
		},
		{
			ID:                    "0002",
			Owner:                 testUser,
			InstanceID:            "test_instance2",
			InstanceAddress:       "127.0.0.1",
			InstanceAdminUsername: "ubuntu",
			CloudID:               "test_cloud2",
			Protocol:              "vnc",
			RedirectURL:           "",
			State:                 cacao_common_service.InteractiveSessionStateActive,
			CreatedAt:             testTime,
			UpdatedAt:             testTime,
		},
	}
	handlers.On("List", testUser, testUser).Return(expectedResults, nil)

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionListQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}
	var result cacao_common_service.InteractiveSessionListModel
	err = json.Unmarshal(replyDataBytes, &result)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.InteractiveSessions))

	firstInteractiveSession := result.InteractiveSessions[0]
	assert.EqualValues(t, expectedResults[0].ID, firstInteractiveSession.ID)
	assert.EqualValues(t, expectedResults[0].InstanceID, firstInteractiveSession.InstanceID)
	assert.EqualValues(t, expectedResults[0].InstanceAddress, firstInteractiveSession.InstanceAddress)
	assert.EqualValues(t, expectedResults[0].InstanceAdminUsername, firstInteractiveSession.InstanceAdminUsername)
	assert.EqualValues(t, expectedResults[0].CloudID, firstInteractiveSession.CloudID)
	assert.EqualValues(t, expectedResults[0].Protocol, firstInteractiveSession.Protocol)
	assert.EqualValues(t, expectedResults[0].State, firstInteractiveSession.State)
}

func TestInteractiveSessionListQueryFailed(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}
	handlers.On("List", testUser, testUser).Return(nil, fmt.Errorf("unable to list interactive sessions"))

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionListQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionListModel
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.InteractiveSessions)
	assert.Error(t, result.GetServiceError())
}

func TestInteractiveSessionGetQuery(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testInteractiveSessionID,
	}

	testTime := time.Now().UTC()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}
	handlers.On("Get", testUser, testUser, testInteractiveSessionID).Return(expectedResult, nil)

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionGetQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionModel
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.InstanceID, result.InstanceID)
	assert.Equal(t, expectedResult.InstanceAddress, result.InstanceAddress)
	assert.Equal(t, expectedResult.InstanceAdminUsername, result.InstanceAdminUsername)
	assert.Equal(t, expectedResult.CloudID, result.CloudID)
	assert.Equal(t, expectedResult.Protocol, result.Protocol)
	assert.Equal(t, expectedResult.State, result.State)
}

func TestInteractiveSessionGetQueryFailed(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testInteractiveSessionID,
	}
	handlers.On("Get", testUser, testUser, testInteractiveSessionID).Return(types.InteractiveSession{}, fmt.Errorf("unable to get an interactive session"))

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionGetQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())
}

func TestInteractiveSessionGetByInstanceIDQuery(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()
	testInstanceID := "test_instanceID1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceID: testInstanceID,
	}

	testTime := time.Now().UTC()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            testInstanceID,
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}
	handlers.On("GetByInstanceID", testUser, testUser, testInstanceID).Return(expectedResult, nil)

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionGetByInstanceIDQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionModel
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.InstanceID, result.InstanceID)
	assert.Equal(t, expectedResult.InstanceAddress, result.InstanceAddress)
	assert.Equal(t, expectedResult.InstanceAdminUsername, result.InstanceAdminUsername)
	assert.Equal(t, expectedResult.CloudID, result.CloudID)
	assert.Equal(t, expectedResult.Protocol, result.Protocol)
	assert.Equal(t, expectedResult.State, result.State)
}

func TestInteractiveSessionGetByInstanceIDQueryFailed(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInstanceID := "test_instanceID1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceID: testInstanceID,
	}
	handlers.On("GetByInstanceID", testUser, testUser, testInstanceID).Return(types.InteractiveSession{}, fmt.Errorf("unable to get an interactive session by InstanceID"))

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionGetByInstanceIDQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())
}

func TestInteractiveSessionGetByInstanceAddressQuery(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()
	testInstanceAddress := "127.0.0.1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress: testInstanceAddress,
	}

	testTime := time.Now().UTC()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instanceID1",
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}
	handlers.On("GetByInstanceAddress", testUser, testUser, testInstanceAddress).Return(expectedResult, nil)

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionGetByInstanceAddressQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionModel
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.InstanceID, result.InstanceID)
	assert.Equal(t, expectedResult.InstanceAddress, result.InstanceAddress)
	assert.Equal(t, expectedResult.InstanceAdminUsername, result.InstanceAdminUsername)
	assert.Equal(t, expectedResult.CloudID, result.CloudID)
	assert.Equal(t, expectedResult.Protocol, result.Protocol)
	assert.Equal(t, expectedResult.State, result.State)
}

func TestInteractiveSessionGetByInstanceAddressQueryFailed(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInstanceAddress := "127.0.0.1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress: testInstanceAddress,
	}
	handlers.On("GetByInstanceAddress", testUser, testUser, testInstanceAddress).Return(types.InteractiveSession{}, fmt.Errorf("unable to get an interactive session by Instance Address"))

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionGetByInstanceAddressQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.InteractiveSessionModel
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())
}

func TestInteractiveSessionCheckPrerequisitesQuery(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInstanceAddress := "127.0.0.1"
	testInstanceAdminUsername := "ubuntu"
	testProtocol := cacao_common_service.InteractiveSessionProtocolSSH

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: testInstanceAdminUsername,
		Protocol:              testProtocol,
	}
	handlers.On("CheckPrerequisites", testProtocol, testInstanceAddress, testInstanceAdminUsername).Return(true, nil)

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionCheckPrerequisitesQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.Session
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.NoError(t, result.GetServiceError())
}

func TestInteractiveSessionCheckPrerequisitesQueryFailed(t *testing.T) {
	handlers := &portsmocks.IncomingQueryHandlers{}
	queryAdapter := createTestQueryAdapter(handlers)

	testUser := "test_user1"
	testInstanceAddress := "127.0.0.1"
	testInstanceAdminUsername := "ubuntu"
	testProtocol := cacao_common_service.InteractiveSessionProtocolSSH

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: testInstanceAdminUsername,
		Protocol:              testProtocol,
	}
	handlers.On("CheckPrerequisites", testProtocol, testInstanceAddress, testInstanceAdminUsername).Return(false, fmt.Errorf("prerequisites are not met"))

	queryDataMarshal, err := json.Marshal(queryData)
	if !assert.NoError(t, err) {
		return
	}
	replyDataBytes, err := queryAdapter.handleInteractiveSessionCheckPrerequisitesQuery(queryDataMarshal)
	if !assert.NoError(t, err) {
		return
	}

	var result cacao_common_service.Session
	err = json.Unmarshal(replyDataBytes, &result)
	assert.NoError(t, err)
	assert.Error(t, result.GetServiceError())
}
