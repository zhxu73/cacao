package adapters

import (
	"bytes"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	ansible_options "github.com/apenella/go-ansible/pkg/options"
	ansible_playbook "github.com/apenella/go-ansible/pkg/playbook"
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// GuacamoleAdapter implements SessionSetupPort
type GuacamoleAdapter struct {
	Config *types.Config
}

// Init initialize session adapter
func (adapter *GuacamoleAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.Init",
	})

	logger.Info("initializing GuacamoleAdapter")

	adapter.Config = config
}

// Finalize finalizes mongodb adapter
func (adapter *GuacamoleAdapter) Finalize() {
}

// Setup sets up a new interactive session and returns redirect URL
func (adapter *GuacamoleAdapter) Setup(user string, cloudID string, protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.Setup",
	})

	switch protocol {
	case cacao_common_service.InteractiveSessionProtocolSSH:
		return adapter.setupSSH(user, cloudID, instanceAddress, instanceAdminUsername)
	case cacao_common_service.InteractiveSessionProtocolVNC:
		return adapter.setupVNC(user, cloudID, instanceAddress, instanceAdminUsername)
	case cacao_common_service.InteractiveSessionProtocolUnknown:
		errorMessage := "unknown interactive session protocol"
		logger.Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	default:
		errorMessage := fmt.Sprintf("unhandled interactive session protocol %s", protocol)
		logger.Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	}
}

// CheckPrerequisites checks if prerequisites are met for the protocol setup
func (adapter *GuacamoleAdapter) CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.CheckPrerequisites",
	})

	switch protocol {
	case cacao_common_service.InteractiveSessionProtocolSSH:
		// always true
		return true, nil
	case cacao_common_service.InteractiveSessionProtocolVNC:
		return adapter.checkPrerequisitesForVNC(instanceAddress, instanceAdminUsername)
	case cacao_common_service.InteractiveSessionProtocolUnknown:
		errorMessage := "unknown interactive session protocol"
		logger.Error(errorMessage)
		return false, cacao_common_service.NewCacaoGeneralError(errorMessage)
	default:
		errorMessage := fmt.Sprintf("unhandled interactive session protocol %s", protocol)
		logger.Error(errorMessage)
		return false, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}
}

// setupSSH sets up SSH public key and returns redirect URL
func (adapter *GuacamoleAdapter) setupSSH(user string, cloudID string, instanceAddress string, instanceAdminUsername string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.setupSSH",
	})

	// get SSH public key for the user from guacamole
	publicKey, guacamoleServer, err := adapter.obtainSSHPublicKey(user, cloudID)
	if err != nil {
		logger.Error(err)
		return "", err
	}

	keyPath, err := adapter.createTempPublicKey(publicKey)
	if err != nil {
		errorMessage := "failed create a temporary public key"
		logger.WithError(err).Error(errorMessage)
		return "", err
	}

	defer os.Remove(keyPath)

	// Setup SSH
	vars := map[string]interface{}{
		"USERNAME":        instanceAdminUsername,
		"PUBLIC_KEY_PATH": keyPath,
	}

	err = adapter.executeAnsibleScriptByName("ansible/playbook-ssh-setup.yaml", instanceAddress, instanceAdminUsername, vars)
	if err != nil {
		errorMessage := "failed Ansible step to setup SSH"
		logger.WithError(err).Error(errorMessage)
		return "", err
	}

	guacamoleLiteConnection := types.GuacamoleLiteConnection{
		Type: "ssh",
		Settings: types.GuacamoleLiteSettings{
			Hostname:            instanceAddress,
			Port:                "",
			Username:            user,
			InstanceUsername:    instanceAdminUsername,
			Password:            "",
			PrivateKey:          "",
			Security:            "any",
			IgnoreCert:          true,
			EnableSFTP:          true,
			DisableSFTPDownload: 0,
			DisableSFTPUpload:   0,
			SFTPRootDirectory:   "/",
		},
	}

	redirectURL, err := adapter.createRedirectURL(guacamoleServer.Address, &guacamoleLiteConnection, adapter.Config.GuacamoleEncryptionKey, adapter.Config.GuacamoleForceHTTP)
	if err != nil {
		errorMessage := "failed to create a redirect URL"
		logger.WithError(err).Error(errorMessage)
		return "", err
	}

	return redirectURL, nil
}

// setupVNC sets up VNC & Desktop.
func (adapter *GuacamoleAdapter) setupVNC(user string, cloudID string, instanceAddress string, instanceAdminUsername string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.setupVNC",
	})

	guacamoleServer, err := adapter.getGuacamoleServer(cloudID)
	if err != nil {
		logger.Error(err)
		return "", err
	}

	vars := map[string]interface{}{
		"USERNAME":            instanceAdminUsername,
		"GUACAMOLE_SERVER_IP": guacamoleServer.Address,
		"PASSWORD":            adapter.Config.VNCDefaultPassword,
	}

	// Setup VNC
	err = adapter.executeAnsibleScriptByName("ansible/playbook-vnc-setup.yaml", instanceAddress, instanceAdminUsername, vars)
	if err != nil {
		errorMessage := "failed Ansible step to setup VNC Desktop Environment"
		logger.WithError(err).Error(errorMessage)
		return "", err
	}

	guacamoleLiteConnection := types.GuacamoleLiteConnection{
		Type: "vnc",
		Settings: types.GuacamoleLiteSettings{
			Hostname:         instanceAddress,
			Username:         user,
			InstanceUsername: instanceAdminUsername,
			Password:         adapter.Config.VNCDefaultPassword,
			Port:             "5905",
			Security:         "any",
			IgnoreCert:       true,
		},
	}

	redirectURL, err := adapter.createRedirectURL(guacamoleServer.Address, &guacamoleLiteConnection, adapter.Config.GuacamoleEncryptionKey, adapter.Config.GuacamoleForceHTTP)
	if err != nil {
		errorMessage := "failed to create a redirect URL"
		logger.WithError(err).Error(errorMessage)
		return "", err
	}

	return redirectURL, nil
}

// checkPrerequisitesForVNC checks if prerequisites are met for VNC setup
func (adapter *GuacamoleAdapter) checkPrerequisitesForVNC(instanceAddress string, instanceAdminUsername string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.checkPrerequisitesForVNC",
	})

	vars := map[string]interface{}{
		"USERNAME": instanceAdminUsername,
	}

	// Check VNC Server
	err := adapter.executeAnsibleScriptByName("ansible/playbook-vnc-check.yaml", instanceAddress, instanceAdminUsername, vars)
	if err != nil {
		if strings.Contains(err.Error(), "failed to execute an ansible script") {
			// ansible error => does not satisfy prerequisites
			errorMessage := "failed to satisfy prerequisites for setting up VNC Desktop Environment"
			logger.WithError(err).Error(errorMessage)
			return false, nil
		}

		return false, err
	}

	return true, nil
}

// getGuacamoleServer returns the Guacamole server with cloudID
func (adapter *GuacamoleAdapter) getGuacamoleServer(cloudID string) (types.GuacamoleServer, error) {
	for _, server := range adapter.Config.GuacamoleServers {
		if server.CloudID == cloudID && len(server.Address) > 0 {
			return server, nil
		}
	}

	errorMessage := fmt.Sprintf("failed to find Guacamole Server with cloudID - %s", cloudID)
	return types.GuacamoleServer{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
}

// obtainSSHPublicKey returns SSH Public Key
func (adapter *GuacamoleAdapter) obtainSSHPublicKey(user string, cloudID string) (string, types.GuacamoleServer, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.obtainSSHPublicKey",
	})

	//http://auth:7LdihMHs@127.0.0.1:3000/pubfor?username=randy

	guacamoleServer, err := adapter.getGuacamoleServer(cloudID)
	if err != nil {
		logger.Error(err)
		return "", types.GuacamoleServer{}, err
	}

	protocol := "https"
	if adapter.Config.GuacamoleForceHTTP {
		protocol = "http"
	}

	requestURL := fmt.Sprintf("%s://%s/pubfor?username=%s", protocol, guacamoleServer.Address, user)

	req, err := http.NewRequest("GET", requestURL, nil)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to access guacamole server - %s", requestURL)
		logger.WithError(err).Error(errorMessage)
		return "", types.GuacamoleServer{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	// use hard-coded login username 'auth' and basic auth
	req.SetBasicAuth("auth", adapter.Config.GuacamoleLiteAuthKey)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to access guacamole server - %s", requestURL)
		logger.WithError(err).Error(errorMessage)
		return "", types.GuacamoleServer{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		errorMessage := fmt.Sprintf("unexpected response from guacamole server - %s, status: %s", requestURL, response.Status)
		logger.Error(errorMessage)
		return "", types.GuacamoleServer{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	// read the response body on the line below
	body, err := io.ReadAll(response.Body)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to read data from guacamole server - %s", requestURL)
		logger.WithError(err).Error(errorMessage)
		return "", types.GuacamoleServer{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	// Convert the body to type string
	publicKey := string(body)
	return publicKey, guacamoleServer, nil
}

func (adapter *GuacamoleAdapter) executeAnsibleScriptByName(ansibleScriptPath string, instanceAddress string, instanceAdminUsername string, extraVars map[string]interface{}) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.executeAnsibleScriptByName",
	})

	keyPath, err := adapter.createTempPrivateKeyForRootUser()
	if err != nil {
		errorMessage := "failed create a temporary private key"
		logger.WithError(err).Error(errorMessage)
		return err
	}

	defer os.Remove(keyPath)

	if extraVars == nil {
		extraVars = map[string]interface{}{}
	} else {
		extraVars["ansible_python_interpreter"] = "/usr/bin/python3"
	}

	ansibleConnectionOptions := ansible_options.AnsibleConnectionOptions{
		PrivateKey:   keyPath,
		User:         instanceAdminUsername,
		SSHExtraArgs: "\"-oStrictHostKeyChecking=no\"",
	}
	ansiblePlaybookOptions := ansible_playbook.AnsiblePlaybookOptions{
		Inventory: instanceAddress + ",",
		ExtraVars: extraVars,
	}

	cmd := &ansible_playbook.AnsiblePlaybookCmd{
		Binary:            "",
		Exec:              nil,
		Playbooks:         []string{ansibleScriptPath},
		Options:           &ansiblePlaybookOptions,
		ConnectionOptions: &ansibleConnectionOptions,
	}

	err = cmd.Run(context.TODO())
	if err != nil {
		errorMessage := fmt.Sprintf("failed to execute an ansible script - %s", ansibleScriptPath)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

func (adapter *GuacamoleAdapter) createTempPrivateKeyForRootUser() (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.createTempPrivateKeyForRootUser",
	})

	file, err := os.CreateTemp("", "cyverse-privatekey-*")
	if err != nil {
		errorMessage := "failed create a temporary private key file"
		logger.WithError(err).Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	// ssh key is really weird when converted to string
	privateKey := strings.ReplaceAll(adapter.Config.MasterSSHKey+"\n", "\\n", "\n")
	privateKeyBytes := []byte(privateKey)
	_, err = file.Write(privateKeyBytes)
	if err != nil {
		errorMessage := "failed write content to a temporary private key file"
		logger.WithError(err).Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	path := file.Name()
	file.Close()

	return path, nil
}

func (adapter *GuacamoleAdapter) createTempPublicKey(publicKey string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.createTempPublicKey",
	})

	file, err := os.CreateTemp("", "cyverse-publickey-*")
	if err != nil {
		errorMessage := "failed create a temporary public key file"
		logger.WithError(err).Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	publicKeyBytes := []byte(publicKey)
	_, err = file.Write(publicKeyBytes)
	if err != nil {
		errorMessage := "failed write content to a temporary public key file"
		logger.WithError(err).Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	path := file.Name()
	file.Close()

	return path, nil
}

func (adapter *GuacamoleAdapter) createRedirectURL(guacamoleAddress string, guacamoleLiteConnection *types.GuacamoleLiteConnection, encryptionKey []byte, forceHTTP bool) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "GuacamoleAdapter.createRedirectURL",
	})

	token, err := adapter.createGuacamoleLiteToken(guacamoleLiteConnection, encryptionKey)
	if err != nil {
		errorMessage := "failed create a guacamole lite token"
		logger.WithError(err).Error(errorMessage)
		return "", cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	protocol := "https"
	if forceHTTP {
		protocol = "http"
	}

	return fmt.Sprintf("%s://%s/?token=%s", protocol, guacamoleAddress, token), nil
}

// createGuacamoleLiteToken creates a guacamole lite token
func (adapter *GuacamoleAdapter) createGuacamoleLiteToken(guacamoleLiteConnection *types.GuacamoleLiteConnection, encryptionKey []byte) (string, error) {
	//	Taken from https://github.com/vadimpronin/guacamole-lite
	//1. Generate initialization vector for encryption (iv).
	//2. Take json object with connection settings (see example above) and encrypt it using cyper and key from clientOptions.
	//3. Base64 encode result of p.2 (put it in value)
	//4. Create another json object containing {"iv": iv, "value": value}
	//5. Base64 encode result of p.4 and this will be your token

	//	Create random bytes
	iv := adapter.createRandomBytes(16)

	// Wrap
	gConnectionWrap := types.GuacamoleLiteConnectionWrapper{
		Connection: *guacamoleLiteConnection,
	}

	//	json dump the jsonConfig
	jsonConfig, err := json.Marshal(gConnectionWrap)
	if err != nil {
		return "", cacao_common_service.NewCacaoMarshalError("failed to marshal guacamole lite connection object to JSON")
	}

	// Store the jsonConfig as a properly padded byte array
	paddedJSONText := adapter.pkcs5Padding(jsonConfig)

	// create the block
	block, err := aes.NewCipher(encryptionKey)
	if err != nil {
		return "", cacao_common_service.NewCacaoGeneralError("failed to create an AES cipher block")
	}

	// create the cypher text
	ciphertext := make([]byte, len(paddedJSONText))
	// set the cypher mode
	mode := cipher.NewCBCEncrypter(block, iv)
	// encrypt
	mode.CryptBlocks(ciphertext, paddedJSONText)

	token := types.GuacamoleLiteTokenObject{
		Iv:    b64.StdEncoding.EncodeToString(iv),
		Value: b64.StdEncoding.EncodeToString(ciphertext),
	}

	marshaledToken, err := json.Marshal(token)
	if err != nil {
		return "", cacao_common_service.NewCacaoMarshalError("failed to marshal guacamole lite token object to JSON")
	}

	return b64.StdEncoding.EncodeToString(marshaledToken), nil
}

// createRandomBytes creates random bytes
func (adapter *GuacamoleAdapter) createRandomBytes(amt int) []byte {
	token := make([]byte, amt)
	rand.Read(token)
	return token
}

// pkcs5Padding pads the encryption to ensure proper placement
func (adapter *GuacamoleAdapter) pkcs5Padding(ciphertext []byte) []byte {
	padding := aes.BlockSize - len(ciphertext)%aes.BlockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}
