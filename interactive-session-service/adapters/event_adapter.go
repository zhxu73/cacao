package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
	"sync"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	handlers ports.IncomingEventHandlers
	conn     messaging2.EventConnection
}

var _ ports.IncomingEventPort = &EventAdapter{}

// NewEventAdapter ...
func NewEventAdapter(connection messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{
		handlers: nil,
		conn:     connection,
	}
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")

	err := adapter.conn.ListenWithConcurrentWorkers(ctx, map[cacao_common.EventType]messaging2.EventHandlerFunc{
		cacao_common_service.InteractiveSessionCreateRequestedEvent: func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
			adapter.handleInteractiveSessionCreateRequest(event, writer)
			return nil
		},
		cacao_common_service.InteractiveSessionDeactivateRequestedEvent: func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
			adapter.handleInteractiveSessionDeactivateRequest(event, writer)
			return nil
		},
	}, wg, 1, 10)
	if err != nil {
		return err
	}
	return nil
}

// SetHandlers ...
func (adapter *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

func (adapter *EventAdapter) handleInteractiveSessionCreateRequest(requestCe cloudevents.Event, writer messaging2.EventResponseWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.handleInteractiveSessionCreateRequest",
		"tid":      messaging2.GetTransactionID(&requestCe),
	})

	var createRequest cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(requestCe.Data(), &createRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		return
	}
	adapter.handlers.Create(createRequest, stanOut{writer: writer})
}

func (adapter *EventAdapter) handleInteractiveSessionDeactivateRequest(requestCe cloudevents.Event, writer messaging2.EventResponseWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.handleInteractiveSessionDeactivateRequest",
		"tid":      messaging2.GetTransactionID(&requestCe),
	})

	var deactivateRequest cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(requestCe.Data(), &deactivateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		return
	}
	adapter.handlers.Deactivate(deactivateRequest, stanOut{writer: writer})
}

type stanOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventPort = stanOut{}

func (s stanOut) publish(eventBody any, eventType cacao_common.EventType) {
	logger := log.WithFields(log.Fields{
		"ceType": eventType,
	})
	ce, err := messaging2.CreateCloudEvent(eventBody, eventType, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", eventType)
		return
	}
	err = s.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", eventType)
	}
}

// Created ...
func (s stanOut) Created(created cacao_common_service.InteractiveSessionModel) {
	s.publish(created, cacao_common_service.InteractiveSessionCreatedEvent)
}

// CreateFailed ...
func (s stanOut) CreateFailed(failed cacao_common_service.InteractiveSessionModel) {
	s.publish(failed, cacao_common_service.InteractiveSessionCreateFailedEvent)
}

// CreateStarted ...
func (s stanOut) CreateStarted(started cacao_common_service.InteractiveSessionModel) {
	s.publish(started, types.InteractiveSessionCreateStartedEvent)
}

// Deactivated ...
func (s stanOut) Deactivated(deactivated cacao_common_service.InteractiveSessionModel) {
	s.publish(deactivated, cacao_common_service.InteractiveSessionDeactivatedEvent)
}

// DeactivateFailed ...
func (s stanOut) DeactivateFailed(failed cacao_common_service.InteractiveSessionModel) {
	s.publish(failed, cacao_common_service.InteractiveSessionDeactivateFailedEvent)
}
