package adapters

import (
	"github.com/kelseyhightower/envconfig"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// generate a config for unit tests
func testConfig() types.Config {
	var envConf types.EnvConfig
	var config types.Config
	err := envconfig.Process("", &envConf.Messaging)
	if err != nil {
		panic(err)
	}
	err = envconfig.Process("", &envConf.MongoDBConfig)
	if err != nil {
		panic(err)
	}
	envConf.Messaging.WildcardSubject = types.DefaultNatsWildcardSubject
	envConf.Messaging.ClientID = "foobar"
	config = types.Config{
		Messaging:                               envConf.Messaging,
		MongoDBConfig:                           envConf.MongoDBConfig,
		InteractiveSessionMongoDBCollectionName: "foobar",
	}
	return config
}

func createTestMongoAdapter() *MongoAdapter {
	var config = testConfig()

	mongoAdapter := &MongoAdapter{}
	mongoAdapter.InitMock(&config)

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.Store)

	mongoAdapter.Finalize()
}

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"

	expectedResults := []types.InteractiveSession{
		{
			ID:                    "0001",
			Owner:                 testUser,
			InstanceID:            "test_instance1",
			InstanceAddress:       "127.0.0.1",
			InstanceAdminUsername: "ubuntu",
			CloudID:               "test_cloud1",
			Protocol:              "vnc",
			RedirectURL:           "",
			State:                 cacao_common_service.InteractiveSessionStateActive,
			CreatedAt:             testTime,
			UpdatedAt:             testTime,
		},
		{
			ID:                    "0002",
			Owner:                 testUser,
			InstanceID:            "test_instance2",
			InstanceAddress:       "127.0.0.1",
			InstanceAdminUsername: "ubuntu",
			CloudID:               "test_cloud2",
			Protocol:              "vnc",
			RedirectURL:           "",
			State:                 cacao_common_service.InteractiveSessionStateActive,
			CreatedAt:             testTime,
			UpdatedAt:             testTime,
		},
	}
	err := mongoAdapter.MockList(testUser, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.List(testUser)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	mongoAdapter.Finalize()
}

func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
	}
	err := mongoAdapter.MockGet(testUser, testInteractiveSessionID, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.Get(testUser, testInteractiveSessionID)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Finalize()
}

func TestMongoAdapterGetByInstanceID(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()
	testInstanceID := "test_instance1"

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            testInstanceID,
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
	}
	err := mongoAdapter.MockGetByInstanceID(testUser, testInstanceID, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.GetByInstanceID(testUser, testInstanceID)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Finalize()
}

func TestMongoAdapterGetByInstanceAddress(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()
	testInstanceAddress := "127.0.0.1"

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instance1",
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
	}
	err := mongoAdapter.MockGetByInstanceAddress(testUser, testInstanceAddress, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.GetByInstanceAddress(testUser, testInstanceAddress)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Finalize()
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	testInteractiveSession := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}

	err := mongoAdapter.MockCreate(testInteractiveSession, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Create(testInteractiveSession)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	testExistingInteractiveSession := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}

	testUpdateInteractiveSession := types.InteractiveSession{
		ID:    testInteractiveSessionID,
		Owner: testUser,
		State: cacao_common_service.InteractiveSessionStateInactive,
	}

	err := mongoAdapter.MockUpdate(testExistingInteractiveSession, testUpdateInteractiveSession, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Update(testUpdateInteractiveSession, []string{"state"})
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDeactivateAllByInstanceID(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testInstanceID := "test_instance1"
	testUser := "testuser123"

	err := mongoAdapter.MockDeactivateAllByInstanceID(testUser, testInstanceID, 2, nil)
	assert.NoError(t, err)

	updated, err := mongoAdapter.DeactivateAllByInstanceID(testUser, testInstanceID)
	assert.NoError(t, err)
	assert.Equal(t, int64(2), updated)

	mongoAdapter.Finalize()
}
