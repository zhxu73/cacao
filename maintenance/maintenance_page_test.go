package main

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

func Test_generateMaintenancePage(t *testing.T) {
	type args struct {
		param MaintenanceTemplateParameter
	}
	tests := []struct {
		name        string
		args        args
		checkOutput func(*testing.T, string)
		wantErr     assert.ErrorAssertionFunc
	}{
		{
			name: "maintenance",
			args: args{
				param: MaintenanceTemplateParameter{
					Msg:   "foobarfoobar",
					Start: time.Date(2023, 1, 1, 1, 1, 1, 0, time.UTC),
					End:   time.Date(2024, 2, 2, 2, 2, 2, 0, time.UTC),
				},
			},
			checkOutput: func(t *testing.T, output string) {
				assert.True(t, strings.Contains(output, "CACAO is under maintenance"))
				assert.False(t, strings.Contains(output, "No Ongoing Maintenance"))
				assert.True(t, strings.Contains(output, "foobarfoobar"))
				assert.True(t, strings.Contains(output, "from 2023-01-01 01:01:01 +0000 UTC"))
				assert.True(t, strings.Contains(output, "to 2024-02-02 02:02:02 +0000 UTC"))
			},
			wantErr: assert.NoError,
		},
		{
			name: "no maintenance",
			args: args{
				param: MaintenanceTemplateParameter{
					Msg:   "foobarfoobar",
					Start: time.Time{},
					End:   time.Time{},
				},
			},
			checkOutput: func(t *testing.T, output string) {
				assert.False(t, strings.Contains(output, "CACAO is under maintenance"))
				assert.True(t, strings.Contains(output, "No Ongoing Maintenance"))
				assert.False(t, strings.Contains(output, "foobarfoobar"))
			},
			wantErr: assert.NoError,
		},
		{
			name: "maintenance missing end time",
			args: args{
				param: MaintenanceTemplateParameter{
					Msg:   "foobarfoobar",
					Start: time.Date(2023, 1, 1, 1, 1, 1, 0, time.UTC),
					End:   time.Time{},
				},
			},
			checkOutput: func(t *testing.T, output string) {
				assert.False(t, strings.Contains(output, "No Ongoing Maintenance"))
				assert.True(t, strings.Contains(output, "foobarfoobar"))
				assert.True(t, strings.Contains(output, "from 2023-01-01 01:01:01 +0000 UTC"))
				assert.False(t, strings.Contains(output, "to 2024-02-02 02:02:02 +0000 UTC"))
				assert.False(t, strings.Contains(output, "to "))
			},
			wantErr: assert.NoError,
		},
		{
			name: "maintenance missing start time",
			args: args{
				param: MaintenanceTemplateParameter{
					Msg:   "foobarfoobar",
					Start: time.Time{},
					End:   time.Date(2024, 2, 2, 2, 2, 2, 0, time.UTC),
				},
			},
			checkOutput: func(t *testing.T, output string) {
				assert.False(t, strings.Contains(output, "No Ongoing Maintenance"))
				assert.True(t, strings.Contains(output, "foobarfoobar"))
				assert.False(t, strings.Contains(output, "from 2023-01-01 01:01:01 +0000 UTC"))
				assert.False(t, strings.Contains(output, "from "))
				assert.True(t, strings.Contains(output, "to 2024-02-02 02:02:02 +0000 UTC"))
			},
			wantErr: assert.NoError,
		},
		{
			name: "maintenance with only start time, no msg",
			args: args{
				param: MaintenanceTemplateParameter{
					Msg:   "",
					Start: time.Date(2023, 1, 1, 1, 1, 1, 0, time.UTC),
					End:   time.Time{},
				},
			},
			checkOutput: func(t *testing.T, output string) {
				assert.False(t, strings.Contains(output, "No Ongoing Maintenance"))
				assert.True(t, strings.Contains(output, "from 2023-01-01 01:01:01 +0000 UTC"))
				assert.False(t, strings.Contains(output, "to 2024-02-02 02:02:02 +0000 UTC"))
				assert.False(t, strings.Contains(output, "to "))
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			err := generateMaintenancePage(w, tt.args.param)
			if !tt.wantErr(t, err, fmt.Sprintf("generateMaintenancePage(%v, %v)", w, tt.args.param)) {
				return
			}
			tt.checkOutput(t, w.String())
		})
	}
}
