package ingress

import (
	log "github.com/sirupsen/logrus"
)

// StartMaintenance update ingress to point them to maintenance service, and save the original ingress spec to configmap.
func StartMaintenance() {
	client := NewClient()
	backupExist, err := CheckBackupConfigmap(client)
	if err != nil {
		log.Panicf(err.Error())
	}
	ingressList, err := ListIngress(client)
	if err != nil {
		log.Panicf(err.Error())
	}
	if !backupExist {
		err = SaveIngressToConfigmap(client, ingressList)
		if err != nil {
			log.Panicf(err.Error())
		}
		log.Info("backup ingress to configmap")
	}
	err = UpdateIngresses(client, ingressList)
	if err != nil {
		log.Panicf(err.Error())
	}
}

// EndMaintenance restore ingress from configmap.
func EndMaintenance() {
	client := NewClient()
	if needToRestore, err := NeedToRestore(client); err == nil && !needToRestore {
		log.Info("no need to restore ingress, no relevant ingress points to maintenance service")
		return
	}
	backupExist, err := CheckBackupConfigmap(client)
	if err != nil {
		log.Panicf(err.Error())
	}
	if backupExist {
		err = RestoreIngressFromConfigmap(client)
		if err != nil {
			log.Panicf(err.Error())
		}
		log.Info("restore ingress from configmap")
	} else {
		log.Error("no backup exist, fail to restore")
	}
}
