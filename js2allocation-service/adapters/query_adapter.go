package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/ports"
	"sync"
	"time"
)

// QueryAdapter communicates to IncomingQueryHandlers
type QueryAdapter struct {
	conn     messaging2.QueryConnection
	handlers ports.IncomingQueryHandlers
}

var _ ports.IncomingQueryPort = &QueryAdapter{}

// NewQueryAdapter ...
func NewQueryAdapter(conn messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{conn: conn}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start(ctx context.Context, handlers ports.IncomingQueryHandlers, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	adapter.handlers = handlers
	err := adapter.conn.ListenWithConcurrentWorkers(ctx, map[cacao_common.QueryOp]messaging2.QueryHandlerFunc{
		service.JS2UserGetQueryOp:     adapter.handleUserGetQuery,
		service.JS2ProjectListQueryOp: adapter.handleProjectListQuery,
	}, wg, 5, 5)
	if err != nil {
		return err
	}
	return nil
}

func (adapter *QueryAdapter) handleUserGetQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.handleUserGetQuery",
	})
	reply := adapter.generateStubReplyForGetUserQuery(requestCe)
	replyCe, err2 := messaging2.CreateCloudEventWithAutoSource(reply, cacao_common.QueryOp(requestCe.Type()))
	if err2 != nil {
		logger.WithError(err2).Error("unable to marshal JS2UserModel into JSON bytes")
		return
	}
	err := writer.Write(replyCe)
	if err != nil {
		logger.WithError(err).Error("fail to write reply")
	}
}

// JS2UserGetQueryOp is no longer supported in the new accounting API for JS2, provide a stub reply for now. This will be removed later
func (adapter *QueryAdapter) generateStubReplyForGetUserQuery(requestCe cloudevents.Event) service.JS2UserModel {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.generateStubReplyForGetUserQuery",
	})
	var getRequest service.JS2UserModel
	err := json.Unmarshal(requestCe.Data(), &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		return service.JS2UserModel{
			Session: service.Session{
				ServiceError: service.NewCacaoMarshalError(errorMessage).GetBase(),
			},
		}
	}
	return service.JS2UserModel{
		Session:      service.CopySessionActors(getRequest.Session),
		Owner:        getRequest.SessionActor,
		TACCUsername: getRequest.SessionActor,
		RetrievedAt:  time.Now(),
	}
}

func (adapter *QueryAdapter) handleProjectListQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.handleProjectListQuery",
	})

	var request service.JS2ProjectListModel
	err := json.Unmarshal(requestCe.Data(), &request)
	if err != nil {
		var reply service.JS2ProjectListModel
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		reply.ServiceError = service.NewCacaoMarshalError(errorMessage).GetBase()

		replyCe, err2 := messaging2.CreateCloudEventWithAutoSource(reply, cacao_common.QueryOp(requestCe.Type()))
		if err2 != nil {
			logger.WithError(err2).Error("unable to marshal JS2ProjectListModel into JSON bytes")
			return
		}
		err = writer.Write(replyCe)
		if err != nil {
			logger.WithError(err).Error("fail to write reply")
		}
		return
	}
	reply := adapter.handlers.ListProjects(request)
	replyCe, err2 := messaging2.CreateCloudEventWithAutoSource(reply, cacao_common.QueryOp(requestCe.Type()))
	if err2 != nil {
		logger.WithError(err2).Error("unable to marshal JS2ProjectListModel into JSON bytes")
		return
	}
	err = writer.Write(replyCe)
	if err != nil {
		logger.WithError(err).Error("fail to write reply")
	}
}
