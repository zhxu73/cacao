package adapters

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"testing"
	"time"
)

func TestProviderMetadataSvc_FindJS2Provider(t *testing.T) {

	type mockObjs struct {
		mockSvcClient *mocks.MockProviderMetadataSvcClient
	}
	type args struct {
		session types.Session
	}

	var testSession = types.Session{
		TID:             messaging.NewTransactionID(),
		SessionActor:    "actor-123",
		SessionEmulator: "emulator-123",
	}

	tests := []struct {
		name     string
		mockObjs mockObjs
		args     args
		want     []service.ProviderModel
		wantErr  bool
	}{
		{
			name: "one-openstack-js2",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-d17e30598850n9abikl0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			args: args{session: testSession},
			want: []service.ProviderModel{
				{
					ID:        "provider-d17e30598850n9abikl0",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "one-openstack-js2, one-other",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-co1g2rt9885fie78ffj0",
							Name:      "",
							Type:      "other",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
							},
						},
						{
							ID:        "provider-d17e30598850n9abikl0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			args: args{session: testSession},
			want: []service.ProviderModel{
				{
					ID:        "provider-d17e30598850n9abikl0",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "js2-non-standard-port",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-co1g2rt9885fie78ffj0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024",
							},
						},
					}, nil)
					return client
				}(),
			},
			args: args{session: testSession},
			want: []service.ProviderModel{
				{
					ID:        "provider-co1g2rt9885fie78ffj0",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "js2-non-standard-port-with-path",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-co1g2rt9885fie78ffj0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024/some/path",
							},
						},
					}, nil)
					return client
				}(),
			},
			args: args{session: testSession},
			want: []service.ProviderModel{
				{
					ID:        "provider-co1g2rt9885fie78ffj0",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024/some/path",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "openstack-diff-domain",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-co1g2rt9885fie78ffj0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://not-js2.cyverse.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			args:    args{session: testSession},
			want:    []service.ProviderModel{},
			wantErr: false,
		},
		{
			name: "openstack-diff-sub-domain",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-co1g2rt9885fie78ffj0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://not-js2.jetstream-cloud.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			args:    args{session: testSession},
			want:    []service.ProviderModel{},
			wantErr: false,
		},
		{
			name: "no-js2",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{
						{
							ID:        "provider-co1g2rt9885fie78ffj0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://not-js2.cyverse.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			args:    args{session: testSession},
			want:    []service.ProviderModel{},
			wantErr: false,
		},
		{
			name: "no-provider",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return([]service.ProviderModel{}, nil)
					return client
				}(),
			},
			args:    args{session: testSession},
			want:    []service.ProviderModel{},
			wantErr: false,
		},
		{
			name: "fail-to-list-provider",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List", context.TODO(), testSession.ToServiceActor()).Return(nil, fmt.Errorf("failed"))
					return client
				}(),
			},
			args:    args{session: testSession},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := ProviderMetadataSvc{
				svcClient: tt.mockObjs.mockSvcClient,
			}
			got, err := svc.FindJS2Provider(tt.args.session)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "FindJS2Provider(%v)", tt.args.session)
			tt.mockObjs.mockSvcClient.AssertExpectations(t)
		})
	}
}
