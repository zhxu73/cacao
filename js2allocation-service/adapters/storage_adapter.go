package adapters

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  cacao_common_db.ObjectStore
	Mutex  sync.Mutex
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Finalize finalizes mongodb adapter
func (adapter *MongoAdapter) Finalize() {
	err := adapter.Store.Release()
	if err != nil {
		log.Fatal(err)
	}

	adapter.Store = nil
}

// ListProjects returns JS2 Projects owned by a user
func (adapter *MongoAdapter) ListProjects(user string) ([]types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "MongoAdapter.ListProjects",
	})

	results := []types.JS2Project{}

	err := adapter.Store.ListForUser(adapter.Config.JS2AllocationMongoDBProjectCollectionName, user, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list JS2 Projects for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockListProjects sets expected results for ListProjects
func (adapter *MongoAdapter) MockListProjects(user string, expectedJS2Projects []types.JS2Project, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("ListForUser", adapter.Config.JS2AllocationMongoDBProjectCollectionName, user).Return(expectedJS2Projects, expectedError)
	return nil
}

// InsertAllProjects inserts all JS2Projects
func (adapter *MongoAdapter) InsertAllProjects(user string, js2Projects []types.JS2Project) error {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "MongoAdapter.InsertAllProjects",
	})

	records := []interface{}{}
	for _, js2Project := range js2Projects {
		js2Project.Owner = user
		records = append(records, js2Project)
	}

	err := adapter.Store.InsertMany(adapter.Config.JS2AllocationMongoDBProjectCollectionName, records)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := "unable to insert JS2 Projects because id conflicts"
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := "unable to insert JS2 Projects"
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockInsertAllProjects sets expected results for InsertAllProjects
func (adapter *MongoAdapter) MockInsertAllProjects(user string, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("InsertMany", adapter.Config.JS2AllocationMongoDBProjectCollectionName).Return(expectedError)
	return nil
}

// DeleteAllProjects deletes all JS2 Projects
func (adapter *MongoAdapter) DeleteAllProjects(user string) error {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "MongoAdapter.DeleteAllProjects",
	})

	condition := map[string]interface{}{
		"owner": user,
	}

	// delete all
	_, err := adapter.Store.DeleteConditional(adapter.Config.JS2AllocationMongoDBProjectCollectionName, condition)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete JS2 Projects for user %s", user)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDeleteAllProjects sets expected results for DeleteAllProjects
func (adapter *MongoAdapter) MockDeleteAllProjects(user string, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	condition := map[string]interface{}{
		"owner": user,
	}

	expectedResult := 1
	if expectedError != nil {
		expectedResult = 0
	}

	mock.On("DeleteConditional", adapter.Config.JS2AllocationMongoDBProjectCollectionName, condition).Return(expectedResult, expectedError)
	return nil
}

// DeleteAndInsertAllProjects deletes and inserts all JS2Projects
func (adapter *MongoAdapter) DeleteAndInsertAllProjects(user string, js2Projects []types.JS2Project) error {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "MongoAdapter.DeleteAndInsertAllProjects",
	})

	adapter.Mutex.Lock()
	defer adapter.Mutex.Unlock()

	condition := map[string]interface{}{
		"owner": user,
	}

	// delete all
	deletedCnt, err := adapter.Store.DeleteConditional(adapter.Config.JS2AllocationMongoDBProjectCollectionName, condition)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete JS2 Projects for user %s", user)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	logger.Infof("deleted %d stale cache records", deletedCnt)

	// insert all
	records := []interface{}{}
	for _, js2Project := range js2Projects {
		js2Project.Owner = user
		records = append(records, js2Project)
	}

	err = adapter.Store.InsertMany(adapter.Config.JS2AllocationMongoDBProjectCollectionName, records)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := "unable to insert JS2 Projects because id conflicts"
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := "unable to insert JS2 Projects"
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDeleteAndInsertAllProjects sets expected results for DeleteAndInsertAllProjects
func (adapter *MongoAdapter) MockDeleteAndInsertAllProjects(user string, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	condition := map[string]interface{}{
		"owner": user,
	}

	expectedResult := 1
	if expectedError != nil {
		expectedResult = 0
	}

	mock.On("DeleteConditional", adapter.Config.JS2AllocationMongoDBProjectCollectionName, condition).Return(expectedResult, expectedError)
	mock.On("InsertMany", adapter.Config.JS2AllocationMongoDBProjectCollectionName).Return(expectedError)
	return nil
}
