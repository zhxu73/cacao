package adapters

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/js2allocation-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"testing"
)

func matchAnyNotNilContext() interface{} {
	return mock.MatchedBy(func(ctx context.Context) bool { return ctx != nil })
}

func matchCredentialIDOption(t *testing.T, credID string) interface{} {
	return mock.MatchedBy(func(credOpt providers.CredentialOption) bool {
		return assert.Equal(t, providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: credID,
		}, credOpt())
	})
}

func TestProviderOpenStackSvc_FindAccountCatalogEntry(t *testing.T) {
	type mockObjs struct {
		mockSvcClient *mocks.MockProviderOpenStackSvcClient
	}
	var testClientConstructor = func(objs mockObjs) providerOpenStackSvcClientConstructor {
		return func(providerID common.ID) providers.OpenStackProvider {
			return objs.mockSvcClient
		}
	}
	type args struct {
		session    types.Session
		providerID common.ID
		credID     string
	}
	tests := []struct {
		name     string
		mockObjs mockObjs
		args     args
		want     providers.CatalogEntry
		wantErr  bool
	}{
		{
			name: "accounting",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderOpenStackSvcClient {
					client := &mocks.MockProviderOpenStackSvcClient{}
					client.On("ListCatalog",
						matchAnyNotNilContext(),
						service.Actor{Actor: "actor-123", Emulator: "emulator-123"},
						matchCredentialIDOption(t, "cred-id-123")).Return([]providers.CatalogEntry{
						{
							Name: "accounting",
							Type: "accounting",
							Endpoints: []providers.CatalogEndpoint{
								{
									ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
									Interface: "public",
									RegionID:  "region-123",
									URL:       "https://cyverse.org",
									Region:    "region-123",
								},
							},
						},
					}, nil)
					return client
				}(),
			},
			args: args{
				session: types.Session{
					TID:             messaging.NewTransactionID(),
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
				},
				providerID: "provider-d17e30598850n9abikl0",
				credID:     "cred-id-123",
			},
			want: providers.CatalogEntry{
				Name: "accounting",
				Type: "accounting",
				Endpoints: []providers.CatalogEndpoint{
					{
						ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
						Interface: "public",
						RegionID:  "region-123",
						URL:       "https://cyverse.org",
						Region:    "region-123",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "accounting and compute",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderOpenStackSvcClient {
					client := &mocks.MockProviderOpenStackSvcClient{}
					client.On("ListCatalog", matchAnyNotNilContext(),
						service.Actor{Actor: "actor-123", Emulator: "emulator-123"},
						matchCredentialIDOption(t, "cred-id-123")).Return([]providers.CatalogEntry{
						{
							Name: "nova",
							Type: "compute",
							Endpoints: []providers.CatalogEndpoint{
								{
									ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
									Interface: "public",
									RegionID:  "region-123",
									URL:       "https://cyverse.org",
									Region:    "region-123",
								},
							},
						},
						{
							Name: "",
							Type: "accounting",
							Endpoints: []providers.CatalogEndpoint{
								{
									ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
									Interface: "public",
									RegionID:  "region-123",
									URL:       "https://cyverse.org",
									Region:    "region-123",
								},
							},
						},
					}, nil)
					return client
				}(),
			},
			args: args{
				session: types.Session{
					TID:             messaging.NewTransactionID(),
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
				},
				providerID: "provider-d17e30598850n9abikl0",
				credID:     "cred-id-123",
			},
			want: providers.CatalogEntry{
				Name: "",
				Type: "accounting",
				Endpoints: []providers.CatalogEndpoint{
					{
						ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
						Interface: "public",
						RegionID:  "region-123",
						URL:       "https://cyverse.org",
						Region:    "region-123",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "fail to list catalog",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderOpenStackSvcClient {
					client := &mocks.MockProviderOpenStackSvcClient{}
					client.On("ListCatalog", matchAnyNotNilContext(),
						service.Actor{Actor: "actor-123", Emulator: "emulator-123"},
						matchCredentialIDOption(t, "cred-id-123")).Return(nil, fmt.Errorf("failed"))
					return client
				}(),
			},
			args: args{
				session: types.Session{
					TID:             messaging.NewTransactionID(),
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
				},
				providerID: "provider-d17e30598850n9abikl0",
				credID:     "cred-id-123",
			},
			want:    providers.CatalogEntry{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := ProviderOpenStackSvc{
				clientConstructor: testClientConstructor(tt.mockObjs),
			}
			got, err := svc.FindAccountCatalogEntry(tt.args.session, tt.args.providerID, tt.args.credID)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "FindAccountCatalogEntry(%v, %v, %v)", tt.args.session, tt.args.providerID, tt.args.credID)
			tt.mockObjs.mockSvcClient.AssertExpectations(t)
		})
	}
}

func TestProviderOpenStackSvc_ObtainToken(t *testing.T) {
	type mockObjs struct {
		mockSvcClient *mocks.MockProviderOpenStackSvcClient
	}
	var testClientConstructor = func(objs mockObjs) providerOpenStackSvcClientConstructor {
		return func(providerID common.ID) providers.OpenStackProvider {
			return objs.mockSvcClient
		}
	}
	type fields struct {
		clientConstructor providerOpenStackSvcClientConstructor
	}
	type args struct {
		session    types.Session
		providerID common.ID
		credID     string
	}
	tests := []struct {
		name     string
		mockObjs mockObjs
		fields   fields
		args     args
		want     providers.Token
		wantErr  bool
	}{
		{
			name: "",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderOpenStackSvcClient {
					client := &mocks.MockProviderOpenStackSvcClient{}
					client.On("GetToken", matchAnyNotNilContext(),
						service.Actor{Actor: "actor-123", Emulator: "emulator-123"},
						matchCredentialIDOption(t, "cred-id-123")).Return(&providers.Token{
						Expires:   "",
						ID:        "token-123",
						ProjectID: "project-id-123",
						UserID:    "user-id-123",
					}, nil)
					return client
				}(),
			},
			fields: fields{},
			args: args{
				session: types.Session{
					TID:             messaging.NewTransactionID(),
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
				},
				providerID: "provider-d17e30598850n9abikl0",
				credID:     "cred-id-123",
			},
			want: providers.Token{
				Expires:   "",
				ID:        "token-123",
				ProjectID: "project-id-123",
				UserID:    "user-id-123",
			},
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := ProviderOpenStackSvc{
				clientConstructor: testClientConstructor(tt.mockObjs),
			}
			got, err := svc.ObtainToken(tt.args.session, tt.args.providerID, tt.args.credID)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "ObtainToken(%v, %v, %v)", tt.args.session, tt.args.providerID, tt.args.credID)
			tt.mockObjs.mockSvcClient.AssertExpectations(t)
		})
	}
}
