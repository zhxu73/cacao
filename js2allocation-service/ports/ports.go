package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"sync"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly WaitGroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	Start(ctx context.Context, handlers IncomingQueryHandlers, wg *sync.WaitGroup) error
}

// IncomingQueryHandlers ...
type IncomingQueryHandlers interface {
	ListProjects(listRequest service.JS2ProjectListModel) service.JS2ProjectListModel
}

// PersistentStoragePort is an interface for Storage of allocation data.
type PersistentStoragePort interface {
	Port
	ListProjects(user string) ([]types.JS2Project, error)
	InsertAllProjects(user string, js2Projects []types.JS2Project) error
	DeleteAllProjects(user string) error
	DeleteAndInsertAllProjects(user string, js2Projects []types.JS2Project) error
}

// JS2APIPort is an interface for a JS2API port.
type JS2APIPort interface {
	Port
	ListProjects(user string) ([]types.JS2Project, error)
}
