package main

import (
	"context"
	"io"
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/js2allocation-service/adapters"
	"gitlab.com/cyverse/cacao/js2allocation-service/domain"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.Override()
	config.ProcessDefaults()

	mongoAdapter := &adapters.MongoAdapter{}
	mongoAdapter.Init(&config)

	natsConn, err := config.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logCloserError(&natsConn)

	queryAdapter := adapters.NewQueryAdapter(&natsConn)

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	// start
	svc := domain.Domain{
		Storage: mongoAdapter,
		QueryIn: queryAdapter,
		JS2API:  adapters.NewJS2AccountingAPI(&natsConn, config.FilterOutInvalidCredential),
		TimeSrc: time.Now,
	}
	svc.Init(&config)
	err = svc.Start(serviceCtx)
	if err != nil {
		log.WithError(err).Panic()
		return
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
	}
}
