package types

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// JS2PI is a struct for storing JS2 PI information
// Deprecated: this is for TAS API for JS1
type JS2PI struct {
	Username  string `bson:"username" json:"username,omitempty"`
	FirstName string `bson:"firstname" json:"firstname,omitempty"`
	LastName  string `bson:"lastname" json:"lastname,omitempty"`
}

// JS2Allocation is a struct for JS2 Allocation information
// Deprecated: this is for TAS API for JS1
type JS2Allocation struct {
	ID               string    `bson:"id" json:"id,omitempty"`
	Start            time.Time `bson:"start" json:"start,omitempty"`
	End              time.Time `bson:"end" json:"end,omitempty"`
	ProjectCode      string    `bson:"project_code" json:"project_code,omitempty"`
	Resource         string    `bson:"resource" json:"resource,omitempty"`
	ComputeAllocated float64   `bson:"compute_allocated" json:"compute_allocated,omitempty"`
	StorageAllocated float64   `bson:"storage_allocated" json:"storage_allocated,omitempty"`
	ComputeUsed      float64   `bson:"compute_used" json:"compute_used,omitempty"`
}

// JS2Project is a struct for storing JS2 Project information
// Deprecated: this is for TAS API for JS1
type JS2Project struct {
	ID          common.ID       `bson:"_id" json:"id,omitempty"`
	Owner       string          `bson:"owner" json:"owner,omitempty"`
	Title       string          `bson:"title" json:"title,omitempty"`
	Description string          `bson:"description" json:"description,omitempty"`
	PI          JS2PI           `bson:"pi" json:"pi,omitempty"`
	Allocations []JS2Allocation `bson:"allocations" json:"allocations,omitempty"`
	RetrivedAt  time.Time       `bson:"retrieved_at" json:"retrieved_at,omitempty"`
}

// NewJS2Allocation is a single allocation from js2 accounting API.
// https://gitlab.com/jetstream-cloud/accounting-api/-/blob/main/openapi.yml
// TODO rename (NewJS2Allocation => JS2Allocation) this after removing the TAS api stuff
type NewJS2Allocation struct {
	Title                 string    `json:"title"`
	Description           string    `json:"description"`
	Abstract              string    `json:"abstract"`
	Resource              string    `json:"resource"`
	ServiceUnitsAllocated float64   `json:"service_units_allocated"`
	ServiceUnitsUsed      float64   `json:"service_units_used"`
	StartDate             time.Time `json:"start_date"`
	EndDate               time.Time `json:"end_date"`
	Active                int       `json:"active"`
}

// UnmarshalJSON unmarshal JSON into NewJS2Allocation.
// This use a staging struct to convert time and service units.
func (j *NewJS2Allocation) UnmarshalJSON(bytes []byte) error {
	var staging struct {
		Abstract              string   `json:"abstract"`
		Active                *int     `json:"active"`
		Description           string   `json:"description"`
		StartDate             string   `json:"start_date"` // use string for date
		EndDate               string   `json:"end_date"`   // use string for date
		Resource              string   `json:"resource"`
		ServiceUnitsAllocated *float64 `json:"service_units_allocated"`
		ServiceUnitsUsed      *float64 `json:"service_units_used"`
		Title                 string   `json:"title"`
	}

	err := json.Unmarshal(bytes, &staging)
	if err != nil {
		return err
	}
	if staging.Active != nil && *staging.Active != 0 && *staging.Active != 1 {
		return fmt.Errorf("'active' is %d", *staging.Active)
	}
	var active int
	if staging.Active != nil {
		active = *staging.Active
	} else {
		// if staging.Active is nil, treat it as active, and rely on the end-date to determine if the allocation is valid
		active = 1
	}
	*j = NewJS2Allocation{
		Abstract:              staging.Abstract,
		Active:                active,
		Description:           staging.Description,
		StartDate:             time.Time{},
		EndDate:               time.Time{},
		Resource:              staging.Resource,
		ServiceUnitsAllocated: 0,
		ServiceUnitsUsed:      0,
		Title:                 staging.Title,
	}
	j.setServiceUnits(staging.ServiceUnitsAllocated, staging.ServiceUnitsUsed)
	return j.setDateRange(staging.StartDate, staging.EndDate)
}

// IsInactiveOrExpired checks if allocation is inactive or expired.
func (j *NewJS2Allocation) IsInactiveOrExpired(now time.Time) bool {
	if j.Active == 0 {
		return true
	}
	if j.EndDate.Before(now) {
		return true
	}
	return false
}

func (j *NewJS2Allocation) setServiceUnits(allocated, used *float64) {
	// handling null and negative for SU (Service Unit)
	if allocated != nil {
		j.ServiceUnitsAllocated = *allocated
	}
	if j.ServiceUnitsAllocated < 0 {
		j.ServiceUnitsAllocated = 0
	}
	if used != nil {
		j.ServiceUnitsUsed = *used
	}
	if j.ServiceUnitsUsed < 0 {
		j.ServiceUnitsUsed = 0
	}
}

// the date returned by API is in the format of 2022-04-15, which is not parse-able, so append a timestamp before parsing it.
func (j *NewJS2Allocation) setDateRange(startDate, endDate string) error {
	parsedStartDate, err := time.Parse(time.RFC3339, startDate+"T00:00:00+00:00") // +00 UTC, start at beginning of day
	if err != nil {
		return err
	}
	parsedEndDate, err := time.Parse(time.RFC3339, endDate+"T23:59:59+00:00") // +00 UTC, end at end of day
	if err != nil {
		return err
	}
	// explicitly set to UTC, so that the timezone info is consistently regardless of host time zone
	j.StartDate = parsedStartDate.UTC()
	j.EndDate = parsedEndDate.UTC()
	return nil
}
