package types

import (
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	// NATS
	Messaging messaging2.NatsStanMsgConfig

	// MongoDB
	MongoDBConfig                             cacao_common_db.MongoDBConfig
	JS2AllocationMongoDBUserCollectionName    string
	JS2AllocationMongoDBProjectCollectionName string

	// Deprecated: this service no longer uses TAS api.
	TASApiUsername string `envconfig:"TACC_API_USER"`
	// Deprecated: this service no longer uses TAS api.
	TASApiPassword string `envconfig:"TACC_API_PASS"`

	// Cache
	UserCacheExpirationTime    time.Duration
	ProjectCacheExpirationTime time.Duration

	FilterOutInvalidCredential bool `envconfig:"FILTER_OUT_INVALID_CREDENTIAL"`
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	// NATS
	if c.Messaging.URL == "" {
		c.Messaging.URL = DefaultNatsURL
	}

	if c.Messaging.ClientID == "" {
		c.Messaging.ClientID = DefaultNatsClientID
	}

	if c.Messaging.MaxReconnects <= 0 {
		c.Messaging.MaxReconnects = DefaultNatsMaxReconnect
	}

	if c.Messaging.ReconnectWait <= 0 {
		c.Messaging.ReconnectWait = DefaultNatsReconnectWait
	}

	if c.Messaging.RequestTimeout <= 0 {
		c.Messaging.RequestTimeout = DefaultNatsRequestTimeout
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}

	if c.JS2AllocationMongoDBUserCollectionName == "" {
		c.JS2AllocationMongoDBUserCollectionName = DefaultJS2AllocationMongoDBUserCollectionName
	}

	if c.JS2AllocationMongoDBProjectCollectionName == "" {
		c.JS2AllocationMongoDBProjectCollectionName = DefaultJS2AllocationMongoDBProjectCollectionName
	}

	// Cache
	if c.UserCacheExpirationTime == 0 {
		c.UserCacheExpirationTime = DefaultUserCacheExpirationTime
	}

	if c.ProjectCacheExpirationTime == 0 {
		c.ProjectCacheExpirationTime = DefaultProjectCacheExpirationTime
	}
}

// Override ...
func (c *Config) Override() {
	c.Messaging.QueueGroup = DefaultNatsQueueGroup
	c.Messaging.WildcardSubject = DefaultNatsWildcardSubject
}
