package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Session contains info for a request session
type Session struct {
	TID             common.TransactionID
	SessionActor    string `json:"actor"`
	SessionEmulator string `json:"emulator,omitempty"`
}

// ToServiceActor ...
func (s Session) ToServiceActor() service.Actor {
	return service.Actor{
		Actor:    s.SessionActor,
		Emulator: s.SessionEmulator,
	}
}
