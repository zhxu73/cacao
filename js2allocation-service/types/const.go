package types

import (
	"time"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

const (
	// DefaultNatsURL is a default NATS URL
	DefaultNatsURL = "nats://nats:4222"
	// DefaultNatsMaxReconnect is a default NATS Max Reconnect Count
	DefaultNatsMaxReconnect = 6
	// DefaultNatsReconnectWait is a default NATS Reconnect Wait Time in Seconds
	DefaultNatsReconnectWait = 10
	// DefaultNatsRequestTimeout is a default NATS Request Timeout in Seconds
	DefaultNatsRequestTimeout = 30

	// DefaultNatsQueueGroup is a default NATS Queue Group
	DefaultNatsQueueGroup = "js2allocation_queue_group"
	// DefaultNatsWildcardSubject is a default NATS subject to subscribe
	DefaultNatsWildcardSubject = string(cacao_common_service.NatsSubjectJS2Allocation) + ".>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "js2allocation-service"
	// DefaultNatsClusterID is a default NATS Cluster ID
	DefaultNatsClusterID = "cacao-cluster"

	// DefaultNatsDurableName is a default NATS Durable Name
	DefaultNatsDurableName = "js2allocation_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao-js2allocation"
	// DefaultJS2AllocationMongoDBUserCollectionName is a default MongoDB Collection Name for User
	DefaultJS2AllocationMongoDBUserCollectionName = "js2allocation_user"
	// DefaultJS2AllocationMongoDBProjectCollectionName is a default MongoDB Collection Name for Project
	DefaultJS2AllocationMongoDBProjectCollectionName = "js2allocation_project"
	// DefaultUserCacheExpirationTime is a default cache expiration time for JS2 User cache
	DefaultUserCacheExpirationTime = 1 * time.Hour
	// DefaultProjectCacheExpirationTime is a default cache expiration time for JS2 Project cache
	DefaultProjectCacheExpirationTime = 1 * time.Hour
)

const (
	// DefaultChannelBufferSize is a default buffer size for a channel used between adapters and domain
	DefaultChannelBufferSize = 100
)
