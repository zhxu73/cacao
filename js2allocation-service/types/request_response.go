package types

import "gitlab.com/cyverse/cacao-common/common"

// JS2AllocationChannelRequest is a request struct used between adapters and domain
type JS2AllocationChannelRequest struct {
	Actor     string
	Emulator  string
	Operation common.QueryOp
	NoCache   bool
	Response  chan<- JS2AllocationChannelReply // channel created by adapter(or other caller) and will receive the response
}

// JS2AllocationChannelReply is a response struct used between adapters and domain
type JS2AllocationChannelReply struct {
	Data  []JS2Project
	Error error
}
