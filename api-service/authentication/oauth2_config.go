package authentication

import (
	"context"
	"net/http"

	"golang.org/x/oauth2"
)

// OAuth2Config specifies the interface used by oauth2.Config so that we can mock it.
type OAuth2Config interface {
	AuthCodeURL(state string, opts ...oauth2.AuthCodeOption) string
	Client(ctx context.Context, t *oauth2.Token) *http.Client
	Exchange(ctx context.Context, code string, opts ...oauth2.AuthCodeOption) (*oauth2.Token, error)
	PasswordCredentialsToken(ctx context.Context, username, password string) (*oauth2.Token, error)
	TokenSource(ctx context.Context, t *oauth2.Token) oauth2.TokenSource
}
