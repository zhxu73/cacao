package authentication

import (
	"errors"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"

	"github.com/gorilla/mux"
)

// SimpleTokenAuth authentication is essentially a way to pass a token for authentication and authorization
// This method should only be used for development or demo purposes
// SimpleTokenAuth authentication also creates the initial admin user per global config
type SimpleTokenAuth struct {
	BaseNatsDriver

	// tokenStore is a map where the key is the token and values are the usernames
	tokenStore map[string]string

	// isStandaloneMode if true, will not require any dependent services to authenticate or authorize
	isStandaloneMode bool
}

// NewSimpleTokenAuth creates a new SimpleTokenAuth, given a configuration
func NewSimpleTokenAuth(c config.Config, userServiceClient service.UserClient) *SimpleTokenAuth {
	log.Trace("NewSimpleTokenAuth() start")
	var simpletoken SimpleTokenAuth = SimpleTokenAuth{
		BaseNatsDriver: BaseNatsDriver{
			context:        c.AppContext,
			serviceClient:  userServiceClient,
			autoCreateUser: c.AutoCreateUser,
		},
		isStandaloneMode: c.SimpleTokenStandalone,
	}

	simpletoken.tokenStore = make(map[string]string)

	if c.SimpleTokenUsername != "" {
		log.Trace("NewSimpleTokenAuth(): adding user '" + c.SimpleTokenUsername + "' to tokenStore with token '" + c.SimpleTokenToken + "'")
		simpletoken.tokenStore[c.SimpleTokenToken] = c.SimpleTokenUsername
	}

	return &simpletoken
}

// AddRoutes adds additional routes, per AuthDriver
func (*SimpleTokenAuth) AddRoutes(router *mux.Router) {
	log.Trace("SimpleTokenAuth.AddRoutes() start")
	// no need to add additional routes
}

// Authenticate method authenticates and returns the user per the AuthDriver interface
func (st *SimpleTokenAuth) Authenticate(header http.Header) (user *service.UserModel, err error) {
	accessToken := header.Get("Authorization")
	log.Trace("SimpleTokenAuth.Authenticate(): start with authorization token, '" + accessToken + "'")
	user = nil
	err = nil

	// If using userpass, get username and return
	username := st.tokenStore[accessToken]
	if accessToken == "" || username == "" {
		err = errors.New("no authorization token was provided or token provided was not found in token store")
		return nil, err
	}

	log.Trace("SimpleTokenAuth.Authenticate: found username '" + username + "'")

	email := username + "@simpletoken-fakedomain.org"
	firstname := "unknown-firstname"
	lastname := "unknown-lastname"

	if st.isStandaloneMode {
		user = &service.UserModel{
			Username:     username,
			PrimaryEmail: email,
			FirstName:    firstname,
			LastName:     lastname,
			IsAdmin:      true,
		}
	} else {
		user, err = st.getOrCreateUser(username, email, firstname, lastname, true)
		if err != nil {
			log.Trace("SimpleTokenAuth.Authenticate:\tfailed to get user")
			return
		}
	}

	log.Trace("SimpleTokenAuth.Authenticate:\t checking authorization")
	var isAuthorized bool
	if isAuthorized, err = st.authorize(user); !isAuthorized || err != nil {
		log.Trace("SimpleTokenAuth.Authenticate: user is unauthorized")
	} else {
		log.Trace("SimpleTokenAuth.Authenticate: user is authorized")
	}

	return user, err
}

// authorize is a key point to authorize users, after authentication and we find the user in the users microservice
// NB: currently, this is a simple method, but other AuthDrivers may support a more robust
// pluggable mechanism where it would be easy to add custom authorization logic based on configuration
func (st *SimpleTokenAuth) authorize(user *service.UserModel) (bool, error) {
	log.Trace("SimpleTokenAuth.authorize(" + user.Username + "): start")

	// let's assume authorized
	isauthorized := true
	var err error

	if !user.DisabledAt.IsZero() && user.DisabledAt.Before(time.Now()) {
		log.Trace("SimpleTokenAuth.authorize: user disabled at: " + user.DisabledAt.String())
		isauthorized = false
		err = errors.New("User '" + user.Username + "' and cannot login")
	}

	return isauthorized, err
}
