// Code generated by mockery v2.22.1. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	service "gitlab.com/cyverse/cacao-common/service"
)

// UserResolver is an autogenerated mock type for the UserResolver type
type UserResolver struct {
	mock.Mock
}

// GetOrCreateUser provides a mock function with given fields: username, email, firstname, lastname, isAdmin
func (_m *UserResolver) GetOrCreateUser(username string, email string, firstname string, lastname string, isAdmin bool) (*service.UserModel, error) {
	ret := _m.Called(username, email, firstname, lastname, isAdmin)

	var r0 *service.UserModel
	var r1 error
	if rf, ok := ret.Get(0).(func(string, string, string, string, bool) (*service.UserModel, error)); ok {
		return rf(username, email, firstname, lastname, isAdmin)
	}
	if rf, ok := ret.Get(0).(func(string, string, string, string, bool) *service.UserModel); ok {
		r0 = rf(username, email, firstname, lastname, isAdmin)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*service.UserModel)
		}
	}

	if rf, ok := ret.Get(1).(func(string, string, string, string, bool) error); ok {
		r1 = rf(username, email, firstname, lastname, isAdmin)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewUserResolver interface {
	mock.TestingT
	Cleanup(func())
}

// NewUserResolver creates a new instance of UserResolver. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewUserResolver(t mockConstructorTestingTNewUserResolver) *UserResolver {
	mock := &UserResolver{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
