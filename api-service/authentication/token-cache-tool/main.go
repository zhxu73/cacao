package main

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"os"
)

func main() {
	envconfig.MustProcess(constants.ConfigVarPrefix, &config.GlobalConfig)
	cache := authentication.NewRedisTokenCache(config.GlobalConfig)
	if len(os.Args) < 2 {
		_, _ = fmt.Fprintln(os.Stderr, "missing username")
		return
	}
	err := authentication.InvalidateRedisCacheForUser(cache, os.Args[1])
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}

// CacheInvalidate ...
type CacheInvalidate interface {
	InvalidateUser(username string)
}
