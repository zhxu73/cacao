package authentication

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"io"
	"net/http"
	"net/url"
	"strings"

	fp "github.com/amonsat/fullname_parser"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
)

// UserProfile contains information about an authorized user.
type UserProfile struct {
	Username   string
	FirstName  string
	LastName   string
	Email      string
	Attributes map[string]interface{}
}

// AccessTokenVerifier is the primary interface used for verifying that an access token is legitimate.
// Any implementations of AccessTokenVerifier should do 2 things:
// 1. verify that an access token is valid.
// 2. obtain enough information about user to create a UserProfile object.
type AccessTokenVerifier interface {

	// Verify accepts a single access token and validates it. If the token is valid, user profile information will be
	// returned. Otherwise, an error will be returned.
	Verify(accessToken string) (*UserProfile, error)
}

// UserProfileExtractor extracts UserProfile from OAuth2 access token or claims.
type UserProfileExtractor interface {
	ExtractUserProfile(accessToken string, claims map[string]interface{}) (*UserProfile, error)
}

// RFC7662AccessTokenVerifier validates OAuth2 access tokens according to RFC 7662.
type RFC7662AccessTokenVerifier struct {
	profileExtractor      UserProfileExtractor
	tokenIntrospectionURL string
}

// NewRFC7662AccessTokenVerifier returns a new access token verifier. Note that the claim names used to obtain
// attribute for the user profile are currently hard-coded. They're included in the struct itself in order to
// make it easy to customize them later if we need to.
func NewRFC7662AccessTokenVerifier(introspectionURL string, profileExtractor UserProfileExtractor) *RFC7662AccessTokenVerifier {
	return &RFC7662AccessTokenVerifier{
		profileExtractor:      profileExtractor,
		tokenIntrospectionURL: introspectionURL,
	}
}

// extractAttribute takes a map form a parsed token introspection response along with a list of claim names. It
// returns the value of the first claim that exists in the map and whose value is a string or something that can be
// converted to a string easily.
func (v *RFC7662AccessTokenVerifier) extractAttribute(claims map[string]interface{}, claimNames []string) string {
	for _, claimName := range claimNames {
		rawClaimValue, ok := claims[claimName]
		if ok {
			switch str := rawClaimValue.(type) {
			case string:
				return str
			case fmt.Stringer:
				return str.String()
			}
		}
	}
	return ""
}

// Verify verifies that an access token is valid using the method described in RFC 7662.
func (v *RFC7662AccessTokenVerifier) Verify(accessToken string) (*UserProfile, error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "RFC7662AccessTokenVerifier.Verify",
		"url":      v.tokenIntrospectionURL,
	})
	logger.Trace("start")

	// Build the request body.
	values := &url.Values{}
	values.Set("token", accessToken)
	values.Set("include", "identity_set,identity_set_detail") // optimization for Globus, this will instruct Globus to return more info in claims
	body := values.Encode()

	// Send the request.
	resp, err := http.Post(v.tokenIntrospectionURL, "application/x-www-form-urlencoded", strings.NewReader(body))
	if err != nil {
		logger.WithError(err).Error("token introspection failed")
		return nil, err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		msg := fmt.Sprintf("unexpected status code in token introspection response: %d", resp.StatusCode)
		logger.Error(msg)
		return nil, fmt.Errorf(msg)
	}

	// Read and parse the response body.
	responseBodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		msg := "unable to read the token introspection response body"
		logger.WithError(err).Error(msg)
		return nil, err
	}
	var claims map[string]interface{}
	err = json.Unmarshal(responseBodyBytes, &claims)
	if err != nil {
		msg := "unable to parse the token introspection response body"
		logger.WithError(err).WithFields(log.Fields{"response": responseBodyBytes}).Error(msg)
		return nil, err
	}

	// If the token is inactive then the user needs to reauthorize.
	active, ok := claims["active"]
	if !ok {
		msg := "the token introspection response did not contain the active flag"
		logger.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	switch isActive := active.(type) {
	case bool:
		if !isActive {
			msg := "invalid or expired access token"
			logger.WithField("claims", claims).Debug(msg)
			return nil, service.NewCacaoUnauthorizedError(msg)
		}
	default:
		msg := "invalid value for active flag"
		logger.WithField("claims", claims).Debug(msg)
		return nil, fmt.Errorf(msg)
	}

	profile, err := v.profileExtractor.ExtractUserProfile(accessToken, claims)
	if err != nil {
		logger.WithField("claims", claims).WithError(err).Error("fail to extract user profile from access token & claims")
		return nil, err
	}
	return profile, nil
}

// globusProfileExtractor parses OAuth2 claims returned from Globus into UserProfile
type globusProfileExtractor struct {
	requiredIdentityProvider string // if empty, use the primary identity
	usernameClaimNames       []string
	fullNameClaimNames       []string
	firstNameClaimNames      []string
	lastNameClaimNames       []string
	emailClaimsNames         []string
}

// NewGlobusProfileExtractor ...
func NewGlobusProfileExtractor(requiredIdentityProvider string) UserProfileExtractor {
	return globusProfileExtractor{
		requiredIdentityProvider: requiredIdentityProvider,
		usernameClaimNames:       []string{"username", "preferred_username"},
		fullNameClaimNames:       []string{"name"},
		firstNameClaimNames:      []string{"given_name"},
		lastNameClaimNames:       []string{"last_name"},
		emailClaimsNames:         []string{"email"},
	}
}

// ExtractUserProfile OAuth2 claims into UserProfile.
// For Globus, accessToken is not necessary to obtain UserProfile, since it does not need to call userinfo endpoint.
// Because we include "identity_set,identity_set_detail" when doing token introspection, the claims should contain what we need for UserProfile.
func (e globusProfileExtractor) ExtractUserProfile(accessToken string, claims map[string]interface{}) (*UserProfile, error) {
	// The username has to be present in the response for authorization to succeed.
	username, err := e.extractUsernameFromClaimWithDetail(claims, e.requiredIdentityProvider)
	if err != nil {
		log.WithField("claims", claims).WithError(err).Error("fail to extract username from claims")
		return nil, err
	}

	// Get the name fields.
	firstName := extractAttribute(claims, e.firstNameClaimNames)
	lastName := extractAttribute(claims, e.lastNameClaimNames)

	// If the first and last names aren't provided separately, try to guess them from the full name. I'm very
	// skeptical that we can always accurately determine the first and last names, so we may want to remove this
	// code if it doesn't work as intended.
	if firstName == "" && lastName == "" {
		name := fp.ParseFullname(extractAttribute(claims, e.fullNameClaimNames))
		firstName = name.First
		lastName = name.Last
	}

	// Build the user profile from the response.
	profile := &UserProfile{
		Username:   username,
		FirstName:  firstName,
		LastName:   lastName,
		Email:      extractAttribute(claims, e.emailClaimsNames),
		Attributes: claims,
	}

	return profile, nil
}

// globusIdentityDetail is detail of an identity provided in the token introspection response when the detail is requested.
// Note: this is a Globus Auth-specific extension.
// Example from https://docs.globus.org/api/auth/reference/#token-introspect
type globusIdentityDetail struct {
	Email                       string `json:"email" mapstructure:"email"`
	IdentityProvider            string `json:"identity_provider" mapstructure:"identity_provider"` // typically a UUID
	IdentityProviderDisplayName string `json:"identity_provider_display_name" mapstructure:"identity_provider_display_name"`
	LastAuthentication          int    `json:"last_authentication" mapstructure:"last_authentication"`
	Name                        string `json:"name" mapstructure:"name"`
	Organization                string `json:"organization" mapstructure:"organization"`
	Sub                         string `json:"sub" mapstructure:"sub"`
	Username                    string `json:"username" mapstructure:"username"`
}

func (e globusProfileExtractor) extractUsernameFromClaimWithDetail(claims map[string]interface{}, requiredIdentityProvider string) (string, error) {
	if requiredIdentityProvider == "" {
		return e.extractDefaultUsername(claims)
	}
	return e.extractUsernameForRequiredIdentityProvider(claims, requiredIdentityProvider)
}

// extract the username for the identity from a specific identity provider.
func (e globusProfileExtractor) extractUsernameForRequiredIdentityProvider(claims map[string]interface{}, requiredIdentityProvider string) (string, error) {
	idSetDetailRaw, ok := claims["identity_set_detail"]
	if !ok {
		return "", fmt.Errorf("identity_set_detail is missing from token introspection response")
	}

	var idDetailList []globusIdentityDetail
	err := mapstructure.Decode(idSetDetailRaw, &idDetailList)
	if err != nil {
		return "", fmt.Errorf("identity_set_detail from token introspection response is ill-formed, %w", err)
	}
	for _, idDetail := range idDetailList {
		if idDetail.IdentityProvider == requiredIdentityProvider {
			if idDetail.Username == "" {
				return "", fmt.Errorf("identity from the desired identity provider has an empty username")
			}
			return idDetail.Username, nil
		}
	}
	return "", fmt.Errorf("user does not have an identity from a required identity provider")
}

// this just extract the username from the root level of the claim, this is generally username of the primary identity.
func (e globusProfileExtractor) extractDefaultUsername(claims map[string]interface{}) (string, error) {
	username := extractAttribute(claims, e.usernameClaimNames)
	if username == "" {
		msg := "no username claim found in the token introspection response"
		return "", fmt.Errorf(msg)
	}
	return username, nil
}

func extractAttribute(claims map[string]interface{}, claimNames []string) string {
	for _, claimName := range claimNames {
		rawClaimValue, ok := claims[claimName]
		if ok {
			switch str := rawClaimValue.(type) {
			case string:
				return str
			case fmt.Stringer:
				return str.String()
			}
		}
	}
	return ""
}

// ciLogonProfileExtractor extracts UserProfile by calling the userinfo endpoint to get additional information about user.
type ciLogonProfileExtractor struct {
	requiredIdentityProviderID string // if empty, skip checking the identity provider
}

// NewCILogonProfileExtractor ...
func NewCILogonProfileExtractor(requiredIdentityProviderID string) UserProfileExtractor {
	return ciLogonProfileExtractor{
		requiredIdentityProviderID: requiredIdentityProviderID,
	}
}

// ciLogonClaims is the claims from token introspection, at https://cilogon.org/oauth2/introspect.
// this is for reference only, only to show what's in claims, not actually used.
type ciLogonClaims struct {
	Active     bool     `mapstructure:"active"`
	ClientID   string   `mapstructure:"client_id"`
	Expiration int      `mapstructure:"exp"`
	IssuerAt   int      `mapstructure:"iat"`
	Issuer     string   `mapstructure:"iss"`
	JWTID      string   `mapstructure:"jti"`
	NotBefore  int      `mapstructure:"nbf"`
	Scope      []string `mapstructure:"scope"`
	TokenType  string   `mapstructure:"token_type"`
	// The username claim is CILogon's internal username, not the username from identity provider, e.g. http://cilogon.org/serverE/users/XXXXX
	Username string `mapstructure:"username"`
}

// ciLogonUserInfo is the structure for response from https://cilogon.org/oauth2/userinfo
type ciLogonUserInfo struct {
	Subject              string                      `json:"sub" mapstructure:"sub"`
	Issuer               string                      `json:"iss" mapstructure:"iss"`
	Audience             string                      `json:"aud" mapstructure:"aud"`
	PreferredUsername    string                      `json:"preferred_username" mapstructure:"preferred_username"`
	Email                ciLogonUserInfoEmail        `json:"email" mapstructure:"email"`
	FirstName            string                      `json:"given_name" mapstructure:"given_name"`
	LastName             string                      `json:"family_name" mapstructure:"family_name"`
	FullName             string                      `json:"name" mapstructure:"name"`
	IdentityProviderID   string                      `json:"idp" mapstructure:"idp"`
	IdentityProviderName string                      `json:"idp_name" mapstructure:"idp_name"`
	Organization         ciLogonUserInfoOrganization `json:"organization" mapstructure:"organization"`
	EPPN                 string                      `json:"eppn" mapstructure:"eppn"`   // eduPersonPrincipalName
	EPTID                string                      `json:"eptid" mapstructure:"eptid"` // eduPersonTargetedID
	CertSubjectDN        string                      `json:"cert_subject_dn" mapstructure:"cert_subject_dn"`
	JWTID                string                      `json:"jti" mapstructure:"jti"`
	//Acr                  string `json:"acr" mapstructure:"acr"`
}

// special type for custom unmarshal logic, since this could be a string or an array of string
type ciLogonUserInfoOrganization string

func (org *ciLogonUserInfoOrganization) UnmarshalJSON(data []byte) error {
	var orgStr string
	err := json.Unmarshal(data, &orgStr)
	if err == nil {
		*org = ciLogonUserInfoOrganization(orgStr)
		return nil
	}
	var orgList []string
	err = json.Unmarshal(data, &orgList)
	if err != nil {
		return err
	}
	if len(orgList) > 0 {
		// use first organization in the list
		*org = ciLogonUserInfoOrganization(orgList[0])
	}
	return nil
}

// special type for custom unmarshal logic, since this could be a string or an array of string
type ciLogonUserInfoEmail string

func (email *ciLogonUserInfoEmail) UnmarshalJSON(data []byte) error {
	var emailStr string
	err := json.Unmarshal(data, &emailStr)
	if err == nil {
		*email = ciLogonUserInfoEmail(emailStr)
		return nil
	}
	var emailList []string
	err = json.Unmarshal(data, &emailList)
	if err != nil {
		return err
	}
	if len(emailList) > 0 {
		// use first email in the list
		*email = ciLogonUserInfoEmail(emailList[0])
	}
	return nil
}

// endpoint to extract userinfo for CILogon, use this to get actual username (not CILogon's internal ID), email, etc.
const ciLogonUserinfoEndpoint = "https://cilogon.org/oauth2/userinfo"

// ExtractUserProfile extract UserProfile by calling the userinfo endpoint for CILogon with accessToken.
func (e ciLogonProfileExtractor) ExtractUserProfile(accessToken string, claims map[string]interface{}) (*UserProfile, error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "ciLogonProfileExtractor.ExtractUserProfile",
	})
	// Build the request body.
	values := &url.Values{}
	values.Set("access_token", accessToken)
	body := values.Encode()

	// Send the request.
	resp, err := http.Post(ciLogonUserinfoEndpoint, "application/x-www-form-urlencoded", strings.NewReader(body))
	if err != nil {
		logger.WithField("error", err).Error("token introspection failed")
		return nil, err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		msg := fmt.Sprintf("unexpected status code in token introspection response: %d", resp.StatusCode)
		logger.Error(msg)
		return nil, fmt.Errorf(msg)
	}

	// json unmarshal response
	all, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var userinfo ciLogonUserInfo
	err = json.Unmarshal(all, &userinfo)
	if err != nil {
		logger.WithError(err).WithField("payload", string(all)).Error("fail to parse userinfo into json")
		return nil, err
	}
	return e.userinfoToProfile(userinfo)
}

func (e ciLogonProfileExtractor) userinfoToProfile(userinfo ciLogonUserInfo) (*UserProfile, error) {
	if err := e.checkIdentityProvider(userinfo); err != nil {
		return nil, err
	}
	username, err := e.selectUsername(userinfo)
	if err != nil {
		return nil, err
	}
	return &UserProfile{
		Username:   username,
		FirstName:  userinfo.FirstName,
		LastName:   userinfo.LastName,
		Email:      string(userinfo.Email),
		Attributes: nil,
	}, nil
}

func (e ciLogonProfileExtractor) selectUsername(userinfo ciLogonUserInfo) (string, error) {
	var username string
	var candidates = [3]string{userinfo.Subject, userinfo.EPPN, userinfo.PreferredUsername}
	for _, candidate := range candidates {
		if candidate == "" {
			continue
		}
		if !strings.HasSuffix(candidate, "@access-ci.org") {
			// for ACCESS-CI specifically
			continue
		}
		username = candidate
		break
	}
	if username == "" {
		return "", fmt.Errorf("no valid username in userinfo")
	}
	return username, nil
}

func (e ciLogonProfileExtractor) checkIdentityProvider(userinfo ciLogonUserInfo) error {
	// check only if requiredIdentityProviderID is set(non-empty)
	if e.requiredIdentityProviderID != "" {
		if userinfo.IdentityProviderID != e.requiredIdentityProviderID {
			return fmt.Errorf("identity is not from required identity provider")
		}
	}
	return nil
}
