package utils

import (
	"encoding/json"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/api-service/config"
)

// AcceptedResponse is used to indicate that a request has been accepted.
type AcceptedResponse struct {
	ID            string               `json:"id,omitempty"`
	TransactionID common.TransactionID `json:"tid,omitempty"`
	Timestamp     time.Time            `json:"timestamp"`
}

// NewAcceptedResponse creates a new response indicating that a request has been accepted.
func NewAcceptedResponse(id common.ID, transactionID common.TransactionID) *AcceptedResponse {
	if config.GlobalConfig.PopulateTIDWithID && transactionID == "" {
		// TODO remove this after making changes to client
		transactionID = common.TransactionID(id)
	}
	return &AcceptedResponse{
		ID:            id.String(),
		TransactionID: transactionID,
		Timestamp:     time.Now(),
	}
}

// NewAcceptedResponseWithStrID creates a new response indicating that a request has been accepted.
func NewAcceptedResponseWithStrID(id string, transactionID common.TransactionID) *AcceptedResponse {
	if config.GlobalConfig.PopulateTIDWithID && transactionID == "" {
		// TODO remove this after making changes to client
		transactionID = common.TransactionID(id)
	}
	return &AcceptedResponse{
		ID:            id,
		TransactionID: transactionID,
		Timestamp:     time.Now(),
	}
}

// ReturnStatus will return any "good" statuses
func ReturnStatus(w http.ResponseWriter, obj interface{}, statusCode int) {
	w.Header().Add("Content-Type", "application/json")

	b, err := json.Marshal(obj)
	if err == nil {
		w.WriteHeader(statusCode)
		w.Write(b)
	} else { // something went horribly wrong when marshalling
		log.Debug("ReturnStatusOK: failed miserably when trying to return a response")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("something went horribly wrong, please contact site operator"))
	}
}
