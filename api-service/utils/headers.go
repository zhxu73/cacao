package utils

import (
	"gitlab.com/cyverse/cacao-common/service"
	"net/http"
	"strconv"

	"gitlab.com/cyverse/cacao/api-service/constants"
)

// GetCacaoHeaders returns headers added by Cacao
// Currently, this includes username, emulator, isAdmin
func GetCacaoHeaders(r *http.Request) (actor string, emulator string, isAdmin bool) {
	actor = r.Header.Get(constants.RequestHeaderCacaoUser)
	emulator = r.Header.Get(constants.RequestHeaderCacaoEmulator)
	isAdmin, _ = strconv.ParseBool(r.Header.Get(constants.RequestHeaderCacaoAdmin))
	return
}

// GetCacaoHeaders1 returns service.Actor instead of separate strings
func GetCacaoHeaders1(r *http.Request) (actor service.Actor, isAdmin bool) {
	actor = service.Actor{
		Actor:    r.Header.Get(constants.RequestHeaderCacaoUser),
		Emulator: r.Header.Get(constants.RequestHeaderCacaoEmulator),
	}
	isAdmin, _ = strconv.ParseBool(r.Header.Get(constants.RequestHeaderCacaoAdmin))
	return
}
