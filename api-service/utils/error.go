package utils

import (
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"k8s.io/apimachinery/pkg/util/json"
)

// ErrorStatus is the struct/json object that is marshalled for every error response per CACAO's openapi spec
type ErrorStatus struct {
	Actor     string `json:"actor"`
	Timestamp string `json:"timestamp"`
	ErrorType string `json:"error"`
	Message   string `json:"message"`
	Path      string `json:"path"`
	Verb      string `json:"verb"`
}

// JSONError writes a JSON error response.
func JSONError(w http.ResponseWriter, r *http.Request, errorType string, errorMsg string, code int) {
	errorObj := new(ErrorStatus)

	actor := r.Header.Get(constants.RequestHeaderCacaoUser)
	if actor != service.ReservedCacaoSystemActor { // we don't want to expose this reserved actor
		errorObj.Actor = r.Header.Get(constants.RequestHeaderCacaoUser)
	}
	errorObj.Timestamp = time.Now().UTC().Format(time.RFC3339)
	errorObj.ErrorType = errorType
	errorObj.Message = errorMsg
	errorObj.Path = r.URL.Path
	errorObj.Verb = r.Method

	w.Header().Add("Content-Type", "application/json")
	b, err := json.Marshal(errorObj)
	if err == nil {
		w.WriteHeader(code)
		w.Write(b)
	} else { // something very bad happened and we shouldn't hit this point
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("could not fulfill request and return a result; please notify site admin"))
	}
}

// JSONCacaoError writes a JSON error response, the status code is selected based on the error type.
func JSONCacaoError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	var status int
	switch err.(type) {
	case *service.CacaoInvalidParameterError, *service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if logger != nil {
		logger.WithField("status", status).Error(err)
	}

	if cerr, ok := err.(service.CacaoError); ok {
		JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		JSONError(w, r, "internal service error", err.Error(), status)
	}
}
