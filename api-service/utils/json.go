package utils

import (
	"bytes"
	"encoding/json"
)

// StrictJSONDecode will take data and a target ojbect and return an error if extra json fields are not found in the target
func StrictJSONDecode(data []byte, target interface{}) error {
	decoder := json.NewDecoder(bytes.NewReader([]byte(data)))
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&target)
	return err
}
