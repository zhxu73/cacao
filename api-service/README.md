# API Service

This service is used as an intermediary between the user (or web UI) and the rest of the microservices.

This service will interact with the other services with a mix of synchronous and asynchronous requests. Queries (often `GET` requests) will synchronously return information about an object. Commands will asynchronously trigger events in other services such as creating a WorkflowDefinition or starting a build.

[Read more...](../docs/developers/api-service.md)
