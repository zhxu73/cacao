package main

import (
	"context"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/api"
	"gitlab.com/cyverse/cacao/api-service/clients/awsprovider"
	"gitlab.com/cyverse/cacao/api-service/clients/credentials"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/clients/interactivesessions"
	"gitlab.com/cyverse/cacao/api-service/clients/js2allocations"
	"gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"
	"gitlab.com/cyverse/cacao/api-service/clients/providermetadata"
	"gitlab.com/cyverse/cacao/api-service/clients/storage"
	"gitlab.com/cyverse/cacao/api-service/clients/templates"
	"gitlab.com/cyverse/cacao/api-service/clients/tokens"
	"gitlab.com/cyverse/cacao/api-service/clients/useractions"
	"gitlab.com/cyverse/cacao/api-service/clients/workspaces"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"io"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

// GitCommit is the commit hash of the cacao repo which api-service is a part of.
// This is meant to be set at build time.
// e.g.
// go build -ldflags="-X 'main.GitCommit=aaaaaaaa'"
var GitCommit string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)

	err := envconfig.Process(constants.ConfigVarPrefix, &config.GlobalConfig)
	if err != nil {
		log.Fatal(err.Error())
	}
	err = envconfig.Process(constants.ConfigVarPrefix, &config.GlobalConfig.NatsStanMsg) // process NatsStanMsg separately so that it uses the prefix
	if err != nil {
		log.Fatal(err.Error())
	}

	level, err := log.ParseLevel(config.GlobalConfig.LogLevel)
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	config.GlobalConfig.ProcessDefaults()
}

func main() {
	log.Info("Auto create user = " + strconv.FormatBool(config.GlobalConfig.AutoCreateUser))
	log.Info("Log level set to: " + config.GlobalConfig.LogLevel)
	log.Info("Nats URL = " + config.GlobalConfig.NatsStanMsg.URL)

	appContext, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	config.GlobalConfig.AppContext = appContext
	common.CancelWhenSignaled(cancelFunc)

	// Create the microservice clients.
	natsConn, err := config.GlobalConfig.NatsStanMsg.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&natsConn)
	stanConn, err := config.GlobalConfig.NatsStanMsg.ConnectStan()
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&stanConn)

	err = api.InitAuthenticationDriver(&config.GlobalConfig)
	if err != nil {
		log.WithError(err).Panic("unable to initialize authentication driver")
		panic(err)
	}

	usersClient, err := service.NewNatsUserClientFromConn(&natsConn, &stanConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	deploymentsClient := deployments.New(&natsConn, &stanConn)
	credentialClient := credentials.New(&natsConn, &stanConn, time.Now)
	templatesClient := templates.New(&natsConn, &stanConn)
	workspacesClient := workspaces.New(&natsConn, &stanConn)
	userActionsClient := useractions.New(&natsConn, &stanConn)
	storageClient := storage.New(&natsConn, &stanConn)
	providerMetadataClient := providermetadata.New(&natsConn, &stanConn)
	openstackProviderClient := openstackprovider.New(&natsConn, &stanConn)
	awsProviderClient := awsprovider.New(&natsConn, &stanConn)
	tokensClient := tokens.New(&natsConn, &stanConn)

	// js2allocationsClient requires longer deadline than other microservices because it communicates with external services at Jetstream2
	js2allocationsClient := js2allocations.New(&natsConn)

	// interactivesessionsClient requires longer deadline than other microservices because it uses ansible to configure instances
	interactivesessionsClient := interactivesessions.New(&natsConn, &stanConn)

	// Router is only used for User things since they won't require authentication
	router := mux.NewRouter().StrictSlash(true)
	api.AuthAPIRouter(router)
	api.DocsAPIRouter(router)
	api.HealthCheckAPIRouter(router)
	api.PublicAPIRouter(templatesClient, router)

	// Require Admin permission to access
	//api.AdminAPIRouter(router)

	// Subrouter will be used for all paths other than login since it will
	// require headers to authenticate with AuthenticateMiddleware
	subRouter := router.NewRoute().Subrouter()
	subRouter.Use(api.AuthenticationMiddleware)
	api.UserAPIRouter(usersClient, subRouter)
	api.DeploymentAPIRouter(deploymentsClient, subRouter)
	api.CredentialAPIRouter(credentialClient, subRouter)
	api.VersionAPIRouter(subRouter, GitCommit)
	api.ProviderAPIRouter(providerMetadataClient, subRouter)
	api.ProviderSpecificAPIRouter(providerMetadataClient, api.NewOpenStackProviderAPI(openstackProviderClient), api.NewAWSProviderAPI(awsProviderClient), subRouter)
	api.StorageAPIRouter(storageClient, subRouter)
	// api.ClusterAPIRouter(subRouter)
	// api.WorkflowDefinitionAPIRouter(subRouter)
	// api.BuildAPIRouter(subRouter)
	// api.AnchoreAPIRouter(subRouter)
	api.TemplateAPIRouter(templatesClient, subRouter)
	api.WorkspaceAPIRouter(workspacesClient, deploymentsClient, subRouter)
	api.UserActionAPIRouter(userActionsClient, subRouter)
	api.JS2AllocationAPIRouter(js2allocationsClient, subRouter)
	api.InteractiveSessionAPIRouter(interactivesessionsClient, subRouter)
	api.TokenAPIRouter(tokensClient, subRouter)
	//Put new routes above this line
	//RunAPIRouter defines a "catchall" endpoint for HTTP scaling
	// api.RunAPIRouter(subRouter)

	// Setting this is useful for triaging errant requests while tracing
	subRouter.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Trace("http path not found: path = " + r.URL.Path + ", method = " + r.Method)
		utils.JSONError(w, r, "path not found", "path not found", http.StatusNotFound)
	})

	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("API Service listening on port 8080")
	var server = http.Server{
		Addr: ":8080",
		Handler: handlers.CORS(
			handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
			handlers.AllowedMethods([]string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"}),
			handlers.AllowedOrigins([]string{"*"}))(router),
	}
	var httpServerShutdown sync.WaitGroup
	httpServerShutdown.Add(1)
	go shutdownServerOnAppContextCancel(&server, &httpServerShutdown)
	err = server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.WithError(err).Error("fail to start http server")
		return
	}
	httpServerShutdown.Wait()
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error("fail to close")
		return
	}
}

// shutdown server when AppContext is canceled, notify WaitGroup when shutdown is
// complete (program need to wait for shutdown to complete before exit).
func shutdownServerOnAppContextCancel(server *http.Server, wg *sync.WaitGroup) {
	defer wg.Done()
	<-config.GlobalConfig.AppContext.Done()
	err := server.Shutdown(context.Background())
	if err != nil {
		log.WithError(err).Error("fail to shutdown http server")
	}
}
