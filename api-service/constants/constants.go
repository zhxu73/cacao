package constants

const (
	// ConfigVarPrefix is used by kelseyhightower/envconfig as the prefix the env variables
	ConfigVarPrefix = "API"

	// DefaultNatsClientID defines the default nats client id for api service
	DefaultNatsClientID = "api"

	// DefaultKeycloakURL is the default url to keycloak
	DefaultKeycloakURL = "http://keycloak:8080/auth/realms/cacao"

	// DefaultKeycloakRedirectURL is the default redirect url from keycloak
	DefaultKeycloakRedirectURL = "http://api:8080/user/login/callback"

	// DefaultKeycloakClientID is the default keycloak id
	DefaultKeycloakClientID = "cacao-client"

	// GlobusOAuthURL is the base URL for Globus OAuth2
	GlobusOAuthURL = "https://auth.globus.org/v2/oauth2"

	// CILogonOAuthURL is the base URL for CILogon OAuth2
	CILogonOAuthURL = "https://cilogon.org"
)

// AuthDriverType is the type of auth driver that will be used on REST api
type AuthDriverType string

// These are authentication driver related constants
const (
	// AuthDriverCILogon designates the constant to use for the CILogon AuthDriver.
	AuthDriverCILogon AuthDriverType = "cilogon"

	// AuthDriverGlobus designates the constant to use for the Globus AuthDriver.
	AuthDriverGlobus AuthDriverType = "globus"

	// AuthDriverKeycloak designates the constant to use the Keycloak AuthDriver
	AuthDriverKeycloak AuthDriverType = "keycloak"

	// AuthDriverSimpleToken designates the constant to use the SimpleToken AuthDriver (local dev only)
	AuthDriverSimpleToken AuthDriverType = "simpletoken"
)

// These are http request related headers
const (
	// RequestHeaderCacaoUser is the request header for the cacao user/actor
	RequestHeaderCacaoUser = "X-Cacao-User"

	// RequestHeaderCacaoAdmin is the request header to check if the user/actor is an admin
	RequestHeaderCacaoAdmin = "X-Cacao-Admin"

	// RequestHeaderCacaoEmulator is the request header to use if there is an emulator (to the actor)
	RequestHeaderCacaoEmulator = "X-Cacao-Emulator"

	RequestHeaderCacaoAPIToken = "X-Cacao-Token"
)

// AccessCIIdentityProvider is the identity provider id for ACCESS-CI in CILogon. This is the value in "idp" field in the response from userinfo endpoint.
const AccessCIIdentityProvider = "https://access-ci.org/idp"
