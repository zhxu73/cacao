package storage

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/messaging2"

	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/clients/templates"
)

// Error message format string constants.
const incorrectTemplatePurposeErrorFmt = "incorrect template purpose for a storage deployment: %s"

// Parameter name constants.
const instanceIDParamName = "instance_id"
const attachParamName = "attach"

// getIncorrectTemplatePurposeError returns an error indicating that the template purpose is incorrect.
func getIncorrectTemplatePurposeError(purpose service.TemplatePurpose) error {
	if purpose != service.TemplatePurposeStorage {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf(incorrectTemplatePurposeErrorFmt, purpose))
	}
	return nil
}

// copyParameters creates a deep copy of an array of parameters.
func copyParameters(source []hm.KeyValue) []hm.KeyValue {
	dest := make([]hm.KeyValue, len(source))
	for i, kv := range source {
		dest[i] = hm.KeyValue{
			Key:   kv.Key,
			Value: kv.Value,
		}
	}
	return dest
}

// copyDeploymentRun creates a selective deep copy of the writeable fields in a deployment run. The only field
// that may be updated is the list of parameters, so that's the only field that is deep copied.
func copyDeploymentRun(source *hm.DeploymentRun) *hm.DeploymentRun {
	return &hm.DeploymentRun{
		BuildID:          source.BuildID,
		Parameters:       copyParameters(source.Parameters),
		CloudCredentials: source.CloudCredentials,
		GitCredentialID:  source.GitCredentialID,
	}
}

// Client is an interface for interacting with the Deployments and Templates microservices. Since there are separate
// client implementations for both microservices, this client acts as an abstraction to encapsulate calls to those
// services.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the Deployments and Templates microservices on behalf of a user. The
// purpose of having a session is to consolitate parameters that are common in all or most requests, but are not known
// at configuration time.
type Session interface {
	ListStorageTemplateIDs() ([]cacaocommon.ID, error)
	ListStorageDeployments() ([]hm.Deployment, error)
	ValidateStorageDeploymentCreationRequest(creationRequest *hm.Deployment) error
	AddStorageDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error)
	RunStorageDeployment(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) (cacaocommon.ID, error)
	GetStorageDeployment(deploymentID cacaocommon.ID) (hm.Deployment, error)
	ValidateStorageDeploymentUpdateRequest(deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate) error
	UpdateStorageDeployment(deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate) (cacaocommon.ID, error)
	ValidateStorageDeploymentDeletionRequest(deploymentID cacaocommon.ID) error
	DeleteStorageDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, bool, error)
	PerformStorageDeploymentAction(deploymentID cacaocommon.ID, actionRequest *hm.StorageActionRequest) (cacaocommon.ID, error)
}

// storageClient is the primary Client implementation.
type storageClient struct {
	deploymentsClient deployments.Client
	templatesClient   templates.Client
}

// New creates a new Storage microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &storageClient{
		deploymentsClient: deployments.New(queryConn, eventConn),
		templatesClient:   templates.New(queryConn, eventConn),
	}
}

// Session returns a new storage microservice client session.
func (c *storageClient) Session(actor, emulator string, isAdmin bool) (Session, error) {
	dsession, err := c.deploymentsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		return nil, err
	}
	tsession, err := c.templatesClient.Session(actor, emulator, isAdmin)
	if err != nil {
		return nil, err
	}
	session := &storageSession{
		actor:              actor,
		emulator:           emulator,
		isAdmin:            isAdmin,
		deploymentsSession: dsession,
		templatesSession:   tsession,
	}
	return session, nil
}

// storageSession is the primary Session implementation.
type storageSession struct {
	actor              string
	emulator           string
	isAdmin            bool
	deploymentsSession deployments.Session
	templatesSession   templates.Session
}

// ListStorageTemplateIDs lists the IDs of all templates accessible to the user with the purpose field set to
// `storage`.
func (s *storageSession) ListStorageTemplateIDs() ([]cacaocommon.ID, error) {

	// Get the list of templates that are accessible to the user.
	templates, err := s.templatesSession.ListTemplates(false)
	if err != nil {
		return nil, err
	}

	// Count the number of storage templates first to avoid haivng to append to a slice.
	count := 0
	for _, template := range templates {
		if template.Metadata.Purpose == service.TemplatePurposeStorage {
			count++
		}
	}

	// Build the result.
	storageTemplateIDs := make([]cacaocommon.ID, count)
	index := 0
	for _, template := range templates {
		if template.Metadata.Purpose == service.TemplatePurposeStorage {
			storageTemplateIDs[index] = template.ID
			index++
		}
	}

	return storageTemplateIDs, nil
}

// ListStorageDeployments lists all of the deployments accessible to the actor that were generated from a template
// whose purpose field is set to `storage`.
func (s *storageSession) ListStorageDeployments() ([]hm.Deployment, error) {

	// Get the list of storage template IDs.
	templateIDs, err := s.ListStorageTemplateIDs()
	if err != nil {
		return nil, err
	}

	// If there aren't any storage templates then there aren't any storage deployments either.
	if len(templateIDs) == 0 {
		return []hm.Deployment{}, nil
	}

	// Build the list options.
	listOption := service.DeploymentListOption{
		SortBy:        service.SortByID,
		SortDirection: service.DescendingSort,
		Filter: service.DeploymentFilter{
			User:        s.actor,
			TemplateSet: templateIDs,
		},
	}

	// List the deployments.
	return s.deploymentsSession.SearchDeployments(listOption)
}

// ValidateStorageDeploymentCreationRequest performs some validation on an incoming deployment creation request.
func (s *storageSession) ValidateStorageDeploymentCreationRequest(creationRequest *hm.Deployment) error {

	// Look up the template.
	templateID := cacaocommon.ID(creationRequest.TemplateID)
	template, err := s.templatesSession.GetTemplate(templateID)
	if err != nil {
		return err
	}

	// Verify that the template purpose is correct.
	templatePurposeError := getIncorrectTemplatePurposeError(template.Metadata.Purpose)
	if templatePurposeError != nil {
		return templatePurposeError
	}

	return s.deploymentsSession.ValidateDeploymentCreationRequest(creationRequest)
}

// AddStorageDeployment adds a storage deployment on behalf of the actor.
func (s *storageSession) AddStorageDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error) {
	return s.deploymentsSession.AddDeployment(creationRequest)
}

// RunStorageDeployment runs a storage deployment on belhalf of the actor.
func (s *storageSession) RunStorageDeployment(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) (cacaocommon.ID, error) {
	return s.deploymentsSession.RunDeployment(deploymentID, runRequest)
}

// GetStorageDeployment retrieves a storage deployment by its identifier.
func (s *storageSession) GetStorageDeployment(deploymentID cacaocommon.ID) (hm.Deployment, error) {

	// Retrieve the deployment.
	deployment, err := s.deploymentsSession.GetDeployment(deploymentID)
	if err != nil {
		return hm.Deployment{}, err
	}

	// Retrieve the template.
	templateID := cacaocommon.ID(deployment.TemplateID)
	template, err := s.templatesSession.GetTemplate(templateID)
	if err != nil {
		return hm.Deployment{}, err
	}

	// Verify that the template purpose is correct.
	templatePurposeError := getIncorrectTemplatePurposeError(template.Metadata.Purpose)
	if templatePurposeError != nil {
		return hm.Deployment{}, templatePurposeError
	}

	return deployment, nil
}

// ValidateStorageDeploymentUpdateRequest performs some validation on an incoming deployment update request.
func (s *storageSession) ValidateStorageDeploymentUpdateRequest(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) error {

	// Do the lower-level validation first.
	err := s.deploymentsSession.ValidateDeploymentUpdateRequest(deploymentID, updateRequest)
	if err != nil {
		return err
	}

	// Get the template ID.
	var templateID cacaocommon.ID
	if updateRequest.TemplateID != nil {
		templateID = cacaocommon.ID(*updateRequest.TemplateID)
	} else {
		deployment, err := s.deploymentsSession.GetDeployment(deploymentID)
		if err != nil {
			return err
		}
		templateID = cacaocommon.ID(deployment.TemplateID)
	}

	// Retrieve the template.
	template, err := s.templatesSession.GetTemplate(templateID)
	if err != nil {
		return err
	}

	// Verify that the template purpose is correct.
	templatePurposeError := getIncorrectTemplatePurposeError(template.Metadata.Purpose)
	if templatePurposeError != nil {
		return templatePurposeError
	}

	return nil
}

// UpdateStorageDeployment submits a request to update a storage deployment. Validation should be done prior to calling
// this method via a call to ValidateStorageDeploymentUPdateRequest.
func (s *storageSession) UpdateStorageDeployment(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) (cacaocommon.ID, error) {
	return s.deploymentsSession.UpdateDeployment(deploymentID, updateRequest)
}

// ValidateStorageDeploymentDeletionRequest verifies that a deployment can be deleted by the user.
func (s *storageSession) ValidateStorageDeploymentDeletionRequest(deploymentID cacaocommon.ID) error {

	// Do the lower-level validation first.
	err := s.deploymentsSession.ValidateDeploymentDeletionRequest(deploymentID)
	if err != nil {
		return err
	}

	// Call GetStorageDeployment to do the rest of the validation for us.
	_, err = s.GetStorageDeployment(deploymentID)
	return err
}

// DeleteStorageDeployment deletes a deployment on behalf of the user. Validation should be done prior to calling this
// method via a call to ValidateDeploymentDeletionRequest.
func (s storageSession) DeleteStorageDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, bool, error) {
	return s.deploymentsSession.DeleteDeployment(deploymentID)
}

// PerformStorageDeploymentAction performs an action on a storage deployment. The action can be `attach` or `detach`.
// An instance ID can optionally be provided as well.
func (s storageSession) PerformStorageDeploymentAction(
	deploymentID cacaocommon.ID, actionRequest *hm.StorageActionRequest,
) (cacaocommon.ID, error) {

	// The instance ID must be specified in order to be able to perform an action.
	if actionRequest.InstanceID == "" {
		return "", service.NewCacaoInvalidParameterError("no instance ID specified")
	}

	// List the deployment runs.
	runs, err := s.deploymentsSession.ListDeploymentRuns(deploymentID)
	if err != nil {
		return "", err
	}

	// Find the most recent successful deployment run. This assumes that runs are listed in creation order.
	lastSuccessfulDeploymentRunIndex := -1
	for i, run := range runs {
		if run.Status != "errored" {
			lastSuccessfulDeploymentRunIndex = i
		}
	}
	if lastSuccessfulDeploymentRunIndex < 0 {
		return "", service.NewCacaoInvalidParameterError("no successful deployment run found")
	}

	// Make a copy of the last successful deployment run and update the parameters.
	newDeploymentRun := copyDeploymentRun(&runs[lastSuccessfulDeploymentRunIndex])
	var instanceIDParamFound, attachParamFound bool
	for i, param := range newDeploymentRun.Parameters {
		if param.Key == instanceIDParamName {
			instanceIDParamFound = true
			newDeploymentRun.Parameters[i].Value = actionRequest.InstanceID
		} else if param.Key == attachParamName {
			attachParamFound = true
			newDeploymentRun.Parameters[i].Value = actionRequest.Action.AttachParamValue()
		}
	}
	if !instanceIDParamFound {
		newDeploymentRun.Parameters = append(newDeploymentRun.Parameters, hm.KeyValue{
			Key:   instanceIDParamName,
			Value: actionRequest.InstanceID,
		})
	}
	if !attachParamFound {
		newDeploymentRun.Parameters = append(newDeploymentRun.Parameters, hm.KeyValue{
			Key:   attachParamName,
			Value: actionRequest.Action.AttachParamValue(),
		})
	}

	return s.deploymentsSession.RunDeployment(deploymentID, newDeploymentRun)
}
