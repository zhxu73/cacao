package tokens

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"time"
)

// Client is client for token service
type Client interface {
	Session(actor, emulator string) (Session, error)
}

// Session is an interface for interacting with the token microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	// Tokens
	GetToken(id common.ID) (service.TokenModel, error)
	ListTokens() ([]service.TokenModel, error)
	ValidateCreateTokenRequest(model service.TokenCreateModel) error
	CreateToken(model service.TokenCreateModel) (common.TokenID, error)
	ValidateTokenDeleteRequest(id common.ID) error
	UpdateTokenFields(id common.ID, updateRequest service.TokenModel, updateFields []string) (common.ID, error)
	ValidateTokenUpdateFieldsRequest(id common.ID, updateRequest service.TokenModel, updateFields []string) error
	DeleteToken(id common.ID) (common.ID, error)
	RevokeToken(id common.ID) (common.ID, error)
	// Token Types
	ListTypes() ([]service.TokenType, error)
}

type tokensClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

type tokenSession struct {
	serviceClient service.TokenClient
	actor         service.Actor
	context       context.Context
	isAdmin       bool
}

func (s *tokenSession) getCtx() context.Context {
	ctx, _ := context.WithTimeout(s.context, time.Second*5)
	return ctx
}

func (s *tokenSession) RevokeToken(id common.ID) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "RevokeToken",
	})

	err := s.serviceClient.Revoke(s.getCtx(), s.actor, id)
	if err != nil {
		logger.WithError(err).Error("issue revoking token")
		return "", err
	}
	return id, nil
}

func (s *tokenSession) GetToken(id common.ID) (service.TokenModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetToken",
	})

	token, err := s.serviceClient.Get(s.getCtx(), s.actor, id)
	if err != nil {
		msg := fmt.Sprintf("failed to get the token for ID %s", id)
		logger.WithField("error", err).Error(msg)
		return service.TokenModel{}, err
	}
	return *token, nil
}

func (s *tokenSession) ListTokens() ([]service.TokenModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTokens",
	})

	tokens, err := s.serviceClient.List(s.getCtx(), s.actor)
	if err != nil {
		msg := "failed to list tokens"
		logger.WithField("error", err).Error(msg)
		return []service.TokenModel{}, err
	}
	return tokens, nil
}

func (s *tokenSession) ValidateCreateTokenRequest(model service.TokenCreateModel) error {
	if len(model.Name) == 0 {
		return service.NewCacaoInvalidParameterError("token name is not valid")
	}
	if len(model.Type) == 0 {
		return service.NewCacaoInvalidParameterError("token type is not valid")
	}
	if len(model.Owner) == 0 {
		return service.NewCacaoInvalidParameterError("token owner is not valid")
	}

	return nil
}

func (s *tokenSession) CreateToken(model service.TokenCreateModel) (common.TokenID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateToken",
	})

	tokenID, err := s.serviceClient.Create(s.getCtx(), s.actor, model)
	if err != nil {
		msg := "failed to create a token"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return tokenID, nil
}

func (s *tokenSession) ValidateTokenDeleteRequest(id common.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("id is not valid")
	}
	return nil
}

func (s *tokenSession) DeleteToken(id common.ID) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteToken",
	})

	err := s.serviceClient.Delete(s.getCtx(), s.actor, id)
	if err != nil {
		msg := fmt.Sprintf("failed to delete token: %s", id.String())
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return id, nil
}

func (s *tokenSession) UpdateTokenFields(id common.ID, updateRequest service.TokenModel, updateFields []string) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTokenFields",
	})

	err := s.serviceClient.UpdateFields(s.getCtx(), s.actor, updateRequest, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update token: %s", id.String())
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return id, nil
}

func (s *tokenSession) ValidateTokenUpdateFieldsRequest(id common.ID, updateRequest service.TokenModel, updateFields []string) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("id is invalid: %s", id.String()))
	}
	if updateRequest.Owner != updateRequest.GetSessionActor() && updateRequest.GetSessionActor() != service.ReservedCacaoSystemActor {
		return service.NewCacaoUnauthorizedError("user is not authorized to update template type")
	}
	for _, field := range updateFields {
		switch field {
		case "name":
			if len(updateRequest.Name) <= 0 {
				return service.NewCacaoInvalidParameterError("name is not valid")
			}
		case "description":
			if len(updateRequest.Description) <= 0 {
				return service.NewCacaoInvalidParameterError("description is not valid")
			}
		case "scopes":
			if len(updateRequest.Scopes) <= 0 {
				return service.NewCacaoInvalidParameterError("scopes is not valid")
			}
		}
	}
	return nil
}

func (s *tokenSession) ListTypes() ([]service.TokenType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTypes",
	})

	tokenTypes, err := s.serviceClient.ListTypes(s.getCtx(), s.actor)
	if err != nil {
		msg := fmt.Sprintf("failed to list token types")
		logger.WithField("error", err).Error(msg)
		empty := make([]service.TokenType, 0)
		return empty, err
	}

	return tokenTypes, nil
}

func (c *tokensClient) Session(actor, emulator string) (Session, error) {
	if actor == "" {
		return nil, service.NewCacaoInvalidParameterError("no actor specified")
	}
	serviceClient, err := service.NewNatsTokenClientFromConn(c.queryConn, c.eventConn)
	if err != nil {
		return nil, err
	}
	session := tokenSession{
		serviceClient: serviceClient,
		actor: service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
		context: context.Background(),
	}
	return &session, nil
}

// New creates a new cacao api token microservice client
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &tokensClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}
