package common

import "fmt"

// ErrNotImplemented is an error indicating that a feature hasn't been implemented yet.
var ErrNotImplemented = fmt.Errorf("not implemented")

// ErrNotFound is an error indicating that the object looking for is not found.
var ErrNotFound = fmt.Errorf("object not found")

// ErrBadRequest is an error indicating that the request parameters are invalid.
var ErrBadRequest = fmt.Errorf("bad request")
