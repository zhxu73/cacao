package providermetadata

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// Client is client for credential service
type Client interface {
	Session(actor, emulator string) (Session, error)
}

// Session is a session for an actor to perform operations on credentials.
type Session interface {
	Get(ctx context.Context, id common.ID) (*service.ProviderModel, error)
	List(ctx context.Context) ([]service.ProviderModel, error)
	Create(ctx context.Context, provider service.ProviderModel) (common.ID, error)
	Delete(ctx context.Context, id common.ID) (service.DepCheckDeletionResult, error)
	Update(ctx context.Context, provider service.ProviderModel) error
}

type providerClient struct {
	serviceClient            service.ProviderClient
	dependencyMediatorClient service.DependencyMediatorClient
}

// New creates a new client
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	serviceClient, err := service.NewNatsProviderClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	dependencyMediatorClient, err := service.NewDependencyMediatorClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return providerClient{
		serviceClient:            serviceClient,
		dependencyMediatorClient: dependencyMediatorClient,
	}
}

func (p providerClient) Session(actor, emulator string) (Session, error) {
	return providerSession{
		serviceClient:            p.serviceClient,
		dependencyMediatorClient: p.dependencyMediatorClient,
		actor: service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
	}, nil
}

type providerSession struct {
	serviceClient            service.ProviderClient
	dependencyMediatorClient service.DependencyMediatorClient
	actor                    service.Actor
}

// Get ...
func (p providerSession) Get(ctx context.Context, id common.ID) (*service.ProviderModel, error) {
	return p.serviceClient.Get(ctx, p.actor, id)
}

// List ...
func (p providerSession) List(ctx context.Context) ([]service.ProviderModel, error) {
	return p.serviceClient.List(ctx, p.actor)
}

// Create ...
func (p providerSession) Create(ctx context.Context, provider service.ProviderModel) (common.ID, error) {
	providerID, _, err := p.serviceClient.Create(ctx, p.actor, provider)
	return providerID, err
}

// Delete ...
func (p providerSession) Delete(ctx context.Context, id common.ID) (service.DepCheckDeletionResult, error) {
	return p.dependencyMediatorClient.DeleteProvider(ctx, p.actor.Session(), id)
}

// Update ...
func (p providerSession) Update(ctx context.Context, provider service.ProviderModel) error {
	_, err := p.serviceClient.Update(ctx, p.actor, provider)
	return err
}
