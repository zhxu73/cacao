package js2allocations

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the JS2Allocation microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the JS2Allocation microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	GetUser(nocache bool) (cacao_common_http.JS2User, error)
	ListProjects(nocache bool) ([]cacao_common_http.JS2Project, error)
}

// js2allocationsClient is the primary Client implementation.
type js2allocationsClient struct {
	serviceClient cacao_common_service.JS2AllocationClient
}

// New creates a new JS2Allocation microservice client.
func New(queryConn messaging2.QueryConnection) Client {
	serviceClient, err := cacao_common_service.NewNatsJS2AllocationClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
	}
	return &js2allocationsClient{
		serviceClient: serviceClient,
	}
}

// Session returns a new JS2Allocation microservice client session.
func (c *js2allocationsClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}

	// Define and return the session.
	session := js2allocationSession{
		serviceClient: c.serviceClient,
		actor: cacao_common_service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
		isAdmin: isAdmin,
		context: context.Background(),
	}
	return &session, nil
}

// js2allocationSession is the primary Session implementation.
type js2allocationSession struct {
	serviceClient cacao_common_service.JS2AllocationClient
	actor         cacao_common_service.Actor
	isAdmin       bool
	context       context.Context
}

// returns a context to be used by service clients
func (s *js2allocationSession) getCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.TODO(), time.Second*30)
}

func (s *js2allocationSession) convertUserToHTTPObject(obj cacao_common_service.JS2UserModel) cacao_common_http.JS2User {
	return cacao_common_http.JS2User{
		Owner:        obj.Owner,
		TACCUsername: obj.TACCUsername,
		RetrievedAt:  obj.RetrievedAt,
	}
}

func (s *js2allocationSession) convertProjectToHTTPObject(obj cacao_common_service.JS2ProjectModel) cacao_common_http.JS2Project {
	return cacao_common_http.JS2Project{
		ID:          obj.ID,
		Owner:       obj.Owner,
		Title:       obj.Title,
		Description: obj.Description,
		PI:          obj.PI,
		Allocations: obj.Allocations,
		RetrievedAt: obj.RetrievedAt,
	}
}

// GetUser obtains a JetStream2 user.
func (s *js2allocationSession) GetUser(nocache bool) (cacao_common_http.JS2User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetUser",
	})

	ctx, cancelFunc := s.getCtx()
	js2User, err := s.serviceClient.GetUser(ctx, s.actor, nocache)
	cancelFunc()
	if err != nil {
		msg := "failed to get JS2 User"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.JS2User{}, err
	}

	// convert to http object
	httpObject := s.convertUserToHTTPObject(*js2User)

	return httpObject, nil
}

// ListProjects obtains a list of JetStream2 projects.
func (s *js2allocationSession) ListProjects(nocache bool) ([]cacao_common_http.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListProjects",
	})

	ctx, cancelFunc := s.getCtx()
	js2Projects, err := s.serviceClient.List(ctx, s.actor, nocache)
	cancelFunc()
	if err != nil {
		msg := "failed to list JS2 Projects"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.JS2Project, 0, len(js2Projects))
	for _, js2Project := range js2Projects {
		httpObject := s.convertProjectToHTTPObject(js2Project)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}
