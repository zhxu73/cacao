package deployments

import (
	"encoding/json"
	"gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
)

// ServiceDeploymentToHTTP  converts the service representation of deployment to HTTP
func ServiceDeploymentToHTTP(svcDeployment service.Deployment) hm.Deployment {
	var httpCloudCreds []string
	if svcDeployment.CloudCredentials != nil {
		httpCloudCreds = make([]string, 0, len(svcDeployment.CloudCredentials))
		for cred := range svcDeployment.CloudCredentials {
			httpCloudCreds = append(httpCloudCreds, cred)
		}
	}

	var httpParameters []hm.KeyValue
	var currentRun hm.DeploymentRun
	if svcDeployment.LastRun == nil {
		httpParameters = []hm.KeyValue{}
	} else {
		httpParameters = ServiceParametersToHTTP(svcDeployment.LastRun.Parameters)
		currentRun = ServiceRunToHTTP(*svcDeployment.LastRun)
	}
	return hm.Deployment{
		ID:                svcDeployment.ID.String(),
		Owner:             svcDeployment.Owner,
		Name:              svcDeployment.Name,
		Description:       svcDeployment.Description,
		WorkspaceID:       svcDeployment.Workspace.String(),
		TemplateID:        svcDeployment.Template.String(),
		PrimaryProviderID: svcDeployment.PrimaryCloud.String(),
		CreatedAt:         svcDeployment.CreatedAt,
		UpdatedAt:         svcDeployment.UpdatedAt,
		CurrentStatus:     svcDeployment.CurrentStatus.String(),
		PendingStatus:     svcDeployment.PendingStatus.String(),
		StatusMsg:         svcDeployment.StatusMsg,
		Parameters:        httpParameters,
		CloudCredentials:  httpCloudCreds,
		GitCredentialID:   svcDeployment.GitCredential,
		CurrentBuild:      hm.DeploymentBuild{}, // TODO Deployment build
		CurrentRun:        currentRun,
	}
}

// HTTPBuildToService converts the HTTP representation of deployment build to service
// TODO deployment build
func HTTPBuildToService(httpBuild hm.DeploymentBuild) interface{} {
	panic("implement this")
}

// ServiceBuildToHTTP converts the service representation of deployment build to HTTP
// TODO deployment build
func ServiceBuildToHTTP(svcBuild interface{}) hm.DeploymentBuild {
	panic("implement this")
}

// ServiceRunToHTTP converts the service representation of deployment run to HTTP
func ServiceRunToHTTP(svcRun service.DeploymentRun) hm.DeploymentRun {
	var httpCloudCreds []string
	if svcRun.CloudCredentials != nil {
		for cred := range svcRun.CloudCredentials {
			httpCloudCreds = append(httpCloudCreds, cred)
		}
	}
	return hm.DeploymentRun{
		ID:               svcRun.ID.String(),
		Owner:            svcRun.Owner,
		DeploymentID:     svcRun.Deployment.String(),
		BuildID:          "", // TODO Deployment build
		TemplateVersion:  svcRun.TemplateVersion.String(),
		Parameters:       ServiceParametersToHTTP(svcRun.Parameters),
		CloudCredentials: httpCloudCreds,
		GitCredentialID:  svcRun.GitCredential,
		LastState:        ServiceStateViewToHTTP(svcRun.LastState),
		Status:           svcRun.Status,
		StatusMsg:        svcRun.StatusMsg,
		Start:            svcRun.Start,
		End:              svcRun.End,
	}
}

// HTTPParametersToServiceParameterValues converts the HTTP representation of parameters to service representation of parameter values.
// In addition to basic conversion, this function will perform basic (non-comprehensive) checks on the parameter values.
// The point of validation here is not to provide comprehensive parameter validation, but to toss out some bad value as early as possible,
// downstream services (deployment) should still do complete validation, since api-service is not the only entry point for creating a run.
func HTTPParametersToServiceParameterValues(meta service.TemplateMetadata, httpParameters []hm.KeyValue) (service.DeploymentParameterValues, error) {
	var parameters = make(service.DeploymentParameterValues)
	if httpParameters == nil {
		return parameters, nil
	}

	for _, keyVal := range httpParameters {
		param := meta.GetParameter(keyVal.Key)
		if param == nil {
			// ignore param value if the parameter is NOT in template definition
			continue
		}
		_, parsedValue, err := param.CheckValueFromString(keyVal.Value)
		if err != nil {
			return nil, err
		}

		parameters[keyVal.Key] = parsedValue
	}
	return parameters, nil
}

// ServiceParametersToHTTP converts the service representation of parameters to  HTTP
func ServiceParametersToHTTP(svcParameters []service.DeploymentParameter) []hm.KeyValue {
	var parameters = make([]hm.KeyValue, 0)
	if svcParameters == nil {
		return parameters
	}
	for _, param := range svcParameters {
		var paramValueAsString string
		switch paramVal := param.Value.(type) {
		case string:
			paramValueAsString = paramVal
		//case int:
		//case float32:
		//case float64:
		//case bool:
		default:
			marshal, err := json.Marshal(param.Value)
			if err != nil {
				return nil
			}
			paramValueAsString = string(marshal)
		}
		parameters = append(parameters, hm.KeyValue{
			Key:   param.Name,
			Value: paramValueAsString,
		})
	}
	return parameters
}

// ServiceStateViewToHTTP converts the service representation of state view to HTTP
func ServiceStateViewToHTTP(svcView service.DeploymentStateView) hm.DeploymentStateView {
	var httpResources []hm.DeploymentResource
	if svcView.Resources != nil {
		httpResources = make([]hm.DeploymentResource, 0, len(svcView.Resources))
		for _, resource := range svcView.Resources {
			httpResources = append(httpResources, ServiceResourceToHTTP(resource))
		}
	}
	return hm.DeploymentStateView{
		Resources: httpResources,
	}
}

// HTTPStateViewToService  converts the HTTP representation of state view to service
func HTTPStateViewToService(httpView hm.DeploymentStateView) service.DeploymentStateView {
	var svcResources []service.DeploymentResource
	if httpView.Resources != nil {
		svcResources = make([]service.DeploymentResource, 0, len(httpView.Resources))
		for _, resource := range httpView.Resources {
			svcResources = append(svcResources, HTTPResourceToService(resource))
		}
	}
	return service.DeploymentStateView{
		Resources: svcResources,
	}
}

// HTTPResourceToService converts the HTTP representation of resource to service
func HTTPResourceToService(resource hm.DeploymentResource) service.DeploymentResource {
	var sensitiveAttributes []interface{}
	// TODO the type of SensitiveAttributes is different between http and service, need to figure out why or fix it.
	if resource.SensitiveAttributes != nil {
		attributes, ok := resource.SensitiveAttributes.([]interface{})
		if !ok {
			sensitiveAttributes = nil
		} else {
			sensitiveAttributes = attributes
		}
	}
	return service.DeploymentResource{
		ID:                  resource.ID,
		Type:                service.DeploymentResourceType(resource.Type),
		ProviderType:        resource.ProviderType,
		Provider:            common.ID(resource.Provider),
		Attributes:          resource.Attributes,
		SensitiveAttributes: sensitiveAttributes,
		AvailableActions:    []service.DeploymentResourceAction{},
	}
}

// ServiceResourceToHTTP converts the service representation of resource to HTTP
func ServiceResourceToHTTP(resource service.DeploymentResource) hm.DeploymentResource {
	var httpActions []string
	if resource.AvailableActions != nil {
		httpActions = make([]string, 0, len(resource.AvailableActions))
		for _, action := range resource.AvailableActions {
			httpActions = append(httpActions, string(action))
		}
	}
	return hm.DeploymentResource{
		ID:                  resource.ID,
		Type:                string(resource.Type),
		ProviderType:        resource.ProviderType,
		Provider:            resource.Provider.String(),
		Attributes:          resource.Attributes,
		SensitiveAttributes: resource.SensitiveAttributes,
		AvailableActions:    httpActions,
	}
}
