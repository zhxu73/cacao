package deployments

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
)

func TestHTTPParametersToServiceParameterValues(t *testing.T) {
	type args struct {
		meta           service.TemplateMetadata
		httpParameters []hm.KeyValue
	}
	tests := []struct {
		name    string
		args    args
		want    service.DeploymentParameterValues
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "1 required param but no param values",
			args: args{
				meta: service.TemplateMetadata{
					Parameters: []service.TemplateParameter{
						{
							Name:               "param1",
							Type:               "string",
							Description:        "",
							Default:            nil, // no default, so the parameter is required
							Enum:               nil,
							DataValidationType: "",
						},
					},
				},
				httpParameters: nil,
			},
			want:    service.DeploymentParameterValues{},
			wantErr: assert.NoError, // let downstream service do the validation on whether required parameters are populated
		},
		{
			name: "2 required param but 1 has no param values",
			args: args{
				meta: service.TemplateMetadata{
					Parameters: []service.TemplateParameter{
						{
							Name:               "param1",
							Type:               "string",
							Description:        "",
							Default:            nil, // no default, so the parameter is required
							Enum:               nil,
							DataValidationType: "",
						},
						{
							Name:               "param2",
							Type:               "string",
							Description:        "",
							Default:            nil, // no default, so the parameter is required
							Enum:               nil,
							DataValidationType: "",
						},
					},
				},
				httpParameters: []hm.KeyValue{
					{
						Key:   "param1",
						Value: "foo",
					},
				},
			},
			want: service.DeploymentParameterValues{
				"param1": "foo",
			},
			wantErr: assert.NoError, // let downstream service do the validation on whether required parameters are populated
		},
		{
			name: "extra param values",
			args: args{
				meta: service.TemplateMetadata{
					Parameters: []service.TemplateParameter{
						{
							Name:               "param1",
							Type:               "string",
							Description:        "",
							Default:            nil,
							Enum:               nil,
							DataValidationType: "",
						},
					},
				},
				httpParameters: []hm.KeyValue{
					{
						Key:   "param1",
						Value: "foobar",
					},
					{
						Key:   "param2", // "param2" is not defined in template
						Value: "foobar",
					},
				},
			},
			want: service.DeploymentParameterValues{
				"param1": "foobar",
				// "param2" should be silently ignored
			},
			wantErr: assert.NoError,
		},
		{
			name: "miss-matched type, want bool, got number string",
			args: args{
				meta: service.TemplateMetadata{
					Parameters: []service.TemplateParameter{
						{
							Name:               "param1",
							Type:               "bool",
							Description:        "",
							Default:            nil,
							Enum:               nil,
							DataValidationType: "",
						},
					},
				},
				httpParameters: []hm.KeyValue{
					{
						Key:   "param1",
						Value: "123",
					},
				},
			},
			want:    nil,
			wantErr: assert.Error,
		},
		{
			name: "miss-matched type, want integer, got alphabet-string",
			args: args{
				meta: service.TemplateMetadata{
					Parameters: []service.TemplateParameter{
						{
							Name:               "param1",
							Type:               "integer",
							Description:        "",
							Default:            nil,
							Enum:               nil,
							DataValidationType: "",
						},
					},
				},
				httpParameters: []hm.KeyValue{
					{
						Key:   "param1",
						Value: "abcdefg",
					},
				},
			},
			want:    nil,
			wantErr: assert.Error,
		},
		{
			name: "miss-matched type, want float, got alphabet-string",
			args: args{
				meta: service.TemplateMetadata{
					Parameters: []service.TemplateParameter{
						{
							Name:               "param1",
							Type:               "float",
							Description:        "",
							Default:            nil,
							Enum:               nil,
							DataValidationType: "",
						},
					},
				},
				httpParameters: []hm.KeyValue{
					{
						Key:   "param1",
						Value: "abcdefg",
					},
				},
			},
			want:    nil,
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := HTTPParametersToServiceParameterValues(tt.args.meta, tt.args.httpParameters)
			if !tt.wantErr(t, err, fmt.Sprintf("HTTPParametersToServiceParameterValues(%v, %v)", tt.args.meta, tt.args.httpParameters)) {
				return
			}
			assert.Equalf(t, tt.want, got, "HTTPParametersToServiceParameterValues(%v, %v)", tt.args.meta, tt.args.httpParameters)
		})
	}
}
