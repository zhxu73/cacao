package openstackprovider

import (
	"context"
	"errors"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

// Client is an interface for interacting with the provider openstack microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(ctx context.Context, actor, emulator string) (Session, error)
}

// Session is an interface for interacting with the provider openstack microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	CreateApplicationCredential(providerID string, credential providers.CredentialOption, namePostfix string, scope providers.ProjectScope) (string, error)
	ApplicationCredentialList(providerID string, credential providers.CredentialOption) ([]providers.ApplicationCredential, error)
	GetApplicationCredential(providerID string, credential providers.CredentialOption, applicationCredentialID string) (*providers.ApplicationCredential, error)
	DeleteApplicationCredential(providerID string, credentialID string) error
	AuthenticationTest(providerID string, variables map[string]string) error
	CredentialList(providerID string, filter CredentialListFilter) ([]service.CredentialModel, error)
	RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error)
	ImageList(providerID string, credential providers.CredentialOption, region string) ([]providers.OpenStackImage, error)
	GetImage(providerID string, credential providers.CredentialOption, region string, imageID string) (*providers.OpenStackImage, error)
	FlavorList(providerID string, credential providers.CredentialOption, region string) ([]providers.Flavor, error)
	GetFlavor(providerID string, credential providers.CredentialOption, region string, flavorID string) (*providers.Flavor, error)
	ProjectList(providerID string, credential providers.CredentialOption) ([]providers.Project, error)
	GetProject(providerID string, credential providers.CredentialOption, projectID string) (*providers.Project, error)
	ZoneList(providerID string, credential providers.CredentialOption) ([]providers.DNSZone, error)
	RecordsetList(providerID string, credential providers.CredentialOption, zoneID string) ([]providers.DNSRecordset, error)
}

// CredentialListFilter is a set of filter options for listing credentials
type CredentialListFilter struct {
	ProjectID   string
	ProjectName string
}

// primary implementation of Client interface.
type providerClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// New creates a new provider openstack microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &providerClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}

// Session ...
func (c providerClient) Session(ctx context.Context, actor, emulator string) (Session, error) {
	if actor == "" {
		return nil, errors.New("no actor specified")
	}
	if ctx == nil {
		log.WithFields(log.Fields{
			"package":  "openstackprovider",
			"function": "providerClient.Session",
		}).Error("context is nil")
		return nil, errors.New("context is nil")
	}
	return providerSession{
		queryConn: c.queryConn,
		eventConn: c.eventConn,
		ctx:       ctx,
		actor: service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
	}, nil
}

// primary implementation of Session interface.
type providerSession struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
	ctx       context.Context
	actor     service.Actor
}

// CreateApplicationCredential ...
func (s providerSession) CreateApplicationCredential(providerID string, credential providers.CredentialOption, namePostfix string, scope providers.ProjectScope) (credID string, err error) {
	svcClient := s.getSvcClient(providerID)
	credID, err = svcClient.CreateApplicationCredential(s.ctx, s.actor, scope, namePostfix, credential)
	if err != nil {
		return "", err
	}
	return credID, nil
}

// ApplicationCredentialList ...
func (s providerSession) ApplicationCredentialList(providerID string, credential providers.CredentialOption) ([]providers.ApplicationCredential, error) {
	svcClient := s.getSvcClient(providerID)
	applicationCredentialList, err := svcClient.ApplicationCredentialList(s.ctx, s.actor, credential)
	if err != nil {
		return nil, err
	}
	return applicationCredentialList, nil
}

// GetApplicationCredential ...
func (s providerSession) GetApplicationCredential(providerID string, credential providers.CredentialOption, applicationCredentialID string) (*providers.ApplicationCredential, error) {
	svcClient := s.getSvcClient(providerID)
	applicationCredential, err := svcClient.GetApplicationCredential(s.ctx, s.actor, applicationCredentialID, credential)
	if err != nil {
		return nil, err
	}
	return applicationCredential, nil
}

// DeleteApplicationCredential ...
func (s providerSession) DeleteApplicationCredential(providerID string, credentialID string) error {
	svcClient := s.getSvcClient(providerID)
	err := svcClient.DeleteApplicationCredential(s.ctx, s.actor, credentialID)
	if err != nil {
		return err
	}
	return nil
}

// AuthenticationTest ...
func (s providerSession) AuthenticationTest(providerID string, variables map[string]string) error {
	svcClient := s.getSvcClient(providerID)
	err := svcClient.AuthenticationTest(s.ctx, s.actor, variables)
	if err != nil {
		return err
	}
	return nil
}

// CredentialList ...
func (s providerSession) CredentialList(providerID string, filter CredentialListFilter) ([]service.CredentialModel, error) {
	svcClient := s.getSvcClient(providerID)
	credList, err := svcClient.CredentialList(s.ctx, s.actor, filter.ProjectID, filter.ProjectName)
	if err != nil {
		return nil, err
	}
	return credList, nil
}

// RegionList ...
func (s providerSession) RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error) {
	svcClient := s.getSvcClient(providerID)
	regionList, err := svcClient.RegionList(s.ctx, s.actor, credential)
	if err != nil {
		return nil, err
	}
	return regionList, nil
}

// ImageList ...
func (s providerSession) ImageList(providerID string, credential providers.CredentialOption, region string) ([]providers.OpenStackImage, error) {
	svcClient := s.getSvcClient(providerID)
	imageList, err := svcClient.ImageList(s.ctx, s.actor, region, credential)
	if err != nil {
		return nil, err
	}
	return imageList, nil
}

// GetImage ...
func (s providerSession) GetImage(providerID string, credential providers.CredentialOption, region string, imageID string) (*providers.OpenStackImage, error) {
	svcClient := s.getSvcClient(providerID)
	image, err := svcClient.GetImage(s.ctx, s.actor, region, imageID, credential)
	if err != nil {
		return nil, err
	}
	return image, nil
}

// FlavorList ...
func (s providerSession) FlavorList(providerID string, credential providers.CredentialOption, region string) ([]providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavorList, err := svcClient.FlavorList(s.ctx, s.actor, region, credential)
	if err != nil {
		return nil, err
	}
	return flavorList, nil
}

// GetFlavor ...
func (s providerSession) GetFlavor(providerID string, credential providers.CredentialOption, region string, flavorID string) (*providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavor, err := svcClient.GetFlavor(s.ctx, s.actor, region, flavorID, credential)
	if err != nil {
		return nil, err
	}
	return flavor, nil
}

// ProjectList ...
func (s providerSession) ProjectList(providerID string, credential providers.CredentialOption) ([]providers.Project, error) {
	svcClient := s.getSvcClient(providerID)
	projectList, err := svcClient.ProjectList(s.ctx, s.actor, credential)
	if err != nil {
		return nil, err
	}
	return projectList, nil
}

// GetProject ...
func (s providerSession) GetProject(providerID string, credential providers.CredentialOption, projectID string) (*providers.Project, error) {
	svcClient := s.getSvcClient(providerID)
	project, err := svcClient.GetProject(s.ctx, s.actor, projectID, credential)
	if err != nil {
		return nil, err
	}
	return project, nil
}

// ZoneList ...
func (s providerSession) ZoneList(providerID string, credential providers.CredentialOption) ([]providers.DNSZone, error) {
	svcClient := s.getSvcClient(providerID)
	project, err := svcClient.ListZone(s.ctx, s.actor, credential)
	if err != nil {
		return nil, err
	}
	return project, nil
}

// RecordsetList ...
func (s providerSession) RecordsetList(providerID string, credential providers.CredentialOption, zoneID string) ([]providers.DNSRecordset, error) {
	svcClient := s.getSvcClient(providerID)
	if zoneID == "" {
		return svcClient.ListAllRecordset(s.ctx, s.actor, credential)
	}
	return svcClient.ListRecordset(s.ctx, s.actor, credential, zoneID)
}

func (s providerSession) getSvcClient(providerID string) providers.OpenStackProvider {
	client, err := providers.NewOpenStackProviderFromConn(common.ID(providerID), s.queryConn, s.eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return client
}
