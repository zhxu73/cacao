package api

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/providermetadata"
	providermetadatamocks "gitlab.com/cyverse/cacao/api-service/clients/providermetadata/mocks"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"net/http"
	"net/http/httptest"
	"runtime"
	"strings"
	"testing"
)

// testing if the correct handler from the correct provider API is being invoked.
// e.g. /providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/images should invoke ImageList() from AWS api.
func TestProviderSpecificAPIRouter_Get(t *testing.T) {
	type args struct {
		providerID               common.ID
		pathFormat               string
		providerSupportOperation bool
		expectedMethodName       string

		// this is used for provider ID that does not contain provider type, e.g. provider-co1g2rt9885fie78ffj0
		providerTypeReturnedByMetadataClient service.ProviderType
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "aws region list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/regions",
				providerSupportOperation: true,
				expectedMethodName:       "mockAWSProviderAPI.RegionList",
			},
		},
		{
			name: "aws image list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/images",
				providerSupportOperation: true,
				expectedMethodName:       "mockAWSProviderAPI.ImageList",
			},
		},
		{
			name: "aws flavor list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/flavors",
				providerSupportOperation: true,
				expectedMethodName:       "mockAWSProviderAPI.FlavorList",
			},
		},
		{
			name: "aws project list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/projects",
				providerSupportOperation: false, // aws does not support this operation
				expectedMethodName:       "",
			},
		},
		{
			name: "aws app cred list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/applicationCredentials",
				providerSupportOperation: false, // aws does not support this operation
				expectedMethodName:       "",
			},
		},
		{
			name: "openstack region list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/regions",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.RegionList",
			},
		},
		{
			name: "openstack image list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/images",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.ImageList",
			},
		},
		{
			name: "openstack flavor list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/flavors",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.FlavorList",
			},
		},
		{
			name: "openstack project list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/projects",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.ProjectList",
			},
		},
		{
			name: "openstack auth test",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/authenticationTest",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.AuthenticationTest",
			},
		},
		{
			name: "openstack app cred list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/applicationCredentials",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.ApplicationCredentialList",
			},
		},
		{
			name: "openstack region list with provider type lookup",
			args: args{
				providerID:                           "provider-d17e30598850n9abikl0",
				pathFormat:                           "/providers/%s/regions",
				providerSupportOperation:             true,
				expectedMethodName:                   "mockOpenStackAPI.RegionList",
				providerTypeReturnedByMetadataClient: "openstack",
			},
		},
		{
			name: "aws region list with provider type lookup",
			args: args{
				providerID:                           "provider-d17e30598850n9abikl0",
				pathFormat:                           "/providers/%s/regions",
				providerSupportOperation:             true,
				expectedMethodName:                   "mockAWSProviderAPI.RegionList",
				providerTypeReturnedByMetadataClient: "aws",
			},
		},
		{
			name: "openstack image list with provider type lookup",
			args: args{
				providerID:                           "provider-d17e30598850n9abikl0",
				pathFormat:                           "/providers/%s/images",
				providerSupportOperation:             true,
				expectedMethodName:                   "mockOpenStackAPI.ImageList",
				providerTypeReturnedByMetadataClient: "openstack",
			},
		},
		{
			name: "aws image list with provider type lookup",
			args: args{
				providerID:                           "provider-d17e30598850n9abikl0",
				pathFormat:                           "/providers/%s/images",
				providerSupportOperation:             true,
				expectedMethodName:                   "mockAWSProviderAPI.ImageList",
				providerTypeReturnedByMetadataClient: "aws",
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			const testActorUsername = "test_username"
			mockAWS := &mockAWSProviderAPI{}
			mockOS := &mockOpenStackAPI{}
			metadataClient := &providermetadatamocks.Client{}
			session := &providermetadatamocks.Session{}
			if tt.args.providerTypeReturnedByMetadataClient != "" {
				session.On("Get", mock.Anything, tt.args.providerID).Return(
					&service.ProviderModel{
						Type: tt.args.providerTypeReturnedByMetadataClient,
					},
					nil,
				).Once()
				metadataClient.On("Session", testActorUsername, "").Return(session, nil)
			}

			path := fmt.Sprintf(tt.args.pathFormat, tt.args.providerID)
			request := httptest.NewRequest(http.MethodGet, path, nil)
			request.Header.Set(constants.RequestHeaderCacaoUser, testActorUsername)
			request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
			recorder := createRecorderForProviderSpecificAPI(metadataClient, mockOS, mockAWS, request)
			metadataClient.AssertExpectations(t)
			if tt.args.providerTypeReturnedByMetadataClient != "" {
				session.AssertExpectations(t)
			}

			if tt.args.providerSupportOperation {
				// the mock method from below should be called (e.g. mockAWSProviderAPI.RegionList)
				assert.Equal(t, 200, recorder.Code)
				assert.Equal(t, tt.args.expectedMethodName, recorder.Body.String())
			} else {
				assert.Equal(t, http.StatusNotImplemented, recorder.Code)
				assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))
			}
		})
	}
}

func createRecorderForProviderSpecificAPI(metadataClient providermetadata.Client, osAPI OpenStackProviderAPI, awsAPI AWSProviderAPI, r *http.Request) *httptest.ResponseRecorder {
	router := mux.NewRouter()
	ProviderSpecificAPIRouter(metadataClient, osAPI, awsAPI, router)
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, r)
	return recorder
}

type mockAWSProviderAPI struct{}

func (m mockAWSProviderAPI) AuthenticationTest(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) CredentialList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) RegionList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

type mockOpenStackAPI struct{}

var _ providermetadata.Client = &mockOpenStackAPI{}

func (m mockOpenStackAPI) Session(actor, emulator string) (providermetadata.Session, error) {
	panic("should not be called")
}

func (m mockOpenStackAPI) CredentialList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) CreateApplicationCredential(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ApplicationCredentialList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetApplicationCredential(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) DeleteApplicationCredential(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) AuthenticationTest(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) RegionList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ProjectList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetProject(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ZoneList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) RecordsetList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

// this struct is for testing currentMethodName()
type Foo struct{}

func (f Foo) Bar() string {
	return currentMethodName()
}

func Test_currentMethodName(t *testing.T) {
	var foo Foo
	assert.Equal(t, "Foo.Bar", foo.Bar())
}

// currentMethodName returns the method name of the caller, e.g. "mockOpenStackAPI.GetProject"
func currentMethodName() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	splits := strings.Split(frame.Function, ".")
	return strings.Join(splits[len(splits)-2:], ".")
}
