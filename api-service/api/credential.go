package api

import (
	"fmt"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/api-service/clients/credentials"
	"io"
	"net/http"
	"time"
	"unicode"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

type credentialAPI struct {
	client credentials.Client
}

// CredentialAPIRouter creates routes for credential-related operations such as creating and
// updating credentials and logging in
func CredentialAPIRouter(client credentials.Client, router *mux.Router) {
	api := &credentialAPI{client: client}
	router.HandleFunc("/credentials", api.listCredentials).Methods("GET")
	router.HandleFunc("/credentials", api.createCredential).Methods("POST")
	router.HandleFunc("/credentials/{credentialid}", api.getCredential).Methods("GET")
	router.HandleFunc("/credentials/{credentialid}", api.updateCredential).Methods("PATCH")
	router.HandleFunc("/credentials/{credentialid}", api.deleteCredential).Methods("DELETE")
	router.HandleFunc("/credentials_types", api.getCredentialTypes).Methods("GET")
}

var credentialTypeList = []string{
	"openstack",
	"github",
	"gitlab",
	"dockerhub",
	"kubernetes",
	"ssh",
	"aws",
	"gcp",
	"azure",
}

func (api credentialAPI) getCredentialTypes(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "credentials_types",
	})
	logger.Info("api.getCredentialTypes(): start")
	utils.ReturnStatus(writer, credentialTypeList, http.StatusOK)
}

// delete credential via dependency mediator
func (api credentialAPI) deleteCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteCredential",
	})
	logger.Info("called")

	id := mux.Vars(request)["credentialid"]

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithError(err).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to delete credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	deletionResult, err := session.DeleteCredential(id)
	if err != nil {
		logger.WithError(err).Error("fail to delete credential with dependency check")
		utils.JSONError(writer, request, "internal error", err.Error(), http.StatusInternalServerError)
		return
	}
	logger.WithField("result", deletionResult).Info("deletion result")

	err = deletionResult.ToError("credential", id)
	if err != nil {
		logger.WithError(err).Error("error from deletion result")
		utils.JSONError(writer, request, "dependency conflict", err.Error(), http.StatusConflict)
		return
	}
	msg := map[string]string{"credential": id, "timestamp": time.Now().String()}
	utils.ReturnStatus(writer, msg, http.StatusOK)
}

func (api credentialAPI) updateCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateCredential",
	})
	logger.Info("called")

	id := mux.Vars(request)["credentialid"]

	credUpdate, err := api.parseUpdateRequest(logger, request)
	if err != nil {
		utils.JSONError(writer, request, "bad creation request", err.Error(), http.StatusBadRequest)
		return
	}
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithError(err).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to update credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	result, err := session.UpdateCredential(id, credUpdate)
	if err != nil {
		logger.WithError(err).Error("could not update credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to update credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	utils.ReturnStatus(writer, result, http.StatusOK)
}

func (api credentialAPI) parseUpdateRequest(logger *log.Entry, request *http.Request) (hm.CredentialUpdate, error) {

	reqBody, err := io.ReadAll(request.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		return hm.CredentialUpdate{}, fmt.Errorf("unable to read request body, %w", err)
	}
	var update hm.CredentialUpdate
	err = utils.StrictJSONDecode(reqBody, &update)
	if err != nil {
		logger.WithError(err).Error("json request body is invalid")
		return hm.CredentialUpdate{}, fmt.Errorf("json request body is invalid, %w", err)
	}
	return update, nil
}

func (api credentialAPI) getCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getCredential",
	})
	logger.Info("called")

	id := mux.Vars(request)["credentialid"]

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)
	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithError(err).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	cred, err := session.GetCredential(id)
	if err != nil {
		logger.WithError(err).Error("could not get credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(writer, cred, http.StatusOK)
}

func (api credentialAPI) createCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createCredential",
	})
	logger.Debug("called")

	credential, err := api.parseCreationRequest(logger, request)
	if err != nil {
		utils.JSONError(writer, request, "bad creation request", err.Error(), http.StatusBadRequest)
		return
	}
	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)
	credential.Username = actor // set owner

	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithError(err).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	result, err := session.CreateCredential(credential)
	if err != nil {
		logger.WithError(err).Error("could not create credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	utils.ReturnStatus(writer, result, http.StatusCreated)
}

func (api credentialAPI) parseCreationRequest(logger *log.Entry, request *http.Request) (hm.Credential, error) {
	reqBody, err := io.ReadAll(request.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		return hm.Credential{}, fmt.Errorf("unable to read request body, %w", err)
	}
	var credential hm.Credential
	err = utils.StrictJSONDecode(reqBody, &credential)
	if err != nil {
		logger.WithError(err).Error("json request body is invalid")
		return hm.Credential{}, fmt.Errorf("json request body is invalid, %w", err)
	}
	if credential.Name == "" {
		credential.Name = credential.ID // use ID as name if absent, this is to make it compatible with UI
	} else if credential.ID == "" {
		credential.ID = credential.Name // we still want to populate ID, so that service client does not error when it tries to verify locally
	}
	err = validateCredential(credential)
	if err != nil {
		logger.WithError(err).Error("credential is invalid")
		return hm.Credential{}, fmt.Errorf("bad credential, %w", err)
	}
	return credential, nil
}

func (api credentialAPI) listCredentials(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listCredentials",
	})
	logger.Debug("called")

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the deployments microservice client")
		utils.JSONError(writer, request, "unexpected error", err.Error(), http.StatusInternalServerError)
		return
	}

	credList, err := session.ListCredentials()
	if err != nil {
		logger.WithError(err).Error("could not list credentials")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	if err != nil {
		logger.WithError(err).Error("could not get credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	if len(credList) == 0 {
		writer.Header().Add("Content-Type", "application/json")
		writer.WriteHeader(200)
		writer.Write([]byte("[]"))
		return
	}

	utils.ReturnStatus(writer, credList, http.StatusOK)
}

func validateCredential(cred hm.Credential) error {
	err := validateCredentialName(cred.Name)
	if err != nil {
		return err
	}
	err = validateCredentialType(cred.Type)
	if err != nil {
		return err
	}
	if len(cred.Description) > 300 {
		return fmt.Errorf("description too long")
	}
	return nil
}

func validateCredentialName(credName string) error {
	if credName == "" {
		return fmt.Errorf("credential name cannot be empty")
	}
	if len(credName) > 80 {
		return fmt.Errorf("credential name too long")
	}
	for i, r := range credName {
		if r > unicode.MaxASCII {
			// only allow ASCII
			return fmt.Errorf("invalid character in credential name at index %d", i)
		}
		if r < ' ' || r > '~' {
			// check if it is outside of range of normal ASCII characters (not digit, not letter, not punct, not space)
			return fmt.Errorf("invalid character in credential name at index %d", i)
		}
	}
	return nil
}

func validateCredentialType(credType string) error {
	allowedType := false
	for _, typeName := range credentialTypeList {
		if credType == typeName {
			allowedType = true
			break
		}
	}
	if !allowedType {
		return fmt.Errorf("unknown credential type")
	}
	return nil
}
