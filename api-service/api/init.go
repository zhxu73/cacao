package api

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"time"
)

// InitAuthenticationDriver initializes the authentication driver
func InitAuthenticationDriver(c *config.Config) error {
	log.Trace("InitAuthentication(): start with driver = " + c.AuthDriver)
	var err error = nil
	// use a separate client ID for the user client
	config.GlobalConfig.NatsStanMsg.ClientID = config.GlobalConfig.NatsStanMsg.ClientID + "-auth"
	natsConn, err := config.GlobalConfig.NatsStanMsg.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
	}
	stanConn, err := config.GlobalConfig.NatsStanMsg.ConnectStan()
	if err != nil {
		log.WithError(err).Panic()
	}
	userServiceClient, err := service.NewNatsUserClientFromConn(&natsConn, &stanConn)
	if err != nil {
		return err
	}
	tokenServiceClient, err := service.NewNatsTokenClientFromConn(&natsConn, &stanConn)
	if err != nil {
		return err
	}

	CacaoAPITokenAuthenticator, err = authentication.NewCacaoTokenAuth(*c, userServiceClient, tokenServiceClient, func() time.Time {
		return time.Now()
	})
	if err != nil {
		return err
	}

	switch c.AuthDriver {
	case constants.AuthDriverCILogon:
		if c.OAuth2ClientID == "" || c.OAuth2ClientSecret == "" {
			log.Panic("CILogon client ID and secret should not be empty")
		}

		Authenticator, err = authentication.NewCILogonOAuth2Driver(*c, userServiceClient)
	case constants.AuthDriverGlobus:
		if c.OAuth2ClientID == "" || c.OAuth2ClientSecret == "" {
			log.Panic("Globus client ID and secret should not be empty")
		}

		Authenticator, err = authentication.NewGlobusOAuth2Driver(*c, userServiceClient)
	case constants.AuthDriverKeycloak:
		if c.OAuth2ClientID == "" || c.OAuth2ClientSecret == "" {
			log.Panic("Keycloak ID and client secret should not be empty")
		}

		Authenticator, err = authentication.NewKeycloakAuth(*c, userServiceClient)
	default:
		Authenticator = authentication.NewSimpleTokenAuth(*c, userServiceClient)
	}

	return err
}
