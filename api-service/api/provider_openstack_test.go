package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"
	mockclient "gitlab.com/cyverse/cacao/api-service/clients/openstackprovider/mocks"
	"gitlab.com/cyverse/cacao/api-service/constants"
)

type FakeProvider struct {
	ID common.ID
}

func (f *FakeProvider) Init() error {
	return nil
}

var expectedApplicationCredentials = []providers.ApplicationCredential{
	{
		ID:           "00000000-1111-2222-aaaa-abcdefabcdef",
		Name:         "cred1",
		Description:  "first cred",
		ExpiresAt:    "2022-01-01",
		ProjectID:    "12345678-1111-2222-aaaa-abcdefabcdef",
		Roles:        nil,
		Unrestricted: true,
	},
	{
		ID:           "00000000-1111-2222-bbbb-abcdefabcdef",
		Name:         "cred2",
		Description:  "second cred",
		ExpiresAt:    "2022-01-01",
		ProjectID:    "12345678-1111-2222-bbbb-abcdefabcdef",
		Roles:        nil,
		Unrestricted: true,
	},
	{
		ID:           "00000000-1111-2222-cccc-abcdefabcdef",
		Name:         "cred3",
		Description:  "third cred",
		ExpiresAt:    "2022-01-01",
		ProjectID:    "12345678-1111-2222-cccc-abcdefabcdef",
		Roles:        nil,
		Unrestricted: true,
	},
}

var expectedImages = []providers.OpenStackImage{
	{
		Image: providers.Image{
			ID:     "85effb53-9842-49cc-9c5f-f26b4a2bf08c",
			Name:   "CentOS-6.5-x86_64-bin-DVD1.iso",
			Status: "active",
		},

		DiskFormat:      "iso",
		ContainerFormat: "bare",

		Checksum:   "83221db52687c7b857e65bfe60787838",
		Size:       4467982336,
		Visibility: "public",
		Protected:  false,
		Project:    "74d53196e919420b8a23b6fd286cab63",
	},
	{
		Image: providers.Image{
			ID:     "bff03fee-1886-4e4c-8d18-02f7e2e8ac96",
			Name:   "centos-7-x86_64-dvd-1708.iso",
			Status: "active",
		},

		DiskFormat:      "iso",
		ContainerFormat: "bare",

		Checksum:   "82b4160df8d2a360f0f38432ad7e049b",
		Size:       4521459712,
		Visibility: "public",
		Protected:  false,
		Project:    "74d53196e919420b8a23b6fd286cab63",
	},
	{
		Image: providers.Image{
			ID:     "faca47d6-0dc0-4871-b444-f2114fc88bb0",
			Name:   "CentOS-7-x86_64-DVD-1708.iso",
			Status: "queued",
		},

		DiskFormat:      "qcow2",
		ContainerFormat: "bare",

		Checksum: "82b4160df8d2a360f0f38432ad7e049b",

		Size:       4521459712,
		Visibility: "public",
		Protected:  false,
		Project:    "74d53196e919420b8a23b6fd286cab63",
	},
}

var expectedFlavors = []providers.Flavor{
	{
		Name:      "large3",
		RAM:       65536,
		Ephemeral: 0,
		VCPUs:     8,
		IsPublic:  true,
		Disk:      0,
		ID:        "000d1872-c4a7-11eb-8529-0242ac130003",
	},
	{
		Name:      "xlarge1",
		RAM:       32768,
		Ephemeral: 0,
		VCPUs:     16,
		IsPublic:  true,
		Disk:      0,
		ID:        "08daa0be-c4a7-11eb-8529-0242ac130003",
	},
	{
		Name:      "xxlarge1",
		RAM:       65536,
		Ephemeral: 0,
		VCPUs:     32,
		IsPublic:  true,
		Disk:      0,
		ID:        "12492d6e-c4a7-11eb-8529-0242ac130003",
	},
}

var expectedProjects = []providers.Project{
	{
		ID:          "abcdef12-1111-2222-aaaa-abcdefabcdef",
		Name:        "project1",
		Description: "first project",
		DomainID:    "dddddd12-1111-2222-aaaa-abcdefabcdef",
		Enabled:     true,
		IsDomain:    false,
		ParentID:    "ffffeedd-1111-2222-aaaa-abcdefabcdef",
		Properties:  nil,
	},
	{
		ID:          "abcdef12-1111-3333-aaaa-abcdefabcdef",
		Name:        "project2",
		Description: "second project",
		DomainID:    "dddddd12-1111-3333-aaaa-abcdefabcdef",
		Enabled:     true,
		IsDomain:    false,
		ParentID:    "ffffeedd-1111-3333-aaaa-abcdefabcdef",
		Properties:  nil,
	},
	{
		ID:          "abcdef12-1111-4444-aaaa-abcdefabcdef",
		Name:        "project3",
		Description: "third project",
		DomainID:    "dddddd12-1111-4444-aaaa-abcdefabcdef",
		Enabled:     true,
		IsDomain:    false,
		ParentID:    "ffffeedd-1111-4444-aaaa-abcdefabcdef",
		Properties:  nil,
	},
}

func createRecorderForOpenStackProvider(client openstackprovider.Client, r *http.Request) *httptest.ResponseRecorder {
	router := mux.NewRouter()
	// Note we do not need provider metadata client, because we only test openstack
	// API here, metadata client is used to determine provider type if the type is
	// missing from provider ID
	ProviderSpecificAPIRouter(nil, NewOpenStackProviderAPI(client), &mockAWSProviderAPI{}, router)
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, r)
	return recorder
}

func matchCredentialIDOption(t *testing.T, expectedCredID string) interface{} {
	return mock.MatchedBy(func(option providers.CredentialOption) bool {
		credential := option()
		if !assert.Equal(t, providers.ProviderCredentialID, credential.Type) {
			return false
		}
		if !assert.IsType(t, "", credential.Data) {
			return false
		}
		credID := credential.Data.(string)
		return assert.Equal(t, expectedCredID, credID)
	})
}

func matchOpenStackTokenCredentialOption(t *testing.T, expectedToken providers.OpenStackTokenCredential) interface{} {
	return mock.MatchedBy(func(option providers.CredentialOption) bool {
		credential := option()
		if !assert.Equal(t, providers.ProviderOpenStackTokenCredential, credential.Type) {
			return false
		}
		if !assert.IsType(t, providers.OpenStackTokenCredential{}, credential.Data) {
			return false
		}
		tokenCred := credential.Data.(providers.OpenStackTokenCredential)
		return assert.Equal(t, expectedToken, tokenCred)
	})
}

func (f *FakeProvider) ApplicationCredentialList(ctx context.Context, session *service.Session) ([]providers.ApplicationCredential, error) {
	return expectedApplicationCredentials, nil
}

func (f *FakeProvider) GetApplicationCredential(ctx context.Context, session *service.Session, id string) (*providers.ApplicationCredential, error) {
	credentials, _ := f.ApplicationCredentialList(ctx, session)
	for _, credential := range credentials {
		if credential.ID == id {
			return &credential, nil
		}
	}
	return nil, errors.New("credential not found")
}

func (f *FakeProvider) ImageList(ctx context.Context, session *service.Session) ([]providers.OpenStackImage, error) {
	return expectedImages, nil

}

func (f *FakeProvider) GetImage(ctx context.Context, session *service.Session, id string) (*providers.OpenStackImage, error) {
	images, _ := f.ImageList(ctx, session)
	for _, image := range images {
		if image.ID == id {
			return &image, nil
		}
	}
	return nil, errors.New("image not found")
}

func (f *FakeProvider) FlavorList(ctx context.Context, session *service.Session) ([]providers.Flavor, error) {
	return expectedFlavors, nil
}

func (f *FakeProvider) GetFlavor(ctx context.Context, session *service.Session, id string) (*providers.Flavor, error) {
	flavors, _ := f.FlavorList(ctx, session)
	for _, flavor := range flavors {
		if flavor.ID == id {
			return &flavor, nil
		}
	}
	return nil, errors.New("flavor not found")
}

func (f *FakeProvider) ProjectList(ctx context.Context, session *service.Session) ([]providers.Project, error) {
	return expectedProjects, nil
}

func (f *FakeProvider) GetProject(ctx context.Context, session *service.Session, id string) (*providers.Project, error) {
	projects, _ := f.ProjectList(ctx, session)
	for _, project := range projects {
		if project.ID == id {
			return &project, nil
		}
	}
	return nil, errors.New("project not found")
}

func (f *FakeProvider) Finish() error {
	return nil
}

func createMockClientForOpenStackProvider(session *mockclient.Session) *mockclient.Client {
	client := &mockclient.Client{}
	client.On("Session", mock.Anything, "test_username", "").Return(session, nil)
	return client
}

func TestListApplicationCredentials(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
	)
	session := &mockclient.Session{}
	session.On("ApplicationCredentialList", providerID, matchCredentialIDOption(t, credID)).Return(expectedApplicationCredentials, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	client.AssertExpectations(t)
	session.AssertExpectations(t)
	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.ApplicationCredential
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedApplicationCredentials, actual)
		}
	}
}

func TestListImages(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
		region     = "region-123"
	)
	session := &mockclient.Session{}
	session.On("ImageList", providerID, matchCredentialIDOption(t, credID), region).Return(expectedImages, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/images", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	queryParams := request.URL.Query()
	queryParams.Set("region", region)
	request.URL.RawQuery = queryParams.Encode()
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	client.AssertExpectations(t)
	session.AssertExpectations(t)
	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.OpenStackImage
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedImages, actual)
		}
	}
}

func TestListFlavors(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
		region     = "region-123"
	)
	session := &mockclient.Session{}
	session.On("FlavorList", providerID, matchCredentialIDOption(t, credID), region).Return(expectedFlavors, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/flavors", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	queryParams := request.URL.Query()
	queryParams.Set("region", region)
	request.URL.RawQuery = queryParams.Encode()
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Flavor
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedFlavors, actual)
		}
	}
}

func TestListProjects(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
	)
	session := &mockclient.Session{}
	session.On("ProjectList", providerID, matchCredentialIDOption(t, credID)).Return(expectedProjects, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/projects", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Project
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedProjects, actual)
		}
	}
}

func TestListProjectsWithTokenNoScope(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()

		openstackToken = "token-123"
	)
	session := &mockclient.Session{}
	session.On("ProjectList", providerID, matchOpenStackTokenCredentialOption(t, providers.OpenStackTokenCredential{
		Token: openstackToken,
		Scope: providers.ProjectScope{},
	})).Return(expectedProjects, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/projects", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	q := make(url.Values)
	q.Set("openstacktoken", openstackToken)
	request.URL.RawQuery = q.Encode()
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Project
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedProjects, actual)
		}
	}
}

func TestListProjectsWithTokenAndScope(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()

		openstackToken            = "token-123"
		openstackTokenProjectName = "project-123"
		openstackTokenDomainName  = "domain-123"
	)
	session := &mockclient.Session{}
	session.On("ProjectList", providerID, matchOpenStackTokenCredentialOption(t, providers.OpenStackTokenCredential{
		Token: openstackToken,
		Scope: providers.ProjectScope{
			Project: struct {
				Name string `json:"name,omitempty"`
				ID   string `json:"id,omitempty"`
			}{
				Name: openstackTokenProjectName,
			},
			Domain: struct {
				Name string `json:"name,omitempty"`
				ID   string `json:"id,omitempty"`
			}{
				Name: openstackTokenDomainName,
			},
		},
	})).Return(expectedProjects, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/projects", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	q := make(url.Values)
	q.Set("openstacktoken", openstackToken)
	q.Set("domain-name", openstackTokenDomainName)
	q.Set("project-name", openstackTokenProjectName)
	request.URL.RawQuery = q.Encode()
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Project
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedProjects, actual)
		}
	}
}

func TestGetApplicationCredential(t *testing.T) {
	var (
		providerID              = common.NewID("provider-openstack").String()
		credID                  = ""
		applicationCredentialID = "bff03fee-1886-4e4c-8d18-02f7e2e8ac96"
	)
	session := &mockclient.Session{}
	session.On("GetApplicationCredential", providerID, matchCredentialIDOption(t, credID), applicationCredentialID).Return(&expectedApplicationCredentials[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials/%s", providerID, applicationCredentialID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual providers.ApplicationCredential
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedApplicationCredentials[1], actual)
		}
	}
}

func TestGetImage(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
		region     = "region-123"
		imageID    = "bff03fee-1886-4e4c-8d18-02f7e2e8ac96"
	)
	session := &mockclient.Session{}
	session.On("GetImage", providerID, matchCredentialIDOption(t, credID), region, imageID).Return(&expectedImages[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/images/%s", providerID, imageID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	queryParams := request.URL.Query()
	queryParams.Set("region", region)
	request.URL.RawQuery = queryParams.Encode()
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual providers.OpenStackImage
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedImages[1], actual)
		}
	}
}

func TestGetFlavor(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
		region     = "region-123"
		flavorID   = "08daa0be-c4a7-11eb-8529-0242ac130003"
	)
	session := &mockclient.Session{}
	session.On("GetFlavor", providerID, matchCredentialIDOption(t, credID), region, flavorID).Return(&expectedFlavors[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/flavors/%s", providerID, flavorID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	queryParams := request.URL.Query()
	queryParams.Set("region", region)
	request.URL.RawQuery = queryParams.Encode()
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		actual := providers.Flavor{}
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedFlavors[1], actual)
		}
	}
}

func TestGetProject(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = ""
		projectID  = "08daa0be-c4a7-11eb-8529-0242ac130003"
	)
	session := &mockclient.Session{}
	session.On("GetProject", providerID, matchCredentialIDOption(t, credID), projectID).Return(&expectedProjects[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/projects/%s", providerID, projectID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		actual := providers.Project{}
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedProjects[1], actual)
		}
	}
}

func TestCreateApplicationCredential(t *testing.T) {
	var (
		providerID           = common.NewID("provider-openstack").String()
		credID               = ""
		namePostfix          = "postfix-123"
		openstackProjectName = "project-name-123"
		openstackDomainName  = "domain-name-123"
		newCredID            = "new-cred-id-123"
	)
	session := &mockclient.Session{}
	session.On("CreateApplicationCredential", providerID, matchCredentialIDOption(t, credID), namePostfix, providers.ProjectScope{
		Project: struct {
			Name string `json:"name,omitempty"`
			ID   string `json:"id,omitempty"`
		}{
			Name: openstackProjectName,
		},
		Domain: struct {
			Name string `json:"name,omitempty"`
			ID   string `json:"id,omitempty"`
		}{
			Name: openstackDomainName,
		},
	}).Return(newCredID, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials", providerID)
	requestBody := map[string]interface{}{
		"name": namePostfix,
		"scope": map[string]interface{}{
			"project": map[string]interface{}{
				"name": openstackProjectName,
			},
			"domain": map[string]interface{}{
				"name": openstackDomainName,
			},
		},
	}
	var requestBodyBytes bytes.Buffer
	err := json.NewEncoder(&requestBodyBytes).Encode(requestBody)
	if err != nil {
		panic(err)
	}
	request := httptest.NewRequest(http.MethodPost, path, &requestBodyBytes)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual struct {
			ID string `json:"id"`
		}
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, newCredID, actual.ID)
		}
	}
}

func TestCreateApplicationCredential_WithTokenNoScope(t *testing.T) {
	var (
		providerID           = common.NewID("provider-openstack").String()
		openstackToken       = "token-123"
		namePostfix          = "postfix-123"
		openstackProjectName = "project-name-123"
		openstackDomainName  = "domain-name-123"
		newCredID            = "new-cred-id-123"
	)
	session := &mockclient.Session{}
	expectedTokenCred := providers.OpenStackTokenCredential{Token: openstackToken, Scope: providers.ProjectScope{}}
	session.On("CreateApplicationCredential", providerID, matchOpenStackTokenCredentialOption(t, expectedTokenCred), namePostfix, providers.ProjectScope{
		Project: struct {
			Name string `json:"name,omitempty"`
			ID   string `json:"id,omitempty"`
		}{
			Name: openstackProjectName,
		},
		Domain: struct {
			Name string `json:"name,omitempty"`
			ID   string `json:"id,omitempty"`
		}{
			Name: openstackDomainName,
		},
	}).Return(newCredID, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials", providerID)
	requestBody := map[string]interface{}{
		"name": namePostfix,
		"scope": map[string]interface{}{
			"project": map[string]interface{}{
				"name": openstackProjectName,
			},
			"domain": map[string]interface{}{
				"name": openstackDomainName,
			},
		},
	}
	var requestBodyBytes bytes.Buffer
	err := json.NewEncoder(&requestBodyBytes).Encode(requestBody)
	if err != nil {
		panic(err)
	}
	request := httptest.NewRequest(http.MethodPost, path, &requestBodyBytes)
	q := make(url.Values)
	q.Set("openstacktoken", openstackToken)
	request.URL.RawQuery = q.Encode()
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actualResponseBody struct {
			ID string `json:"id"`
		}
		err = json.Unmarshal(body, &actualResponseBody)
		if assert.NoError(t, err) {
			assert.Equal(t, newCredID, actualResponseBody.ID)
		}
	}
}

func TestDeleteApplicationCredential(t *testing.T) {
	var (
		providerID = common.NewID("provider-openstack").String()
		credID     = "app-cred-id-123"
	)
	session := &mockclient.Session{}
	session.On("DeleteApplicationCredential", providerID, credID).Return(nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials/%s", providerID, credID)
	request := httptest.NewRequest(http.MethodDelete, path, nil)
	q := make(url.Values)
	request.URL.RawQuery = q.Encode()
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actualResponseBody struct {
			ID string `json:"id"`
		}
		err = json.Unmarshal(body, &actualResponseBody)
		if assert.NoError(t, err) {
			assert.Equal(t, credID, actualResponseBody.ID)
		}
	}
}
