package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/storage"
	"gitlab.com/cyverse/cacao/api-service/clients/storage/mocks"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// newDeployment returns a new deployment for testing.
func newDeployment(templateID string) hm.Deployment {
	return hm.Deployment{
		ID:                cacaocommon.NewID("deployment").String(),
		Owner:             "test_username",
		Name:              "Some Test Deployment Name",
		Description:       "Some Test Deployment Description",
		WorkspaceID:       cacaocommon.NewID("workspace").String(),
		TemplateID:        templateID,
		PrimaryProviderID: cacaocommon.NewID("provider").String(),
		CreatedAt:         time.Now().Add(-time.Hour * 2).Truncate(time.Second),
		UpdatedAt:         time.Now().Add(-time.Hour).Truncate(time.Second),
		CurrentStatus:     service.DeploymentStatusActive.String(),
	}
}

// processStorageRequest processes a request and returns a recorder containing information about the HTTP response.
func processStorageRequest(
	client storage.Client, request *http.Request,
) *httptest.ResponseRecorder {
	recorder := httptest.NewRecorder()
	router := mux.NewRouter()
	StorageAPIRouter(client, router)
	router.ServeHTTP(recorder, request)
	return recorder
}

// processStorageRequestForDefaultUser creates and processes a request with the default CACAO auth headers for testing.
// A recorder containing information about hte HTTP response is returned.
func processStorageRequestForDefaultUser(
	assert *assert.Assertions,
	client storage.Client,
	method, urlPath string,
	body []byte,
) *httptest.ResponseRecorder {
	request := createRequestForDefaultUser(assert, method, urlPath, body)
	return processStorageRequest(client, request)
}

// generateDeployment generates a single deployment for testing.
func generateDeployment() hm.Deployment {
	return hm.Deployment{
		ID:                cacaocommon.NewID("deployment").String(),
		Owner:             "test_username",
		Name:              "Some Test Deployment Name",
		Description:       "Some Test Deployment Description",
		WorkspaceID:       cacaocommon.NewID("workspace").String(),
		TemplateID:        cacaocommon.NewID("template").String(),
		PrimaryProviderID: cacaocommon.NewID("provider").String(),
		CreatedAt:         time.Now().Add(-time.Hour * 2).Truncate(time.Second),
		UpdatedAt:         time.Now().Add(-time.Hour).Truncate(time.Second),
		CurrentStatus:     service.DeploymentStatusActive.String(),
	}
}

// generateDeployments generates a slice of deployments for testing.
func generateDeployments(count int) []hm.Deployment {
	deployments := make([]hm.Deployment, count)
	for i := range deployments {
		deployments[i] = generateDeployment()
	}
	return deployments
}

// storageListingTestCase defines a single test case for the GET /storage endpoint.
type storageListingTestCase struct {
	name                string
	sessionError        error
	storageDeployments  []hm.Deployment
	storageListingError error
	expectedStatusCode  int
}

func TestStorageListing(t *testing.T) {
	testCases := make([]storageListingTestCase, 4)

	// An error occurs while getting the session.
	testCases[0] = storageListingTestCase{
		name:               "session error",
		sessionError:       fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// An error occurs while listing storage deployments.
	testCases[1] = storageListingTestCase{
		name:                "storage listing error",
		storageListingError: fmt.Errorf("something bad happened"),
		expectedStatusCode:  http.StatusInternalServerError,
	}

	// No storage deployments exist.
	testCases[2] = storageListingTestCase{
		name:               "no storage deployments exist",
		storageDeployments: []hm.Deployment{},
		expectedStatusCode: http.StatusOK,
	}

	// Successful storage deployment listing.
	testCases[3] = storageListingTestCase{
		name:               "succcessful storage deployment listing",
		storageDeployments: generateDeployments(3),
		expectedStatusCode: http.StatusOK,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the session.
			session := new(mocks.Session)
			if tc.sessionError == nil {
				session.On("ListStorageDeployments").
					Return(tc.storageDeployments, tc.storageListingError)
			}

			// Create the client.
			client := new(mocks.Client)
			client.On("Session", "test_username", "", false).
				Return(session, tc.sessionError)

			// Process the request.
			recorder := processStorageRequestForDefaultUser(assert, client, "GET", "/storage", nil)

			// Verify that all expectations were met.
			client.AssertExpectations(t)
			session.AssertExpectations(t)

			// Verify that the expected status code was returned.
			assert.Equal(tc.expectedStatusCode, recorder.Code)

			// If there were some storage deployments, verify that we got the expected response body.
			if tc.storageDeployments != nil {
				expected, err := json.Marshal(tc.storageDeployments)
				assert.NoError(err)
				assert.Equal(expected, recorder.Body.Bytes())
			}
		})
	}
}

// storageCreationTestCase represents a single test case for the POST /storage endpoint.
type storageCreationTestCase struct {
	name                 string
	requestBody          []byte
	expectUnmarshalError bool
	sessionError         error
	validationError      error
	creationError        error
	deploymentID         cacaocommon.ID
	runError             error
	deletionError        error
	expectedStatusCode   int
}

// ExpectValidationCall returns true if the valiation method should be called.
func (tc storageCreationTestCase) ExpectValidationCall() bool {
	switch {
	case tc.expectUnmarshalError:
		return false
	case tc.sessionError != nil:
		return false
	default:
		return true
	}
}

// ExpectCreationCall returns true if the deployment creation method should be called.
func (tc storageCreationTestCase) ExpectCreationCall() bool {
	switch {
	case !tc.ExpectValidationCall():
		return false
	case tc.validationError != nil:
		return false
	default:
		return true
	}
}

// ExpectRunCall returns true if the deployment run method should be called.
func (tc storageCreationTestCase) ExpectRunCall() bool {
	switch {
	case !tc.ExpectCreationCall():
		return false
	case tc.creationError != nil:
		return false
	default:
		return true
	}
}

// GetRunRequest returns the deployment run request that should be used for the test case.
func (tc storageCreationTestCase) GetRunRequest() (*hm.DeploymentRun, error) {
	var deployment hm.Deployment
	err := json.Unmarshal(tc.requestBody, &deployment)
	if err != nil {
		return nil, err
	}
	deploymentRunRequest := hm.DeploymentRun{
		Parameters: deployment.Parameters,
	}
	return &deploymentRunRequest, nil
}

// ExpectRunDeletionCall returns true if the deployment run deletion method should be called.
func (tc storageCreationTestCase) ExpectRunDeletionCall() bool {
	return tc.ExpectRunCall() && tc.runError != nil
}

// ExpectSuccess returns true if the deployment should be created successfully.
func (tc storageCreationTestCase) expectSuccess() bool {
	switch {
	case !tc.ExpectRunCall():
		return false
	case tc.runError != nil:
		return false
	default:
		return true
	}
}

// TestStorageCreation runs the test cases for the POST /storage endpoint.
func TestStorageCreation(t *testing.T) {
	testCases := make([]storageCreationTestCase, 7)

	// The request body can't be unmarshaled.
	testCases[0] = storageCreationTestCase{
		name:                 "invalid request body",
		requestBody:          []byte("{"),
		expectUnmarshalError: true,
		expectedStatusCode:   http.StatusBadRequest,
	}

	// A deployment for subsequent requests.
	deployment := newDeployment(string(cacaocommon.NewID("template")))
	deploymentJSON, _ := json.Marshal(deployment)

	// An error occurred when creating the session.
	testCases[1] = storageCreationTestCase{
		name:               "session error",
		requestBody:        deploymentJSON,
		sessionError:       fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The request could not be validated.
	testCases[2] = storageCreationTestCase{
		name:               "request validation failure",
		requestBody:        deploymentJSON,
		validationError:    service.NewCacaoInvalidParameterError("something bad happened"),
		expectedStatusCode: http.StatusBadRequest,
	}

	// The request was validated successfully, but an error occurred when we tried to add the deployment.
	testCases[3] = storageCreationTestCase{
		name:               "storage deployment creation failure",
		requestBody:        deploymentJSON,
		creationError:      fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The storage deployment was created successfully, but the deployment run creation failed.
	testCases[4] = storageCreationTestCase{
		name:               "storage deployment run failure",
		requestBody:        deploymentJSON,
		runError:           fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The storage deployment run creation failed, and the deployment couldn't be deleted.
	testCases[5] = storageCreationTestCase{
		name:               "storage deployment run and deletion failure",
		requestBody:        deploymentJSON,
		runError:           fmt.Errorf("something bad happened"),
		deletionError:      fmt.Errorf("another bad thing happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The storage deployment was created successfully.
	testCases[6] = storageCreationTestCase{
		name:               "storage deployment creation success",
		requestBody:        deploymentJSON,
		expectedStatusCode: http.StatusAccepted,
		deploymentID:       cacaocommon.NewID("deployment"),
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the session.
			session := new(mocks.Session)
			if tc.ExpectValidationCall() {
				var request hm.Deployment
				err := json.Unmarshal(tc.requestBody, &request)
				assert.NoError(err)
				session.On("ValidateStorageDeploymentCreationRequest", &request).
					Return(tc.validationError)
				if tc.ExpectCreationCall() {
					session.On("AddStorageDeployment", &request).
						Return(tc.deploymentID, tc.creationError)
				}
				if tc.ExpectRunCall() {
					runRequest, err := tc.GetRunRequest()
					assert.NoError(err)
					session.On("RunStorageDeployment", tc.deploymentID, runRequest).
						Return(cacaocommon.NewID("run"), tc.runError)
				}
				if tc.ExpectRunDeletionCall() {
					session.On("DeleteStorageDeployment", tc.deploymentID).
						Return(cacaocommon.NewID("deployment"), true, tc.deletionError)
				}
			}

			// Create the client.
			client := new(mocks.Client)
			if !tc.expectUnmarshalError {
				client.On("Session", "test_username", "", false).
					Return(session, tc.sessionError)
			}

			// Process the request.
			recorder := processStorageRequestForDefaultUser(assert, client, "POST", "/storage", tc.requestBody)

			// Verify that all expectations were met.
			client.AssertExpectations(t)
			session.AssertExpectations(t)

			// Verify that the expected status code was returned.
			assert.Equal(tc.expectedStatusCode, recorder.Code)

			// Check the response body if we expect the request to succeed.
			if tc.expectSuccess() {
				var response utils.AcceptedResponse
				assert.NoError(json.Unmarshal(recorder.Body.Bytes(), &response))
				assert.Equal(tc.deploymentID.String(), response.ID)
				assert.Equal(cacaocommon.TransactionID(""), response.TransactionID)
			}
		})
	}
}

// storageRetrievalTestCase defines a single test case for the GET /storage/{storage_id} endpoint.
type storageRetrievalTestCase struct {
	name                  string
	storageID             string
	invalidStorageID      bool
	sessionError          error
	storageDeployment     hm.Deployment
	storageRetrievalError error
	expectedStatusCode    int
}

// ExpectSessionCall returns true if there should be an attempt to create a session for the test case.
func (tc storageRetrievalTestCase) ExpectSessionCall() bool {
	return !tc.invalidStorageID
}

// ExpectRetrievalCall returns true if there should be an attempt to retrive the storage deployment for this test case.
func (tc storageRetrievalTestCase) ExpectRetrievalCall() bool {
	return tc.ExpectSessionCall() && tc.sessionError == nil
}

// ExpectSuccess returns true if we expect the request in the test case to succeed.
func (tc storageRetrievalTestCase) ExpectSuccess() bool {
	return tc.ExpectRetrievalCall() && tc.storageRetrievalError == nil
}

// TestGetStorageDeployment runs the test cases for the GET /storage/{storage_id} endpoint.
func TestGetStorageDeployment(t *testing.T) {
	testCases := make([]storageRetrievalTestCase, 5)

	// A deployment for testing.
	deployment := newDeployment(string(cacaocommon.NewID("deployment")))

	// The case where the storage ID is invalid.
	testCases[0] = storageRetrievalTestCase{
		name:               "invalid storage ID",
		storageID:          "quux",
		invalidStorageID:   true,
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where the session can't be created.
	testCases[1] = storageRetrievalTestCase{
		name:               "unable to create the session",
		storageID:          deployment.ID,
		sessionError:       fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The case where an error occurs while trying to fetch the deployment.
	testCases[2] = storageRetrievalTestCase{
		name:                  "storage retrieval error",
		storageID:             deployment.ID,
		storageDeployment:     deployment,
		storageRetrievalError: fmt.Errorf("something bad happened"),
		expectedStatusCode:    http.StatusInternalServerError,
	}

	// The case where the storage deployment can't be found.
	testCases[3] = storageRetrievalTestCase{
		name:                  "storage deployment not found",
		storageID:             deployment.ID,
		storageDeployment:     deployment,
		storageRetrievalError: service.NewCacaoNotFoundError("storage deployment not found"),
		expectedStatusCode:    http.StatusNotFound,
	}

	// The case where the storage deployment retrieval succeeds.
	testCases[4] = storageRetrievalTestCase{
		name:               "storage retrieval success",
		storageID:          deployment.ID,
		storageDeployment:  deployment,
		expectedStatusCode: http.StatusOK,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the session.
			session := new(mocks.Session)
			if tc.ExpectRetrievalCall() {
				session.On("GetStorageDeployment", cacaocommon.ID(tc.storageID)).
					Return(tc.storageDeployment, tc.storageRetrievalError)
			}

			// Create the client.
			client := new(mocks.Client)
			if tc.ExpectSessionCall() {
				client.On("Session", "test_username", "", false).
					Return(session, tc.sessionError)
			}

			// process the request.
			uriPath := fmt.Sprintf("/storage/%s", tc.storageID)
			recorder := processStorageRequestForDefaultUser(assert, client, "GET", uriPath, nil)

			// Verify that all expectations were met.
			client.AssertExpectations(t)
			session.AssertExpectations(t)

			// Verify that the expected status code was returned.
			assert.Equal(tc.expectedStatusCode, recorder.Code)

			// Verify that the response body is correct if the request should succeed.
			if tc.ExpectSuccess() {
				expectedResponse, err := json.Marshal(tc.storageDeployment)
				assert.NoError(err)
				assert.Equal(expectedResponse, recorder.Body.Bytes())
			}
		})
	}
}

// storageUpdateTestCase represents a test case for the PATCH /storage/{storage_id} endpoint.
type storageUpdateTestCase struct {
	name               string
	storageID          string
	invalidStorageID   bool
	requestBody        []byte
	invalidRequestBody bool
	sessionError       error
	validationError    error
	transactionID      cacaocommon.TransactionID
	submissionError    error
	expectedStatusCode int
}

// ExpectSessionCall returns true if there should be an attempt to create a session for the test case.
func (tc storageUpdateTestCase) ExpectSessionCall() bool {
	return !tc.invalidStorageID && !tc.invalidRequestBody
}

// ExpectValidationCall returns true if there should be an attempt to validate the update request for the
// test case.
func (tc storageUpdateTestCase) ExpectValidationCall() bool {
	return tc.ExpectSessionCall() && tc.sessionError == nil
}

// ExpectSubmissionCall returns true if the update request should be submitted for the test case.
func (tc storageUpdateTestCase) ExpectSubmissionCall() bool {
	return tc.ExpectValidationCall() && tc.validationError == nil
}

// ExpectSuccess returns true if the request should succeed for the test case.
func (tc storageUpdateTestCase) ExpectSuccess() bool {
	return tc.ExpectSubmissionCall() && tc.submissionError == nil
}

// TestUpdateStorageDeployment runs the test cases for the PATCH /storage/{storage_id} endpoint.
func TestUpdateStorageDeployment(t *testing.T) {
	testCases := make([]storageUpdateTestCase, 7)

	// A deployment for testing.
	deployment := newDeployment(cacaocommon.NewID("template").String())

	// A deployment update for testing.
	update := hm.DeploymentUpdate{}

	// Marshal the deployment update.
	updateJSON, _ := json.Marshal(update)

	// The case where the storage ID is invalid.
	testCases[0] = storageUpdateTestCase{
		name:               "invalid storage ID",
		storageID:          "quux",
		invalidStorageID:   true,
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where the request body is invalid.
	testCases[1] = storageUpdateTestCase{
		name:               "invalid request body",
		storageID:          deployment.ID,
		requestBody:        []byte("{"),
		invalidRequestBody: true,
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where a session creation error occurs.
	testCases[2] = storageUpdateTestCase{
		name:               "session creation error",
		storageID:          deployment.ID,
		requestBody:        updateJSON,
		sessionError:       fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The case where a validation error occurs.
	testCases[3] = storageUpdateTestCase{
		name:               "request validation error",
		storageID:          deployment.ID,
		requestBody:        updateJSON,
		validationError:    fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The case where the request validation fails.
	testCases[4] = storageUpdateTestCase{
		name:               "request validation failure",
		storageID:          deployment.ID,
		requestBody:        updateJSON,
		validationError:    service.NewCacaoInvalidParameterError("something bad happened"),
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where the request submission fails.
	testCases[5] = storageUpdateTestCase{
		name:               "request submission error",
		storageID:          deployment.ID,
		requestBody:        updateJSON,
		submissionError:    fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The case where the request submission succeeds.
	testCases[6] = storageUpdateTestCase{
		name:               "request submission success",
		storageID:          deployment.ID,
		requestBody:        updateJSON,
		expectedStatusCode: http.StatusAccepted,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)
			storageID := cacaocommon.ID(tc.storageID)

			// Unmarshal the request body if applicable.
			var request hm.DeploymentUpdate
			if tc.requestBody != nil && !tc.invalidRequestBody {
				assert.NoError(json.Unmarshal(tc.requestBody, &request))
			}

			// Create the session.
			session := new(mocks.Session)
			if tc.ExpectValidationCall() {
				session.On("ValidateStorageDeploymentUpdateRequest", storageID, &request).
					Return(tc.validationError)
			}
			if tc.ExpectSubmissionCall() {
				session.On("UpdateStorageDeployment", storageID, &request).
					Return(storageID, tc.submissionError)
			}

			// Create the client.
			client := new(mocks.Client)
			if tc.ExpectSessionCall() {
				client.On("Session", "test_username", "", false).
					Return(session, tc.sessionError)
			}

			// Process the request.
			uriPath := fmt.Sprintf("/storage/%s", tc.storageID)
			recorder := processStorageRequestForDefaultUser(assert, client, "PATCH", uriPath, tc.requestBody)

			// Verify that all expectations were met.
			client.AssertExpectations(t)
			session.AssertExpectations(t)

			// Verify that the expected status code was returned.
			assert.Equal(tc.expectedStatusCode, recorder.Code)

			// Check the response body if we expect the request to succeed.
			if tc.ExpectSuccess() {
				var response utils.AcceptedResponse
				assert.NoError(json.Unmarshal(recorder.Body.Bytes(), &response))
				if tc.expectedStatusCode == http.StatusAccepted {
					assert.Equal(tc.storageID, response.ID)
				} else {
					assert.Empty(response.ID)
				}
				assert.Equal(tc.transactionID, response.TransactionID)
			}
		})
	}
}

// storageDeletionTestCase represents a single test case for the DELETE /storage/{storage_id} endpoint.
type storageDeletionTestCase struct {
	name               string
	storageID          string
	invalidStorageID   bool
	sessionError       error
	validationError    error
	transactionID      cacaocommon.TransactionID
	deleted            bool
	submissionError    error
	expectedStatusCode int
}

// ExpectSessionCall returns true if there should be an attempt to create a session for the test case.
func (tc storageDeletionTestCase) ExpectSessionCall() bool {
	return !tc.invalidStorageID
}

// ExpectValidationCall returns true if the request validation function should be called for the test case.
func (tc storageDeletionTestCase) ExpectValidationCall() bool {
	return tc.ExpectSessionCall() && tc.sessionError == nil
}

// ExpectSubmissionCall returns true if the deletion request should be submitted for the test case.
func (tc storageDeletionTestCase) ExpectSubmissionCall() bool {
	return tc.ExpectValidationCall() && tc.validationError == nil
}

// ExpectSuccess returns true if the request should succeed for the test case.
func (tc storageDeletionTestCase) ExpectSuccess() bool {
	return tc.ExpectSubmissionCall() && tc.submissionError == nil
}

// TestDeleteStorageDeployment runs the test caes for the DELETE /storage/{storage_id} endpoint.
func TestDeleteStorageDeployment(t *testing.T) {
	testCases := make([]storageDeletionTestCase, 7)

	// A storage ID for testing.
	storageID := cacaocommon.NewID("deployment").String()

	// The storage ID is invalid.
	testCases[0] = storageDeletionTestCase{
		name:               "invalid storage ID",
		storageID:          "quux",
		invalidStorageID:   true,
		expectedStatusCode: http.StatusBadRequest,
	}

	// An error occurs while creating the session.
	testCases[1] = storageDeletionTestCase{
		name:               "session error",
		storageID:          storageID,
		sessionError:       fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// An error occurs during validation.
	testCases[2] = storageDeletionTestCase{
		name:               "validation error",
		storageID:          storageID,
		validationError:    fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// Validation fails.
	testCases[3] = storageDeletionTestCase{
		name:               "validation failure",
		storageID:          storageID,
		validationError:    service.NewCacaoInvalidParameterError("something bad happened"),
		expectedStatusCode: http.StatusBadRequest,
	}

	// Submission error.
	testCases[4] = storageDeletionTestCase{
		name:               "submission error",
		storageID:          storageID,
		submissionError:    fmt.Errorf("something bad happened"),
		expectedStatusCode: http.StatusInternalServerError,
	}

	// Storage deployment deleted.
	testCases[5] = storageDeletionTestCase{
		name:               "storage deployment deleted",
		storageID:          storageID,
		deleted:            true,
		expectedStatusCode: http.StatusOK,
	}

	// Storage deployment deletion request accepted.
	testCases[6] = storageDeletionTestCase{
		name:               "storage deletion request accepted",
		storageID:          storageID,
		deleted:            false,
		expectedStatusCode: http.StatusAccepted,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Convert the storage ID for convenience.
			var storageID cacaocommon.ID
			if !tc.invalidStorageID {
				storageID = cacaocommon.ID(tc.storageID)
			}

			// Create the session.
			session := new(mocks.Session)
			if tc.ExpectValidationCall() {
				session.On("ValidateStorageDeploymentDeletionRequest", storageID).
					Return(tc.validationError)
			}
			if tc.ExpectSubmissionCall() {
				session.On("DeleteStorageDeployment", storageID).
					Return(storageID, tc.deleted, tc.submissionError)
			}

			// Create the client.
			client := new(mocks.Client)
			if tc.ExpectSessionCall() {
				client.On("Session", "test_username", "", false).
					Return(session, tc.sessionError)
			}

			// Process the request.
			uriPath := fmt.Sprintf("/storage/%s", tc.storageID)
			recorder := processStorageRequestForDefaultUser(assert, client, "DELETE", uriPath, nil)

			// Verify that all expectations were met.
			client.AssertExpectations(t)
			session.AssertExpectations(t)

			// Verify that the expected status code was returned.
			assert.Equal(tc.expectedStatusCode, recorder.Code)

			// Check the response body if we expect the request to succeed.
			if tc.ExpectSuccess() {
				var response utils.AcceptedResponse
				assert.NoError(json.Unmarshal(recorder.Body.Bytes(), &response))
				assert.Equal(tc.storageID, response.ID)
				assert.Equal(tc.transactionID, response.TransactionID)
			}
		})
	}
}

// storageActionTestCase represents a single test case for the POST /storage/{storage_id}/action endpoint.
type storageActionTestCase struct {
	name               string
	storageID          string
	invalidStorageID   bool
	requestBody        []byte
	invalidRequestBody bool
	sessionError       error
	runID              cacaocommon.ID
	storageActionError error
	expectedStatusCode int
}

// expectSessionCall returns true if a new session should be created for the test case.
func (tc storageActionTestCase) expectSessionCall() bool {
	return !tc.invalidStorageID && !tc.invalidRequestBody
}

// expectPerformActionCall returns true if PerformStorageDeploymentAction should be called for the test case.
func (tc storageActionTestCase) expectPerformActionCall() bool {
	return tc.expectSessionCall() && tc.sessionError == nil
}

// expectSuccess returns true if the request should succeed for the test case.
func (tc storageActionTestCase) expectSuccess() bool {
	return tc.expectPerformActionCall() && tc.storageActionError == nil
}

// TestPerformStorageDeploymentAction runs the test cases for the POST /storage/{storage_id}/action endpoint.
func TestPerformStorageDeploymentAction(t *testing.T) {
	testCases := make([]storageActionTestCase, 6)

	// A storage ID for testing.
	storageID := cacaocommon.NewID("deployment").String()

	// Format a request body for testing.
	storageActionRequest := hm.StorageActionRequest{
		Action:     hm.StorageActionAttach,
		InstanceID: "some-instance",
	}
	requestBody, _ := json.Marshal(&storageActionRequest)

	// Some errors for testing.
	genericError := fmt.Errorf("something bad happened")
	badParameterError := service.NewCacaoInvalidParameterError("something bad happened")

	// The case where the storage ID is invalid.
	testCases[0] = storageActionTestCase{
		name:               "invalid storage ID",
		storageID:          "quux",
		invalidStorageID:   true,
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where the request body is invalid.
	testCases[1] = storageActionTestCase{
		name:               "invalid request body",
		storageID:          storageID,
		requestBody:        []byte("{"),
		invalidRequestBody: true,
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where an error occurs during session creation.
	testCases[2] = storageActionTestCase{
		name:               "session error",
		storageID:          storageID,
		requestBody:        requestBody,
		sessionError:       genericError,
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The case where a bad parameter error is returned by PerformStorageDeploymentAction.
	testCases[3] = storageActionTestCase{
		name:               "storage action invalid parameter error",
		storageID:          storageID,
		requestBody:        requestBody,
		storageActionError: badParameterError,
		expectedStatusCode: http.StatusBadRequest,
	}

	// The case where a more general error is returned by PerformStorageDeploymentAction.
	testCases[4] = storageActionTestCase{
		name:               "storage action general error",
		storageID:          storageID,
		requestBody:        requestBody,
		storageActionError: genericError,
		expectedStatusCode: http.StatusInternalServerError,
	}

	// The case where the request succeeds.
	testCases[5] = storageActionTestCase{
		name:               "storage action success",
		storageID:          storageID,
		requestBody:        requestBody,
		runID:              cacaocommon.NewID("run"),
		expectedStatusCode: http.StatusAccepted,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Convert the storage ID for convenience.
			var storageID cacaocommon.ID
			if !tc.invalidStorageID {
				storageID = cacaocommon.ID(tc.storageID)
			}

			// Create the session.
			session := new(mocks.Session)
			if tc.expectPerformActionCall() {
				var actionRequest hm.StorageActionRequest
				assert.NoError(json.Unmarshal(tc.requestBody, &actionRequest))
				session.On("PerformStorageDeploymentAction", storageID, &actionRequest).
					Return(tc.runID, tc.storageActionError)
			}

			// Create the client.
			client := new(mocks.Client)
			if tc.expectSessionCall() {
				client.On("Session", "test_username", "", false).
					Return(session, tc.sessionError)
			}

			// Process the request.
			urlPath := fmt.Sprintf("/storage/%s/action", tc.storageID)
			recorder := processStorageRequestForDefaultUser(assert, client, "POST", urlPath, tc.requestBody)

			// Verify that all expectations were met.
			client.AssertExpectations(t)
			session.AssertExpectations(t)

			// Verify that the expected status code was returned.
			assert.Equal(tc.expectedStatusCode, recorder.Code)

			// Check the response body if the request should succeed.
			if tc.expectSuccess() {
				var actualResponse utils.AcceptedResponse
				assert.NoError(json.Unmarshal(recorder.Body.Bytes(), &actualResponse))
				if tc.expectedStatusCode == http.StatusAccepted {
					assert.Equal(tc.runID.String(), actualResponse.ID)
				} else {
					assert.Empty(actualResponse.ID)
				}
				assert.Equal(cacaocommon.TransactionID(""), actualResponse.TransactionID)
			}
		})
	}
}
