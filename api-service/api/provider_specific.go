package api

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/api-service/clients/providermetadata"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"net/http"
)

// ProviderSpecificAPIRouter ...
func ProviderSpecificAPIRouter(client providermetadata.Client, openstackAPI OpenStackProviderAPI, awsAPI AWSProviderAPI, router *mux.Router) {

	router.HandleFunc("/providers/{providerid}/authenticationTest",
		multiProvider(client).OpenStack(openstackAPI.AuthenticationTest).AWS(awsAPI.AuthenticationTest).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/regions",
		multiProvider(client).OpenStack(openstackAPI.RegionList).AWS(awsAPI.RegionList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/credentials",
		multiProvider(client).OpenStack(openstackAPI.CredentialList).AWS(awsAPI.CredentialList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/images",
		multiProvider(client).OpenStack(openstackAPI.ImageList).AWS(awsAPI.ImageList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/images/{imageid}",
		multiProvider(client).OpenStack(openstackAPI.GetImage).AWS(awsAPI.GetImage).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/flavors",
		multiProvider(client).OpenStack(openstackAPI.FlavorList).AWS(awsAPI.FlavorList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/flavors/{flavorid}",
		multiProvider(client).OpenStack(openstackAPI.GetFlavor).AWS(awsAPI.GetFlavor).Handler,
	).Methods(http.MethodGet)

	// OpenStack only
	router.HandleFunc("/providers/{providerid}/applicationCredentials", openstackOnly(client, openstackAPI.CreateApplicationCredential).Handler).Methods(http.MethodPost)
	router.HandleFunc("/providers/{providerid}/applicationCredentials", openstackOnly(client, openstackAPI.ApplicationCredentialList).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/applicationCredentials/{applicationcredentialid}", openstackOnly(client, openstackAPI.GetApplicationCredential).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/applicationCredentials/{credentialid}", openstackOnly(client, openstackAPI.DeleteApplicationCredential).Handler).Methods(http.MethodDelete)
	router.HandleFunc("/providers/{providerid}/authenticationTest", openstackOnly(client, openstackAPI.AuthenticationTest).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/projects", openstackOnly(client, openstackAPI.ProjectList).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/projects/{projectid}", openstackOnly(client, openstackAPI.GetProject).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/zone", openstackOnly(client, openstackAPI.ZoneList).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/recordset/{zone_id}", openstackOnly(client, openstackAPI.RecordsetList).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/recordset", openstackOnly(client, openstackAPI.RecordsetList).Handler).Methods(http.MethodGet)
}

type baseProviderSpecificHandler struct {
	providerClient   providermetadata.Client
	openstackHandler http.HandlerFunc
	awsHandler       http.HandlerFunc
}

func (mph *baseProviderSpecificHandler) OpenStack(handler http.HandlerFunc) *baseProviderSpecificHandler {
	mph.openstackHandler = handler
	return mph
}

func (mph *baseProviderSpecificHandler) AWS(handler http.HandlerFunc) *baseProviderSpecificHandler {
	mph.awsHandler = handler
	return mph
}

func (mph *baseProviderSpecificHandler) Handler(w http.ResponseWriter, r *http.Request) {

	providerID := common.ID(mux.Vars(r)["providerid"])
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "baseProviderSpecificHandler.Handler",
		"providerID": providerID,
	})

	if providerID.PrimaryType() != "provider" {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), "bad provider ID", status)
	}
	providerType := providerID.SubType()
	if providerType == "" {
		// if no subtype, query provider svc for type. This is to maintain compatibility with existing provider ID w/o subtype
		logger.Debug("query for provider type")
		var err error
		providerType, err = mph.queryForProviderType(r, providerID)
		if err != nil {
			const status = http.StatusBadRequest
			utils.JSONError(w, r, http.StatusText(status), err.Error(), http.StatusBadRequest)
			return
		}
	}

	switch providerType {
	case "openstack":
		if mph.openstackHandler == nil {
			logger.WithField("endpoint", r.URL).Info("provider does not support this endpoint")
			const status = http.StatusNotImplemented
			utils.JSONError(w, r, http.StatusText(status), "provider does not support this endpoint", status)
			return
		}
		mph.openstackHandler(w, r)
	case "aws":
		if mph.awsHandler == nil {
			logger.WithField("endpoint", r.URL).Info("provider does not support this endpoint")
			const status = http.StatusNotImplemented
			utils.JSONError(w, r, http.StatusText(status), "provider does not support this endpoint", status)
			return
		}
		mph.awsHandler(w, r)
	default:
		logger.WithField("providerType", providerType).Info("unknown provider type")
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), "unsupported provider type", status)
	}
}

// Get provider type by query for provider by its ID via provider metadata service.
func (mph *baseProviderSpecificHandler) queryForProviderType(r *http.Request, providerID common.ID) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "baseProviderSpecificHandler.queryForProviderType",
		"providerID": providerID,
	})
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	session, err := mph.providerClient.Session(actor, emulator)
	if err != nil {
		logger.WithError(err).Error("could not create session to get provider type")
		return "", err
	}
	provider, err := session.Get(r.Context(), providerID)
	if err != nil {
		logger.WithError(err).Error("error when trying to get provider type")
		return "", err
	}
	return string(provider.Type), nil
}

func multiProvider(client providermetadata.Client) *baseProviderSpecificHandler {
	return &baseProviderSpecificHandler{providerClient: client}
}

func openstackOnly(client providermetadata.Client, handler http.HandlerFunc) *baseProviderSpecificHandler {
	return &baseProviderSpecificHandler{providerClient: client, openstackHandler: handler}
}
