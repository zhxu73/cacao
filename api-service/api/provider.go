package api

import (
	"fmt"
	"gitlab.com/cyverse/cacao/api-service/clients/providermetadata"
	"io"
	"net/http"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/utils"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// ProviderAPIRouter creates routes for provider-related operations such as creating and
// updating providers and logging in
func ProviderAPIRouter(client providermetadata.Client, router *mux.Router) {
	api := providerMetadataAPI{client: client}
	router.HandleFunc("/providers", api.getProviders).Methods("GET")
	router.HandleFunc("/providers", api.createProvider).Methods("POST")
	router.HandleFunc("/providers/{providerid}", api.getProvider).Methods("GET")
	router.HandleFunc("/providers/{providerid}", api.updateProvider).Methods("PATCH")
	router.HandleFunc("/providers/{providerid}", api.deleteProvider).Methods("DELETE")
}

type providerMetadataAPI struct {
	client providermetadata.Client
}

func (api providerMetadataAPI) getSession(logger *log.Entry, actor, emulator string) (providermetadata.Session, error) {
	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithError(err).Error("unable to create the provider sessions microservice client")
		return nil, err
	}
	return session, nil
}

// createProvider creates a provider within cacao
func (api providerMetadataAPI) createProvider(w http.ResponseWriter, r *http.Request) {
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createProvider",
		"actor":    actor,
		"emulator": emulator,
	})
	logger.Info("start")

	// get the request body
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	var provider service.ProviderModel
	err = utils.StrictJSONDecode(reqBody, &provider)
	if err != nil {
		logger.WithError(err).Error("json request body is invalid")
		utils.JSONError(w, r, "json request body is invalid", err.Error(), http.StatusBadRequest)
		return
	}

	session, err := api.getSession(logger, actor, emulator)
	if err != nil {
		utils.JSONError(w, r, fmt.Sprintf("unable to create session: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	providerID, err := session.Create(r.Context(), provider)
	if err != nil {
		logger.WithError(err).Error("could not create provider")
		utils.JSONError(w, r, "provider could not be created", err.Error(), http.StatusBadRequest)
		return
	}

	utils.ReturnStatus(w, utils.NewAcceptedResponse(providerID, ""), http.StatusCreated)
}

func (api providerMetadataAPI) deleteProvider(w http.ResponseWriter, r *http.Request) {
	providerID := common.ID(mux.Vars(r)["providerid"])
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "deleteProvider",
		"providerID": providerID,
		"actor":      actor,
		"emulator":   emulator,
	})
	logger.Info("start")

	session, err := api.getSession(logger, actor, emulator)
	if err != nil {
		utils.JSONError(w, r, fmt.Sprintf("unable to create session: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	deletionResult, err := session.Delete(r.Context(), providerID)
	if err != nil {
		logger.WithError(err).Error("could not delete provider")
		utils.JSONError(w, r, fmt.Sprintf("provider %v could not be deleted", providerID), err.Error(), http.StatusBadRequest)
		return
	}
	if err = deletionResult.ToError("provider", providerID.String()); err != nil {
		logger.WithError(err).Error("error from deletion result")
		utils.JSONError(w, r, fmt.Sprintf("provider %v could not be deleted", providerID), err.Error(), http.StatusConflict)
	}
	utils.ReturnStatus(w, utils.NewAcceptedResponse(providerID, ""), http.StatusAccepted)
}

func (api providerMetadataAPI) getProvider(w http.ResponseWriter, r *http.Request) {
	providerID := common.ID(mux.Vars(r)["providerid"])
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "getProvider",
		"providerID": providerID,
		"actor":      actor,
		"emulator":   emulator,
	})

	session, err := api.getSession(logger, actor, emulator)
	if err != nil {
		utils.JSONError(w, r, fmt.Sprintf("unable to create session: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the provider object
	// TODO: provide a more informative error type/message
	logger.Trace("getting Provider")
	var provider *service.ProviderModel
	provider, err = session.Get(r.Context(), providerID)
	if err != nil {
		logger.WithError(err).Error("error when trying to get providerid")
		utils.JSONError(w, r, fmt.Sprintf("unable to get provider: %v", providerID), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(w, provider, http.StatusOK)
}

func (api providerMetadataAPI) getProviders(w http.ResponseWriter, r *http.Request) {
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getProviders",
		"actor":    actor,
		"emulator": emulator,
	})
	logger.Info("start")

	session, err := api.getSession(logger, actor, emulator)
	if err != nil {
		utils.JSONError(w, r, fmt.Sprintf("unable to create session: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the provider object
	// TODO: provide a more informative error type/message
	logger.Trace("getting Providers")
	providers, err := session.List(r.Context())
	if err != nil {
		logger.WithError(err).Error("error when trying to get providers")
		utils.JSONError(w, r, fmt.Sprintf("unable to get providers: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(w, providers, http.StatusOK)
}

// updateProvider allows someone to update a provider's information/secrets in Vault
func (api providerMetadataAPI) updateProvider(w http.ResponseWriter, r *http.Request) {
	providerID := common.ID(mux.Vars(r)["providerid"])
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "updateProvider",
		"providerID": providerID,
		"actor":      actor,
		"emulator":   emulator,
	})
	logger.Info("start")

	session, err := api.getSession(logger, actor, emulator)
	if err != nil {
		utils.JSONError(w, r, fmt.Sprintf("unable to create session: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// get the request body
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http provider
	var provider service.ProviderModel
	err = utils.StrictJSONDecode(reqBody, &provider)
	if err != nil {
		logger.WithError(err).Error("invalid request body")
		utils.JSONError(w, r, "invalid request body", err.Error(), http.StatusBadRequest)
		return
	}
	provider.ID = providerID

	// now write back the provider
	err = session.Update(r.Context(), provider)
	if err != nil {
		logger.WithError(err).Error("could not update provider")
		utils.JSONError(w, r, fmt.Sprintf("provider %v could not be updated", providerID), err.Error(), http.StatusBadRequest)
		return
	}

	utils.ReturnStatus(w, utils.NewAcceptedResponse(providerID, ""), http.StatusCreated)
}
