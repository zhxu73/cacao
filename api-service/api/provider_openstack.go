package api

import (
	"context"
	"encoding/json"
	"errors"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// OSProviderConstructor describes the signature of a function that can
// create new OpenStackProvider. Useful for unit tests.
type OSProviderConstructor func(string, *messaging2.NatsConfig, *messaging2.StanConfig) providers.OpenStackProvider

// OpenStackProviderAPI ...
type OpenStackProviderAPI interface {
	CreateApplicationCredential(w http.ResponseWriter, r *http.Request)
	ApplicationCredentialList(w http.ResponseWriter, r *http.Request)
	GetApplicationCredential(w http.ResponseWriter, r *http.Request)
	DeleteApplicationCredential(w http.ResponseWriter, r *http.Request)
	AuthenticationTest(w http.ResponseWriter, r *http.Request)
	CredentialList(w http.ResponseWriter, r *http.Request)
	RegionList(w http.ResponseWriter, r *http.Request)
	ImageList(w http.ResponseWriter, r *http.Request)
	GetImage(w http.ResponseWriter, r *http.Request)
	FlavorList(w http.ResponseWriter, r *http.Request)
	GetFlavor(w http.ResponseWriter, r *http.Request)
	ProjectList(w http.ResponseWriter, r *http.Request)
	GetProject(w http.ResponseWriter, r *http.Request)
	ZoneList(w http.ResponseWriter, r *http.Request)
	RecordsetList(w http.ResponseWriter, r *http.Request)
}

// openStackProviderAPI implements the HTTP API for the OpenStack Provider stuff.
type openStackProviderAPI struct {
	client openstackprovider.Client
}

// NewOpenStackProviderAPI ...
func NewOpenStackProviderAPI(client openstackprovider.Client) OpenStackProviderAPI {
	return &openStackProviderAPI{client}
}

// CreateApplicationCredential is an HTTP handler creating a new application credential and save it in CACAO.
func (p *openStackProviderAPI) CreateApplicationCredential(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.CreateApplicationCredential",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	var args providers.ApplicationCredentialCreationArgs
	err = json.NewDecoder(r.Body).Decode(&args)
	defer r.Body.Close()
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	credID, err := session.CreateApplicationCredential(providerID, credential, args.NamePostfix, args.Scope)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	utils.ReturnStatus(w, map[string]string{"id": credID}, http.StatusOK)
}

// ApplicationCredentialList is an HTTP handler listing all of the available application credentials available
// to the user.
func (p *openStackProviderAPI) ApplicationCredentialList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.ApplicationCredentialList",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	applicationCredentialList, err := session.ApplicationCredentialList(providerID, credential)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	if applicationCredentialList != nil {
		logger.WithField("length", len(applicationCredentialList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	utils.ReturnStatus(w, applicationCredentialList, http.StatusOK)
}

// GetApplicationCredential is an HTTP handler for getting a single application credential description.
func (p *openStackProviderAPI) GetApplicationCredential(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.GetApplicationCredential",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	applicationCredentialID, err := p.extractApplicationCredentialIDFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":              providerID,
		"credential":            credential(),
		"applicationCredential": applicationCredentialID,
	})
	session1, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	applicationCredential, err := session1.GetApplicationCredential(providerID, credential, applicationCredentialID)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger.Info("responded")
	utils.ReturnStatus(w, applicationCredential, http.StatusOK)
}

// DeleteApplicationCredential is an HTTP handler for deleting an application credential.
func (p *openStackProviderAPI) DeleteApplicationCredential(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.DeleteApplicationCredential",
	})
	logger.Info("request received")

	providerID, _, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	credentialID, err := p.extractCredentialIDFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	err = session.DeleteApplicationCredential(providerID, credentialID)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger.Info("responded")
	utils.ReturnStatus(w, map[string]string{"id": credentialID}, http.StatusOK)
}

// AuthenticationTest is an HTTP handler for testing authentication.
func (p *openStackProviderAPI) AuthenticationTest(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.AuthenticationTest",
	})
	logger.Info("request received")

	providerID, ok := mux.Vars(r)["providerid"]
	if !ok {
		err := errors.New("missing providerid variable")
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	variables := make(map[string]string)
	for n, v := range r.URL.Query() {
		if len(v) > 0 {
			variables[n] = v[0]
		}
	}
	logger = logger.WithFields(log.Fields{
		"provider":  providerID,
		"variables": variables,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	err = session.AuthenticationTest(providerID, variables)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger.Info("responded")
	utils.ReturnStatus(w, "success", http.StatusOK)
}

// CredentialList ...
func (p *openStackProviderAPI) CredentialList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.CredentialList",
	})
	logger.Info("request received")

	providerID, _, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	projectID := r.URL.Query().Get("project-id")
	projectName := r.URL.Query().Get("project-name")
	logger = logger.WithFields(log.Fields{
		"provider":     providerID,
		"project-id":   projectID,
		"project-name": projectName,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	credList, err := session.CredentialList(providerID, openstackprovider.CredentialListFilter{
		ProjectID:   projectID,
		ProjectName: projectName,
	})
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	if credList != nil {
		logger.WithField("length", len(credList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	utils.ReturnStatus(w, credList, http.StatusOK)
}

// RegionList is an HTTP handler listing all of the available regions available
// to the user.
func (p *openStackProviderAPI) RegionList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.RegionList",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	regionList, err := session.RegionList(providerID, credential)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	if regionList != nil {
		logger.WithField("length", len(regionList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	utils.ReturnStatus(w, regionList, http.StatusOK)
}

// ImageList is an HTTP handler listing all of the available images available
// to the user.
func (p *openStackProviderAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.ImageList",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	region, err := p.extractRegionFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	imageList, err := session.ImageList(providerID, credential, region)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	if imageList != nil {
		logger.WithField("length", len(imageList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	utils.ReturnStatus(w, imageList, http.StatusOK)
}

// GetImage is an HTTP handler for getting a single image description.
func (p *openStackProviderAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.GetImage",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	region, err := p.extractRegionFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	imageID, err := p.extractImageIDFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
		"image":      imageID,
	})
	session1, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	image, err := session1.GetImage(providerID, credential, region, imageID)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger.Info("responded")
	utils.ReturnStatus(w, image, http.StatusOK)
}

// FlavorList is an HTTP handler for listing all of the available flavors.
func (p *openStackProviderAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.FlavorList",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	region, err := p.extractRegionFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	flavorList, err := session.FlavorList(providerID, credential, region)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	if flavorList != nil {
		logger.WithField("length", len(flavorList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	utils.ReturnStatus(w, flavorList, http.StatusOK)
}

// GetFlavor is an HTTP handler for getting a single flavor.
func (p *openStackProviderAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.GetFlavor",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	region, err := p.extractRegionFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	flavorID, err := p.extractFlavorIDFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
		"flavor":     flavorID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	flavor, err := session.GetFlavor(providerID, credential, region, flavorID)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger.Info("responded")
	utils.ReturnStatus(w, flavor, http.StatusOK)
}

// ProjectList is an HTTP handler for listing all of the available projects.
func (p *openStackProviderAPI) ProjectList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.ProjectList",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	projectList, err := session.ProjectList(providerID, credential)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	if projectList != nil {
		logger.WithField("length", len(projectList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	utils.ReturnStatus(w, projectList, http.StatusOK)
}

// GetProject is an HTTP handler for getting a single project.
func (p *openStackProviderAPI) GetProject(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.GetProject",
	})
	logger.Info("request received")

	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	projectID, err := p.extractProjectIDFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
		"project":    projectID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	project, err := session.GetProject(providerID, credential, projectID)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	logger.Info("responded")
	utils.ReturnStatus(w, project, http.StatusOK)
}

// ZoneList ...
func (p *openStackProviderAPI) ZoneList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.ZoneList",
	})
	logger.Info("request received")
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	list, err := session.ZoneList(providerID, credential)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	utils.ReturnStatus(w, list, http.StatusOK)
}

// RecordsetList ...
func (p *openStackProviderAPI) RecordsetList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "openStackProviderAPI.RecordsetList",
	})
	logger.Info("request received")
	session, cancel, err := p.newSession(r)
	if err != nil {
		const status = http.StatusInternalServerError
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	defer cancel()
	providerID, credential, err := p.extractSharedFromRequest(r)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	zoneID, _ := mux.Vars(r)["zone_id"]
	list, err := session.RecordsetList(providerID, credential, zoneID)
	if err != nil {
		const status = http.StatusBadRequest
		utils.JSONError(w, r, http.StatusText(status), err.Error(), status)
		return
	}
	utils.ReturnStatus(w, list, http.StatusOK)
}

// create a new session from request for user
func (p *openStackProviderAPI) newSession(r *http.Request) (openstackprovider.Session, context.CancelFunc, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*8)

	actor, emulator, _ := utils.GetCacaoHeaders(r)
	session, err := p.client.Session(ctx, actor, emulator)
	return session, cancel, err
}

// extract values from an HTTP request that is shared by all requests to provider openstack service
func (p *openStackProviderAPI) extractSharedFromRequest(r *http.Request) (providerID string, credential providers.CredentialOption, err error) {
	providerID, ok := mux.Vars(r)["providerid"]
	if !ok {
		err = errors.New("missing providerid variable")
		return "", nil, err
	}
	return providerID, p.extractCredentialOptionFromRequest(r), nil
}

func (p *openStackProviderAPI) extractApplicationCredentialIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["applicationcredentialid"]
	if !ok {
		err := errors.New("missing applicationcredentialid variable")
		return "", err
	}
	return id, nil
}

func (p *openStackProviderAPI) extractCredentialIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["credentialid"]
	if !ok {
		err := errors.New("missing credentialid variable")
		return "", err
	}
	return id, nil
}

func (p *openStackProviderAPI) extractImageIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["imageid"]
	if !ok {
		err := errors.New("missing imageid variable")
		return "", err
	}
	return id, nil
}

func (p *openStackProviderAPI) extractFlavorIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["flavorid"]
	if !ok {
		err := errors.New("missing flavorid variable")
		return "", err
	}
	return id, nil
}

func (p *openStackProviderAPI) extractProjectIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["projectid"]
	if !ok {
		err := errors.New("missing projectid variable")
		return "", err
	}
	return id, nil
}

func (p *openStackProviderAPI) extractRegionFromRequest(r *http.Request) (string, error) {
	region := r.URL.Query().Get("region")
	if region == "" {
		err := errors.New("missing region variable")
		return "", err
	}
	return region, nil
}

// there are 2 ways to specify a credential to use with the endpoints.
// 1. credential ID. This is the default, if openstack token is not specified. The credential ID can also be empty to let backend infer the credential ID to use for the provider.
// 2. openstack token. You can explicitly narrow down the scope to a project.
func (p *openStackProviderAPI) extractCredentialOptionFromRequest(r *http.Request) providers.CredentialOption {

	tokenCred := p.extractOpenStackTokenCredentialFromRequest(r)
	if tokenCred == nil {
		// credential ID is optional, so it can be absent (empty)
		return providers.WithCredentialID(r.URL.Query().Get("credential"))
	}
	if tokenCred.Scope.Scoped() {
		return providers.WithOpenStackTokenProjectScoped(tokenCred.Token, tokenCred.Scope)
	}
	return providers.WithOpenStackToken(tokenCred.Token)
}

func (p *openStackProviderAPI) extractOpenStackTokenCredentialFromRequest(r *http.Request) *providers.OpenStackTokenCredential {
	openstackToken := r.URL.Query().Get("openstacktoken")
	if openstackToken == "" {
		return nil
	}
	scope := providers.ProjectScope{
		Project: struct {
			Name string `json:"name,omitempty"`
			ID   string `json:"id,omitempty"`
		}{
			Name: r.URL.Query().Get("project-name"),
			ID:   r.URL.Query().Get("project-id"),
		},
		Domain: struct {
			Name string `json:"name,omitempty"`
			ID   string `json:"id,omitempty"`
		}{
			Name: r.URL.Query().Get("domain-name"),
			ID:   r.URL.Query().Get("domain-id"),
		},
	}
	return &providers.OpenStackTokenCredential{
		Token: openstackToken,
		Scope: scope,
	}
}
