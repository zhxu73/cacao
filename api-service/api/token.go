package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/tokens"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"io"
	"net/http"
	"strings"
)

type tokenAPI struct {
	client tokens.Client
}

// TokenAPIRouter creates routes related to managing cacao api tokens
func TokenAPIRouter(tokensClient tokens.Client, router *mux.Router) {
	tAPI := tokenAPI{client: tokensClient}
	// token types
	router.HandleFunc("/tokens/types", tAPI.listTokenTypes).Methods("GET")
	// tokens
	router.HandleFunc("/tokens", tAPI.listTokens).Methods("GET")
	router.HandleFunc("/tokens", tAPI.createToken).Methods("POST")
	router.HandleFunc("/tokens/{token_id}", tAPI.getToken).Methods("GET")
	// router.HandleFunc("/tokens/{token_id}", tAPI.updateToken).Methods("PUT")
	router.HandleFunc("/tokens/{token_id}", tAPI.updateTokenFields).Methods("PATCH")
	router.HandleFunc("/tokens/{token_id}", tAPI.deleteToken).Methods("DELETE")
	router.HandleFunc("/tokens/{token_id}/revoke", tAPI.revokeToken).Methods("POST")
	// TODO bulk token create
	// router.HandleFunc("/tokens/bulk", tAPI.bulkCreate).Methods("POST")
}

func (tapi tokenAPI) listTokenTypes(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listTokenTypes",
	})
	logger.Info("api.listTokenTypes(): start")
	var err error

	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	tokenTypes, err := session.ListTypes()
	if err != nil {
		errorMessage := "error listing token types"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Guard against listTokenTypes returning nil.
	if tokenTypes == nil {
		tokenTypes = make([]cacao_common_service.TokenType, 0)
	}
	// Format the response body.
	utils.ReturnStatus(w, tokenTypes, http.StatusOK)
}

func (tapi tokenAPI) listTokens(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listTokens",
	})
	logger.Info("api.listTokens(): start")
	var err error

	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of tokens
	tokenSlice, err := session.ListTokens()
	if err != nil {
		errorMessage := "error listing tokens"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Guard against listTokens returning nil.
	if tokenSlice == nil {
		tokenSlice = make([]cacao_common_service.TokenModel, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, tokenSlice, http.StatusOK)
}

// unmarshalRequestAndGetFieldNames reads the request body and unmarshals it. Also returns field names.
func (tapi *tokenAPI) unmarshalRequestAndGetFieldNames(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) ([]string, error) {
	// Read the request body.
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return nil, err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return nil, err
	}

	requestMap := map[string]interface{}{}
	// Unmarshal the request body to Map.
	err = json.Unmarshal(requestBody, &requestMap)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return nil, err
	}

	fields := []string{}
	for k := range requestMap {
		fields = append(fields, k)
	}

	return fields, nil
}

// getTokensClientSession obtains the tokens client session for a given request. Writes an error response and
// returns nil if the session can't be created.
func (tapi *tokenAPI) getTokensClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) tokens.Session {
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	session, err := tapi.client.Session(actor, emulator)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the tokens microservice client")
		utils.JSONCacaoError(logger, w, r, err)
		return nil
	}
	return session
}

func (tapi tokenAPI) getToken(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getToken",
	})
	logger.Info("api.getToken(): start")
	var err error

	// Extract the token ID from the request and validate it.
	tokenID, err := tapi.extractID(logger, w, r, "token_id", "token")
	if err != nil {
		return
	}

	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the token.
	token, err := session.GetToken(tokenID)
	if err != nil {
		errorMessage := "error obtaining token information"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	if !token.ID.Validate() {
		errorMessage := fmt.Sprintf("token not found: %s", tokenID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return
	}
	// Format the response body.
	utils.ReturnStatus(w, token, http.StatusOK)
}

// unndeeded
//func (tapi tokenAPI) updateToken(w http.ResponseWriter, r *http.Request) {
//	logger := log.WithFields(log.Fields{
//		"package":  "api",
//		"function": "updateToken",
//	})
//
//	logger.Info("api.updateToken(): start")
//	transactionID := messaging2.NewTransactionID()
//	// Extract the token ID from the request and validate it.
//	tokenID, err := tapi.extractID(logger, w, r, "token_id", "token")
//	if err != nil {
//		return
//	}
//
//	// Unmarshal the request body.
//	var incomingRequest cacao_common_service.TokenModel
//	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
//	if err != nil {
//		return
//	}
//
//	// Create the tokens client session.
//	session := tapi.getTokensClientSession(logger, w, r)
//	if session == nil {
//		return
//	}
//
//	// update
//	// Build and validate the token update request.
//	err = session.ValidateTokenUpdateRequest(tokenID, incomingRequest)
//	if err != nil {
//		errorMessage := "token update request validation failed"
//		logger.WithField("error", err).Error(errorMessage)
//		utils.JSONCacaoError(logger, w, r, err)
//		return
//	}
//
//	// Submit the update request.
//	_, err = session.UpdateToken(tokenID, incomingRequest)
//	if err != nil {
//		errorMessage := "token update request submission failed"
//		logger.WithField("error", err).Error(errorMessage)
//		utils.JSONCacaoError(logger, w, r, err)
//		return
//	}
//
//	// Format the response body.
//	body := utils.NewAcceptedResponse(tokenID.String(), transactionID.String())
//	utils.ReturnStatus(w, body, http.StatusAccepted)
//}

func (tapi tokenAPI) updateTokenFields(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTokenFields",
	})
	logger.Info("api.updateTokenFields(): start")
	transactionID := messaging2.NewTransactionID()
	// Extract the token ID from the request and validate it.
	tokenID, err := tapi.extractID(logger, w, r, "token_id", "token")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_service.TokenModel
	updateFields, err := tapi.unmarshalRequestAndGetFieldNames(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the token update request.
	err = session.ValidateTokenUpdateFieldsRequest(tokenID, incomingRequest, updateFields)
	logger.WithFields(log.Fields{
		"incomingRequest": incomingRequest,
		"updateFields":    updateFields,
		"tokenXID":        tokenID,
	}).Trace()
	if err != nil {
		errorMessage := "token update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}
	// Submit the update request.
	_, err = session.UpdateTokenFields(tokenID, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "token update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", transactionID)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (tapi tokenAPI) deleteToken(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteToken",
	})
	logger.Info("api.deleteToken(): start")
	transactionID := messaging2.NewTransactionID()
	// Extract the token ID from the request and validate it.
	tokenID, err := tapi.extractID(logger, w, r, "token_id", "token")
	if err != nil {
		return
	}

	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the token deletion request.
	err = session.ValidateTokenDeleteRequest(tokenID)
	if err != nil {
		errorMessage := "token deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	_, err = session.DeleteToken(tokenID)
	if err != nil {
		errorMessage := "token deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", transactionID)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (tapi *tokenAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string, idType string) (cacao_common.ID, error) {
	token := mux.Vars(r)[placeholder]
	dashIndex := strings.IndexRune(token, '-')
	var tokenID cacao_common.ID
	// allow for passing in token-xid or plain xid
	if dashIndex <= 0 {
		tokenID = cacao_common.ID(fmt.Sprintf("token-%s", token))
	} else {
		tokenID = cacao_common.ID(token)
	}
	if !tokenID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, tokenID)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return cacao_common.ID(""), cerr
	}
	return tokenID, nil
}

func (tapi *tokenAPI) createToken(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createToken",
	})
	logger.Info("api.createToken(): start")
	transactionID := messaging2.NewTransactionID()
	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	var incomingRequest cacao_common_service.TokenCreateModel
	err := tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		errorMessage := "token create request unmarshall failed"
		logger.WithField("error", err).Error(errorMessage)
	}

	actor, _ := utils.GetCacaoHeaders1(r)
	incomingRequest.Owner = actor.Actor

	err = session.ValidateCreateTokenRequest(incomingRequest)
	if err != nil {
		errorMessage := "token create request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the create request.
	tokenID, err := session.CreateToken(incomingRequest)
	if err != nil {
		errorMessage := "token create request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponseWithStrID(tokenID.String(), transactionID)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (tapi *tokenAPI) revokeToken(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "revokeToken",
	})
	logger.Info("api.revokeToken(): start")
	transactionID := messaging2.NewTransactionID()
	// Create the tokens client session.
	session := tapi.getTokensClientSession(logger, w, r)
	if session == nil {
		return
	}

	var incomingRequest cacao_common_service.TokenModel
	err := tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		errorMessage := "token revoke request unmarshall failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	_, err = session.RevokeToken(incomingRequest.ID)
	if err != nil {
		logger.WithError(err).Error("failed to revoke token")
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", transactionID)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// unmarshalRequest reads the request body and unmarshals it.
func (tapi *tokenAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return err
	}

	return nil
}
