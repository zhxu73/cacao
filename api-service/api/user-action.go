package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/useractions"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// userActionsAPI is a single instance of the user actions API implementation.
type userActionsAPI struct {
	userActionsClient useractions.Client
}

// UserActionAPIRouter creates routes for operations related to user actions.
func UserActionAPIRouter(userActionsClient useractions.Client, router *mux.Router) {
	uaapi := &userActionsAPI{
		userActionsClient: userActionsClient,
	}

	router.HandleFunc("/useractions", uaapi.listUserActions).Methods("GET")
	router.HandleFunc("/useractions", uaapi.createUserAction).Methods("POST")
	router.HandleFunc("/useractions/{user_action_id}", uaapi.getUserActionByID).Methods("GET")
	router.HandleFunc("/useractions/{user_action_id}", uaapi.updateUserAction).Methods("PUT")
	router.HandleFunc("/useractions/{user_action_id}", uaapi.updateUserActionFields).Methods("PATCH")
	router.HandleFunc("/useractions/{user_action_id}", uaapi.deleteUserAction).Methods("DELETE")
}

// unmarshalRequest reads the request body and unmarshals it.
func (uaapi *userActionsAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return err
	}

	return nil
}

// unmarshalRequestAndGetFieldNames reads the request body and unmarshals it. Also returns field names.
func (uaapi *userActionsAPI) unmarshalRequestAndGetFieldNames(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) ([]string, error) {
	// Read the request body.
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return nil, err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return nil, err
	}

	requestMap := map[string]interface{}{}
	// Unmarshal the request body to Map.
	err = json.Unmarshal(requestBody, &requestMap)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return nil, err
	}

	fields := []string{}
	for k := range requestMap {
		fields = append(fields, k)
	}

	return fields, nil
}

// getUserActionsClientSession obtains the user actions client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (uaapi *userActionsAPI) getUserActionsClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) useractions.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := uaapi.userActionsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the user actions microservice client")
		utils.JSONCacaoError(logger, w, r, err)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (uaapi *userActionsAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string, idType string) (cacao_common.ID, error) {
	userActionID := cacao_common.ID(mux.Vars(r)[placeholder])
	if !userActionID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, userActionID)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return cacao_common.ID(""), cerr
	}
	return userActionID, nil
}

// listUserActions implements the GET /useractions endpoint.
func (uaapi *userActionsAPI) listUserActions(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listUserActions",
	})
	logger.Info("api.listUserActions(): start")
	var err error

	// Create the user actions client session.
	session := uaapi.getUserActionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of user actions
	userActions, err := session.ListUserActions()
	if err != nil {
		errorMessage := "error listing user actions"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Guard against ListUserActions returning nil.
	if userActions == nil {
		userActions = make([]cacao_common_http.UserAction, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, userActions, http.StatusOK)
}

// getUserActionByID implements the GET /useractions/{user_action_id} endpoint.
// getUserActionByID returns the user action with the specified ID if it exists and the authenticated user may view it.
func (uaapi *userActionsAPI) getUserActionByID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUserActionByID",
	})
	logger.Info("api.getUserActionByID(): start")
	var err error

	// Extract the user action ID from the request and validate it.
	userActionID, err := uaapi.extractID(logger, w, r, "user_action_id", "useraction")
	if err != nil {
		return
	}

	// Create the user actions client session.
	session := uaapi.getUserActionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the user action.
	userAction, err := session.GetUserAction(userActionID)
	if err != nil {
		errorMessage := "error obtaining user action information"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	if !userAction.ID.Validate() {
		errorMessage := fmt.Sprintf("user action not found: %s", userActionID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, userAction, http.StatusOK)
}

// createUserAction implements the POST /useractions endpoint.
func (uaapi *userActionsAPI) createUserAction(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createUserAction",
	})
	logger.Info("api.createUserAction(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.UserAction
	err = uaapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the user actions session.
	session := uaapi.getUserActionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the user action creation request.
	err = session.ValidateUserActionCreationRequest(incomingRequest)
	if err != nil {
		errorMessage := "user action creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the creation request.
	id, err := session.CreateUserAction(incomingRequest)
	if err != nil {
		errorMessage := "user action creation request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(id, "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateUserAction implements the PUT /useractions/{user_action_id} endpoint.
// updateUserAction updates (replaces) a user action information.
func (uaapi *userActionsAPI) updateUserAction(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateUserAction",
	})
	logger.Info("api.updateUserAction(): start")

	// Extract the user action ID from the request and validate it.
	userActionID, err := uaapi.extractID(logger, w, r, "user_action_id", "useraction")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.UserAction
	err = uaapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the user actions client session.
	session := uaapi.getUserActionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the user action update request.
	err = session.ValidateUserActionUpdateRequest(userActionID, incomingRequest)
	if err != nil {
		errorMessage := "user action update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the update request.
	id, err := session.UpdateUserAction(userActionID, incomingRequest)
	if err != nil {
		errorMessage := "user action update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(id, "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateUserActionFields implements the PATCH /useractions/{user_action_id} endpoint.
// updateUserActionFields updates fields of a user action information.
func (uaapi *userActionsAPI) updateUserActionFields(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateUserActionFields",
	})
	logger.Info("api.updateUserActionFields(): start")

	// Extract the user action ID from the request and validate it.
	userActionID, err := uaapi.extractID(logger, w, r, "user_action_id", "useraction")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.UserAction
	updateFields, err := uaapi.unmarshalRequestAndGetFieldNames(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the user actions client session.
	session := uaapi.getUserActionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the user action update request.
	err = session.ValidateUserActionUpdateFieldsRequest(userActionID, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "user action update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the update request.
	id, err := session.UpdateUserActionFields(userActionID, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "user action update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(id, "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// deleteUserAction implements the DELETE /useractions/{user_action_id} endpoint.
func (uaapi *userActionsAPI) deleteUserAction(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteUserAction",
	})
	logger.Info("api.deleteUserAction(): start")

	// Extract the user action ID from the request and validate it.
	userActionID, err := uaapi.extractID(logger, w, r, "user_action_id", "useraction")
	if err != nil {
		return
	}

	// Create the user actions client session.
	session := uaapi.getUserActionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the user action deletion request.
	err = session.ValidateUserActionDeletionRequest(userActionID)
	if err != nil {
		errorMessage := "user action deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	id, err := session.DeleteUserAction(userActionID)
	if err != nil {
		errorMessage := "user action deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(id, "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}
