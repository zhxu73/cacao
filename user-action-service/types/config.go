package types

import (
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	// NATS
	NatsStanConfig messaging2.NatsStanMsgConfig

	// MongoDB
	MongoDBConfig                   cacao_common_db.MongoDBConfig
	UserActionMongoDBCollectionName string
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	// NATS

	if c.NatsStanConfig.ClientID == "" {
		c.NatsStanConfig.ClientID = DefaultNatsClientID
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}
}

// Override overrides certain config
func (c *Config) Override() {
	c.NatsStanConfig.QueueGroup = natsQueueGroup
	c.NatsStanConfig.DurableName = natsDurableName
	c.NatsStanConfig.WildcardSubject = natsWildcardSubject
	c.UserActionMongoDBCollectionName = DefaultUserActionMongoDBCollectionName
}
