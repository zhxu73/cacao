package types

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
)

// This is to ensure that UserAction contains the same data field as
// service.UserActionModel (data field meaning not service.Session). This makes
// sure that the JSON wire format does not change.
func TestUserActionConvert(t *testing.T) {
	// UserAction => service.UserActionModel => UserAction
	var ua1 = UserAction{
		ID:          "useraction-aaaaaaaaaaaaaaaaaaaa",
		Owner:       "testuser123",
		Name:        "testuser456",
		Description: "description",
		Public:      true,
		Type:        service.UserActionTypeInstanceExecution,
		Action:      service.NewUserActionItemForScriptAction(service.UserActionScriptSourceTypeURL, service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
		CreatedAt:   time.Now().Add(-time.Hour).UTC(),
		UpdatedAt:   time.Now().UTC(),
	}

	var ua2 service.UserActionModel
	var ua3 UserAction

	marshal, err := json.Marshal(ua1)
	if !assert.NoError(t, err) {
		return
	}
	err = json.Unmarshal(marshal, &ua2)
	if !assert.NoError(t, err) {
		return
	}
	marshal, err = json.Marshal(ua2)
	if !assert.NoError(t, err) {
		return
	}
	err = json.Unmarshal(marshal, &ua3)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, ua1, ua3)
}
