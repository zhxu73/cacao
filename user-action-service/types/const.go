package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

const (
	// natsQueueGroup is the NATS Queue Group for this service
	natsQueueGroup = "user_action_queue_group"
	// natsWildcardSubject is the NATS subject to subscribe
	natsWildcardSubject = string(cacao_common_service.NatsSubjectUserAction) + ".>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "user-action-service"

	// natsDurableName is the NATS Durable Name for this service
	natsDurableName = "user_action_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao-user-action"
	// DefaultUserActionMongoDBCollectionName is a default MongoDB Collection Name
	DefaultUserActionMongoDBCollectionName = "user_action"
)
