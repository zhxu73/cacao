package main

import (
	"context"
	"io"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"

	"gitlab.com/cyverse/cacao/user-action-service/adapters"
	"gitlab.com/cyverse/cacao/user-action-service/domain"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()
	config.Override()

	natsConn, err := config.NatsStanConfig.ConnectNats()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logError(&natsConn)
	stanConn, err := config.NatsStanConfig.ConnectStan()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logError(&stanConn)

	// add and initialize the storage adapter
	mongoAdapter := &adapters.MongoAdapter{}
	defer logError(mongoAdapter)

	eventAdapter := adapters.NewEventAdapter(&stanConn)

	var svc = domain.Domain{
		Storage: mongoAdapter,
		QueryIn: adapters.NewQueryAdapter(&natsConn),
		EventIn: eventAdapter,
	}
	err = svc.Init(&config)
	if err != nil {
		log.WithError(err).Error("fail to init service")
		return
	}

	serviceContext, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	svc.Start(serviceContext)
}

func logError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
	}
}
