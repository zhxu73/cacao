package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/ports"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	handlers ports.IncomingEventHandlers
	// internal
	conn messaging2.EventConnection
}

var _ ports.IncomingEventPort = &EventAdapter{}

// NewEventAdapter creates a new EventAdapter from EventConnection
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{conn: conn}
}

// SetHandlers ...
func (adapter *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")
	return adapter.conn.Listen(ctx, map[cacao_common.EventType]messaging2.EventHandlerFunc{
		cacao_common_service.UserActionCreateRequestedEvent: handlerWrapper(adapter.handlers.Create),
		cacao_common_service.UserActionDeleteRequestedEvent: handlerWrapper(adapter.handlers.Delete),
		cacao_common_service.UserActionUpdateRequestedEvent: handlerWrapper(adapter.handlers.Update),
	}, wg)
}

func handlerWrapper[T any](handler func(T, ports.OutgoingEventPort)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			errorMessage := "unable to unmarshal JSON bytes into UserActionModel"
			log.WithField("ceType", event.Type()).WithError(err).Error(errorMessage)
			return err
		}
		handler(request, eventOut{writer: writer})
		return nil
	}
}

/*
func (adapter *EventAdapter) handleUserActionCreateRequest(transactionID cacao_common.TransactionID, requestCe *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.handleUserActionCreateRequest",
	})

	var createRequest cacao_common_service.UserActionModel
	err := json.Unmarshal(requestCe.Data(), &createRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into UserActionModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	userAction := types.ConvertFromModel(createRequest)

	err = adapter.handlers.Create(createRequest.GetSessionActor(), createRequest.GetSessionEmulator(), userAction, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleUserActionDeleteRequest(transactionID cacao_common.TransactionID, requestCe *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.handleUserActionDeleteRequest",
	})

	var deleteRequest cacao_common_service.UserActionModel
	err := json.Unmarshal(requestCe.Data(), &deleteRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into UserActionModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	userAction := types.ConvertFromModel(deleteRequest)

	err = adapter.handlers.Delete(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), userAction, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleUserActionUpdateRequest(transactionID cacao_common.TransactionID, requestCe *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.handleUserActionUpdateRequest",
	})

	var updateRequest cacao_common_service.UserActionModel
	err := json.Unmarshal(requestCe.Data(), &updateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into UserActionModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	userAction := types.ConvertFromModel(updateRequest)

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "description", "default_provider_id")
	}

	err = adapter.handlers.Update(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), userAction, updateRequest.UpdateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}
*/

type eventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventPort = &eventOut{}

// UserActionCreatedEvent ...
func (e eventOut) UserActionCreatedEvent(model cacao_common_service.UserActionModel) {
	e.publish(model, cacao_common_service.UserActionCreatedEvent)
}

// UserActionCreateFailedEvent ...
func (e eventOut) UserActionCreateFailedEvent(model cacao_common_service.UserActionModel) {
	e.publish(model, cacao_common_service.UserActionCreateFailedEvent)
}

// UserActionUpdatedEvent ...
func (e eventOut) UserActionUpdatedEvent(model cacao_common_service.UserActionModel) {
	e.publish(model, cacao_common_service.UserActionUpdatedEvent)
}

// UserActionUpdateFailedEvent ...
func (e eventOut) UserActionUpdateFailedEvent(model cacao_common_service.UserActionModel) {
	e.publish(model, cacao_common_service.UserActionUpdateFailedEvent)
}

// UserActionDeletedEvent ...
func (e eventOut) UserActionDeletedEvent(model cacao_common_service.UserActionModel) {
	e.publish(model, cacao_common_service.UserActionDeletedEvent)
}

// UserActionDeleteFailedEvent ...
func (e eventOut) UserActionDeleteFailedEvent(model cacao_common_service.UserActionModel) {
	e.publish(model, cacao_common_service.UserActionDeleteFailedEvent)
}

func (e eventOut) publish(model interface{}, eventType cacao_common.EventType) {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "eventOut.publish",
	})

	ce, err := messaging2.CreateCloudEventWithAutoSource(model, eventType)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", eventType)
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", eventType)
	}
}

// Created publishes cacao_common_service.UserActionCreatedEvent
func (adapter *EventAdapter) Created(actor string, emulator string, userAction types.UserAction, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.Created",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, userAction)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.UserActionCreatedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.UserActionCreatedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.UserActionCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.UserActionCreatedEvent)
	}

	return nil
}

// CreateFailed publishes cacao_common_service.UserActionCreateFailedEvent
func (adapter *EventAdapter) CreateFailed(actor string, emulator string, userAction types.UserAction, creationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.CreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
	}

	if cerr, ok := creationError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(creationError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, userAction)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.UserActionCreateFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.UserActionCreateFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.UserActionCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.UserActionCreateFailedEvent)
	}

	return nil
}

// Updated publishes cacao_common_service.UserActionUpdatedEvent.
func (adapter *EventAdapter) Updated(actor string, emulator string, userAction types.UserAction, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.Updated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, userAction)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.UserActionUpdatedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.UserActionUpdatedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.UserActionUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.UserActionUpdatedEvent)
	}

	return nil
}

// UpdateFailed publishes cacao_common_service.UserActionUpdateFailedEvent
func (adapter *EventAdapter) UpdateFailed(actor string, emulator string, userAction types.UserAction, updateError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
	}

	if cerr, ok := updateError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(updateError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, userAction)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.UserActionUpdateFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.UserActionUpdateFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.UserActionUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.UserActionUpdateFailedEvent)
	}

	return nil
}

// Deleted publishes cacao_common_service.UserActionDeletedEvent
func (adapter *EventAdapter) Deleted(actor string, emulator string, userAction types.UserAction, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, userAction)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.UserActionDeletedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.UserActionDeletedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.UserActionDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.UserActionDeletedEvent)
	}

	return nil
}

// DeleteFailed publishes cacao_common_service.UserActionDeleteFailedEvent
func (adapter *EventAdapter) DeleteFailed(actor string, emulator string, userAction types.UserAction, deletionError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
	}

	if cerr, ok := deletionError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(deletionError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, userAction)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.UserActionDeleteFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.UserActionDeleteFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.UserActionDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.UserActionDeleteFailedEvent)
	}

	return nil
}
