package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	config *types.Config
	store  cacao_common_db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Error("unable to connect to MongoDB")
		return err
	}

	adapter.store = store
	return nil
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Close closes Mongo DB connection
func (adapter *MongoAdapter) Close() error {
	return adapter.store.Release()
}

// List returns user actions owned by a user
func (adapter *MongoAdapter) List(user string) ([]types.UserAction, error) {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.UserAction{}

	filter := map[string]interface{}{
		"$or": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"public": true,
			},
		},
	}

	err := adapter.store.ListConditional(adapter.config.UserActionMongoDBCollectionName, filter, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list user actions for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedUserActions []types.UserAction, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"$or": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"public": true,
			},
		},
	}

	mock.On("ListConditional", adapter.config.UserActionMongoDBCollectionName, filter).Return(expectedUserActions, expectedError)
	return nil
}

// Get returns the user action with the ID
func (adapter *MongoAdapter) Get(user string, userActionID cacao_common.ID) (types.UserAction, error) {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.UserAction{}

	err := adapter.store.Get(adapter.config.UserActionMongoDBCollectionName, userActionID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the user action for id %s", userActionID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if !result.Public && result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the user action for id %s", userActionID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, userActionID cacao_common.ID, expectedUserAction types.UserAction, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.config.UserActionMongoDBCollectionName, userActionID.String()).Return(expectedUserAction, expectedError)
	return nil
}

// Create inserts a user action
func (adapter *MongoAdapter) Create(userAction types.UserAction) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.store.Insert(adapter.config.UserActionMongoDBCollectionName, userAction)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a user action because id %s conflicts", userAction.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a user action with id %s", userAction.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(userAction types.UserAction, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.config.UserActionMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a user action
func (adapter *MongoAdapter) Update(userAction types.UserAction, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// validate the update field names, this ensures there is no extra field being passed in
	if !adapter.validateUpdateFieldNames(updateFieldNames) {
		return cacao_common_service.NewCacaoInvalidParameterError("invalid value in update_field_names")
	}

	// get and check ownership
	result := types.UserAction{}

	err := adapter.store.Get(adapter.config.UserActionMongoDBCollectionName, userAction.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the user action for id %s", userAction.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != userAction.Owner {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the user action for id %s", userAction.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.store.Update(adapter.config.UserActionMongoDBCollectionName, userAction.ID.String(), userAction, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the user action for id %s", userAction.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the user action for id %s", userAction.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

var allowedUpdateFieldNames = map[string]struct{}{
	"name":        {},
	"description": {},
	"public":      {},
	"type":        {},
	"action":      {},
}

func (adapter *MongoAdapter) validateUpdateFieldNames(updateFieldNames []string) bool {
	for _, name := range updateFieldNames {
		_, ok := allowedUpdateFieldNames[name]
		if !ok {
			return false
		}
	}
	return true
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingUserAction types.UserAction, newUserAction types.UserAction, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.config.UserActionMongoDBCollectionName, newUserAction.ID.String()).Return(existingUserAction, expectedError)
	mock.On("Update", adapter.config.UserActionMongoDBCollectionName, newUserAction.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a user action
func (adapter *MongoAdapter) Delete(user string, userActionID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.UserAction{}

	err := adapter.store.Get(adapter.config.UserActionMongoDBCollectionName, userActionID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the user action for id %s", userActionID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the user action for id %s", userActionID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// delete
	deleted, err := adapter.store.Delete(adapter.config.UserActionMongoDBCollectionName, userActionID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the user action for id %s", userActionID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the user action for id %s", userActionID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, userActionID cacao_common.ID, existingUserAction types.UserAction, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.config.UserActionMongoDBCollectionName, userActionID.String()).Return(existingUserAction, expectedError)
	mock.On("Delete", adapter.config.UserActionMongoDBCollectionName, userActionID.String()).Return(expectedResult, expectedError)
	return nil
}
