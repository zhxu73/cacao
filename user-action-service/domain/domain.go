package domain

import (
	"context"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/user-action-service/ports"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	config  *types.Config
	Storage ports.PersistentStoragePort
	QueryIn ports.IncomingQueryPort
	EventIn ports.IncomingEventPort

	queryHandlers *QueryHandlers
	eventHandlers *EventHandlers
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) error {
	d.config = config
	err := d.Storage.Init(config)
	if err != nil {
		return err
	}

	d.queryHandlers = &QueryHandlers{Storage: d.Storage}
	d.eventHandlers = &EventHandlers{
		Storage: d.Storage,
		TimeSrc: func() time.Time {
			return time.Now().UTC()
		},
	}

	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) {
	var wg sync.WaitGroup
	d.QueryIn.SetHandlers(d.queryHandlers)
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Panic()
		return
	}

	d.EventIn.SetHandlers(d.eventHandlers)
	wg.Add(1)
	err = d.EventIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Panic()
		return
	}

	wg.Wait()
}
