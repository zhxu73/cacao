package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/ports"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

// QueryHandlers implements ports.IncomingQueryHandlers
type QueryHandlers struct {
	Storage ports.PersistentStoragePort
}

var _ ports.IncomingQueryHandlers = &QueryHandlers{}

// List retrieves all user action of the user
func (h *QueryHandlers) List(request cacao_common_service.Session) cacao_common_service.UserActionListModel {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.domain",
		"function": "QueryHandlers.List",
	})
	logger.Info("List user actions")

	if len(request.SessionActor) == 0 {
		return h.listErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty"))
	}

	userActionList, err := h.Storage.List(request.SessionActor)
	if err != nil {
		logger.WithError(err).Error("fail to list user actions from storage")
		return h.listErrorReply(request, err)
	}
	logger.WithField("len", len(userActionList)).Info("Listed")

	var listReply cacao_common_service.UserActionListModel
	listReply.Session = cacao_common_service.CopySessionActors(request)
	var models = make([]cacao_common_service.UserActionModel, 0, len(userActionList))
	for _, userAction := range userActionList {
		model := types.ConvertToModel(cacao_common_service.Session{}, userAction)
		models = append(models, model)
	}
	listReply.UserActions = models
	return listReply
}

func (h *QueryHandlers) listErrorReply(request cacao_common_service.Session, err error) cacao_common_service.UserActionListModel {
	var listReply cacao_common_service.UserActionListModel
	listReply.Session = cacao_common_service.CopySessionActors(request)
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		listReply.ServiceError = cerr.GetBase()
	} else {
		listReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return listReply
}

// Get retrieves the user action
func (h *QueryHandlers) Get(request cacao_common_service.UserActionModel) cacao_common_service.UserActionModel {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.domain",
		"function": "QueryHandlers.Get",
	})
	if len(request.SessionActor) == 0 {
		return h.getErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty"))
	}

	if len(request.ID) == 0 {
		return h.getErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action ID is empty"))
	}

	logger.Infof("Get user action %s", request.ID.String())

	userAction, err := h.Storage.Get(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch user action from storage")
		return h.getErrorReply(request, err)
	}
	return types.ConvertToModel(cacao_common_service.CopySessionActors(request.Session), userAction)
}

// error reply for get request
func (h *QueryHandlers) getErrorReply(request cacao_common_service.UserActionModel, err error) cacao_common_service.UserActionModel {
	var getReply cacao_common_service.UserActionModel
	getReply.Session = cacao_common_service.CopySessionActors(request.Session)

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		getReply.ServiceError = cerr.GetBase()
	} else {
		getReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return getReply
}
