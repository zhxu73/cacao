package domain

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/user-action-service/ports"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

// EventHandlers implements IncomingEventPort
type EventHandlers struct {
	Storage  ports.PersistentStoragePort
	EventOut ports.OutgoingEventPort
	TimeSrc  func() time.Time
}

var _ ports.IncomingEventHandlers = &EventHandlers{}

// Create creates a user action
func (h *EventHandlers) Create(request cacao_common_service.UserActionModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.domain",
		"function": "EventHandlers.Create",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})
	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.UserActionCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		request.ID = cacao_common_service.NewUserActionID()
	} else if !request.ID.Validate() || request.ID.FullPrefix() != "useraction" {
		// if we are using ID supplied by client, then we need to validate it
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: ill-formed user action id")
		sink.UserActionCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if len(request.Name) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action name is empty")
		logger.WithError(err).Error("bad request")
		sink.UserActionCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	now := h.TimeSrc()
	var userAction = types.UserAction{
		ID:          request.ID,
		Owner:       request.SessionActor,
		Name:        request.Name,
		Description: request.Description,
		Public:      request.Public,
		Type:        request.Type,
		Action:      request.Action,
		CreatedAt:   now,
		UpdatedAt:   now,
	}

	logger.Debugf("Creating user action %s", userAction.ID.String())

	err := h.Storage.Create(userAction)
	if err != nil {
		logger.WithError(err).Error("fail to create user action in storage")
		sink.UserActionCreateFailedEvent(h.creationErrorResponse(request, toCacaoError(err)))
		return
	}

	logger.Info("user action created")
	sink.UserActionCreatedEvent(cacao_common_service.UserActionModel{
		Session:          cacao_common_service.CopySessionActors(request.Session),
		ID:               userAction.ID,
		Owner:            userAction.Owner,
		Name:             userAction.Name,
		CreatedAt:        userAction.CreatedAt,
		UpdatedAt:        time.Time{},
		UpdateFieldNames: nil,
	})
}

func (h *EventHandlers) creationErrorResponse(request cacao_common_service.UserActionModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.UserActionModel {
	return cacao_common_service.UserActionModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
		Name:  request.Name,
	}
}

// Update updates the user action
func (h *EventHandlers) Update(request cacao_common_service.UserActionModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":          "user-action-service.domain",
		"function":         "EventHandlers.Update",
		"actor":            request.SessionActor,
		"emulator":         request.SessionEmulator,
		"userAction":       request.ID,
		"updateFieldNames": request.UpdateFieldNames,
	})
	logger.Infof("Updating user action %s", request.ID.String())
	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.UserActionUpdateFailedEvent(h.updateErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action id is empty")
		logger.WithError(err).Error("bad request")
		sink.UserActionUpdateFailedEvent(h.updateErrorResponse(request, err))
		return
	}

	now := h.TimeSrc()

	var userAction = types.UserAction{
		ID:          request.ID,
		Owner:       request.SessionActor, // user can only them their own user action
		Name:        request.Name,
		Description: request.Description,
		Public:      request.Public,
		Type:        request.Type,
		Action:      request.Action,
		UpdatedAt:   now,
	}

	if len(request.UpdateFieldNames) == 0 {
		// update all fields
		request.UpdateFieldNames = append(request.UpdateFieldNames, "name", "description", "public", "type", "action")
	}

	err := h.Storage.Update(userAction, request.UpdateFieldNames)
	if err != nil {
		logger.WithError(err).Error("fail to update user action in storage")
		sink.UserActionUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	// get the final result
	updatedUserAction, err := h.Storage.Get(request.SessionActor, userAction.ID)
	if err != nil {
		logger.WithError(err).Error("fail to re-fetch user action after update")
		sink.UserActionUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	var response = cacao_common_service.UserActionModel{
		Session:   cacao_common_service.CopySessionActors(request.Session),
		ID:        updatedUserAction.ID,
		Owner:     updatedUserAction.Owner,
		Name:      updatedUserAction.Name,
		CreatedAt: updatedUserAction.CreatedAt,
		UpdatedAt: updatedUserAction.UpdatedAt,
	}
	sink.UserActionUpdatedEvent(response)
}

func (h *EventHandlers) updateErrorResponse(request cacao_common_service.UserActionModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.UserActionModel {
	return cacao_common_service.UserActionModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:               request.ID,
		Owner:            request.SessionActor,
		Name:             request.Name,
		UpdateFieldNames: request.UpdateFieldNames,
	}
}

// Delete deletes the user action
func (h *EventHandlers) Delete(request cacao_common_service.UserActionModel, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":    "user-action-service.domain",
		"function":   "EventHandlers.Delete",
		"actor":      request.SessionActor,
		"emulator":   request.SessionEmulator,
		"userAction": request.ID,
	})

	logger.Infof("deleting user action")
	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.UserActionDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action id is empty")
		logger.WithError(err).Error("bad request")
		sink.UserActionDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	// get the user action first
	userAction, err := h.Storage.Get(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch user action from storage before deletion")
		sink.UserActionDeleteFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}

	err = h.Storage.Delete(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete user action in storage")
		sink.UserActionDeleteFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}
	logger.Infof("user action deleted")

	sink.UserActionDeletedEvent(cacao_common_service.UserActionModel{
		Session: cacao_common_service.CopySessionActors(request.Session),
		ID:      userAction.ID,
		Owner:   userAction.Owner,
		Name:    userAction.Name,
	})
}

func (h *EventHandlers) deletionErrorResponse(request cacao_common_service.UserActionModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.UserActionModel {
	return cacao_common_service.UserActionModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
	}
}

func toCacaoError(err error) cacao_common_service.CacaoError {
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		return cerr
	}
	return cacao_common_service.NewCacaoGeneralError(err.Error())
}
