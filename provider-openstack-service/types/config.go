package types

import (
	"errors"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Some constants for NATS/STAN configuration
const (
	ServiceNATSQueueGroup      = "provider-openstack-queue"
	ServiceNATSWildCardSubject = "cyverse.providers.openstack.>"
	ServiceNATSDurableName     = "provider-openstack"
)

// Configuration contains the settings needed to configure this microservice.
type Configuration struct {
	Messaging                messaging2.NatsStanMsgConfig
	DefaultChannelBufferSize int `envconfig:"DEFAULT_CHANNEL_BUFFER_SIZE" default:"1"`
	QueryWorkerCount         int `envconfig:"QUERY_WORKER_COUNT" default:"10"`
	// Deprecated:
	PodName       string `envconfig:"POD_NAME"`
	K8SNamespace  string `envconfig:"NAMESPACE"`
	LogLevel      string `envconfig:"LOG_LEVEL" default:"debug"`
	RedisAddress  string `envconfig:"REDIS_ADDRESS" default:"redis:6379"`
	RedisPassword string `envconfig:"REDIS_PASSWORD" default:""`
	RedisDB       string `envConfig:"REDIS_DB" default:"0"`
	// expiration period for cache entry in seconds, default to 1 hour
	CacheTTL int `envConfig:"CACHE_TTL" default:"3600"`

	MongoDBConfig db.MongoDBConfig
}

// Override some config value
func (c *Configuration) Override() {
	c.Messaging.QueueGroup = ServiceNATSQueueGroup
	c.Messaging.WildcardSubject = ServiceNATSWildCardSubject
	c.Messaging.DurableName = ServiceNATSDurableName
	if c.Messaging.ClientID == "" && c.PodName != "" {
		c.Messaging.ClientID = c.PodName
	}
}

// Validate returns an error if there's something wrong with the configuration.
func (c *Configuration) Validate() error {
	if c.Messaging.ClientID == "" {
		return errors.New("NATS_CLIENT_ID environment variable must be set")
	}

	if c.QueryWorkerCount <= 0 {
		return errors.New("QUERY_WORKER_COUNT cannot be zero or negative")
	}

	if c.Messaging.DurableName == "" {
		return errors.New("NATS_DURABLE_NAME environment variable must be set")
	}

	if c.QueryWorkerCount < 1 {
		return errors.New("QUERY_WORKER_COUNT must be greater than 0")
	}
	return nil
}
