package types

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func TestOpenStackCatalogEntry_unmarshal(t *testing.T) {
	var input = []byte(`{
  "endpoints": [
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "public",
      "region_id": "Region1-ID",
      "url": "https://cyverse.org",
      "region": "Region1"
    },
    {
      "id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
      "interface": "public",
      "region_id": "Region2-ID",
      "url": "https://cyverse.org",
      "region": "Region2"
    },
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "admin",
      "region_id": "Region1-ID",
      "url": "https://cyverse.org",
      "region": "Region1"
    }
  ],
  "id": "ffffffffffffffffffffffffffffffff",
  "name": "nova",
  "type": "compute"
}`)
	var entry OpenStackCatalogEntry
	err := json.Unmarshal(input, &entry)
	assert.NoError(t, err)
	assert.Equal(t, OpenStackCatalogEntry{
		Name: "nova",
		Type: "compute",
		Endpoints: []OpenStackCatalogEndpoint{
			{
				ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				Interface: "public",
				RegionID:  "Region1-ID",
				URL:       "https://cyverse.org",
				Region:    "Region1",
			},
			{
				ID:        "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
				Interface: "public",
				RegionID:  "Region2-ID",
				URL:       "https://cyverse.org",
				Region:    "Region2",
			},
			{
				ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				Interface: "admin",
				RegionID:  "Region1-ID",
				URL:       "https://cyverse.org",
				Region:    "Region1",
			},
		},
	}, entry)
}

func TestOpenStackCatalogEntry_unmarshal_list(t *testing.T) {
	// For catalog list operation, the field names at root level start with a capitalized character.
	var input = []byte(`[
  {
    "Name": "keystone",
    "Type": "identity",
    "Endpoints": [
      {
        "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "interface": "public",
        "region_id": "Region1-ID",
        "url": "https://foobar.cyverse.org",
        "region": "Region1"
      }
    ]
  },
  {
    "Name": "nova",
    "Type": "compute",
    "Endpoints": [
      {
        "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "interface": "public",
        "region_id": "Region1-ID",
        "url": "https://foo.cyverse.org",
        "region": "Region1"
      },
      {
        "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "interface": "admin",
        "region_id": "Region1-ID",
        "url": "https://bar.cyverse.org",
        "region": "Region1"
      }
    ]
  }
]`)
	var list []OpenStackCatalogEntry
	err := json.Unmarshal(input, &list)
	assert.NoError(t, err)
	if !assert.NotNil(t, list) {
		return
	}
	if !assert.Len(t, list, 2) {
		return
	}
	assert.ElementsMatch(t, []OpenStackCatalogEntry{
		{
			Name: "keystone",
			Type: "identity",
			Endpoints: []OpenStackCatalogEndpoint{
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "public",
					RegionID:  "Region1-ID",
					URL:       "https://foobar.cyverse.org",
					Region:    "Region1",
				},
			},
		},
		{
			Name: "nova",
			Type: "compute",
			Endpoints: []OpenStackCatalogEndpoint{
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "public",
					RegionID:  "Region1-ID",
					URL:       "https://foo.cyverse.org",
					Region:    "Region1",
				},
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "admin",
					RegionID:  "Region1-ID",
					URL:       "https://bar.cyverse.org",
					Region:    "Region1",
				},
			},
		},
	}, list)
}

func TestAuthURLComparison(t *testing.T) {
	type args struct {
		url1 string
		url2 string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "same string",
			args: args{
				url1: "https://auth.openstack.cloud:123/",
				url2: "https://auth.openstack.cloud:123/",
			},
			want: true,
		},
		{
			name: "trailing slash",
			args: args{
				url1: "https://auth.openstack.cloud:123/",
				url2: "https://auth.openstack.cloud:123",
			},
			want: true,
		},
		{
			name: "trailing slash with longer path",
			args: args{
				url1: "https://auth.openstack.cloud:123/foo",
				url2: "https://auth.openstack.cloud:123/foo/",
			},
			want: true,
		},
		{
			name: "explicit default port",
			args: args{
				url1: "https://auth.openstack.cloud",
				url2: "https://auth.openstack.cloud:443",
			},
			want: true,
		},
		{
			name: "diff port",
			args: args{
				url1: "https://auth.openstack.cloud:123",
				url2: "https://auth.openstack.cloud:456",
			},
			want: false,
		},
		{
			name: "longer path",
			args: args{
				url1: "https://auth.openstack.cloud/foo",
				url2: "https://auth.openstack.cloud/foo/bar",
			},
			want: false,
		},
		{
			name: "one have empty path",
			args: args{
				// sometimes auth url does not specify path, this is to accommodate that
				url1: "https://auth.openstack.cloud/v3/",
				url2: "https://auth.openstack.cloud",
			},
			want: true,
		},
		{
			name: "diff scheme (http-and-https)",
			args: args{
				url1: "https://auth.openstack.cloud/foo",
				url2: "http://auth.openstack.cloud/foo",
			},
			want: false,
		},
		{
			name: "missing scheme",
			args: args{
				url1: "https://auth.openstack.cloud/foo",
				url2: "auth.openstack.cloud/foo",
			},
			want: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			url1, err := url.Parse(tt.args.url1)
			if !assert.NoError(t, err, "bad test case") {
				return
			}
			url2, err := url.Parse(tt.args.url2)
			if !assert.NoError(t, err, "bad test case") {
				return
			}
			got := AuthURLComparison(url1, url2)
			assert.Equalf(t, tt.want, got, "AuthURLComparison(%v, %v)", tt.args.url1, tt.args.url2)
		})
	}
}
