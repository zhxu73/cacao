package main

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"io"
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

var config types.Configuration

func init() {
	var err error

	if err = envconfig.Process("", &config); err != nil {
		log.WithError(err).Fatal("fail to load config from env var")
	}
	config.Override()
	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.WithError(err).Fatal("fail to set log level")
	}
	log.SetLevel(logLevel)

	if err = config.Validate(); err != nil {
		log.WithError(err).Fatal("fail to validate config")
	}
}

func main() {
	redisCache := adapters.NewRedisCacheManager(config)
	osAdapter := adapters.NewOpenStackAdapter()

	natsConn, err := config.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logCloserError(&natsConn)
	stanConn, err := config.Messaging.ConnectStan()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logCloserError(&stanConn)

	credMS := adapters.NewCredentialMicroservice(&natsConn, &stanConn)
	providerMS := adapters.NewProviderMetadataClient(&natsConn)
	workspaceMS := adapters.NewWorkspaceMicroservice(&natsConn, &stanConn)

	credMetadataStorage, err := adapters.NewCredentialMetadataMongoAdapter(&config)
	if err != nil {
		log.WithError(err).Error()
		return
	}

	var queryAdapter adapters.QueryAdapter
	queryAdapter.Init(config)
	defer logCloserError(&queryAdapter)
	var eventAdapter = adapters.NewEventAdapter(&stanConn)

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	d := domain.NewOpenStackDomain(&config, eventAdapter, &queryAdapter, osAdapter, redisCache, time.Second*time.Duration(config.CacheTTL), credMS, workspaceMS, providerMS, credMetadataStorage, &adapters.UTCTime{})
	err = d.Start(serviceCtx)
	if err != nil {
		cancelFunc()
		log.WithError(err).Error()
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
	}
}
