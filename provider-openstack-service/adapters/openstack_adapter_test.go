package adapters

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"net"
	"os"
	"strings"
	"testing"
	"time"
)

// This test requires openstack CLI to be installed and app cred to be set as env var, otherwise this is skipped.
func TestOpenStackAdapter_ListApplicationCredentials(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}

	credEnv := extractCredFromEnvVar()
	cred := types.Credential{
		Username:     "testuser123",
		ID:           "cred-id-123",
		CreatedAt:    time.Time{},
		UpdatedAt:    time.Time{},
		OpenStackEnv: credEnv,
	}
	osAdapter := NewOpenStackAdapter()
	credentials, err := osAdapter.ListApplicationCredentials(context.Background(), cred)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, credentials)
	// if we are using app cred to authenticate, then there must be at least 1 app cred
	assert.GreaterOrEqual(t, len(credentials), 1)
	for _, c := range credentials {
		assert.NotEmpty(t, c.ID)
		// 32 is the length of uuid with no hyphen, e.g. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		assert.GreaterOrEqual(t, len(c.ID), 32)
		assert.NotEmpty(t, c.Name)
		assert.NotEmpty(t, c.ProjectID)
	}
}

// This test requires openstack CLI to be installed and app cred to be set as env var, otherwise this is skipped.
func TestOpenStackAdapter_ListImages(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}

	credEnv := extractCredFromEnvVar()
	cred := types.Credential{
		Username:     "testuser123",
		ID:           "cred-id-123",
		CreatedAt:    time.Time{},
		UpdatedAt:    time.Time{},
		OpenStackEnv: credEnv,
	}
	osAdapter := NewOpenStackAdapter()
	regions, err := osAdapter.ListRegions(context.Background(), cred)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotEmpty(t, regions) {
		return
	}

	images, err := osAdapter.ListImages(context.Background(), cred, regions[0].Name)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, images)
	assert.GreaterOrEqual(t, len(images), 1) // let's assume there is at least 1 image
	for _, img := range images {
		assert.NotEmpty(t, img.ID)
		// 32 is the length of uuid with no hyphen, e.g. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		assert.GreaterOrEqual(t, len(img.ID), 32)
		assert.NotEmpty(t, img.Name)
		assert.GreaterOrEqual(t, img.Size, int64(1000))
	}
}

// This test requires openstack CLI to be installed and app cred to be set as env var, otherwise this is skipped.
func TestOpenStackAdapter_ListFlavors(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}

	credEnv := extractCredFromEnvVar()
	cred := types.Credential{
		Username:     "testuser123",
		ID:           "cred-id-123",
		CreatedAt:    time.Time{},
		UpdatedAt:    time.Time{},
		OpenStackEnv: credEnv,
	}
	osAdapter := NewOpenStackAdapter()
	regions, err := osAdapter.ListRegions(context.Background(), cred)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotEmpty(t, regions) {
		return
	}

	flavors, err := osAdapter.ListFlavors(context.Background(), cred, regions[0].Name)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, flavors)
	// if we are using app cred to authenticate, then there must be at least 1 app cred
	assert.GreaterOrEqual(t, len(flavors), 1)
	for _, f := range flavors {
		assert.NotEmpty(t, f.ID)
		assert.NotEmpty(t, f.Name)
		assert.GreaterOrEqual(t, f.Disk, int64(1))
	}
}

// This test requires openstack CLI to be installed and app cred to be set as env var, otherwise this is skipped.
func TestOpenStackAdapter_ListProjects(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}

	credEnv := extractCredFromEnvVar()
	cred := types.Credential{
		Username:     "testuser123",
		ID:           "cred-id-123",
		CreatedAt:    time.Time{},
		UpdatedAt:    time.Time{},
		OpenStackEnv: credEnv,
	}
	osAdapter := NewOpenStackAdapter()
	projects, err := osAdapter.ListProjects(context.Background(), cred)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, projects)
	assert.GreaterOrEqual(t, len(projects), 1)
	for _, proj := range projects {
		assert.NotEmpty(t, proj.ID)
		// 32 is the length of uuid with no hyphen, e.g. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		assert.GreaterOrEqual(t, len(proj.ID), 32)
		assert.NotEmpty(t, proj.Name)
		assert.NotEmpty(t, proj.DomainID)
	}
}

// This test requires openstack CLI to be installed and app cred to be set as env var, otherwise this is skipped.
func TestOpenStackAdapter_ListRegions(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}

	credEnv := extractCredFromEnvVar()
	cred := types.Credential{
		Username:     "testuser123",
		ID:           "cred-id-123",
		CreatedAt:    time.Time{},
		UpdatedAt:    time.Time{},
		OpenStackEnv: credEnv,
	}
	osAdapter := NewOpenStackAdapter()
	regions, err := osAdapter.ListRegions(context.Background(), cred)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, regions)
	// there is at least 1 region
	assert.GreaterOrEqual(t, len(regions), 1)
	for _, r := range regions {
		assert.NotEmpty(t, r.ID)
		assert.NotEmpty(t, r.Name)
	}
}

func TestOpenStackAdapter_ListRecordset(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}

	credEnv := extractCredFromEnvVar()
	cred := types.Credential{
		Username:     "testuser123",
		ID:           "cred-id-123",
		CreatedAt:    time.Time{},
		UpdatedAt:    time.Time{},
		OpenStackEnv: credEnv,
	}
	osAdapter := NewOpenStackAdapter()
	hasDNSService, err := osAdapter.hasService(context.Background(), cred.OpenStackEnv, "designate")
	if !assert.NoError(t, err) {
		return
	}
	if !hasDNSService {
		t.Skip("the OpenStack cloud has no designate service")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*20)
	zoneList, err := osAdapter.ListZone(ctx, cred)
	cancel()
	if !assert.NoError(t, err) {
		return
	}
	// make the assumption that there exists at least 1 zone in current project
	if !assert.NotEmpty(t, zoneList) {
		return
	}
	for _, z := range zoneList {
		assert.NotEmpty(t, z.Name)
		assert.NotEmpty(t, z.ID)
		// 32 is the length of uuid with no hyphen, e.g. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		assert.GreaterOrEqual(t, len(z.ID), 32)
		assert.NotEmpty(t, z.Status)
		assert.NotEmpty(t, z.Action)
	}
	t.Log(zoneList)

	// list all recordset in a single zone
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*20)
	list, err := osAdapter.ListRecordset(ctx, cred, zoneList[0].ID)
	cancel()
	if !assert.NoError(t, err) {
		return
	}
	// making the assumption that there exists DNS recordset in current project.
	if !assert.NotEmpty(t, list) {
		return
	}
	t.Logf("length: %d", len(list))
	for _, rs := range list {
		assert.NotEmpty(t, rs.ID)
		// 32 is the length of uuid with no hyphen, e.g. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		assert.GreaterOrEqual(t, len(rs.ID), 32)
		assert.NotEmpty(t, rs.Name)
		assert.NotEmpty(t, rs.Type)
		assert.Contains(t, []string{
			"A",
			"AAAA",
			"CNAME",
			"NS",
			"MX",
			"SOA",
			"TXT",
			"PTR",
			"SRV",
			"CERT",
			"DCHILD",
			"DNAME",
		}, rs.Type)
		assert.NotEmpty(t, rs.Records)
		if rs.Type == "A" || rs.Type == "AAAA" {
			_, err = net.LookupHost(rs.Name)
			assert.NoError(t, err)
		}
		for _, r := range rs.Records {
			assert.NotEmpty(t, r)
			if rs.Type == "A" || rs.Type == "AAAA" {
				assert.NotNil(t, net.ParseIP(r))
			}
		}
		assert.NotEmpty(t, rs.Status)
		assert.NotEmpty(t, rs.Action)
	}

	// list all recordset in current project
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*20)
	list, err = osAdapter.ListRecordset(ctx, cred, "all")
	cancel()
	if !assert.NoError(t, err) {
		return
	}
	// making the assumption that there exists DNS recordset in current project.
	if !assert.NotEmpty(t, list) {
		return
	}
	t.Logf("length: %d", len(list))
	for _, rs := range list {
		assert.NotEmpty(t, rs.ID)
		// 32 is the length of uuid with no hyphen, e.g. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		assert.GreaterOrEqual(t, len(rs.ID), 32)
		assert.NotEmpty(t, rs.Name)
		assert.NotEmpty(t, rs.Type)
		assert.NotEmpty(t, rs.Records)
		for _, r := range rs.Records {
			assert.NotEmpty(t, r)
		}
		assert.NotEmpty(t, rs.Status)
		assert.NotEmpty(t, rs.Action)
	}
}

func openstackCredSetInEnvVar() bool {
	if val, ok := os.LookupEnv("OS_AUTH_URL"); !ok || val == "" {
		return false
	}
	if val, ok := os.LookupEnv("OS_APPLICATION_CREDENTIAL_ID"); !ok || val == "" {
		return false
	}
	if val, ok := os.LookupEnv("OS_APPLICATION_CREDENTIAL_SECRET"); !ok || val == "" {
		return false
	}
	return true
}

func extractCredFromEnvVar() types.Environment {
	var result = make(types.Environment)
	for _, envvar := range os.Environ() {
		if !strings.HasPrefix(envvar, "OS_") {
			continue
		}
		splits := strings.Split(envvar, "=")
		if len(splits) < 2 {
			continue
		}
		result[splits[0]] = splits[1]
	}
	if len(result) < 3 {
		panic("failed to extract OS credential from env var")
	}
	return result
}
