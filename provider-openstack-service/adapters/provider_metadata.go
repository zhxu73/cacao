package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// ProviderMetadataClient ...
type ProviderMetadataClient struct {
	queryConn messaging2.QueryConnection
}

// NewProviderMetadataClient ...
func NewProviderMetadataClient(queryConn messaging2.QueryConnection) *ProviderMetadataClient {
	return &ProviderMetadataClient{
		queryConn: queryConn,
	}
}

// GetMetadata fetches metadata for a provider
func (svc ProviderMetadataClient) GetMetadata(ctx context.Context, actor, emulator string, providerID common.ID) (map[string]interface{}, error) {
	metadataSvcClient, err := service.NewNatsProviderClientFromConn(svc.queryConn, nil)
	if err != nil {
		return nil, err
	}
	provider, err := metadataSvcClient.Get(ctx, service.Actor{Actor: actor, Emulator: emulator}, providerID)
	if err != nil {
		return nil, err
	}
	return provider.Metadata, nil
}

// ListOpenStackProvider ...
func (svc ProviderMetadataClient) ListOpenStackProvider(ctx context.Context, actor, emulator string) ([]service.ProviderModel, error) {
	metadataSvcClient, err := service.NewNatsProviderClientFromConn(svc.queryConn, nil)
	if err != nil {
		return nil, err
	}
	providerList, err := metadataSvcClient.List(ctx, service.Actor{Actor: actor, Emulator: emulator})
	if err != nil {
		return nil, err
	}
	var resultList []service.ProviderModel
	for i := range providerList {
		// TODO ideally, we would pass a filter to the service client, that filters out the non-openstack ones.
		// only return openstack providers
		if providerList[i].Type == "openstack" {
			resultList = append(resultList, providerList[i])
		}
	}
	return resultList, nil
}
