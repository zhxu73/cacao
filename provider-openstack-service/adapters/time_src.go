package adapters

import "time"

// UTCTime is a source of current UTC time
type UTCTime struct {
}

// Now returns current UTC time
func (utc UTCTime) Now() time.Time {
	return time.Now().UTC()
}
