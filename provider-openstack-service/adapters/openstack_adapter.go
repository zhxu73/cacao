package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// OpenStackAdapter is used to access metadata from OpenStack clusters.
type OpenStackAdapter struct {
	cli  OpenStackCLI
	http OpenStackHTTPClient
}

// NewOpenStackAdapter creates a new *OpenStackAdapter.
func NewOpenStackAdapter() *OpenStackAdapter {
	baseEnv := splitEnvironment(os.Environ())

	retval := &OpenStackAdapter{
		cli: &OpenStackCLIAdapter{
			baseEnv: baseEnv,
		},
		http: &OpenStackHTTPClientAdapter{},
	}

	return retval
}

func splitEnvironment(env []string) types.Environment {
	retval := map[string]string{}

	for _, setting := range env {
		parts := strings.Split(setting, "=")
		if len(parts) != 2 {
			continue
		}
		retval[parts[0]] = parts[1]
	}

	return retval
}

// GetApplicationCredential attempts to retrieve a specific OpenStack application credential by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetApplicationCredential(ctx context.Context, credential types.Credential, id string) (*providers.ApplicationCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetApplicationCredential",
	})

	appCred, err := o.cli.getApplicationCredential(ctx, credential.OpenStackEnv, id)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	convertedList := o.convertApplicationCredentialList([]applicationCredentialListItem{appCred})
	return &convertedList[0], nil
}

// GetImage attempts to retrieve a specific OpenStack image by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.OpenStackImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetImage",
	})
	credential.OpenStackEnv = injectRegion(credential.OpenStackEnv, region)
	image, err := o.cli.getImage(ctx, credential.OpenStackEnv, id)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	convertedList := o.convertImageList([]imageListItem{image})
	return &convertedList[0], nil
}

// GetFlavor attempts to retrieve a specific OpenStack flavor by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetFlavor",
	})

	credential.OpenStackEnv = injectRegion(credential.OpenStackEnv, region)
	flavor, err := o.cli.getFlavor(ctx, credential.OpenStackEnv, id)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	convertedList := o.convertFlavorList([]flavorListItem{flavor})
	return &convertedList[0], nil
}

// GetProject attempts to retrieve a specific OpenStack Project by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetProject(ctx context.Context, credential types.Credential, id string) (*providers.Project, error) {
	project, err := o.cli.getProject(ctx, credential.OpenStackEnv, id)
	if err != nil {
		return nil, err
	}
	convertedList := o.convertProjectList([]projectListItem{project})
	return &convertedList[0], nil
}

// ListApplicationCredentials ...
func (o *OpenStackAdapter) ListApplicationCredentials(ctx context.Context, credential types.Credential) ([]providers.ApplicationCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListApplicationCredentials",
	})
	appCredList, err := o.cli.listApplicationCredentials(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}

	applicationCredentials := o.convertApplicationCredentialList(appCredList)
	logger.WithField("length", len(applicationCredentials)).Info("ApplicationCredentials listed")
	return applicationCredentials, err
}

func (o *OpenStackAdapter) convertApplicationCredentialList(appCredList []applicationCredentialListItem) []providers.ApplicationCredential {
	var result []providers.ApplicationCredential
	for _, appCred := range appCredList {
		result = append(result, appCred.Convert())
	}
	return result
}

// ListImages ...
func (o *OpenStackAdapter) ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.OpenStackImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListImages",
	})
	credential.OpenStackEnv = injectRegion(credential.OpenStackEnv, region)

	endpointURL, err := o.findPublicGlanceEndpointURL(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}

	token, err := o.GetToken(ctx, credential)
	if err != nil {
		return nil, err
	}

	var marker string
	var images = make([]providers.OpenStackImage, 0)
	for {
		var imageListPage []imageListItem
		imageListPage, marker, err = o.listImagesUsingAPISinglePage(ctx, endpointURL, token.ID, marker)
		if err != nil {
			return nil, err
		}
		images = append(images, o.convertImageList(imageListPage)...)
		if marker == "" {
			break
		}
	}
	logger.WithField("length", len(images)).Info("images listed.")
	return images, nil
}

func (o *OpenStackAdapter) listImagesUsingAPISinglePage(ctx context.Context, glanceEndpointURL string, tokenID string, markerInput string) (imageList []imageListItem, markerOutput string, err error) {
	respBody, err := o.http.MakePaginatedRequest(ctx, glanceEndpointURL, tokenID, markerInput, "v2", "images", 50)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"function": "OpenStackAdapter.listImagesUsingAPISinglePage"}).Error()
		return nil, "", err
	}
	var resp imageListAPIResponse
	err = json.Unmarshal(respBody, &resp)
	if err != nil {
		return nil, "", err
	}
	// resp.Next is the URL path, not the full URL
	markerOutput, err = o.http.ParsePaginationMarker(resp.Next)
	if err != nil {
		return nil, "", err
	}
	return resp.Images, markerOutput, nil
}

func (o *OpenStackAdapter) findPublicGlanceEndpointURL(ctx context.Context, credEnv types.Environment) (url string, err error) {
	region, ok := credEnv["OS_REGION_NAME"]
	if !ok {
		return "", fmt.Errorf("OS_REGION_NAME missing from credential, cannot find glance endpoint")
	}

	catalogEntry, err := o.cli.getCatalogEntry(ctx, credEnv, "glance")
	if err != nil {
		return "", err
	}

	for _, endpoint := range catalogEntry.Endpoints {
		// find the public endpoint of the region
		if endpoint.Interface == "public" && (endpoint.Region == region || endpoint.RegionID == region) {
			return endpoint.URL, nil
		}
	}
	return "", fmt.Errorf("no public OpenStack Glance endpoint available, fail to list images")
}

func (o *OpenStackAdapter) convertImageList(imageList []imageListItem) []providers.OpenStackImage {
	var result []providers.OpenStackImage
	for _, img := range imageList {
		result = append(result, img.Convert())
	}
	return result
}

// ListFlavors attempts to list all the available flavors in an OpenStack
// cluster.
func (o *OpenStackAdapter) ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListFlavors",
	})
	credential.OpenStackEnv = injectRegion(credential.OpenStackEnv, region)
	flavorList, err := o.cli.listFlavors(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}

	flavors := o.convertFlavorList(flavorList)
	logger.WithField("length", len(flavors)).Info("flavors listed")
	return flavors, err
}

func (o *OpenStackAdapter) convertFlavorList(flavorList []flavorListItem) []providers.Flavor {
	var result []providers.Flavor
	for _, flavor := range flavorList {
		result = append(result, flavor.Convert())
	}
	return result
}

// ListProjects attempts to list all the available projects in an OpenStack/
// cluster.
func (o *OpenStackAdapter) ListProjects(ctx context.Context, credential types.Credential) ([]providers.Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListProjects",
	})
	projectList, err := o.cli.listProjects(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}

	projects := o.convertProjectList(projectList)
	logger.WithField("length", len(projects)).Info("Projects listed")
	return projects, err
}

func (o *OpenStackAdapter) convertProjectList(projectList []projectListItem) []providers.Project {
	var result []providers.Project
	for _, proj := range projectList {
		result = append(result, proj.Convert())
	}
	return result
}

// ListRegions attempts to list all the regions that is available to user in an OpenStack cluster.
//
// Note: this retrieves regions from catalog entry for nova service rather than region list. This is because regions
// can be disabled by endpoint filtering. This will only return regions that is reported in the catalog nova endpoints.
func (o *OpenStackAdapter) ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListRegions",
	})
	catalogEntry, err := o.cli.getCatalogEntry(ctx, credential.OpenStackEnv, "nova")
	if err != nil {
		return nil, err
	}

	regions := getRegionsFromCatalogEntry(catalogEntry)
	logger.WithField("length", len(regions)).Info("Regions listed")
	return regions, nil
}

func getRegionsFromCatalogEntry(entry types.OpenStackCatalogEntry) []providers.Region {
	var regions []providers.Region
	for _, endpoint := range entry.Endpoints {
		if endpoint.Interface == "public" {
			// only include public endpoints
			regions = append(regions, providers.Region{
				ID:   endpoint.RegionID,
				Name: endpoint.Region,
			})
		}
	}
	return regions
}

// ListCatalog returns a list of catalog.
func (o *OpenStackAdapter) ListCatalog(ctx context.Context, credential types.Credential) ([]providers.CatalogEntry, error) {
	catalogEntries, err := o.cli.listCatalogs(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}

	return catalogEntries, err
}

// ListZone returns a list of DNS zones.
func (o *OpenStackAdapter) ListZone(ctx context.Context, credential types.Credential) ([]providers.DNSZone, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListZone",
	})
	zoneList, err := o.cli.listZone(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}
	var result []providers.DNSZone
	for _, z := range zoneList {
		result = append(result, z.Convert())
	}
	logger.WithField("length", len(result)).Info("zone listed")
	return result, nil
}

// ListRecordset returns a list of DNS recordset. This uses REST api directly instead of using CLI.
//
// https://docs.openstack.org/api-ref/dns/dns-api-v2-index.html
func (o *OpenStackAdapter) ListRecordset(ctx context.Context, credential types.Credential, zone string) ([]providers.DNSRecordset, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListRecordset",
	})
	designateURL, err := o.findPublicDesignateEndpointURL(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}
	token, err := o.cli.getToken(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}
	var result []providers.DNSRecordset
	var marker string
	for _, r := range zone {
		if unicode.IsDigit(r) || unicode.IsLetter(r) || r == '-' {
			continue
		}
		return nil, fmt.Errorf("bad zone ID")
	}
	for {
		var list []recordsetListItem
		if zone == "all" || zone == "" {
			list, marker, err = o.listRecordsetUsingAPISinglePage(ctx, designateURL, token.ID, marker)
		} else {
			list, marker, err = o.listRecordsetUsingAPISinglePageWithZoneID(ctx, designateURL, token.ID, marker, zone)
		}
		if err != nil {
			return nil, err
		}
		for _, rs := range list {
			result = append(result, rs.Convert())
		}
		if marker == "" {
			break
		}
	}
	logger.WithField("length", len(result)).Info("recordset listed")
	return result, nil
}

func (o *OpenStackAdapter) findPublicDesignateEndpointURL(ctx context.Context, credEnv types.Environment) (url string, err error) {
	region, ok := credEnv["OS_REGION_NAME"]
	if !ok {
		return "", fmt.Errorf("OS_REGION_NAME missing from credential, cannot find designate endpoint")
	}

	catalogEntry, err := o.cli.getCatalogEntry(ctx, credEnv, "designate")
	if err != nil {
		return "", err
	}

	for _, endpoint := range catalogEntry.Endpoints {
		// find the public endpoint of the region
		if endpoint.Interface == "public" && (endpoint.Region == region || endpoint.RegionID == region) {
			return endpoint.URL, nil
		}
	}
	return "", fmt.Errorf("no public OpenStack Designate endpoint available, fail to list recordset")
}

// https://docs.openstack.org/api-ref/dns/dns-api-v2-index.html#list-recordsets-in-a-zone
func (o *OpenStackAdapter) listRecordsetUsingAPISinglePageWithZoneID(ctx context.Context, designateEndpointURL string, tokenID string, markerInput string, zoneID string) (recordsetList []recordsetListItem, markerOutput string, err error) {
	respBody, err := o.http.MakePaginatedRequest(ctx, designateEndpointURL, tokenID, markerInput, "v2", fmt.Sprintf("zones/%s/recordsets", zoneID), 5)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"function": "OpenStackAdapter.listRecordsetUsingAPISinglePage"}).Error()
		return nil, "", err
	}
	var resp recordsetListAPIResponse
	err = json.Unmarshal(respBody, &resp)
	if err != nil {
		return nil, "", err
	}
	markerOutput, err = o.http.ParsePaginationMarker(resp.Links.Next)
	if err != nil {
		return nil, "", err
	}
	return resp.Recordsets, markerOutput, nil
}

// https://docs.openstack.org/api-ref/dns/dns-api-v2-index.html#list-all-recordsets-owned-by-project
func (o *OpenStackAdapter) listRecordsetUsingAPISinglePage(ctx context.Context, designateEndpointURL string, tokenID string, markerInput string) (recordsetList []recordsetListItem, markerOutput string, err error) {
	respBody, err := o.http.MakePaginatedRequest(ctx, designateEndpointURL, tokenID, markerInput, "v2", "recordsets", 5)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"function": "OpenStackAdapter.listRecordsetUsingAPISinglePage"}).Error()
		return nil, "", err
	}
	var resp recordsetListAPIResponse
	err = json.Unmarshal(respBody, &resp)
	if err != nil {
		return nil, "", err
	}
	markerOutput, err = o.http.ParsePaginationMarker(resp.Links.Next)
	if err != nil {
		return nil, "", err
	}
	return resp.Recordsets, markerOutput, nil
}

type recordsetListAPIResponse struct {
	Recordsets []recordsetListItem `json:"recordsets"`
	Links      struct {
		Self string `json:"self"`
		Next string `json:"next"`
	} `json:"links"`
	Metadata struct {
		TotalCount int `json:"total_count"`
	} `json:"metadata"`
}

type recordsetListItem struct {
	Description string `json:"description"`
	Links       struct {
		Self string `json:"self"`
	} `json:"links"`
	UpdatedAt interface{} `json:"updated_at"`
	Records   []string    `json:"records"`
	TTL       int         `json:"ttl"`
	ID        string      `json:"id"`
	Name      string      `json:"name"`
	ProjectID string      `json:"project_id"`
	ZoneID    string      `json:"zone_id"`
	ZoneName  string      `json:"zone_name"`
	CreatedAt string      `json:"created_at"`
	Version   int         `json:"version"`
	Type      string      `json:"type"`
	Status    string      `json:"status"`
	Action    string      `json:"action"`
}

// Convert ...
func (rs recordsetListItem) Convert() providers.DNSRecordset {
	return providers.DNSRecordset{
		ID:      rs.ID,
		Name:    rs.Name,
		Type:    rs.Type,
		Records: rs.Records,
		Status:  rs.Status,
		Action:  rs.Action,
	}
}

// GetToken issues a token.
func (o *OpenStackAdapter) GetToken(ctx context.Context, credential types.Credential) (*providers.Token, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetToken",
	})

	token, err := o.cli.getToken(ctx, credential.OpenStackEnv)
	if err != nil {
		return nil, err
	}
	logger.WithField("token", token).Info("Token retrieved")
	return &token, err
}

// InspectCredential inspects a credential.
func (o *OpenStackAdapter) InspectCredential(ctx context.Context, credential types.Credential) (types.CredentialMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.InspectCredential",
		"credID":   credential.ID,
		"owner":    credential.Username,
	})
	tokenInfo, err := o.cli.getToken(ctx, credential.OpenStackEnv)
	if err != nil {
		return types.CredentialMetadata{}, err
	}
	var project projectListItem
	if tokenInfo.ProjectID != "" {
		project, err = o.cli.getProject(ctx, credential.OpenStackEnv, tokenInfo.ProjectID)
		if err != nil {
			logger.WithError(err).Error("fail to fetch openstack project")
			return types.CredentialMetadata{}, err
		}
	}

	var domain domainCLIShow
	if project.DomainID != "" {
		domain, err = o.cli.getDomain(ctx, credential.OpenStackEnv, project.DomainID)
		if err != nil {
			logger.WithError(err).Error("fail to fetch openstack domain")
			return types.CredentialMetadata{}, err
		}
	}

	err = o.cli.revokeToken(ctx, credential.OpenStackEnv, tokenInfo.ID)
	if err != nil {
		logger.WithError(err).Error("fail to revoke token")
	}

	return types.CredentialMetadata{
		ID:                credential.ID,
		Owner:             credential.Username,
		UpdatedAt:         time.Time{},
		AuthURL:           credential.OpenStackEnv["OS_AUTH_URL"],
		DomainID:          project.DomainID,
		DomainName:        domain.Name,
		UserID:            tokenInfo.UserID,
		ProjectID:         project.ID,
		ProjectName:       project.Name,
		MetadataUpdatedAt: time.Now().UTC(),
	}, nil
}

// AuthenticationTest attempts to list all the available projects in an OpenStack cluster.
func (o *OpenStackAdapter) AuthenticationTest(ctx context.Context, credential types.Environment) error {
	_, err := o.cli.getToken(ctx, credential)
	return err
}

// CreateApplicationCredential creates an application credential with the given name.
func (o *OpenStackAdapter) CreateApplicationCredential(ctx context.Context, credential types.Credential, scope providers.ProjectScope, name string) (types.ApplicationCredential, error) {
	return o.cli.createApplicationCredential(ctx, o.changeProjectScope(credential.OpenStackEnv, scope), name)
}

// changes the project scope of the credential inside context
func (o *OpenStackAdapter) changeProjectScope(credEnv types.Environment, newScope providers.ProjectScope) types.Environment {
	// clear the scope before re-setting it
	clearCredScope(credEnv)
	setCredScope(credEnv, newScope)
	return credEnv
}

// DeleteApplicationCredential deletes an application credential
func (o *OpenStackAdapter) DeleteApplicationCredential(ctx context.Context, credential types.Credential, idOrName string) error {
	_, err := o.cli.deleteApplicationCredential(ctx, credential.OpenStackEnv, idOrName)
	return err
}

func (o *OpenStackAdapter) hasService(ctx context.Context, credEnv types.Environment, openstackService string) (bool, error) {
	_, err := o.cli.getCatalogEntry(ctx, credEnv, openstackService)
	if err == nil {
		return true, nil
	}
	if cerr, ok := err.(service.CacaoError); ok && cerr.StandardError() == service.CacaoNotFoundErrorMessage {
		return false, nil
	}
	return false, err
}

func injectRegion(credential types.Environment, region string) types.Environment {
	credential["OS_REGION_NAME"] = region
	return credential
}

func clearCredScope(env types.Environment) {
	delete(env, "OS_PROJECT_NAME")
	delete(env, "OS_PROJECT_ID")
	delete(env, "OS_PROJECT_DOMAIN_NAME")
	delete(env, "OS_PROJECT_DOMAIN_ID")
}

func setCredScope(env types.Environment, scope providers.ProjectScope) {
	if scope.Project.Name != "" {
		env["OS_PROJECT_NAME"] = scope.Project.Name
	} else if scope.Project.ID != "" {
		env["OS_PROJECT_ID"] = scope.Project.ID
	}
	if scope.Domain.Name != "" {
		env["OS_PROJECT_DOMAIN_NAME"] = scope.Domain.Name
	} else if scope.Domain.ID != "" {
		env["OS_PROJECT_DOMAIN_ID"] = scope.Domain.ID
	}
}

// imageList is a list of images
type imageListAPIResponse struct {
	Images []imageListItem `json:"images"`
	Next   string          `json:"next"`
}

// imageListItem is an item in the "image list" output, this has different json field names than "image show" which is what providers.Image is based on
type imageListItem struct {
	ID              string   `json:"id"`
	Name            string   `json:"name"`
	DiskFormat      string   `json:"disk_format"`
	ContainerFormat string   `json:"container_format"`
	MinDisk         int64    `json:"min_disk"`
	MinRAM          int64    `json:"min_ram"`
	Size            int64    `json:"size"`
	Checksum        string   `json:"checksum"`
	Status          string   `json:"status"`
	Visibility      string   `json:"visibility"`
	Protected       bool     `json:"protected"`
	Project         string   `json:"project"`
	Tags            []string `json:"tags"`
}

// Convert to providers.Image
func (img imageListItem) Convert() providers.OpenStackImage {
	return providers.OpenStackImage{
		Image: providers.Image{
			ID:     img.ID,
			Name:   img.Name,
			Status: img.Status,
		},
		DiskFormat:      img.DiskFormat,
		MinDisk:         img.MinDisk,
		MinRAM:          img.MinRAM,
		ContainerFormat: img.ContainerFormat,
		Checksum:        img.Checksum,
		Size:            img.Size,
		Visibility:      img.Visibility,
		Protected:       img.Protected,
		Project:         img.Project,
		Tags:            img.Tags,
	}
}

// flavorListItem is an item in the "flavor list" output, this has different json field names than "flavor show" which is what providers.Flavor is based on
type flavorListItem struct {
	ID        string `json:"ID"`
	Name      string `json:"Name"`
	RAM       int64  `json:"RAM"`
	Disk      int64  `json:"Disk"`
	Ephemeral int64  `json:"Ephemeral"`
	VCPUs     int64  `json:"VCPUs"`
	IsPublic  bool   `json:"Is Public"` // different tag than providers.Flavor
}

// Convert converts to providers.Flavor
func (f flavorListItem) Convert() providers.Flavor {
	return providers.Flavor{
		ID:        f.ID,
		Name:      f.Name,
		RAM:       f.RAM,
		Ephemeral: f.Ephemeral,
		VCPUs:     f.VCPUs,
		IsPublic:  f.IsPublic,
		Disk:      f.Disk,
	}
}

// projectListItem is an item in the "project list" output, this has different json field names than "project show" which is what providers.Project is based on
type projectListItem struct {
	ID          string `json:"ID"`
	Name        string `json:"Name"`
	DomainID    string `json:"Domain ID"` // different tag than providers.Project
	Description string `json:"Description"`
	Enabled     bool   `json:"Enabled"`
}

// Convert converts to providers.Project
func (proj projectListItem) Convert() providers.Project {
	return providers.Project{
		ID:          proj.ID,
		Name:        proj.Name,
		Description: proj.Description,
		DomainID:    proj.DomainID,
		Enabled:     proj.Enabled,
	}
}

// applicationCredentialListItem is an item in the "application credential list" output, this has different json field names than "application credential show" which is what providers.ApplicationCredential is based on
type applicationCredentialListItem struct {
	ID          string      `json:"ID"`
	Name        string      `json:"Name"`
	ProjectID   string      `json:"Project ID"`
	Description string      `json:"Description"`
	ExpiresAt   interface{} `json:"Expires At"`
}

// Convert converts to providers.ApplicationCredential
func (appCred applicationCredentialListItem) Convert() providers.ApplicationCredential {
	return providers.ApplicationCredential{
		ID:           appCred.ID,
		Name:         appCred.Name,
		Description:  appCred.Description,
		ExpiresAt:    appCred.ExpiresAt,
		ProjectID:    appCred.ProjectID,
		Roles:        nil,
		Unrestricted: false,
	}
}
