package adapters

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"os"
	"os/exec"
	"testing"
)

// This test requires openstack CLI to be installed, otherwise this is skipped.
// This test does not require any openstack credentials.
// This test checks if openstack CLI return the expected error when app cred ID used to authenticate does not exist.
func TestOpenStackCLIAdapter_ErrApplicationCredentialNotFound(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	o := OpenStackCLIAdapter{
		baseEnv: splitEnvironment(os.Environ()),
	}
	var credEnv = types.Environment{
		"OS_AUTH_TYPE":                     "v3applicationcredential",
		"OS_AUTH_URL":                      "https://js2.jetstream-cloud.org:5000/v3/",
		"OS_IDENTITY_API_VERSION":          "3",
		"OS_REGION_NAME":                   "IU",
		"OS_INTERFACE":                     "public",
		"OS_APPLICATION_CREDENTIAL_ID":     "x",
		"OS_APPLICATION_CREDENTIAL_SECRET": "x",
	}
	_, err := o.getToken(context.TODO(), credEnv)
	if !assert.Error(t, err) {
		return
	}
	if !errors.Is(err, types.ErrApplicationCredentialNotFound) {
		t.Error("error is not types.ErrApplicationCredentialNotFound")
		t.Log(err)
	}
}

// This test requires openstack CLI to be installed and app cred to be set as env var, otherwise this is skipped.
// This test checks if openstack CLI return the expected error when app cred ID used to authenticate exist but the secret is bad.
func TestOpenStackCLIAdapter_ErrAuthentication(t *testing.T) {
	if !openstackCLIInstalled() {
		t.Skip(fmt.Sprintf("openstack cli is NOT in path, it is required for this test to run"))
		return
	}
	if !openstackCredSetInEnvVar() {
		t.Skip("openstack credential is not set in environment variable")
		return
	}
	o := OpenStackCLIAdapter{
		baseEnv: splitEnvironment(os.Environ()),
	}

	credEnv := extractCredFromEnvVar()
	if _, ok := credEnv["OS_APPLICATION_CREDENTIAL_ID"]; !ok {
		assert.Fail(t, "'OS_APPLICATION_CREDENTIAL_ID' is not set in env var, we need application credential to run this test")
		return
	}

	// override OS_APPLICATION_CREDENTIAL_SECRET with some random bad value, so that the auth would fail
	randomBytes := make([]byte, 8)
	_, err := rand.Read(randomBytes)
	if !assert.NoError(t, err) {
		return
	}
	credEnv["OS_APPLICATION_CREDENTIAL_SECRET"] = base64.StdEncoding.EncodeToString(randomBytes)

	_, err = o.getToken(context.TODO(), credEnv)
	if !assert.Error(t, err) {
		return
	}
	if !errors.Is(err, types.ErrAuthentication) {
		// since we purposefully override the secret, we should get
		// types.ErrAuthentication as error, unless there is a bug in the code or cli, or
		// we used a bad app cred to test this.
		t.Error("error is not types.ErrAuthentication")
		t.Log(err)
	}
}

func openstackCLIInstalled() bool {
	cmd := exec.Command("openstack", "--version")
	err := cmd.Run()
	if err != nil {
		log.Info(err.Error())
	}
	return err == nil
}
