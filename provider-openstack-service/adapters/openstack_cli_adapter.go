package adapters

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"io"
	"os/exec"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// OpenStackCLI is an interface that comprises the necessary operations for
// interacting with the OpenStack CLI
type OpenStackCLI interface {
	getApplicationCredential(context.Context, types.Environment, string) (applicationCredentialListItem, error)
	getImage(context.Context, types.Environment, string) (imageListItem, error)
	getFlavor(context.Context, types.Environment, string) (flavorListItem, error)
	getDomain(ctx context.Context, env types.Environment, id string) (domainCLIShow, error)
	getProject(context.Context, types.Environment, string) (projectListItem, error)
	getCatalogEntry(context.Context, types.Environment, string) (types.OpenStackCatalogEntry, error)
	getToken(ctx context.Context, env types.Environment) (providers.Token, error)
	revokeToken(ctx context.Context, env types.Environment, tokenID string) error
	listApplicationCredentials(context.Context, types.Environment) ([]applicationCredentialListItem, error)
	listFlavors(context.Context, types.Environment) ([]flavorListItem, error)
	//ListImages(context.Context, types.Environment) ([]byte, error)
	listProjects(context.Context, types.Environment) ([]projectListItem, error)
	listCatalogs(ctx context.Context, env types.Environment) ([]providers.CatalogEntry, error)
	listZone(ctx context.Context, env types.Environment) ([]cliZoneListItem, error)
	listRecordset(ctx context.Context, env types.Environment, zoneID string) ([]cliRecordsetListItem, error)
	createApplicationCredential(context.Context, types.Environment, string) (types.ApplicationCredential, error)
	deleteApplicationCredential(ctx context.Context, env types.Environment, id string) ([]byte, error)
}

// OpenStackCLIAdapter is a concrete implementation of the OpenStackCLI interface
type OpenStackCLIAdapter struct {
	baseEnv types.Environment
}

// getApplicationCredential retrieves a specific OpenStack ApplicationCredential by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) getApplicationCredential(ctx context.Context, env types.Environment, id string) (applicationCredentialListItem, error) {
	appCred, err := cliJSON[appCredCLIShow](ctx, env, o, []string{"application", "credential", "show", id, "-f", "json"})
	if err != nil {
		return applicationCredentialListItem{}, err
	}
	return applicationCredentialListItem{
		ID:          appCred.ID,
		Name:        appCred.Name,
		ProjectID:   appCred.ProjectID,
		Description: appCred.Description,
		ExpiresAt:   appCred.ExpiresAt,
	}, nil
}

// schema of `openstack application credential show -f json`
type appCredCLIShow struct {
	Description  string      `json:"description"`
	ExpiresAt    interface{} `json:"expires_at"`
	ID           string      `json:"id"`
	Name         string      `json:"name"`
	ProjectID    string      `json:"project_id"`
	Roles        string      `json:"roles"`
	System       interface{} `json:"system"`
	Unrestricted bool        `json:"unrestricted"`
	UserID       string      `json:"user_id"`
}

// getFlavor retrieves a specific OpenStack Flavor by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) getFlavor(ctx context.Context, env types.Environment, id string) (flavorListItem, error) {
	flavor, err := cliJSON[flavorCLIShow](ctx, env, o, []string{"flavor", "show", id, "-f", "json"})
	if err != nil {
		return flavorListItem{}, err
	}
	return flavorListItem{
		ID:        flavor.ID,
		Name:      flavor.Name,
		RAM:       int64(flavor.RAM),
		Disk:      int64(flavor.Disk),
		Ephemeral: int64(flavor.OSFLVEXTDATAEphemeral),
		VCPUs:     int64(flavor.Vcpus),
		IsPublic:  flavor.OsFlavorAccessIsPublic,
	}, nil
}

// schema of `openstack flavor show -f json`
type flavorCLIShow struct {
	OSFLVDISABLEDDisabled  bool        `json:"OS-FLV-DISABLED:disabled"`
	OSFLVEXTDATAEphemeral  int         `json:"OS-FLV-EXT-DATA:ephemeral"`
	AccessProjectIds       []string    `json:"access_project_ids"`
	Description            interface{} `json:"description"`
	Disk                   int         `json:"disk"`
	ID                     string      `json:"id"`
	Name                   string      `json:"name"`
	OsFlavorAccessIsPublic bool        `json:"os-flavor-access:is_public"`
	Properties             struct {
		AggregateInstanceExtraSpecsCPU string `json:"aggregate_instance_extra_specs:cpu"`
	} `json:"properties"`
	RAM        int `json:"ram"`
	RxtxFactor int `json:"rxtx_factor"`
	Swap       int `json:"swap"`
	Vcpus      int `json:"vcpus"`
}

// getImage retrieves a specific OpenStack Image by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) getImage(ctx context.Context, env types.Environment, id string) (imageListItem, error) {
	image, err := cliJSON[imageCLIShow](ctx, env, o, []string{"image", "show", id, "-f", "json"})
	if err != nil {
		return imageListItem{}, err
	}
	return imageListItem{
		ID:              image.ID,
		Name:            image.Name,
		DiskFormat:      image.DiskFormat,
		ContainerFormat: image.ContainerFormat,
		MinDisk:         int64(image.MinDisk),
		MinRAM:          int64(image.MinRAM),
		Size:            image.Size,
		Checksum:        image.Checksum,
		Status:          image.Status,
		Visibility:      image.Visibility,
		Protected:       image.Protected,
		Project:         "",
		Tags:            image.Tags,
	}, nil
}

// schema of `openstack image show -f json`
type imageCLIShow struct {
	Checksum        string    `json:"checksum"`
	ContainerFormat string    `json:"container_format"`
	CreatedAt       time.Time `json:"created_at"`
	DiskFormat      string    `json:"disk_format"`
	File            string    `json:"file"`
	ID              string    `json:"id"`
	MinDisk         int       `json:"min_disk"`
	MinRAM          int       `json:"min_ram"`
	Name            string    `json:"name"`
	Owner           string    `json:"owner"`
	Properties      struct {
		OsHidden                      bool   `json:"os_hidden"`
		OsHashAlgo                    string `json:"os_hash_algo"`
		OsHashValue                   string `json:"os_hash_value"`
		HwFirmwareType                string `json:"hw_firmware_type"`
		OwnerSpecifiedOpenstackMd5    string `json:"owner_specified.openstack.md5"`
		OwnerSpecifiedOpenstackObject string `json:"owner_specified.openstack.object"`
		OwnerSpecifiedOpenstackSha256 string `json:"owner_specified.openstack.sha256"`
		Locations                     []struct {
			URL      string `json:"url"`
			Metadata struct {
			} `json:"metadata"`
		} `json:"locations"`
		DirectURL        string `json:"direct_url"`
		HwDiskBus        string `json:"hw_disk_bus"`
		HwMachineType    string `json:"hw_machine_type"`
		HwScsiModel      string `json:"hw_scsi_model"`
		HwQemuGuestAgent string `json:"hw_qemu_guest_agent"`
		OsRequireQuiesce bool   `json:"os_require_quiesce"`
	} `json:"properties"`
	Protected   bool      `json:"protected"`
	Schema      string    `json:"schema"`
	Size        int64     `json:"size"`
	Status      string    `json:"status"`
	Tags        []string  `json:"tags"`
	UpdatedAt   time.Time `json:"updated_at"`
	VirtualSize int64     `json:"virtual_size"`
	Visibility  string    `json:"visibility"`
}

type domainCLIShow struct {
	Description string                 `json:"description"`
	Enabled     bool                   `json:"enabled"`
	ID          string                 `json:"id"`
	Name        string                 `json:"name"`
	Options     map[string]interface{} `json:"options"`
	Tags        []string               `json:"tags"`
}

// getDomain retrieves a specific OpenStack domain by ID and returns it
func (o OpenStackCLIAdapter) getDomain(ctx context.Context, env types.Environment, id string) (domainCLIShow, error) {
	return cliJSON[domainCLIShow](ctx, env, o, []string{"domain", "show", id, "-f", "json"})
}

// getProject retrieves a specific OpenStack Project by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) getProject(ctx context.Context, env types.Environment, id string) (projectListItem, error) {
	return cliJSON[projectListItem](ctx, env, o, []string{"project", "show", id, "-f", "json"})
}

// getCatalogEntry retrieves catalog entry for a service (e.g. "nova") and returns it in JSON formatted byte slice.
func (o OpenStackCLIAdapter) getCatalogEntry(ctx context.Context, env types.Environment, serviceName string) (types.OpenStackCatalogEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackCLIAdapter.GetCatalogEntry",
	})
	output, err := o.execute(ctx, env, []string{"catalog", "show", serviceName, "-f", "json"})
	if err != nil {
		logger.WithError(err).Error()
		if strings.Contains(string(output), "not found") || strings.Contains(err.Error(), "not found") {
			return types.OpenStackCatalogEntry{}, service.NewCacaoNotFoundError(fmt.Sprintf("%s service not found", serviceName))
		}
		return types.OpenStackCatalogEntry{}, err
	}
	var entry types.OpenStackCatalogEntry
	err = json.Unmarshal(output, &entry)
	if err != nil {
		if strings.Contains(string(output), "not found") {
			// sometimes CLI prints the error msg directly, so output is not fully JSON, and has exit code 0
			return types.OpenStackCatalogEntry{}, service.NewCacaoNotFoundError(fmt.Sprintf("%s service not found", serviceName))
		}
		logger.WithField("output", output).WithError(err).Error("'catalog show' result not json")
		return types.OpenStackCatalogEntry{}, fmt.Errorf("'catalog show' result not json, %w", err)
	}
	if entry.Name == "" || entry.Type == "" || len(entry.Endpoints) == 0 {
		return types.OpenStackCatalogEntry{}, service.NewCacaoNotFoundError(fmt.Sprintf("%s service not found", serviceName))
	}
	return entry, nil
}

// listApplicationCredentials retrieves all the available OpenStack application credentials and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) listApplicationCredentials(ctx context.Context, env types.Environment) ([]applicationCredentialListItem, error) {
	return cliJSON[[]applicationCredentialListItem](ctx, env, o, []string{"application", "credential", "list", "-f", "json"})
}

// listImages retrieves all the available OpenStack flavors and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) listImages(ctx context.Context, env types.Environment) ([]byte, error) {
	// not used, we switch to calling REST api directly in OpenStackAdapter
	return o.execute(ctx, env, []string{"image", "list", "-f", "json", "--long"})

}

// listFlavors retrieves all the available OpenStack flavors and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) listFlavors(ctx context.Context, env types.Environment) ([]flavorListItem, error) {
	return cliJSON[[]flavorListItem](ctx, env, o, []string{"flavor", "list", "-f", "json", "--all"})
}

// listProjects retrieves all the available OpenStack projects and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) listProjects(ctx context.Context, env types.Environment) ([]projectListItem, error) {
	return cliJSON[[]projectListItem](ctx, env, o, []string{"project", "list", "-f", "json", "--my-projects", "--long"})
}

// listCatalogs retrieves the catalog list (list of REST endpoints) and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) listCatalogs(ctx context.Context, env types.Environment) ([]providers.CatalogEntry, error) {
	return cliJSON[[]providers.CatalogEntry](ctx, env, o, []string{"catalog", "list", "-f", "json"})
}

// schema of list item from `openstack zone list -f json`
type cliZoneListItem struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Type   string `json:"type"`
	Serial int    `json:"serial"`
	Status string `json:"status"`
	Action string `json:"action"`
}

func (z cliZoneListItem) Convert() providers.DNSZone {
	return providers.DNSZone{
		ID:     z.ID,
		Name:   z.Name,
		Type:   z.Type,
		Serial: z.Serial,
		Status: z.Status,
		Action: z.Action,
	}
}

// listZone list all active DNS zone in the project.
func (o OpenStackCLIAdapter) listZone(ctx context.Context, env types.Environment) ([]cliZoneListItem, error) {
	return cliJSON[[]cliZoneListItem](ctx, env, o, []string{"zone", "list", "-f", "json", "--status", "ACTIVE"})
}

// schema of list item from `openstack recordset list <zone-id> -f json`
type cliRecordsetListItem struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Type    string `json:"type"`
	Records string `json:"records"`
	Status  string `json:"status"`
	Action  string `json:"action"`
}

// listRecordset lists all active record set in the zone.
func (o OpenStackCLIAdapter) listRecordset(ctx context.Context, env types.Environment, zoneID string) ([]cliRecordsetListItem, error) {
	return cliJSON[[]cliRecordsetListItem](ctx, env, o, []string{"recordset", "list", zoneID, "-f", "json", "--status", "ACTIVE"})
}

// createApplicationCredential creates an application credential. The app cred is unrestricted because that is the only way to allow it to delete itself.
func (o OpenStackCLIAdapter) createApplicationCredential(ctx context.Context, env types.Environment, name string) (types.ApplicationCredential, error) {
	// TODO consider adding access rule to restrict the ability to create application credential with the newly created one.
	// Note that this might be difficult because access rule is whitelist rather than blacklist. So we need to list all other endpoints.
	return cliJSON[types.ApplicationCredential](ctx, env, o, []string{"application", "credential", "create", "--unrestricted", "-f", "json", name})
}

// deleteApplicationCredential ...
func (o OpenStackCLIAdapter) deleteApplicationCredential(ctx context.Context, env types.Environment, idOrName string) ([]byte, error) {
	return o.execute(ctx, env, []string{"application", "credential", "delete", idOrName})
}

// getToken issues a token based on the current scope and returns result in a JSON formatted byte slice.
//
// This will return types.ErrApplicationCredentialNotFound if application credential used to authenticate is not found (app cred ID not exists).
// This will return types.ErrAuthentication if application credential ID exist, but secret is incorrect.
func (o OpenStackCLIAdapter) getToken(ctx context.Context, env types.Environment) (providers.Token, error) {
	output, err := o.execute(ctx, env, []string{"token", "issue", "-f", "json"})
	if err == nil {
		var token providers.Token
		err2 := json.Unmarshal(output, &token)
		if err2 != nil {
			return providers.Token{}, err2
		}
		return token, nil
	}
	return providers.Token{}, o.parseGetTokenError(output, err)
}

// parsing error output to determine what kind of error we are getting
func (o OpenStackCLIAdapter) parseGetTokenError(output []byte, err error) error {
	var jsonOutput interface{}
	errJSON := json.Unmarshal(output, &jsonOutput)
	if errJSON == nil {
		// openstack CLI does not output JSON when errored, at least when targeting JS2.
		// unmarshal to json is meant as a check to detect changes to CLI.
		log.WithField("output", string(output)).Warn("openstack cli is returning json when errored, this is NOT expected")
	}

	lowerCaseErrorOutput := strings.ToLower(err.Error())
	if strings.Contains(lowerCaseErrorOutput, "could not find application credential") {
		// application credential ID does not exist
		return types.ErrApplicationCredentialNotFound
	}
	if strings.Contains(lowerCaseErrorOutput, "the request you have made requires authentication") {
		// application credential ID exist, but secret is incorrect
		return types.ErrAuthentication
	}
	return err
}

func (o OpenStackCLIAdapter) revokeToken(ctx context.Context, env types.Environment, tokenID string) error {
	_, err := o.execute(ctx, env, []string{"token", "revoke", tokenID})
	return err
}

func cliJSON[T any](ctx context.Context, env types.Environment, o OpenStackCLIAdapter, args []string) (T, error) {
	var jsonStruct T
	output, err := o.execute(ctx, env, args)
	if err != nil {
		return jsonStruct, err
	}

	err = json.Unmarshal(output, &jsonStruct)
	if err != nil {
		return jsonStruct, err
	}
	return jsonStruct, err
}

// execute calls the openstack command with the provided context and arguments
// and returns a []byte containing the stdout of the command.
// Note: baseEnv is read only.
func (o OpenStackCLIAdapter) execute(ctx context.Context, credEnv types.Environment, args []string) ([]byte, error) {
	var (
		err error
	)

	// make a copy of baseEnv to prevent race condition, since the caller is likely passing in baseEnv as a singleton,
	// thus concurrent read & write is possible with concurrent requests.
	env := copyMap(o.baseEnv)

	// Merge the passed in environment with the process's environment
	// This will pick up the EDITOR env var, which the credentials
	// are unlikely to provide.
	env, err = cleanAndMergeEnvVar(credEnv, env)
	if err != nil {
		return nil, err
	}

	flattenedEnvironment := flattenEnvironment(env)

	cmd := exec.CommandContext(ctx, "openstack", args...)
	cmd.Env = flattenedEnvironment

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, err
	}

	if err = cmd.Start(); err != nil {
		return nil, err
	}

	var stdoutOutput bytes.Buffer
	_, err = stdoutOutput.ReadFrom(stdout)
	if err != nil {
		return nil, err
	}
	var stderrOutput strings.Builder
	_, err = io.Copy(&stderrOutput, stderr)
	if err != nil {
		return nil, err
	}

	if err = cmd.Wait(); err != nil {
		log.WithField("stdout", stdoutOutput.String()).Info("cli output")
		log.WithField("stderr", stderrOutput.String()).Info("cli output")
		return nil, fmt.Errorf("%w, %s", err, stderrOutput.String())
	}

	return stdoutOutput.Bytes(), nil
}

// flattenEnvironment is a helper function that accepts an Environment struct
// and returns a []string with values in the format "<property name>=<value>".
// This makes it possible to set the environment in which a command executes.
func flattenEnvironment(env types.Environment) []string {
	var flattenedEnv []string
	for k, v := range env {
		flattenedEnv = append(flattenedEnv, fmt.Sprintf("%s=%s", k, v))
	}
	return flattenedEnv
}

// make a copy of a map
func copyMap(old map[string]string) map[string]string {
	newMap := make(map[string]string, len(old))
	for key, val := range old {
		newMap[key] = val
	}
	return newMap
}

func cleanAndMergeEnvVar(credEnv, baseEnv types.Environment) (types.Environment, error) {
	size := 0
	var result = baseEnv
	for k, v := range credEnv {
		if !strings.HasPrefix(k, "OS_") {
			continue
		}
		result[k] = v
		size += len(k)
		size += len(v)
	}
	if size > 1024 {
		// there is a size limit on env var, in ubuntu is 2k.
		// if the size exceeds 1k, something is not right
		log.Warn("environment is too large")
		return make(types.Environment), fmt.Errorf("environment is too large")
	}
	return result, nil
}
