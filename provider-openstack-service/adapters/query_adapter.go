package adapters

import (
	"context"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"sync"
)

// QueryAdapter implements ports.IncomingQueryPort & ports.OutgoingQueryPort
type QueryAdapter struct {
	queryChan    chan types.CloudEventRequest
	clientID     string
	subject      string
	queue        string
	nc           *nats.Conn
	sub          *nats.Subscription
	closeChannel sync.Once
}

// Init perform initialization on the adapter
func (adapter *QueryAdapter) Init(conf types.Configuration) {
	adapter.clientID = adapter.generateClientID(conf)

	err := adapter.natsConnect(conf)
	if err != nil {
		log.WithError(err).Panic("fail to connect to NATS")
	}

	adapter.subject = conf.Messaging.WildcardSubject
	adapter.queue = conf.Messaging.QueueGroup
}

func (adapter *QueryAdapter) generateClientID(conf types.Configuration) string {
	var clientID = conf.PodName
	if clientID == "" {
		clientID = fmt.Sprintf("provider-openstack-%s", xid.New().String())
	}
	return clientID
}

// natsConnect establish the NATS connection
func (adapter *QueryAdapter) natsConnect(conf types.Configuration) error {
	nc, err := nats.Connect(conf.Messaging.URL, nats.Name(conf.Messaging.ClientID))
	if err != nil {
		return err
	}
	log.Info("NATS connection established")

	adapter.nc = nc

	return nil
}

// InitChannel takes in the data channel for depositing incoming message
func (adapter *QueryAdapter) InitChannel(c chan types.CloudEventRequest) {
	adapter.queryChan = c
}

// Start starts the queue subscriber
func (adapter *QueryAdapter) Start(ctx context.Context) error {
	if adapter.nc == nil {
		log.Panic("nats connection is nil")
	}
	if adapter.queryChan == nil {
		log.Panic("query channel is nil, it is not initialized")
	}
	err := adapter.natsQueueSubscribe()
	if err != nil {
		log.WithError(err).Error("fail to start NATS queue subscription")
	}
	go func() {
		<-ctx.Done()
		err := adapter.sub.Drain()
		if err != nil {
			log.WithError(err).Error("fail to drain nats subscription")
			return
		}
		adapter.closeChannel.Do(func() {
			close(adapter.queryChan)
		})
	}()
	return err
}

// natsQueueSubscribe starts the subscriber
func (adapter *QueryAdapter) natsQueueSubscribe() error {
	// subscribe with adapter.natsCallbck()
	sub, err := adapter.nc.QueueSubscribe(adapter.subject, adapter.queue, adapter.natsCallback)
	if err != nil {
		return err
	}
	adapter.sub = sub
	return nil
}

func (adapter *QueryAdapter) natsCallback(m *nats.Msg) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "QueryAdapter.natsCallback",
	})
	ce, err := messaging.ConvertNats(m)
	if err != nil {
		logger.WithError(err).Error("unable to get request from CloudEvent")
		return
	}

	log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")
	var request = types.CloudEventRequest{
		CloudEvent: ce,
		Replyer: QueryReplyer{
			replySubject: m.Reply,
			adapter:      adapter,
		},
	}

	// deposit query into channel
	adapter.queryChan <- request
}

// Close ...
func (adapter *QueryAdapter) Close() error {
	err := adapter.sub.Drain() // use Drain() to allow ongoing callbacks to continue
	adapter.nc.Close()
	adapter.closeChannel.Do(func() {
		// close the channel to signal handlers in domain to stop
		close(adapter.queryChan)
	})
	return err
}

// QueryReplyer implements types.QueryReply and allows the domain to send
// a response.
type QueryReplyer struct {
	replySubject string
	adapter      *QueryAdapter
}

// Reply send a response
func (r QueryReplyer) Reply(ce cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "QueryReplyer.Reply",
		"eventType": ce.Type(),
		"tid":       messaging.GetTransactionID(&ce),
	})
	marshal, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Error("fail to marshal reply cloudevent into JSON")
		return err
	}
	if err = r.adapter.nc.Publish(r.replySubject, marshal); err != nil {
		logger.WithError(err).Error("fail to publish reply")
		return err
	}
	logger.WithField("dataLength", len(ce.Data())).Info("query replied")
	logger.WithField("data", string(ce.Data())).Trace("reply")
	return nil
}
