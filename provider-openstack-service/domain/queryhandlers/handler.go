package queryhandlers

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/triggeredactions"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"net/url"
	"sort"
	"strings"
	"sync"
)

// Handler is query handler
type Handler interface {
	Handle(ctx context.Context, request providers.ProviderRequest) Reply
}

// Reply is an interface implemented by all reply structs in cacao-common (e.g. providers.ImageListReply)
type Reply interface {
	ToCloudEvent(source string, providerPrefix common.QueryOp) (cloudevents.Event, error)
}

// CredentialListHandler handles getting a list of ApplicationCredentials
type CredentialListHandler struct {
	OpenStack                 ports.OpenStack
	CredMS                    ports.CredentialMS
	ProviderMetadataMS        ports.ProviderMetadataMS
	CredentialMetadataStorage ports.CredentialMetadataStorage
}

// Handle ...
func (h CredentialListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	logger := log.WithFields(log.Fields{
		"package":  "queryhandlers",
		"function": "CredentialListHandler.Handle",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"provider": request.Provider,
	})
	metadata, err := h.ProviderMetadataMS.GetMetadata(ctx, request.SessionActor, request.SessionEmulator, request.Provider)
	if err != nil {
		return h.errorReply(request, err)
	}
	var openstackMetadata struct {
		AuthURL           string `mapstructure:"OS_AUTH_URL"`
		RegionName        string `mapstructure:"OS_REGION_NAME"`
		ProjectDomainName string `mapstructure:"OS_PROJECT_DOMAIN_NAME"`
		ProjectDomainID   string `mapstructure:"OS_PROJECT_DOMAIN_ID"`
	}
	err = mapstructure.Decode(metadata, &openstackMetadata)
	if err != nil {
		return h.errorReply(request, err)
	}
	metadataAuthURL, err := url.Parse(openstackMetadata.AuthURL)
	if err != nil {
		return h.errorReply(request, err)
	}

	var requestArgs providers.OpenStackCredentialListArgs
	err = mapstructure.Decode(request.Args, &requestArgs)
	if err != nil {
		return h.errorReply(request, err)
	}

	// list all openstack cred
	credList1, err := h.CredMS.ListCredentials(ctx, request.SessionActor, request.SessionEmulator)
	if err != nil {
		return h.errorReply(request, err)
	}

	credentialMetadataList, err := h.CredentialMetadataStorage.ListCredentials(request.SessionActor, requestArgs.ProjectID, requestArgs.ProjectName)
	if err != nil {
		return h.errorReply(request, err)
	}

	// cross-check the 2 lists, if a credential does not have its metadata stored, then we fetch it on the fly
	lookupTable := make(map[string]struct{})
	for _, m := range credentialMetadataList {
		lookupTable[m.ID] = struct{}{}
	}

	credIDsNeedsFetch := make(map[string]types.CredentialMetadata)
	for _, cred := range credList1 {
		_, hasMetadata := lookupTable[cred.ID]
		if !hasMetadata {
			credIDsNeedsFetch[cred.ID] = types.CredentialMetadata{}
		}
	}
	var fetchWG sync.WaitGroup
	for credID := range credIDsNeedsFetch {
		fetchWG.Add(1)
		credentialID := credID
		go func() {
			defer fetchWG.Done()
			credential, err := h.CredMS.GetCredential(ctx, request.SessionActor, request.SessionEmulator, credentialID)
			if err != nil {
				logger.WithError(err).WithField("credID", credentialID).Error("failed to fetch credential")
				return
			}
			err = triggeredactions.InspectCredential(nil, h.OpenStack, credential, h.CredentialMetadataStorage, request.Session, credentialID)
			if err != nil {
				logger.WithError(err).WithField("credID", credentialID).Error("failed to inspect credential")
				return
			}
			credMetadata, err := h.CredentialMetadataStorage.GetCredential(request.SessionActor, credentialID)
			if err != nil {
				logger.WithError(err).WithField("credID", credentialID).Error("failed to get credential metadata")
				return
			}
			credIDsNeedsFetch[credentialID] = credMetadata
		}()
	}
	fetchWG.Wait()

	suitableCredList := make([]service.CredentialModel, 0)
	for _, cred := range credList1 {
		_, hasMetadata := lookupTable[cred.ID]
		if !hasMetadata {
			continue
		}
		osCred, err := types.ConvertCredential(cred)
		if err != nil {
			continue
		}
		if _, ok := osCred.OpenStackEnv["OS_AUTH_URL"]; !ok {
			continue
		}
		credAuthURL, err := url.Parse(osCred.OpenStackEnv["OS_AUTH_URL"])
		if err != nil {
			continue
		}
		if !types.AuthURLComparison(metadataAuthURL, credAuthURL) {
			continue
		}
		// explicitly set the Session and Value field
		cred.Session = service.Session{}
		cred.Value = "REDACTED"
		suitableCredList = append(suitableCredList, cred)
	}
	return providers.CredentialListReply{
		BaseProviderReply: requestToBaseReply(request),
		Credentials:       suitableCredList,
	}
}

func (h CredentialListHandler) errorReply(request providers.ProviderRequest, err error) providers.CredentialListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "CredentialListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.CredentialListReply{
		BaseProviderReply: errorReply(request, err),
		Credentials:       nil,
	}
}

// ApplicationCredentialListHandler handles getting a list of ApplicationCredentials
type ApplicationCredentialListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle handles the app cred list operation. Since app cred exists independent of regions, the args is ignored
func (h ApplicationCredentialListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	applicationCredentialList, err := h.OpenStack.ListApplicationCredentials(ctx, *cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.ApplicationCredentialListReply{
		BaseProviderReply:      requestToBaseReply(request),
		ApplicationCredentials: applicationCredentialList,
	}
}

func (h ApplicationCredentialListHandler) errorReply(request providers.ProviderRequest, err error) providers.ApplicationCredentialListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ApplicationCredentialListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ApplicationCredentialListReply{
		BaseProviderReply:      errorReply(request, err),
		ApplicationCredentials: nil,
	}
}

// ApplicationCredentialGetHandler handles getting a single ApplicationCredential
type ApplicationCredentialGetHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h ApplicationCredentialGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	applicationCredentialID, ok := request.Args.(string)
	if !ok {
		return h.errorReply(request, service.NewCacaoMarshalError("flavor ID is not string"))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	ApplicationCredential, err := h.OpenStack.GetApplicationCredential(ctx, *cred, applicationCredentialID)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetApplicationCredentialReply{
		BaseProviderReply:     requestToBaseReply(request),
		ApplicationCredential: ApplicationCredential,
	}
}

func (h ApplicationCredentialGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetApplicationCredentialReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ApplicationCredentialGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetApplicationCredentialReply{
		BaseProviderReply:     errorReply(request, err),
		ApplicationCredential: nil,
	}
}

// AuthenticationTestHandler handles getting a list of projects
type AuthenticationTestHandler struct {
	OpenStack ports.OpenStack
}

// Handle ...
func (h AuthenticationTestHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var variables = make(map[string]string)
	err := mapstructure.Decode(request.Args, &variables)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}

	env := h.sanitizeEnvironmentVariables(variables)
	if err := h.OpenStack.AuthenticationTest(ctx, env); err != nil {
		return h.errorReply(request, err)
	}

	return providers.AuthenticationTestReply{
		BaseProviderReply: requestToBaseReply(request),
		Result:            "authentication valid",
	}
}

func (h AuthenticationTestHandler) sanitizeEnvironmentVariables(env types.Environment) types.Environment {
	for k, v := range env {
		if !strings.HasPrefix(k, "OS_") {
			delete(env, k)
		}
		if len(k) > 50 || len(v) > 200 {
			delete(env, k)
		}
	}
	return env
}

func (h AuthenticationTestHandler) errorReply(request providers.ProviderRequest, err error) providers.AuthenticationTestReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "AuthenticationTestReply.errorReply",
	}).WithError(err).Error("reply error")
	return providers.AuthenticationTestReply{
		BaseProviderReply: errorReply(request, err),
		Result:            "",
	}
}

// RegionListHandler handles getting a list of regions
type RegionListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h RegionListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.RegionListReply
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	regionList, err := h.OpenStack.ListRegions(ctx, *cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.RegionListReply{
		BaseProviderReply: requestToBaseReply(request),
		Regions:           regionList,
	}
}

func (h RegionListHandler) errorReply(request providers.ProviderRequest, err error) providers.RegionListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "RegionListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.RegionListReply{
		BaseProviderReply: errorReply(request, err),
		Regions:           nil,
	}
}

// ImageListHandler handles getting a list of images
type ImageListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h ImageListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.ImageListingArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	err = h.validateArgs(args)
	if err != nil {
		return h.errorReply(request, err)
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	imageList, err := h.OpenStack.ListImages(ctx, *cred, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}
	sort.Sort(imageListSort(imageList))

	return providers.ImageListReply[providers.OpenStackImage]{
		BaseProviderReply: requestToBaseReply(request),
		Images:            imageList,
	}
}

// implements sort.Interface for sorting image list
type imageListSort []providers.OpenStackImage

// Len implements sort.Interface
func (list imageListSort) Len() int {
	return len(list)
}

// Less implements sort.Interface
func (list imageListSort) Less(i, j int) bool {
	// public image first
	if list[i].Visibility == "public" && list[j].Visibility != "public" {
		return true
	}
	if list[i].Visibility != "public" && list[j].Visibility == "public" {
		return false
	}

	// featured public image first
	const featuredPrefix = "Featured-" // this is prefix used by JS2, other openstack cloud might use other conventions
	iFeatured := strings.HasPrefix(list[i].Name, featuredPrefix) && list[i].Visibility == "public"
	jFeatured := strings.HasPrefix(list[j].Name, featuredPrefix) && list[j].Visibility == "public"
	if iFeatured && !jFeatured {
		return true
	}
	if !iFeatured && jFeatured {
		return false
	}

	return strings.Compare(list[i].Name, list[j].Name) < 0
}

// Swap implements sort.Interface
func (list imageListSort) Swap(i, j int) {
	t := list[j]
	list[j] = list[i]
	list[i] = t
}

func (h ImageListHandler) validateArgs(args providers.ImageListingArgs) error {
	if args.Region == "" {
		return service.NewCacaoInvalidParameterError("region cannot be empty")
	}
	return nil
}

func (h ImageListHandler) errorReply(request providers.ProviderRequest, err error) providers.ImageListReply[providers.OpenStackImage] {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ImageListReply[providers.OpenStackImage]{
		BaseProviderReply: errorReply(request, err),
		Images:            nil,
	}
}

// ImageGetHandler handles getting a single image
type ImageGetHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h ImageGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.ImageGetInArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	err = h.validateArgs(args)
	if err != nil {
		return h.errorReply(request, err)
	}

	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	image, err := h.OpenStack.GetImage(ctx, *cred, args.ID, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetImageReply[providers.OpenStackImage]{
		BaseProviderReply: requestToBaseReply(request),
		Image:             image,
	}
}

func (h ImageGetHandler) validateArgs(args providers.ImageGetInArgs) error {
	if args.ID == "" {
		return service.NewCacaoInvalidParameterError("image id cannot be empty")
	}
	if args.Region == "" {
		return service.NewCacaoInvalidParameterError("region cannot be empty")
	}
	return nil
}

func (h ImageGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetImageReply[providers.OpenStackImage] {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetImageReply[providers.OpenStackImage]{
		BaseProviderReply: errorReply(request, err),
		Image:             nil,
	}
}

// FlavorListHandler handles getting a list of flavors
type FlavorListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h FlavorListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.FlavorListingArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	err = h.validateArgs(args)
	if err != nil {
		return h.errorReply(request, err)
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	flavorList, err := h.OpenStack.ListFlavors(ctx, *cred, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.FlavorListReply{
		BaseProviderReply: requestToBaseReply(request),
		Flavors:           flavorList,
	}
}

func (h FlavorListHandler) validateArgs(args providers.FlavorListingArgs) error {
	if args.Region == "" {
		return service.NewCacaoInvalidParameterError("region cannot be empty")
	}
	return nil
}

func (h FlavorListHandler) errorReply(request providers.ProviderRequest, err error) providers.FlavorListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "FlavorListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.FlavorListReply{
		BaseProviderReply: errorReply(request, err),
		Flavors:           nil,
	}
}

// FlavorGetHandler handles getting a single flavor
type FlavorGetHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h FlavorGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.FlavorGetInArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	err = h.validateArgs(args)
	if err != nil {
		return h.errorReply(request, err)
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	flavor, err := h.OpenStack.GetFlavor(ctx, *cred, args.ID, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetFlavorReply{
		BaseProviderReply: requestToBaseReply(request),
		Flavor:            flavor,
	}
}

func (h FlavorGetHandler) validateArgs(args providers.FlavorGetInArgs) error {
	if args.ID == "" {
		return service.NewCacaoInvalidParameterError("flavor id cannot be empty")
	}
	if args.Region == "" {
		return service.NewCacaoInvalidParameterError("region cannot be empty")
	}
	return nil
}

func (h FlavorGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetFlavorReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "FlavorGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetFlavorReply{
		BaseProviderReply: errorReply(request, err),
		Flavor:            nil,
	}
}

// ProjectListHandler handles getting a list of projects
type ProjectListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h ProjectListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	projectList, err := h.OpenStack.ListProjects(ctx, *cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.ProjectListReply{
		BaseProviderReply: requestToBaseReply(request),
		Projects:          projectList,
	}
}

func (h ProjectListHandler) errorReply(request providers.ProviderRequest, err error) providers.ProjectListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ProjectListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ProjectListReply{
		BaseProviderReply: errorReply(request, err),
		Projects:          nil,
	}
}

// ProjectGetHandler handles getting a single Project
type ProjectGetHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h ProjectGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	projectID, ok := request.Args.(string)
	if !ok {
		return h.errorReply(request, service.NewCacaoMarshalError("Project ID is not string"))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	Project, err := h.OpenStack.GetProject(ctx, *cred, projectID)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetProjectReply{
		BaseProviderReply: requestToBaseReply(request),
		Project:           Project,
	}
}

func (h ProjectGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetProjectReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ProjectGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetProjectReply{
		BaseProviderReply: errorReply(request, err),
		Project:           nil,
	}
}

// CatalogListHandler handles listing Catalog
type CatalogListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h CatalogListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	catalog, err := h.OpenStack.ListCatalog(ctx, *cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.CatalogListReply{
		BaseProviderReply: requestToBaseReply(request),
		Catalog:           catalog,
	}
}

func (h CatalogListHandler) errorReply(request providers.ProviderRequest, err error) providers.CatalogListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "CatalogListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.CatalogListReply{
		BaseProviderReply: errorReply(request, err),
		Catalog:           nil,
	}
}

// ZoneListHandler handles listing Zone
type ZoneListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h ZoneListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	zoneList, err := h.OpenStack.ListZone(ctx, *cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.DNSZoneListReply{
		BaseProviderReply: requestToBaseReply(request),
		Zone:              zoneList,
	}
}

func (h ZoneListHandler) errorReply(request providers.ProviderRequest, err error) providers.DNSZoneListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ZoneListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.DNSZoneListReply{
		BaseProviderReply: errorReply(request, err),
		Zone:              nil,
	}
}

// RecordsetListHandler handles listing DNS recordset
type RecordsetListHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h RecordsetListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.DNSRecordsetListArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}

	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	recordsetList, err := h.OpenStack.ListRecordset(ctx, *cred, args.Zone)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.DNSRecordsetListReply{
		BaseProviderReply: requestToBaseReply(request),
		Recordset:         recordsetList,
	}
}

func (h RecordsetListHandler) errorReply(request providers.ProviderRequest, err error) providers.DNSRecordsetListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "RecordsetListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.DNSRecordsetListReply{
		BaseProviderReply: errorReply(request, err),
		Recordset:         nil,
	}
}

// TokenGetHandler handles issuing a Token
type TokenGetHandler struct {
	OpenStack ports.OpenStack
	CredFac   credsrc.CredentialFactory
}

// Handle ...
func (h TokenGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	token, err := h.OpenStack.GetToken(ctx, *cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetTokenReply{
		BaseProviderReply: requestToBaseReply(request),
		Token:             *token,
	}
}

func (h TokenGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetTokenReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "TokenGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetTokenReply{
		BaseProviderReply: errorReply(request, err),
		Token:             providers.Token{},
	}
}

// // CachePopulateHandler ...
// type CachePopulateHandler struct {
// 	OpenStack    ports.OpenStack
// 	CredSelector credentialSelector
// }

// // Handle ...
// func (h CachePopulateHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
// 	logger := log.WithFields(log.Fields{
// 		"package":  "domain",
// 		"function": "CachePopulateHandler.Handle",
// 	})
// 	err := h.OpenStack.PopulateCache(h.CredSelector.credMS, ctx, request)
// 	if err != nil {
// 		return h.errorReply(request, err)
// 	}

// 	logger.Info("success")
// 	return providers.CachePopulateReply{
// 		Session:   request.Session,
// 		Operation: request.Operation,
// 		Provider:  request.Provider,
// 	}
// }

// func (h CachePopulateHandler) errorReply(request providers.ProviderRequest, err error) providers.CachePopulateReply {
// 	log.WithFields(log.Fields{
// 		"package":  "domain",
// 		"function": "CachePopulateHandler.errorReply",
// 	}).WithError(err).Error("reply error")
// 	return providers.CachePopulateReply(errorReply(request, err))
// }
