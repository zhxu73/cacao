package credsrc

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// credentialSelector will select which credential to use for the operation.
// Note: it may not select any credential to use, and will return an error as result.
//
// Strategy:
// 1. select the credential specified in the request.
// 2. select credential that is taged with provider (tag name == provider ID) after searching in all credentials that users have.
// If there is multiple, use the most recent one.
// 3. select credential that has the same name as provider ID.
// 4. no credential is selected, error.
type credentialSelector struct {
	credMS ports.CredentialMS
}

// SelectCredential selects a credential to use for the request.
// If no credential is selected, then an error will be returned.
// On success, the credential ID will be returned.
func (cs credentialSelector) SelectCredential(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "credentialSelector.SelectCredential",
		"provider": request.Provider,
		"actor":    request.GetSessionActor(),
		"emulator": request.GetSessionEmulator(),
	})
	if request.Credential.Type != providers.ProviderCredentialID {
		return nil, fmt.Errorf("wrong credential type passed in, requires a credential ID")
	}
	credID, ok := request.Credential.Data.(string)
	if !ok {
		err := errors.New("credential ID is not string")
		logger.WithField("credType", request.Credential.Type).WithError(err).Error()
		return nil, err
	}
	if credID != "" {
		cred, err := cs.fetchCredentialInRequest(ctx, request.Session, credID)
		if err != nil {
			logger.WithError(err).WithField("credID", request.Credential).Error(
				"fail to fetch credential specified in the request",
			)
			return nil, err
		}
		logger.WithField("credID", cred.ID).Info("use credential specified in the request")
		return cred, nil
	}

	logger.Debug("request does not specify a credential to use, start searching")
	cred, err := cs.selectByTag(ctx, request)
	if err == nil {
		logger.WithField("credID", cred.ID).Info("found credential with matched tags")
		return cred, nil
	}
	logger.WithError(err).Error("no credential with matched tag found, continue searching")

	logger.Debug("start searching for matching name")
	cred, err = cs.selectByName(ctx, request)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential with same name as provider ID")
		return nil, err
	}
	logger.WithField("credID", cred.ID).Info("found credential with matched name")
	return cred, nil
}

func (cs credentialSelector) fetchCredentialInRequest(ctx context.Context, session service.Session, credID string) (*types.Credential, error) {
	return cs.credMS.GetCredential(ctx, session.GetSessionActor(), session.GetSessionEmulator(), credID)
}

// search by tags, check if credential has tag that is the same as provider ID. If there is multiple ones, select the most recent one
func (cs credentialSelector) selectByTag(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error) {
	tagMatched, err := cs.credMS.ListCredentialsByTag(ctx, request.GetSessionActor(), request.GetSessionEmulator(), request.Provider.String())
	if err != nil {
		return nil, err
	}
	if len(tagMatched) == 0 {
		return nil, errors.New("no credential matched the tag")
	} else if len(tagMatched) == 1 {
		return &tagMatched[0], nil
	}
	// when there is more than 1, select the most recent one
	return cs.findMostRecent(tagMatched)
}

func (cs credentialSelector) findMostRecent(list []types.Credential) (*types.Credential, error) {
	var i int
	mostRecentIndex := 0
	mostRecentTimestamp := list[0].CreatedAt
	for i = range list {
		if !list[i].UpdatedAt.IsZero() {
			if list[i].UpdatedAt.After(mostRecentTimestamp) {
				mostRecentIndex = i
				mostRecentTimestamp = list[i].UpdatedAt
			}
		} else if list[i].CreatedAt.After(mostRecentTimestamp) {
			mostRecentIndex = i
			mostRecentTimestamp = list[i].CreatedAt
		}
	}
	return &list[mostRecentIndex], nil
}

func (cs credentialSelector) selectByName(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error) {
	// search cred with the same name as provider ID
	credential, err := cs.credMS.GetCredential(ctx, request.GetSessionActor(), request.GetSessionEmulator(), request.Provider.String())
	if err != nil {
		return nil, err
	}
	return credential, nil
}
