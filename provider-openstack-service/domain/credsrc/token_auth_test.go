package credsrc

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portsmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
	"time"
)

func TestTokenAuth_CreateCredential(t *testing.T) {
	type fields struct {
		providerSvc func() *portsmocks.ProviderMetadataMS
	}
	type args struct {
		ctx     context.Context
		request providers.ProviderRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.Credential
		wantErr bool
	}{
		{
			name: "unscoped-token",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{}, // unscoped
						},
					},
					Args: nil,
				},
			},
			want: &types.Credential{
				Username:  "actor123",
				ID:        "",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
				OpenStackEnv: map[string]string{
					"OS_AUTH_URL":             "https://cyverse.org",
					"OS_IDENTITY_API_VERSION": "3",
					"OS_REGION_NAME":          "region-123",
					"OS_INTERFACE":            "interface-123",

					"OS_AUTH_TYPE": "v3token",
					"OS_TOKEN":     "token12345",
				},
			},
			wantErr: false,
		},
		{
			name: "scoped-token project-name domain-name",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{
								Project: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									Name: "my-os-project-name-123",
								},
								Domain: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									Name: "my-os-domain-name-123",
								},
							}, // scoped
						},
					},
					Args: nil,
				},
			},
			want: &types.Credential{
				Username:  "actor123",
				ID:        "",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
				OpenStackEnv: map[string]string{
					"OS_AUTH_URL":             "https://cyverse.org",
					"OS_IDENTITY_API_VERSION": "3",
					"OS_REGION_NAME":          "region-123",
					"OS_INTERFACE":            "interface-123",

					"OS_AUTH_TYPE":           "v3token",
					"OS_TOKEN":               "token12345",
					"OS_PROJECT_NAME":        "my-os-project-name-123",
					"OS_PROJECT_DOMAIN_NAME": "my-os-domain-name-123",
				},
			},
			wantErr: false,
		},
		{
			name: "scoped-token project-id domain-name",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{
								Project: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									ID: "my-os-project-id-123",
								},
								Domain: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									Name: "my-os-domain-name-123",
								},
							}, // scoped
						},
					},
					Args: nil,
				},
			},
			want: &types.Credential{
				Username:  "actor123",
				ID:        "",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
				OpenStackEnv: map[string]string{
					"OS_AUTH_URL":             "https://cyverse.org",
					"OS_IDENTITY_API_VERSION": "3",
					"OS_REGION_NAME":          "region-123",
					"OS_INTERFACE":            "interface-123",

					"OS_AUTH_TYPE":           "v3token",
					"OS_TOKEN":               "token12345",
					"OS_PROJECT_ID":          "my-os-project-id-123",
					"OS_PROJECT_DOMAIN_NAME": "my-os-domain-name-123",
				},
			},
			wantErr: false,
		},
		{
			name: "scoped-token project-name domain-id",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{
								Project: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									Name: "my-os-project-name-123",
								},
								Domain: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									ID: "my-os-domain-id-123",
								},
							}, // scoped
						},
					},
					Args: nil,
				},
			},
			want: &types.Credential{
				Username:  "actor123",
				ID:        "",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
				OpenStackEnv: map[string]string{
					"OS_AUTH_URL":             "https://cyverse.org",
					"OS_IDENTITY_API_VERSION": "3",
					"OS_REGION_NAME":          "region-123",
					"OS_INTERFACE":            "interface-123",

					"OS_AUTH_TYPE":         "v3token",
					"OS_TOKEN":             "token12345",
					"OS_PROJECT_NAME":      "my-os-project-name-123",
					"OS_PROJECT_DOMAIN_ID": "my-os-domain-id-123",
				},
			},
			wantErr: false,
		},
		{
			name: "scoped-token project-id domain-id",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{
								Project: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									ID: "my-os-project-id-123",
								},
								Domain: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									ID: "my-os-domain-id-123",
								},
							}, // scoped
						},
					},
					Args: nil,
				},
			},
			want: &types.Credential{
				Username:  "actor123",
				ID:        "",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
				OpenStackEnv: map[string]string{
					"OS_AUTH_URL":             "https://cyverse.org",
					"OS_IDENTITY_API_VERSION": "3",
					"OS_REGION_NAME":          "region-123",
					"OS_INTERFACE":            "interface-123",

					"OS_AUTH_TYPE":         "v3token",
					"OS_TOKEN":             "token12345",
					"OS_PROJECT_ID":        "my-os-project-id-123",
					"OS_PROJECT_DOMAIN_ID": "my-os-domain-id-123",
				},
			},
			wantErr: false,
		},
		{
			name: "scoped-token bad-scope no-domain",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{
								Project: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									ID: "my-os-project-id-123",
								},
								Domain: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{}, // no domain, bad scope
							},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "scoped-token bad-scope no-project",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{
								Project: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{}, // no project, bad scope
								Domain: struct {
									Name string `json:"name,omitempty"`
									ID   string `json:"id,omitempty"`
								}{
									ID: "my-os-domain-id-123",
								},
							},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty token",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "", // empty token
							Scope: providers.ProjectScope{},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "metadata missing field",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							//"OS_AUTH_URL":             "https://cyverse.org", // missing field
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "metadata has empty field",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "", // empty field
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "metadata field has bad value type",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             123, // bad type
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
						}, nil)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty metadata",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{}, // empty metadata
						nil,
					)
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail to get metadata",
			fields: fields{
				providerSvc: func() *portsmocks.ProviderMetadataMS {
					svc := &portsmocks.ProviderMetadataMS{}
					svc.On("GetMetadata", anyContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(nil, fmt.Errorf("failed"))
					return svc
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token12345",
							Scope: providers.ProjectScope{},
						},
					},
					Args: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			providerSvc := tt.fields.providerSvc()
			ta := tokenAuth{
				providerSvc: providerSvc,
			}
			got, err := ta.CreateCredential(tt.args.ctx, tt.args.request)
			if tt.wantErr {
				assert.Errorf(t, err, "CreateCredential(%v, %v)", tt.args.ctx, tt.args.request)
			} else {
				assert.NoErrorf(t, err, "CreateCredential(%v, %v)", tt.args.ctx, tt.args.request)
			}
			assert.Equalf(t, tt.want, got, "CreateCredential(%v, %v)", tt.args.ctx, tt.args.request)
			providerSvc.AssertExpectations(t)
		})
	}
}

// match any non-nil context
func anyContext() interface{} {
	return mock.MatchedBy(func(ctx context.Context) bool { return ctx != nil })
}
