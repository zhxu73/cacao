package triggeredactions

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// TagCredential tags a credential with relevant information, including project name, project ID, and provider IDs that match the credential's OS_AUTH_URL.
func TagCredential(openstack ports.OpenStack, credential *types.Credential, credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS, session service.Session, credID string) {
	logger := log.WithFields(log.Fields{
		"package":  "triggeredactions",
		"function": "TagCredential",
		"actor":    session.SessionActor,
		"emulator": session.SessionEmulator,
		"credID":   credID,
	})
	ctx, cancel := context.WithTimeout(context.TODO(), time.Minute)
	defer cancel()

	_ = tagCredential(ctx, logger, openstack, credential, credMS, providerMS, session, credID)
}

func tagCredential(ctx context.Context, logger *log.Entry, openstack ports.OpenStack, credential *types.Credential, credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS, session service.Session, credID string) error {
	providerList, err := providerMS.ListOpenStackProvider(ctx, credential.Username, session.GetSessionEmulator())
	if err != nil {
		logger.WithError(err).Error("fail to list openstack providers")
		return err
	}
	if len(providerList) == 0 {
		logger.Info("no openstack providers accessible by user, skip creating workspaces")
		return nil
	}
	var primaryProviderID common.ID
	matchedProviderIDs := make([]common.ID, 0)
	for i := range providerList {
		if providerList[i].URL == credential.OpenStackEnv["OS_AUTH_URL"] {
			matchedProviderIDs = append(matchedProviderIDs, providerList[i].ID)
			if providerList[i].Public && primaryProviderID == "" {
				// use the first public provider that matches OS_AUTH_URL as the primary provider ID
				primaryProviderID = providerList[i].ID
			}
		}
	}
	token, err := openstack.GetToken(ctx, *credential)
	if err != nil {
		logger.WithError(err).Error("fai to inspect credential by getting a token")
		return err
	}
	osProject, err := openstack.GetProject(ctx, *credential, token.ProjectID)
	if err != nil {
		logger.WithError(err).Error("fai to get openstack project that credential belong to")
		return err
	}
	var tags map[string]string
	tags = map[string]string{
		"cacao_openstack_project_name": osProject.Name,
		"cacao_openstack_project_id":   token.ProjectID,
		"cacao_provider":               primaryProviderID.String(), // use cacao reserved tagging for primary provider ID
		"os_auth_type":                 "v3applicationcredential",
	}
	for _, id := range matchedProviderIDs {
		tags[id.String()] = "" // tag with provider ID as tag name
	}
	err = credMS.TagCredential(ctx, session.SessionActor, session.SessionEmulator, credID, tags)
	if err != nil {
		logger.WithError(err).Error("fail to tag credential")
		return err
	}
	return nil
}
