package eventhandlers

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/triggeredactions"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"net/http"
	"sync"
	"time"
)

// UserLoginHandler handles the user login event
type UserLoginHandler struct {
	openStack       ports.OpenStack
	credMS          ports.CredentialMS
	metadataStorage ports.CredentialMetadataStorage
	k8sNamespace    string // the namespace in which api-service is deployed, likely the same namespace as this service
}

// NewUserLoginHandler ...
func NewUserLoginHandler(k8sNamespace string, OpenStack ports.OpenStack, CredMS ports.CredentialMS, metadataStorage ports.CredentialMetadataStorage) *UserLoginHandler {
	return &UserLoginHandler{
		openStack:       OpenStack,
		credMS:          CredMS,
		metadataStorage: metadataStorage,
		k8sNamespace:    k8sNamespace,
	}
}

// Handle handles the user login event and populate the cache for user.
func (h UserLoginHandler) Handle(incoming types.UserLoginEvent, sink ports.OutgoingEvents) {
	log.Info("received UserLogin event")
	username, err := h.getUsernameUsingToken(incoming.Token)
	if err != nil {
		log.WithError(err).Error("fail to get profile using token in event")
		return
	}
	logger := log.WithField("username", username)
	logger.Info("resolved token to username")

	var wg sync.WaitGroup

	wg.Add(1)
	go triggeredactions.InspectUserCredentials(&wg, h.openStack, h.credMS, h.metadataStorage, username, service.Session{SessionActor: username, SessionEmulator: ""})

	h.populateCache(username)
	logger.Info("cache populated")

	wg.Wait()
	sink.Nothing()
}

func (h UserLoginHandler) populateCache(username string) {
	credList, err := h.credMS.ListCredentialsByTag(context.TODO(), username, "", "cacao_provider")
	if err != nil {
		log.WithField("username", username).WithError(err).Error("failed to list credential for user to refresh cache")
		return
	}
	var jobQueue = make(chan types.Credential, len(credList))
	for i := range credList {
		jobQueue <- credList[i]
	}
	close(jobQueue)

	const maxWorkerCount = 5
	workerCount := len(credList)
	if len(credList) > maxWorkerCount {
		workerCount = maxWorkerCount
	}
	var wg sync.WaitGroup
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for cred := range jobQueue {
				h.refreshCacheForOneCredential(cred)
			}
		}()
	}
	wg.Wait()
}

func (h UserLoginHandler) refreshCacheForOneCredential(cred types.Credential) {
	logger := log.WithFields(log.Fields{
		"function": "UserLoginHandler.refreshCacheForOneCredential",
		"username": cred.Username,
		"credID":   cred.ID,
	})
	const timeout = time.Second * 5
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	regions, err := h.openStack.ListRegions(ctx, cred)
	cancel()
	if err != nil {
		logger.WithError(err).Error("failed to refresh ListRegions cache")
		return
	}
	ctx, cancel = context.WithTimeout(context.Background(), timeout)
	_, err = h.openStack.ListProjects(context.TODO(), cred)
	cancel()
	if err != nil {
		logger.WithError(err).Error("failed to refresh ListProjects cache")
		return
	}
	ctx, cancel = context.WithTimeout(context.Background(), timeout)
	_, err = h.openStack.ListApplicationCredentials(context.TODO(), cred)
	cancel()
	if err != nil {
		logger.WithError(err).Error("failed to refresh ListApplicationCredentials cache")
		return
	}
	ctx, cancel = context.WithTimeout(context.Background(), timeout)
	_, err = h.openStack.ListCatalog(context.TODO(), cred)
	cancel()
	if err != nil {
		logger.WithError(err).Error("failed to refresh ListCatalog cache")
		return
	}
	for _, region := range regions {
		ctx, cancel = context.WithTimeout(context.Background(), timeout)
		_, err = h.openStack.ListImages(context.TODO(), cred, region.Name)
		cancel()
		if err != nil {
			logger.WithError(err).Error("failed to refresh ListImages cache")
			return
		}
		ctx, cancel = context.WithTimeout(context.Background(), timeout)
		_, err = h.openStack.ListFlavors(context.TODO(), cred, region.Name)
		cancel()
		if err != nil {
			logger.WithError(err).Error("failed to refresh ListFlavors cache")
			return
		}
	}
}

type getUserAPIResponse struct {
	Username  string `json:"username"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

func (h UserLoginHandler) getUsernameUsingToken(token string) (string, error) {
	client := http.Client{
		Timeout: time.Second * 10,
	}
	var urlStr = fmt.Sprintf("http://api.%s.svc/users/mine", h.k8sNamespace) // use the in-cluster dns name to talk to api-service
	var wg sync.WaitGroup
	var respChan = make(chan getUserAPIResponse, 2)
	// make 2 request in parallel, 1 with "Bearer " in Authorization header, the other without

	wg.Add(1)
	go func() {
		defer wg.Done()
		request1, err := http.NewRequest("GET", urlStr, nil)
		if err != nil {
			return
		}
		request1.Header.Set("Authorization", token)
		resp1, err := client.Do(request1)
		if err != nil {
			return
		}
		defer resp1.Body.Close()
		if resp1.StatusCode != 200 {
			return
		}
		var jsonResp1 getUserAPIResponse
		err = json.NewDecoder(resp1.Body).Decode(&jsonResp1)
		if err != nil {
			return
		}
		if jsonResp1.Username == "" {
			log.Warn("api-service resolve to empty username")
		}
		respChan <- jsonResp1
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		request2, err := http.NewRequest("GET", urlStr, nil)
		if err != nil {
			return
		}
		request2.Header.Set("Authorization", "Bearer "+token)
		resp2, err := client.Do(request2)
		if err != nil {
			return
		}
		defer resp2.Body.Close()
		if resp2.StatusCode != 200 {
			return
		}
		var jsonResp2 getUserAPIResponse
		err = json.NewDecoder(resp2.Body).Decode(&jsonResp2)
		if err != nil {
			return
		}
		if jsonResp2.Username == "" {
			log.Warn("api-service resolve to empty username")
		}
		respChan <- jsonResp2
	}()

	select {
	case resp := <-respChan:
		if resp.Username == "" {
			return "", fmt.Errorf("fail to resolve username, api-service return empty username")
		}
		return resp.Username, nil
	case <-time.After(time.Second * 15):
		return "", fmt.Errorf("fetch user timed out")
	}
}
