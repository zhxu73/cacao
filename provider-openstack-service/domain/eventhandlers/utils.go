package eventhandlers

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

func checkRequest(req providers.ProviderRequest) error {
	if req.GetSessionActor() == "" {
		return service.NewCacaoInvalidParameterError("request has no actor")
	}
	if req.Provider == "" {
		return service.NewCacaoInvalidParameterError("request missing provider ID")
	}
	if !req.Provider.Validate() || req.Provider.PrimaryType() != "provider" {
		return service.NewCacaoInvalidParameterError("request has bad provider ID")
	}
	return nil
}
