package eventhandlers

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/triggeredactions"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
)

// CredentialUpdatedHandler handles the event CredentialUpdated.
type CredentialUpdatedHandler struct {
	providerMS ports.ProviderMetadataMS
	credMS     ports.CredentialMS
	openStack  ports.OpenStack
}

// NewCredentialUpdatedHandler ...
func NewCredentialUpdatedHandler(openStack ports.OpenStack, providerMS ports.ProviderMetadataMS, credMS ports.CredentialMS) CredentialUpdatedHandler {
	return CredentialUpdatedHandler{
		providerMS: providerMS,
		credMS:     credMS,
		openStack:  openStack,
	}
}

// Handle ...
func (h CredentialUpdatedHandler) Handle(incoming service.CredentialUpdateResponse, sink ports.OutgoingEvents) error {
	if incoming.Username == "" || incoming.ID == "" || incoming.Type != service.OpenStackCredentialType {
		return nil // skip
	}
	if !incoming.ValueUpdated {
		// NOTE: it is important that we only proceeds when ValueUpdated is true,
		// otherwise we will have an infinite loop between provider-openstack-service and
		// credential service.
		// CredentialUpdated -> UpdateTag -> CredentialUpdated -> UpdateTag -> ...
		return nil // skip
	}

	// get the full credential, including tags
	credential, err := h.credMS.GetCredential(context.TODO(), incoming.GetSessionActor(), incoming.GetSessionEmulator(), incoming.ID)
	if err != nil {
		log.WithError(err).Error("fail to fetch credential")
		return err
	}
	if credential == nil {
		return fmt.Errorf("credential is nil, but no error when fetching it")
	}

	triggeredactions.TagCredential(h.openStack, credential, h.credMS, h.providerMS, incoming.Session, incoming.ID)
	return nil
}
