package eventhandlers

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/caching"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"time"
)

// AppCredDeletionHandler is handler for deletion request for openstack application credential.
// This handler will use the credential ID specified in the request to authenticate with OpenStack and also delete the application credential specified in the credential.
// Namely, this uses an application credential (in the form of credential ID) to delete itself.
type AppCredDeletionHandler struct {
	credFac   credsrc.CredentialFactory
	openStack caching.OpenStackOperationCache
}

// NewAppCredDeletionHandler creates a new AppCredDeletionHandler
func NewAppCredDeletionHandler(
	openstack caching.OpenStackOperationCache,
	providerMS ports.ProviderMetadataMS,
	credMS ports.CredentialMS,
) AppCredDeletionHandler {
	return AppCredDeletionHandler{
		credFac:   credsrc.NewCredentialFactory(credMS, providerMS),
		openStack: openstack,
	}
}

// Handle ...
func (h AppCredDeletionHandler) Handle(request providers.ProviderRequest, sink ports.OutgoingEvents) {
	if err := checkRequest(request); err != nil {
		sink.EventApplicationCredentialsDeleteFailed(h.createFailureResponse(request, err))
		return
	}
	err := h.handle(context.TODO(), request)
	if err != nil {
		log.WithError(err).Errorf("fail to delete application credential")
		sink.EventApplicationCredentialsDeleteFailed(h.createFailureResponse(request, err))
	} else {
		sink.EventApplicationCredentialsDeleted(h.createSuccessResponse(request))
	}
}

// - fetch credential from cred svc
// - check if credential is openstack app cred
// - check if app cred exists
// - if app cred exists, deletes app cred
// - update cache
// Checking if the application credential is necessary, because we want this operation to be idempotent, so that this
// operation can be safely retried on the same application credential.
func (h AppCredDeletionHandler) handle(ctx context.Context, request providers.ProviderRequest) error {
	err := h.checkCredentialOption(request)
	if err != nil {
		return err
	}
	cred, err := h.credFac.GetCredential(ctx, request)
	if err != nil {
		return err
	}
	appCred, err := h.extractAppCredIdentifier(cred)
	if err != nil {
		return err
	}
	exists, err := h.checkIfAppCredExists(ctx, *cred)
	if err != nil {
		log.WithError(err).Error("fail to check if application credential exists")
		return err
	}
	if !exists {
		return nil
	}
	err = h.openStack.DeleteApplicationCredential(ctx, *cred, appCred)
	if err != nil {
		return err
	}
	h.updateCache(ctx, *cred)
	return nil
}

// check to ensure that the credential in the request is a non-empty credential ID
func (h AppCredDeletionHandler) checkCredentialOption(request providers.ProviderRequest) error {
	if request.Credential.Type != providers.ProviderCredentialID {
		return service.NewCacaoInvalidParameterError("this operation requires credential ID")
	}
	credID, ok := request.Credential.Data.(string)
	if !ok {
		return service.NewCacaoInvalidParameterError("credential ID must be string")
	}
	if credID == "" {
		return service.NewCacaoInvalidParameterErrorWithOptions("credential ID cannot be empty")
	}
	return nil
}

// extract ID or name of the application credential
func (h AppCredDeletionHandler) extractAppCredIdentifier(cred *types.Credential) (string, error) {
	authType, ok := cred.OpenStackEnv["OS_AUTH_TYPE"]
	if !ok {
		return "", providers.NewNotApplicationCredentialError(cred.ID, "bad application credential, missing OS_AUTH_TYPE")
	} else if authType != "v3applicationcredential" {
		return "", providers.NewNotApplicationCredentialError(cred.ID, "bad application credential, OS_AUTH_TYPE not v3applicationcredential")
	}

	appCredID, ok := cred.OpenStackEnv["OS_APPLICATION_CREDENTIAL_ID"]
	if ok {
		if len(appCredID) == 0 {
			return "", providers.NewNotApplicationCredentialError(cred.ID, "bad application credential, empty OS_APPLICATION_CREDENTIAL_ID")
		}
		return appCredID, nil
	}
	appCredName, ok := cred.OpenStackEnv["OS_APPLICATION_CREDENTIAL_NAME"]
	if !ok {
		return "", providers.NewNotApplicationCredentialError(cred.ID, "bad application credential, missing OS_APPLICATION_CREDENTIAL_ID or OS_APPLICATION_CREDENTIAL_NAME")
	} else if len(appCredID) == 0 {
		return "", providers.NewNotApplicationCredentialError(cred.ID, "bad application credential, empty OS_APPLICATION_CREDENTIAL_NAME")
	}
	return appCredName, nil
}

func (h AppCredDeletionHandler) checkIfAppCredExists(ctx context.Context, cred types.Credential) (bool, error) {
	check := func(ctx context.Context, cred types.Credential) (bool, error) {
		err := h.openStack.AuthenticationTest(ctx, cred.OpenStackEnv)
		if err == nil {
			return true, nil
		}
		if err == types.ErrApplicationCredentialNotFound {
			// if the app cred does not exist (deleted or never existed)
			return false, nil
		}
		if err == types.ErrAuthentication {
			// if application credential ID is provided, this error means that app cred ID exists, but app cred secret is incorrect.
			// but since the secret is incorrect, we cannot delete the app cred, so return error still.
			// if application credential name is provided (along with user), this error means either ID or secret or both is incorrect.
			return false, service.NewCacaoInvalidParameterError(err.Error())
		}
		return false, err
	}
	exists, err := check(ctx, cred)
	const retryCount = 2 // retry just in case the error is temporary.
	for i := 0; err != nil && i < retryCount; i++ {
		time.Sleep(time.Millisecond * 500 * time.Duration(i+1))
		exists, err = check(ctx, cred)
	}
	return exists, err
}

func (h AppCredDeletionHandler) updateCache(ctx context.Context, cred types.Credential) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-openstack-service.domain.eventhandlers",
		"function": "AppCredDeletionHandler.updateCache",
	})
	err := h.openStack.InvalidateCache(types.CachePrefixListApplicationCredentials, cred.Username)
	if err != nil {
		logger.WithError(err).Warn("fail to invalidate cache for app cred list after app cred deletion")
	}
	_, err = h.openStack.ListApplicationCredentials(ctx, cred)
	if err != nil {
		// log and ignore the error
		// TODO consider purge the cache entry when error (if cache key can be computed)
		logger.WithError(err).Errorf("fail to update cache")
		return
	}
	logger.Info("cache updated")
}

func (h AppCredDeletionHandler) createSuccessResponse(req providers.ProviderRequest) providers.DeleteApplicationCredentialResponse {
	credID, _ := req.Credential.Data.(string) // "" if not string
	return providers.DeleteApplicationCredentialResponse{
		BaseProviderReply: providers.BaseProviderReply{
			Session: service.Session{
				SessionActor:    req.GetSessionActor(),
				SessionEmulator: req.GetSessionEmulator(),
				ServiceError:    service.CacaoErrorBase{},
			},
			Operation: req.Operation,
			Provider:  req.Provider,
		},
		CredentialID: credID,
	}

}

func (h AppCredDeletionHandler) createFailureResponse(req providers.ProviderRequest, err error) providers.DeleteApplicationCredentialResponse {
	credID, _ := req.Credential.Data.(string) // "" if not string
	cacaoErr, ok := err.(service.CacaoError)
	if !ok {
		cacaoErr = service.NewCacaoGeneralError(err.Error())
	}
	return providers.DeleteApplicationCredentialResponse{
		BaseProviderReply: providers.BaseProviderReply{
			Session: service.Session{
				SessionActor:    req.GetSessionActor(),
				SessionEmulator: req.GetSessionEmulator(),
				ServiceError:    cacaoErr.GetBase(),
			},
			Operation: req.Operation,
			Provider:  req.Provider,
		},
		CredentialID: credID,
	}
}
