package eventhandlers

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	cachemocks "gitlab.com/cyverse/cacao/provider-openstack-service/domain/caching/mocks"
	credsrcmocks "gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc/mocks"
	portsmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
	"time"
)

var testDeleteAppCredRequest = providers.ProviderRequest{
	Session: service.Session{
		SessionActor:    "actor-123",
		SessionEmulator: "emulator-123",
	},
	Operation: "",
	Provider:  "provider-d17e30598850n9abikl0",
	Credential: providers.ProviderCredential{
		Type: providers.ProviderCredentialID,
		Data: "cred-id-123",
	},
	Args: nil,
}

var testDeleteAppCredEnv = map[string]string{
	"OS_AUTH_TYPE":                     "v3applicationcredential",
	"OS_AUTH_URL":                      "https://cyverse.org",
	"OS_IDENTITY_API_VERSION":          "3",
	"OS_REGION_NAME":                   "region-123",
	"OS_INTERFACE":                     "public",
	"OS_APPLICATION_CREDENTIAL_ID":     "app-cred-id-delete-123",
	"OS_APPLICATION_CREDENTIAL_SECRET": "app-cred-secret-123",
}

func TestAppCredDeletionHandler_Handle(t *testing.T) {
	// some objects used in multiple places in a single test case. this struct provides a place to define them in one place,
	// rather than copy and paste the same definition.
	type sharedObjs struct {
		request providers.ProviderRequest
	}
	type args struct {
		sink func(*testing.T, args) *portsmocks.OutgoingEvents
	}
	type fields struct {
		credFac   func(sharedObjs) *credsrcmocks.CredentialFactory
		openStack func(*testing.T) *cachemocks.OpenStackOperationCache
	}
	tests := []struct {
		name       string
		sharedObjs sharedObjs
		fields     fields
		args       args
	}{
		{
			name: "success",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						Username:     testDeleteAppCredRequest.SessionActor,
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					cred := types.Credential{
						Username:     testDeleteAppCredRequest.SessionActor,
						ID:           testDeleteAppCredRequest.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}
					openStack.On("AuthenticationTest", anyNotNilContext(), cred.OpenStackEnv).Return(nil)
					openStack.On("DeleteApplicationCredential", anyNotNilContext(), cred, testDeleteAppCredEnv["OS_APPLICATION_CREDENTIAL_ID"]).Return(nil)
					openStack.On("InvalidateCache", types.CachePrefixListApplicationCredentials, testDeleteAppCredRequest.SessionActor).Return(nil)
					openStack.On("ListApplicationCredentials", anyNotNilContext(), cred).Return([]providers.ApplicationCredential{}, nil)
					return openStack
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleted",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "success app cred ID not exists",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						Username:     testDeleteAppCredRequest.SessionActor,
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					cred := types.Credential{
						Username:     testDeleteAppCredRequest.SessionActor,
						ID:           testDeleteAppCredRequest.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}
					openStack.On("AuthenticationTest", anyNotNilContext(), cred.OpenStackEnv).Return(types.ErrApplicationCredentialNotFound)
					return openStack
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleted",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "fail to parse request - no actor",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator-123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-delete-123",
					},
					Args: nil,
				},
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					return &credsrcmocks.CredentialFactory{}
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					return &cachemocks.OpenStackOperationCache{}
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "",
									SessionEmulator: "emulator-123",
									ServiceError:    service.NewCacaoInvalidParameterError("request has no actor").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: "cred-id-delete-123",
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "bad credential type - token",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					Provider: "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token-123",
							Scope: providers.ProjectScope{},
						},
					},
					Args: "bad args",
				},
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					return &credsrcmocks.CredentialFactory{}
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					return &cachemocks.OpenStackOperationCache{}
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
									ServiceError:    service.NewCacaoInvalidParameterError("this operation requires credential ID").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: "",
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "fail to get credential",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(nil, fmt.Errorf("failed"))
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					return &cachemocks.OpenStackOperationCache{}
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
									ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "credential not app cred",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:        objs.request.Credential.Data.(string),
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						OpenStackEnv: map[string]string{
							"OS_AUTH_TYPE":            "v3token",
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "public",
							"OS_TOKEN":                "token-123",
						},
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					return &cachemocks.OpenStackOperationCache{}
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
									ServiceError:    providers.NewNotApplicationCredentialError(testDeleteAppCredRequest.Credential.Data.(string), "bad application credential, OS_AUTH_TYPE not v3applicationcredential").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "credential missing key",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:        objs.request.Credential.Data.(string),
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						OpenStackEnv: map[string]string{
							"OS_AUTH_TYPE":            "v3applicationcredential",
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "public",
							//"OS_APPLICATION_CREDENTIAL_ID":     "app-cred-id-delete-123", // missing OS_APPLICATION_CREDENTIAL_ID
							"OS_APPLICATION_CREDENTIAL_SECRET": "app-cred-secret-123",
						},
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					return &cachemocks.OpenStackOperationCache{}
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
									ServiceError:    providers.NewNotApplicationCredentialError(testDeleteAppCredRequest.Credential.Data.(string), "bad application credential, missing OS_APPLICATION_CREDENTIAL_ID or OS_APPLICATION_CREDENTIAL_NAME").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "auth failed - auth error",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						Username:     testDeleteAppCredRequest.SessionActor,
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					cred := types.Credential{
						Username:     testDeleteAppCredRequest.SessionActor,
						ID:           testDeleteAppCredRequest.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}
					openStack.On("AuthenticationTest", anyNotNilContext(), cred.OpenStackEnv).Return(types.ErrAuthentication)
					return openStack
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed", providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    service.NewCacaoInvalidParameterError(types.ErrAuthentication.Error()).GetBase(),
							},
							Operation: "",
							Provider:  "provider-d17e30598850n9abikl0",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}).Once()
					return eventOut
				},
			},
		},
		{
			name: "auth failed - other error",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						Username:     testDeleteAppCredRequest.SessionActor,
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					cred := types.Credential{
						Username:     testDeleteAppCredRequest.SessionActor,
						ID:           testDeleteAppCredRequest.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}
					openStack.On("AuthenticationTest", anyNotNilContext(), cred.OpenStackEnv).Return(fmt.Errorf("failed"))
					return openStack
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
									ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "fail to delete app cred",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						Username:     testDeleteAppCredRequest.SessionActor,
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					cred := types.Credential{
						Username:     testDeleteAppCredRequest.SessionActor,
						ID:           testDeleteAppCredRequest.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}
					openStack.On("AuthenticationTest", anyNotNilContext(), cred.OpenStackEnv).Return(nil)
					openStack.On("DeleteApplicationCredential", anyNotNilContext(), cred, testDeleteAppCredEnv["OS_APPLICATION_CREDENTIAL_ID"]).Return(fmt.Errorf("failed"))
					return openStack
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleteFailed",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
									ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "fail to update cache (do nothing)",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						Username:     testDeleteAppCredRequest.SessionActor,
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					cred := types.Credential{
						Username:     testDeleteAppCredRequest.SessionActor,
						ID:           testDeleteAppCredRequest.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}
					openStack.On("AuthenticationTest", anyNotNilContext(), cred.OpenStackEnv).Return(nil)
					openStack.On("DeleteApplicationCredential", anyNotNilContext(), cred, testDeleteAppCredEnv["OS_APPLICATION_CREDENTIAL_ID"]).Return(nil)
					openStack.On("InvalidateCache", types.CachePrefixListApplicationCredentials, testDeleteAppCredRequest.SessionActor).Return(fmt.Errorf("failed"))
					openStack.On("ListApplicationCredentials", anyNotNilContext(), cred).Return([]providers.ApplicationCredential{}, nil)
					return openStack
				},
			},
			args: args{
				sink: func(t *testing.T, args args) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsDeleted",
						providers.DeleteApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor-123",
									SessionEmulator: "emulator-123",
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
						}).Once()
					return eventOut
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credFac := tt.fields.credFac(tt.sharedObjs)
			openStack := tt.fields.openStack(t)
			sink := tt.args.sink(t, tt.args)
			h := AppCredDeletionHandler{
				credFac:   credFac,
				openStack: openStack,
			}

			h.Handle(tt.sharedObjs.request, sink)
			credFac.AssertExpectations(t)
			openStack.AssertExpectations(t)
			sink.AssertExpectations(t)
		})
	}
}
