package eventhandlers

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/triggeredactions"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
)

// CredentialCreatedHandler handles the event CredentialCreated
type CredentialCreatedHandler struct {
	providerMS      ports.ProviderMetadataMS
	credMS          ports.CredentialMS
	openStack       ports.OpenStack
	workspaceMS     ports.WorkspaceMS
	metadataStorage ports.CredentialMetadataStorage
}

// NewCredentialCreatedHandler ...
func NewCredentialCreatedHandler(openStack ports.OpenStack, providerMS ports.ProviderMetadataMS, workspaceMS ports.WorkspaceMS, credMS ports.CredentialMS, metadataStorage ports.CredentialMetadataStorage) CredentialCreatedHandler {
	return CredentialCreatedHandler{
		providerMS:      providerMS,
		credMS:          credMS,
		openStack:       openStack,
		workspaceMS:     workspaceMS,
		metadataStorage: metadataStorage,
	}
}

// Handle ...
func (h CredentialCreatedHandler) Handle(incoming service.CredentialCreateResponse, sink ports.OutgoingEvents) error {
	if incoming.Username == "" || incoming.ID == "" || incoming.Type != service.OpenStackCredentialType {
		return nil // skip
	}

	// get the full credential, including tags
	credential, err := h.credMS.GetCredential(context.TODO(), incoming.GetSessionActor(), incoming.GetSessionEmulator(), incoming.ID)
	if err != nil {
		log.WithError(err).Error("fail to fetch credential")
		return err
	}
	if credential == nil {
		return fmt.Errorf("credential is nil, but no error when fetching it")
	}

	var wg sync.WaitGroup
	wg.Add(1)
	go triggeredactions.CreateWorkspaceForProvider(&wg, credential, h.providerMS, h.workspaceMS, incoming.Session, incoming.ID)
	wg.Add(1)
	go triggeredactions.InspectCredentialWithWaitGroup(&wg, h.openStack, credential, h.metadataStorage, incoming.Session, incoming.ID)

	triggeredactions.TagCredential(h.openStack, credential, h.credMS, h.providerMS, incoming.Session, incoming.ID)

	wg.Wait()
	return nil
}
