package eventhandlers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/caching"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"time"
)

// AppCredCreationHandler is handler for creation request for openstack application credential
type AppCredCreationHandler struct {
	appCredFac       appCredentialFactory
	appCredConverter appCredentialConverter
	credMS           ports.CredentialMS
}

// NewAppCredCreationHandler creates a new AppCredCreationHandler
func NewAppCredCreationHandler(
	openstack caching.OpenStackOperationCache,
	credMS ports.CredentialMS,
	providerMS ports.ProviderMetadataMS,
	timeSrc ports.TimeSrc,
) AppCredCreationHandler {
	credFac := credsrc.NewCredentialFactory(credMS, providerMS)
	return AppCredCreationHandler{
		appCredFac:       newAppCredFactory(openstack, credFac, timeSrc),
		appCredConverter: newAppCredConverter(providerMS),
		credMS:           credMS,
	}
}

// Handle ...
func (h AppCredCreationHandler) Handle(request providers.ProviderRequest, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-openstack-service.domain.eventhandlers",
		"function": "AppCredCreationHandler.Handle",
	})
	request, err := h.checkRequestAndParseArgs(request)
	if err != nil {
		logger.WithError(err).Error("failed to handle request")
		sink.EventApplicationCredentialsCreateFailed(h.createFailureResponse(&request, err))
		return
	}
	credID, err := h.handle(context.TODO(), request)
	if err != nil {
		logger.WithError(err).Error("failed to handle request")
		sink.EventApplicationCredentialsCreateFailed(h.createFailureResponse(&request, err))
	} else {
		sink.EventApplicationCredentialsCreated(h.createSuccessResponse(&request, credID))
	}
}

func (h AppCredCreationHandler) checkRequestAndParseArgs(request providers.ProviderRequest) (providers.ProviderRequest, error) {
	if err := checkRequest(request); err != nil {
		return request, err
	}
	// parse args into struct
	var args providers.ApplicationCredentialCreationArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return request, err
	}
	request.Args = args
	return request, nil
}

// - create an application credential with openstack credential specified in the request
// - convert it to credential object
// - store in credential microservice
// - returned the credential ID on success
func (h AppCredCreationHandler) handle(ctx context.Context, request providers.ProviderRequest) (credID string, err error) {
	appCred, err := h.appCredFac.Create(ctx, request)
	if err != nil {
		return "", err
	}
	cred, err := h.appCredConverter.ConvertToCredObj(ctx, appCred, request.Session, request.Provider)
	if err != nil {
		return "", err
	}
	err = h.saveToCredMS(ctx, request.Session, cred)
	if err != nil {
		return "", err
	}
	return cred.ID, nil
}

// save the credential to credential service
func (h AppCredCreationHandler) saveToCredMS(ctx context.Context, session service.Session, credential *service.CredentialModel) error {
	return h.credMS.CreateCredential(ctx, session.GetSessionActor(), session.GetSessionEmulator(), *credential)
}

func (h AppCredCreationHandler) createSuccessResponse(req *providers.ProviderRequest, credID string) providers.CreateApplicationCredentialResponse {
	return providers.CreateApplicationCredentialResponse{
		BaseProviderReply: providers.BaseProviderReply{
			Session: service.Session{
				SessionActor:    req.GetSessionActor(),
				SessionEmulator: req.GetSessionEmulator(),
				ServiceError:    service.CacaoErrorBase{},
			},
			Operation: req.Operation,
			Provider:  req.Provider,
		},
		CredentialID: credID,
	}
}

func (h AppCredCreationHandler) createFailureResponse(req *providers.ProviderRequest, err error) providers.CreateApplicationCredentialResponse {
	cacaoErr, ok := err.(service.CacaoError)
	if !ok {
		cacaoErr = service.NewCacaoGeneralError(err.Error())
	}
	return providers.CreateApplicationCredentialResponse{
		BaseProviderReply: providers.BaseProviderReply{
			Session: service.Session{
				SessionActor:    req.GetSessionActor(),
				SessionEmulator: req.GetSessionEmulator(),
				ServiceError:    cacaoErr.GetBase(),
			},
			Operation: req.Operation,
			Provider:  req.Provider,
		},
		CredentialID: "",
	}
}

type appCredentialFactory interface {
	Create(ctx context.Context, request providers.ProviderRequest) (types.ApplicationCredential, error)
}

type appCredFactory struct {
	openStack caching.OpenStackOperationCache
	credFac   credsrc.CredentialFactory
	timeSrc   ports.TimeSrc
}

func newAppCredFactory(openstack caching.OpenStackOperationCache, credFac credsrc.CredentialFactory, timeSrc ports.TimeSrc) appCredFactory {
	return appCredFactory{
		openStack: openstack,
		credFac:   credFac,
		timeSrc:   timeSrc,
	}
}

// Create use credsrc.CredentialFactory to extract credential from request, then use the credential to create an openstack application credential.
func (f appCredFactory) Create(ctx context.Context, request providers.ProviderRequest) (types.ApplicationCredential, error) {
	// get credential for performing the openstack operation
	cred, err := f.credFac.GetCredential(ctx, request)
	if err != nil {
		return types.ApplicationCredential{}, err
	}
	args, err := f.extractRequestArgs(request)
	if err != nil {
		return types.ApplicationCredential{}, err
	}
	project, err := f.getProject(ctx, *cred, args)
	if err != nil {
		return types.ApplicationCredential{}, err
	}
	return f.createAppCred(ctx, *cred, args, project)
}

func (f appCredFactory) extractRequestArgs(request providers.ProviderRequest) (providers.ApplicationCredentialCreationArgs, error) {
	var args providers.ApplicationCredentialCreationArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return providers.ApplicationCredentialCreationArgs{}, err
	}
	// scope in args cannot be empty since app cred must be scoped to a specific project
	err = args.Scope.CheckScoped()
	if err != nil {
		return providers.ApplicationCredentialCreationArgs{}, err
	}
	return args, nil
}

func (f appCredFactory) getProject(ctx context.Context, cred types.Credential, args providers.ApplicationCredentialCreationArgs) (providers.Project, error) {
	// use ListProjects because GetProject does not allow lookup by name
	err := f.openStack.InvalidateCache(types.CachePrefixListProjects, cred.Username)
	if err != nil {
		log.WithError(err).Warn("fail to invalidate project list cache after app cred creation")
	}
	projectList, err := f.openStack.ListProjects(ctx, cred)
	if err != nil {
		return providers.Project{}, err
	}
	if args.Scope.Project.Name != "" {
		for _, project := range projectList {
			if project.Name == args.Scope.Project.Name {
				return project, nil
			}
		}
	} else {
		for _, project := range projectList {
			if project.ID == args.Scope.Project.ID {
				return project, nil
			}
		}
	}
	return providers.Project{}, fmt.Errorf("project %s/%s not found", args.Scope.Project.Name, args.Scope.Project.ID)
}

func (f appCredFactory) createAppCred(ctx context.Context, cred types.Credential, args providers.ApplicationCredentialCreationArgs, project providers.Project) (types.ApplicationCredential, error) {
	appCred, err := f.openStack.CreateApplicationCredential(ctx, cred, args.Scope, f.appCredName(args, project))
	if err != nil {
		return types.ApplicationCredential{}, err
	}
	return appCred, nil
}

func (f appCredFactory) appCredName(args providers.ApplicationCredentialCreationArgs, project providers.Project) string {
	return fmt.Sprintf("cacao_%s_%d_%s", project.Name, f.timeSrc.Now().Unix(), args.NamePostfix)
}

type appCredentialConverter interface {
	ConvertToCredObj(ctx context.Context, appCred types.ApplicationCredential, session service.Session, providerID common.ID) (*service.CredentialModel, error)
}

type appCredConverter struct {
	providerMS ports.ProviderMetadataMS
}

func newAppCredConverter(providerMS ports.ProviderMetadataMS) appCredConverter {
	return appCredConverter{
		providerMS: providerMS,
	}
}

// ConvertToCredObj convert an application credential object to a credential object
func (h appCredConverter) ConvertToCredObj(ctx context.Context, appCred types.ApplicationCredential, session service.Session, providerID common.ID) (*service.CredentialModel, error) {
	newCredName := appCred.Name // use the name of application credential as the credential Name
	if newCredName == "" {
		return nil, fmt.Errorf("name of application credential is empty, fail to create credential from it")
	}
	providerMetadata, err := h.getProviderMetadata(ctx, session, providerID)
	if err != nil {
		return nil, err
	}
	envFromMetadata, err := h.extractEnvFromMetadata(providerMetadata)
	if err != nil {
		return nil, err
	}
	env := h.appCredToEnv(appCred, envFromMetadata)
	credValue, err := json.Marshal(env)
	if err != nil {
		return nil, err
	}
	return &service.CredentialModel{
		Session: service.Session{
			SessionActor:    session.GetSessionActor(),
			SessionEmulator: session.GetSessionEmulator(),
		},
		ID:                newCredName,
		Name:              newCredName,
		Username:          session.GetSessionActor(),
		Type:              "openstack",
		Value:             string(credValue),
		Description:       fmt.Sprintf("openstack application credential created by CACAO for project %s", appCred.ProjectID),
		IsSystem:          false,
		IsHidden:          false,
		Visibility:        "",
		Tags:              h.credTags(providerID, appCred),
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}, nil
}

func (h appCredConverter) getProviderMetadata(ctx context.Context, session service.Session, providerID common.ID) (map[string]interface{}, error) {
	return h.providerMS.GetMetadata(ctx, session.GetSessionActor(), session.GetSessionEmulator(), providerID)
}

func (h appCredConverter) extractEnvFromMetadata(providerMetadata map[string]interface{}) (types.Environment, error) {
	var env = make(types.Environment)
	for _, fieldName := range h.requiredMetadataField() {
		fieldValue, ok := providerMetadata[fieldName]
		if !ok {
			return nil, fmt.Errorf("provider metadata missing field %s, required for construct CACAO credential from application credential", fieldName)
		}
		fieldValueStr, ok := fieldValue.(string)
		if !ok {
			return nil, fmt.Errorf("field %s in provider metadata is not string", fieldName)
		}
		if len(fieldValueStr) == 0 {
			return nil, fmt.Errorf("field %s in provider metadata is empty", fieldName)
		}
		env[fieldName] = fieldValueStr
	}
	return env, nil
}

// list of field names (in provider metadata) that is required
func (h appCredConverter) requiredMetadataField() []string {
	return []string{
		"OS_AUTH_URL",
		"OS_IDENTITY_API_VERSION",
		"OS_REGION_NAME",
		"OS_INTERFACE",
	}
}

// combine the env extracted from provider metadata and the ID and SECRET from types.ApplicationCredential.
func (h appCredConverter) appCredToEnv(appCred types.ApplicationCredential, envFromMetadata types.Environment) types.Environment {
	return types.Environment{
		"OS_AUTH_TYPE":                     "v3applicationcredential",
		"OS_AUTH_URL":                      envFromMetadata["OS_AUTH_URL"],
		"OS_IDENTITY_API_VERSION":          envFromMetadata["OS_IDENTITY_API_VERSION"],
		"OS_REGION_NAME":                   envFromMetadata["OS_REGION_NAME"],
		"OS_INTERFACE":                     envFromMetadata["OS_INTERFACE"],
		"OS_APPLICATION_CREDENTIAL_ID":     appCred.ID,
		"OS_APPLICATION_CREDENTIAL_SECRET": appCred.Secret,
	}
}

// tag credential with the provider ID and also "application" to indicate that it is an application credential
func (h appCredConverter) credTags(providerID common.ID, appCred types.ApplicationCredential) map[string]string {
	return map[string]string{
		providerID.String(): "", // keep compatibility with current frontend
		"application":       "", // keep compatibility with current frontend
		"cacao_managed":     "true",
		//"cacao_openstack_project_name": appCred.ProjectName,
		"cacao_openstack_project_id": appCred.ProjectID,
		"cacao_provider":             providerID.String(),
		"os_auth_type":               "v3applicationcredential",
	}
}
