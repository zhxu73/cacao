package domain

import (
	"context"
	"encoding/json"
	"github.com/eko/gocache/cache"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/caching"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/eventhandlers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/queryhandlers"

	log "github.com/sirupsen/logrus"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// Domain define the functions that a Domain type must support.
type Domain interface {
	Init(c types.Configuration)
	Start()
}

// OpenStackDomain is a domain that lists images/flavors from OpenStack and writes the responses out
type OpenStackDomain struct {
	config              *types.Configuration
	EventIn             ports.IncomingEventPort
	QueryIn             ports.IncomingQueryPort
	openStack           caching.OpenStackOperationCache
	CredMS              ports.CredentialMS
	ProviderMS          ports.ProviderMetadataMS
	WorkspaceMS         ports.WorkspaceMS
	CredMetadataStorage ports.CredentialMetadataStorage
	TimeSrc             ports.TimeSrc
	k8sNamespace        string
}

// NewOpenStackDomain returns a new *OpenStackDomain. Init is called
// on it as part of this function.
func NewOpenStackDomain(
	c *types.Configuration,
	eventIn ports.IncomingEventPort,
	queryIn ports.IncomingQueryPort,
	openstack ports.OpenStack,
	Cache cache.CacheInterface,
	CacheDefaultTTL time.Duration,
	credMS ports.CredentialMS,
	workspaceMS ports.WorkspaceMS,
	providerMS ports.ProviderMetadataMS,
	credMetadataStorage ports.CredentialMetadataStorage,
	timeSrc ports.TimeSrc,
) *OpenStackDomain {
	retval := &OpenStackDomain{
		config:              c,
		EventIn:             eventIn,
		QueryIn:             queryIn,
		openStack:           caching.NewOpenStackOperationCache(openstack, Cache, CacheDefaultTTL),
		CredMS:              credMS,
		ProviderMS:          providerMS,
		WorkspaceMS:         workspaceMS,
		CredMetadataStorage: credMetadataStorage,
		TimeSrc:             timeSrc,
		k8sNamespace:        c.K8SNamespace,
	}
	return retval
}

// Start fires up the domain object, allowing it to listen for incoming queries.
func (domain *OpenStackDomain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	err := domain.startQueryWorker(ctx, &wg)
	if err != nil {
		return err
	}

	domain.EventIn.SetHandlers(domain.getEventHandlers())
	wg.Add(1)
	err = domain.EventIn.Start(ctx, &wg)
	if err != nil {
		return err
	}

	<-ctx.Done()
	wg.Wait()
	return nil
}

func (domain *OpenStackDomain) startQueryWorker(ctx context.Context, wg *sync.WaitGroup) error {
	incomingQueryChan := make(chan types.CloudEventRequest, domain.config.DefaultChannelBufferSize)
	domain.QueryIn.InitChannel(incomingQueryChan)
	err := domain.QueryIn.Start(ctx)
	if err != nil {
		return err
	}

	for i := 0; i < domain.config.QueryWorkerCount; i++ {
		wg.Add(1)
		go domain.processQueryWorker(ctx, incomingQueryChan, wg)
	}
	return nil
}

// processQueryWorker performs the listing and writes out the response on the ResponsePort.
func (domain *OpenStackDomain) processQueryWorker(ctx context.Context, cloudEventReqChan chan types.CloudEventRequest, wg *sync.WaitGroup) {
	defer wg.Done()

	// given that we are closing the channel when the service context is canceled, it
	// is not necessary to use service context as the parent for the queryCtx, since
	// we still want to process the queries in the channel buffer
	origin := context.Background()

	for cloudEventReq := range cloudEventReqChan {
		queryCtx, cancel := context.WithTimeout(origin, time.Second*60)
		domain.processQuery(queryCtx, cloudEventReq)
		cancel()
	}
}

func (domain *OpenStackDomain) processQuery(ctx context.Context, cloudEventReq types.CloudEventRequest) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "OpenStackDomain.processQuery",
	})
	request, err := domain.parseRequest(cloudEventReq.CloudEvent)
	if err != nil {
		logger.WithError(err).Error("fail to parse request")
		return
	}
	handler := domain.selectQueryHandler(request)
	if handler == nil {
		logger.WithField("op", request.Operation).Error("no handler selected for query")
		return
	}
	response := handler.Handle(ctx, request)
	replyCe, err := response.ToCloudEvent("openstack-provider-service", providers.OpenStackQueryPrefix)
	if err != nil {
		logger.WithError(err).Error("fail to convert reply to cloudevent")
		return
	}
	if err = cloudEventReq.Replyer.Reply(replyCe); err != nil {
		logger.WithError(err).Error("fail to send reply")
	}
}

func (domain *OpenStackDomain) parseRequest(ce cloudevents.Event) (providers.ProviderRequest, error) {
	var req providers.ProviderRequest
	err := json.Unmarshal(ce.Data(), &req)
	if err != nil {
		return providers.ProviderRequest{}, err
	}
	return req, nil
}

// selects a handler based on the operation
func (domain *OpenStackDomain) selectQueryHandler(request providers.ProviderRequest) queryhandlers.Handler {
	credFac := credsrc.NewCredentialFactory(domain.CredMS, domain.ProviderMS)
	switch request.Operation {
	case providers.ApplicationCredentialsGetOp:
		return queryhandlers.ApplicationCredentialGetHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.ApplicationCredentialsListOp:
		return queryhandlers.ApplicationCredentialListHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.AuthenticationTestOp:
		return queryhandlers.AuthenticationTestHandler{OpenStack: domain.openStack}
	case providers.CredentialListOp:
		return queryhandlers.CredentialListHandler{
			OpenStack:                 domain.openStack,
			CredMS:                    domain.CredMS,
			ProviderMetadataMS:        domain.ProviderMS,
			CredentialMetadataStorage: domain.CredMetadataStorage,
		}
	case providers.RegionsListOp:
		return queryhandlers.RegionListHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.ImagesGetOp:
		return queryhandlers.ImageGetHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.ImagesListOp:
		return queryhandlers.ImageListHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.FlavorsGetOp:
		return queryhandlers.FlavorGetHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.FlavorsListOp:
		return queryhandlers.FlavorListHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.ProjectsGetOp:
		return queryhandlers.ProjectGetHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.ProjectsListOp:
		return queryhandlers.ProjectListHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.CatalogListOp:
		return queryhandlers.CatalogListHandler{OpenStack: domain.openStack, CredFac: credFac}
	case providers.TokenGetOp:
		return queryhandlers.TokenGetHandler{OpenStack: domain.openStack, CredFac: credFac}
	// Force the cache to get filled in response to the cache.populate operation.
	// Remember, the user is set in the environment variables passed along in the
	// credentials, so the username will not appear in the args here.
	// case providers.CachePopulateOp:
	// 	return CachePopulateHandler{domain.OpenStack, credSelector}
	default:
		return nil
	}
}

func (domain *OpenStackDomain) getEventHandlers() eventHandlers {
	return eventHandlers{
		UserLoginHandler:         eventhandlers.NewUserLoginHandler(domain.k8sNamespace, domain.openStack, domain.CredMS, domain.CredMetadataStorage),
		AppCredCreationHandler:   eventhandlers.NewAppCredCreationHandler(domain.openStack, domain.CredMS, domain.ProviderMS, domain.TimeSrc),
		AppCredDeletionHandler:   eventhandlers.NewAppCredDeletionHandler(domain.openStack, domain.ProviderMS, domain.CredMS),
		CredentialCreatedHandler: eventhandlers.NewCredentialCreatedHandler(domain.openStack, domain.ProviderMS, domain.WorkspaceMS, domain.CredMS, domain.CredMetadataStorage),
		CredentialUpdatedHandler: eventhandlers.NewCredentialUpdatedHandler(domain.openStack, domain.ProviderMS, domain.CredMS),
	}
}

type eventHandlers struct {
	UserLoginHandler         *eventhandlers.UserLoginHandler
	AppCredCreationHandler   eventhandlers.AppCredCreationHandler
	AppCredDeletionHandler   eventhandlers.AppCredDeletionHandler
	CredentialCreatedHandler eventhandlers.CredentialCreatedHandler
	CredentialUpdatedHandler eventhandlers.CredentialUpdatedHandler
}

// HandleUserLogin ...
func (h eventHandlers) HandleUserLogin(request types.UserLoginEvent, sink ports.OutgoingEvents) {
	h.UserLoginHandler.Handle(request, sink)
}

// HandleApplicationCredentialsCreationRequested ...
func (h eventHandlers) HandleApplicationCredentialsCreationRequested(request providers.ProviderRequest, sink ports.OutgoingEvents) {
	h.AppCredCreationHandler.Handle(request, sink)
}

// HandleApplicationCredentialsDeletionRequested ...
func (h eventHandlers) HandleApplicationCredentialsDeletionRequested(request providers.ProviderRequest, sink ports.OutgoingEvents) {
	h.AppCredDeletionHandler.Handle(request, sink)
}

func (h eventHandlers) HandleCredentialCreated(request service.CredentialCreateResponse, sink ports.OutgoingEvents) {
	h.CredentialCreatedHandler.Handle(request, sink)
}

func (h eventHandlers) HandleCredentialUpdated(request service.CredentialUpdateResponse, sink ports.OutgoingEvents) {
	h.CredentialUpdatedHandler.Handle(request, sink)
}
