// Code generated by mockery v2.14.0. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"

	types "gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// IncomingQueryPort is an autogenerated mock type for the IncomingQueryPort type
type IncomingQueryPort struct {
	mock.Mock
}

// Init provides a mock function with given fields: config
func (_m *IncomingQueryPort) Init(config types.Configuration) {
	_m.Called(config)
}

// InitChannel provides a mock function with given fields: data
func (_m *IncomingQueryPort) InitChannel(data chan types.CloudEventRequest) {
	_m.Called(data)
}

// Start provides a mock function with given fields:
func (_m *IncomingQueryPort) Start() {
	_m.Called()
}

type mockConstructorTestingTNewIncomingQueryPort interface {
	mock.TestingT
	Cleanup(func())
}

// NewIncomingQueryPort creates a new instance of IncomingQueryPort. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewIncomingQueryPort(t mockConstructorTestingTNewIncomingQueryPort) *IncomingQueryPort {
	mock := &IncomingQueryPort{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
