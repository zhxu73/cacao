package ports

import (
	"context"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao-common/service"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fulfill the purposes of both income and outgoing query ports
type IncomingQueryPort interface {
	Start(ctx context.Context) error
	InitChannel(data chan types.CloudEventRequest)
}

// IncomingEventPort ...
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for the event operations that this service supports
type IncomingEventHandlers interface {
	HandleUserLogin(event types.UserLoginEvent, sink OutgoingEvents)
	HandleApplicationCredentialsCreationRequested(request providers.ProviderRequest, sink OutgoingEvents)
	HandleApplicationCredentialsDeletionRequested(request providers.ProviderRequest, sink OutgoingEvents)
	HandleCredentialCreated(request service.CredentialCreateResponse, sink OutgoingEvents)
	HandleCredentialUpdated(request service.CredentialUpdateResponse, sink OutgoingEvents)
}

// OutgoingEvents represents all events this service will be publishing (except
// for via service client for other service), each method publish an event
// associate with the method name.
type OutgoingEvents interface {
	EventApplicationCredentialsCreated(providers.CreateApplicationCredentialResponse)
	EventApplicationCredentialsCreateFailed(providers.CreateApplicationCredentialResponse)
	EventApplicationCredentialsDeleted(providers.DeleteApplicationCredentialResponse)
	EventApplicationCredentialsDeleteFailed(providers.DeleteApplicationCredentialResponse)
	Nothing() // do nothing, this method explicitly denote that a handler does not publish any events
}

// ProviderMetadataMS is an interface to interact with provider metadata service.
type ProviderMetadataMS interface {
	GetMetadata(ctx context.Context, actor, emulator string, providerID common.ID) (map[string]interface{}, error)
	ListOpenStackProvider(ctx context.Context, actor, emulator string) ([]service.ProviderModel, error)
}

// CredentialMS ...
type CredentialMS interface {
	GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error)
	ListCredentials(ctx context.Context, actor, emulator string) ([]service.CredentialModel, error)
	ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error)
	CreateCredential(ctx context.Context, actor, emulator string, cred service.CredentialModel) error
	TagCredential(ctx context.Context, actor, emulator string, ID string, tags map[string]string) error
	DeleteCredential(ctx context.Context, actor, emulator, ID string) error
}

// WorkspaceMS ...
type WorkspaceMS interface {
	CreateWorkspaceForProviderAsync(ctx context.Context, actor, emulator string, providerID common.ID) error
}

// OpenStack ...
type OpenStack interface {
	AuthenticationTest(ctx context.Context, credential types.Environment) error
	InspectCredential(ctx context.Context, credential types.Credential) (types.CredentialMetadata, error)
	GetApplicationCredential(ctx context.Context, credential types.Credential, id string) (*providers.ApplicationCredential, error)
	GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error)
	GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.OpenStackImage, error)
	GetProject(ctx context.Context, credential types.Credential, id string) (*providers.Project, error)
	GetToken(ctx context.Context, credential types.Credential) (*providers.Token, error)
	ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error)
	ListApplicationCredentials(ctx context.Context, credential types.Credential) ([]providers.ApplicationCredential, error)
	ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error)
	ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.OpenStackImage, error)
	ListProjects(ctx context.Context, credential types.Credential) ([]providers.Project, error)
	ListCatalog(ctx context.Context, credential types.Credential) ([]providers.CatalogEntry, error)
	ListZone(ctx context.Context, credential types.Credential) ([]providers.DNSZone, error)
	ListRecordset(ctx context.Context, credential types.Credential, zone string) ([]providers.DNSRecordset, error)
	CreateApplicationCredential(ctx context.Context, credential types.Credential, scope providers.ProjectScope, name string) (types.ApplicationCredential, error)
	DeleteApplicationCredential(ctx context.Context, credential types.Credential, idOrName string) error
}

// CredentialMetadataStorage ...
type CredentialMetadataStorage interface {
	ListCredentials(owner string, projectID, projectName string) ([]types.CredentialMetadata, error)
	GetCredential(owner string, credID string) (types.CredentialMetadata, error)
	SaveCredential(cred *types.Credential, metadata types.CredentialMetadata) error
}

// TimeSrc is source of current time
type TimeSrc interface {
	Now() time.Time
}
