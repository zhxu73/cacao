// Package workflowdefinition implements functions for creating, updating, saving,
// and deleting Workflow Definitions. A database backend is used to save and manage
// WorkflowDefinitions without having to access Git each time (like a cache)
package workflowdefinition

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strings"
	"text/template"
	"time"

	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/workflow-definition-service/workflowdefinition/types"

	"github.com/docker/distribution/reference"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-git/go-git/v5/storage/memory"
	"go.mongodb.org/mongo-driver/bson"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/validation"
	"sigs.k8s.io/yaml"
)

const (
	cacaoFile     = "wfd.yaml"
	defaultBranch = "master"
)

// WorkflowDefinition struct is created from a YAML file in a git repository and holds
// information necessary to start a WorkflowDefinition
type WorkflowDefinition struct {
	ID          string              `bson:"_id" json:"id,omitempty" yaml:"id"`
	Name        string              `bson:"name" json:"name,omitempty" yaml:"name"`
	Description string              `bson:"description" json:"description,omitempty" yaml:"description"`
	Type        string              `bson:"type" json:"type,omitempty" yaml:"type"`
	Owner       string              `bson:"owner" json:"owner,omitempty"`
	Build       []*common.BuildStep `bson:"build" json:"build,omitempty" yaml:"build"`
	Repository  *common.Repository  `bson:"repository" json:"repository,omitempty"`
	Commit      *object.Commit      `bson:"commit" json:"commit,omitempty"`
	TCPPorts    []core.ServicePort  `bson:"tcp_ports" json:"tcp_ports,omitempty" yaml:"tcp_ports"`
	HTTPPorts   []core.ServicePort  `bson:"http_ports" json:"http_ports,omitempty" yaml:"http_ports"`
	Error       string              `bson:"error_msg" json:"error_msg,omitempty"`
	// This is used to read in a 'workflow' string before parsing/converting
	// to the correct Workflow type
	RawWorkflow string              `bson:"workflow" json:"workflow,omitempty" yaml:"workflow"`
	Workflow    types.CacaoWorkflow `bson:"-" json:"-" yaml:"-"`
}

var adapter common.CRUD
var collection = os.Getenv("MONGODB_COLLECTION_NAME")

// InitMongoDB will initialize the connection to the database
func InitMongoDB(name, address string) error {
	var err error
	adapter, err = common.GetMongoDBAdapter(name, address)
	return err
}

// Define allows a User to create a WorkflowDefinition by submitting a link to
// their Github repository and branch name
func define(id, rawurl, branch string, user common.User, secretID string) (wfd WorkflowDefinition, err error) {
	// Add and save as much info before using git in case it fails or is slow
	if branch == "" {
		branch = defaultBranch
	}
	wfd.Owner = user.Username
	wfd.ID = id
	rawurl = strings.TrimSuffix(rawurl, "\\.git")

	// Parse URL to create a Repository with URL/Branch for ID and RawURL for cloning
	url, err := url.Parse(rawurl)
	if err != nil {
		return
	}
	wfd.Repository = &common.Repository{
		RawURL: rawurl,
		URL:    url.Host + strings.TrimRight(url.Path, "/"),
		Branch: branch,
	}
	// Do a preliminary save in case reading from git fails
	err = wfd.save()
	if err != nil {
		return
	}

	// Read WorkflowDefinition file from git repo
	var secret common.Secret
	if len(secretID) > 0 {
		secret = user.Secrets[secretID]
	}
	err = wfd.readFromGit(secret)
	if err != nil {
		if err.Error() != "repository not found" {
			oldErr := err
			wfd.Error = fmt.Sprintf("correct error in git and update: %s", err)
			err = wfd.save()
			if err == nil {
				err = oldErr
			}
		}
		return
	}

	err = wfd.validate()
	if err != nil {
		return
	}
	err = wfd.save()
	return
}

// Validates the contents of the WorkflowDefinition.
// Returns whether the WorkflowDefinition appears valid.
func (wfd *WorkflowDefinition) validate() (err error) {
	// Check that required attribute, 'type', is used
	if len(wfd.Type) == 0 {
		wfd.Error = fmt.Sprintf("correct error in git and update: 'type' parameter is required")
		return errors.New("error: WorkflowDefinition did not contain 'type' parameter")
	}

	// Check that name is valid (using DNS 1123 so limit is 253)
	errList := validation.IsDNS1123Subdomain(wfd.Name)
	if len(errList) > 0 {
		wfd.Error = fmt.Sprintf("correct error in git and update: %s", errList)
		return fmt.Errorf("error validating WorkflowDefinition Name: %v", errList)
	}

	// Check that Image is valid
	for _, step := range wfd.Build {
		_, imageErr := reference.Parse(step.Image)
		if imageErr != nil {
			wfd.Error = fmt.Sprintf(
				"correct error in git and update: invalid value for Image '%s': %s",
				step.Image, imageErr,
			)
			return fmt.Errorf(
				"error: invalid value for Image '%s': %s",
				step.Image, imageErr)
		}
	}
	return
}

// GetID returns the WorkflowDefinition ID
func (wfd WorkflowDefinition) GetID() string {
	return wfd.ID
}

// GetCollection returns the collection in which WorkflowDefinition should be stored
func (wfd WorkflowDefinition) GetCollection() string {
	return collection
}

// GetOwner returns the Owner of this WorkflowDefinition
func (wfd WorkflowDefinition) GetOwner() string {
	return wfd.Owner
}

// update will clone the Git repo again to update the WorkflowDefinition
func (wfd *WorkflowDefinition) update(user common.User, secretID string) error {
	var secret common.Secret
	if len(secretID) > 0 {
		secret = user.Secrets[secretID]
	}
	// Create a copy of the WFD so that if reading or validation fails we won't
	// have changed the original object.
	newWFD := *wfd
	err := newWFD.readFromGit(secret)
	if err != nil {
		return err
	}
	err = newWFD.validate()
	if err != nil {
		return err
	}
	*wfd = newWFD
	return wfd.save()
}

// edit allows the User to change the WorkflowDefinition and push these changes
// back to the git repository. Branch and URL cannot be changed since that is a
// new WorkflowDefinition
func (wfd *WorkflowDefinition) edit(reqBytes []byte, commitMsg string, secret common.Secret) (err error) {
	// Create a copy of the WFD so that if parsing or validation fails we won't
	// have changed the original object.
	editedWFD := *wfd
	err = json.Unmarshal(reqBytes, &editedWFD)
	if err != nil {
		return
	}

	err = editedWFD.validate()
	if err != nil {
		return
	}

	*wfd = editedWFD

	if len(commitMsg) == 0 {
		commitMsg = "Default Cacao commit message"
	}

	err = wfd.save()
	if err != nil {
		return
	}

	commit, err := wfd.writeToGit(commitMsg, secret)
	if err != nil {
		return
	}
	wfd.Commit = commit
	err = wfd.save()
	return
}

// save inserts or updates the WorkflowDefinition struct into the database
func (wfd *WorkflowDefinition) save() error {
	return adapter.Replace(collection, wfd)
}

func (wfd *WorkflowDefinition) delete() error {
	return adapter.Delete(collection, wfd.ID)
}

func get(id, owner string) (*WorkflowDefinition, error) {
	data, err := adapter.Read(collection, id)
	if err != nil {
		return &WorkflowDefinition{}, fmt.Errorf("error reading from database: %v", err)
	}
	var wfd WorkflowDefinition
	err = bson.Unmarshal(data, &wfd)

	if err != nil {
		return &WorkflowDefinition{}, fmt.Errorf("error unmarshalling BSON response: %v", err)
	}

	if wfd.Owner != owner {
		return &WorkflowDefinition{}, fmt.Errorf("user not authorized for this WorkflowDefinition")
	}
	// Parse the RawWorkflow string only if Type exists to avoid errors
	if wfd.Type != "" {
		err = wfd.parseRawWorkflow()
		if err != nil {
			return &WorkflowDefinition{}, err
		}
	}

	return &wfd, nil
}

func getForUser(owner string) (wfds []WorkflowDefinition, err error) {
	documents, err := adapter.ReadForUser(collection, owner)
	if err != nil {
		return
	}
	for _, doc := range documents {
		var bsonBytes []byte
		bsonBytes, err = bson.Marshal(doc)
		if err != nil {
			return
		}
		var wfd WorkflowDefinition
		err = bson.Unmarshal(bsonBytes, &wfd)
		if err != nil {
			return
		}
		// Parse the RawWorkflow string only if Type exists to avoid errors
		if wfd.Type != "" {
			err = wfd.parseRawWorkflow()
			if err != nil {
				return
			}
		}
		wfd.parseRawWorkflow()
		wfds = append(wfds, wfd)
	}
	return
}

// readFromGit clones a git repository into memory and creates a WorkflowDefinition
func (wfd *WorkflowDefinition) readFromGit(secret common.Secret) (err error) {
	repo, fs, err := cloneRepository(wfd.Repository, secret)
	if err != nil {
		return
	}
	// Get commit information to add to WorkflowDefinition
	head, err := repo.Head()
	if err != nil {
		return
	}
	commit, err := repo.CommitObject(head.Hash())
	if err != nil {
		return
	}
	wfd.Commit = commit
	// Now read remaining information from file
	file, err := fs.Open(cacaoFile)
	if err != nil {
		err = fmt.Errorf("repository not found")
		return
	}
	defer file.Close()
	byteValue, err := ioutil.ReadAll(file)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(byteValue, &wfd)
	if err != nil {
		return
	}

	// Make sure repository has correct Dockerfile and parse image name templates
	if len(wfd.Build) > 0 {
		for _, step := range wfd.Build {
			var parsedImageName string
			parsedImageName, err = parseBuildTemplate(step.Image, wfd.Repository.Branch, wfd.Commit.Hash.String())
			if err != nil {
				err = fmt.Errorf("failed to template image name '%s': %s", step.Image, err.Error())
				return
			}
			step.Image = parsedImageName
			_, err = fs.Stat(step.Dockerfile)
			if err != nil {
				err = fmt.Errorf("dockerfile '%s' not found", step.Dockerfile)
				return
			}
		}
	}
	// Clear any error message from the WFD
	wfd.Error = ""
	return
}

// parseRawWorkflow will read the RawWorkflow string and create the correct
// type of CacaoWorkflow from it
func (wfd *WorkflowDefinition) parseRawWorkflow() error {
	workflow, err := types.ParseRawWorkflow(wfd.RawWorkflow, wfd.Type, wfd.Build)
	if err != nil {
		return err
	}
	wfd.Workflow = workflow
	return nil
}

// parseBuildTemplate is used to fill out commit and branch variables for image tags
func parseBuildTemplate(imageName, branch, commit string) (string, error) {
	// Create "image" function to get Image name from BuildStep n
	funcMap := template.FuncMap{
		"commit": func() string {
			return commit
		},
		"branch": func() string {
			return branch
		},
	}
	// Create template and parse input text
	t, err := template.New("template").Funcs(funcMap).Parse(imageName)
	if err != nil {
		return "", err
	}
	// Execute template and write to buffer
	var result bytes.Buffer
	err = t.Execute(&result, nil)
	if err != nil {
		return "", err
	}
	return string(result.Bytes()), nil
}

// writeToGit will create a commit and push back to the git repository
func (wfd *WorkflowDefinition) writeToGit(commitMsg string, secret common.Secret) (commit *object.Commit, err error) {
	repo, fs, err := cloneRepository(wfd.Repository, secret)
	if err != nil {
		return
	}

	file, err := fs.OpenFile(cacaoFile, os.O_RDWR, 0)
	if err != nil {
		return
	}
	defer file.Close()

	// Copy WFD value to avoid overwriting in the next steps
	wfdCopy := *wfd

	// Empty out some fields that shouldn't be written to git
	wfdCopy.ID = ""
	wfdCopy.Owner = ""
	wfdCopy.Error = ""
	wfdCopy.Commit = nil
	wfdCopy.Workflow = nil
	wfdCopy.Repository = nil

	data, err := yaml.Marshal(&wfdCopy)
	if err != nil {
		return
	}
	file.Truncate(0)
	file.Write(data)

	worktree, err := repo.Worktree()
	if err != nil {
		return
	}

	hash, err := worktree.Commit(commitMsg, &git.CommitOptions{
		All: true,
		Author: &object.Signature{
			Name:  "Cacao WorkflowDefinition",
			Email: "cacao@cyverse.org",
			When:  time.Now(),
		},
	})
	if err != nil {
		return
	}

	commit, err = repo.CommitObject(hash)
	if err != nil {
		return
	}
	if len(secret.Value) == 0 {
		err = fmt.Errorf("a secret is required for pushing to git, but none are present")
		return
	}
	err = repo.Push(&git.PushOptions{
		Auth: &http.BasicAuth{
			Username: secret.Username,
			Password: secret.Value,
		},
	})
	return
}

func cloneRepository(repo *common.Repository, secret common.Secret) (gitRepo *git.Repository, fs billy.Filesystem, err error) {
	fs = memfs.New()
	cloneOpts := &git.CloneOptions{
		URL:           repo.RawURL,
		ReferenceName: plumbing.NewBranchReferenceName(repo.Branch),
		Depth:         1,
	}
	if len(secret.Value) > 0 {
		cloneOpts.Auth = &http.BasicAuth{
			Username: secret.Username,
			Password: secret.Value,
		}
	}
	gitRepo, err = git.Clone(memory.NewStorage(), fs, cloneOpts)
	return
}
