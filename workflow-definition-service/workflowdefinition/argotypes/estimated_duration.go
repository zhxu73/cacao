// This file is copied from
// https://github.com/argoproj/argo/blob/master/pkg/apis/workflow/v1alpha1/estimated_duration.go

package argotypes

import "time"

// EstimatedDuration is in seconds.
type EstimatedDuration int

func (d EstimatedDuration) ToDuration() time.Duration {
	return time.Second * time.Duration(d)
}

func NewEstimatedDuration(d time.Duration) EstimatedDuration {
	return EstimatedDuration(d.Seconds())
}
