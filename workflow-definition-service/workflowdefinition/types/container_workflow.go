package types

import (
	"fmt"

	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/common/wfdefcommon"

	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/yaml"
)

// ContainerWorkflow is a type alias for a list of Kubernetes Containers
type ContainerWorkflow []core.Container

// NewContainerWorkflow is a constructor to create a ContainerWorkflow type by reading
// the []byte from the string in 'wfd.yaml'
func NewContainerWorkflow(parsedWorkflow []byte) (CacaoWorkflow, error) {
	var containerWorkflow ContainerWorkflow
	err := yaml.Unmarshal(parsedWorkflow, &containerWorkflow)
	return containerWorkflow, err
}

// Type returns the name of this Workflow Type
func (containers ContainerWorkflow) Type() string {
	return "ContainerWorkflow"
}

// Run will create and deploy all the necessary Kubernetes resources for this type
// of Workflow. These are the resources created by this function:
//   - deployments.apps
//   - scaledobjects.keda.k8s.io
func (containers ContainerWorkflow) Run(clientsets *wfdefcommon.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	runID := request["id"].(string)
	version := request["version"].(string)

	// Create Deployment
	deployment := createDeployment(containers, runID, name, version)
	_, err := clientsets.CreateDeployment(deployment, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`deployments.apps "%s" already exists`, deployment.Name) {
		_, err = clientsets.UpdateDeployment(deployment, namespace)
	}
	if err != nil {
		return fmt.Errorf("Unable to create deployment '%s': %v", name, err)
	}

	// Create ScaledObject
	scaledObject := createUnstructuredScaledObject(name, runID, namespace)
	_, err = clientsets.CreateScaledObject(scaledObject, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`scaledobjects.keda.k8s.io "%s" already exists`, name) {
		_, err = clientsets.UpdateScaledObject(scaledObject, namespace)
	}
	if err != nil {
		return fmt.Errorf("Unable to create ScaledObject '%s': %v", name, err)
	}

	// Create Service
	service := createService(name, runID, version, append(request["httpPorts"].([]core.ServicePort), request["tcpPorts"].([]core.ServicePort)...))
	_, err = clientsets.CreateService(service, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`services "%s" already exists`, service.Name) {
		_, err = clientsets.UpdateService(service, namespace)
	}
	if err != nil {
		return fmt.Errorf("Error creating Service: %v", err)
	}
	return err
}

// SetupRouting ...
func (containers ContainerWorkflow) SetupRouting(clientsets *wfdefcommon.K8sClientsets, adminClientsets *wfdefcommon.K8sClientsets, request map[string]interface{}, name, namespace string, cluster common.Cluster) error {
	err := standardIstioRouting(clientsets, adminClientsets, request, name, namespace, cluster)
	if err != nil {
		return fmt.Errorf("Error creating Istio routing: %v", err)
	}
	return nil
}

// Delete will delete the resources created by the Run method
func (containers ContainerWorkflow) Delete(clientsets *wfdefcommon.K8sClientsets, name, namespace string) error {
	// Delete ScaledObject
	err := clientsets.DeleteScaledObject(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteService(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteDeployment(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteVirtualService(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteDestinationRule(name, namespace)
	if err != nil {
		return err
	}
	return err
}

// ScaleFromZero is used to scale up or re-create a Deployment that has been scaled
// to zero due to inactivity
func (containers ContainerWorkflow) ScaleFromZero(clientsets *wfdefcommon.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	runID := request["id"].(string)
	version := request["version"].(string)

	// Check if deployment already exists
	_, err := clientsets.GetDeployment(name, namespace)
	if err != nil {
		// Deployment DNE, so re-create instead of erroring
		return containers.Run(clientsets, request, name, namespace)
	}
	err = clientsets.ScaleDeployment(name, namespace, int32(1))
	if err != nil {
		return fmt.Errorf("Error scaling up deployment '%s': %v", name, err)
	}

	// Create ScaledObject
	scaledObject := createUnstructuredScaledObject(name, request["id"].(string), namespace)
	_, err = clientsets.CreateScaledObject(scaledObject, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`scaledobjects.keda.k8s.io "%s" already exists`, name) {
		_, err = clientsets.UpdateScaledObject(scaledObject, namespace)
	}
	if err != nil {
		return fmt.Errorf("Unable to create scaled object '%s': %v", name, err)
	}

	// Create Service
	service := createService(name, runID, version, append(request["httpPorts"].([]core.ServicePort), request["tcpPorts"].([]core.ServicePort)...))
	_, err = clientsets.CreateService(service, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`services "%s" already exists`, service.Name) {
		_, err = clientsets.UpdateService(service, namespace)
	}
	if err != nil {
		return fmt.Errorf("Error creating Service: %v", err)
	}
	return err
}

// createUnstructuredScaledObject creates a K8s Unstructured object that is a
// KEDA ScaledObject
func createUnstructuredScaledObject(name, id, namespace string) *unstructured.Unstructured {
	serviceName := fmt.Sprintf("%s.%s.svc.cluster.local", name, namespace)
	promQuery := fmt.Sprintf("sum(rate(istio_requests_total{destination_service=\"%s\"}[1m]))", serviceName)

	return &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "keda.k8s.io/v1alpha1",
			"kind":       "ScaledObject",
			"metadata": map[string]interface{}{
				"name": name,
				"labels": map[string]interface{}{
					"deploymentName": name,
					"cacao_id":       id,
				},
			},
			"spec": map[string]interface{}{
				"scaleTargetRef": map[string]interface{}{
					"deploymentName": name,
				},
				"pollingInterval": int64(15),
				"cooldownPeriod":  int64(30),
				"maxReplicaCount": int64(10),
				"minReplicaCount": int64(1),
				"triggers": []interface{}{
					map[string]interface{}{
						"type": "prometheus",
						"metadata": map[string]interface{}{
							"serverAddress": "http://prometheus.istio-system.svc.cluster.local:9090",
							"metricName":    "access_frequency",
							"threshold":     "1",
							"query":         promQuery,
						},
					},
				},
			},
		},
	}
}
