package fixtures

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

const (
	// TemplateTypeFixturesFilename is the name of a file containing template type fixtures
	TemplateTypeFixturesFilename string = "template_type_fixtures.json"
	// TemplateCustomFieldTypeFixturesFilename is the name of a file containing template custom field type fixtures
	TemplateCustomFieldTypeFixturesFilename string = "template_custom_field_type_fixtures.json"
)

// DataFixtures communicates to IncomingEventPort to add fixtures
type DataFixtures struct {
	Config            *types.Config
	IncomingEventPort ports.IncomingEventHandlers
	IncomingQueryPort ports.IncomingQueryHandlers
}

// Init initializes the fixtures
func (fixtures *DataFixtures) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.Init",
	})

	logger.Info("initializing DataFixtures")

	fixtures.Config = config
}

// Start starts adding the fixtures
func (fixtures *DataFixtures) Start() error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.Start",
	})

	logger.Info("starting DataFixtures")

	actor := service.Actor{
		Actor:    "cacao-admin",
		Emulator: "",
	}

	err := fixtures.addTemplateTypes(actor)
	if err != nil {
		return err
	}
	err = fixtures.addTemplateCustomFieldTypes(actor)
	if err != nil {
		return err
	}
	logger.Infof("DataFixtures finished")
	return nil
}

func (fixtures *DataFixtures) getTemplateTypeFixtures() ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.getTemplateTypeFixtures",
	})

	// read template_type_fixtures.json
	absFixturesFilename, err := filepath.Abs(TemplateTypeFixturesFilename)
	if err != nil {
		errorMessage := fmt.Sprintf("Could not access the template type fixture file %s", TemplateTypeFixturesFilename)
		logger.WithError(err).Errorf(errorMessage)
		return []types.TemplateType{}, fmt.Errorf(errorMessage)
	}

	_, err = os.Stat(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("the template type fixture file (%s) error", absFixturesFilename)
		return []types.TemplateType{}, err
	}

	jsonBytes, err := os.ReadFile(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("Could not read the template type fixture file %s", absFixturesFilename)
		return []types.TemplateType{}, err
	}

	templateTypeFixtures := []types.TemplateType{}

	err = json.Unmarshal(jsonBytes, &templateTypeFixtures)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into []TemplateType"
		logger.WithError(err).Error(errorMessage)
		return []types.TemplateType{}, fmt.Errorf(errorMessage)
	}

	return templateTypeFixtures, nil
}

func (fixtures *DataFixtures) addTemplateTypes(actor service.Actor) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.addTemplateTypes",
	})

	result := fixtures.IncomingQueryPort.ListTypes(service.TemplateTypeModel{Session: actor.Session()})
	if err := result.GetServiceError(); err != nil {
		logger.WithError(err).Errorf("unable to list template types")
		return err
	}
	templateTypes := result.TemplateTypes
	if len(templateTypes) == 0 {
		// add fixtures only when there is no template types registered
		logger.Info("adding template types")

		templateTypes, err := fixtures.getTemplateTypeFixtures()
		if err != nil {
			logger.WithError(err).Fatal("unable to get template type fixtures")
		}

		for _, templateType := range templateTypes {
			logger.Infof(`adding template type "%s"`, templateType.Name)

			err := fixtures.IncomingEventPort.TemplateTypeCreateRequestedEvent(service.TemplateTypeModel{
				Session:       actor.Session(),
				Name:          templateType.Name,
				Formats:       templateType.Formats,
				Engine:        templateType.Engine,
				ProviderTypes: templateType.ProviderTypes,
			}, noOpEventSink{})
			if err != nil {
				logger.WithError(err).Errorf("failed to add template type %s", templateType.Name)
			}
		}

		logger.Infof("added %d template types", len(templateTypes))
	} else {
		logger.Infof("there are %d template types already - skip", len(templateTypes))
	}
	return nil
}

func (fixtures *DataFixtures) getTemplateCustomFieldTypeFixtures() ([]types.TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.getTemplateCustomFieldTypeFixtures",
	})

	// read template_type_fixtures.json
	absFixturesFilename, err := filepath.Abs(TemplateCustomFieldTypeFixturesFilename)
	if err != nil {
		errorMessage := fmt.Sprintf("Could not access the template custom field type fixture file %s", TemplateCustomFieldTypeFixturesFilename)
		logger.WithError(err).Errorf(errorMessage)
		return []types.TemplateCustomFieldType{}, fmt.Errorf(errorMessage)
	}

	_, err = os.Stat(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("the template custom field type fixture file (%s) error", absFixturesFilename)
		return []types.TemplateCustomFieldType{}, err
	}

	jsonBytes, err := os.ReadFile(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("Could not read the template custom field type fixture file %s", absFixturesFilename)
		return []types.TemplateCustomFieldType{}, err
	}

	templateCustomFieldTypeFixtures := []types.TemplateCustomFieldType{}

	err = json.Unmarshal(jsonBytes, &templateCustomFieldTypeFixtures)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into []TemplateCustomFieldType"
		logger.WithError(err).Error(errorMessage)
		return []types.TemplateCustomFieldType{}, fmt.Errorf(errorMessage)
	}

	return templateCustomFieldTypeFixtures, nil
}

func (fixtures *DataFixtures) addTemplateCustomFieldTypes(actor service.Actor) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.addTemplateCustomFieldTypes",
	})

	result := fixtures.IncomingQueryPort.ListCustomFieldTypes(service.TemplateCustomFieldTypeModel{
		Session: actor.Session(),
		Name:    "",
	})
	if err := result.Session.GetServiceError(); err != nil {
		logger.WithError(err).Fatal("unable to list template custom field types")
	}

	if len(result.TemplateCustomFieldTypes) == 0 {
		// add fixtures only when there is no template types registered
		logger.Info("adding template custom field types")

		templateCustomFieldTypes, err := fixtures.getTemplateCustomFieldTypeFixtures()
		if err != nil {
			logger.WithError(err).Errorf("unable to get template custom field type fixtures")
			return err
		}

		for _, templateCustomFieldType := range templateCustomFieldTypes {
			logger.Infof(`adding template custom field type "%s"`, templateCustomFieldType.Name)

			//err := fixtures.IncomingEventPort.CreateCustomFieldType(actor, emulator, templateCustomFieldType, transactionID)
			err := fixtures.IncomingEventPort.TemplateCustomFieldTypeCreateRequestedEvent(service.TemplateCustomFieldTypeModel{
				Session:                   actor.Session(),
				Name:                      templateCustomFieldType.Name,
				Description:               templateCustomFieldType.Description,
				QueryMethod:               templateCustomFieldType.QueryMethod,
				QueryTarget:               templateCustomFieldType.QueryTarget,
				QueryData:                 templateCustomFieldType.QueryData,
				QueryResultJSONPathFilter: templateCustomFieldType.QueryResultJSONPathFilter,
			}, noOpEventSink{})
			if err != nil {
				logger.WithError(err).Errorf("failed to add template custom field type %s", templateCustomFieldType.Name)
			}
		}

		logger.Infof("added %d template custom field types", len(templateCustomFieldTypes))
	} else {
		logger.Infof("there are %d template custom field types already - skip", len(result.TemplateCustomFieldTypes))
	}
	return nil
}

type noOpEventSink struct {
}

// VersionDisabled ...
func (e noOpEventSink) VersionDisabled(actor service.Actor, version types.TemplateVersion) {
}

// VersionDisableFailed ...
func (e noOpEventSink) VersionDisableFailed(actor service.Actor, version types.TemplateVersion, versionErr service.CacaoError) {
}

// TypeCreated ...
func (e noOpEventSink) TypeCreated(actor service.Actor, templateType types.TemplateType) {
}

// TypeCreateFailed ...
func (e noOpEventSink) TypeCreateFailed(actor service.Actor, templateType types.TemplateType, creationError service.CacaoError) {
}

// TypeUpdated ...
func (e noOpEventSink) TypeUpdated(actor service.Actor, templateType types.TemplateType) {
}

// TypeUpdateFailed ...
func (e noOpEventSink) TypeUpdateFailed(actor service.Actor, templateType types.TemplateType, updateError service.CacaoError) {
}

// TypeDeleted ...
func (e noOpEventSink) TypeDeleted(actor service.Actor, templateType types.TemplateType) {
}

// TypeDeleteFailed ...
func (e noOpEventSink) TypeDeleteFailed(actor service.Actor, templateType types.TemplateType, deletionError service.CacaoError) {
}

// CustomFieldTypeCreated ...
func (e noOpEventSink) CustomFieldTypeCreated(actor service.Actor, templateCustomFieldType types.TemplateCustomFieldType) {
}

// CustomFieldTypeCreateFailed ...
func (e noOpEventSink) CustomFieldTypeCreateFailed(actor service.Actor, templateCustomFieldType types.TemplateCustomFieldType, creationError service.CacaoError) {
}

// CustomFieldTypeUpdated ...
func (e noOpEventSink) CustomFieldTypeUpdated(actor service.Actor, templateCustomFieldType types.TemplateCustomFieldType) {
}

// CustomFieldTypeUpdateFailed ...
func (e noOpEventSink) CustomFieldTypeUpdateFailed(actor service.Actor, templateCustomFieldType types.TemplateCustomFieldType, updateError service.CacaoError) {
}

// CustomFieldTypeDeleted ...
func (e noOpEventSink) CustomFieldTypeDeleted(actor service.Actor, templateCustomFieldType types.TemplateCustomFieldType) {
}

// CustomFieldTypeDeleteFailed ...
func (e noOpEventSink) CustomFieldTypeDeleteFailed(actor service.Actor, templateCustomFieldType types.TemplateCustomFieldType, deletionError service.CacaoError) {
}

// Imported ...
func (e noOpEventSink) Imported(actor service.Actor, template types.Template) {
}

// ImportFailed ...
func (e noOpEventSink) ImportFailed(actor service.Actor, template types.Template, importError service.CacaoError) {
}

// Updated ...
func (e noOpEventSink) Updated(actor service.Actor, template types.Template) {
}

// UpdateFailed ...
func (e noOpEventSink) UpdateFailed(actor service.Actor, template types.Template, updateError service.CacaoError) {
}

// Deleted ...
func (e noOpEventSink) Deleted(actor service.Actor, template types.Template) {
}

// DeleteFailed ...
func (e noOpEventSink) DeleteFailed(actor service.Actor, template types.Template, deletionError service.CacaoError) {
}

// Synced ...
func (e noOpEventSink) Synced(actor service.Actor, template types.Template) {
}

// SyncFailed ...
func (e noOpEventSink) SyncFailed(actor service.Actor, template types.Template, syncError service.CacaoError) {
}
