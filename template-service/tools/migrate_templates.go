package main

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// this converts v1 templates to v3

func main() {
	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "main",
	})

	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		logger.WithError(err).Fatal("failed to read config from Environment")
	}

	// other empty parameters will be filled with defaults
	config.ProcessDefaults()

	err = convertFromMongo(&config)
	if err != nil {
		logger.WithError(err).Fatal("failed to convert templates stored in MongoDB")
	}
}

func convertFromMongo(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "convertFromMongo",
	})

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Errorf("failed to create MongoDB object store")
		return err
	}

	templates := []types.TemplateUnknown{}

	err = store.List(config.TemplateMongoDBCollectionName, &templates)
	if err != nil {
		logger.WithError(err).Errorf("failed to list templates")
		return err
	}

	logger.Infof("loaded %d templates from MongoDB", len(templates))

	oldTemplates := []types.TemplateV1{}
	for _, template := range templates {
		if template.Metadata == nil {
			logger.Infof("skip template %s as it is v3", template.ID.String())
			continue
		}

		if _, ok := template.Metadata["cacao_pre_tasks"]; ok {
			// has cacao_pre_tasks which is only available in v3
			logger.Infof("skip template %s as it is v3", template.ID.String())
			continue
		}

		if parameters, ok := template.Metadata["parameters"]; ok {
			if pMap, ok2 := parameters.(map[string]interface{}); ok2 {
				if _, ok3 := pMap["name"]; ok3 {
					// has name key in the parameter which is only available in v3
					// v1 has field name as key
					logger.Infof("skip template %s as it is v3", template.ID.String())
					continue
				}
			}
		}

		oldTemplate := types.TemplateV1{}
		err = store.Get(config.TemplateMongoDBCollectionName, template.ID.String(), &oldTemplate)
		if err != nil {
			logger.WithError(err).Errorf("failed to get template %s", template.ID.String())
			return err
		}

		oldTemplates = append(oldTemplates, oldTemplate)
	}

	logger.Infof("found %d v1 templates from MongoDB", len(oldTemplates))

	for _, template := range oldTemplates {
		logger.Infof("converting template %s...", template.ID.String())
		newTemplate := convertOne(template)

		updated, err := store.Update(config.TemplateMongoDBCollectionName, template.ID.String(), newTemplate, []string{"metadata"})
		if err != nil {
			logger.WithError(err).Errorf("failed to update template %s", template.ID.String())
			return err
		}

		if !updated {
			logger.WithError(err).Errorf("failed to update template %s", template.ID.String())
			return fmt.Errorf("failed to update template %s", template.ID.String())
		}
	}

	return nil
}

func convertMetadataPurpose(metadataV1 types.TemplateMetadataV1) (purposeV3 cacao_common_service.TemplatePurpose) {
	switch metadataV1.Purpose {
	case cacao_common_service.TemplatePurposeCompute:
		switch metadataV1.TemplateTypeName {
		case "openstack_terraform":
			return cacao_common_service.TemplatePurposeOpenStackCompute
		case "aws_terraform":
			return cacao_common_service.TemplatePurposeAWSCompute
		default:
			panic("unhandled template type name" + metadataV1.TemplateTypeName)
		}
	case cacao_common_service.TemplatePurposeStorage:
		switch metadataV1.TemplateTypeName {
		case "openstack_terraform":
			return cacao_common_service.TemplatePurposeOpenStackStorage
		case "aws_terraform":
			return cacao_common_service.TemplatePurposeAWSStorage
		default:
			panic("unhandled template type name" + metadataV1.TemplateTypeName)
		}
	case cacao_common_service.TemplatePurposeGeneral:
		return cacao_common_service.TemplatePurposeGeneral
	case "":
		return cacao_common_service.TemplatePurposeGeneral
	default:
		panic("unhandled purpose: " + metadataV1.Purpose)
	}
}

func convertOne(t types.TemplateV1) types.Template {
	template := types.Template{}

	template.ID = t.ID
	template.Owner = t.Owner
	template.Name = t.Name
	template.Description = t.Description
	template.Public = t.Public
	template.Source = cacao_common_service.TemplateSource(t.Source)
	template.CreatedAt = t.CreatedAt
	template.UpdatedAt = t.UpdatedAt

	template.LatestVersionMetadata = cacao_common_service.TemplateMetadata{
		SchemaURL:      "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v3/schema.json",
		SchemaVersion:  "3",
		Name:           t.Metadata.Name,
		Author:         t.Metadata.Author,
		AuthorEmail:    t.Metadata.AuthorEmail,
		Description:    t.Metadata.Description,
		DocURL:         "",
		TemplateType:   t.Metadata.TemplateTypeName,
		Purpose:        convertMetadataPurpose(t.Metadata),
		CacaoPreTasks:  []cacao_common_service.TemplateCacaoTask{},
		CacaoPostTasks: []cacao_common_service.TemplateCacaoTask{},

		Parameters: []cacao_common_service.TemplateParameter{},
		Filters:    []cacao_common_service.TemplateFilter{},
	}

	for _, postTask := range t.Metadata.CacaoPostTasks {
		task := cacao_common_service.TemplateCacaoTask{
			Type:      cacao_common_service.TemplateCacaoTaskType(postTask.Type),
			Location:  postTask.LocationType,
			ExecuteOn: cacao_common_service.TemplateCacaoTaskExecuteOn{},
		}

		template.LatestVersionMetadata.CacaoPostTasks = append(template.LatestVersionMetadata.CacaoPostTasks, task)
	}

	for parameterKey, parameter := range t.Metadata.Parameters {
		if parameterMap, ok := parameter.(map[string]interface{}); ok {
			param := cacao_common_service.TemplateParameter{
				Name:               parameterKey,
				Type:               "",
				Description:        "",
				Default:            parameterMap["default"],
				Enum:               nil,
				DataValidationType: "",
			}

			if t, ok2 := parameterMap["type"]; ok2 {
				param.Type = t.(string)
			}

			if desc, ok2 := parameterMap["description"]; ok2 {
				param.Description = desc.(string)
			}

			if enum, ok2 := parameterMap["enum"]; ok2 {
				switch enum.(type) {
				case []interface{}:
					param.Enum = enum.([]interface{})
				case primitive.A:
					param.Enum = []interface{}(enum.(primitive.A))
				}
			}

			template.LatestVersionMetadata.Parameters = append(template.LatestVersionMetadata.Parameters, param)
		}
	}

	return template
}
