package main

import (
	"encoding/json"
	"testing"

	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"go.mongodb.org/mongo-driver/bson"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestMigrateTemplate(t *testing.T) {
	templateV1Json := `
	{
		"id": "template-cglkl1tjc7kf6frjqtp0",
		"owner": "cacao-admin",
		"name": "single-image",
		"description": "simple launch of one or more vms",
		"public": true,
		"source": {
			"type": "git",
			"uri": "https://gitlab.com/cyverse/cacao-tf-os-ops",
			"access_parameters": { "branch": "main", "path": "single-image" },
			"source_visibility": "public"
		}
		,
		"metadata": {
			"name": "single image openstack instances",
			"author": "Edwin Skidmore",
			"author_email": "edwin@cyverse.org",
			"description": "simple launch of one or more vms",
			"template_type": "openstack_terraform",
			"purpose": "compute",
			"cacao_post_tasks": [ { "type": "ansible", "location_type": "cacao_atmosphere_legacy" } ],
			"parameters": {
				"image": {
					"default": "",
					"type": "cacao_provider_image",
					"field_type": "plain_text",
					"ui_label": "Boot image id",
					"description": "Boot image id"
				},
				"flavor": {
					"field_type": "plain_text",
					"ui_label": "Flavor",
					"description": "Instance type",
					"type": "cacao_provider_flavor"
				},
				"power_state": {
					"description": "Power state",
					"type": "string",
					"default": "active",
					"enum": [ "active", "shutoff", "suspend", "shelved_offloaded" ],
					"field_type": "plain_text",
					"ui_label": "Power state"
				},
				"instance_count": {
					"description": "# of instances",
					"type": "integer",
					"default": 1,
					"field_type": "plain_text",
					"ui_label": "# of Instances"
				},
				"region": {
					"field_type": "plain_text",
					"ui_label": "Openstack region",
					"description": "Openstack region",
					"type": "cacao_provider_region",
					"default": "IU"
				},
				"project": {
					"type": "cacao_provider_project",
					"field_type": "plain_text",
					"ui_label": "Project ID",
					"description": "OpenStack Project ID"
				},
				"keypair": {
					"type": "cacao_provider_key_pair",
					"default": "cacao-ssh-key",
					"field_type": "plain_text",
					"ui_label": "Key-pair",
					"description": "Key-pair for instance access"
				},
				"user_data": {
					"type": "cacao_cloud_init",
					"description": "cloud init script"
				},
				"root_storage_size": {
					"default": -1,
					"type": "integer",
					"field_type": "plain_text",
					"ui_label": "Root storage size",
					"description": "Size of root storage in GB"
				},
				"username": {
					"type": "cacao_username",
					"default": "username",
					"field_type": "plain_text",
					"ui_label": "CACAO username",
					"description": "CACAO username"
				},
				"image_name": {
					"default": "",
					"type": "cacao_provider_image_name",
					"field_type": "plain_text",
					"ui_label": "Boot image name",
					"description": "Boot image name"
				},
				"ip_pool": {
					"default": "external_network_name",
					"field_type": "plain_text",
					"ui_label": "IP pool",
					"description": "IP pool",
					"type": "cacao_provider_external_network"
				},
				"instance_name": {
					"field_type": "plain_text",
					"ui_label": "Instance name",
					"description": "Instance name",
					"type": "string"
				},
				"root_storage_type": {
					"type": "string",
					"default": "local",
					"enum": [ "local", "volume" ],
					"field_type": "plain_text",
					"ui_label": "Root storage type",
					"description": "type of root storage, either local or volume"
				},
				"root_storage_delete_on_termination": {
					"type": "bool",
					"field_type": "plain_text",
					"ui_label": "Root storage delete on termination",
					"description": "if true, will delete the root storage when instance is deleted",
					"default": true
				},
				"root_storage_source": {
					"type": "string",
					"default": "image",
					"enum": [ "image" ],
					"field_type": "plain_text",
					"ui_label": "Root source type",
					"description": "Source of root disk; currently, only image is supported "
				}
			}
		},
		"created_at": "2023-04-03T21:52:07.788Z",
		"updated_at": "2023-04-03T21:52:07.788Z"
	}
	`

	// the migration tool reads from mongo, so convert from json string to bson first
	var tmp interface{}
	err := json.Unmarshal([]byte(templateV1Json), &tmp)
	assert.NoError(t, err)

	registry, err := db.GetBSONRegistry()
	assert.NoError(t, err)

	bsonMarshal, err := bson.MarshalWithRegistry(registry, tmp)
	assert.NoError(t, err)

	var templateV1 types.TemplateV1
	err = bson.UnmarshalWithRegistry(registry, bsonMarshal, &templateV1)
	assert.NoError(t, err)

	templateV3 := convertOne(templateV1)
	assert.Equal(t, templateV1.ID, templateV3.ID)
	assert.Equal(t, templateV1.Owner, templateV3.Owner)
	assert.Equal(t, templateV1.Name, templateV3.Name)
	assert.Equal(t, templateV1.Description, templateV3.Description)
	assert.Equal(t, templateV1.Metadata.Author, templateV3.LatestVersionMetadata.Author)
	assert.Equal(t, templateV1.Metadata.TemplateTypeName, templateV3.LatestVersionMetadata.TemplateType)
	assert.Equal(t, service.TemplatePurpose("openstack_compute"), templateV3.LatestVersionMetadata.Purpose)
	assert.Equal(t, len(templateV1.Metadata.CacaoPostTasks), len(templateV3.LatestVersionMetadata.CacaoPostTasks))
	assert.Equal(t, 0, len(templateV3.LatestVersionMetadata.CacaoPreTasks))
	assert.Equal(t, len(templateV1.Metadata.Parameters), len(templateV3.LatestVersionMetadata.Parameters))
}
