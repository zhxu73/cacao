package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/adapters"
	"gitlab.com/cyverse/cacao/template-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// TemplateBeforeVersion is the schema of template before we added TemplateVersion
type TemplateBeforeVersion struct {
	ID          common.ID                  `bson:"_id" json:"id,omitempty"`
	Owner       string                     `bson:"owner" json:"owner,omitempty"`
	Name        string                     `bson:"name" json:"name,omitempty"`
	Description string                     `bson:"description" json:"description,omitempty"`
	Public      bool                       `bson:"public" json:"public,omitempty"`
	Source      service.TemplateSource     `bson:"source" json:"source,omitempty"`
	Metadata    service.TemplateMetadata   `bson:"metadata" json:"metadata,omitempty"`
	UIMetadata  service.TemplateUIMetadata `bson:"ui_metadata" json:"ui_metadata,omitempty"`
	CreatedAt   time.Time                  `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time                  `bson:"updated_at" json:"updated_at,omitempty"`
}

func processTemplateDocument(rawDoc bson.M, config *types.Config, storage *adapters.MongoAdapter) {
	marshal, err := bson.Marshal(rawDoc)
	if err != nil {
		log.WithError(err).Panicf("fail to marshal bson raw document")
	}
	var oldTemplate TemplateBeforeVersion
	err = bson.Unmarshal(marshal, &oldTemplate)
	if err != nil {
		log.WithError(err).Panicf("fail to unmarshal into old template")
	}

	logger := log.WithFields(log.Fields{"templateID": oldTemplate.ID})
	newTemplate, templateVersion, err := convertToNewTemplateAndVersion(oldTemplate)
	if err != nil {
		logger.WithError(err).Panicf("fail to migrate template")
	}

	versions, err := storage.ListVersions(oldTemplate.Owner, oldTemplate.ID)
	if err != nil {
		logger.WithError(err).Panicf("fail to list template versions")
	}
	if len(versions) == 0 {
		err = storage.Conn.Insert(config.TemplateVersionMongoDBCollectionName, templateVersion)
		if err != nil {
			logger.WithError(err).Panicf("fail to insert template version to storage")
		}
	} else if len(versions) == 1 {
		// if template version is already inserted
		newTemplate.LatestVersionID = versions[0].ID
	} else {
		logger.Panicf("template has more than 1 versions in storage, should not happen during migration")
	}
	replaced, err := storage.Conn.Replace(config.TemplateMongoDBCollectionName, bson.M{"_id": newTemplate.ID}, newTemplate)
	if err != nil {
		logger.WithError(err).Panicf("fail to replace template in storage")
	}
	if !replaced {
		logger.Warnf("fail to replace template %s, no template found in storage", newTemplate.ID)
	}
}

func convertToNewTemplateAndVersion(oldTemplate TemplateBeforeVersion) (types.Template, types.TemplateVersion, error) {
	gitAdapter := adapters.GitAdapter{}
	// assume no private source template
	metadata, uiMetadata, updatedSource, err := gitAdapter.GetTemplateMetadata(oldTemplate.Source, service.TemplateSourceCredential{
		Username: "",
		Password: "",
		SSHKey:   "",
	})
	if err != nil {
		return types.Template{}, types.TemplateVersion{}, err
	}

	versionID := types.NewTemplateVersionID()
	newTemplate := types.Template{
		ID:                      oldTemplate.ID,
		Owner:                   oldTemplate.Owner,
		Name:                    oldTemplate.Name,
		Description:             oldTemplate.Description,
		Public:                  oldTemplate.Public,
		Deleted:                 false,
		LatestVersionID:         versionID,
		Source:                  oldTemplate.Source,
		LatestVersionMetadata:   metadata,
		LatestVersionUIMetadata: uiMetadata,
		CreatedAt:               oldTemplate.CreatedAt,
		UpdatedAt:               oldTemplate.UpdatedAt,
	}
	templateVersion := types.TemplateVersion{
		ID:           versionID,
		TemplateID:   oldTemplate.ID,
		Source:       updatedSource,
		Disabled:     false,
		DisabledAt:   time.Time{},
		CredentialID: "",
		Metadata:     metadata,
		UIMetadata:   uiMetadata,
		CreatedAt:    time.Now().UTC(),
	}
	return newTemplate, templateVersion, nil
}
