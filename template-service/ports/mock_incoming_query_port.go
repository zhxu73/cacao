package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// ListTypesHandler is a handler for ListTypes query
type ListTypesHandler func(actor string, emulator string) ([]types.TemplateType, error)

// ListTypesForProviderTypeHandler is a handler for ListTypesForProviderType query
type ListTypesForProviderTypeHandler func(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error)

// GetTypeHandler is a handler for GetType query
type GetTypeHandler func(actor string, emulator string, typeName cacao_common_service.TemplateTypeName) (types.TemplateType, error)

/////////////////////////////////////////////////////////////////////////
// TemplateSourceType
/////////////////////////////////////////////////////////////////////////

// ListSourceTypesHandler is a handler for ListSourceTypes query
type ListSourceTypesHandler func(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error)

/////////////////////////////////////////////////////////////////////////
// TemplateCustomFieldType
/////////////////////////////////////////////////////////////////////////

// ListCustomFieldTypesHandler is a handler for ListCustomFieldTypes query
type ListCustomFieldTypesHandler func(actor string, emulator string) ([]types.TemplateCustomFieldType, error)

// GetCustomFieldTypeHandler is a handler for GetCustomFieldType query
type GetCustomFieldTypeHandler func(actor string, emulator string, typeName string) (types.TemplateCustomFieldType, error)

// QueryCustomFieldTypeHandler is a handler for QueryCustomFieldType query
type QueryCustomFieldTypeHandler func(actor string, emulator string, typeName string, queryParams map[string]string) (types.TemplateCustomFieldTypeQueryResult, error)

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// ListHandler is a handler for List query
type ListHandler func(actor string, emulator string, includeCacaoReservedPurposes bool) ([]types.Template, error)

// GetHandler is a handler for Get query
type GetHandler func(actor string, emulator string, templateID cacao_common.ID) (types.Template, error)

// MockIncomingQueryPort is a mock implementation of IncomingQueryPort
type MockIncomingQueryPort struct {
	Config *types.Config

	// TemplateType
	ListTypesHandler                ListTypesHandler
	ListTypesForProviderTypeHandler ListTypesForProviderTypeHandler
	GetTypeHandler                  GetTypeHandler

	// TemplateSourceType
	ListSourceTypesHandler ListSourceTypesHandler

	// TemplateCustomFieldType
	ListCustomFieldTypesHandler ListCustomFieldTypesHandler
	GetCustomFieldTypeHandler   GetCustomFieldTypeHandler
	QueryCustomFieldTypeHandler QueryCustomFieldTypeHandler

	// Template
	ListHandler ListHandler
	GetHandler  GetHandler
}

// Init inits the port
func (port *MockIncomingQueryPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingQueryPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingQueryPort) InitChannel(channel chan types.TemplateChannelRequest) {}

// Start starts the port
func (port *MockIncomingQueryPort) Start() {}

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// SetListTypesHandler sets a handler for ListTypes query
func (port *MockIncomingQueryPort) SetListTypesHandler(listTypesHandler ListTypesHandler) {
	port.ListTypesHandler = listTypesHandler
}

// SetListTypesForProviderTypeHandler sets a handler for ListTypesForProviderType query
func (port *MockIncomingQueryPort) SetListTypesForProviderTypeHandler(listTypesForProviderTypeHandler ListTypesForProviderTypeHandler) {
	port.ListTypesForProviderTypeHandler = listTypesForProviderTypeHandler
}

// SetGetTypeHandler sets a handler for GetType query
func (port *MockIncomingQueryPort) SetGetTypeHandler(getTypeHandler GetTypeHandler) {
	port.GetTypeHandler = getTypeHandler
}

// ListTypes lists template types
func (port *MockIncomingQueryPort) ListTypes(actor string, emulator string) ([]types.TemplateType, error) {
	return port.ListTypesHandler(actor, emulator)
}

// ListTypesForProviderType lists template types for the given provider type
func (port *MockIncomingQueryPort) ListTypesForProviderType(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	return port.ListTypesForProviderTypeHandler(actor, emulator, providerType)
}

// GetType returns a template type
func (port *MockIncomingQueryPort) GetType(actor string, emulator string, typeName cacao_common_service.TemplateTypeName) (types.TemplateType, error) {
	return port.GetTypeHandler(actor, emulator, typeName)
}

/////////////////////////////////////////////////////////////////////////
// TemplateSourceType
/////////////////////////////////////////////////////////////////////////

// SetListSourceTypesHandler sets a handler for ListSourceTypes query
func (port *MockIncomingQueryPort) SetListSourceTypesHandler(listSourceTypesHandler ListSourceTypesHandler) {
	port.ListSourceTypesHandler = listSourceTypesHandler
}

// ListSourceTypes lists template types
func (port *MockIncomingQueryPort) ListSourceTypes(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error) {
	return port.ListSourceTypesHandler(actor, emulator)
}

/////////////////////////////////////////////////////////////////////////
// TemplateCustomFieldType
/////////////////////////////////////////////////////////////////////////

// SetListCustomFieldTypesHandler sets a handler for ListCustomFieldTypes query
func (port *MockIncomingQueryPort) SetListCustomFieldTypesHandler(listCustomFieldTypesHandler ListCustomFieldTypesHandler) {
	port.ListCustomFieldTypesHandler = listCustomFieldTypesHandler
}

// SetGetCustomFieldTypeHandler sets a handler for GetCustomFieldType query
func (port *MockIncomingQueryPort) SetGetCustomFieldTypeHandler(getCustomFieldTypeHandler GetCustomFieldTypeHandler) {
	port.GetCustomFieldTypeHandler = getCustomFieldTypeHandler
}

// SetQueryCustomFieldTypeHandler sets a handler for QueryCustomFieldType query
func (port *MockIncomingQueryPort) SetQueryCustomFieldTypeHandler(queryCustomFieldTypeHandler QueryCustomFieldTypeHandler) {
	port.QueryCustomFieldTypeHandler = queryCustomFieldTypeHandler
}

// ListCustomFieldTypes lists template custom field types
func (port *MockIncomingQueryPort) ListCustomFieldTypes(actor string, emulator string) ([]types.TemplateCustomFieldType, error) {
	return port.ListCustomFieldTypesHandler(actor, emulator)
}

// GetCustomFieldType returns a template custom field type
func (port *MockIncomingQueryPort) GetCustomFieldType(actor string, emulator string, typeName string) (types.TemplateCustomFieldType, error) {
	return port.GetCustomFieldTypeHandler(actor, emulator, typeName)
}

// QueryCustomFieldType returns a template custom field type query result
func (port *MockIncomingQueryPort) QueryCustomFieldType(actor string, emulator string, typeName string, queryParams map[string]string) (types.TemplateCustomFieldTypeQueryResult, error) {
	return port.QueryCustomFieldTypeHandler(actor, emulator, typeName, queryParams)
}

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// SetListHandler sets a handler for List query
func (port *MockIncomingQueryPort) SetListHandler(listHandler ListHandler) {
	port.ListHandler = listHandler
}

// SetGetHandler sets a handler for Get query
func (port *MockIncomingQueryPort) SetGetHandler(getHandler GetHandler) {
	port.GetHandler = getHandler
}

// List lists templates
func (port *MockIncomingQueryPort) List(actor string, emulator string, includeCacaoReservedPurposes bool) ([]types.Template, error) {
	return port.ListHandler(actor, emulator, includeCacaoReservedPurposes)
}

// Get returns a template
func (port *MockIncomingQueryPort) Get(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
	return port.GetHandler(actor, emulator, templateID)
}
