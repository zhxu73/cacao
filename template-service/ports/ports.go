package ports

import (
	"context"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
	"sync"
	"time"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config) error
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	Port
	Start(ctx context.Context, wg *sync.WaitGroup, handlers IncomingQueryHandlers) error
}

// IncomingQueryHandlers ...
type IncomingQueryHandlers interface {
	// TemplateType
	ListTypes(request cacao_common_service.TemplateTypeModel) cacao_common_service.TemplateTypeListModel
	ListTypesForProviderType(request cacao_common_service.TemplateTypeModel) cacao_common_service.TemplateTypeListModel
	GetType(getRequest cacao_common_service.TemplateTypeModel) cacao_common_service.TemplateTypeModel

	// TemplateSourceType
	ListSourceTypes(request cacao_common_service.Session) cacao_common_service.TemplateSourceTypeListModel

	// TemplateCustomFieldType
	ListCustomFieldTypes(request cacao_common_service.TemplateCustomFieldTypeModel) cacao_common_service.TemplateCustomFieldTypeListModel
	GetCustomFieldType(request cacao_common_service.TemplateCustomFieldTypeModel) cacao_common_service.TemplateCustomFieldTypeModel
	QueryCustomFieldType(request cacao_common_service.TemplateCustomFieldTypeModel) cacao_common_service.TemplateCustomFieldTypeQueryResultModel

	// Template
	List(request cacao_common_service.TemplateModel) cacao_common_service.TemplateListModel
	Get(request cacao_common_service.TemplateModel) cacao_common_service.TemplateModel
	GetTemplateVersion(request cacao_common_service.TemplateVersionModel) cacao_common_service.TemplateVersionModel
	ListTemplateVersions(request cacao_common_service.TemplateVersionModel) cacao_common_service.TemplateVersionListModel
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	Port
	Start(ctx context.Context, wg *sync.WaitGroup, handlers IncomingEventHandlers) error
}

// IncomingEventHandlers ...
type IncomingEventHandlers interface {
	// TemplateType
	TemplateTypeCreateRequestedEvent(request cacao_common_service.TemplateTypeModel, sink OutgoingEventPort) error
	TemplateTypeUpdateRequestedEvent(request cacao_common_service.TemplateTypeModel, sink OutgoingEventPort) error
	TemplateTypeDeleteRequestedEvent(request cacao_common_service.TemplateTypeModel, sink OutgoingEventPort) error

	// TemplateCustomFieldType
	TemplateCustomFieldTypeCreateRequestedEvent(request cacao_common_service.TemplateCustomFieldTypeModel, sink OutgoingEventPort) error
	TemplateCustomFieldTypeUpdateRequestedEvent(request cacao_common_service.TemplateCustomFieldTypeModel, sink OutgoingEventPort) error
	TemplateCustomFieldTypeDeleteRequestedEvent(request cacao_common_service.TemplateCustomFieldTypeModel, sink OutgoingEventPort) error

	// Template
	TemplateImportRequestedEvent(request cacao_common_service.TemplateModel, sink OutgoingEventPort) error
	TemplateUpdateRequestedEvent(request cacao_common_service.TemplateModel, sink OutgoingEventPort) error
	TemplateDeleteRequestedEvent(request cacao_common_service.TemplateModel, sink OutgoingEventPort) error
	TemplateSyncRequestedEvent(request cacao_common_service.TemplateModel, sink OutgoingEventPort) error

	// TemplateVersion
	TemplateDisableVersionEvent(request cacao_common_service.TemplateVersionDisableRequest, sink OutgoingEventPort) error
}

// OutgoingEventPort ...
type OutgoingEventPort interface {

	// TemplateType
	TypeCreated(actor cacao_common_service.Actor, templateType types.TemplateType)
	TypeCreateFailed(actor cacao_common_service.Actor, templateType types.TemplateType, creationError cacao_common_service.CacaoError)
	TypeUpdated(actor cacao_common_service.Actor, templateType types.TemplateType)
	TypeUpdateFailed(actor cacao_common_service.Actor, templateType types.TemplateType, updateError cacao_common_service.CacaoError)
	TypeDeleted(actor cacao_common_service.Actor, templateType types.TemplateType)
	TypeDeleteFailed(actor cacao_common_service.Actor, templateType types.TemplateType, deletionError cacao_common_service.CacaoError)

	// TemplateCustomFieldType
	CustomFieldTypeCreated(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType)
	CustomFieldTypeCreateFailed(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType, creationError cacao_common_service.CacaoError)
	CustomFieldTypeUpdated(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType)
	CustomFieldTypeUpdateFailed(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType, updateError cacao_common_service.CacaoError)
	CustomFieldTypeDeleted(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType)
	CustomFieldTypeDeleteFailed(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType, deletionError cacao_common_service.CacaoError)

	// Template
	Imported(actor cacao_common_service.Actor, template types.Template)
	ImportFailed(actor cacao_common_service.Actor, template types.Template, importError cacao_common_service.CacaoError)
	Updated(actor cacao_common_service.Actor, template types.Template)
	UpdateFailed(actor cacao_common_service.Actor, template types.Template, updateError cacao_common_service.CacaoError)
	Deleted(actor cacao_common_service.Actor, template types.Template)
	DeleteFailed(actor cacao_common_service.Actor, template types.Template, deletionError cacao_common_service.CacaoError)
	Synced(actor cacao_common_service.Actor, template types.Template)
	SyncFailed(actor cacao_common_service.Actor, template types.Template, syncError cacao_common_service.CacaoError)

	// TemplateVersion
	VersionDisabled(actor cacao_common_service.Actor, version types.TemplateVersion)
	VersionDisableFailed(actor cacao_common_service.Actor, version types.TemplateVersion, versionErr cacao_common_service.CacaoError)
}

// PersistentStoragePort is an interface for a Persistent Storage port.
// Implementation of PersistentStoragePort is also responsible for authorization
// check.
//
// User can only read the template they owned and templates that are public, and
// user can only modify (update/delete/sync) templates that they owned.
type PersistentStoragePort interface {
	Port

	// TemplateType
	ListTypes() ([]types.TemplateType, error)
	ListTypesForProviderType(providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error)
	GetType(templateTypeName cacao_common_service.TemplateTypeName) (types.TemplateType, error)
	CreateType(templateType types.TemplateType) error
	UpdateType(templateType types.TemplateType, updateFieldNames []string) error
	DeleteType(templateTypeName cacao_common_service.TemplateTypeName) error

	// TemplateCustomFieldType
	ListCustomFieldTypes() ([]types.TemplateCustomFieldType, error)
	GetCustomFieldType(customFieldTypeName string) (types.TemplateCustomFieldType, error)
	CreateCustomFieldType(templateCustomFieldType types.TemplateCustomFieldType) error
	UpdateCustomFieldType(templateCustomFieldType types.TemplateCustomFieldType, updateFieldNames []string) error
	DeleteCustomFieldType(customFieldTypeName string) error

	// Template
	List(user string, includeCacaoReservedPurposes bool) ([]types.Template, error)
	Get(user string, templateID cacao_common.ID) (types.Template, error)
	Create(template types.Template) error
	Update(template types.Template, updateFieldNames []string) error
	TemplateSyncStart(templateID cacao_common.ID) (syncLockAcquired bool, err error)
	TemplateSyncFinished(templateID cacao_common.ID) error
	SoftDelete(user string, templateID cacao_common.ID) error

	// TemplateVersion
	ListVersions(user string, templateID cacao_common.ID) ([]types.TemplateVersion, error)
	GetVersion(user string, versionID cacao_common.ID) (types.TemplateVersion, error)
	CreateVersion(version types.TemplateVersion) error
	DisableVersion(versionID cacao_common.ID) (time.Time, error)
}

// TemplateSourcePort is an interface for a template source port.
type TemplateSourcePort interface {
	Port

	GetTemplateMetadata(source cacao_common_service.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (cacao_common_service.TemplateMetadata, cacao_common_service.TemplateUIMetadata, cacao_common_service.TemplateSource, error)
	// Check if 2 source are the identical version of the template.
	// Note this checks if identical, NOT compatible, so same commit hash in the case of Git.
	//IsSameVersion(source1 cacao_common_service.TemplateSource, source2 cacao_common_service.TemplateSource) (bool, error)
}

// TemplateCustomFieldTypeQueryPort is an interface for a template custom field type query port.
type TemplateCustomFieldTypeQueryPort interface {
	Port

	QueryTemplateCustomFieldType(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, queryParams map[string]string) (types.TemplateCustomFieldTypeQueryResult, error)
}

// CredentialPort is an interface for a credential service port.
type CredentialPort interface {
	Port

	GetTemplateSourceCredential(actor string, emulator string, credentialID string) (cacao_common_service.TemplateSourceCredential, error)
}

// TimeSrc is a source of current time.
type TimeSrc interface {
	Now() time.Time
}
