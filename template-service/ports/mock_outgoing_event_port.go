package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/template-service/types"
)

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// TypeCreatedHandler is a handler for TypeCreated event
type TypeCreatedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeCreateFailedHandler is a handler for TypeCreateFailed event
type TypeCreateFailedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeUpdatedHandler is a handler for TypeUpdated event
type TypeUpdatedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeUpdateFailedHandler is a handler for TypeUpdateFailed event
type TypeUpdateFailedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeDeletedHandler is a handler for TypeDeleted event
type TypeDeletedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeDeleteFailedHandler is a handler for TypeDeleteFailed event
type TypeDeleteFailedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

/////////////////////////////////////////////////////////////////////////
// TemplateCustomFieldType
/////////////////////////////////////////////////////////////////////////

// CustomFieldTypeCreatedHandler is a handler for CustomFieldTypeCreated event
type CustomFieldTypeCreatedHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CustomFieldTypeCreateFailedHandler is a handler for CustomFieldTypeCreateFailed event
type CustomFieldTypeCreateFailedHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CustomFieldTypeUpdatedHandler is a handler for CustomFieldTypeUpdated event
type CustomFieldTypeUpdatedHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CustomFieldTypeUpdateFailedHandler is a handler for CustomFieldTypeUpdateFailed event
type CustomFieldTypeUpdateFailedHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CustomFieldTypeDeletedHandler is a handler for CustomFieldTypeDeleted event
type CustomFieldTypeDeletedHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CustomFieldTypeDeleteFailedHandler is a handler for CustomFieldTypeDeleteFailed event
type CustomFieldTypeDeleteFailedHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// ImportedHandler is a handler for Imported event
type ImportedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// ImportFailedHandler is a handler for ImportFailed event
type ImportFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// UpdatedHandler is a handler for Updated event
type UpdatedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// UpdateFailedHandler is a handler for UpdateFailed event
type UpdateFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// DeletedHandler is a handler for Deleted event
type DeletedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// DeleteFailedHandler is a handler for DeleteFailed event
type DeleteFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// SyncedHandler is a handler for Synced event
type SyncedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// SyncFailedHandler is a handler for SyncFailed event
type SyncFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// MockOutgoingEventPort is a mock implementation of OutgoingEventPort
type MockOutgoingEventPort struct {
	Config *types.Config

	// TemplateType
	TypeCreatedHandler      TypeCreatedHandler
	TypeCreateFailedHandler TypeCreateFailedHandler
	TypeUpdatedHandler      TypeUpdatedHandler
	TypeUpdateFailedHandler TypeUpdateFailedHandler
	TypeDeletedHandler      TypeDeletedHandler
	TypeDeleteFailedHandler TypeDeleteFailedHandler

	// TemplateCustomFieldType
	CustomFieldTypeCreatedHandler      CustomFieldTypeCreatedHandler
	CustomFieldTypeCreateFailedHandler CustomFieldTypeCreateFailedHandler
	CustomFieldTypeUpdatedHandler      CustomFieldTypeUpdatedHandler
	CustomFieldTypeUpdateFailedHandler CustomFieldTypeUpdateFailedHandler
	CustomFieldTypeDeletedHandler      CustomFieldTypeDeletedHandler
	CustomFieldTypeDeleteFailedHandler CustomFieldTypeDeleteFailedHandler

	// Template
	ImportedHandler     ImportedHandler
	ImportFailedHandler ImportFailedHandler
	UpdatedHandler      UpdatedHandler
	UpdateFailedHandler UpdateFailedHandler
	DeletedHandler      DeletedHandler
	DeleteFailedHandler DeleteFailedHandler
	SyncedHandler       SyncedHandler
	SyncFailedHandler   SyncFailedHandler
}

// Init inits the port
func (port *MockOutgoingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockOutgoingEventPort) Finalize() {}

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// SetTypeCreatedHandler sets a handler for TypeCreated event
func (port *MockOutgoingEventPort) SetTypeCreatedHandler(typeCreatedHandler TypeCreatedHandler) {
	port.TypeCreatedHandler = typeCreatedHandler
}

// SetTypeCreateFailedHandler sets a handler for TypeCreateFailed event
func (port *MockOutgoingEventPort) SetTypeCreateFailedHandler(typeCreateFailedHandler TypeCreateFailedHandler) {
	port.TypeCreateFailedHandler = typeCreateFailedHandler
}

// SetTypeUpdatedHandler sets a handler for TypeUpdated event
func (port *MockOutgoingEventPort) SetTypeUpdatedHandler(typeUpdatedHandler TypeUpdatedHandler) {
	port.TypeUpdatedHandler = typeUpdatedHandler
}

// SetTypeUpdateFailedHandler sets a handler for TypeUpdateFailed event
func (port *MockOutgoingEventPort) SetTypeUpdateFailedHandler(typeUpdateFailedHandler TypeUpdateFailedHandler) {
	port.TypeUpdateFailedHandler = typeUpdateFailedHandler
}

// SetTypeDeletedHandler sets a handler for TypeDeleted event
func (port *MockOutgoingEventPort) SetTypeDeletedHandler(typeDeletedHandler TypeDeletedHandler) {
	port.TypeDeletedHandler = typeDeletedHandler
}

// SetTypeDeleteFailedHandler sets a handler for TypeDeleteFailed event
func (port *MockOutgoingEventPort) SetTypeDeleteFailedHandler(typeDeleteFailedHandler TypeDeleteFailedHandler) {
	port.TypeDeleteFailedHandler = typeDeleteFailedHandler
}

// TypeCreated creates an event for template type creation
func (port *MockOutgoingEventPort) TypeCreated(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.TypeCreatedHandler(actor, emulator, templateType, status, transactionID)
}

// TypeCreateFailed creates an event for template type creation failure
func (port *MockOutgoingEventPort) TypeCreateFailed(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.TypeCreateFailedHandler(actor, emulator, templateType, status, transactionID)
}

// TypeUpdated creates an event for template type update
func (port *MockOutgoingEventPort) TypeUpdated(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.TypeUpdatedHandler(actor, emulator, templateType, status, transactionID)
}

// TypeUpdateFailed creates an event for template type update failure
func (port *MockOutgoingEventPort) TypeUpdateFailed(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.TypeUpdateFailedHandler(actor, emulator, templateType, status, transactionID)
}

// TypeDeleted creates an event for template type deletion
func (port *MockOutgoingEventPort) TypeDeleted(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.TypeDeletedHandler(actor, emulator, templateType, status, transactionID)
}

// TypeDeleteFailed creates an event for template type deletion failure
func (port *MockOutgoingEventPort) TypeDeleteFailed(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.TypeDeleteFailedHandler(actor, emulator, templateType, status, transactionID)
}

/////////////////////////////////////////////////////////////////////////
// TemplateCustomFieldType
/////////////////////////////////////////////////////////////////////////

// SetCustomFieldTypeCreatedHandler sets a handler for CustomFieldTypeCreated event
func (port *MockOutgoingEventPort) SetCustomFieldTypeCreatedHandler(customFieldTypeCreatedHandler CustomFieldTypeCreatedHandler) {
	port.CustomFieldTypeCreatedHandler = customFieldTypeCreatedHandler
}

// SetCustomFieldTypeCreateFailedHandler sets a handler for CustomFieldTypeCreateFailed event
func (port *MockOutgoingEventPort) SetCustomFieldTypeCreateFailedHandler(customFieldTypeCreateFailedHandler CustomFieldTypeCreateFailedHandler) {
	port.CustomFieldTypeCreateFailedHandler = customFieldTypeCreateFailedHandler
}

// SetCustomFieldTypeUpdatedHandler sets a handler for CustomFieldTypeUpdated event
func (port *MockOutgoingEventPort) SetCustomFieldTypeUpdatedHandler(customFieldTypeUpdatedHandler CustomFieldTypeUpdatedHandler) {
	port.CustomFieldTypeUpdatedHandler = customFieldTypeUpdatedHandler
}

// SetCustomFieldTypeUpdateFailedHandler sets a handler for CustomFieldTypeUpdateFailed event
func (port *MockOutgoingEventPort) SetCustomFieldTypeUpdateFailedHandler(customFieldTypeUpdateFailedHandler CustomFieldTypeUpdateFailedHandler) {
	port.CustomFieldTypeUpdateFailedHandler = customFieldTypeUpdateFailedHandler
}

// SetCustomFieldTypeDeletedHandler sets a handler for CustomFieldTypeDeleted event
func (port *MockOutgoingEventPort) SetCustomFieldTypeDeletedHandler(customFieldTypeDeletedHandler CustomFieldTypeDeletedHandler) {
	port.CustomFieldTypeDeletedHandler = customFieldTypeDeletedHandler
}

// SetCustomFieldTypeDeleteFailedHandler sets a handler for CustomFieldTypeDeleteFailed event
func (port *MockOutgoingEventPort) SetCustomFieldTypeDeleteFailedHandler(customFieldTypeDeleteFailedHandler CustomFieldTypeDeleteFailedHandler) {
	port.CustomFieldTypeDeleteFailedHandler = customFieldTypeDeleteFailedHandler
}

// CustomFieldTypeCreated creates an event for template custom field type creation
func (port *MockOutgoingEventPort) CustomFieldTypeCreated(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CustomFieldTypeCreatedHandler(actor, emulator, templateCustomFieldType, status, transactionID)
}

// CustomFieldTypeCreateFailed creates an event for template custom field type creation failure
func (port *MockOutgoingEventPort) CustomFieldTypeCreateFailed(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CustomFieldTypeCreateFailedHandler(actor, emulator, templateCustomFieldType, status, transactionID)
}

// CustomFieldTypeUpdated creates an event for template custom field type update
func (port *MockOutgoingEventPort) CustomFieldTypeUpdated(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CustomFieldTypeUpdatedHandler(actor, emulator, templateCustomFieldType, status, transactionID)
}

// CustomFieldTypeUpdateFailed creates an event for template custom field type update failure
func (port *MockOutgoingEventPort) CustomFieldTypeUpdateFailed(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CustomFieldTypeUpdateFailedHandler(actor, emulator, templateCustomFieldType, status, transactionID)
}

// CustomFieldTypeDeleted creates an event for template custom field type deletion
func (port *MockOutgoingEventPort) CustomFieldTypeDeleted(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CustomFieldTypeDeletedHandler(actor, emulator, templateCustomFieldType, status, transactionID)
}

// CustomFieldTypeDeleteFailed creates an event for template custom field type deletion failure
func (port *MockOutgoingEventPort) CustomFieldTypeDeleteFailed(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CustomFieldTypeDeleteFailedHandler(actor, emulator, templateCustomFieldType, status, transactionID)
}

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// SetImportedHandler sets a handler for Imported event
func (port *MockOutgoingEventPort) SetImportedHandler(importedHandler ImportedHandler) {
	port.ImportedHandler = importedHandler
}

// SetImportFailedHandler sets a handler for ImportFailed event
func (port *MockOutgoingEventPort) SetImportFailedHandler(importFailedHandler ImportFailedHandler) {
	port.ImportFailedHandler = importFailedHandler
}

// SetUpdatedHandler sets a handler for Updated event
func (port *MockOutgoingEventPort) SetUpdatedHandler(updatedHandler UpdatedHandler) {
	port.UpdatedHandler = updatedHandler
}

// SetUpdateFailedHandler sets a handler for UpdateFailed event
func (port *MockOutgoingEventPort) SetUpdateFailedHandler(updateFailedHandler UpdateFailedHandler) {
	port.UpdateFailedHandler = updateFailedHandler
}

// SetDeletedHandler sets a handler for Deleted event
func (port *MockOutgoingEventPort) SetDeletedHandler(deletedHandler DeletedHandler) {
	port.DeletedHandler = deletedHandler
}

// SetDeleteFailedHandler sets a handler for DeleteFailed event
func (port *MockOutgoingEventPort) SetDeleteFailedHandler(deleteFailedHandler DeleteFailedHandler) {
	port.DeleteFailedHandler = deleteFailedHandler
}

// SetSyncedHandler sets a handler for Synced event
func (port *MockOutgoingEventPort) SetSyncedHandler(syncedHandler SyncedHandler) {
	port.SyncedHandler = syncedHandler
}

// SetSyncFailedHandler sets a handler for SyncFailed event
func (port *MockOutgoingEventPort) SetSyncFailedHandler(syncFailedHandler SyncFailedHandler) {
	port.SyncFailedHandler = syncFailedHandler
}

// Imported creates an event for template import
func (port *MockOutgoingEventPort) Imported(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.ImportedHandler(actor, emulator, template, status, transactionID)
}

// ImportFailed creates an event for template import failure
func (port *MockOutgoingEventPort) ImportFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.ImportFailedHandler(actor, emulator, template, status, transactionID)
}

// Updated creates an event for template update
func (port *MockOutgoingEventPort) Updated(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.UpdatedHandler(actor, emulator, template, status, transactionID)
}

// UpdateFailed creates an event for template update failure
func (port *MockOutgoingEventPort) UpdateFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.UpdateFailedHandler(actor, emulator, template, status, transactionID)
}

// Deleted creates an event for template deletion
func (port *MockOutgoingEventPort) Deleted(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.DeletedHandler(actor, emulator, template, status, transactionID)
}

// DeleteFailed creates an event for template deletion failure
func (port *MockOutgoingEventPort) DeleteFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.DeleteFailedHandler(actor, emulator, template, status, transactionID)
}

// Synced creates an event for template sync succeeded
func (port *MockOutgoingEventPort) Synced(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.SyncedHandler(actor, emulator, template, status, transactionID)
}

// SyncFailed creates an event for template sync failure
func (port *MockOutgoingEventPort) SyncFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.SyncFailedHandler(actor, emulator, template, status, transactionID)
}
