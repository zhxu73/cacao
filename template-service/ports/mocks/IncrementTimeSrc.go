package mocks

import "time"

// IncrementTimeSrc ...
type IncrementTimeSrc struct {
	Start time.Time
	Step  time.Duration
	curr  time.Time
}

// Now ...
func (t *IncrementTimeSrc) Now() time.Time {
	if t.Step <= 0 {
		t.Step = time.Second
	}
	if t.curr.IsZero() {
		t.curr = t.Start
	} else {
		t.curr = t.curr.Add(t.Step)
	}
	return t.curr.UTC()
}
