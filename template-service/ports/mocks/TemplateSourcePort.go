// Code generated by mockery v2.33.3. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"

	service "gitlab.com/cyverse/cacao-common/service"

	types "gitlab.com/cyverse/cacao/template-service/types"
)

// TemplateSourcePort is an autogenerated mock type for the TemplateSourcePort type
type TemplateSourcePort struct {
	mock.Mock
}

// GetTemplateMetadata provides a mock function with given fields: source, credential
func (_m *TemplateSourcePort) GetTemplateMetadata(source service.TemplateSource, credential service.TemplateSourceCredential) (service.TemplateMetadata, service.TemplateUIMetadata, service.TemplateSource, error) {
	ret := _m.Called(source, credential)

	var r0 service.TemplateMetadata
	var r1 service.TemplateUIMetadata
	var r2 service.TemplateSource
	var r3 error
	if rf, ok := ret.Get(0).(func(service.TemplateSource, service.TemplateSourceCredential) (service.TemplateMetadata, service.TemplateUIMetadata, service.TemplateSource, error)); ok {
		return rf(source, credential)
	}
	if rf, ok := ret.Get(0).(func(service.TemplateSource, service.TemplateSourceCredential) service.TemplateMetadata); ok {
		r0 = rf(source, credential)
	} else {
		r0 = ret.Get(0).(service.TemplateMetadata)
	}

	if rf, ok := ret.Get(1).(func(service.TemplateSource, service.TemplateSourceCredential) service.TemplateUIMetadata); ok {
		r1 = rf(source, credential)
	} else {
		r1 = ret.Get(1).(service.TemplateUIMetadata)
	}

	if rf, ok := ret.Get(2).(func(service.TemplateSource, service.TemplateSourceCredential) service.TemplateSource); ok {
		r2 = rf(source, credential)
	} else {
		r2 = ret.Get(2).(service.TemplateSource)
	}

	if rf, ok := ret.Get(3).(func(service.TemplateSource, service.TemplateSourceCredential) error); ok {
		r3 = rf(source, credential)
	} else {
		r3 = ret.Error(3)
	}

	return r0, r1, r2, r3
}

// Init provides a mock function with given fields: config
func (_m *TemplateSourcePort) Init(config *types.Config) error {
	ret := _m.Called(config)

	var r0 error
	if rf, ok := ret.Get(0).(func(*types.Config) error); ok {
		r0 = rf(config)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NewTemplateSourcePort creates a new instance of TemplateSourcePort. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewTemplateSourcePort(t interface {
	mock.TestingT
	Cleanup(func())
}) *TemplateSourcePort {
	mock := &TemplateSourcePort{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
