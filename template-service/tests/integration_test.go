package tests

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"io"
	"os"
	"strings"
	"testing"
	"time"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/adapters"
	"gitlab.com/cyverse/cacao/template-service/domain"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestTemplateService(t *testing.T) {
	if skipIntegrationTest(t) {
		return
	}
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Panic("fail to load conf from env var for testing")
	}
	config.ProcessDefaults()
	config.Override()
	config.MessagingConfig.ClientID = cacao_common.NewID("natsclient").String()
	storageAdapter := &adapters.MongoAdapter{}
	err = storageAdapter.Init(&config)
	if err != nil {
		log.WithError(err).Panic("fail to init DB adapter")
	}
	defer cleanupStorage(storageAdapter)

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Minute*10)
	defer cancelFunc()
	go serviceMain(ctx)
	time.Sleep(time.Second * 5) // wait for service to startup

	natsConn, err := messaging2.NewNatsConnectionFromConfig(config.MessagingConfig)
	if err != nil {
		log.WithError(err).Panic("fail to establish NATS conn for testing")
	}
	defer natsConn.Close()
	stanConn, err := messaging2.NewStanConnectionFromConfig(config.MessagingConfig)
	if err != nil {
		log.WithError(err).Panic("fail to establish STAN conn for testing")
	}
	defer stanConn.Close()
	templateClient, err := service.NewNatsTemplateClientFromConn(&natsConn, &stanConn)
	if err != nil {
		log.WithError(err).Panic("fail to create template service client for testing")
	}

	// start testing the service using service client

	testActor := service.Actor{Actor: "testuser123"}

	templateList, err := templateClient.List(ctx, testActor, false)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, templateList)
	assert.Len(t, templateList, 0)
	templateImport := &service.TemplateModel{
		Public: false,
		Source: service.TemplateSource{
			Type: "git",
			URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops.git",
			AccessParameters: map[string]interface{}{
				"branch": "main",
				"path":   "prerequisite",
			},
			Visibility: "public",
		},
	}

	templateID, err := templateClient.Import(ctx, testActor, templateImport, "")
	if !assert.NoError(t, err) {
		return
	}
	assert.NotEmpty(t, templateID.String())
	fetchedTemplate, err := templateClient.Get(ctx, testActor, templateID)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, templateID, fetchedTemplate.GetID())
	{
		source := fetchedTemplate.GetSource()
		commit := source.AccessParameters["commit"]
		delete(source.AccessParameters, "commit") // remove "commit" for the sake of comparison
		assert.Equal(t, source, templateImport.Source)
		source.AccessParameters["commit"] = commit
	}
	assert.False(t, fetchedTemplate.IsPublic())
	assert.NotEmpty(t, fetchedTemplate.GetMetadata().Name)
	assert.Zero(t, fetchedTemplate.GetUIMetadata()) // prerequisite template does not have UI metadata
	if !assert.NotNil(t, fetchedTemplate.GetSource().Git()) {
		return
	}
	assert.NotEmptyf(t, fetchedTemplate.GetSource().Git().AccessParameters.Commit, "%+v", fetchedTemplate.GetSource().Git().AccessParameters)
	templateVersions, err := templateClient.ListTemplateVersions(ctx, testActor, templateID)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, templateVersions)
	if !assert.Len(t, templateVersions, 1) {
		return
	}
	assert.Equal(t, templateVersions[0].TemplateID, fetchedTemplate.GetID())
	assert.Equal(t, fetchedTemplate.GetLatestVersionID(), templateVersions[0].ID)
	assert.Equal(t, fetchedTemplate.GetMetadata(), templateVersions[0].Metadata)
	assert.Equal(t, fetchedTemplate.GetUIMetadata(), templateVersions[0].UIMetadata)
	assert.Equal(t, fetchedTemplate.GetSource(), templateVersions[0].Source)
	assert.WithinDuration(t, templateVersions[0].CreatedAt, time.Now(), time.Second)
	fetchedVersion, err := templateClient.GetTemplateVersion(ctx, testActor, fetchedTemplate.GetLatestVersionID())
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, fetchedVersion) {
		return
	}
	assert.Equal(t, templateVersions[0], *fetchedVersion)

	err = templateClient.Sync(ctx, testActor, templateID, "")
	if !assert.NoError(t, err) {
		return
	}
	templateVersions, err = templateClient.ListTemplateVersions(ctx, testActor, templateID)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, templateVersions)
	if !assert.Len(t, templateVersions, 1) {
		// Making the assumption that no new commits is pushed to the repo during the run
		// of this integration test, therefore no new version should be created by sync
		// operation
		return
	}
	assert.Equal(t, templateVersions[0], *fetchedVersion)

	err = templateClient.Delete(ctx, testActor, templateID)
	if !assert.NoError(t, err) {
		return
	}
	templateList, err = templateClient.List(ctx, testActor, false)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, templateList)
	assert.Len(t, templateList, 0) // deleted template should not show up in the list
	fetchedTemplateAfterDeletion, err := templateClient.Get(ctx, testActor, templateID)
	// deleted template should still be able to be fetched individually, since it is soft-delete
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, templateID, fetchedTemplateAfterDeletion.GetID())
	assert.Equal(t, fetchedTemplate.GetSource(), fetchedTemplateAfterDeletion.GetSource())
	assert.Equal(t, fetchedTemplate.GetMetadata(), fetchedTemplateAfterDeletion.GetMetadata())
	assert.Equal(t, fetchedTemplate.GetUIMetadata(), fetchedTemplateAfterDeletion.GetUIMetadata())
	err = templateClient.Delete(ctx, testActor, templateID)
	if !assert.NoError(t, err) {
		return
	}

	cancelFunc()
	time.Sleep(time.Second)
}

func serviceMain(rootCtx context.Context) {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()
	config.Override()

	mongoAdapter := &adapters.MongoAdapter{}
	defer logCloserError(mongoAdapter)

	natsConn, err := config.MessagingConfig.ConnectNats()
	if err != nil {
		log.WithError(err).Panic("fail to connect to NATS")
	}
	defer logCloserError(&natsConn)
	stanConn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(config.MessagingConfig)
	if err != nil {
		log.WithError(err).Panic("fail to connect to STAN")
	}
	defer logCloserError(&stanConn)

	svc := domain.Domain{
		Storage:                      mongoAdapter,
		TemplateSource:               &adapters.GitAdapter{},
		TemplateCustomFieldTypeQuery: &adapters.TemplateCustomFieldTypeQueryAdapter{},
		Credential:                   adapters.NewCredentialAdapter(&natsConn),
		QueryIn:                      adapters.NewQueryAdapter(&natsConn),
		EventIn:                      adapters.NewEventAdapter(&stanConn),
	}
	err = svc.Init(&config)
	if err != nil {
		log.WithError(err).Panic("fail to init service")
	}

	serviceCtx, cancelFunc := context.WithCancel(rootCtx)
	defer cancelFunc()
	cacao_common.CancelWhenSignaled(cancelFunc)
	err = svc.Start(serviceCtx)
	if err != nil {
		log.WithError(err).Error("fail to start service")
		return
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error("fail to close")
	}
}

func skipIntegrationTest(t *testing.T) bool {
	if val, ok := os.LookupEnv("MONGO_INTEGRATION_TEST"); !ok || strings.ToLower(val) != "true" {
		t.Skip("skipping integration test with MongoDB, MONGO_INTEGRATION_TEST not set")
		return true
	}
	if val, ok := os.LookupEnv("NATS_INTEGRATION_TEST"); !ok || strings.ToLower(val) != "true" {
		t.Skip("skipping integration test with NATS, NATS_INTEGRATION_TEST not set")
		return true
	}
	return false
}

func cleanupStorageAfterTest(storage ports.PersistentStoragePort) func() {
	ma := storage.(*adapters.MongoAdapter)
	return func() {
		err := ma.Conn.Database.Collection(ma.Config.TemplateMongoDBCollectionName).Drop(context.Background())
		if err != nil {
			log.WithError(err).Panic("fail to drop template collection")
		}
		err = ma.Conn.Database.Collection(ma.Config.TemplateVersionMongoDBCollectionName).Drop(context.Background())
		if err != nil {
			log.WithError(err).Panic("fail to drop version collection")
		}
		err = ma.Close()
		if err != nil {
			log.WithError(err).Panic("fail to close mongo adapter during testing")
		}
	}
}

func cleanupStorage(storage ports.PersistentStoragePort) {
	ma := storage.(*adapters.MongoAdapter)
	err := ma.Conn.Database.Collection(ma.Config.TemplateMongoDBCollectionName).Drop(context.Background())
	if err != nil {
		log.WithError(err).Panic("fail to drop template collection")
	}
	err = ma.Conn.Database.Collection(ma.Config.TemplateVersionMongoDBCollectionName).Drop(context.Background())
	if err != nil {
		log.WithError(err).Panic("fail to drop version collection")
	}
	err = ma.Conn.Database.Collection("template_sync").Drop(context.Background())
	if err != nil {
		log.WithError(err).Panic("fail to drop version collection")
	}
	err = ma.Close()
	if err != nil {
		log.WithError(err).Panic("fail to close mongo adapter during testing")
	}
}
