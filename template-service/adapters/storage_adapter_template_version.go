package adapters

import (
	"context"
	"fmt"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

var templateVersionCollectionSchema = bson.M{
	"$jsonSchema": bson.M{
		"bsonType": "object",
		"required": []string{"_id", "template_id", "source", "disabled", "created_at"},
		"properties": bson.M{
			"_id": bson.M{
				"bsonType":    "string",
				"description": "xid of template version",
				"minLength":   22,
			},
			"template_id": bson.M{
				"bsonType":    "string",
				"description": "xid of template",
				"minLength":   22,
			},
			"source": bson.M{
				"bsonType":             "object",
				"description":          "source of the template, specify where the template is stored and how to download it",
				"additionalProperties": false,
				"required":             []string{"type", "uri", "access_parameters", "source_visibility"},
				"properties": bson.M{
					"type": bson.M{
						"bsonType":    "string",
						"description": "type of the template source (e.g. git)",
						"minLength":   1,
					},
					"uri": bson.M{
						"bsonType":    "string",
						"description": "uri of the source, format depend on the type",
						"minLength":   5,
					},
					"access_parameters": bson.M{
						"bsonType":    "object",
						"description": "additional parameters to specify how to download the template from source",
						"properties": bson.M{
							"branch": bson.M{
								"bsonType": "string",
							},
							"tag": bson.M{
								"bsonType": "string",
							},
							"commit": bson.M{
								"bsonType": "string",
							},
							"path": bson.M{
								"bsonType":  "string",
								"minLength": 1,
							},
						},
					},
					"source_visibility": bson.M{
						"bsonType":    "string",
						"enum":        []string{"public", "private"},
						"description": "whether the template source is public (able to access without any credential) or not",
					},
				},
			},
			"disabled": bson.M{
				"bsonType":    "bool",
				"description": "whether the template version is disabled or not",
			},
			"disabled_at": bson.M{
				"bsonType":    "date",
				"description": "when is the template version disabled",
			},
			"credential_id": bson.M{
				"bsonType":    "string",
				"description": "credential ID to use to download the template from source, if the template source is private",
			},
			"metadata": bson.M{
				"bsonType": "object",
			},
			"ui_metadata": bson.M{
				"bsonType": "object",
			},
			"created_at": bson.M{
				"bsonType":    "date",
				"description": "when is the template version created",
			},
		},
	},
}

func (adapter *MongoAdapter) createVersionCollection(ctx context.Context, config *types.Config) error {
	if config.MongoDBValidationSchema {
		err := adapter.createCollectionWithSchema(ctx, adapter.Config.TemplateVersionMongoDBCollectionName, templateVersionCollectionSchema)
		if err != nil {
			return err
		}
	}
	models := []mongo.IndexModel{
		{
			Keys: bson.M{
				"template_id": 1, // ascending order
			},
			Options: options.Index().SetName("template_id"),
		},
	}
	_, err := adapter.Conn.Database.Collection(adapter.Config.TemplateVersionMongoDBCollectionName).Indexes().CreateMany(ctx, models)
	if err != nil {
		return err
	}
	return nil
}

// ListVersions returns all template versions
func (adapter *MongoAdapter) ListVersions(user string, templateID cacao_common.ID) ([]types.TemplateVersion, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.ListVersions",
	})

	template, err := adapter.Get(user, templateID)
	if err != nil {
		return nil, err
	}

	results := []types.TemplateVersion{}

	filter := map[string]interface{}{
		"template_id": template.ID.String(),
	}

	err = adapter.Store.ListConditional(adapter.Config.TemplateVersionMongoDBCollectionName, filter, &results)
	if err != nil {
		errorMessage := "unable to list template versions"
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// GetVersion returns the template version with the ID
func (adapter *MongoAdapter) GetVersion(user string, versionID cacao_common.ID) (types.TemplateVersion, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.GetVersion",
	})

	result := types.TemplateVersion{}

	err := adapter.Store.Get(adapter.Config.TemplateVersionMongoDBCollectionName, versionID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template version for ID %s", versionID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return result, nil
}

// MockGetVersion sets expected results for GetVersion
func (adapter *MongoAdapter) MockGetVersion(templateTypeName cacao_common_service.TemplateTypeName, expectedTemplateType types.TemplateVersion, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateVersionMongoDBCollectionName, string(templateTypeName)).Return(expectedTemplateType, expectedError)
	return nil
}

// CreateVersion inserts a template version
func (adapter *MongoAdapter) CreateVersion(version types.TemplateVersion) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.CreateVersion",
	})

	if version.CreatedAt.IsZero() {
		version.CreatedAt = time.Now().UTC()
	}
	err := adapter.Store.Insert(adapter.Config.TemplateVersionMongoDBCollectionName, version)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a template version because ID %s (%s) conflicts", version.ID, version.TemplateID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a template version with ID %s (%s)", version.ID, version.TemplateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreateVersion sets expected results for CreateVersion
func (adapter *MongoAdapter) MockCreateVersion(version types.TemplateVersion, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateVersionMongoDBCollectionName).Return(expectedError)
	return nil
}

// DisableVersion disables a template version.
func (adapter *MongoAdapter) DisableVersion(versionID cacao_common.ID) (time.Time, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.DisableVersion",
	})
	now := time.Now().UTC()
	filter := bson.M{
		"_id":      versionID.String(),
		"disabled": false,
	}
	update := bson.M{
		"$set": bson.M{
			"disabled":    true,
			"disabled_at": now,
		},
	}
	updated, err := adapter.Conn.Update(adapter.Config.TemplateVersionMongoDBCollectionName, filter, update)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to disable template version %s", versionID)
		logger.WithError(err).Error(errorMessage)
		return time.Time{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		// if no document is updated, this means either versionID does not exist or it is
		// already disabled. we will fetch again to check which cases this is.
		var version types.TemplateVersion
		err = adapter.Store.Get(adapter.Config.TemplateVersionMongoDBCollectionName, versionID.String(), &version)
		if err != nil {
			errorMessage := fmt.Sprintf("unable to disable template version %s", versionID)
			logger.Error(errorMessage)
			return time.Time{}, cacao_common_service.NewCacaoNotFoundError(errorMessage)
		}
		if version.DisabledAt.IsZero() {
			// this is highly unlikely to happen.
			return now.UTC(), nil
		}
		return version.DisabledAt.UTC(), nil
	}

	return now.UTC(), nil
}

// DeleteVersion permanently deletes a template version. This permanently deletes
// the version, there will be no recover after this except recover from backup.
func (adapter *MongoAdapter) DeleteVersion(versionID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.DeleteVersion",
	})

	deleted, err := adapter.Store.Delete(adapter.Config.TemplateVersionMongoDBCollectionName, versionID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the template version %s", versionID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the template version %s", versionID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return nil
}

// MockDeleteVersion sets expected results for DeleteVersion
func (adapter *MongoAdapter) MockDeleteVersion(templateTypeName cacao_common_service.TemplateTypeName, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Delete", adapter.Config.TemplateVersionMongoDBCollectionName, string(templateTypeName)).Return(expectedResult, expectedError)
	return nil
}

func (adapter *MongoAdapter) createCollectionWithSchema(ctx context.Context, collectionName string, schema bson.M) error {
	if _, ok := schema["$jsonSchema"]; !ok {
		return fmt.Errorf("'$jsonSchema' key is missing from the validation schema object")
	}
	if len(schema) > 1 {
		return fmt.Errorf("validation schema object should only have '$jsonSchema' key")
	}
	collections, err := adapter.Conn.Database.ListCollections(ctx, bson.M{"name": collectionName})
	if err != nil {
		return err
	}
	defer collections.Close(context.Background())
	if collections.Next(context.Background()) {
		// if the collection exists, check existing documents in collection and then update the schema.

		// find documents that does not match the new schema, https://www.mongodb.com/docs/manual/core/schema-validation/use-json-schema-query-conditions/#find-documents-that-don-t-match-the-schema
		findResult, err := adapter.Conn.Database.Collection(collectionName).Find(ctx, bson.M{"$nor": bson.A{schema}}, options.Find().SetProjection(bson.M{"_id": 1}))
		if err != nil {
			return err
		}
		var noMatchList []struct {
			ID string `bson:"_id"`
		}
		err = findResult.All(ctx, &noMatchList)
		if err != nil {
			return err
		}
		if len(noMatchList) > 0 {
			return fmt.Errorf("there are documents in the collection that does not match the new schema, fix them before update the validation schema, IDs: %v", noMatchList)
		}
		// set the validator with "collMod" command
		cmd := bson.D{
			{"collMod", collectionName},
			{"validator", schema},
			{"validationLevel", "strict"},
			{"validationAction", "error"},
		}
		cmdResult := adapter.Conn.Database.RunCommand(context.Background(), cmd)
		if err := cmdResult.Err(); err != nil {
			return err
		}
		return nil
	}

	collectionOpts := options.CreateCollection().SetValidationAction("error").SetValidationLevel("strict").SetValidator(schema)
	err = adapter.Conn.Database.CreateCollection(ctx, collectionName, collectionOpts)
	if err != nil {
		return err
	}
	return nil
}
