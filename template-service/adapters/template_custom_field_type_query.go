package adapters

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/PaesslerAG/jsonpath"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

const (
	paramRegex string = "\\$\\{\\w+\\}"
)

// TemplateCustomFieldTypeQueryAdapter implements TemplateCustomFieldTypeQueryPort
type TemplateCustomFieldTypeQueryAdapter struct {
	Config    *types.Config
	queryConn messaging2.QueryConnection

	ParamRegex *regexp.Regexp
}

// Init initialize template custom field type query adapter
func (adapter *TemplateCustomFieldTypeQueryAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "TemplateCustomFieldTypeQueryAdapter.Init",
	})

	logger.Info("initializing TemplateCustomFieldTypeQueryAdapter")

	adapter.Config = config
	adapter.ParamRegex = regexp.MustCompile(paramRegex)
	return nil
}

// Finalize finalizes credential adapter
func (adapter *TemplateCustomFieldTypeQueryAdapter) Finalize() {
}

// QueryTemplateCustomFieldType returns template custom field type query result
func (adapter *TemplateCustomFieldTypeQueryAdapter) QueryTemplateCustomFieldType(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, queryParams map[string]string) (types.TemplateCustomFieldTypeQueryResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "TemplateCustomFieldTypeQueryAdapter.QueryTemplateCustomFieldType",
	})

	queryParams["actor"] = actor
	queryParams["emulator"] = emulator

	target, data, err := adapter.fillParams(templateCustomFieldType.QueryTarget, templateCustomFieldType.QueryData, queryParams)
	if err != nil {
		errorMessage := "unable to create a query using given parameters"
		logger.WithError(err).Error(errorMessage)
		return types.TemplateCustomFieldTypeQueryResult{}, err
	}

	// query
	switch templateCustomFieldType.QueryMethod {
	case cacao_common_service.TemplateCustomFieldTypeQueryMethodREST:
		logger.Infof("Querying to REST Endpoint '%s' - data = '%s'", target, data)

		authToken := queryParams["auth_token"]
		dataType, value, err := adapter.queryToREST(target, data, authToken, templateCustomFieldType.QueryResultJSONPathFilter)
		if err != nil {
			errorMessage := "unable to make query to REST endpoint"
			logger.WithError(err).Error(errorMessage)
			return types.TemplateCustomFieldTypeQueryResult{}, err
		}

		return types.TemplateCustomFieldTypeQueryResult{
			Name:     templateCustomFieldType.Name,
			DataType: dataType,
			Value:    value,
		}, nil
	case cacao_common_service.TemplateCustomFieldTypeQueryMethodNATS:
		logger.Infof("Querying to NATS message channel '%s' - data = '%s'", target, data)

		dataType, value, err := adapter.queryToNATS(target, data, templateCustomFieldType.QueryResultJSONPathFilter)
		if err != nil {
			errorMessage := "unable to make query to NATS message channel"
			logger.WithError(err).Error(errorMessage)
			return types.TemplateCustomFieldTypeQueryResult{}, err
		}

		return types.TemplateCustomFieldTypeQueryResult{
			Name:     templateCustomFieldType.Name,
			DataType: dataType,
			Value:    value,
		}, nil
	default:
		errorMessage := fmt.Sprintf("unable to handle a query via %s", templateCustomFieldType.QueryMethod.String())
		logger.Error(errorMessage)
		return types.TemplateCustomFieldTypeQueryResult{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}
}

func (adapter *TemplateCustomFieldTypeQueryAdapter) queryToREST(target string, data string, authToken string, jsonpathFilter string) (string, interface{}, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "TemplateCustomFieldTypeQueryAdapter.queryToREST",
	})

	targetURL := target
	if strings.HasPrefix(targetURL, "/") {
		if strings.HasSuffix(adapter.Config.CacaoAPIURL, "/") {
			targetURL = adapter.Config.CacaoAPIURL + target[1:]
		} else {
			targetURL = adapter.Config.CacaoAPIURL + target
		}
	}

	req, err := http.NewRequest("GET", targetURL, nil)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to create a HTTP request with targetURL %s", targetURL)
		logger.WithError(err).Error(errorMessage)
		return "", nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	if len(data) > 0 {
		req.Body = ioutil.NopCloser(strings.NewReader(data))
	}

	if len(authToken) > 0 {
		req.Header.Add("Authorization", authToken)
	}

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to make a HTTP request with targetURL %s", targetURL)
		logger.WithError(err).Error(errorMessage)
		return "", nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	// received result
	if resp.StatusCode < 300 {
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			errorMessage := fmt.Sprintf("unable to read a HTTP response from targetURL %s", targetURL)
			logger.WithError(err).Error(errorMessage)
			return "", nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
		}

		dataType, value, err := adapter.parseDataTypeAndValue(respBody, jsonpathFilter)
		if err != nil {
			logger.Error(err)
			return "", nil, err
		}

		return dataType, value, nil
	}

	errorMessage := fmt.Sprintf("received HTTP error response from targetURL %s, code %d", targetURL, resp.StatusCode)
	logger.Error(errorMessage)
	return "", nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
}

func (adapter *TemplateCustomFieldTypeQueryAdapter) queryToNATS(target string, data string, jsonpathFilter string) (string, interface{}, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters.adapters",
		"function": "TemplateCustomFieldTypeQueryAdapter.queryToNATS",
	})

	timeout := time.Duration(adapter.Config.MessagingConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(context.TODO(), timeout)
	defer cancel()

	var dataObj map[string]interface{}
	err := json.Unmarshal([]byte(data), &dataObj)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to map object"
		logger.WithError(err).Error(errorMessage)
		return "", nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	requestCe, err := messaging2.CreateCloudEventWithAutoSource(dataObj, cacao_common.QueryOp(target))
	if err != nil {
		return "", nil, err
	}
	replyCe, err := adapter.queryConn.Request(timeoutCtx, requestCe)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to request to NATS - %s", target)
		logger.WithError(err).Error(errorMessage)
		return "", nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	var jsonFieldMap map[string]interface{}
	err = json.Unmarshal(replyCe.Data(), &jsonFieldMap)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to map object"
		logger.WithError(err).Error(errorMessage)
		return "", nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	if svcErr, ok := jsonFieldMap["service_error"]; ok {
		// error
		svcErrJSON, err := json.Marshal(svcErr)
		if err != nil {
			errorMessage := "unable to marshal service error object to JSON"
			logger.WithError(err).Error(errorMessage)
			return "", nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
		}

		var cacaoError cacao_common_service.CacaoErrorBase
		err = json.Unmarshal(svcErrJSON, &cacaoError)
		if err != nil {
			errorMessage := "unable to unmarshal service error JSON to object"
			logger.WithError(err).Error(errorMessage)
			return "", nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
		}

		if len(cacaoError.Error()) > 0 {
			errorMessage := fmt.Sprintf("received a cacao service error - %s", cacaoError.Error())
			logger.Error(errorMessage)
			return "", nil, &cacaoError
		}
	}

	if errorTypeVal, ok := jsonFieldMap["error_type"]; ok {
		if len(errorTypeVal.(string)) > 0 {
			errorMessage := fmt.Sprintf("received a service error - %s", errorTypeVal.(string))
			logger.Error(errorMessage)
			return "", nil, cacao_common_service.NewCacaoGeneralError(errorTypeVal.(string))
		}
	}

	if errorMessageVal, ok := jsonFieldMap["error_message"]; ok {
		if len(errorMessageVal.(string)) > 0 {
			errorMessage := fmt.Sprintf("received a service error - %s", errorMessageVal.(string))
			logger.Error(errorMessage)
			return "", nil, cacao_common_service.NewCacaoGeneralError(errorMessageVal.(string))
		}
	}

	dataType, value, err := adapter.parseDataTypeAndValue(replyCe.Data(), jsonpathFilter)
	if err != nil {
		logger.Error(err)
		return "", nil, err
	}

	return dataType, value, nil
}

func (adapter *TemplateCustomFieldTypeQueryAdapter) fillParams(target string, data string, queryParams map[string]string) (string, string, error) {
	varsInTarget := adapter.ParamRegex.FindAllString(target, -1)
	varsInData := adapter.ParamRegex.FindAllString(data, -1)

	// check
	for _, varInTarget := range varsInTarget {
		varName, err := adapter.getParamName(varInTarget)
		if err != nil {
			return "", "", err
		}

		if _, ok := queryParams[varName]; !ok {
			errorMessage := fmt.Sprintf("query parameter %s is not provided", varName)
			return "", "", cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		}
	}

	for _, varInData := range varsInData {
		varName, err := adapter.getParamName(varInData)
		if err != nil {
			return "", "", err
		}

		if _, ok := queryParams[varName]; !ok {
			errorMessage := fmt.Sprintf("query parameter %s is not provided", varName)
			return "", "", cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		}
	}

	// replace
	replaceFunc := func(match string) string {
		varName, err := adapter.getParamName(match)
		if err != nil {
			return match
		}

		val := queryParams[varName]
		return val
	}

	newTarget := adapter.ParamRegex.ReplaceAllStringFunc(target, replaceFunc)
	newData := adapter.ParamRegex.ReplaceAllStringFunc(data, replaceFunc)

	return newTarget, newData, nil
}

// convert "${varname}" to "varname"
func (adapter *TemplateCustomFieldTypeQueryAdapter) getParamName(paramName string) (string, error) {
	if strings.HasPrefix(paramName, "${") && strings.HasSuffix(paramName, "}") {
		return paramName[2 : len(paramName)-1], nil
	} else if strings.HasPrefix(paramName, "{") && strings.HasSuffix(paramName, "}") {
		return paramName[1 : len(paramName)-1], nil
	} else if strings.HasPrefix(paramName, "${") || strings.HasPrefix(paramName, "{") || strings.HasSuffix(paramName, "}") {
		errorMessage := fmt.Sprintf("broken parameter name '%s'", paramName)
		return "", cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
	} else if strings.ContainsAny(paramName, "{}") {
		errorMessage := fmt.Sprintf("broken parameter name '%s'", paramName)
		return "", cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
	} else {
		return paramName, nil
	}
}

func (adapter *TemplateCustomFieldTypeQueryAdapter) parseDataTypeAndValue(body []byte, jsonpathFilter string) (string, interface{}, error) {
	// Determine if response contains Object or Array
	x := bytes.TrimLeft(body, " \t\r\n")
	isJSONObject := len(x) > 0 && x[0] == '{'

	if len(body) > 0 {
		if isJSONObject && len(jsonpathFilter) > 0 {
			// json object
			v := interface{}(nil)
			err := json.Unmarshal(x, &v)
			if err != nil {
				errorMessage := fmt.Sprintf("unable to unmarshal JSON response to interface{} - %s", string(x))
				return "", nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
			}

			filteredValues, err := jsonpath.Get(jsonpathFilter, v)
			if err != nil {
				errorMessage := fmt.Sprintf("unable to perform jsonpath to JSON response - %s", jsonpathFilter)
				return "", nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
			}

			// TODO: currently data type is always string
			resultsFiltered := []string{}

			for _, filteredValue := range filteredValues.([]interface{}) {
				resultsFiltered = append(resultsFiltered, filteredValue.(string))
			}
			return "slice/string", resultsFiltered, nil
		}

		return "string", string(x), nil
	}

	return "string", "", nil
}
