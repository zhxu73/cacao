package adapters

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testIncludeCacaoReservedPurposes := true

	expectedResults := []types.Template{
		{
			ID:          "0001",
			Owner:       testUser,
			Name:        "test_template1",
			Description: "test_template_description1",
			Public:      true,
			Source: cacao_common_service.TemplateSource{
				Type: cacao_common_service.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-single-image",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			LatestVersionMetadata: cacao_common_service.TemplateMetadata{
				Name:         "single image openstack instances",
				Author:       "Edwin Skidmore",
				AuthorEmail:  "edwin@cyverse.org",
				Description:  "launches a single image on a single cloud",
				TemplateType: "openstack_terraform",
				Purpose:      cacao_common_service.TemplatePurposeCompute,
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
		{
			ID:          "0002",
			Owner:       testUser,
			Name:        "test_template2",
			Description: "test_template_description2",
			Public:      false,
			Source: cacao_common_service.TemplateSource{
				Type: cacao_common_service.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-single-image",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			LatestVersionMetadata: cacao_common_service.TemplateMetadata{
				Name:         "single image openstack instances",
				Author:       "Edwin Skidmore",
				AuthorEmail:  "edwin@cyverse.org",
				Description:  "launches a single image on a single cloud",
				TemplateType: "openstack_terraform",
				Purpose:      cacao_common_service.TemplatePurposeCompute,
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
		{
			ID:          "0003",
			Owner:       testUser,
			Name:        "test_template3",
			Description: "test_template_description3",
			Public:      false,
			Source: cacao_common_service.TemplateSource{
				Type: cacao_common_service.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-storage",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			LatestVersionMetadata: cacao_common_service.TemplateMetadata{
				Name:         "single image openstack instances",
				Author:       "Edwin Skidmore",
				AuthorEmail:  "edwin@cyverse.org",
				Description:  "launches a single image on a single cloud",
				TemplateType: "openstack_terraform",
				Purpose:      cacao_common_service.TemplatePurposeStorage,
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
	}
	err := mongoAdapter.MockList(testUser, testIncludeCacaoReservedPurposes, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.List(testUser, testIncludeCacaoReservedPurposes)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	_ = mongoAdapter.Close()
}
func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	expectedResult := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: cacao_common_service.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		LatestVersionMetadata: cacao_common_service.TemplateMetadata{
			Name:         "single image openstack instances",
			Author:       "Edwin Skidmore",
			AuthorEmail:  "edwin@cyverse.org",
			Description:  "launches a single image on a single cloud",
			TemplateType: "openstack_terraform",
			Purpose:      cacao_common_service.TemplatePurposeCompute,
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}
	err := mongoAdapter.MockGet(testUser, testTemplateID, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.Get(testUser, testTemplateID)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	testTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: cacao_common_service.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		LatestVersionMetadata: cacao_common_service.TemplateMetadata{
			Name:         "single image openstack instances",
			Author:       "Edwin Skidmore",
			AuthorEmail:  "edwin@cyverse.org",
			Description:  "launches a single image on a single cloud",
			TemplateType: "openstack_terraform",
			Purpose:      cacao_common_service.TemplatePurposeCompute,
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	err := mongoAdapter.MockCreate(testTemplate, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Create(testTemplate)
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	testExistingTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: cacao_common_service.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		LatestVersionMetadata: cacao_common_service.TemplateMetadata{
			Name:         "single image openstack instances",
			Author:       "Edwin Skidmore",
			AuthorEmail:  "edwin@cyverse.org",
			Description:  "launches a single image on a single cloud",
			TemplateType: "openstack_terraform",
			Purpose:      cacao_common_service.TemplatePurposeCompute,
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	testUpdateTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description2",
		Public:      false,
	}

	err := mongoAdapter.MockUpdate(testExistingTemplate, testUpdateTemplate, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Update(testUpdateTemplate, []string{"description", "public"})
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterDelete(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	testTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: cacao_common_service.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		LatestVersionMetadata: cacao_common_service.TemplateMetadata{
			Name:         "single image openstack instances",
			Author:       "Edwin Skidmore",
			AuthorEmail:  "edwin@cyverse.org",
			Description:  "launches a single image on a single cloud",
			TemplateType: "openstack_terraform",
			Purpose:      cacao_common_service.TemplatePurposeCompute,
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	err := mongoAdapter.MockDelete(testUser, testTemplateID, testTemplate, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Delete(testUser, testTemplateID)
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}
