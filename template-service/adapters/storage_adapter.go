package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/template-service/types"
	"time"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  cacao_common_db.ObjectStore
	Conn   *cacao_common_db.MongoDBConnection
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Error("unable to connect to MongoDB")
		return err
	}

	adapter.Store = store
	adapter.Conn = store.Connection

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
	defer cancelFunc()
	err = adapter.createTemplateCollection(ctx, config)
	if err != nil {
		logger.WithError(err).Error("unable to create template collection in MongoDB")
		return err
	}
	err = adapter.createVersionCollection(ctx, config)
	if err != nil {
		logger.WithError(err).Error("unable to create version collection in MongoDB")
		return err
	}
	return nil
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Close mongodb adapter
func (adapter *MongoAdapter) Close() error {
	err := adapter.Store.Release()
	if err != nil {
		log.WithError(err).Error("fail to close mongo adapter")
		return err
	}

	adapter.Store = nil
	return nil
}
