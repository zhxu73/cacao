package adapters

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
	"os"
	"strings"
	"sync"
	"testing"
	"time"
)

// Require the adapter to implement additional methods for the purpose of integration testing
type storageInterface interface {
	ports.PersistentStoragePort
	Delete(user string, templateID cacao_common.ID) error
	DeleteVersion(versionID cacao_common.ID) error
}

func TestPersistentStoragePort(t *testing.T) {
	if skipMongoIntegrationTest(t) {
		return
	}

	t.Run("template_basic", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		templateID := cacao_common.NewID("template")
		username := "testuser123"
		//now := time.Now().UTC()

		templateList, err := storage.List(username, false)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, templateList)
		assert.Len(t, templateList, 0)

		err = storage.Create(types.Template{
			ID:              templateID,
			Owner:           username,
			Name:            "foobar",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   service.TemplateMetadata{},
			LatestVersionUIMetadata: service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}
		fetchedTemplate, err := storage.Get(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, templateID, fetchedTemplate.ID)
		assert.Equal(t, username, fetchedTemplate.Owner)
		assert.False(t, fetchedTemplate.Deleted)
		templateList, err = storage.List(username, false)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, templateList)
		assert.Len(t, templateList, 1)

		templateUpdate := fetchedTemplate
		templateUpdate.Name = "updated-name-123"
		err = storage.Update(templateUpdate, []string{"name"})
		if !assert.NoError(t, err) {
			return
		}
		fetchedTemplate, err = storage.Get(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, templateID, fetchedTemplate.ID)
		assert.Equal(t, username, fetchedTemplate.Owner)
		assert.False(t, fetchedTemplate.Deleted)
		assert.Equal(t, templateUpdate.Name, fetchedTemplate.Name)

		err = storage.SoftDelete(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		fetchedTemplate, err = storage.Get(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, templateID, fetchedTemplate.ID)
		assert.Equal(t, username, fetchedTemplate.Owner)
		assert.True(t, fetchedTemplate.Deleted)
		templateList, err = storage.List(username, false)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, templateList)
		assert.Len(t, templateList, 0) // list should not return soft-deleted templates

		err = storage.Delete(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		fetchedTemplate, err = storage.Get(username, templateID)
		if !assert.Error(t, err) {
			return
		}
		assert.Zero(t, fetchedTemplate)
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("template_get_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		template, err := storage.Get("testuser123", cacao_common.NewID("template"))
		if !assert.Error(t, err) {
			return
		}
		assert.Zero(t, template)
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("template_get_others_private_template", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		templateID := cacao_common.NewID("template")
		username := "testuser123"
		err := storage.Create(types.Template{
			ID:              templateID,
			Owner:           username,
			Name:            "foobar",
			Description:     "",
			Public:          false, // private template
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   service.TemplateMetadata{},
			LatestVersionUIMetadata: service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		_, err = storage.Get("different_user123", templateID)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("template_update_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		err := storage.Update(types.Template{ID: cacao_common.NewID("template"), Name: "foobar"}, []string{"name"})
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("template_soft_delete_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		err := storage.SoftDelete("testuser123", cacao_common.NewID("template"))
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("template_sync_lock", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		templateID := cacao_common.NewID("template")
		username := "testuser123"
		err := storage.Create(types.Template{
			ID:              templateID,
			Owner:           username,
			Name:            "foobar",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   service.TemplateMetadata{},
			LatestVersionUIMetadata: service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		syncLocked, err := storage.TemplateSyncStart(templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.True(t, syncLocked)
		syncLocked2ndTime, err := storage.TemplateSyncStart(templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.False(t, syncLocked2ndTime)
		err = storage.TemplateSyncFinished(templateID)
		if !assert.NoError(t, err) {
			return
		}
		syncLocked3rdTime, err := storage.TemplateSyncStart(templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.True(t, syncLocked3rdTime)
		err = storage.TemplateSyncFinished(templateID)
		if !assert.NoError(t, err) {
			return
		}
	})
	t.Run("template_sync_lock_concurrent_race", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		templateID := cacao_common.NewID("template")
		username := "testuser123"
		err := storage.Create(types.Template{
			ID:              templateID,
			Owner:           username,
			Name:            "foobar",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   service.TemplateMetadata{},
			LatestVersionUIMetadata: service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		// spawn many go-routines race to lock the same template
		var wg sync.WaitGroup
		const N = 1000
		var result = make([]bool, N)
		for i := 0; i < N; i++ {
			wg.Add(1)
			index := i
			go func() {
				defer wg.Done()
				locked, err := storage.TemplateSyncStart(templateID)
				if !assert.NoError(t, err) {
					return
				}
				result[index] = locked
			}()
		}
		wg.Wait()
		count := 0
		for i := 0; i < N; i++ {
			if result[i] {
				count++
			}
		}
		assert.Equalf(t, 1, count, "there should be only 1 func call that gets the lock")
	})

	t.Run("version_basic", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		templateID := cacao_common.NewID("template")
		username := "testuser123"

		// insert a placeholder template, does not matter the value of the template, as
		// long as the template ID exists in DB. we are creating this to make ListVersions work.
		err := storage.Create(types.Template{
			ID:              templateID,
			Owner:           username,
			Name:            "foobar",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   service.TemplateMetadata{},
			LatestVersionUIMetadata: service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		versions, err := storage.ListVersions(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, versions)
		assert.Empty(t, versions)

		versionID := types.NewTemplateVersionID()
		now := time.Now()
		versionToBeCreated := types.TemplateVersion{
			ID:         versionID,
			TemplateID: templateID,
			Source: service.TemplateSource{
				Type: service.TemplateSourceTypeGit,
				URI:  "gitlab.com/cyverse/cacao-tf-os-ops.git",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "single-image",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Disabled:     false,
			DisabledAt:   time.Time{},
			CredentialID: "",
			Metadata:     service.TemplateMetadata{},
			UIMetadata:   service.TemplateUIMetadata{},
			CreatedAt:    now,
		}
		err = storage.CreateVersion(versionToBeCreated)
		if !assert.NoError(t, err) {
			return
		}

		templateVersion, err := storage.GetVersion(username, versionID)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotZero(t, templateVersion.CreatedAt)
		assert.WithinDuration(t, templateVersion.CreatedAt, now, 10*time.Second)
		expectedVersionAfterCreation := versionToBeCreated
		expectedVersionAfterCreation.CreatedAt = templateVersion.CreatedAt
		assert.Equal(t, versionID, templateVersion.ID)
		assert.Equal(t, expectedVersionAfterCreation, templateVersion)
		assert.Truef(t, templateVersion.DisabledAt.IsZero(), "DisabledAt should be unpopulated")

		versions, err = storage.ListVersions(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, versions, 1) {
			return
		}
		assert.Equal(t, expectedVersionAfterCreation, versions[0])

		disabledAt, err := storage.DisableVersion(versionID)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotZero(t, disabledAt)
		assert.False(t, disabledAt.IsZero())
		assert.Greater(t, disabledAt, templateVersion.CreatedAt)

		templateVersion, err = storage.GetVersion(username, versionID)
		if !assert.NoError(t, err) {
			return
		}
		assert.True(t, templateVersion.Disabled)
		assert.WithinDuration(t, templateVersion.DisabledAt, disabledAt, time.Millisecond*200) // MongoDB has limited time precision
		assert.NotZero(t, templateVersion.DisabledAt)
		assert.GreaterOrEqual(t, templateVersion.DisabledAt.UnixNano(), templateVersion.CreatedAt.UnixNano())
		assert.WithinDuration(t, templateVersion.DisabledAt, time.Now(), 10*time.Second)
		expectedAfterDisabled := expectedVersionAfterCreation
		expectedAfterDisabled.Disabled = templateVersion.Disabled
		expectedAfterDisabled.DisabledAt = templateVersion.DisabledAt
		assert.Equal(t, versionID, templateVersion.ID)
		assert.Equal(t, expectedAfterDisabled, templateVersion)

		err = storage.DeleteVersion(versionID)
		if !assert.NoError(t, err) {
			return
		}

		templateVersion, err = storage.GetVersion(username, versionID)
		if !assert.Error(t, err) {
			return
		}
		assert.Zero(t, templateVersion)
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())

		versions, err = storage.ListVersions(username, templateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, versions)
		assert.Empty(t, versions)
	})

	t.Run("version_get_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		version, err := storage.GetVersion("testuser123", types.NewTemplateVersionID())
		if !assert.Error(t, err) {
			return
		}
		assert.Zero(t, version)
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("version_disable_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		disabledAt, err := storage.DisableVersion(types.NewTemplateVersionID())
		if !assert.Error(t, err) {
			return
		}
		assert.Zero(t, disabledAt)
		assert.True(t, disabledAt.IsZero())
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("version_disable__already_disabled", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		versionID := types.NewTemplateVersionID()
		now := time.Now()
		versionToBeCreated := types.TemplateVersion{
			ID:         versionID,
			TemplateID: cacao_common.NewID("template"),
			Source: service.TemplateSource{
				Type: service.TemplateSourceTypeGit,
				URI:  "gitlab.com/cyverse/cacao-tf-os-ops.git",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "single-image",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Disabled:     false,
			DisabledAt:   time.Time{},
			CredentialID: "",
			Metadata:     service.TemplateMetadata{},
			UIMetadata:   service.TemplateUIMetadata{},
			CreatedAt:    now,
		}
		err := storage.CreateVersion(versionToBeCreated)
		if !assert.NoError(t, err) {
			return
		}

		disabledAt, err := storage.DisableVersion(versionID)
		if !assert.NoError(t, err) {
			return
		}
		version, err := storage.GetVersion("testuser123", versionID)
		if !assert.NoError(t, err) {
			return
		}
		assert.WithinDuration(t, disabledAt, version.DisabledAt, time.Millisecond*200) // MongoDB has limited time precision
		assert.True(t, version.Disabled)
		assert.NotZero(t, version.DisabledAt)
		firstDisabledAt := version.DisabledAt

		disabledAt, err = storage.DisableVersion(versionID)
		if !assert.NoError(t, err) {
			return
		}
		version, err = storage.GetVersion("testuser123", versionID)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, disabledAt, version.DisabledAt)
		assert.True(t, version.Disabled)
		assert.NotZero(t, version.DisabledAt)
		secondDisabledAt := version.DisabledAt
		assert.Equal(t, secondDisabledAt, firstDisabledAt) // timestamp should not change
	})

	t.Run("version_disable__race", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		versionID := types.NewTemplateVersionID()
		now := time.Now()
		versionToBeCreated := types.TemplateVersion{
			ID:         versionID,
			TemplateID: cacao_common.NewID("template"),
			Source: service.TemplateSource{
				Type: service.TemplateSourceTypeGit,
				URI:  "gitlab.com/cyverse/cacao-tf-os-ops.git",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "single-image",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Disabled:     false,
			DisabledAt:   time.Time{},
			CredentialID: "",
			Metadata:     service.TemplateMetadata{},
			UIMetadata:   service.TemplateUIMetadata{},
			CreatedAt:    now,
		}
		err := storage.CreateVersion(versionToBeCreated)
		if !assert.NoError(t, err) {
			return
		}

		var wg sync.WaitGroup
		const N = 100
		var result = make([]time.Time, N)
		for i := 0; i < N; i++ {
			index := i
			wg.Add(1)
			go func() {
				defer wg.Done()
				disabledAt, err := storage.DisableVersion(versionID)
				if !assert.NoError(t, err) {
					return
				}
				assert.NotZero(t, disabledAt)
				result[index] = disabledAt
			}()
		}
		wg.Wait()
		for i := range result {
			assert.NotZero(t, result[i])
			// MongoDB has limited time precision
			assert.WithinDuration(t, result[0], result[i], time.Millisecond*200, "%v, %v", result[0], result[i])
		}
	})

	t.Run("version_delete_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		err := storage.DeleteVersion(types.NewTemplateVersionID())
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("version_list__template_not_exists", func(t *testing.T) {
		storage := initMongoStorageForTesting()
		t.Cleanup(cleanupStorageAfterTest(storage))

		templateID := cacao_common.NewID("template")
		versions, err := storage.ListVersions("testuser123", templateID)
		if !assert.Error(t, err) {
			return
		}
		assert.Nil(t, versions)
		assert.Equalf(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
	})
}

func skipMongoIntegrationTest(t *testing.T) bool {
	if val, ok := os.LookupEnv("CI_INTEGRATION_MONGO"); !ok || strings.ToLower(val) != "true" {
		t.Skip("skipping integration test with MongoDB, MONGO_INTEGRATION_TEST not set")
		return true
	}
	return false
}

func initMongoStorageForTesting() storageInterface {
	var conf types.Config
	err := envconfig.Process("", &conf)
	if err != nil {
		log.WithError(err).Panic()
	}
	conf.ProcessDefaults()

	var ma MongoAdapter
	err = ma.Init(&conf)
	if err != nil {
		log.WithError(err).Error("fail to init MongoAdapter, backoff")
		time.Sleep(time.Second * 10) // backoff and try again (wait for CI to spin up mongo)
		err = ma.Init(&conf)
		if err != nil {
			log.WithError(err).Panic("fail to init MongoAdapter")
		}
	}
	// create a placeholder collection, so that the database is auto-created
	_ = ma.Conn.Database.CreateCollection(context.Background(), "testing_placeholder")
	return &ma
}

func cleanupStorageAfterTest(storage storageInterface) func() {
	ma := storage.(*MongoAdapter)
	return func() {
		ma.Conn.Database.Collection(ma.Config.TemplateMongoDBCollectionName).Drop(context.Background())
		ma.Conn.Database.Collection(ma.Config.TemplateVersionMongoDBCollectionName).Drop(context.Background())
		err := ma.Close()
		if err != nil {
			log.WithError(err).Panic("fail to close mongo adapter during testing")
		}
	}
}
