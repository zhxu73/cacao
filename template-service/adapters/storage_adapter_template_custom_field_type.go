package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListCustomFieldTypes returns all template custom field types
func (adapter *MongoAdapter) ListCustomFieldTypes() ([]types.TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.ListCustomFieldTypes",
	})

	results := []types.TemplateCustomFieldType{}

	err := adapter.Store.List(adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, &results)
	if err != nil {
		errorMessage := "unable to list template custom field types"
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockListCustomFieldTypes sets expected results for ListCustomFieldTypes
func (adapter *MongoAdapter) MockListCustomFieldTypes(expectedTemplateCustomFieldTypes []types.TemplateCustomFieldType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("List", adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName).Return(expectedTemplateCustomFieldTypes, expectedError)
	return nil
}

// GetCustomFieldType returns the template custom field type with the name
func (adapter *MongoAdapter) GetCustomFieldType(templateCustomFieldTypeName string) (types.TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.GetCustomFieldType",
	})

	result := types.TemplateCustomFieldType{}

	err := adapter.Store.Get(adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldTypeName, &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return result, nil
}

// MockGetCustomFieldType sets expected results for GetCustomFieldType
func (adapter *MongoAdapter) MockGetCustomFieldType(templateCustomFieldTypeName string, expectedTemplateCustomFieldType types.TemplateCustomFieldType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldTypeName).Return(expectedTemplateCustomFieldType, expectedError)
	return nil
}

// CreateCustomFieldType inserts a template custom field type
func (adapter *MongoAdapter) CreateCustomFieldType(templateCustomFieldType types.TemplateCustomFieldType) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.CreateCustomFieldType",
	})

	err := adapter.Store.Insert(adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldType)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a template custom field type because name %s conflicts", templateCustomFieldType.Name)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a template custom field type with name %s", templateCustomFieldType.Name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreateCustomFieldType sets expected results for CreateCustomFieldType
func (adapter *MongoAdapter) MockCreateCustomFieldType(templateCustomFieldType types.TemplateCustomFieldType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName).Return(expectedError)
	return nil
}

// UpdateCustomFieldType updates/edits a template custom field type
func (adapter *MongoAdapter) UpdateCustomFieldType(templateCustomFieldType types.TemplateCustomFieldType, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.UpdateCustomFieldType",
	})

	updated, err := adapter.Store.Update(adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldType.Name, templateCustomFieldType, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the template custom field type for name %s", templateCustomFieldType.Name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the template custom field type for name %s", templateCustomFieldType.Name)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdateCustomFieldType sets expected results for UpdateCustomFieldType
func (adapter *MongoAdapter) MockUpdateCustomFieldType(templateCustomFieldType types.TemplateCustomFieldType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Update", adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldType.Name).Return(expectedResult, expectedError)
	return nil
}

// DeleteCustomFieldType deletes a template custom field type
func (adapter *MongoAdapter) DeleteCustomFieldType(templateCustomFieldTypeName string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.DeleteCustomFieldType",
	})

	deleted, err := adapter.Store.Delete(adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldTypeName)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the template custom field type for name %s", templateCustomFieldTypeName)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDeleteCustomFieldType sets expected results for DeleteCustomFieldType
func (adapter *MongoAdapter) MockDeleteCustomFieldType(templateCustomFieldTypeName string, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Delete", adapter.Config.TemplateCustomFieldTypeMongoDBCollectionName, templateCustomFieldTypeName).Return(expectedResult, expectedError)
	return nil
}
