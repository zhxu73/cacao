package adapters

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var templateCollectionSchema = bson.M{
	"$jsonSchema": bson.M{
		"bsonType": "object",
		"required": []string{"_id", "owner", "name", "public", "deleted", "latest_version_id", "source", "created_at", "updated_at"},
		"properties": bson.M{
			"_id": bson.M{
				"bsonType":    "string",
				"description": "xid of template",
				"minLength":   22,
			},
			"owner": bson.M{
				"bsonType":    "string",
				"description": "username of the owner",
				"minLength":   1,
			},
			"name": bson.M{
				"bsonType":    "string",
				"description": "name of the template",
				"minLength":   1,
				"maxLength":   128,
			},
			"description": bson.M{
				"bsonType":    "string",
				"description": "description of the template",
				"maxLength":   512,
			},
			"public": bson.M{
				"bsonType":    "bool",
				"description": "whether the template is public (any user can access it) or not",
			},
			"deleted": bson.M{
				"bsonType":    "bool",
				"description": "whether the template is deleted or not",
			},
			"latest_version_id": bson.M{
				"bsonType":    "string",
				"description": "XID of the latest template version for this template",
				"minLength":   22,
			},
			"source": bson.M{
				"bsonType":             "object",
				"description":          "source of the template, specify where the template is stored and how to download it",
				"additionalProperties": false,
				"required":             []string{"type", "uri", "access_parameters", "source_visibility"},
				"properties": bson.M{
					"type": bson.M{
						"bsonType":    "string",
						"description": "type of the template source (e.g. git)",
						"minLength":   1,
					},
					"uri": bson.M{
						"bsonType":    "string",
						"description": "uri of the source, format depend on the type",
						"minLength":   5,
					},
					"access_parameters": bson.M{
						"bsonType":    "object",
						"description": "additional parameters to specify how to download the template from source",
						"properties": bson.M{
							"branch": bson.M{
								"bsonType": "string",
							},
							"tag": bson.M{
								"bsonType": "string",
							},
							"commit": bson.M{
								"bsonType": "string",
							},
							"path": bson.M{
								"bsonType":  "string",
								"minLength": 1,
							},
						},
					},
					"source_visibility": bson.M{
						"bsonType":    "string",
						"enum":        []string{"public", "private"},
						"description": "whether the template source is public (able to access without any credential) or not",
					},
				},
			},
			"credential_id": bson.M{
				"bsonType":    "string",
				"description": "credential ID to use to download the template from source, if the template source is private",
			},
			"metadata": bson.M{
				"bsonType":    "object",
				"description": "same as metadata of latest template version",
			},
			"ui_metadata": bson.M{
				"bsonType":    "object",
				"description": "same as ui_metadata of latest template version",
			},
			"created_at": bson.M{
				"bsonType":    "date",
				"description": "when is the template created",
			},
			"updated_at": bson.M{
				"bsonType":    "date",
				"description": "when is the template updated",
			},
		},
	},
}

func (adapter *MongoAdapter) createTemplateCollection(ctx context.Context, config *types.Config) error {
	if config.MongoDBValidationSchema {
		err := adapter.createCollectionWithSchema(ctx, adapter.Config.TemplateMongoDBCollectionName, templateCollectionSchema)
		if err != nil {
			return err
		}
	}
	models := []mongo.IndexModel{
		{
			Keys: bson.M{
				"owner": 1, // ascending order
			},
			Options: options.Index().SetName("owner"),
		},
		{
			Keys: bson.M{
				"public": 1, // ascending order
			},
			Options: options.Index().SetName("public"),
		},
	}
	_, err := adapter.Conn.Database.Collection(adapter.Config.TemplateMongoDBCollectionName).Indexes().CreateMany(ctx, models)
	if err != nil {
		return err
	}
	return nil
}

// List returns all templates accessible for a user.
// Only return public templates and templates that user owns.
func (adapter *MongoAdapter) List(user string, includeCacaoReservedPurposes bool) ([]types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.Template{}

	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"$or": []map[string]interface{}{
					{
						"owner": user,
					},
					{
						"public": true,
					},
				},
			},
			{
				"deleted": false, // list do not return deleted template
			},
		},
	}

	if !includeCacaoReservedPurposes {
		reservedPurposes := []string{}
		for _, purpose := range cacao_common_service.TemplateCacaoReservedPurposes {
			reservedPurposes = append(reservedPurposes, purpose.String())
		}

		filter = map[string]interface{}{
			"$and": []map[string]interface{}{
				{
					"metadata.purpose": map[string]interface{}{
						"$nin": reservedPurposes,
					},
				},
				{
					"$or": []map[string]interface{}{
						{
							"owner": user,
						},
						{
							"public": true,
						},
					},
				},
				{
					"deleted": false, // list do not return deleted template
				},
			},
		}
	}

	err := adapter.Store.ListConditional(adapter.Config.TemplateMongoDBCollectionName, filter, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list templates for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}
	log.Println(results)
	if len(results) > 0 {
		log.Println(results[0].Deleted)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, includeCacaoReservedPurposes bool, expectedTemplates []types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"$or": []map[string]interface{}{
					{
						"owner": user,
					},
					{
						"public": true,
					},
				},
			},
			{
				"deleted": false,
			},
		},
	}

	if !includeCacaoReservedPurposes {
		reservedPurposes := []string{}
		for _, purpose := range cacao_common_service.TemplateCacaoReservedPurposes {
			reservedPurposes = append(reservedPurposes, purpose.String())
		}

		filter = map[string]interface{}{
			"$and": []map[string]interface{}{
				{
					"metadata.purpose": []map[string]interface{}{
						{
							"$nin": reservedPurposes,
						},
					},
				},
				{
					"$or": []map[string]interface{}{
						{
							"owner": user,
						},
						{
							"public": true,
						},
					},
				},
				{
					"deleted": false,
				},
			},
		}
	}

	mock.On("ListConditional", adapter.Config.TemplateMongoDBCollectionName, filter).Return(expectedTemplates, expectedError)
	return nil
}

// Get returns the template with the ID.
// Return the template only if it exists AND user is authorized (template is public or user owns the template).
func (adapter *MongoAdapter) Get(user string, templateID cacao_common.ID) (types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if !result.Public && result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", templateID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, templateID cacao_common.ID, expectedTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(expectedTemplate, expectedError)
	return nil
}

// Create inserts a template
func (adapter *MongoAdapter) Create(template types.Template) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.TemplateMongoDBCollectionName, template)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a template because id %s conflicts", template.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a template with id %s", template.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(template types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a template.
// Only update the template if user is authorized (user owns the template).
func (adapter *MongoAdapter) Update(template types.Template, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, template.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", template.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != template.Owner {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", template.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.TemplateMongoDBCollectionName, template.ID.String(), template, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the template for id %s", template.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the template for id %s", template.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingTemplate types.Template, newTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, newTemplate.ID.String()).Return(existingTemplate, expectedError)
	mock.On("Update", adapter.Config.TemplateMongoDBCollectionName, newTemplate.ID.String()).Return(expectedResult, expectedError)
	return nil
}

const templateSyncCollectionName = "template_sync"

// TemplateSyncStart insert a template sync object into DB. Return true if successfully acquire the sync lock for the template.
// Return (false, nil) if there is already a template sync in DB.
func (adapter *MongoAdapter) TemplateSyncStart(templateID cacao_common.ID) (success bool, err error) {
	now := time.Now().UTC()
	_, err = adapter.Conn.Database.Collection(templateSyncCollectionName).InsertOne(context.TODO(), types.TemplateSync{
		TemplateID:    templateID,
		SyncStartedAt: now,
	})
	if err != nil {
		if mongo.IsDuplicateKeyError(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// TemplateSyncFinished releases the sync lock for the template.
func (adapter *MongoAdapter) TemplateSyncFinished(templateID cacao_common.ID) error {
	result, err := adapter.Conn.Database.Collection(templateSyncCollectionName).DeleteOne(context.TODO(), bson.M{"_id": templateID.String()})
	if err != nil {
		return err
	}
	if result.DeletedCount != 1 {
		return fmt.Errorf("did not find sync for the template ID %s", templateID)
	}
	return nil
}

// SoftDelete soft deletes a template, template is simply being marked as deleted.
// Only delete the template if user is authorized (user owns the template).
func (adapter *MongoAdapter) SoftDelete(user string, templateID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.SoftDelete",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", templateID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// soft delete
	updated, err := adapter.Store.Update(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), types.Template{Deleted: true}, []string{"deleted"})
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to delete the template for id %s", templateID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// Delete deletes a template.
// Only delete the template if user is authorized (user owns the template).
func (adapter *MongoAdapter) Delete(user string, templateID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", templateID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.TemplateMongoDBCollectionName, templateID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the template for id %s", templateID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, templateID cacao_common.ID, existingTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(existingTemplate, expectedError)
	mock.On("Delete", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(expectedResult, expectedError)
	return nil
}
