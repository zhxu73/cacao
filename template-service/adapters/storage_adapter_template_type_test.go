package adapters

import (
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestMongoAdapterListTypes(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	expectedResults := []types.TemplateType{
		{
			Name: "test_template_type1",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
				cacao_common_service.TemplateFormatJSON,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
				cacao_common_service.TemplateProviderTypeKubernetes,
			},
		},
		{
			Name: "test_template_type2",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
			},
		},
	}
	err := mongoAdapter.MockListTypes(expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.ListTypes()
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterListByProviderType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testProviderType := cacao_common_service.TemplateProviderTypeOpenStack

	expectedResults := []types.TemplateType{
		{
			Name: "test_template_type1",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
				cacao_common_service.TemplateFormatJSON,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
				cacao_common_service.TemplateProviderTypeKubernetes,
			},
		},
		{
			Name: "test_template_type2",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
			},
		},
	}
	err := mongoAdapter.MockListTypesForProviderType(testProviderType, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.ListTypesForProviderType(testProviderType)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterGetType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	var testTemplateTypeName cacao_common_service.TemplateTypeName = "test_template_type1"

	expectedResult := types.TemplateType{
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}
	err := mongoAdapter.MockGetType(testTemplateTypeName, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.GetType(testTemplateTypeName)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterCreateType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	var testTemplateTypeName cacao_common_service.TemplateTypeName = "test_template_type1"

	testTemplateType := types.TemplateType{
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	err := mongoAdapter.MockCreateType(testTemplateType, nil)
	assert.NoError(t, err)

	err = mongoAdapter.CreateType(testTemplateType)
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterUpdateType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	var testTemplateTypeName cacao_common_service.TemplateTypeName = "test_template_type1"

	testTemplateType := types.TemplateType{
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	err := mongoAdapter.MockUpdateType(testTemplateType, nil)
	assert.NoError(t, err)

	err = mongoAdapter.UpdateType(testTemplateType, []string{"formats", "provider_types"})
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterDeleteType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	var testTemplateTypeName cacao_common_service.TemplateTypeName = "test_template_type1"

	err := mongoAdapter.MockDeleteType(testTemplateTypeName, nil)
	assert.NoError(t, err)

	err = mongoAdapter.DeleteType(testTemplateTypeName)
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}
