#!/bin/bash

# this sets necessary env var to run integration test with mongodb spawned by ./docker-compose.yaml
export CI_INTEGRATION_MONGO=true
export MONGODB_URL=mongodb://root:example@localhost:27017/
export MONGODB_DB_NAME=cacao
