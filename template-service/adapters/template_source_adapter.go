package adapters

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-git/go-billy/v5"
	log "github.com/sirupsen/logrus"
	cacao_common_git "gitlab.com/cyverse/cacao-common/git"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	cacao_common_template "gitlab.com/cyverse/cacao-common/template"
	"gitlab.com/cyverse/cacao/template-service/types"
)

const (
	// TemplateMetadataVersionSupported is the current version of template metadata supported
	TemplateMetadataVersionSupported string = "3"
	// TemplateUIMetadataVersionSupported is the current version of template UI metadata supported
	TemplateUIMetadataVersionSupported string = "1"
)

// GitAdapter implements TemplateSourcePort
type GitAdapter struct {
	Config           *types.Config
	GitStorageOption *cacao_common_git.RAMStorageOption
}

// Init initialize git adapter
func (adapter *GitAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.Init",
	})

	logger.Info("Initializing Git Adapter")

	adapter.Config = config

	adapter.GitStorageOption = &cacao_common_git.RAMStorageOption{
		SizeLimit: config.GitRAMSizeLimit,
	}
	return nil
}

// Finalize finalizes git adapter
func (adapter *GitAdapter) Finalize() {
	adapter.GitStorageOption = nil
}

// GetTemplateMetadata returns all template metadata
func (adapter *GitAdapter) GetTemplateMetadata(source cacao_common_service.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (cacao_common_service.TemplateMetadata, cacao_common_service.TemplateUIMetadata, cacao_common_service.TemplateSource, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.GetTemplateMetadata",
	})

	logger.Infof("Reading template metadata from %s", source.Type)
	if source.Type == cacao_common_service.TemplateSourceTypeGit {
		return adapter.getTemplateMetadataFromGit(source, credential)
	}

	errorMessage := fmt.Sprintf("unhandled template source type %s", source.Type)
	logger.Error(errorMessage)
	return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, cacao_common_service.TemplateSource{}, cacao_common_service.NewCacaoNotImplementedError(errorMessage)
}

func (adapter *GitAdapter) getTemplateMetadataFromGit(source cacao_common_service.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (cacao_common_service.TemplateMetadata, cacao_common_service.TemplateUIMetadata, cacao_common_service.TemplateSource, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.getTemplateMetadataFromGit",
	})

	logger.Infof("Reading template metadata from %s", source.URI)

	referenceType := cacao_common_git.BranchReferenceType
	referenceName := "main"
	templateDir := "/"

	if val, ok := source.AccessParameters["branch"]; ok {
		referenceType = cacao_common_git.BranchReferenceType
		referenceName = val.(string)
	}

	if val, ok := source.AccessParameters["tag"]; ok {
		referenceType = cacao_common_git.TagReferenceType
		referenceName = val.(string)
	}

	if val, ok := source.AccessParameters["path"]; ok {
		templateDir = val.(string)
	}

	repoConfig := cacao_common_git.NewGitRepositoryConfigWithNoAuth(source.URI, referenceType, referenceName)

	if len(credential.SSHKey) > 0 {
		username := "git"
		if len(credential.Username) > 0 {
			username = credential.Username
		}

		repoConfig = cacao_common_git.NewGitRepositoryConfigWithSSHAuth(source.URI, username, []byte(credential.SSHKey), credential.Password, referenceType, referenceName)
	} else {
		if len(credential.Password) > 0 {
			if len(credential.Username) > 0 {
				repoConfig = cacao_common_git.NewGitRepositoryConfigWithPasswordAuth(source.URI, credential.Username, credential.Password, referenceType, referenceName)
			} else {
				repoConfig = cacao_common_git.NewGitRepositoryConfigWithAccessTokenAuth(source.URI, credential.Password, referenceType, referenceName)
			}
		}
	}

	metadata := cacao_common_service.TemplateMetadata{}
	uiMetadata := cacao_common_service.TemplateUIMetadata{}

	clone, err := cacao_common_git.NewGitCloneToRAM(repoConfig, adapter.GitStorageOption)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to clone git repository %s", source.URI)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, cacao_common_service.TemplateSource{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	defer clone.Release()

	headRef, err := clone.Repository.Head()
	if err != nil {
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, cacao_common_service.TemplateSource{}, err
	}
	source.AccessParameters["commit"] = headRef.Hash().String()

	metadataBytes, uiMetadataBytes, err := adapter.readTemplateMetadataFromGit(clone, templateDir)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to read template metadata from the git repository %s", source.URI)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
	}

	// validate against given metadata schema
	err = cacao_common_template.ValidateMetadata(metadataBytes)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to validate template metadata from the git repository %s", source.URI)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
	}

	// we only support "TemplateMetadataVersionSupported" version for now
	err = cacao_common_template.ValidateMetadataWithVersion(metadataBytes, TemplateMetadataVersionSupported)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to validate template metadata from the git repository %s with metadata schema v%s", source.URI, TemplateMetadataVersionSupported)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
	}

	// unmarshal
	err = json.Unmarshal(metadataBytes, &metadata)
	if err != nil {
		errorMessage := "unable to unmarshal metadata to TemplateMetadata"
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
	}

	// if schema version is empty, fill default value
	if len(metadata.SchemaVersion) == 0 {
		metadata.SchemaVersion = TemplateMetadataVersionSupported
	} else {
		// if schema version is not supported, fail
		if metadata.SchemaVersion != TemplateMetadataVersionSupported {
			errorMessage := fmt.Sprintf("unable to validate template metadata from the git repository %s with metadata schema v%s", source.URI, TemplateMetadataVersionSupported)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
		}
	}

	// if published versions field is empty, fill default value
	if len(metadata.PublishedVersions) == 0 {
		// referenceName value will have "main" by default
		metadata.PublishedVersions = append(metadata.PublishedVersions, referenceName)
	}

	err = checkParametersDefaultValueAndEnumValue(metadataBytes)
	if err != nil {
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, err
	}

	// ui metadata is optional
	if uiMetadataBytes != nil {
		// validate against given metadata schema
		err = cacao_common_template.ValidateUIMetadata(uiMetadataBytes)
		if err != nil {
			errorMessage := fmt.Sprintf("unable to validate template ui metadata from the git repository %s", source.URI)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
		}

		// we only support "TemplateUIMetadataVersionSupported" version for now
		err = cacao_common_template.ValidateUIMetadataWithVersion(uiMetadataBytes, TemplateUIMetadataVersionSupported)
		if err != nil {
			errorMessage := fmt.Sprintf("unable to validate template ui metadata from the git repository %s with ui metadata schema v%s", source.URI, TemplateUIMetadataVersionSupported)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
		}

		// unmarshal
		err = json.Unmarshal(uiMetadataBytes, &uiMetadata)
		if err != nil {
			errorMessage := "unable to unmarshal ui metadata to TemplateUIMetadata"
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
		}

		if len(uiMetadata.SchemaVersion) == 0 {
			uiMetadata.SchemaVersion = TemplateUIMetadataVersionSupported
		} else {
			if uiMetadata.SchemaVersion != TemplateUIMetadataVersionSupported {
				errorMessage := fmt.Sprintf("unable to validate template ui metadata from the git repository %s with ui metadata schema v%s", source.URI, TemplateUIMetadataVersionSupported)
				logger.WithError(err).Error(errorMessage)
				return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, source, cacao_common_service.NewCacaoGeneralError(fmt.Sprintf("%s - %s", err.Error(), errorMessage))
			}
		}
	}

	return metadata, uiMetadata, source, nil
}

// readTemplateMetadataFromGit reads template metadata from git repository
func (adapter *GitAdapter) readTemplateMetadataFromGit(clone *cacao_common_git.Clone, subdirpath string) ([]byte, []byte, error) {
	subpath := "/"
	if len(subdirpath) > 0 {
		subpath = subdirpath
	}

	if !strings.HasPrefix(subpath, "/") {
		subpath = "/" + subpath
	}
	subpath = filepath.Clean(subpath)

	fs := clone.GetFileSystem()

	// possible metadata.json file locations
	possibleMetadataLocations := []string{
		".cacao/metadata.json",
		"metadata.json",
	}

	// find metadata.json first
	for _, possibleLocation := range possibleMetadataLocations {
		metadataPath := filepath.Join(subpath, possibleLocation)
		metadataBytes, err := adapter.readFile(fs, metadataPath)
		if err != nil {
			if os.IsNotExist(err) {
				// not found
				continue
			}

			// other error
			return nil, nil, err
		}

		// if metadata.json is found, find ui.json (optional)
		uiMetadataDirPath := filepath.Dir(metadataPath)
		uiMetadataPath := filepath.Join(uiMetadataDirPath, "ui.json")
		uiMetadataBytes, err := adapter.readFile(fs, uiMetadataPath)
		if err != nil {
			uiMetadataBytes = nil
		}

		return metadataBytes, uiMetadataBytes, nil
	}

	return nil, nil, fmt.Errorf("failed to find metadata files (metadata.json and ui.json) from the source")
}

// readFile reads file from fs
func (adapter *GitAdapter) readFile(fs billy.Filesystem, path string) ([]byte, error) {
	entry, err := fs.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			// file not exist
			return nil, err
		}

		return nil, fmt.Errorf("failed to stat the file %s", path)
	}

	if entry.IsDir() {
		// is a dir ?
		return nil, fmt.Errorf("failed to open the template metadata file %s, the path is a directory", path)
	}

	fd, err := fs.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open the template metadata file %s", path)
	}

	bytes, err := io.ReadAll(fd)
	if err != nil {
		return nil, fmt.Errorf("failed to read metadata from the file %s", path)
	}

	return bytes, nil
}

// Checks if the type of default value and enum values matches the type definition.
// The type of default value and enum values are not checked in json schema, so they need to be checked separately.
func checkParametersDefaultValueAndEnumValue(metadataBytes []byte) error {
	var metadata cacao_common_service.TemplateMetadata
	err := json.Unmarshal(metadataBytes, &metadata)
	if err != nil {
		return cacao_common_service.NewCacaoInvalidParameterError("fail to unmarshal metadata to validate the type of default and enum values")
	}

	for _, paramDef := range metadata.Parameters {
		if paramDef.Default != nil {
			_, _, err := paramDef.CheckValue(paramDef.Default)
			if err != nil {
				return cacao_common_service.NewCacaoInvalidParameterError(fmt.Sprintf("default value for parameter %s does not match the parameter type", paramDef.Name))
			}
		}
		if len(paramDef.Enum) > 0 {
			for _, enumVal := range paramDef.Enum {
				_, _, err := paramDef.CheckValue(enumVal)
				if err != nil {
					return cacao_common_service.NewCacaoInvalidParameterError(fmt.Sprintf("enum values for parameter %s does not match the parameter type", paramDef.Name))
				}
			}
		}
	}
	return nil
}
