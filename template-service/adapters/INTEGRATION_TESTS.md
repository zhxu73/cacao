# Integration Tests

This directory contains integration tests that tests the storage adapter against a real instance of MongoDB.

This tests requires MongoDB to run.

# Run tests locally
- make sure you have docker compose installed.
- go to the current directory (`template-service/adapters`)
- `docker compose up -d`
- `source mongo_integration_test_env.sh`
- `go test -v ./...`
