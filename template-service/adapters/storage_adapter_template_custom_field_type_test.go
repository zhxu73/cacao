package adapters

import (
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestMongoAdapterListCustomFieldTypes(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	expectedResults := []types.TemplateCustomFieldType{
		{
			Name:                      "test_template_custom_field_type1",
			Description:               "test_description1",
			QueryMethod:               cacao_common_service.TemplateCustomFieldTypeQueryMethodREST,
			QueryTarget:               "test_query_target1",
			QueryData:                 "test_query_data1",
			QueryResultJSONPathFilter: "test/filter1",
		},
		{
			Name:                      "test_template_custom_field_type2",
			Description:               "test_description2",
			QueryMethod:               cacao_common_service.TemplateCustomFieldTypeQueryMethodREST,
			QueryTarget:               "test_query_target2",
			QueryData:                 "test_query_data2",
			QueryResultJSONPathFilter: "test/filter2",
		},
	}
	err := mongoAdapter.MockListCustomFieldTypes(expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.ListCustomFieldTypes()
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterGetCustomFieldType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTemplateCustomFieldTypeName := "test_template_custom_field_type1"

	expectedResult := types.TemplateCustomFieldType{
		Name:                      testTemplateCustomFieldTypeName,
		Description:               "test_description1",
		QueryMethod:               cacao_common_service.TemplateCustomFieldTypeQueryMethodREST,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}
	err := mongoAdapter.MockGetCustomFieldType(testTemplateCustomFieldTypeName, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.GetCustomFieldType(testTemplateCustomFieldTypeName)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterCreateCustomFieldType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTemplateCustomFieldTypeName := "test_template_custom_field_type1"

	testTemplateCustomFieldType := types.TemplateCustomFieldType{
		Name:                      testTemplateCustomFieldTypeName,
		Description:               "test_description1",
		QueryMethod:               cacao_common_service.TemplateCustomFieldTypeQueryMethodREST,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	err := mongoAdapter.MockCreateCustomFieldType(testTemplateCustomFieldType, nil)
	assert.NoError(t, err)

	err = mongoAdapter.CreateCustomFieldType(testTemplateCustomFieldType)
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterUpdateCustomFieldType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTemplateCustomFieldTypeName := "test_template_custom_field_type1"

	testTemplateCustomFieldType := types.TemplateCustomFieldType{
		Name:                      testTemplateCustomFieldTypeName,
		Description:               "test_description1",
		QueryMethod:               cacao_common_service.TemplateCustomFieldTypeQueryMethodREST,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	err := mongoAdapter.MockUpdateCustomFieldType(testTemplateCustomFieldType, nil)
	assert.NoError(t, err)

	err = mongoAdapter.UpdateCustomFieldType(testTemplateCustomFieldType, []string{"description", "query_method", "query_target"})
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}

func TestMongoAdapterDeleteCustomFieldType(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTemplateCustomFieldTypeName := "test_template_custom_field_type1"

	err := mongoAdapter.MockDeleteCustomFieldType(testTemplateCustomFieldTypeName, nil)
	assert.NoError(t, err)

	err = mongoAdapter.DeleteCustomFieldType(testTemplateCustomFieldTypeName)
	assert.NoError(t, err)

	_ = mongoAdapter.Close()
}
