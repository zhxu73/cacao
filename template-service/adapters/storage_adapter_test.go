package adapters

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	mongoAdapter := &MongoAdapter{}
	mongoAdapter.InitMock(&config)

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.Store)

	_ = mongoAdapter.Close()
}

func loadMongoConfigFromEnv() types.Config {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Panic("fail to load config from env")
	}
	config.ProcessDefaults()
	config.Override()
	return config
}

// This is an integration test that require a live MongoDB instance
func TestMongoStorage(t *testing.T) {
	if env, ok := os.LookupEnv("CI_INTEGRATION_MONGO"); !ok || env != "true" {
		t.Skip("CI_INTEGRATION_MONGO != 'true'")
		return
	}
	config := loadMongoConfigFromEnv()
	testStorage(t, func() ports.PersistentStoragePort {
		mongoAdapter := &MongoAdapter{}
		err := mongoAdapter.Init(&config)
		if !assert.NoError(t, err) {
			t.Fatal(err.Error())
		}
		return mongoAdapter
	}, func(storage ports.PersistentStoragePort) {
		mongoAdapter := storage.(*MongoAdapter)
		store := mongoAdapter.Store.(*cacao_common_db.MongoDBObjectStore)

		// drop the data after each test case, so that each test case starts fresh
		err := store.Connection.Database.Collection(types.DefaultTemplateMongoDBCollectionName).Drop(context.Background())
		if !assert.NoError(t, err) {
			return
		}
		err = store.Connection.Database.Collection(types.DefaultTemplateTypeMongoDBCollectionName).Drop(context.Background())
		if !assert.NoError(t, err) {
			return
		}
		err = store.Connection.Database.Collection(types.DefaultTemplateCustomFieldTypeMongoDBCollectionName).Drop(context.Background())
		if !assert.NoError(t, err) {
			return
		}
		err = mongoAdapter.Close()
		assert.NoError(t, err)
	})
}

func testStorage(t *testing.T, setup func() ports.PersistentStoragePort, cleanup func(storage ports.PersistentStoragePort)) {
	t.Run("fetch non-existing template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)
		template, err := storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.Error(t, err) {
			return
		}
		cacaoError, ok := err.(cacao_common_service.CacaoError)
		if !assert.Truef(t, ok, "err.(cacao_common_service.CacaoError)") {
			return
		}
		assert.Equal(t, cacao_common_service.CacaoNotFoundErrorMessage, cacaoError.StandardError())
		assert.Equal(t, types.Template{}, template)
	})
	t.Run("fetch other's private template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)
		err := storage.Create(types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		template, err := storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.Error(t, err) {
			return
		}
		cacaoError, ok := err.(cacao_common_service.CacaoError)
		if !assert.Truef(t, ok, "err.(cacao_common_service.CacaoError)") {
			return
		}
		assert.Equal(t, cacao_common_service.CacaoUnauthorizedErrorMessage, cacaoError.StandardError())
		assert.Equal(t, types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		}, template)
	})
	t.Run("fetch other's public template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)
		err := storage.Create(types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "desc123",
			Public:          true,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		template, err := storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "desc123",
			Public:          true,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		}, template)
	})
	t.Run("update other's public template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)
		err := storage.Create(types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "desc123",
			Public:          true,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		err = storage.Update(types.Template{
			ID:                      "template-cnm8f0598850n9abikj0",
			Owner:                   "",
			Name:                    "newName123",
			Description:             "",
			Public:                  false,
			Source:                  cacao_common_service.TemplateSource{},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Date(2023, 6, 6, 6, 6, 6, 6, time.UTC),
		}, []string{"name"})
		if !assert.Error(t, err) {
			return
		}
		cacaoError, ok := err.(cacao_common_service.CacaoError)
		if !assert.Truef(t, ok, "err.(cacao_common_service.CacaoError)") {
			return
		}
		assert.Equal(t, cacao_common_service.CacaoUnauthorizedErrorMessage, cacaoError.StandardError())

		// re-fetch to make sure template is not modified
		template, err := storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "desc123",
			Public:          true,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		}, template)
	})
	t.Run("update other's public template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)
		err := storage.Create(types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "otheruser123",
			Name:            "template123",
			Description:     "desc123",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}
		err = storage.Create(types.Template{
			ID:              "template-bbbbbbbbbbbbbbbbbbbb",
			Owner:           "otheruser123",
			Name:            "template456",
			Description:     "desc456",
			Public:          true,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}
		list, err := storage.List("testuser123", false)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, list, 1) {
			return
		}
		assert.Equal(t, types.Template{
			ID:              "template-bbbbbbbbbbbbbbbbbbbb",
			Owner:           "otheruser123",
			Name:            "template456",
			Description:     "desc456",
			Public:          true,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   cacao_common_service.TemplateMetadata{},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			UpdatedAt:               time.Time{},
		}, list[0])
	})
	t.Run("fetch template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		var insertedTemplate = types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "testuser123",
			Name:            "template123",
			Description:     "desc123",
			Public:          true,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "git",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   ".",
				},
				Visibility: "public",
			},
			LatestVersionMetadata: cacao_common_service.TemplateMetadata{
				SchemaURL:     "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v3/schema.json",
				SchemaVersion: "3",
				Name:          "single image openstack instances",
				Author:        "ExampleUser123",
				AuthorEmail:   "cacao@cyverse.org",
				Description:   "simple launch of one or more vms",
				DocURL:        "",
				TemplateType:  "openstack_terraform",
				Purpose:       "openstack_compute",
				CacaoPreTasks: []cacao_common_service.TemplateCacaoTask{},
				CacaoPostTasks: []cacao_common_service.TemplateCacaoTask{
					{
						Type:     "ansible",
						Location: "cacao_atmosphere_legacy",
					},
				},
				Parameters: []cacao_common_service.TemplateParameter{
					{
						Name:        "username",
						Type:        "cacao_username",
						Description: "CACAO username",
						Default:     "username",
					},
					{
						Name:        "instance_count",
						Type:        "integer",
						Description: "# of instances",
						Default:     int32(1),
						Editable:    true,
						Base64:      false,
					},
					{
						Name:        "image_name",
						Type:        "cacao_provider_image_name",
						Description: "Boot image name",
						Default:     "",
					},
					{
						Name:        "power_state",
						Type:        "string",
						Description: "Power state",
						Default:     "active",
						Enum: []interface{}{
							"active",
							"shutoff",
							"suspend",
							"shelved_offloaded",
						},
						DataValidationType: "",
						Required:           false,
						Editable:           true,
						Base64:             false,
					},
				},
				Filters: nil,
			},
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{
				SchemaURL:     "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/ui-schemas/v1/schema.json",
				SchemaVersion: "1",
				Author:        "Example User",
				AuthorEmail:   "cacao@cyverse.org",
				Description:   "",
				DocURL:        "",
				Steps: []cacao_common_service.TemplateUIStep{
					{
						Title:    "Parameters",
						HelpText: "",
						Items: []cacao_common_service.TemplateUIGroupItem{
							func() cacao_common_service.TemplateUIGroupItem {
								var result cacao_common_service.TemplateUIGroupItem
								s := `
{
    "name": "instance_name",
	"field_type": "plain_text",
    "ui_label": "Instance name"
}
`
								err := json.Unmarshal([]byte(s), &result)
								if !assert.NoError(t, err) {
									panic(err)
								}
								return result
							}(),
							func() cacao_common_service.TemplateUIGroupItem {
								var result cacao_common_service.TemplateUIGroupItem
								s := `
{
    "name": "image_name",
	"field_type": "plain_text",
    "ui_label": "Boot image name",
    "parameters": [
        { "key": "windows_disclaimer", "value": true }
    ]
}
`
								err := json.Unmarshal([]byte(s), &result)
								if !assert.NoError(t, err) {
									panic(err)
								}
								return result
							}(),
							func() cacao_common_service.TemplateUIGroupItem {
								var result cacao_common_service.TemplateUIGroupItem
								s := `
{
    "name": "power_state",
	"field_type": "plain_text",
    "ui_label": "Power state"
}
`
								err := json.Unmarshal([]byte(s), &result)
								if !assert.NoError(t, err) {
									panic(err)
								}
								return result
							}(),
							func() cacao_common_service.TemplateUIGroupItem {
								var result cacao_common_service.TemplateUIGroupItem
								s := `
{
    "name": "instance_count",
	"field_type": "plain_text",
    "ui_label": "# of Instances"
}
`
								err := json.Unmarshal([]byte(s), &result)
								if !assert.NoError(t, err) {
									panic(err)
								}
								return result
							}(),
							func() cacao_common_service.TemplateUIGroupItem {
								var result cacao_common_service.TemplateUIGroupItem
								s := `
{
    "type": "advanced_settings",
    "collapsible": true,
    "title": "Advanced",
    "items": [
        {
            "name": "root_storage"
        }
    ]
}
`
								err := json.Unmarshal([]byte(s), &result)
								if !assert.NoError(t, err) {
									panic(err)
								}
								return result
							}(),
						},
					},
				},
			},
			CreatedAt: time.Date(2023, 1, 1, 1, 1, 1, 0, time.UTC),
			UpdatedAt: time.Time{},
		}
		err := storage.Create(insertedTemplate)
		if !assert.NoError(t, err) {
			return
		}

		template, err := storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.NoError(t, err) {
			return
		}
		expectedTemplate := insertedTemplate
		expectedTemplate.LatestVersionMetadata.CacaoPreTasks = nil
		expectedTemplate.LatestVersionMetadata.CacaoPostTasks[0].ExecuteOn.Type = "all"

		assert.Equal(t, expectedTemplate, template)
	})
	t.Run("update template", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		// fetch metadata from URL
		var metadata cacao_common_service.TemplateMetadata
		resp, err := http.Get("https://gitlab.com/cyverse/cacao-tf-os-ops/-/raw/a7bbb6ae598a71ff8a578d841520e550681f92b1/single-image-v3/metadata.json")
		if !assert.NoError(t, err) {
			return
		}
		data, err := io.ReadAll(resp.Body)
		if !assert.NoError(t, err) {
			return
		}
		err = json.Unmarshal(data, &metadata)
		if !assert.NoError(t, err) {
			return
		}

		err = storage.Create(types.Template{
			ID:              "template-cnm8f0598850n9abikj0",
			Owner:           "testuser123",
			Name:            "template123",
			Description:     "",
			Public:          false,
			Deleted:         false,
			LatestVersionID: "templateversion-00000000000000000000",
			Source: cacao_common_service.TemplateSource{
				Type: "terraform_openstack",
				URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPublic,
			},
			LatestVersionMetadata:   metadata,
			LatestVersionUIMetadata: cacao_common_service.TemplateUIMetadata{},
			CreatedAt:               time.Time{},
			UpdatedAt:               time.Time{},
		})
		if !assert.NoError(t, err) {
			return
		}

		template, err := storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.NoError(t, err) {
			return
		}

		// emulate Sync operation, which uses Update()
		template.UpdatedAt = time.Now().UTC()
		err = storage.Update(template, []string{"metadata", "ui_metadata", "updated_at"})
		if !assert.NoError(t, err) {
			return
		}

		// fetch again after Update(), this is to ensure that we can fetch again and there is no bson marshal error
		template, err = storage.Get("testuser123", "template-cnm8f0598850n9abikj0")
		if !assert.NoError(t, err) {
			return
		}
	})
}
