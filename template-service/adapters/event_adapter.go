package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingEventPort
	// internal
	Connection     cacao_common_messaging.StreamingEventService
	EventWaitGroup sync.WaitGroup
	conn           messaging2.EventConnection
}

var _ ports.IncomingEventPort = (*EventAdapter)(nil)

// NewEventAdapter ...
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{conn: conn}
}

// Init initializes the adapter
func (adapter *EventAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Init",
	})

	logger.Info("initializing EventAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
	return nil
}

// Finalize finalizes the adapter
func (adapter *EventAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Finalize",
	})

	logger.Info("finalizing EventAdapter")

	adapter.EventWaitGroup.Done()
}

//func (adapter *EventAdapter) getEventHandlerMapping() []cacao_common_messaging.StreamingEventHandlerMapping {
//	return []cacao_common_messaging.StreamingEventHandlerMapping{
//		{
//			Subject:      cacao_common_service.TemplateTypeCreateRequestedEvent,
//			EventHandler: adapter.handleTemplateTypeCreateRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateTypeDeleteRequestedEvent,
//			EventHandler: adapter.handleTemplateTypeDeleteRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateTypeUpdateRequestedEvent,
//			EventHandler: adapter.handleTemplateTypeUpdateRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateCustomFieldTypeCreateRequestedEvent,
//			EventHandler: adapter.handleTemplateCustomFieldTypeCreateRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateCustomFieldTypeDeleteRequestedEvent,
//			EventHandler: adapter.handleTemplateCustomFieldTypeDeleteRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateCustomFieldTypeUpdateRequestedEvent,
//			EventHandler: adapter.handleTemplateCustomFieldTypeUpdateRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateImportRequestedEvent,
//			EventHandler: adapter.handleTemplateImportRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateDeleteRequestedEvent,
//			EventHandler: adapter.handleTemplateDeleteRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateUpdateRequestedEvent,
//			EventHandler: adapter.handleTemplateUpdateRequest,
//		},
//		{
//			Subject:      cacao_common_service.TemplateSyncRequestedEvent,
//			EventHandler: adapter.handleTemplateSyncRequest,
//		},
//		{
//			Subject:      cacao_common.EventType(""),
//			EventHandler: adapter.handleDefaultEvent,
//		},
//	}
//}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup, handlers ports.IncomingEventHandlers) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Start",
	})
	logger.Info("starting EventAdapter")
	err := adapter.conn.ListenWithConcurrentWorkers(ctx, map[cacao_common.EventType]messaging2.EventHandlerFunc{
		cacao_common_service.TemplateTypeCreateRequestedEvent:            eventHandlerWrapper(handlers.TemplateTypeCreateRequestedEvent),
		cacao_common_service.TemplateTypeDeleteRequestedEvent:            eventHandlerWrapper(handlers.TemplateTypeDeleteRequestedEvent),
		cacao_common_service.TemplateTypeUpdateRequestedEvent:            eventHandlerWrapper(handlers.TemplateTypeUpdateRequestedEvent),
		cacao_common_service.TemplateCustomFieldTypeCreateRequestedEvent: eventHandlerWrapper(handlers.TemplateCustomFieldTypeCreateRequestedEvent),
		cacao_common_service.TemplateCustomFieldTypeDeleteRequestedEvent: eventHandlerWrapper(handlers.TemplateCustomFieldTypeDeleteRequestedEvent),
		cacao_common_service.TemplateCustomFieldTypeUpdateRequestedEvent: eventHandlerWrapper(handlers.TemplateCustomFieldTypeUpdateRequestedEvent),
		cacao_common_service.TemplateImportRequestedEvent:                eventHandlerWrapper(handlers.TemplateImportRequestedEvent),
		cacao_common_service.TemplateDeleteRequestedEvent:                eventHandlerWrapper(handlers.TemplateDeleteRequestedEvent),
		cacao_common_service.TemplateUpdateRequestedEvent:                eventHandlerWrapper(handlers.TemplateUpdateRequestedEvent),
		cacao_common_service.TemplateSyncRequestedEvent:                  eventHandlerWrapper(handlers.TemplateSyncRequestedEvent),
	}, wg, 1, 10)
	if err != nil {
		return err
	}
	return nil
}

func eventHandlerWrapper[T any](handler func(T, ports.OutgoingEventPort) error) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			return err
		}
		err = handler(request, eventOut{writer: writer})
		if err != nil {
			log.WithFields(log.Fields{"ceType": event.Type(), "ceID": event.ID()}).WithError(err).Error("handler errored")
		}
		return nil
	}
}

type eventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventPort = (*eventOut)(nil)

func (o eventOut) write(eventType cacao_common.EventType, eventData any) {
	logger := log.WithFields(log.Fields{
		"function":  "eventOut.write",
		"eventType": eventType,
	})
	ce, err := messaging2.CreateCloudEventWithAutoSource(eventData, eventType)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return
	}
	err = o.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Error("fail to publish cloudevent")
		return
	}
}

func (o eventOut) TypeCreated(actor cacao_common_service.Actor, templateType types.TemplateType) {
	model := types.ConvertTypeToModel(actor.Session(), templateType)
	o.write(cacao_common_service.TemplateTypeCreatedEvent, model)
}

func (o eventOut) TypeCreateFailed(actor cacao_common_service.Actor, templateType types.TemplateType, creationError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
		ServiceError:    creationError.GetBase(),
	}
	model := types.ConvertTypeToModel(session, templateType)
	o.write(cacao_common_service.TemplateTypeCreateFailedEvent, model)
}

func (o eventOut) TypeUpdated(actor cacao_common_service.Actor, templateType types.TemplateType) {
	model := types.ConvertTypeToModel(actor.Session(), templateType)
	o.write(cacao_common_service.TemplateTypeUpdatedEvent, model)
}

func (o eventOut) TypeUpdateFailed(actor cacao_common_service.Actor, templateType types.TemplateType, updateError cacao_common_service.CacaoError) {

	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
		ServiceError:    updateError.GetBase(),
	}
	model := types.ConvertTypeToModel(session, templateType)
	o.write(cacao_common_service.TemplateTypeUpdateFailedEvent, model)
}

func (o eventOut) TypeDeleted(actor cacao_common_service.Actor, templateType types.TemplateType) {
	model := types.ConvertTypeToModel(actor.Session(), templateType)
	o.write(cacao_common_service.TemplateTypeDeletedEvent, model)
}

func (o eventOut) TypeDeleteFailed(actor cacao_common_service.Actor, templateType types.TemplateType, deletionError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
		ServiceError:    deletionError.GetBase(),
	}
	model := types.ConvertTypeToModel(session, templateType)
	o.write(cacao_common_service.TemplateTypeDeleteFailedEvent, model)
}

func (o eventOut) CustomFieldTypeCreated(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType) {
	model := types.ConvertCustomFieldTypeToModel(actor.Session(), templateCustomFieldType)
	o.write(cacao_common_service.TemplateCustomFieldTypeCreatedEvent, model)
}

func (o eventOut) CustomFieldTypeCreateFailed(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType, creationError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
		ServiceError:    creationError.GetBase(),
	}
	model := types.ConvertCustomFieldTypeToModel(session, templateCustomFieldType)
	o.write(cacao_common_service.TemplateCustomFieldTypeCreateFailedEvent, model)
}

func (o eventOut) CustomFieldTypeUpdated(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType) {
	model := types.ConvertCustomFieldTypeToModel(actor.Session(), templateCustomFieldType)
	o.write(cacao_common_service.TemplateCustomFieldTypeUpdatedEvent, model)
}

func (o eventOut) CustomFieldTypeUpdateFailed(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType, updateError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
		ServiceError:    updateError.GetBase(),
	}
	model := types.ConvertCustomFieldTypeToModel(session, templateCustomFieldType)
	o.write(cacao_common_service.TemplateCustomFieldTypeUpdateFailedEvent, model)
}

func (o eventOut) CustomFieldTypeDeleted(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType) {
	model := types.ConvertCustomFieldTypeToModel(actor.Session(), templateCustomFieldType)
	o.write(cacao_common_service.TemplateCustomFieldTypeDeletedEvent, model)
}

func (o eventOut) CustomFieldTypeDeleteFailed(actor cacao_common_service.Actor, templateCustomFieldType types.TemplateCustomFieldType, deletionError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
		ServiceError:    deletionError.GetBase(),
	}
	model := types.ConvertCustomFieldTypeToModel(session, templateCustomFieldType)
	o.write(cacao_common_service.TemplateCustomFieldTypeDeleteFailedEvent, model)
}

func (o eventOut) Imported(actor cacao_common_service.Actor, template types.Template) {
	model := types.ConvertToModel(actor.Session(), template)
	o.write(cacao_common_service.TemplateImportedEvent, model)
}

func (o eventOut) ImportFailed(actor cacao_common_service.Actor, template types.Template, importError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       importError.Error(),
		ErrorMessage:    importError.Error(),
		ServiceError:    importError.GetBase(),
	}
	model := types.ConvertToModel(session, template)
	o.write(cacao_common_service.TemplateImportFailedEvent, model)
}

func (o eventOut) Updated(actor cacao_common_service.Actor, template types.Template) {
	model := types.ConvertToModel(actor.Session(), template)
	o.write(cacao_common_service.TemplateUpdatedEvent, model)
}

func (o eventOut) UpdateFailed(actor cacao_common_service.Actor, template types.Template, updateError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
		ServiceError:    updateError.GetBase(),
	}
	model := types.ConvertToModel(session, template)
	o.write(cacao_common_service.TemplateUpdateFailedEvent, model)
}

func (o eventOut) Deleted(actor cacao_common_service.Actor, template types.Template) {
	model := types.ConvertToModel(actor.Session(), template)
	o.write(cacao_common_service.TemplateDeletedEvent, model)
}

func (o eventOut) DeleteFailed(actor cacao_common_service.Actor, template types.Template, deletionError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
		ServiceError:    deletionError.GetBase(),
	}
	model := types.ConvertToModel(session, template)
	o.write(cacao_common_service.TemplateDeleteFailedEvent, model)
}

func (o eventOut) Synced(actor cacao_common_service.Actor, template types.Template) {
	model := types.ConvertToModel(actor.Session(), template)
	o.write(cacao_common_service.TemplateSyncedEvent, model)
}

func (o eventOut) SyncFailed(actor cacao_common_service.Actor, template types.Template, syncError cacao_common_service.CacaoError) {
	session := cacao_common_service.Session{
		SessionActor:    actor.Actor,
		SessionEmulator: actor.Emulator,
		ErrorType:       syncError.Error(),
		ErrorMessage:    syncError.Error(),
		ServiceError:    syncError.GetBase(),
	}
	model := types.ConvertToModel(session, template)
	o.write(cacao_common_service.TemplateSyncFailedEvent, model)
}

func (o eventOut) VersionDisabled(actor cacao_common_service.Actor, version types.TemplateVersion) {
	model := types.ConvertVersionToModel(actor.Session(), version)
	o.write(cacao_common_service.TemplateVersionDisabledEvent, model)
}

func (o eventOut) VersionDisableFailed(actor cacao_common_service.Actor, version types.TemplateVersion, versionErr cacao_common_service.CacaoError) {
	model := types.ConvertVersionToModel(actor.Session(), version)
	model.Session.ServiceError = versionErr.GetBase()
	o.write(cacao_common_service.TemplateVersionDisableFailedEvent, model)
}
