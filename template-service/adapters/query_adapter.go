package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	config *types.Config
	conn   messaging2.QueryConnection
}

// NewQueryAdapter ...
func NewQueryAdapter(conn messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{conn: conn}
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.config = config
	return nil
}

// Start starts the adapter
func (adapter *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup, handlers ports.IncomingQueryHandlers) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	err := adapter.conn.Listen(ctx, map[cacao_common.QueryOp]messaging2.QueryHandlerFunc{
		cacao_common_service.TemplateTypeListQueryOp:             queryHandlerWrapper(handlers.ListTypes),
		cacao_common_service.TemplateTypeGetQueryOp:              queryHandlerWrapper(handlers.GetType),
		cacao_common_service.TemplateSourceTypeListQueryOp:       queryHandlerWrapper(handlers.ListSourceTypes),
		cacao_common_service.TemplateCustomFieldTypeListQueryOp:  queryHandlerWrapper(handlers.ListCustomFieldTypes),
		cacao_common_service.TemplateCustomFieldTypeGetQueryOp:   queryHandlerWrapper(handlers.GetCustomFieldType),
		cacao_common_service.TemplateCustomFieldTypeQueryQueryOp: queryHandlerWrapper(handlers.QueryCustomFieldType),
		cacao_common_service.TemplateListQueryOp:                 queryHandlerWrapper(handlers.List),
		cacao_common_service.TemplateGetQueryOp:                  queryHandlerWrapper(handlers.Get),
		cacao_common_service.TemplateVersionListQueryOp:          queryHandlerWrapper(handlers.ListTemplateVersions),
		cacao_common_service.TemplateVersionGetQueryOp:           queryHandlerWrapper(handlers.GetTemplateVersion),
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

func queryHandlerWrapper[T1 any, T2 any](handler func(T1) T2) messaging2.QueryHandlerFunc {
	return func(ctx context.Context, ce cloudevents.Event, writer messaging2.ReplyWriter) {
		var request T1
		err := json.Unmarshal(ce.Data(), &request)
		if err != nil {
			return
		}
		reply := handler(request)
		ceReply, err := messaging2.CreateCloudEventWithAutoSource(reply, cacao_common.QueryOp(ce.Type()))
		if err != nil {
			return
		}
		err = writer.Write(ceReply)
		if err != nil {
			return
		}
	}
}
