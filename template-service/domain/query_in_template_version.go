package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// GetTemplateVersion ...
func (d *QueryPortImpl) GetTemplateVersion(request cacao_common_service.TemplateVersionModel) cacao_common_service.TemplateVersionModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.GetTemplateVersion",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"id":       request.ID,
	})

	logger.Infof("Get template version %s", request.ID.String())

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateVersionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			TemplateVersion: cacao_common_service.TemplateVersion{
				ID:         request.ID,
				TemplateID: request.ID,
			},
		}
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template version ID is empty")
		return cacao_common_service.TemplateVersionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			TemplateVersion: cacao_common_service.TemplateVersion{
				ID:         request.ID,
				TemplateID: request.ID,
			},
		}
	}

	templateVersion, err := d.Storage.GetVersion(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch template version from storage")
		return cacao_common_service.TemplateVersionModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			TemplateVersion: cacao_common_service.TemplateVersion{
				ID:         request.ID,
				TemplateID: request.ID,
			},
		}
	}
	logger.Infof("templateVersion fetched")
	return types.ConvertVersionToModel(cacao_common_service.CopySessionActors(request.Session), templateVersion)
}

// ListTemplateVersions lists all versions for a template (specified by template ID).
// TODO consider pagination/streaming of results.
func (d *QueryPortImpl) ListTemplateVersions(request cacao_common_service.TemplateVersionModel) cacao_common_service.TemplateVersionListModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.ListTemplateVersions",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"template": request.TemplateID,
	})

	logger.Infof("List versions %s", request.TemplateID.String())

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateVersionListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Versions: nil,
		}
	}

	if len(request.TemplateID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template ID is empty")
		return cacao_common_service.TemplateVersionListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Versions: nil,
		}
	}

	versions, err := d.Storage.ListVersions(request.SessionActor, request.TemplateID)
	if err != nil {
		logger.WithError(err).Error("fail to list template versions from storage")
		return cacao_common_service.TemplateVersionListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Versions: nil,
		}
	}
	models := make([]cacao_common_service.TemplateVersion, 0, len(versions))
	for _, ss := range versions {
		model := types.ConvertVersionToListItemModel(ss)
		models = append(models, model)
	}

	logger.WithField("len", len(models)).Infof("listed")
	return cacao_common_service.TemplateVersionListModel{
		Session:  cacao_common_service.CopySessionActors(request.Session),
		Versions: models,
	}
}
