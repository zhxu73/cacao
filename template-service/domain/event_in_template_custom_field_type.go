package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// TemplateCustomFieldTypeCreateRequestedEvent creates a template custom field type
func (d *EventPortImpl) TemplateCustomFieldTypeCreateRequestedEvent(createRequest cacao_common_service.TemplateCustomFieldTypeModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "EventPortImpl.TemplateCustomFieldTypeCreateRequestedEvent",
		"actor":    createRequest.SessionActor,
		"emulator": createRequest.SessionEmulator,
		"name":     createRequest.Name,
	})
	logger.Infof("Create template custom field type %s", createRequest.Name)
	templateCustomFieldType := types.ConvertCustomFieldTypeFromModel(createRequest)
	actor := cacao_common_service.ActorFromSession(createRequest.Session)

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateCustomFieldType.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	}

	if len(templateCustomFieldType.QueryMethod) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: query method is empty")
	}

	if len(templateCustomFieldType.QueryTarget) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: query target is empty")
	}

	// TODO limit this to admin user

	err := d.Storage.CreateCustomFieldType(templateCustomFieldType)
	if err != nil {
		createFailedEvent := types.TemplateCustomFieldType{
			Name: templateCustomFieldType.Name,
		}
		logger.WithError(err).Error("fail to create custom field type in storage")
		sink.CustomFieldTypeCreateFailed(actor, createFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	createdEvent := types.TemplateCustomFieldType{
		Name: templateCustomFieldType.Name,
	}

	logger.Infof("custom field type created")
	sink.CustomFieldTypeCreated(actor, createdEvent)
	return nil
}

// TemplateCustomFieldTypeUpdateRequestedEvent updates the template custom field type
func (d *EventPortImpl) TemplateCustomFieldTypeUpdateRequestedEvent(updateRequest cacao_common_service.TemplateCustomFieldTypeModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.UpdateTemplateCustomFieldType",
		"actor":    updateRequest.SessionActor,
		"emulator": updateRequest.SessionEmulator,
		"name":     updateRequest.Name,
	})
	logger.Infof("Update template custom field type %s", updateRequest.Name)
	actor := cacao_common_service.ActorFromSession(updateRequest.Session)

	templateCustomFieldType := types.ConvertCustomFieldTypeFromModel(updateRequest)

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "description", "query_method", "query_target", "query_data", "query_result_jsonpath_filter")
	}
	updateFieldNames := updateRequest.UpdateFieldNames

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateCustomFieldType.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	}

	// TODO limit this to admin user

	err := d.Storage.UpdateCustomFieldType(templateCustomFieldType, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.TemplateCustomFieldType{
			Name: templateCustomFieldType.Name,
		}
		logger.WithError(err).Error("fail to update template field type in storage")

		sink.CustomFieldTypeUpdateFailed(actor, updateFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	updatedEvent := types.TemplateCustomFieldType{
		Name: templateCustomFieldType.Name,
	}

	logger.Infof("custom field type updated")
	sink.CustomFieldTypeUpdated(actor, updatedEvent)
	return nil
}

// TemplateCustomFieldTypeDeleteRequestedEvent deletes the template custom field type
func (d *EventPortImpl) TemplateCustomFieldTypeDeleteRequestedEvent(deleteRequest cacao_common_service.TemplateCustomFieldTypeModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.DeleteTemplateCustomFieldType",
		"actor":    deleteRequest.SessionActor,
		"emulator": deleteRequest.SessionEmulator,
		"name":     deleteRequest.Name,
	})
	logger.Infof("Delete template custom field type %s", deleteRequest.Name)
	templateCustomFieldType := types.ConvertCustomFieldTypeFromModel(deleteRequest)
	actor := cacao_common_service.ActorFromSession(deleteRequest.Session)

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateCustomFieldType.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	}

	// TODO limit this to admin user

	templateCustomFieldType = types.TemplateCustomFieldType{
		Name: templateCustomFieldType.Name,
	}
	templateCustomFieldTypeName := templateCustomFieldType.Name

	err := d.Storage.DeleteCustomFieldType(templateCustomFieldTypeName)
	if err != nil {
		deleteFailedEvent := types.TemplateCustomFieldType{
			Name: templateCustomFieldTypeName,
		}
		logger.WithError(err).Error("fail to delete custom field type")
		sink.CustomFieldTypeDeleteFailed(actor, deleteFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	deletedEvent := types.TemplateCustomFieldType{
		Name: templateCustomFieldTypeName,
	}

	logger.Infof("custom field type deleted")
	sink.CustomFieldTypeDeleted(actor, deletedEvent)
	return nil
}
