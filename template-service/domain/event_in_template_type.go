package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// TemplateTypeCreateRequestedEvent creates a template type
func (d *EventPortImpl) TemplateTypeCreateRequestedEvent(createRequest cacao_common_service.TemplateTypeModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":          "template-service.domain",
		"function":         "domain.CreateTemplateType",
		"actor":            createRequest.SessionActor,
		"emulator":         createRequest.SessionEmulator,
		"templateTypeName": createRequest.Name,
	})
	logger.Info("Create template type")
	templateType := types.ConvertTypeFromModel(createRequest)
	actor := cacao_common_service.ActorFromSession(createRequest.Session)

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateType.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	}

	if len(templateType.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: engine is empty")
	}

	if len(templateType.Formats) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: formats are empty")
	}

	if len(templateType.ProviderTypes) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: provider types are empty")
	}

	// TODO limit this to admin user

	err := d.Storage.CreateType(templateType)
	if err != nil {
		createFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}
		logger.WithError(err).Error("fail to create template type in storage")

		sink.TypeCreateFailed(actor, createFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	createdEvent := types.TemplateType{
		Name: templateType.Name,
	}

	logger.Infof("template type created")
	sink.TypeCreated(actor, createdEvent)
	return nil
}

// TemplateTypeUpdateRequestedEvent updates the template type
func (d *EventPortImpl) TemplateTypeUpdateRequestedEvent(updateRequest cacao_common_service.TemplateTypeModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "EventPortImpl.TemplateTypeUpdateRequestedEvent",
		"actor":    updateRequest.SessionActor,
		"emulator": updateRequest.SessionEmulator,
		"name":     updateRequest.Name,
	})
	logger.Infof("Update template type %s", updateRequest.Name)
	templateType := types.ConvertTypeFromModel(updateRequest)
	actor := cacao_common_service.ActorFromSession(updateRequest.Session)
	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "formats", "engine", "provider_types")
	}
	updateFieldNames := updateRequest.UpdateFieldNames

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateType.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	}

	// TODO limit this to admin user

	err := d.Storage.UpdateType(templateType, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}
		logger.WithError(err).Error("fail to update template type in storage")
		sink.TypeUpdateFailed(actor, updateFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	updatedEvent := types.TemplateType{
		Name: templateType.Name,
	}

	logger.Infof("template type updated")
	sink.TypeUpdated(actor, updatedEvent)
	return nil
}

// TemplateTypeDeleteRequestedEvent deletes the template type
func (d *EventPortImpl) TemplateTypeDeleteRequestedEvent(deleteRequest cacao_common_service.TemplateTypeModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "EventPortImpl.TemplateTypeDeleteRequestedEvent",
		"actor":    deleteRequest.SessionActor,
		"emulator": deleteRequest.SessionEmulator,
		"name":     deleteRequest.Name,
	})
	logger.Infof("Delete template type %s", deleteRequest.Name)
	templateType := types.ConvertTypeFromModel(deleteRequest)
	actor := cacao_common_service.ActorFromSession(deleteRequest.Session)

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateType.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	}

	// TODO limit this to admin user

	templateType = types.TemplateType{
		Name: templateType.Name,
	}

	err := d.Storage.DeleteType(templateType.Name)
	if err != nil {
		deleteFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}
		logger.WithError(err).Error("fail to delete template type in storage")

		sink.TypeDeleteFailed(actor, deleteFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	deletedEvent := types.TemplateType{
		Name: templateType.Name,
	}

	logger.Infof("template type deleted")
	sink.TypeDeleted(actor, deletedEvent)
	return nil
}
