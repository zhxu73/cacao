package domain

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/fixtures"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	config                       *types.Config
	Storage                      ports.PersistentStoragePort
	TemplateSource               ports.TemplateSourcePort
	TemplateCustomFieldTypeQuery ports.TemplateCustomFieldTypeQueryPort
	Credential                   ports.CredentialPort
	QueryIn                      ports.IncomingQueryPort
	EventIn                      ports.IncomingEventPort

	queryHandlers ports.IncomingQueryHandlers
	eventHandlers ports.IncomingEventHandlers
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) error {
	d.config = config
	err := d.Storage.Init(config)
	if err != nil {
		return err
	}
	err = d.TemplateSource.Init(config)
	if err != nil {
		return err
	}
	err = d.Credential.Init(config)
	if err != nil {
		return err
	}
	err = d.QueryIn.Init(config)
	if err != nil {
		return err
	}
	err = d.EventIn.Init(config)
	if err != nil {
		return err
	}
	d.queryHandlers = d.NewQueryHandlers()
	d.eventHandlers = d.NewEventHandlers()

	// initialize the fixtures
	dataFixtures := &fixtures.DataFixtures{
		Config:            config,
		IncomingEventPort: d.eventHandlers,
		IncomingQueryPort: d.queryHandlers,
	}
	err = dataFixtures.Start()
	if err != nil {
		return err
	}
	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) error {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg, d.queryHandlers)
	if err != nil {
		return err
	}

	wg.Add(1)
	err = d.EventIn.Start(ctx, &wg, d.eventHandlers)
	if err != nil {
		return err
	}

	wg.Wait()
	return nil
}

// NewQueryHandlers ...
func (d *Domain) NewQueryHandlers() ports.IncomingQueryHandlers {
	return &QueryPortImpl{
		Storage:                      d.Storage,
		TemplateCustomFieldTypeQuery: d.TemplateCustomFieldTypeQuery,
	}
}

// NewEventHandlers ...
func (d *Domain) NewEventHandlers() ports.IncomingEventHandlers {
	return &EventPortImpl{
		Storage:        d.Storage,
		TemplateSource: d.TemplateSource,
		Credential:     d.Credential,
		TimeSrc:        utcTime{},
		IDGenerator: func() common.ID {
			return cacao_common_service.NewTemplateID()
		},
		VersionIDGenerator: types.NewTemplateVersionID,
	}
}

type utcTime struct{}

var _ ports.TimeSrc = (*utcTime)(nil)

// Now ...
func (t utcTime) Now() time.Time {
	return time.Now().UTC()
}
