package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTypes retrieves all template types
func (d *QueryPortImpl) ListTypes(request cacao_common_service.TemplateTypeModel) cacao_common_service.TemplateTypeListModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.ListTypes",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
	})

	logger.Info("List template types")

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateTypeListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			TemplateTypes: nil,
		}
	}

	var templateTypes []types.TemplateType
	var err error
	if len(request.ProviderTypes) > 0 {
		providerTypeFilter := request.ProviderTypes[0] // only uses the 1st one
		templateTypes, err = d.Storage.ListTypesForProviderType(providerTypeFilter)
		if err != nil {
			logger.WithError(err).Errorf("fail to list template types filtered by provider")
			return cacao_common_service.TemplateTypeListModel{
				Session: cacao_common_service.Session{
					SessionActor:    request.SessionActor,
					SessionEmulator: request.SessionEmulator,
					ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
				},
				TemplateTypes: nil,
			}
		}
	} else {
		templateTypes, err = d.Storage.ListTypes()
		if err != nil {
			logger.WithError(err).Errorf("fail to list template types")
			return cacao_common_service.TemplateTypeListModel{
				Session: cacao_common_service.Session{
					SessionActor:    request.SessionActor,
					SessionEmulator: request.SessionEmulator,
					ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
				},
				TemplateTypes: nil,
			}
		}
	}

	models := make([]cacao_common_service.TemplateTypeListItemModel, 0, len(templateTypes))
	for _, templateType := range templateTypes {
		model := types.ConvertTypeToListItemModel(templateType)
		models = append(models, model)
	}
	logger.WithField("len", len(models)).Infof("template type listed")
	return cacao_common_service.TemplateTypeListModel{
		Session:       cacao_common_service.CopySessionActors(request.Session),
		TemplateTypes: models,
	}
}

// ListTypesForProviderType retrieves all template types for the given provider type
func (d *QueryPortImpl) ListTypesForProviderType(request cacao_common_service.TemplateTypeModel) cacao_common_service.TemplateTypeListModel {
	return d.ListTypes(request)
}

// GetType retrieves the template type
func (d *QueryPortImpl) GetType(request cacao_common_service.TemplateTypeModel) cacao_common_service.TemplateTypeModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.GetType",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})
	logger.Infof("Get template type %s", request.Name)

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateTypeModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Name: request.Name,
		}
	}

	if len(request.Name) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template type name is empty")
		return cacao_common_service.TemplateTypeModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Name: request.Name,
		}
	}

	templateType, err := d.Storage.GetType(request.Name)
	if err != nil {
		logger.WithError(err).Errorf("fail to fetch template type from storage")
		return cacao_common_service.TemplateTypeModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Name: request.Name,
		}
	}

	logger.Infof("template type fetched")
	return types.ConvertTypeToModel(cacao_common_service.CopySessionActors(request.Session), templateType)
}
