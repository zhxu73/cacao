package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// List retrieves all templates of the user
func (d *QueryPortImpl) List(request cacao_common_service.TemplateModel) cacao_common_service.TemplateListModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.List",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
	})
	logger.Info("List templates")

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Templates: nil,
		}
	}

	templates, err := d.Storage.List(request.SessionActor, request.IncludeCacaoReservedPurposes)
	if err != nil {
		logger.WithError(err).Error("fail to list template from storage")
		return cacao_common_service.TemplateListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Templates: nil,
		}
	}

	models := make([]cacao_common_service.TemplateListItemModel, 0, len(templates))
	for _, template := range templates {
		model := types.ConvertToListItemModel(template)
		models = append(models, model)
	}

	logger.WithField("len", len(models)).Infof("listed")
	return cacao_common_service.TemplateListModel{
		Session:   cacao_common_service.CopySessionActors(request.Session),
		Templates: models,
	}
}

// Get retrieves the template
func (d *QueryPortImpl) Get(request cacao_common_service.TemplateModel) cacao_common_service.TemplateModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.Get",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"id":       request.ID,
	})

	logger.Infof("Get template %s", request.ID.String())

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			ID:    request.ID,
			Owner: request.SessionActor,
		}
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template ID is empty")
		return cacao_common_service.TemplateModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			ID:    request.ID,
			Owner: request.Owner,
		}
	}

	template, err := d.Storage.Get(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch template from storage")
		return cacao_common_service.TemplateModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			ID:    request.ID,
			Owner: request.SessionActor,
		}
	}
	logger.Infof("template fetched")
	return types.ConvertToModel(cacao_common_service.CopySessionActors(request.Session), template)
}
