package types

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// TemplateType is a struct for storing template type information
type TemplateType struct {
	Name          cacao_common_service.TemplateTypeName       `bson:"_id" json:"name,omitempty"`
	Formats       []cacao_common_service.TemplateFormat       `bson:"formats" json:"formats,omitempty"`
	Engine        cacao_common_service.TemplateEngine         `bson:"engine" json:"engine"`
	ProviderTypes []cacao_common_service.TemplateProviderType `bson:"provider_types" json:"provider_types,omitempty"`
}

// TemplateCustomFieldType is a struct for storing template custom field type information
type TemplateCustomFieldType struct {
	Name                      string                                                  `bson:"_id" json:"name,omitempty"`
	Description               string                                                  `bson:"description" json:"description,omitempty"`
	QueryMethod               cacao_common_service.TemplateCustomFieldTypeQueryMethod `bson:"query_method" json:"query_method,omitempty"`
	QueryTarget               string                                                  `bson:"query_target" json:"query_target,omitempty"`
	QueryData                 string                                                  `bson:"query_data" json:"query_data,omitempty"`
	QueryResultJSONPathFilter string                                                  `bson:"query_result_jsonpath_filter" json:"query_result_jsonpath_filter,omitempty"`
}

// TemplateCustomFieldTypeQueryResult is a struct for storing template custom field type query result
type TemplateCustomFieldTypeQueryResult struct {
	Name     string      `json:"name"`
	DataType string      `json:"data_type"`
	Value    interface{} `json:"value"`
}

// Template is a struct for storing template information
type Template struct {
	ID                      common.ID                               `bson:"_id" json:"id,omitempty"`
	Owner                   string                                  `bson:"owner" json:"owner,omitempty"`
	Name                    string                                  `bson:"name" json:"name,omitempty"`
	Description             string                                  `bson:"description" json:"description,omitempty"`
	Public                  bool                                    `bson:"public" json:"public,omitempty"`
	Deleted                 bool                                    `bson:"deleted" json:"deleted,omitempty"`
	LatestVersionID         common.ID                               `bson:"latest_version_id" json:"latest_version_id"`
	Source                  cacao_common_service.TemplateSource     `bson:"source" json:"source,omitempty"`
	LatestVersionMetadata   cacao_common_service.TemplateMetadata   `bson:"metadata" json:"metadata,omitempty"`
	LatestVersionUIMetadata cacao_common_service.TemplateUIMetadata `bson:"ui_metadata" json:"ui_metadata,omitempty"`
	CreatedAt               time.Time                               `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt               time.Time                               `bson:"updated_at" json:"updated_at,omitempty"`
}

// TemplateVersionIDPrefix is the prefix for XID for template version objects
const TemplateVersionIDPrefix = "templateversion"

// NewTemplateVersionID generates a new ID for TemplateVersion
func NewTemplateVersionID() common.ID {
	return common.NewID(TemplateVersionIDPrefix)
}

// TemplateVersion ...
type TemplateVersion struct {
	ID         common.ID                           `bson:"_id"`
	TemplateID common.ID                           `bson:"template_id"`
	Source     cacao_common_service.TemplateSource `bson:"source" json:"source"`
	Disabled   bool                                `bson:"disabled" json:"disabled"`
	DisabledAt time.Time                           `bson:"disabled_at"`
	// Credential that is needed to get the template from source, empty if not needed
	CredentialID string                                  `bson:"credential_id" json:"credential_id"`
	Metadata     cacao_common_service.TemplateMetadata   `bson:"metadata" json:"metadata"`
	UIMetadata   cacao_common_service.TemplateUIMetadata `bson:"ui_metadata" json:"ui_metadata"`
	CreatedAt    time.Time                               `bson:"created_at" json:"created_at"`
}

// TemplateSync represents an in-progress template syncing operation. One
// template can only have 1 sync operation in progress, this object serves as the
// "lock" for a template to ensure only 1 sync operation happens at a time.
//
// To lock a template insert an object into the sync collection.
// To release the lock for a template, remove the object with the same template ID.
type TemplateSync struct {
	// Use template ID as the primary ID, this way it is easy to lookup and ensure
	// uniqueness on insert.
	TemplateID common.ID `bson:"_id"`
	// This timestamp can help resolve "deadlock" situation where the process/request
	// that acquire the lock "crashed" or lost connection to DB and did not release
	// the lock.
	//
	// For now, let the resolution of the "deadlock" situation be an administrative action.
	SyncStartedAt time.Time `bson:"sync_started_at"`
}
