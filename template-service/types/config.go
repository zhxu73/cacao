package types

import (
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	// NATS
	MessagingConfig messaging2.NatsStanMsgConfig

	// MongoDB
	MongoDBConfig                                cacao_common_db.MongoDBConfig
	TemplateMongoDBCollectionName                string
	TemplateVersionMongoDBCollectionName         string
	TemplateTypeMongoDBCollectionName            string
	TemplateCustomFieldTypeMongoDBCollectionName string
	MongoDBValidationSchema                      bool `envconfig:"MONGODB_VALIDATION_SCHEMA" default:"true"`

	// GIT
	GitRAMSizeLimit int64

	// Cacao
	CacaoAPIURL string `envconfig:"CACAO_API_URL"`
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	// NATS
	if c.MessagingConfig.URL == "" {
		c.MessagingConfig.URL = DefaultNatsURL
	}

	if c.MessagingConfig.ClientID == "" {
		c.MessagingConfig.ClientID = DefaultNatsClientID
	}

	if c.MessagingConfig.MaxReconnects <= 0 {
		c.MessagingConfig.MaxReconnects = DefaultNatsMaxReconnect
	}

	if c.MessagingConfig.ReconnectWait <= 0 {
		c.MessagingConfig.ReconnectWait = DefaultNatsReconnectWait
	}

	if c.MessagingConfig.RequestTimeout <= 0 {
		c.MessagingConfig.RequestTimeout = DefaultNatsRequestTimeout
	}

	if c.MessagingConfig.ClusterID == "" {
		c.MessagingConfig.ClusterID = DefaultNatsClusterID
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}

	if c.TemplateMongoDBCollectionName == "" {
		c.TemplateMongoDBCollectionName = DefaultTemplateMongoDBCollectionName
	}

	if c.TemplateVersionMongoDBCollectionName == "" {
		c.TemplateVersionMongoDBCollectionName = DefaultTemplateVersionMongoDBCollectionName
	}

	if c.TemplateTypeMongoDBCollectionName == "" {
		c.TemplateTypeMongoDBCollectionName = DefaultTemplateTypeMongoDBCollectionName
	}

	if c.TemplateCustomFieldTypeMongoDBCollectionName == "" {
		c.TemplateCustomFieldTypeMongoDBCollectionName = DefaultTemplateCustomFieldTypeMongoDBCollectionName
	}

	// GIT
	if c.GitRAMSizeLimit <= 0 {
		c.GitRAMSizeLimit = DefaultGitRAMSizeLimit
	}

	// Cacao
	if c.CacaoAPIURL == "" {
		c.CacaoAPIURL = DefaultCacaoAPIURL
	}
}

// Override ...
func (c *Config) Override() {
	c.MessagingConfig.QueueGroup = DefaultNatsQueueGroup
	c.MessagingConfig.WildcardSubject = DefaultNatsWildcardSubject
	c.MessagingConfig.DurableName = DefaultNatsDurableName
}
