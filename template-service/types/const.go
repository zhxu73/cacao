package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

const (
	// DefaultNatsURL is a default NATS URL
	DefaultNatsURL = "nats://nats:4222"
	// DefaultNatsMaxReconnect is a default NATS Max Reconnect Count
	DefaultNatsMaxReconnect = 6
	// DefaultNatsReconnectWait is a default NATS Reconnect Wait Time in Seconds
	DefaultNatsReconnectWait = 10
	// DefaultNatsRequestTimeout is a default NATS Request Timeout in Seconds
	DefaultNatsRequestTimeout = 10

	// DefaultNatsQueueGroup is a default NATS Queue Group
	DefaultNatsQueueGroup = "template_queue_group"
	// DefaultNatsWildcardSubject is a default NATS subject to subscribe
	DefaultNatsWildcardSubject = string(cacao_common_service.NatsSubjectTemplate) + ".>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "template-service"
	// DefaultNatsClusterID is a default NATS Cluster ID
	DefaultNatsClusterID = "cacao-cluster"

	// DefaultNatsDurableName is a default NATS Durable Name
	DefaultNatsDurableName = "template_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao-template"
	// DefaultTemplateMongoDBCollectionName is a default MongoDB Collection Name for template
	DefaultTemplateMongoDBCollectionName = "template"
	// DefaultTemplateVersionMongoDBCollectionName is a default MongoDB Collection Name for template versions
	DefaultTemplateVersionMongoDBCollectionName = "template_version"
	// DefaultTemplateTypeMongoDBCollectionName is a default MongoDB Collection Name for template type
	DefaultTemplateTypeMongoDBCollectionName = "template_type"
	// DefaultTemplateCustomFieldTypeMongoDBCollectionName is a default MongoDB Collection Name for template custom field type
	DefaultTemplateCustomFieldTypeMongoDBCollectionName = "template_custom_field_type"

	// DefaultGitRAMSizeLimit is a defaut RAM size limit for git clone
	DefaultGitRAMSizeLimit = 0 // unlimited

	// DefaultCacaoAPIURL is a default Cacao API URL
	DefaultCacaoAPIURL = "http://api.default.svc/api"
)

const (
	// DefaultChannelBufferSize is a default buffer size for a channel used between adapters and domain
	DefaultChannelBufferSize = 100
)
