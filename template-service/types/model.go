package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ConvertTypeFromModel converts TemplateTypeModel to TemplateType
func ConvertTypeFromModel(model cacao_common_service.TemplateTypeModel) TemplateType {
	templateType := TemplateType{
		Name:          model.Name,
		Formats:       model.Formats,
		Engine:        model.Engine,
		ProviderTypes: model.ProviderTypes,
	}

	return templateType
}

// ConvertCustomFieldTypeFromModel converts TemplateCustomFieldTypeModel to TemplateCustomFieldType
func ConvertCustomFieldTypeFromModel(model cacao_common_service.TemplateCustomFieldTypeModel) TemplateCustomFieldType {
	entry := TemplateCustomFieldType{
		Name:                      model.Name,
		Description:               model.Description,
		QueryMethod:               model.QueryMethod,
		QueryTarget:               model.QueryTarget,
		QueryData:                 model.QueryData,
		QueryResultJSONPathFilter: model.QueryResultJSONPathFilter,
	}

	return entry
}

// ConvertFromModel converts TemplateModel to Template
func ConvertFromModel(model cacao_common_service.TemplateModel) Template {
	template := Template{
		ID:                      model.ID,
		Owner:                   model.Owner,
		Name:                    model.Name,
		Description:             model.Description,
		Public:                  model.Public,
		Deleted:                 false,
		LatestVersionID:         "",
		Source:                  model.Source,
		LatestVersionMetadata:   model.Metadata,
		LatestVersionUIMetadata: model.UIMetadata,
		CreatedAt:               model.CreatedAt,
		UpdatedAt:               model.UpdatedAt,
	}

	return template
}

// ConvertTypeToModel converts TemplateType to TemplateTypeModel
func ConvertTypeToModel(session cacao_common_service.Session, templateType TemplateType) cacao_common_service.TemplateTypeModel {
	return cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		Name:          templateType.Name,
		Formats:       templateType.Formats,
		Engine:        templateType.Engine,
		ProviderTypes: templateType.ProviderTypes,
	}
}

// ConvertTypeToListItemModel converts TemplateType to TemplateTypeListItemModel
func ConvertTypeToListItemModel(templateType TemplateType) cacao_common_service.TemplateTypeListItemModel {
	return cacao_common_service.TemplateTypeListItemModel{
		Name:          templateType.Name,
		Formats:       templateType.Formats,
		Engine:        templateType.Engine,
		ProviderTypes: templateType.ProviderTypes,
	}
}

// ConvertCustomFieldTypeToModel converts TemplateCustomFieldType to TemplateCustomFieldTypeModel
func ConvertCustomFieldTypeToModel(session cacao_common_service.Session, templateCustomFieldType TemplateCustomFieldType) cacao_common_service.TemplateCustomFieldTypeModel {
	return cacao_common_service.TemplateCustomFieldTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		Name:                      templateCustomFieldType.Name,
		Description:               templateCustomFieldType.Description,
		QueryMethod:               templateCustomFieldType.QueryMethod,
		QueryTarget:               templateCustomFieldType.QueryTarget,
		QueryData:                 templateCustomFieldType.QueryData,
		QueryResultJSONPathFilter: templateCustomFieldType.QueryResultJSONPathFilter,
	}
}

// ConvertCustomFieldTypeToListItemModel converts TemplateCustomFieldType to TemplateCustomFieldTypeListItemModel
func ConvertCustomFieldTypeToListItemModel(entry TemplateCustomFieldType) cacao_common_service.TemplateCustomFieldTypeListItemModel {
	return cacao_common_service.TemplateCustomFieldTypeListItemModel{
		Name:                      entry.Name,
		Description:               entry.Description,
		QueryMethod:               entry.QueryMethod,
		QueryTarget:               entry.QueryTarget,
		QueryData:                 entry.QueryData,
		QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
	}
}

// ConvertCustomFieldTypeQueryResultToModel converts TemplateCustomFieldTypeQueryResult to TemplateCustomFieldTypeQueryResultModel
func ConvertCustomFieldTypeQueryResultToModel(session cacao_common_service.Session, result TemplateCustomFieldTypeQueryResult) cacao_common_service.TemplateCustomFieldTypeQueryResultModel {
	return cacao_common_service.TemplateCustomFieldTypeQueryResultModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		Name:     result.Name,
		DataType: result.DataType,
		Value:    result.Value,
	}
}

// ConvertToModel converts Template to TemplateModel
func ConvertToModel(session cacao_common_service.Session, template Template) cacao_common_service.TemplateModel {
	return cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		ID:              template.ID,
		Owner:           template.Owner,
		Name:            template.Name,
		Description:     template.Description,
		Public:          template.Public,
		Deleted:         template.Deleted,
		Source:          template.Source,
		LatestVersionID: template.LatestVersionID,
		Metadata:        template.LatestVersionMetadata,
		UIMetadata:      template.LatestVersionUIMetadata,
		CreatedAt:       template.CreatedAt,
		UpdatedAt:       template.UpdatedAt,
		CredentialID:    "",
	}
}

// ConvertToListItemModel converts Template to TemplateListItemModel
func ConvertToListItemModel(template Template) cacao_common_service.TemplateListItemModel {
	return cacao_common_service.TemplateListItemModel{
		ID:              template.ID,
		Owner:           template.Owner,
		Name:            template.Name,
		Description:     template.Description,
		Public:          template.Public,
		Deleted:         template.Deleted,
		Source:          template.Source,
		LatestVersionID: template.LatestVersionID,
		Metadata:        template.LatestVersionMetadata,
		UIMetadata:      template.LatestVersionUIMetadata,
		CreatedAt:       template.CreatedAt,
		UpdatedAt:       template.UpdatedAt,
	}
}

// ConvertVersionToModel converts Template to TemplateModel
func ConvertVersionToModel(session cacao_common_service.Session, version TemplateVersion) cacao_common_service.TemplateVersionModel {
	return cacao_common_service.TemplateVersionModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		TemplateVersion: cacao_common_service.TemplateVersion{
			ID:           version.ID,
			TemplateID:   version.TemplateID,
			Source:       version.Source,
			Disabled:     version.Disabled,
			DisabledAt:   version.DisabledAt,
			CredentialID: version.CredentialID,
			Metadata:     version.Metadata,
			UIMetadata:   version.UIMetadata,
			CreatedAt:    version.CreatedAt,
		},
	}
}

// ConvertVersionToListItemModel converts Template to TemplateListItemModel
func ConvertVersionToListItemModel(version TemplateVersion) cacao_common_service.TemplateVersion {
	return cacao_common_service.TemplateVersion{
		ID:           version.ID,
		TemplateID:   version.TemplateID,
		Source:       version.Source,
		Disabled:     version.Disabled,
		DisabledAt:   version.DisabledAt,
		CredentialID: version.CredentialID,
		Metadata:     version.Metadata,
		UIMetadata:   version.UIMetadata,
		CreatedAt:    version.CreatedAt,
	}
}
