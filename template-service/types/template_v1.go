package types

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// TemplateSourceV1 is a struct for storing template source information
type TemplateSourceV1 struct {
	Type cacao_common_service.TemplateSourceType `bson:"type" json:"type,omitempty"`
	URI  string                                  `bson:"uri" json:"uri,omitempty"`
	// AccessParamters is used to pass additional parameters for accessing source (e.g., git branch name ...)
	// e.g., for git,
	// "branch": "main"
	// "tag": "tag_test"
	// "path": "/sub_dir_to_template_in_repo"
	AccessParameters map[string]interface{}                        `bson:"access_parameters" json:"access_parameters,omitempty"`
	Visibility       cacao_common_service.TemplateSourceVisibility `bson:"source_visibility" json:"source_visibility,omitempty"`
}

// CacaoPostTaskV1 is a struct for a post task in metadata
type CacaoPostTaskV1 struct {
	Type         string `bson:"type" json:"type,omitempty"`
	LocationType string `bson:"location_type" json:"location_type,omitempty"`
}

// TemplateMetadataV1 is a struct for template metadata v1
type TemplateMetadataV1 struct {
	Name             string                                `bson:"name" json:"name,omitempty"`
	Author           string                                `bson:"author" json:"author,omitempty"`
	AuthorEmail      string                                `bson:"author_email" json:"author_email,omitempty"`
	Description      string                                `bson:"description" json:"description,omitempty"`
	TemplateTypeName cacao_common_service.TemplateTypeName `bson:"template_type" json:"template_type,omitempty"`
	Purpose          cacao_common_service.TemplatePurpose  `bson:"purpose" json:"purpose,omitempty"`
	CacaoPostTasks   []CacaoPostTaskV1                     `bson:"cacao_post_tasks" json:"cacao_post_tasks,omitempty"`
	// Parameters stores template parameters
	// e.g.,
	// "instance_count": {"default": 1, "type": "integer"}
	// "instance_name": {"type": "string"},
	Parameters map[string]interface{} `bson:"parameters" json:"parameters,omitempty"`
}

// TemplateV1 is a struct for storing template v1 information
type TemplateV1 struct {
	ID          common.ID          `bson:"_id" json:"id,omitempty"`
	Owner       string             `bson:"owner" json:"owner,omitempty"`
	Name        string             `bson:"name" json:"name,omitempty"`
	Description string             `bson:"description" json:"description,omitempty"`
	Public      bool               `bson:"public" json:"public,omitempty"`
	Source      TemplateSourceV1   `bson:"source" json:"source,omitempty"`
	Metadata    TemplateMetadataV1 `bson:"metadata" json:"metadata,omitempty"`
	CreatedAt   time.Time          `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time          `bson:"updated_at" json:"updated_at,omitempty"`
}

// TemplateUnknown is a struct for testing if template is in v1 or v3(or v2)
type TemplateUnknown struct {
	ID          common.ID                           `bson:"_id" json:"id,omitempty"`
	Owner       string                              `bson:"owner" json:"owner,omitempty"`
	Name        string                              `bson:"name" json:"name,omitempty"`
	Description string                              `bson:"description" json:"description,omitempty"`
	Public      bool                                `bson:"public" json:"public,omitempty"`
	Source      cacao_common_service.TemplateSource `bson:"source" json:"source,omitempty"`
	Metadata    map[string]interface{}              `bson:"metadata" json:"metadata,omitempty"`
	UIMetadata  map[string]interface{}              `bson:"ui_metadata" json:"ui_metadata,omitempty"`
	CreatedAt   time.Time                           `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time                           `bson:"updated_at" json:"updated_at,omitempty"`
}
