package types

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
)

// TemplateChannelRequest is a request struct used between adapters and domain
type TemplateChannelRequest struct {
	Actor                        string
	Emulator                     string
	Data                         interface{}
	Operation                    string
	UpdateFieldNames             []string          // list field names to update, this is used only when operation is Update
	QueryParams                  map[string]string // for query
	CredentialID                 string
	IncludeCacaoReservedPurposes bool
	TransactionID                cacao_common.TransactionID
	Response                     chan TemplateChannelResponse // channel created by adapter(or other caller) and will receive the response
}

// TemplateChannelResponse ...
type TemplateChannelResponse struct {
	Data  interface{}
	Error error
}
