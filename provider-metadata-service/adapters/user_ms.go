package adapters

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
)

// UserSvc implements UserService, this communicates with user service to check if a user is admin.
type UserSvc struct {
	config    *types.Config
	queryConn messaging2.QueryConnection
}

var _ ports.UserService = &UserSvc{}

// NewUserSvc ...
func NewUserSvc(queryConn messaging2.QueryConnection) *UserSvc {
	return &UserSvc{
		queryConn: queryConn,
	}
}

// Init ...
func (u *UserSvc) Init(config *types.Config) error {
	u.config = config
	return nil
}

// CheckForAdmin communicates with user service to check if a user is admin.
// Return error if check fails or user is NOT admin.
func (u *UserSvc) CheckForAdmin(actor service.Actor) (actorIsAdmin bool, err error) {

	userclient, err := service.NewNatsUserClientFromConn(u.queryConn, nil)
	if err != nil {
		return false, err
	}
	if actor.Actor == "" {
		return false, fmt.Errorf("actor not specified")
	}
	log.Debug("about to userclient.Get")
	user, err := userclient.Get(context.TODO(), actor, actor.Actor, false)
	log.Debug("back from userclient.Get")
	if err != nil {
		return false, err
	}
	return user.IsAdmin, nil
}
