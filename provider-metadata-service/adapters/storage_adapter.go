package adapters

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config             *types.Config
	Store              db.ObjectStore
	providerCollection *mongo.Collection
}

var _ ports.ProviderStoragePort = (*MongoAdapter)(nil)

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Init",
	})
	logger.Info("Initializing Mongo Adapter")
	if config.ProviderMongoDBCollectionName == "" {
		return fmt.Errorf("mongo collection not specified in config")
	}
	adapter.Config = config

	store, err := db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("Unable to connect to MongoDB")
		return err
	}
	adapter.Store = store
	adapter.providerCollection = store.Connection.Database.Collection(config.ProviderMongoDBCollectionName)

	models := []mongo.IndexModel{
		// index on owner field for faster lookup (list operation)
		{
			Keys: bson.M{
				"owner": db.AscendingSort,
			},
			Options: options.Index().SetName("owner"),
		},
		// index on public field for faster lookup (list operation)
		{
			Keys: bson.M{
				"public": db.DescendingSort,
			},
			Options: options.Index().SetName("public"),
		},
		// restrict uniqueness on provider name for public providers, public provider must have unique name.
		{
			Keys: bson.M{
				"name": db.AscendingSort,
			},
			Options: options.Index().SetName("unique_public_provider_name").SetUnique(true).SetPartialFilterExpression(bson.M{
				"public": true,
			}),
		},
		// restrict uniqueness on provider name, user cannot have multiple provider with same name.
		{
			Keys: bson.D{
				{
					Key:   "owner",
					Value: db.AscendingSort,
				},
				{
					Key:   "name",
					Value: db.AscendingSort,
				},
			},
			Options: options.Index().SetName("owner_name").SetUnique(true),
		},
	}
	indexNames, err := store.Connection.Database.Collection(config.ProviderMongoDBCollectionName).Indexes().CreateMany(context.TODO(), models)
	if err != nil {
		return err
	}
	logger.WithField("names", indexNames).Info("created indices on collection")

	return nil
}

// Close finalizes mongodb adapter
func (adapter *MongoAdapter) Close() error {
	return adapter.Store.Release()
}

// Create inserts a Provider
func (adapter *MongoAdapter) Create(actor service.Actor, provider types.Provider) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Create",
	})
	if provider.Type == types.ProviderTypeForTesting {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("bad provider type, %s", types.ProviderTypeForTesting))
	}

	err := adapter.Store.Insert(adapter.Config.ProviderMongoDBCollectionName, provider)
	if err != nil {
		if db.IsDuplicateError(err) {
			errorMessage := "unable to insert a provider due to conflict"
			logger.WithError(err).Error(errorMessage)
			return service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := "unable to insert a provider"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	return nil
}

// Delete deletes a Provider
func (adapter *MongoAdapter) Delete(actor service.Actor, providerID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	p := types.Provider{}

	err := adapter.Store.Get(adapter.Config.ProviderMongoDBCollectionName, string(providerID), &p)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", providerID)
		return types.NewCacaoNotFoundError(providerID)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.ProviderMongoDBCollectionName, string(providerID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to delete a provider for id %s", providerID)
		return fmt.Errorf("unable to delete a provider for id %s", providerID)
	}

	if !deleted {
		logger.Errorf("unable to delete a provider for id %s", providerID)
		return service.NewCacaoNotFoundError(fmt.Sprintf("unable to delete a provider for id %s", providerID))
	}

	return nil
}

// Get returns the Provider with the ID
func (adapter *MongoAdapter) Get(providerID common.ID) (types.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Get",
	})

	p := types.Provider{}

	err := adapter.Store.Get(adapter.Config.ProviderMongoDBCollectionName, string(providerID), &p)
	if err == mongo.ErrNoDocuments {
		logger.WithFields(log.Fields{"error": err}).Errorf("provider %s does not exists", providerID)
		return p, types.NewCacaoNotFoundError(providerID)
	}
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", providerID)
		return p, fmt.Errorf("unable to get a provider for id %s", providerID)
	}

	return p, nil
}

const maxListCount = 1000

// ListPublicAndOwnedByUser returns Providers
func (adapter *MongoAdapter) ListPublicAndOwnedByUser(actor string, skip, limit uint) ([]types.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.List",
		"actor":    actor,
	})
	if limit == 0 {
		return []types.Provider{}, nil
	}
	if limit > maxListCount {
		limit = maxListCount
	}
	filter := map[string]interface{}{
		"$or": []interface{}{
			map[string]string{"owner": actor},
			map[string]bool{"public": true},
		},
	}
	var limit64 = int64(limit)
	var skip64 = int64(skip)
	result, err := adapter.providerCollection.Find(context.TODO(), filter, &options.FindOptions{
		Skip:  &skip64,
		Limit: &limit64,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to list providers")
		return nil, fmt.Errorf("unable to list providers")
	}
	var providers []types.Provider
	err = result.All(context.TODO(), &providers)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to list providers")
		return nil, fmt.Errorf("unable to list providers")
	}
	return providers, nil
}

// ListAll ...
func (adapter *MongoAdapter) ListAll(skip, limit uint) ([]types.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.ListAll",
	})

	if limit == 0 {
		return []types.Provider{}, nil
	}
	if limit > maxListCount {
		limit = maxListCount
	}
	var limit64 = int64(limit)
	var skip64 = int64(skip)
	result, err := adapter.providerCollection.Find(context.TODO(), bson.M{}, &options.FindOptions{
		Skip:  &skip64,
		Limit: &limit64,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to list providers")
		return nil, fmt.Errorf("unable to list providers")
	}
	var providers []types.Provider
	err = result.All(context.TODO(), &providers)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to list providers")
		return nil, fmt.Errorf("unable to list providers")
	}
	return providers, nil
}

// Update updates/edits a Provider
func (adapter *MongoAdapter) Update(providerID common.ID, update types.ProviderUpdate) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Update",
		"provider": providerID,
	})

	result, err := adapter.providerCollection.UpdateOne(context.TODO(), bson.M{"_id": providerID.String()}, bson.M{
		"$set": update.ToBSON(),
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", providerID)
		return err
	}
	if result.MatchedCount == 0 {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", providerID)
		return types.NewCacaoNotFoundError(providerID)
	}
	if result.ModifiedCount > 0 {
		logger.Info("provider modified")
	}
	return nil
}
