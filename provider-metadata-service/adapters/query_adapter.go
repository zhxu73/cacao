package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"sync"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	conn     messaging2.QueryConnection
	handlers ports.IncomingQueryHandlers
}

// NewQueryAdapter ...
func NewQueryAdapter(conn messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{
		conn: conn,
	}
}

// SetHandlers ...
func (adapter *QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	err := adapter.conn.Listen(ctx, map[common.QueryOp]messaging2.QueryHandlerFunc{
		service.ProviderGetQueryOp:  adapter.handleProviderGetQuery,
		service.ProviderListQueryOp: adapter.handleProviderListQuery,
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

func (adapter *QueryAdapter) handleProviderGetQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.handleProviderGetQuery",
	})

	var getRequest service.ProviderRequest
	err := json.Unmarshal(requestCe.Data(), &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderRequest"
		logger.WithError(err).Error(errorMessage)
		return
	}

	response := adapter.handlers.Get(ctx, service.ActorFromSession(getRequest.Session), getRequest.ID)
	replyCe, err := messaging2.CreateCloudEventWithAutoSource(response, common.QueryOp(requestCe.Type()))
	if err != nil {
		logger.WithError(err).Error("unable to create cloudevent from ServiceRequestResult")
		return
	}
	err = writer.Write(replyCe)
	if err != nil {
		logger.WithError(err).Error("fail to write reply")
		return
	}
}

func (adapter *QueryAdapter) handleProviderListQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.handleProviderListQuery",
	})
	var request service.ProviderRequest
	err := json.Unmarshal(requestCe.Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderRequest"
		logger.WithError(err).Error(errorMessage)
		return
	}

	result := adapter.handlers.List(ctx, service.ActorFromSession(request.Session))
	replyCe, err := messaging2.CreateCloudEventWithAutoSource(result, common.QueryOp(requestCe.Type()))
	if err != nil {
		logger.WithError(err).Error("unable to create cloudevent from ServiceRequestResult")
		return
	}
	err = writer.Write(replyCe)
	if err != nil {
		logger.WithError(err).Error("fail to write reply")
		return
	}
}
