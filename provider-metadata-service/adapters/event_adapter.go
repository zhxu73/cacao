package adapters

import (
	"context"
	"encoding/json"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
)

// EventAdapter communicates to IncomingEventPort
type EventAdapter struct {
	handlers       ports.IncomingEventHandlers
	conn           messaging2.EventConnection
	EventWaitGroup sync.WaitGroup
}

var _ ports.IncomingEventPort = (*EventAdapter)(nil)

// NewEventAdapter ...
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{
		handlers:       nil,
		conn:           conn,
		EventWaitGroup: sync.WaitGroup{},
	}
}

// SetHandlers ...
func (adapter *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Start",
	})
	logger.Info("Starting Event Adapter")

	return adapter.conn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		service.ProviderCreateRequestedEvent: eventHandlerWrapper(adapter.handlers.Create),
		service.ProviderDeleteRequestedEvent: eventHandlerWrapper(adapter.handlers.Delete),
		service.ProviderUpdateRequestedEvent: eventHandlerWrapper(adapter.handlers.Update),
	}, wg)
}

func eventHandlerWrapper(handler func(service.ProviderRequest, ports.OutgoingEventPort)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request service.ProviderRequest
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			return err
		}
		handler(request, eventOut{writer: writer})
		return nil
	}
}

// implements OutgoingEventPort
type eventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventPort = (*eventOut)(nil)

// Created publishes ProviderCreatedEvent
func (out eventOut) Created(response service.ProviderResponse) {
	const eventType = service.ProviderCreatedEvent
	out.write(response, eventType)
}

// CreateFailed publishes ProviderCreateFailedEvent
func (out eventOut) CreateFailed(response service.ProviderResponse) {
	const eventType = service.ProviderCreateFailedEvent
	out.write(response, eventType)
}

// Updated publishes ProviderUpdatedEvent
func (out eventOut) Updated(response service.ProviderResponse) {
	const eventType = service.ProviderUpdatedEvent
	out.write(response, eventType)
}

// UpdateFailed publishes ProviderUpdateFailedEvent
func (out eventOut) UpdateFailed(response service.ProviderResponse) {
	const eventType = service.ProviderUpdateFailedEvent
	out.write(response, eventType)
}

// Deleted publishes ProviderDeletedEvent
func (out eventOut) Deleted(response service.ProviderResponse) {
	const eventType = service.ProviderDeletedEvent
	out.write(response, eventType)
}

// DeleteFailed publishes ProviderDeleteFailedEvent
func (out eventOut) DeleteFailed(response service.ProviderResponse) {
	const eventType = service.ProviderDeleteFailedEvent
	out.write(response, eventType)
}

func (out eventOut) write(response service.ProviderResponse, eventType common.EventType) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "eventOut.write",
		"ceType":   eventType,
	})
	ce, err := messaging2.CreateCloudEventWithAutoSource(response, eventType)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return
	}
	err = out.writer.Write(ce)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", eventType)
	}
}
