package adapters

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"os"
	"testing"
	"time"
)

func loadMongoConfigFromEnv() types.Config {
	var config = types.Config{
		LogLevel:                      "",
		OnlyAdmins:                    false,
		Messaging:                     messaging2.NatsStanMsgConfig{},
		MongoDBConfig:                 cacao_common_db.MongoDBConfig{},
		ProviderMongoDBCollectionName: types.DefaultProviderMongoDBCollectionName,
	}
	err := envconfig.Process("", &config.MongoDBConfig)
	if err != nil {
		panic(err)
	}
	config.MongoDBConfig.DBName = "cacao"
	return config
}

func TestMongoAdapter(t *testing.T) {
	if env, ok := os.LookupEnv("CI_INTEGRATION_MONGO"); !ok || env != "true" {
		t.Skip("CI_INTEGRATION_MONGO!='true' in env var, skip the integration test")
		return
	}
	config := loadMongoConfigFromEnv()
	testStorageAdapter(t, func() ports.ProviderStoragePort {
		var adapter MongoAdapter
		err := adapter.Init(&config)
		if err != nil {
			panic(err)
		}
		return &adapter
	}, func(storage ports.ProviderStoragePort) {
		adapter := storage.(*MongoAdapter)
		err := adapter.Close()
		if err != nil {
			panic(err)
		}
		conn, err := cacao_common_db.NewMongoDBConnection(&config.MongoDBConfig)
		if err != nil {
			panic(err)
		}
		err = conn.Database.Collection(config.ProviderMongoDBCollectionName).Drop(context.Background())
		if err != nil {
			panic(err)
		}
	})
}

func testStorageAdapter(t *testing.T, setup func() ports.ProviderStoragePort, cleanup func(port ports.ProviderStoragePort)) {
	t.Run("Get non-existent", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		provider, err := storage.Get("provider-d17e30598850n9abikl0")
		assert.Equal(t, types.Provider{}, provider)
		assertCacaoErrorOfType(t, err, service.CacaoNotFoundErrorMessage)
	})
	t.Run("Create & Get", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		provider := types.Provider{
			ID:             "provider-d17e30598850n9abikl0",
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}

		err := storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			return
		}

		fetchedProvider, err := storage.Get("provider-d17e30598850n9abikl0")
		if !assert.NoError(t, err) {
			return
		}
		compareProviderWithoutTime(t, provider, fetchedProvider)
	})
	t.Run("Create & Get & Delete & Get", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		provider := types.Provider{
			ID:             "provider-d17e30598850n9abikl0",
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}

		err := storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			return
		}

		fetchedProvider, err := storage.Get("provider-d17e30598850n9abikl0")
		if !assert.NoError(t, err) {
			return
		}
		compareProviderWithoutTime(t, provider, fetchedProvider)

		err = storage.Delete(service.Actor{
			Actor:    "testuser123",
			Emulator: "",
		}, "provider-d17e30598850n9abikl0")
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch after delete
		fetchedProvider, err = storage.Get("provider-d17e30598850n9abikl0")
		assert.Equal(t, types.Provider{}, fetchedProvider)
		assertCacaoErrorOfType(t, err, service.CacaoNotFoundErrorMessage)
	})
	t.Run("Create private provider name uniqueness conflict", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		originalProvider := types.Provider{
			ID:             common.NewID("provider"),
			Name:           "test_provider1",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		provider := originalProvider
		err := storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			return
		}

		// re-create same provider but diff ID
		provider = originalProvider
		provider.ID = common.NewID("provider")
		err = storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.Error(t, err) {
			// should fail, provider name is still the same
			return
		}

		// re-create same provider but diff ID AND diff name
		provider = originalProvider
		provider.ID = common.NewID("provider")
		provider.Name = common.NewID("name").String()
		err = storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			// owned by the same user, but provider name is different
			return
		}

		// re-create same provider but diff ID AND diff owner
		provider = originalProvider
		provider.ID = common.NewID("provider")
		provider.Owner = "testuser456"
		err = storage.Create(
			service.Actor{
				Actor:    provider.Owner,
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			// provider name is the same, but owned by the same user and both are private provider
			return
		}
	})
	t.Run("Create public provider name uniqueness conflict", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		originalProvider := types.Provider{
			ID:             common.NewID("provider"),
			Name:           "test_provider1",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         true,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		provider := originalProvider
		err := storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			return
		}

		// re-create same provider but diff ID
		provider = originalProvider
		provider.ID = common.NewID("provider")
		err = storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.Error(t, err) {
			// should fail, provider name is still the same
			return
		}

		// re-create provider with same name, but diff ID AND private
		provider = originalProvider
		provider.ID = common.NewID("provider")
		provider.Public = false
		err = storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.Error(t, err) {
			// should fail, provider name should be unique if owned by same user, there is
			// already a public provider owned by the same user.
			return
		}

		// re-create provider with same name, but diff ID AND diff owner AND private
		provider = originalProvider
		provider.ID = common.NewID("provider")
		provider.Owner = "testuser456"
		provider.Public = false
		err = storage.Create(
			service.Actor{
				Actor:    provider.Owner,
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			return
		}

		// re-create provider with same name, but diff ID AND diff owner
		provider = originalProvider
		provider.ID = common.NewID("provider")
		provider.Owner = "testuser456"
		err = storage.Create(
			service.Actor{
				Actor:    provider.Owner,
				Emulator: "",
			},
			provider,
		)
		if !assert.Error(t, err) {
			// should fail, public provider name should be globally unique, owner does not matter
			return
		}
	})
	t.Run("Delete non-existent", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		err := storage.Delete(service.Actor{
			Actor:    "testuser123",
			Emulator: "",
		}, "provider-d17e30598850n9abikl0")
		assertCacaoErrorOfType(t, err, service.CacaoNotFoundErrorMessage)
	})
	t.Run("Empty list", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		list, err := storage.ListAll(0, 100)
		assert.NoError(t, err)
		assert.Len(t, list, 0)
	})
	t.Run("Create N & List", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		const N = 100

		var toBeCreated = make([]types.Provider, 0, N)
		for i := 0; i < N; i++ {
			now := time.Now().UTC()
			toBeCreated = append(
				toBeCreated,
				types.Provider{
					ID:             common.NewID("provider-openstack"),
					Name:           common.NewID("test_provider123_").String(), // generate unique name
					Type:           "openstack",
					URL:            "https://cyverse.org",
					Owner:          "testuser123",
					Public:         false,
					Shared:         false,
					FromDeployment: "",
					CreatedAt:      now,
					UpdatedAt:      now,
					Metadata:       nil,
				})
		}

		for i := 0; i < N; i++ {
			err := storage.Create(
				service.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
				toBeCreated[i],
			)
			if !assert.NoError(t, err) {
				return
			}
		}

		listed, err := storage.ListPublicAndOwnedByUser("testuser123", 0, N+1)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, listed)
		assert.Len(t, listed, N)
		for _, provider := range listed {
			var matched uint
			for i := 0; i < N; i++ {
				if provider.ID == toBeCreated[i].ID {
					matched++
				}
			}
			assert.Equalf(t, uint(1), matched, "ID should only match 1 provider ID that was fed into Create(), %s", provider.ID)
		}
	})
	t.Run("List with skip and limit", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		const N = 100
		const limit = 10
		const skip = 5

		var toBeCreated = make([]types.Provider, 0, N)
		for i := 0; i < N; i++ {
			now := time.Now().UTC()
			toBeCreated = append(
				toBeCreated,
				types.Provider{
					ID:             common.NewID("provider-openstack"),
					Name:           common.NewID("test_provider123_").String(), // generate unique name
					Type:           "openstack",
					URL:            "https://cyverse.org",
					Owner:          "testuser123",
					Public:         false,
					Shared:         false,
					FromDeployment: "",
					CreatedAt:      now,
					UpdatedAt:      now,
					Metadata:       nil,
				})
		}

		for i := 0; i < N; i++ {
			err := storage.Create(
				service.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
				toBeCreated[i],
			)
			if !assert.NoError(t, err) {
				return
			}
		}

		listed, err := storage.ListAll(skip, limit)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, listed)
		assert.Len(t, listed, limit)
		for _, provider := range listed {
			var matched uint
			for i := skip; i < limit+skip; i++ {
				if provider.ID == toBeCreated[i].ID {
					matched++
				}
			}
			assert.Equalf(t, uint(1), matched, "ID should only match 1 provider ID that was fed into Create(), %s", provider.ID)
		}
	})
	t.Run("ListPublicAndOwnedByUser", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		// public provider belong to self
		selfPublicProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         true,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		// private provider belong to self
		selfPrivateProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123_diff",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		// private provider belong to other user
		otherUserPrivateProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "otheruser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		// public provider that belong to other user
		otherUserPublicProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123_diff",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "otheruser123",
			Public:         true,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}

		for _, p := range []types.Provider{selfPublicProvider, selfPrivateProvider, otherUserPrivateProvider, otherUserPublicProvider} {
			err := storage.Create(
				service.Actor{
					Actor:    p.Owner,
					Emulator: "",
				},
				p,
			)
			if !assert.NoError(t, err) {
				return
			}
		}

		listed, err := storage.ListPublicAndOwnedByUser("testuser123", 0, 100)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, listed)
		if !assert.Len(t, listed, 3) {
			return
		}
		// list should be sorted by ID, so they are appearing in the order of insertion
		compareProviderWithoutTime(t, selfPublicProvider, listed[0])
		compareProviderWithoutTime(t, selfPrivateProvider, listed[1])
		compareProviderWithoutTime(t, otherUserPublicProvider, listed[2])
	})
	t.Run("ListAll", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		// public provider belong to self
		selfPublicProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         true,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		// private provider belong to self
		selfPrivateProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123_diff",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		// private provider belong to other user
		otherUserPrivateProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "otheruser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}
		// public provider that belong to other user
		otherUserPublicProvider := types.Provider{
			ID:             common.NewID("provider-openstack"),
			Name:           "test_provider123_diff",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "otheruser123",
			Public:         true,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}

		for _, p := range []types.Provider{selfPublicProvider, selfPrivateProvider, otherUserPrivateProvider, otherUserPublicProvider} {
			err := storage.Create(
				service.Actor{
					Actor:    p.Owner,
					Emulator: "",
				},
				p,
			)
			if !assert.NoError(t, err) {
				return
			}
		}

		listed, err := storage.ListAll(0, 100)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, listed)
		// all 4 provider are listed regardless of public/private and owner
		if !assert.Len(t, listed, 4) {
			return
		}
	})
	t.Run("Update non-existent", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		err := storage.Update(
			"provider-d17e30598850n9abikl0",
			types.ProviderUpdate{
				Name: stringPtr("new_name123"),
				Type: (*service.ProviderType)(stringPtr("new_type123")),
				URL:  stringPtr("https://new-url.cyverse.org"),
				Metadata: map[string]interface{}{
					"key1": "val1",
				},
				UpdatedAt: now,
				UpdatedBy: "testuser123",
			},
		)
		assertCacaoErrorOfType(t, err, service.CacaoNotFoundErrorMessage)
	})
	t.Run("Create & Update & Get", func(t *testing.T) {
		storage := setup()
		defer cleanup(storage)

		now := time.Now().UTC()
		provider := types.Provider{
			ID:             "provider-d17e30598850n9abikl0",
			Name:           "test_provider123",
			Type:           "openstack",
			URL:            "https://cyverse.org",
			Owner:          "testuser123",
			Public:         false,
			Shared:         false,
			FromDeployment: "",
			CreatedAt:      now,
			UpdatedAt:      now,
			Metadata:       nil,
		}

		err := storage.Create(
			service.Actor{
				Actor:    "testuser123",
				Emulator: "",
			},
			provider,
		)
		if !assert.NoError(t, err) {
			return
		}
		now = time.Now().UTC()
		err = storage.Update(
			common.ID("provider-d17e30598850n9abikl0"),
			types.ProviderUpdate{
				Name: stringPtr("new_name123"),
				Type: (*service.ProviderType)(stringPtr("new_type123")),
				URL:  stringPtr("https://new-url.cyverse.org"),
				Metadata: map[string]interface{}{
					"key1": "val1",
				},
				UpdatedAt: now,
				UpdatedBy: "testuser123",
			},
		)
		if !assert.NoError(t, err) {
			return
		}

		expectedUpdatedProvider := provider
		expectedUpdatedProvider.Name = "new_name123"
		fetchedProvider, err := storage.Get("provider-d17e30598850n9abikl0")
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, provider.ID, fetchedProvider.ID)
		assert.Equal(t, "new_name123", fetchedProvider.Name)
		assert.Equal(t, "new_type123", string(fetchedProvider.Type))
		assert.Equal(t, "https://new-url.cyverse.org", fetchedProvider.URL)
		assert.Equal(t, provider.Owner, fetchedProvider.Owner)
		assert.Equal(t, provider.Public, fetchedProvider.Public)
		assert.Equal(t, provider.Shared, fetchedProvider.Shared)
		assert.Equal(t, applyTimePrecision(provider.CreatedAt), fetchedProvider.CreatedAt)
		// update timestamp should be later than creation timestamp
		assert.GreaterOrEqualf(t, fetchedProvider.UpdatedAt.Sub(fetchedProvider.CreatedAt), time.Duration(0), "%v, %v", fetchedProvider.UpdatedAt, fetchedProvider.CreatedAt)
		// new update timestamp should be later than original one
		assert.GreaterOrEqualf(t, fetchedProvider.UpdatedAt.Sub(applyTimePrecision(provider.UpdatedAt)), time.Duration(0), "%v, %v", fetchedProvider.UpdatedAt, applyTimePrecision(provider.UpdatedAt))
		assert.Equal(t, map[string]interface{}{
			"key1": "val1",
		}, fetchedProvider.Metadata)
	})
}

func assertCacaoErrorOfType(t *testing.T, err error, msg service.CacaoStandardErrorMessage) {
	if !assert.Error(t, err) {
		return
	}
	cErr, ok := err.(service.CacaoError)
	if !assert.Truef(t, ok, "error not CacaoError, %v", err) {
		return
	}
	assert.Equal(t, msg, cErr.StandardError())
}

func compareProviderWithoutTime(t *testing.T, provider1, provider2 types.Provider) {
	provider1.CreatedAt = time.Time{}
	provider1.UpdatedAt = time.Time{}
	provider2.CreatedAt = time.Time{}
	provider2.UpdatedAt = time.Time{}
	assert.Equal(t, provider1, provider2)
}

// there is limited precision in mongodb timestamp
func applyTimePrecision(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond()/1000000*1000000, t.Location())
}

func stringPtr(s string) *string {
	return &s
}
