package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"net/url"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// EventHandlers implements  IncomingEventHandlers
type EventHandlers struct {
	userSvc     ports.UserService
	storage     ports.ProviderStoragePort
	timeSrc     func() time.Time
	idGenerator func(providerType service.ProviderType) common.ID
}

var _ ports.IncomingEventHandlers = &EventHandlers{}

// Create creates a Provider
func (h *EventHandlers) Create(request service.ProviderRequest, eventOut ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "EventHandlers.Create",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})
	logger.Info()
	actorIsAdmin, err := h.userSvc.CheckForAdmin(service.ActorFromSession(request.Session))
	if err != nil {
		logger.WithError(err).Error("fail to check authorization")
		eventOut.CreateFailed(h.creationErrorResponse(request, err))
		return
	}
	if !actorIsAdmin && request.Public {
		// only admin user can create public provider
		logger.WithError(err).Error("user not admin")
		eventOut.CreateFailed(h.creationErrorResponse(request, service.NewCacaoUnauthorizedError("user is not admin")))
		return
	}

	if err := validateMetadata(&request.ProviderModel); err != nil {
		logger.WithError(err).Error("fail to validate provider metadata")
		eventOut.CreateFailed(h.creationErrorResponse(request, service.NewCacaoInvalidParameterError(err.Error())))
		return
	}

	if err := h.validateCreation(request); err != nil {
		logger.WithError(err).Error("bad request")
		eventOut.CreateFailed(h.creationErrorResponse(request, err))
		return
	}

	now := h.timeSrc().UTC()

	provider := request.ProviderModel
	p := service.ProviderModel{
		ID:        h.idGenerator(provider.Type),
		Name:      provider.Name,
		Type:      provider.Type,
		URL:       provider.URL,
		Owner:     request.SessionActor,
		Public:    provider.Public,
		CreatedAt: now,
		UpdatedAt: now,
		Metadata:  provider.Metadata,
	}

	err = h.storage.Create(service.ActorFromSession(request.Session), types.FromServiceModel(p))
	if err != nil {
		logger.WithError(err).Error("fail to create in storage")
		eventOut.CreateFailed(h.creationErrorResponse(request, err))
		return
	}
	logger.WithField("id", p.ID).Info("provider created")
	eventOut.Created(service.ProviderResponse{
		Session:       service.CopySessionActors(request.Session),
		ProviderModel: p,
	})
}

func (h *EventHandlers) validateCreation(request service.ProviderRequest) service.CacaoError {
	if len(request.ProviderModel.Name) == 0 {
		return service.NewCacaoInvalidParameterError("Creation input validation error: Name is empty")
	}
	if len(request.ProviderModel.Name) > types.MaxProviderNameLength {
		return service.NewCacaoInvalidParameterError("Creation input validation error: Name is too long")
	}
	if len(request.ProviderModel.Type) == 0 {
		return service.NewCacaoInvalidParameterError("Creation input validation error: Type is empty")
	}
	if len(request.ProviderModel.Type) > types.MaxProviderTypeLength {
		return service.NewCacaoInvalidParameterError("Creation input validation error: Type is too long")
	}
	if len(request.ProviderModel.URL) == 0 {
		return service.NewCacaoInvalidParameterError("Creation input validation error: URL is empty")
	}
	if len(request.ProviderModel.URL) > types.MaxProviderURLLength {
		return service.NewCacaoInvalidParameterError("Creation input validation error: URL is too long")
	}
	_, err := url.Parse(request.URL)
	if err != nil {
		return service.NewCacaoInvalidParameterError("Creation input validation error: URL is invalid")
	}
	return nil
}

func (h *EventHandlers) creationErrorResponse(request service.ProviderRequest, err error) service.ProviderResponse {
	return service.ProviderResponse{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.ToCacaoError(err).GetBase(),
		},
		ProviderModel: service.ProviderModel{
			Name: request.Name,
		},
	}
}

// Generate ID for new provider to be created. this will generate ID that look
// like "provider-aws-aaaaaaaaaaaaaaaaaaaa".
//
// this is used to extract the behavior so that handler become deterministic and
// easier to test.
func providerIDGenerator(providerType service.ProviderType) common.ID {
	return common.NewID(string("provider-" + providerType))
}

// Update updates the Provider
func (h *EventHandlers) Update(request service.ProviderRequest, eventOut ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "EventHandlers.Update",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"id":       request.ID,
	})

	if err := h.validateUpdate(request); err != nil {
		logger.WithError(err).Error("bad request")
		eventOut.UpdateFailed(h.updateErrorResponse(request, err))
		return
	}

	actorIsAdmin, err := h.userSvc.CheckForAdmin(service.ActorFromSession(request.Session))
	if err != nil {
		logger.WithError(err).Error("fail to check authorization")
		eventOut.UpdateFailed(h.updateErrorResponse(request, err))
		return
	}
	fetchedProvider, err := h.storage.Get(request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch provider before update")
		eventOut.UpdateFailed(h.updateErrorResponse(request, err))
		return
	}
	if err := h.authorizedToUpdate(request, actorIsAdmin, fetchedProvider); err != nil {
		logger.WithError(err).Error("user is not authorized to update")
		eventOut.UpdateFailed(h.updateErrorResponse(request, err))
		return
	}

	var update types.ProviderUpdate
	var fieldCount = 0
	if request.ProviderModel.Name != "" {
		update.Name = &request.ProviderModel.Name
		fieldCount++
	}
	if request.ProviderModel.URL != "" {
		update.URL = &request.ProviderModel.URL
		fieldCount++
	}
	if request.ProviderModel.Type != "" {
		update.Type = &request.ProviderModel.Type
		fieldCount++
	}
	if request.ProviderModel.Metadata != nil {
		update.Metadata = request.ProviderModel.Metadata
		fieldCount++
	}
	if request.ProviderModel.Public {
		update.Public = &request.ProviderModel.Public
		fieldCount++
	}

	if fieldCount == 0 {
		logger.Info("no field to update")
		eventOut.Updated(service.ProviderResponse{
			Session:       service.CopySessionActors(request.Session),
			ProviderModel: request.ProviderModel,
		})
		return
	}
	update.UpdatedBy = request.SessionActor
	update.UpdatedAt = h.timeSrc().UTC()

	err = h.storage.Update(request.ID, update)
	if err != nil {
		logger.WithError(err).Error("fail to update in storage")
		eventOut.UpdateFailed(h.updateErrorResponse(request, err))
		return
	}
	logger.WithField("updateFieldCount", fieldCount).Info("provider updated")
	eventOut.Updated(service.ProviderResponse{
		Session: service.CopySessionActors(request.Session),
		ProviderModel: service.ProviderModel{
			ID:        request.ID,
			Name:      request.Name,
			Type:      request.Type,
			URL:       request.URL,
			Owner:     request.Owner,
			Public:    request.Public,
			CreatedAt: fetchedProvider.CreatedAt,
			UpdatedAt: update.UpdatedAt,
			Metadata:  request.Metadata,
		},
	})
}

func (h *EventHandlers) authorizedToUpdate(request service.ProviderRequest, actorIsAdmin bool, provider types.Provider) service.CacaoError {
	if actorIsAdmin {
		return nil
	}
	if provider.Owner == request.SessionActor {
		// non-admin user can update the provider they owned, but they cannot make a private provider public
		if request.Public {
			return service.NewCacaoUnauthorizedError("user is not authorized")
		}
		return nil
	}
	// when non-admin user try to update other's provider, we return different error based on whether they can access the provider or not (public/private).
	if provider.Public {
		return service.NewCacaoUnauthorizedError("user is not authorized")
	}
	return types.NewCacaoNotFoundError(provider.ID)
}

func (h *EventHandlers) validateUpdate(request service.ProviderRequest) service.CacaoError {
	if len(request.SessionActor) == 0 {
		return service.NewCacaoInvalidParameterError("actor cannot be empty")
	}
	if request.ID == "" {
		return service.NewCacaoInvalidParameterError("Update input validation error: ProviderID is empty")
	}
	if len(request.ProviderModel.Name) > types.MaxProviderNameLength {
		return service.NewCacaoInvalidParameterError("Creation input validation error: Name is too long")
	}
	if len(request.ProviderModel.Type) > types.MaxProviderTypeLength {
		return service.NewCacaoInvalidParameterError("Creation input validation error: Type is too long")
	}
	if len(request.ProviderModel.URL) > types.MaxProviderURLLength {
		return service.NewCacaoInvalidParameterError("Creation input validation error: URL is too long")
	}
	if request.ProviderModel.URL != "" {
		_, err := url.Parse(request.URL)
		if err != nil {
			return service.NewCacaoInvalidParameterError("Creation input validation error: URL is invalid")
		}
	}
	if request.ProviderModel.Metadata != nil {
		if err := validateMetadata(&request.ProviderModel); err != nil {
			return service.NewCacaoInvalidParameterError(err.Error())
		}
	}
	return nil
}

func (h *EventHandlers) updateErrorResponse(request service.ProviderRequest, err error) service.ProviderResponse {
	return service.ProviderResponse{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.ToCacaoError(err).GetBase(),
		},
		ProviderModel: service.ProviderModel{
			ID: request.ID,
		},
	}
}

// Delete deletes the Provider
func (h *EventHandlers) Delete(request service.ProviderRequest, eventOut ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "EventHandlers.Update",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"id":       request.ID,
	})

	if len(request.SessionActor) == 0 {
		err := service.NewCacaoInvalidParameterError("actor cannot be empty")
		logger.WithError(err).Error("bad request")
		eventOut.DeleteFailed(h.deletionErrorResponse(request, err))
		return
	}
	if len(request.ID) == 0 {
		err := service.NewCacaoInvalidParameterError("Deletion input validation error: ProviderID is empty")
		logger.WithError(err).Error("bad request")
		eventOut.DeleteFailed(h.deletionErrorResponse(request, err))
		return
	}

	actorIsAdmin, err := h.userSvc.CheckForAdmin(service.ActorFromSession(request.Session))
	if err != nil {
		logger.WithError(err).Error("fail to check authorization")
		eventOut.DeleteFailed(h.deletionErrorResponse(request, err))
		return
	}
	fetchedProvider, err := h.storage.Get(request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch provider from storage before deletion")
		eventOut.DeleteFailed(h.deletionErrorResponse(request, err))
		return
	}
	if err := h.checkDeletionAuthorization(request, actorIsAdmin, fetchedProvider); err != nil {
		logger.WithError(err).Error("user not authorized")
		eventOut.DeleteFailed(h.deletionErrorResponse(request, err))
		return
	}

	err = h.storage.Delete(service.ActorFromSession(request.Session), request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete provider from storage")
		eventOut.DeleteFailed(h.deletionErrorResponse(request, err))
		return
	}

	logger.Info("provider deleted")
	eventOut.Deleted(service.ProviderResponse{
		Session: service.CopySessionActors(request.Session),
		ProviderModel: service.ProviderModel{
			ID:    request.ProviderModel.ID,
			Owner: fetchedProvider.Owner,
		},
	})
}

func (h *EventHandlers) checkDeletionAuthorization(request service.ProviderRequest, actorIsAdmin bool, provider types.Provider) service.CacaoError {
	if actorIsAdmin {
		return nil
	}
	if provider.Owner != request.SessionActor {
		if provider.Public {
			// if public provider, then non-admin user can access it, so return UnauthorizedError
			return service.NewCacaoUnauthorizedError("user not authorized")
		}
		// if private provider, then non-admin user CANNOT access it, so return NotFoundError
		return types.NewCacaoNotFoundError(request.ID)
	} else if provider.Public {
		// if for whatever reason, a non-admin is the owner of a public provider, then we do not allow the user to delete it.
		return service.NewCacaoUnauthorizedError("user not authorized")
	}
	return nil
}

func (h *EventHandlers) deletionErrorResponse(request service.ProviderRequest, err error) service.ProviderResponse {
	return service.ProviderResponse{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.ToCacaoError(err).GetBase(),
		},
		ProviderModel: service.ProviderModel{
			ID: request.ID,
		},
	}
}
