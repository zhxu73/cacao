package domain

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
)

type testValidateArgs struct {
	p *service.ProviderModel
}
type validateTestCase struct {
	name    string
	args    testValidateArgs
	wantErr bool
}

func Test_validate(t *testing.T) {
	tests := []validateTestCase{
		{
			name: "normal",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "nil",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:       "provider-d17e30598850n9abikl0",
					Type:     "openstack",
					Metadata: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "empty",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:       "provider-d17e30598850n9abikl0",
					Type:     "openstack",
					Metadata: map[string]interface{}{},
				},
			},
			wantErr: true,
		},
		{
			name: "empty template id",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty prerequisite_template",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template":   map[string]string{},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "public_ssh_key nil",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          nil,
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty public_ssh_key",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_name nil",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   nil,
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty external_network_name",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_uuid nil",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   nil,
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty external_network_uuid",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_uuid not uuid",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "not-uuid",
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_uuid int",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   123,
						"external_subnet_uuids":   []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids nil",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   nil,
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty external_subnet_uuids",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty uuid in external_subnet_uuids",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{""},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids int",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   123,
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids []int",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []int{123},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids []string not uuid",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-cnm8f0598850n9abikj0",
						},
						"public_ssh_key":          "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name":   "external-network",
						"external_network_uuid":   "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids":   []string{"not-uuid"},
						"OS_AUTH_URL":             "https://cyverse.org",
						"OS_IDENTITY_API_VERSION": "3",
						"OS_REGION_NAME":          "foobar",
						"OS_INTERFACE":            "foobar",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "k8s non-ssh-tunnel",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "kubernetes",
					Metadata: map[string]interface{}{
						"kubeconfig_cred_id": "foobar",
						"k8s_api_ssh_tunnel": false,
					},
				},
			},
			wantErr: false,
		},
		{
			name: "k8s empty",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:       "provider-d17e30598850n9abikl0",
					Type:     "kubernetes",
					Metadata: map[string]interface{}{},
				},
			},
			wantErr: true,
		},
		{
			name: "k8s ssh-tunnel",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "kubernetes",
					Metadata: map[string]interface{}{
						"kubeconfig_cred_id": "foobar",
						"k8s_api_ssh_tunnel": true,
						"k8s_api_ssh_tunnel_config": map[string]interface{}{
							"host":              "example.cyverse.org",
							"port":              22,
							"username":          "example",
							"use_cacao_ssh_key": true,
							"ssh_host_key":      "ssh=================================",
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "k8s ssh-tunnel user ssh key",
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Type: "kubernetes",
					Metadata: map[string]interface{}{
						"kubeconfig_cred_id": "foobar",
						"k8s_api_ssh_tunnel": true,
						"k8s_api_ssh_tunnel_config": map[string]interface{}{
							"host":              "example.cyverse.org",
							"port":              22,
							"username":          "example",
							"use_cacao_ssh_key": false,
							"ssh_key_cred_id":   "foobar123",
							"ssh_host_key":      "ssh=================================",
						},
					},
				},
			},
			wantErr: false,
		},
	}
	tests = generateMissingFieldTestCases(tests)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateMetadata(tt.args.p); (err != nil) != tt.wantErr {
				t.Errorf("validateMetadata() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func generateMissingFieldTestCases(tests []validateTestCase) []validateTestCase {
	var baseTestCase validateTestCase
	for _, tt := range tests {
		if tt.name == "normal" {
			baseTestCase = tt
			break
		}
	}
	if baseTestCase.name == "" {
		panic("cannot find the 'normal' test case")
	}
	requiredFieldNames := []string{"prerequisite_template", "public_ssh_key", "external_network_name", "external_network_uuid", "external_subnet_uuids", "OS_AUTH_URL", "OS_IDENTITY_API_VERSION", "OS_REGION_NAME", "OS_INTERFACE"}
	for _, fieldName := range requiredFieldNames {
		metadataCopy := copyMap(baseTestCase.args.p.Metadata)
		delete(metadataCopy, fieldName)
		tests = append(tests, validateTestCase{
			name: fmt.Sprintf("missing %s", fieldName),
			args: testValidateArgs{
				p: &service.ProviderModel{
					ID:       baseTestCase.args.p.ID,
					Type:     baseTestCase.args.p.Type,
					Metadata: metadataCopy,
				},
			},
			wantErr: true,
		})
	}
	return tests
}

func copyMap(src map[string]interface{}) map[string]interface{} {
	var dest = make(map[string]interface{})
	for k, v := range src {
		dest[k] = v
	}
	return dest
}
