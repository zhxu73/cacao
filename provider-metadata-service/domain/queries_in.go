package domain

import (
	"context"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
)

// QueryHandlers implements IncomingQueryHandlers
type QueryHandlers struct {
	userSvc ports.UserService
	storage ports.ProviderStoragePort
}

var _ ports.IncomingQueryHandlers = &QueryHandlers{}

// Get retrieves the Provider
func (h *QueryHandlers) Get(ctx context.Context, actor service.Actor, id common.ID) service.ProviderResponse {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "QueryHandlers.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"provider": id,
	})
	logger.Debug("called")
	if len(id) == 0 {
		err := service.NewCacaoInvalidParameterError("ProviderID is empty")
		logger.WithError(err).Error("bad request")
		return service.ProviderResponse{
			Session: service.Session{
				SessionActor:    actor.Actor,
				SessionEmulator: actor.Emulator,
				ServiceError:    err.GetBase(),
			},
		}
	}
	p, err := h.storage.Get(id)
	if err != nil {
		logger.WithError(err).Error("fail to fetch provider in storage")
		return service.ProviderResponse{
			Session: service.Session{
				SessionActor:    actor.Actor,
				SessionEmulator: actor.Emulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
		}
	}
	actorIsAdmin, err := h.userSvc.CheckForAdmin(actor)
	if err != nil {
		return service.ProviderResponse{
			Session: service.Session{
				SessionActor:    actor.Actor,
				SessionEmulator: actor.Emulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
		}
	}
	if !p.Public && !actorIsAdmin && p.Owner != actor.Actor {
		// non-admin cannot access private provider owned by other user
		return service.ProviderResponse{
			Session: service.Session{
				SessionActor:    actor.Actor,
				SessionEmulator: actor.Emulator,
				ServiceError:    service.NewCacaoUnauthorizedError("user is not authorized").GetBase(),
			},
		}
	}
	logger.Info("fetched")
	return service.ProviderResponse{
		Session:       actor.Session(),
		ProviderModel: p.ToServiceModel(),
	}
}

// List retrieves all providers of the user
func (h *QueryHandlers) List(ctx context.Context, actor service.Actor) service.ProviderListModel {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "QueryHandlers.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})
	logger.Debug("called")
	actorIsAdmin, err := h.userSvc.CheckForAdmin(actor)
	if err != nil {
		return service.ProviderListModel{
			Session: service.Session{
				SessionActor:    actor.Actor,
				SessionEmulator: actor.Emulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
		}
	}
	var providers []types.Provider
	if actorIsAdmin {
		providers, err = h.storage.ListAll(0, types.DefaultListLimit)
	} else {
		providers, err = h.storage.ListPublicAndOwnedByUser(actor.Actor, 0, types.DefaultListLimit)
	}
	if err != nil {
		logger.WithError(err).Error("fail to list provider in storage")
		return service.ProviderListModel{
			Session: service.Session{
				SessionActor:    actor.Actor,
				SessionEmulator: actor.Emulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
			List: nil,
		}
	}
	var list = make([]service.ProviderModel, 0, len(providers))
	for _, p := range providers {
		list = append(list, p.ToServiceModel())
	}
	logger.WithField("len", len(list)).Info("listed")
	return service.ProviderListModel{
		Session: actor.Session(),
		List:    list,
	}
}
