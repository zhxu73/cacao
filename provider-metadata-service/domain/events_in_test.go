package domain

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/provider-metadata-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"testing"
	"time"
)

func incrementTimeSrc(start time.Time, step time.Duration) func() time.Time {
	var stepCount = new(int)
	return func() time.Time {
		result := start.Add(time.Duration(*stepCount) * step)
		*stepCount++
		return result
	}
}

func TestEventHandlers_Create(t *testing.T) {
	start := time.Now().UTC()
	const timeStep = time.Second
	idGenerator := func(type1 service.ProviderType) common.ID {
		return common.ID("provider-" + type1 + "-aaaaaaaaaaaaaaaaaaaa")
	}

	type fields struct {
		userSvc     *portsmocks.UserService
		storage     *portsmocks.ProviderStoragePort
		timeSrc     func() time.Time
		idGenerator func(providerType service.ProviderType) common.ID
	}
	type args struct {
		request  service.ProviderRequest
		eventOut *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "",
						Emulator: "",
					}).Return(false, fmt.Errorf("error123")).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					return storage
				}(),
				timeSrc:     incrementTimeSrc(start, timeStep),
				idGenerator: idGenerator,
			},
			args: args{
				request: service.ProviderRequest{
					Session:       service.Session{},
					ProviderModel: service.ProviderModel{},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("CreateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						ProviderModel: service.ProviderModel{},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin creates private provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Create", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}, types.Provider{
						ID:             "provider-" + common.ID(types.ProviderTypeForTesting) + "-aaaaaaaaaaaaaaaaaaaa",
						Name:           "provider_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "https://cyverse.org",
						Owner:          "testuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      start,
						UpdatedAt:      start,
						UpdatedBy:      "",
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					}).Return(nil).Once()
					return storage
				}(),
				timeSrc:     incrementTimeSrc(start, timeStep),
				idGenerator: idGenerator,
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ProviderModel: service.ProviderModel{
						ID:        "provider-co1g2vl9885fkmrf2ong",
						Name:      "provider_name123",
						Type:      types.ProviderTypeForTesting,
						URL:       "https://cyverse.org",
						Owner:     "foobar_user", // this will be overridden to be actor
						Public:    false,
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Created", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
						},
						ProviderModel: service.ProviderModel{
							ID:        "provider-" + common.ID(types.ProviderTypeForTesting) + "-aaaaaaaaaaaaaaaaaaaa",
							Name:      "provider_name123",
							Type:      types.ProviderTypeForTesting,
							URL:       "https://cyverse.org",
							Owner:     "testuser123",
							Public:    false,
							CreatedAt: start,
							UpdatedAt: start,
							Metadata: map[string]interface{}{
								"k1": "v1",
							},
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin creates public provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					return storage
				}(),
				timeSrc:     incrementTimeSrc(start, timeStep),
				idGenerator: idGenerator,
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ProviderModel: service.ProviderModel{
						ID:        "provider-co1g2vl9885fkmrf2ong",
						Name:      "provider_name123",
						Type:      types.ProviderTypeForTesting,
						URL:       "https://cyverse.org",
						Owner:     "foobar_user", // this will be overridden to be actor
						Public:    true,
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("CreateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    service.NewCacaoUnauthorizedError("user is not admin").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID:   "",
							Name: "provider_name123",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin creates private provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}).Return(true, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Create", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}, types.Provider{
						ID:             "provider-" + common.ID(types.ProviderTypeForTesting) + "-aaaaaaaaaaaaaaaaaaaa",
						Name:           "provider_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "https://cyverse.org",
						Owner:          "testuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      start,
						UpdatedAt:      start,
						UpdatedBy:      "",
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					}).Return(nil).Once()
					return storage
				}(),
				timeSrc:     incrementTimeSrc(start, timeStep),
				idGenerator: idGenerator,
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ProviderModel: service.ProviderModel{
						ID:        "provider-co1g2vl9885fkmrf2ong",
						Name:      "provider_name123",
						Type:      types.ProviderTypeForTesting,
						URL:       "https://cyverse.org",
						Owner:     "foobar_user", // this will be overridden to be actor
						Public:    false,
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Created", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
						},
						ProviderModel: service.ProviderModel{
							ID:        "provider-" + common.ID(types.ProviderTypeForTesting) + "-aaaaaaaaaaaaaaaaaaaa",
							Name:      "provider_name123",
							Type:      types.ProviderTypeForTesting,
							URL:       "https://cyverse.org",
							Owner:     "testuser123",
							Public:    false,
							CreatedAt: start,
							UpdatedAt: start,
							Metadata: map[string]interface{}{
								"k1": "v1",
							},
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin creates public provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}).Return(true, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Create", service.Actor{
						Actor:    "testuser123",
						Emulator: "emulator123",
					}, types.Provider{
						ID:             "provider-" + common.ID(types.ProviderTypeForTesting) + "-aaaaaaaaaaaaaaaaaaaa",
						Name:           "provider_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "https://cyverse.org",
						Owner:          "testuser123",
						Public:         true,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      start,
						UpdatedAt:      start,
						UpdatedBy:      "",
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					}).Return(nil).Once()
					return storage
				}(),
				timeSrc:     incrementTimeSrc(start, timeStep),
				idGenerator: idGenerator,
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ProviderModel: service.ProviderModel{
						ID:        "provider-co1g2vl9885fkmrf2ong",
						Name:      "provider_name123",
						Type:      types.ProviderTypeForTesting,
						URL:       "https://cyverse.org",
						Owner:     "foobar_user", // this will be overridden to be actor
						Public:    true,
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Created", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
						},
						ProviderModel: service.ProviderModel{
							ID:        "provider-" + common.ID(types.ProviderTypeForTesting) + "-aaaaaaaaaaaaaaaaaaaa",
							Name:      "provider_name123",
							Type:      types.ProviderTypeForTesting,
							URL:       "https://cyverse.org",
							Owner:     "testuser123",
							Public:    true,
							CreatedAt: start,
							UpdatedAt: start,
							Metadata: map[string]interface{}{
								"k1": "v1",
							},
						},
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				userSvc:     tt.fields.userSvc,
				storage:     tt.fields.storage,
				timeSrc:     tt.fields.timeSrc,
				idGenerator: tt.fields.idGenerator,
			}
			h.Create(tt.args.request, tt.args.eventOut)
			tt.fields.storage.AssertExpectations(t)
			tt.fields.userSvc.AssertExpectations(t)
			tt.args.eventOut.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Update(t *testing.T) {
	start := time.Now().UTC()
	const timeStep = time.Second

	type fields struct {
		userSvc *portsmocks.UserService
		storage *portsmocks.ProviderStoragePort
		timeSrc func() time.Time
	}
	type args struct {
		request  service.ProviderRequest
		eventOut *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session:       service.Session{},
					ProviderModel: service.ProviderModel{},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoInvalidParameterError("actor cannot be empty").GetBase(),
						},
						ProviderModel: service.ProviderModel{},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "cannot check actor",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, fmt.Errorf("error123")).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "update Name URL Metadata",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "testuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					storage.On("Update", common.ID("provider-d17e30598850n9abikl0"), types.ProviderUpdate{
						Name:   stringPtr("new_name123"),
						Type:   nil,
						URL:    stringPtr("https://cyverse.org"),
						Public: nil,
						Shared: nil,
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
						UpdatedAt: start,
						UpdatedBy: "testuser123",
					}).Return(nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID:        "provider-d17e30598850n9abikl0",
						Name:      "new_name123",
						Type:      "",
						URL:       "https://cyverse.org",
						Owner:     "",
						Public:    false,
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						Metadata: map[string]interface{}{
							"k1": "v1",
						},
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Updated", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						ProviderModel: service.ProviderModel{
							ID:        "provider-d17e30598850n9abikl0",
							Name:      "new_name123",
							Type:      "",
							URL:       "https://cyverse.org",
							Owner:     "",
							Public:    false,
							CreatedAt: time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
							UpdatedAt: start,
							Metadata: map[string]interface{}{
								"k1": "v1",
							},
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin update other user's private provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "diff_user123", // owned by different user
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID:   "provider-d17e30598850n9abikl0",
						Name: "new_name123",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoNotFoundError("unable to get a provider for id provider-d17e30598850n9abikl0").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin update other user's public provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "diff_user123", // owned by different user
						Public:         true,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID:   "provider-d17e30598850n9abikl0",
						Name: "new_name123",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedErrorWithOptions("user is not authorized").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin update Public=true",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "testuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID:     "provider-d17e30598850n9abikl0",
						Public: true,
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError("user is not authorized").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin update Public=true",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "adminuser123",
						Emulator: "",
					}).Return(true, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "adminuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					storage.On("Update", common.ID("provider-d17e30598850n9abikl0"), types.ProviderUpdate{
						Name: nil,
						Type: nil,
						URL:  nil,
						Public: func() *bool {
							b := true
							return &b
						}(),
						Shared:    nil,
						Metadata:  nil,
						UpdatedAt: start,
						UpdatedBy: "adminuser123",
					}).Return(nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "adminuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID:     "provider-d17e30598850n9abikl0",
						Public: true,
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Updated", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "adminuser123",
							SessionEmulator: "",
						},
						ProviderModel: service.ProviderModel{
							ID:        "provider-d17e30598850n9abikl0",
							Public:    true,
							CreatedAt: time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
							UpdatedAt: start,
						},
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				userSvc:     tt.fields.userSvc,
				storage:     tt.fields.storage,
				timeSrc:     tt.fields.timeSrc,
				idGenerator: nil,
			}
			h.Update(tt.args.request, tt.args.eventOut)
			tt.fields.storage.AssertExpectations(t)
			tt.fields.userSvc.AssertExpectations(t)
			tt.args.eventOut.AssertExpectations(t)
		})
	}
}

func stringPtr(s string) *string {
	return &s
}

func TestEventHandlers_Delete(t *testing.T) {
	start := time.Now().UTC()
	const timeStep = time.Second

	type fields struct {
		userSvc *portsmocks.UserService
		storage *portsmocks.ProviderStoragePort
		timeSrc func() time.Time
	}
	type args struct {
		request  service.ProviderRequest
		eventOut *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session:       service.Session{},
					ProviderModel: service.ProviderModel{},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoInvalidParameterError("actor cannot be empty").GetBase(),
						},
						ProviderModel: service.ProviderModel{},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "cannot check actor",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, fmt.Errorf("error123")).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin delete owned private provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "testuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					storage.On("Delete",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						},
						common.ID("provider-d17e30598850n9abikl0"),
					).Return(nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Deleted", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						ProviderModel: service.ProviderModel{
							ID:    "provider-d17e30598850n9abikl0",
							Owner: "testuser123",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			// this is unlikely to happen, if happened this indicates data is not consistent, but we will test this anyway
			name: "non-admin delete owned public provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "testuser123",
						Public:         true,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError("user not authorized").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin delete others' private provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "diff_user123", // different user
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							// non-admin cannot access other's private provider, so return NotFoundError to not leak the fact that provider exists.
							ServiceError: types.NewCacaoNotFoundError("provider-d17e30598850n9abikl0").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin delete others' public provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}).Return(false, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "diff_user123", // different user
						Public:         true,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError("user not authorized").GetBase(),
						},
						ProviderModel: service.ProviderModel{
							ID: "provider-d17e30598850n9abikl0",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin delete public provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "adminuser123",
						Emulator: "",
					}).Return(true, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "adminuser123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					storage.On("Delete",
						service.Actor{
							Actor:    "adminuser123",
							Emulator: "",
						},
						common.ID("provider-d17e30598850n9abikl0"),
					).Return(nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "adminuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Deleted", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "adminuser123",
							SessionEmulator: "",
						},
						ProviderModel: service.ProviderModel{
							ID:    "provider-d17e30598850n9abikl0",
							Owner: "adminuser123",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin delete others' provider",
			fields: fields{
				userSvc: func() *portsmocks.UserService {
					svc := &portsmocks.UserService{}
					svc.On("CheckForAdmin", service.Actor{
						Actor:    "adminuser123",
						Emulator: "",
					}).Return(true, nil).Once()
					return svc
				}(),
				storage: func() *portsmocks.ProviderStoragePort {
					storage := &portsmocks.ProviderStoragePort{}
					storage.On("Get", common.ID("provider-d17e30598850n9abikl0")).Return(types.Provider{
						ID:             "provider-d17e30598850n9abikl0",
						Name:           "old_name123",
						Type:           types.ProviderTypeForTesting,
						URL:            "http://old-url.cyverse.org",
						Owner:          "diff_user123",
						Public:         false,
						Shared:         false,
						FromDeployment: "",
						CreatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedAt:      time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
						UpdatedBy:      "",
						Metadata:       nil,
					}, nil).Once()
					storage.On("Delete",
						service.Actor{
							Actor:    "adminuser123",
							Emulator: "",
						},
						common.ID("provider-d17e30598850n9abikl0"),
					).Return(nil).Once()
					return storage
				}(),
				timeSrc: incrementTimeSrc(start, timeStep),
			},
			args: args{
				request: service.ProviderRequest{
					Session: service.Session{
						SessionActor:    "adminuser123",
						SessionEmulator: "",
					},
					ProviderModel: service.ProviderModel{
						ID: "provider-d17e30598850n9abikl0",
					},
				},
				eventOut: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Deleted", service.ProviderResponse{
						Session: service.Session{
							SessionActor:    "adminuser123",
							SessionEmulator: "",
						},
						ProviderModel: service.ProviderModel{
							ID:    "provider-d17e30598850n9abikl0",
							Owner: "diff_user123",
						},
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				userSvc: tt.fields.userSvc,
				storage: tt.fields.storage,
				timeSrc: tt.fields.timeSrc,
			}
			h.Delete(tt.args.request, tt.args.eventOut)
		})
	}
}
