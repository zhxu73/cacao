package domain

import (
	// embed json file that contains provider metadata schema
	_ "embed"
	"errors"
	"fmt"
	"github.com/xeipuuv/gojsonschema"
	"gitlab.com/cyverse/cacao-common/service"
)

var schemas = map[string]string{
	"kubernetes": k8sMetadataSchema,
	"openstack":  openstackMetadataSchema,
}

//go:embed k8s_metadata.json
var k8sMetadataSchema string

//go:embed openstack_metadata.json
var openstackMetadataSchema string

func validateMetadata(p *service.ProviderModel) error {
	schema, ok := schemas[string(p.Type)]
	if !ok {
		return nil
	}
	schemaLoader := gojsonschema.NewStringLoader(schema)
	documentLoader := gojsonschema.NewGoLoader(p.Metadata)
	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return err
	}
	if !result.Valid() {
		s := ""
		for _, desc := range result.Errors() {
			s += fmt.Sprintf("- %s\n", desc)
		}
		return errors.New(s)
	}
	return nil
}
