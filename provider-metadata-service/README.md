# provider-metadata-service

A microservice for managing providers and their associated metadata. A NATS client interface facilitates basic CRUD operations on providers.
The metadata associated with a provider includes:
- ID - a cacao-common ID
- Name - a string
- Type - a string, e.g. openstack
- URL - a string
- Metadata - a map of string keys to things like strings, arrays of strings, any other object that can be marshalled to JSON
- CreatedAt - a UTC timestamp of when the provider was first added
- UpdatedAt - a UTC timestamp of the most recent update to the provider

> This services only manages metadata for provider, it does not provide provider specific operations.

## Building

To build this microservice, you'll need to have a reasonably up-to-date version
of Go installed. Then run the following in the top-level directory containing
the service code:

```
go build
```

The service should build with that single step. If that is not the case, please
file an issue.

This service uses Go modules.

## Configuration

The service is configured through a set of environment variables. Here are the
currently supported variables and their defaults:

| env name                        | description                                                |
|---------------------------------|------------------------------------------------------------|
| `LogLevel`                      | log level (trace/debug/info/error)                         |
| `OnlyAdmins`                    | restrict Create/Update/Delete access to admin only if true |
| `ProviderMongoDBCollectionName` | -                                                          |
| `NATS_URL`                      | -                                                          |
| `NATS_QUEUE_GROUP`              | -                                                          |
| `NATS_WILDCARD_SUBJECT`         | -                                                          |
| `NATS_CLIENT_ID`                | -                                                          |
| `NATS_MAX_RECONNECTS`           | -                                                          |
| `NATS_RECONNECT_WAIT`           | -                                                          |
| `NATS_REQUEST_TIMEOUT`          | -                                                          |
| `MONGODB_URL`                   | -                                                          |
| `MONGODB_DB_NAME`               | -                                                          |

## Operation Authorization
There are 3 types of visibilities
- public
- private
- private & shared

- Get
  - any user can fetch any public provider
  - user can fetch the private providers they own
  - user can fetch private providers that are shared with them
- List
  - non-admin
    - list will include:
      - all public providers
      - the private providers user owns
      - private providers that are shared with user
  - admin
    - admin can list all providers
- Creation
  - non-admin can only create private provider
  - admin can create private and public provider
- Update
  - only admin or owner can update
  - non-admin cannot make a private provider public
- Deletion
  - admin or owner


## Provider Metadata
[schema](./adapters/event_adapter.go) for metadata is defined using json schema.

### OpenStack provider
`OS_REGION_NAME`, `project_domain_uuid`, `OS_INTERFACE`, `OS_USER_DOMAIN_NAME`, `OS_IDENTITY_API_VERSION` are required by ionosphere for populate fields for new credential when user manually import it.

`prerequisite_template.template_id` is required by deployment service to determine the prerequisite template for the openstack provider.

`public_ssh_key`, `external_network_name`, `external_subnet_uuids` are required by deployment execution service for populate the value of parameter for some special parameter types.
