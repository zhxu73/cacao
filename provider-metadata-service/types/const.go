package types

import "gitlab.com/cyverse/cacao-common/service"

const (
	// DefaultNatsQueueGroup is a default NATS Queue Group
	DefaultNatsQueueGroup = "provider_queue_group"
	// DefaultNatsWildcardSubject is a default NATS subject to subscribe
	DefaultNatsWildcardSubject = "cyverse.provider.>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "providers-service"

	// DefaultNatsDurableName is a default NATS Durable Name
	DefaultNatsDurableName = "provider_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao"
	// DefaultProviderMongoDBCollectionName is a default MongoDB Collection Name
	DefaultProviderMongoDBCollectionName = "provider"
)

// constants that specify some constraints on provider fields
const (
	MaxProviderNameLength = 100
	MaxProviderTypeLength = 20
	MaxProviderURLLength  = 2048
)

// DefaultListLimit is the default limit (# of providers) on list operation in storage
const DefaultListLimit = 500

// ProviderTypeForTesting ...
const ProviderTypeForTesting service.ProviderType = "__test__"
