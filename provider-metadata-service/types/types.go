package types

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// ProviderRequest is a bundle for domain data and an event/query operation on that data.
type ProviderRequest struct {
	service.ProviderModel
	Operation        string
	UpdateFieldNames []string // list field names to update, this is used only when operation is Update
	TransactionID    common.TransactionID
	Response         chan ProviderResponse // channel created by adapter(or other caller) and will receive the response
}

// ProviderResponse ...
type ProviderResponse struct {
	Data  []service.ProviderModel
	Error error
}

// Provider is the internal storage format.
//
// Keeping the storage struct internal instead of in cacao-common allow us to
// change storage format without changing the service client.
type Provider struct {
	ID     common.ID            `bson:"_id"`
	Name   string               `bson:"name"`
	Type   service.ProviderType `bson:"type"`
	URL    string               `bson:"url"`
	Owner  string               `bson:"owner"`
	Public bool                 `bson:"public"`
	// Shared means only shared with some users, only private provider can be shared.
	// TODO add fields (or separate document) to specify whom to share with
	Shared bool `bson:"shared"`
	// ID of the deployment that this provider is created from, leave empty is not created from deployment.
	// Example use case is kubernetes provider created from a terraform deployment.
	FromDeployment common.ID `bson:"from_deployment"`
	CreatedAt      time.Time `bson:"created_at"`
	UpdatedAt      time.Time `bson:"updated_at"`
	// username that last updated the provider
	UpdatedBy string                 `bson:"updated_by"`
	Metadata  map[string]interface{} `bson:"metadata"`
}

// ToServiceModel converts internal storage model to service model(the format send over message bus)
func (p *Provider) ToServiceModel() service.ProviderModel {
	return service.ProviderModel{
		ID:        p.ID,
		Name:      p.Name,
		Type:      p.Type,
		URL:       p.URL,
		Owner:     p.Owner,
		Public:    p.Public,
		CreatedAt: p.CreatedAt,
		UpdatedAt: p.UpdatedAt,
		Metadata:  p.Metadata,
	}
}

// FromServiceModel converts from service model(the format send over message bus) to internal storage model
func FromServiceModel(p service.ProviderModel) Provider {
	return Provider{
		ID:             p.ID,
		Name:           p.Name,
		Type:           p.Type,
		URL:            p.URL,
		Owner:          p.Owner,
		Public:         p.Public,
		Shared:         false,
		FromDeployment: "",
		CreatedAt:      p.CreatedAt,
		UpdatedAt:      p.UpdatedAt,
		UpdatedBy:      "",
		Metadata:       p.Metadata,
	}
}

// ProviderUpdate ...
type ProviderUpdate struct {
	Name     *string                `bson:"name"`
	Type     *service.ProviderType  `bson:"type"`
	URL      *string                `bson:"url"`
	Public   *bool                  `bson:"public"`
	Shared   *bool                  `bson:"shared"`
	Metadata map[string]interface{} `bson:"metadata"`

	UpdatedAt time.Time `bson:"updated_at"`
	UpdatedBy string    `bson:"updated_by"`
}

// ToBSON ...
func (u *ProviderUpdate) ToBSON() bson.M {
	var update = make(bson.M)
	if u.Name != nil {
		update["name"] = u.Name
	}
	if u.Type != nil {
		update["type"] = u.Type
	}
	if u.URL != nil {
		update["url"] = u.URL
	}
	if u.Public != nil {
		update["public"] = u.Public
	}
	if u.Shared != nil {
		update["shared"] = u.Shared
	}
	if u.Metadata != nil {
		update["metadata"] = u.Metadata
	}
	update["updated_at"] = u.UpdatedAt
	update["updated_by"] = u.UpdatedBy
	return update
}

// NewCacaoNotFoundError returns a NotFoundError, this is used to have a consistent error message for NotFoundError.
func NewCacaoNotFoundError(providerID common.ID) service.CacaoError {
	return service.NewCacaoNotFoundError(fmt.Sprintf("unable to get a provider for id %s", providerID))
}
