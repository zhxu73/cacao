package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"sync"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config) error
}

// IncomingQueryPort ...
type IncomingQueryPort interface {
	SetHandlers(handlers IncomingQueryHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// IncomingQueryHandlers is an interface that represents handlers for query, the implementation is in domain package.
type IncomingQueryHandlers interface {
	Get(ctx context.Context, actor service.Actor, id common.ID) service.ProviderResponse
	List(ctx context.Context, actor service.Actor) service.ProviderListModel
}

// IncomingEventPort ...
type IncomingEventPort interface {
	SetHandlers(handlers IncomingEventHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// IncomingEventHandlers is an interface that represents handlers for event, the implementation is in domain package.
type IncomingEventHandlers interface {
	Create(request service.ProviderRequest, out OutgoingEventPort)
	Update(request service.ProviderRequest, out OutgoingEventPort)
	Delete(request service.ProviderRequest, out OutgoingEventPort)
}

// OutgoingEventPort represents all events this service will be publishing.
type OutgoingEventPort interface {
	Created(service.ProviderResponse)
	CreateFailed(service.ProviderResponse)
	Updated(service.ProviderResponse)
	UpdateFailed(service.ProviderResponse)
	Deleted(service.ProviderResponse)
	DeleteFailed(service.ProviderResponse)
}

// ProviderStoragePort ...
type ProviderStoragePort interface {
	Port
	Create(service.Actor, types.Provider) error
	Get(id common.ID) (types.Provider, error)
	// ListPublicAndOwnedByUser return public providers and providers owned by user.
	ListPublicAndOwnedByUser(actor string, skip uint, limit uint) ([]types.Provider, error)
	// ListAll will return all providers
	ListAll(skip uint, limit uint) ([]types.Provider, error)
	Update(providerID common.ID, update types.ProviderUpdate) error
	Delete(service.Actor, common.ID) error
}

// UserService ...
type UserService interface {
	Port
	// CheckForAdmin communicates with user service to check if a user is admin.
	// Return error if check fails or user is NOT admin.
	// This can be overridden with config to skip checks.
	CheckForAdmin(actor service.Actor) (actorIsAdmin bool, err error)
}
