package login

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
	"strings"
)

// Cmd is root for login sub-command
var Cmd = &cobra.Command{
	Use:   "login",
	Short: "Login to the Cacao API",
	Long: `Login to the CACAO API.

Default to using CACAO API token as credential to login, you need to create
the token beforehand to use this option (e.g. create it in the credential page
in Web UI).

You can specify the CACAO API token directly as a command line argument with
--cacao-token flag.

You can also get an auth token via browser, the --browser flag will guide you
through it.

If you are targeting a local development version of CACAO, use --keycloak flag
to login.
`,
	RunE: login,
}

var loginParams struct {
	APIAddress    string // e.g. http://ca.cyverse.local/api
	AuthToken     string
	BrowserLogin  bool
	KeycloakLogin bool
	CACAOToken    string
}

func init() {
	Cmd.Flags().StringVar(&loginParams.APIAddress, "api-addr", "", "specify the api address directly as flag (non-interactive), e.g. https://cacao.jetstream-cloud.org/api")
	Cmd.Flags().BoolVar(&loginParams.KeycloakLogin, "keycloak", false, "use keycloak username/password flow to login")
	Cmd.Flags().StringVar(&loginParams.AuthToken, "auth-token", "", "specify auth token directly as flag")
	Cmd.Flags().BoolVar(&loginParams.BrowserLogin, "browser", false, "login in web browser then enter the auth token to cli")
	Cmd.Flags().StringVar(&loginParams.CACAOToken, "cacao-token", "", "login in with CACAO API token and provide the token as a flag")
	Cmd.MarkFlagsMutuallyExclusive("auth-token", "browser", "keycloak", "cacao-token")
}

func login(command *cobra.Command, args []string) error {
	var client utils.Client
	if command.Flags().Changed("api-addr") {
		err := utils.IsHTTPOrHTTPSUrl(loginParams.APIAddress)
		if err != nil {
			return fmt.Errorf("the URL passed to --api-addr flag is not a valid URL, %w", err)
		}
		err = utils.WriteAPIURLToConfigFile(loginParams.APIAddress)
		if err != nil {
			return err
		}
		client = utils.NewClientWithAPIAddr(loginParams.APIAddress)
	} else {
		client = utils.NewClient()
	}

	var err error

	err = client.CheckToken()
	if err == nil {
		fmt.Println("Already logged in. If you want to login again, logout first with `cacao logout`")
		return nil
	}
	err = nil

	if command.Flags().Changed("auth-token") {
		err = utils.SaveAuthTokenToConfigFile(loginParams.AuthToken)
	} else if loginParams.BrowserLogin {
		err = browserLogin(&client)
	} else if command.Flags().Changed("cacao-token") {
		// non-interactive cacao token login
		err = utils.SaveAuthTokenToConfigFile(loginParams.CACAOToken)
	} else if loginParams.KeycloakLogin {
		// normal username&password login flow for keycloak
		_, err = keycloakLogin(&client)
	} else {
		// interactive cacao token login
		err = cacaoTokenLogin()
	}
	if err != nil {
		return err
	}
	return client.CheckToken()
}

func cacaoTokenLogin() error {
	fmt.Println(`You will login with a CACAO API token. You need to create the token beforehand
to use this option (e.g. create it in the credential page in Web UI).`)
	cacaoToken, err := utils.GetInput("Enter CACAO API token:", utils.ValidateInput(func(s string) error {
		if len(s) <= 0 {
			return fmt.Errorf("cacao token cannot be empty")
		}
		return nil
	}), utils.Retry(3))
	if err != nil {
		return err
	}
	return utils.SaveAuthTokenToConfigFile(cacaoToken)
}

// ask user to go the URL in the browser to complete the OAuth2 login flow, and then copy the auth token from the json payload page they finally landed on.
func browserLogin(client *utils.Client) error {
	req := client.NewRequest("GET", "/user/login", "") // this is to create request URL prefixed with API base URL
	fmt.Printf("You will login via a web browser to get your token to access CACAO's api.\n\nPlease go to this URL in the browser to login: %s\n", req.URL.String())
	fmt.Print(`
After login, you should get a JSON response, the auth token could be the value of following properties:
- "IDToken" or "id_token" if keycloak
- "access_token" if other auth provider

`)
	authToken, err := utils.GetInput("Enter the auth token you get from browser: ", utils.ValidateInput(func(s string) error {
		if s == "" {
			return fmt.Errorf("auth token cannot be empty")
		}
		return nil
	}),
		utils.Retry(3),
	)
	if err != nil {
		return err
	}
	if !strings.HasPrefix(authToken, "Bearer ") {
		authToken = "Bearer " + authToken
	}
	return utils.SaveAuthTokenToConfigFile(authToken)
}
