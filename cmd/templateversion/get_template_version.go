package templateversion

import (
	"fmt"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:          "get [template-version-id] [flags]",
	Short:        "Get template version",
	Long:         `Get one template versions`,
	Args:         cobra.ExactArgs(1),
	RunE:         templateVersionGet,
	SilenceUsage: true,
}

func templateVersionGet(command *cobra.Command, args []string) error {
	client := getTemplateVersionClient()

	req := client.NewRequest("GET", fmt.Sprintf("/templates/versions/%s", args[0]), "")
	return client.DoRequest(req)
}
