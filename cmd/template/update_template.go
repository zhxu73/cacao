package template

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	neturl "net/url"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// templateUpdateParams ...
var templateUpdateParams = struct {
	service.TemplateModel
	Filename     string
	SourceBranch string
	SourcePath   string
	SourceTag    string
}{}

// updateCmd ...
var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <template-id> [flags])",
	Short:                 "Update a template",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(1),
	RunE:                  templateUpdate,
	SilenceUsage:          true,
}

func templateUpdate(command *cobra.Command, args []string) error {
	client := getTemplateClient()
	client.PrintDebug("Parsed flags are: %v", templateUpdateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(templateUpdateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &templateUpdateParams.TemplateModel); err != nil {
			return err
		}
	} else {
		templateUpdateParams.ID = common.ID(args[0])

		// check if branch or tag or path is set
		templateUpdateParams.Source.AccessParameters = make(map[string]interface{})
		if command.Flags().Changed("branch") {
			templateUpdateParams.Source.AccessParameters["branch"] = templateUpdateParams.SourceBranch
		}
		if command.Flags().Changed("tag") {
			templateUpdateParams.Source.AccessParameters["tag"] = templateUpdateParams.SourceTag
		}
		if command.Flags().Changed("path") {
			templateUpdateParams.Source.AccessParameters["path"] = templateUpdateParams.SourcePath
		}

	}

	if templateUpdateParams.Source.Visibility == "private" {
		if !command.Flags().Changed("credential-id") {
			return fmt.Errorf("the credential-id flag is required when the source-visibility is set to private")
		}
	}

	data, err := json.Marshal(templateUpdateParams.TemplateModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("PATCH", "/templates/"+neturl.PathEscape(templateUpdateParams.ID.String()), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&templateUpdateParams.Filename, "file", "f", "", "The filename of the template to update")
	updateCmd.Flags().StringVarP(&templateUpdateParams.Name, "name", "", "", "The name of the template to update")
	updateCmd.Flags().StringVarP(&templateUpdateParams.Description, "description", "", "", "The description of the template to update")
	updateCmd.Flags().BoolVarP(&templateUpdateParams.Public, "public", "p", false, "The visibility of the template to update. Defaults to public")
	updateCmd.Flags().StringVarP((*string)(&templateUpdateParams.Source.Visibility), "source-visibility", "", "public", "The visibility of the source of the template to update. Defaults to private")
	updateCmd.Flags().StringVarP(&templateUpdateParams.Source.URI, "uri", "", "", "The URI of the source of the template to update")
	updateCmd.Flags().StringVarP(&templateUpdateParams.SourceBranch, "branch", "", "", "The branch of the source of the template to update")
	updateCmd.Flags().StringVarP(&templateUpdateParams.SourcePath, "path", "", "", "The path of the source of the template to update")
	updateCmd.Flags().StringVarP(&templateUpdateParams.SourceTag, "tag", "", "", "The tag of the source of the template to update")
	updateCmd.Flags().StringVarP(&templateUpdateParams.CredentialID, "credential-id", "", "", "The credential ID to access the private template source. Required if the --source-visibility is set to private")
	updateCmd.Flags().MarkHidden("file")
	createCmd.MarkFlagsMutuallyExclusive("file", "branch")
	createCmd.MarkFlagsMutuallyExclusive("file", "tag")
	createCmd.MarkFlagsMutuallyExclusive("file", "path")
	createCmd.MarkFlagsMutuallyExclusive("file", "tag")
	createCmd.MarkFlagsMutuallyExclusive("file", "credential-id")
	createCmd.MarkFlagsMutuallyExclusive("file", "public")
}
