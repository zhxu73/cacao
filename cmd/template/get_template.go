package template

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var getCmd = &cobra.Command{
	Use:          "get [template-id] [flags]",
	Short:        "Get template(s)",
	Long:         `Gets all templates unless specific ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         templateGet,
	SilenceUsage: true,
}

// Run ...
func templateGet(command *cobra.Command, args []string) error {
	client := getTemplateClient()
	if len(args) >= 1 {
		return getOneTemplate(client, args[0])
	}

	req := client.NewRequest("GET", "/templates", "")
	return client.DoRequest(req)
}

func getOneTemplate(client utils.Client, templateID string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", templateID)
	req := client.NewRequest("GET", "/templates/"+neturl.PathEscape(templateID), "")
	return client.DoRequest(req)
}
