package template

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:          "delete <template-id>",
	Short:        "Delete a template",
	RunE:         templateDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// Run ...
func templateDelete(command *cobra.Command, args []string) error {
	client := getTemplateClient()
	templateID := args[0]
	req := client.NewRequest("DELETE", "/templates/"+neturl.PathEscape(templateID), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	return client.DoRequest(req)
}
