package template

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for template sub-command
var Cmd = &cobra.Command{
	Use:   "template",
	Short: "Manage templates",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(listVersionCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(syncCmd)
}

func getTemplateClient() utils.Client {
	client := utils.NewClient()
	return client
}
