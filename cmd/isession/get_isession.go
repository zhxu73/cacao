package isession

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var isessionGetParams = struct {
	InstanceID        string
	instanceAddressID string
}{}

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get [isession-id] [flags]",
	Short:        "Get isession(s)",
	Long:         `Gets all interactive sessions unless specific interactive-session ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         isessionGet,
	SilenceUsage: true,
}

func init() {
	getCmd.Flags().StringVar(&isessionGetParams.InstanceID, "instance-id", "", "instance id to retrieve")
	getCmd.Flags().StringVar(&isessionGetParams.instanceAddressID, "instance-address-id", "", "instance address id to retrieve")
	getCmd.MarkFlagsMutuallyExclusive("instance-id", "instance-address-id")
}

func isessionGet(command *cobra.Command, args []string) error {
	client := getIsessionClient()

	if len(args) >= 1 {
		return getOneSession(client, args[0])
	}
	if command.Flags().Changed("InstanceID") {
		// check if id flag is set
		if command.Flags().Changed("id") {
			req := client.NewRequest("GET", "/isessions/instanceid/"+neturl.PathEscape(isessionGetParams.InstanceID), "")
			return client.DoRequest(req)
		}
		req := client.NewRequest("GET", "/isessions/instanceid", "")
		return client.DoRequest(req)
	}
	if command.Flags().Changed("instanceAddressID") {
		// check if id flag is set
		if command.Flags().Changed("id") {
			req := client.NewRequest("GET", "/isessions/instanceaddr/"+neturl.PathEscape(isessionGetParams.instanceAddressID), "")
			return client.DoRequest(req)
		}
		req := client.NewRequest("GET", "/isessions/instanceaddr", "")
		return client.DoRequest(req)
	}

	req := client.NewRequest("GET", "/isessions", "")
	return client.DoRequest(req)
}

func getOneSession(client utils.Client, isessionID string) error {
	req := client.NewRequest("GET", "/isessions/"+neturl.PathEscape(isessionID), "")
	return client.DoRequest(req)
}
