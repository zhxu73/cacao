package isession

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var isessionCreateParams = struct {
	http.InteractiveSession
	Filename string
}{}

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <instance-id> <instance-address> <instance-admin> <cloud-id> <protocol>) [flags]",
	Long:                  "The interactive sessions manages Guacamole web shell and desktop sessions. Users can create and retrieve their Guacamale sessions. To create a session, the user must provide the instance id, instance address, instance admin, cloud id, and protocol.",
	Short:                 "Create a isession",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(5),
	RunE:                  isessionCreate,
	SilenceUsage:          true,
}

func isessionCreate(command *cobra.Command, args []string) error {
	client := getIsessionClient()
	if command.Flags().Changed("file") {
		data, err := os.ReadFile(isessionCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &isessionCreateParams.InteractiveSession); err != nil {
			return err
		}

	} else {
		isessionCreateParams.InstanceID = args[0]
		isessionCreateParams.InstanceAddress = args[1]
		isessionCreateParams.InstanceAdminUsername = args[2]
		isessionCreateParams.CloudID = args[3]
		isessionCreateParams.Protocol = service.InteractiveSessionProtocol(args[4])
	}

	// Let's marshal the isession model to json so we can print it
	data, err := json.Marshal(isessionCreateParams.InteractiveSession)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("POST", "/isessions", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&isessionCreateParams.Filename, "file", "f", "", "The file containing the isession information")
	createCmd.Flags().MarkHidden("file")
}
