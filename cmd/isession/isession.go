package isession

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for isession sub-command
var Cmd = &cobra.Command{
	Use:   "isession",
	Short: "Manage interactive sessions",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(checkCmd)

}

func getIsessionClient() utils.Client {
	client := utils.NewClient()
	return client
}
