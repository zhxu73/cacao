package user

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get <username>",
	Short:        "Get a user",
	Args:         cobra.RangeArgs(0, 1),
	RunE:         userGet,
	SilenceUsage: true,
}

var userGetParams = struct {
	Self bool
}{}

func userGet(command *cobra.Command, args []string) error {
	client := getUserClient()
	if len(args) > 0 {
		username := args[0]
		client.PrintDebug("Parsed username argument: '%s'", username)
		path := "/users/" + neturl.PathEscape(username)
		req := client.NewRequest("GET", path, "")
		return client.DoRequest(req)
	}

	var path string
	if userGetParams.Self {
		path = "/users/mine"
	} else {
		path = "/users"
	}
	req := client.NewRequest("GET", path, "")
	return client.DoRequest(req)
}

func init() {
	getCmd.Flags().BoolVar(&userGetParams.Self, "self", false, "get user themself")
}
