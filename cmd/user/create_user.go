package user

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
)

var userCreateParams = struct {
	common.HTTPUser
	Filename string
}{}

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <username> [flags])",
	Short:                 "Create a user",
	DisableFlagsInUseLine: true,
	ValidArgs:             []string{"username"},
	Args:                  cobra.MatchAll(cobra.MaximumNArgs(1), cobra.MinimumNArgs(0)),
	SilenceUsage:          true,
	RunE:                  userCreate,
}

func userCreate(command *cobra.Command, args []string) error {
	client := getUserClient()
	client.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  first_name: '%s'\n  last_name: '%s'\n email: '%s'\n  admin: %t\n",
		userCreateParams.Filename, userCreateParams.FirstName, userCreateParams.LastName, userCreateParams.PrimaryEmail, userCreateParams.IsAdmin)

	if command.Flags().Changed("filename") && len(args) > 0 {
		color.Red("You cannot use the filename flag and the username argument at the same time")
		return nil

	} else if command.Flags().Changed("filename") {
		data, err := os.ReadFile(userCreateParams.Filename)
		if err != nil {
			color.Set(color.FgRed)
			return err
		}
		client.PrintDebug("Successfully read file '%s':\n%s", userCreateParams.Filename, string(data))
		json.Unmarshal(data, &userCreateParams.HTTPUser)
	} else if len(args) > 0 {
		userCreateParams.Username = args[0]
	} else {
		color.Red("You must provide a username, see cacao %s %s -h for more information", command.Parent().Name(), command.Name())
		return nil
	}

	data, err := json.Marshal(&userCreateParams.HTTPUser)
	if err != nil {
		color.Red("Error marshalling user data: %s", err)
		return err
	}
	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/users", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	// notes: create user does not print anything, it just returns "OK"
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&userCreateParams.Filename, "filename", "f", "", "filename")
	createCmd.Flags().BoolVarP(&userCreateParams.IsAdmin, "admin", "a", false, "admin")
	createCmd.Flags().StringVarP(&userCreateParams.PrimaryEmail, "email", "", "", "email")
	createCmd.Flags().StringVarP(&userCreateParams.LastName, "lastname", "", "", "lastname")
	createCmd.Flags().StringVarP(&userCreateParams.FirstName, "firstname", "", "", "firstname")
	createCmd.Flags().MarkHidden("filename")
	createCmd.MarkFlagsMutuallyExclusive("filename", "admin")
	createCmd.MarkFlagsMutuallyExclusive("filename", "email")
	createCmd.MarkFlagsMutuallyExclusive("filename", "lastname")
	createCmd.MarkFlagsMutuallyExclusive("filename", "firstname")

}
