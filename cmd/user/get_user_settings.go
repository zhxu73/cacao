package user

import (
	"fmt"
	neturl "net/url"

	"github.com/spf13/cobra"
)

var getSettingsCmd = &cobra.Command{
	Use:          "get-settings <username>",
	Short:        "Get settings for a user",
	Args:         cobra.ExactArgs(1),
	RunE:         userGetSettings,
	SilenceUsage: true,
}

func userGetSettings(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	client.PrintDebug("Parsed username argument: '%s'", username)
	path := fmt.Sprintf("/users/%s/settings", neturl.PathEscape(username))
	req := client.NewRequest("GET", path, "")
	return client.DoRequest(req)
}
