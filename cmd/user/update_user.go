package user

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/fatih/color"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
)

// userUpdateParams ...
var userUpdateParams = struct {
	common.HTTPUser
	Filename string
}{}

var updateCmd = &cobra.Command{
	Use:          "update (-f filename | <username> [flags])",
	Short:        "Update a user",
	RunE:         userUpdate,
	SilenceUsage: true,
}

// Run ...
func userUpdate(command *cobra.Command, args []string) error {
	client := getUserClient()
	client.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  first_name: '%s'\n  last_name: '%s'\n email: '%s'\n  admin: %t\n",
		userUpdateParams.Filename, userUpdateParams.FirstName, userUpdateParams.LastName, userUpdateParams.PrimaryEmail, userUpdateParams.IsAdmin)

	if command.Flags().Changed("filename") && len(args) > 0 {
		color.Red("You cannot use the filename flag and the username argument at the same time")
		return nil

	} else if command.Flags().Changed("filename") {
		data, err := os.ReadFile(userUpdateParams.Filename)
		if err != nil {
			color.Set(color.FgRed)
			return err
		}
		client.PrintDebug("Successfully read file '%s':\n%s", userUpdateParams.Filename, string(data))
		json.Unmarshal(data, &userUpdateParams.HTTPUser)
	} else if len(args) > 0 {
		userUpdateParams.Username = args[0]
	} else {
		color.Red("You must provide a username, use cacao %s %s -h for more information", command.Parent().Name(), command.Name())
		return nil
	}

	data, err := json.Marshal(&userUpdateParams.HTTPUser)
	if err != nil {
		return errors.Wrap(err, "failed to marshal user information")
	}
	client.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := client.NewRequest("PUT", "/users/"+userUpdateParams.Username, string(data))
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&userUpdateParams.Filename, "filename", "f", "", "Filename containing user information")
	updateCmd.Flags().StringVarP(&userUpdateParams.FirstName, "first_name", "", "", "First name")
	updateCmd.Flags().StringVarP(&userUpdateParams.LastName, "last_name", "", "", "Last name")
	updateCmd.Flags().StringVarP(&userUpdateParams.PrimaryEmail, "email", "", "", "Primary email")
	updateCmd.Flags().BoolVarP(&userUpdateParams.IsAdmin, "admin", "a", false, "Is admin")
}
