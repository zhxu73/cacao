package user

import (
	"fmt"
	"net/http"

	"github.com/spf13/cobra"
)

var deleteFavoriteCmd = &cobra.Command{
	Use:          "delete-favorite <username> <name> <value> [flags])",
	Short:        "Delete a favorite config for a user",
	Args:         cobra.ExactArgs(3),
	RunE:         userDeleteFavorite,
	SilenceUsage: true,
}

func userDeleteFavorite(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	name := args[1]
	value := args[2]

	req := client.NewRequest(http.MethodDelete, fmt.Sprintf("/users/%s/favorites/%s/%s", username, name, value), "")
	return client.DoRequest(req)
}
