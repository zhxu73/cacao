package user

import (
	"fmt"
	neturl "net/url"

	"github.com/spf13/cobra"
)

var getRecentCmd = &cobra.Command{
	Use:          "get-recent <username>",
	Short:        "Get recently used config value for a user",
	Args:         cobra.ExactArgs(1),
	RunE:         userGetRecent,
	SilenceUsage: true,
}

func userGetRecent(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	client.PrintDebug("Parsed username argument: '%s'", username)
	path := fmt.Sprintf("/users/%s/recents", neturl.PathEscape(username))
	req := client.NewRequest("GET", path, "")
	return client.DoRequest(req)
}
