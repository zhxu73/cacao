package user

import (
	"fmt"
	"net/http"

	"github.com/spf13/cobra"
)

var setRecentCmd = &cobra.Command{
	Use:          "set-recent <username> <name> <value> [flags])",
	Short:        "Set a recent config for a user",
	Args:         cobra.ExactArgs(3),
	RunE:         userSetRecent,
	SilenceUsage: true,
}

func userSetRecent(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	name := args[1]
	value := args[2]

	req := client.NewRequest(http.MethodPost, fmt.Sprintf("/users/%s/recents/%s/%s", username, name, value), "")
	return client.DoRequest(req)
}
