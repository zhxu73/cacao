package user

import (
	"fmt"
	"net/http"

	"github.com/spf13/cobra"
)

var setConfigCmd = &cobra.Command{
	Use:          "set-config <username> <name> <value> [flags])",
	Short:        "Set a config for a user",
	Args:         cobra.ExactArgs(3),
	RunE:         userSetConfig,
	SilenceUsage: true,
}

func userSetConfig(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	name := args[1]
	value := args[2]

	req := client.NewRequest(http.MethodPost, fmt.Sprintf("/users/%s/configs/%s/%s", username, name, value), "")
	return client.DoRequest(req)
}
