package credential

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// credentialCreateParams ...
var credentialCreateParams = struct {
	Credential    hm.Credential
	Filename      string
	ValueFilename string
}{}

// createCmd ...
var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <name> <type> [flags])",
	Short:                 "Create a provider",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(2),
	RunE:                  credentialCreate,
	SilenceUsage:          true,
}

// credentialCreate ...
func credentialCreate(command *cobra.Command, args []string) error {
	client := getCredentialClient()
	if command.Flags().Changed("filename") {
		data, err := os.ReadFile(credentialCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &credentialCreateParams.Credential); err != nil {
			return err
		}
	} else {
		credentialCreateParams.Credential.Name = args[0]
		credentialCreateParams.Credential.Type = args[1]
	}

	if command.Flags().Changed("value") {
	} else if command.Flags().Changed("value-file") {
		data, err := os.ReadFile(credentialCreateParams.ValueFilename)
		if err != nil {
			return err
		}
		credentialCreateParams.Credential.Value = string(data)
	}
	fmt.Printf("%+v\n", credentialCreateParams.Credential)

	data, err := json.Marshal(credentialCreateParams.Credential)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/credentials", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&credentialCreateParams.Filename, "file", "f", "", "JSON file to read credential info from")
	createCmd.Flags().StringVarP(&credentialCreateParams.ValueFilename, "value-file", "", "", "Filename of credential value to create. If credential is created from a file, this will override the value in the file")
	createCmd.Flags().StringVarP(&credentialCreateParams.Credential.Value, "value", "", "", "Credential value to create. If credential is created from a file, this will override the value in the file")
	createCmd.Flags().MarkHidden("file")
	createCmd.MarkFlagsMutuallyExclusive("value", "value-file")

}
