package credential

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for credential sub-command
var Cmd = &cobra.Command{
	Use:   "credential",
	Short: "Manage credentials",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(deleteCmd)

}

func getCredentialClient() utils.Client {
	client := utils.NewClient()
	return client
}
