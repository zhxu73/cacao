package credential

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/spf13/cobra"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// credentialUpdateParams ...
var credentialUpdateParams = struct {
	CredID        string
	Value         string // credential value from cmd line arg
	Filename      string
	ValueFilename string // credential value from file
}{}

// updateCmd ...
var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <credential-id> [flags])",
	Short:                 "Update a credential",
	Long:                  "Update credential information",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(1),
	RunE:                  credentialUpdate,
	SilenceUsage:          true,
}

// credentialUpdate ...
func credentialUpdate(command *cobra.Command, args []string) error {
	client := getCredentialClient()
	var update hm.CredentialUpdate
	if command.Flags().Changed("filename") {
		data, err := os.ReadFile(credentialUpdateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &update); err != nil {
			return err
		}
	} else {
		credentialUpdateParams.CredID = args[0]
	}

	if command.Flags().Changed("value") {
		update.Value = &credentialUpdateParams.Value
	} else if command.Flags().Changed("value-file") {
		data, err := os.ReadFile(credentialUpdateParams.ValueFilename)
		if err != nil {
			return err
		}
		value := string(data)
		update.Value = &value
	}

	data, err := json.Marshal(update)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/credentials", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&credentialUpdateParams.Filename, "file", "f", "", "JSON file to read credential info from")
	updateCmd.Flags().StringVarP(&credentialUpdateParams.ValueFilename, "value-file", "", "", "The filename of the credential value. If credential is updated from a file, this will override the value in the file")
	updateCmd.Flags().StringVarP(&credentialUpdateParams.Value, "value", "", "", "Credential value to update. If credential is updated from a file, this will override the value in the file")
	updateCmd.Flags().MarkHidden("filename")
	updateCmd.MarkFlagsMutuallyExclusive("value", "value-file")

}
