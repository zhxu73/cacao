package token

import (
	"github.com/spf13/cobra"
	neturl "net/url"
)

var deleteCmd = &cobra.Command{
	Use:   "delete (<token_id>)",
	Short: "Delete a token",
	Args:  cobra.ExactArgs(1),
	RunE:  tokenDelete,
}

func tokenDelete(cmd *cobra.Command, args []string) error {
	client := getTokenClient()
	tokenID := args[0]
	client.PrintDebug("parsed token id %s", tokenID)
	req := client.NewRequest("DELETE", "/tokens/"+neturl.PathEscape(tokenID), "")
	client.PrintDebug("sending request to '%s'", req.URL)
	client.DoRequest(req)
	return nil
}
