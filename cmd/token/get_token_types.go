package token

import (
	"github.com/spf13/cobra"
)

var getTypesCmd = &cobra.Command{
	Use:   "gettypes",
	Short: "Get available types of tokens for creation",
	Args:  cobra.NoArgs,
	RunE:  tokenTypesGet,
}

func tokenTypesGet(cmd *cobra.Command, args []string) error {
	client := getTokenClient()
	req := client.NewRequest("GET", "/tokens/types", "")
	client.PrintDebug("sending request to '%s'", req.URL)
	client.DoRequest(req)
	return nil
}
