package token

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
	neturl "net/url"
)

var getCmd = &cobra.Command{
	Use:   "get [token ID]",
	Short: "Get token(s)",
	Long:  "Get all tokens unless token ID is specified",
	Args:  cobra.RangeArgs(0, 1),
	RunE:  tokenGet,
}

func tokenGet(cmd *cobra.Command, args []string) error {
	client := getTokenClient()
	if len(args) >= 1 {
		return getOneToken(client, args[0])
	}
	req := client.NewRequest("GET", "/tokens", "")
	client.PrintDebug("sending request to '%s'", req.URL)
	client.DoRequest(req)
	return nil
}

func getOneToken(client utils.Client, tokenID string) error {
	client.PrintDebug("Parsed command line flags:\nid: '%s'", tokenID)
	req := client.NewRequest("GET", "/tokens/"+neturl.PathEscape(tokenID), "")
	client.DoRequest(req)
	return nil
}
