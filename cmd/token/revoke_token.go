package token

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/rs/xid"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"io"
	neturl "net/url"
	"strings"
)

var tokenRevokeParams = struct {
	service.TokenModel
	TokenString string
}{}

var revokeCmd = &cobra.Command{
	Use:               "revoke ([flags])",
	Short:             "Revoke/expire a token",
	ValidArgs:         []string{},
	ValidArgsFunction: nil,
	Args:              cobra.NoArgs,
	RunE:              tokenRevoke,
	SilenceUsage:      true,
}

func tokenRevoke(cmd *cobra.Command, args []string) error {
	client := getTokenClient()
	client.PrintDebug("Parsed flags are %+v", tokenCreateParams)
	client.PrintDebug("Parsed args are %+v", args)

	if len(tokenRevokeParams.TokenString) <= 0 {
		return fmt.Errorf("must provide token as token-xid or full token secret")
	}
	tokenID := common.ID("")
	dashIndex := strings.IndexRune(tokenRevokeParams.TokenString, '-')
	// if id provided is a full token, grab the xid from it.
	if dashIndex >= 0 && (tokenRevokeParams.TokenString[:dashIndex] == string(common.PersonalTokenPrefix) ||
		tokenRevokeParams.TokenString[:dashIndex] == string(common.InternalTokenPrefix) ||
		tokenRevokeParams.TokenString[:dashIndex] == string(common.DelegatedTokenPrefix)) {
		lastDash := strings.LastIndex(tokenRevokeParams.TokenString, "-")
		tokenXid, err := xid.FromString(tokenRevokeParams.TokenString[lastDash+1:])
		if err != nil {
			return err
		}
		tokenID = common.NewIDFromXID("token", tokenXid)
	} else {
		// try using provided token string as token-xid
		tokenID = common.ID(tokenRevokeParams.TokenString)
		if !tokenID.Validate() {
			return fmt.Errorf("'%s' is not a valid ID", tokenID)
		}
		if tokenID.PrimaryType() != "token" {
			return fmt.Errorf("'%s' is not a token ID", tokenID)
		}
	}
	tokenRevokeParams.ID = tokenID
	data, err := json.Marshal(&tokenRevokeParams.TokenModel)
	if err != nil {
		return err
	}
	client.PrintDebug("constructed request body:\n%s\n", data)
	req := client.NewRequest("POST", "/tokens/"+neturl.PathEscape(tokenRevokeParams.ID.String()+"/revoke"), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	err = client.DoRequest(req)
	if err != nil {
		return err
	}
	return nil
}

func init() {
	revokeCmd.Flags().StringVarP(&tokenRevokeParams.TokenString, "token", "t", "", "TokenID to revoke/expire")
	_ = revokeCmd.MarkFlagRequired("token")
}
