package token

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for user sub-command
var Cmd = &cobra.Command{
	Use:          "token",
	Short:        "Manage tokens",
	Args:         cobra.NoArgs,
	SilenceUsage: true,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(getTypesCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(revokeCmd)
}

func getTokenClient() utils.Client {
	client := utils.NewClient()
	return client
}
