package templatesourcetype

import (
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:          "get [flags]",
	Short:        "Get template source types",
	Long:         `Gets all template source types`,
	Args:         cobra.NoArgs,
	RunE:         templateSourceTypeGet,
	SilenceUsage: true,
}

// Run ...
func templateSourceTypeGet(command *cobra.Command, args []string) error {
	client := getTemplateSourceTypeClient()

	req := client.NewRequest("GET", "/templates/sourcetypes", "")
	return client.DoRequest(req)
}
