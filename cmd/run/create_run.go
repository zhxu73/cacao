package run

import (
	"encoding/json"
	"fmt"
	neturl "net/url"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// runCreateParams ...
var runCreateParams = struct {
	http.DeploymentRun
	Filename    string
	Params      string
	CloudCredID string
}{}

// createCmd ...
var createCmd = &cobra.Command{
	Use:          "create (-f <filename> | <deployment-id> [flags])",
	Short:        "Create a run",
	Args:         utils.ExactNArgsOrFileFlag(1),
	RunE:         runCreate,
	SilenceUsage: true,
}

func init() {
	createCmd.Flags().StringVarP(&runCreateParams.Filename, "filename", "f", "", "JSON file to read deployment info from")
	createCmd.Flags().StringVarP(&runCreateParams.Params, "params", "p", "", "run parameters")
	createCmd.Flags().StringVar(&runCreateParams.CloudCredID, "cloud-cred", "", "id of cloud credential")
	createCmd.Flags().StringVar(&runCreateParams.GitCredentialID, "git-cred", "", "id of git credential")
}

// runCreate ...
func runCreate(command *cobra.Command, args []string) error {
	client := getRunClient()
	if command.Flags().Changed("filename") {
		data, err := os.ReadFile(runCreateParams.Filename)
		if err != nil {
			return err
		}
		err = json.Unmarshal(data, &runCreateParams.DeploymentRun)
		if err != nil {
			return err
		}
	} else {
		runCreateParams.DeploymentID = args[0]

		parameters, err := parseRunParameters(runCreateParams.Params)
		if err != nil {
			return err
		}
		runCreateParams.DeploymentRun.Parameters = parameters

		if command.Flags().Changed("cloud-cred") {
			runCreateParams.DeploymentRun.CloudCredentials = []string{runCreateParams.CloudCredID}
		}

		if command.Flags().Changed("git-cred") {
			runCreateParams.DeploymentRun.GitCredentialID = runCreateParams.GitCredentialID
		}
	}
	marshal, err := json.Marshal(runCreateParams.DeploymentRun)
	if err != nil {
		return err
	}
	req := client.NewRequest("POST", fmt.Sprintf("/deployments/%s/runs", neturl.PathEscape(runCreateParams.DeploymentID)), string(marshal))
	return client.DoRequest(req)
}

func parseRunParameters(parameters string) ([]http.KeyValue, error) {
	if parameters == "" {
		return []http.KeyValue{}, nil
	}
	// try parse as JSON first
	result, err := parseRunParametersAsJSON(parameters)
	if err == nil {
		return result, nil
	}
	// if NOT JSON, then parse as key-value pairs(e.g. "k1=v1,k2=v2")
	return utils.ParseKeyValuePairs(parameters)
}

func parseRunParametersAsJSON(parameters string) ([]http.KeyValue, error) {
	var result []http.KeyValue
	var paramsJSON map[string]interface{}
	err := json.Unmarshal([]byte(parameters), &paramsJSON)
	if err != nil {
		return nil, err
	}
	for k, v := range paramsJSON {
		if _, ok := v.(string); !ok {
			// convert value to string
			valueMarshal, err := json.Marshal(v)
			if err != nil {
				return nil, err
			}
			result = append(result, http.KeyValue{
				Key:   k,
				Value: string(valueMarshal),
			})
		} else {
			// if already string, then just use value as is
			result = append(result, http.KeyValue{
				Key:   k,
				Value: v.(string),
			})
		}
	}
	return result, err
}
