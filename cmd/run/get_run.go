package run

import (
	"fmt"
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get <deployment-id> [run-id] [flags]",
	Short:        "Get run(s)",
	Long:         `Gets all runs of a deployment unless specific run ID is provided`,
	Args:         cobra.RangeArgs(1, 2),
	RunE:         runGet,
	SilenceUsage: true,
}

func runGet(command *cobra.Command, args []string) error {

	client := getRunClient()
	deploymentID := args[0]
	if len(args) >= 2 {
		runID := args[1]
		return getOneRun(client, deploymentID, runID)
	}

	req := client.NewRequest("GET", fmt.Sprintf("/deployments/%s/runs", neturl.PathEscape(deploymentID)), "")
	return client.DoRequest(req)
}

func getOneRun(client utils.Client, deploymentID, runID string) error {
	req := client.NewRequest("GET", fmt.Sprintf("/deployments/%s/runs/%s", neturl.PathEscape(deploymentID), neturl.PathEscape(runID)), "")
	return client.DoRequest(req)
}
