package run

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for run sub-command
var Cmd = &cobra.Command{
	Use:   "run",
	Short: "Manage run",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
}

func getRunClient() utils.Client {
	client := utils.NewClient()
	return client
}
