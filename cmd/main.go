package main

import (
	"fmt"
	"gitlab.com/cyverse/cacao/cmd/templateversion"
	"os"

	cli "github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/credential"
	"gitlab.com/cyverse/cacao/cmd/deployment"
	"gitlab.com/cyverse/cacao/cmd/isession"
	"gitlab.com/cyverse/cacao/cmd/js2allocation"
	"gitlab.com/cyverse/cacao/cmd/js2user"
	"gitlab.com/cyverse/cacao/cmd/login"
	"gitlab.com/cyverse/cacao/cmd/logout"
	"gitlab.com/cyverse/cacao/cmd/provider"
	"gitlab.com/cyverse/cacao/cmd/run"
	"gitlab.com/cyverse/cacao/cmd/template"
	"gitlab.com/cyverse/cacao/cmd/templatecustomfieldtype"
	"gitlab.com/cyverse/cacao/cmd/templatesourcetype"
	"gitlab.com/cyverse/cacao/cmd/templatetype"
	"gitlab.com/cyverse/cacao/cmd/token"
	"gitlab.com/cyverse/cacao/cmd/user"
	"gitlab.com/cyverse/cacao/cmd/useraction"
	"gitlab.com/cyverse/cacao/cmd/utils"
	"gitlab.com/cyverse/cacao/cmd/workspace"
)

var rootCmd = &cli.Command{
	Use:   "cacao",
	Short: "cacao interacts with the CACAO REST API",
	Long: `CACAO CLI is a command line program for interacting with CACAO REST API.
It is used to manage users, providers, and other resources.

CACAO API requires authentication. You need to login with "cacao login" command.
`,
}

var versionCmd = &cli.Command{
	Use:   "version",
	Short: "cacao cli version",
	Run: func(cmd *cli.Command, args []string) {
		if GitCommit == "" {
			fmt.Println("unknown")
		} else {
			fmt.Println(GitCommit)
		}
	},
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&utils.Curl, "curl", "c", false, "Print curl command")
	rootCmd.PersistentFlags().BoolVarP(&utils.Verbose, "verbose", "v", false, "Print verbose log")
	rootCmd.PersistentFlags().BoolVarP(&utils.MoreVerbose, "vv", "", false, "Print more verbose log")

	rootCmd.AddCommand(login.Cmd)
	rootCmd.AddCommand(logout.Cmd)
	rootCmd.AddCommand(versionCmd)

	rootCmd.AddCommand(credential.Cmd)
	rootCmd.AddCommand(deployment.Cmd)
	rootCmd.AddCommand(isession.Cmd)
	rootCmd.AddCommand(js2allocation.Cmd)
	rootCmd.AddCommand(js2user.Cmd)
	rootCmd.AddCommand(provider.Cmd)
	rootCmd.AddCommand(run.Cmd)
	rootCmd.AddCommand(template.Cmd)
	rootCmd.AddCommand(templatetype.Cmd)
	rootCmd.AddCommand(templatesourcetype.Cmd)
	rootCmd.AddCommand(templatecustomfieldtype.Cmd)
	rootCmd.AddCommand(templateversion.Cmd)
	rootCmd.AddCommand(token.Cmd)
	rootCmd.AddCommand(user.Cmd)
	rootCmd.AddCommand(workspace.Cmd)
	rootCmd.AddCommand(useraction.Cmd)
}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
