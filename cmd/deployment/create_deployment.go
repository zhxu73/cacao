package deployment

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// deploymentCreateParams ...
var deploymentCreateParams = struct {
	http.Deployment
	Filename   string
	CloudCreds string
}{}

// createCmd ...
var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <workspace-id> <template-id> <provider-id> [flags])",
	Short:                 "Create a deployment",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(3),
	RunE:                  deploymentCreate,
	SilenceUsage:          true,
}

// deploymentCreate ...
func deploymentCreate(command *cobra.Command, args []string) error {
	client := getDeploymentClient()
	if command.Flags().Changed("filename") {
		data, err := os.ReadFile(deploymentCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &deploymentCreateParams.Deployment); err != nil {
			return err
		}
	} else {
		deploymentCreateParams.WorkspaceID = args[0]
		deploymentCreateParams.TemplateID = args[1]
		deploymentCreateParams.PrimaryProviderID = args[2]
	}

	if command.Flags().Changed("cloud-creds") {
		// the format of the cloud-creds flag is a comma-separated list of strings,
		// with the format "value1,value2,value3"
		// we need to split the string into a slice of strings
		deploymentCreateParams.CloudCredentials = strings.Split(deploymentCreateParams.CloudCreds, ",")

	}

	data, err := json.Marshal(deploymentCreateParams.Deployment)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/deployments", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&deploymentCreateParams.Filename, "file", "f", "", "JSON file to read deployment info from")
	createCmd.Flags().StringVar(&deploymentCreateParams.CloudCreds, "cloud-creds", "", "Cloud credentials to use for the deployment")
	createCmd.Flags().StringVar(&deploymentCreateParams.GitCredentialID, "git-cred", "", "Git credential to use for the deployment")
	createCmd.Flags().StringVarP(&deploymentCreateParams.Name, "name", "n", "", "Deployment name, need to be FQDN")
	createCmd.Flags().StringVar(&deploymentCreateParams.Description, "description", "", "Deployment description")
	_ = createCmd.MarkFlagRequired("name")
	_ = createCmd.Flags().MarkHidden("file")

}
