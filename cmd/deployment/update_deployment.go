package deployment

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// deploymentUpdateParams ...
var deploymentUpdateParams = struct {
	http.Deployment
	Filename      string
	Params        string
	ValueFilename string
	CloudCreds    string
}{}

// updateCmd ...
var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <deployment-id> [flags])",
	Short:                 "Update a deployment",
	Long:                  "Update deployment information",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(1),
	RunE:                  deploymentUpdate,
	SilenceUsage:          true,
}

// deploymentUpdate ...
func deploymentUpdate(command *cobra.Command, args []string) error {
	client := getDeploymentClient()
	if command.Flags().Changed("filename") {
		data, err := os.ReadFile(deploymentUpdateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &deploymentUpdateParams.Deployment); err != nil {
			return err
		}
	} else {
		deploymentUpdateParams.ID = args[0]
	}

	if command.Flags().Changed("params") {
		// the format of the params is key=value,key=value
		// we need to split the string into an array of KeyValue
		parameters, err := utils.ParseKeyValuePairs(deploymentUpdateParams.Params)
		if err != nil {
			return err
		}
		deploymentUpdateParams.Parameters = parameters
	}
	if command.Flags().Changed("cloud-creds") {
		deploymentUpdateParams.CloudCredentials = strings.Split(deploymentUpdateParams.CloudCreds, ",")
	}

	data, err := json.Marshal(deploymentUpdateParams.Deployment)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("PATCH", "/deployments", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&deploymentUpdateParams.Filename, "file", "f", "", "JSON file to read deployment info from")
	updateCmd.Flags().StringVarP(&deploymentUpdateParams.Name, "name", "", "", "Name of the deployment")
	updateCmd.Flags().StringVarP(&deploymentUpdateParams.Description, "description", "", "", "Description of the deployment")
	updateCmd.Flags().StringVarP(&deploymentUpdateParams.Params, "params", "p", "", "Parameters to pass to the deployment")
	updateCmd.Flags().StringVarP(&deploymentUpdateParams.CloudCreds, "cloud-creds", "", "", "Cloud credentials to use for the deployment")
	updateCmd.Flags().StringVarP(&deploymentUpdateParams.GitCredentialID, "git-cred", "", "", "Git credentials to use for the deployment")
	updateCmd.Flags().MarkHidden("file")

}
