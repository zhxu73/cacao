package deployment

import (
	"net/http"
	neturl "net/url"
	"strconv"

	"gitlab.com/cyverse/cacao/cmd/utils"

	"github.com/spf13/cobra"
)

// GetDeploymentCommand ...
var deploymentGetParams struct {
	workspaceID string
	fullRun     bool
	offset      uint64
	pageSize    uint64
}

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get [deployment-id] [flags]",
	Short:        "Get deployment(s)",
	Long:         `Gets all deployments unless specific ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         deploymentGet,
	SilenceUsage: true,
}

func init() {
	getCmd.Flags().StringVar(&deploymentGetParams.workspaceID, "workspace-id", "", "workspace id to retrieve")
	getCmd.Flags().BoolVar(&deploymentGetParams.fullRun, "full-run", false, "full run")
	getCmd.Flags().Uint64Var(&deploymentGetParams.offset, "offset", 0, "offset")
	getCmd.Flags().Uint64Var(&deploymentGetParams.pageSize, "page-size", 0, "page size")
}

func deploymentGet(command *cobra.Command, args []string) error {

	client := getDeploymentClient()
	if len(args) >= 1 {
		return getOneDeployment(client, args[0])
	}

	var req *http.Request
	var query = neturl.Values{}
	if command.Flags().Changed("workspace-id") {
		query.Set("workspaceId", deploymentGetParams.workspaceID)
	}
	if command.Flags().Changed("full-run") {
		query.Set("fullRun", "t")
	}
	if command.Flags().Changed("offset") {
		query.Set("offset", strconv.FormatUint(deploymentGetParams.offset, 10))
	}
	if command.Flags().Changed("page-size") && deploymentGetParams.pageSize > 0 {
		query.Set("pageSize", strconv.FormatUint(deploymentGetParams.pageSize, 10))
	}

	path := "/deployments"
	if query.Encode() != "" {
		path += "?" + query.Encode()
	}
	req = client.NewRequest("GET", path, "")
	return client.DoRequest(req)
}

func getOneDeployment(client utils.Client, deploymentID string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", deploymentID)
	req := client.NewRequest("GET", "/deployments/"+neturl.PathEscape(deploymentID), "")
	return client.DoRequest(req)
}
