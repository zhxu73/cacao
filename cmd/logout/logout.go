package logout

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for logout sub-command
var Cmd = &cobra.Command{
	Use:   "logout",
	Short: "Logs out of the current user",
	RunE:  logout,
}

func logout(command *cobra.Command, args []string) error {
	utils.ClearDotCACAODirectory()
	fmt.Println("Successfully logged out!")
	return nil
}
