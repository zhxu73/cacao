package templatecustomfieldtype

import (
	"bytes"
	"encoding/json"
	"io"
	neturl "net/url"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// templateCustomFieldTypeUpdateParams ...
var templateCustomFieldTypeUpdateParams = struct {
	service.TemplateCustomFieldTypeModel
	Filename string
}{}

// updateCmd ...
var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <name> <query-method> <query-target> [flags])",
	Short:                 "Update a template custom field type",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(3),
	RunE:                  templateCustomFieldTypeUpdate,
	SilenceUsage:          true,
}

func templateCustomFieldTypeUpdate(command *cobra.Command, args []string) error {
	client := getTemplateCustomFieldTypeClient()
	client.PrintDebug("Parsed flags are: %v", templateCustomFieldTypeUpdateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(templateCustomFieldTypeUpdateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &templateCustomFieldTypeUpdateParams.TemplateCustomFieldTypeModel); err != nil {
			return err
		}
	} else {
		templateCustomFieldTypeUpdateParams.Name = args[0]
		templateCustomFieldTypeUpdateParams.QueryMethod = service.TemplateCustomFieldTypeQueryMethod(args[1])
		templateCustomFieldTypeUpdateParams.QueryTarget = args[2]
	}

	data, err := json.Marshal(templateCustomFieldTypeUpdateParams.TemplateCustomFieldTypeModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("PATCH", "/templates/customfieldtypes/"+neturl.PathEscape(string(templateCustomFieldTypeUpdateParams.Name)), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&templateCustomFieldTypeUpdateParams.Filename, "file", "f", "", "The filename of the template type to update")
	updateCmd.Flags().MarkHidden("file")

	updateCmd.Flags().StringVarP(&templateCustomFieldTypeUpdateParams.Description, "description", "", "", "The description of the template custom field type to update")
	updateCmd.Flags().StringVarP(&templateCustomFieldTypeUpdateParams.QueryData, "query-data", "", "", "The query data of the template custom field type to update")
	updateCmd.Flags().StringVarP(&templateCustomFieldTypeUpdateParams.QueryResultJSONPathFilter, "result-filter", "", "", "The query result jsonpath filter of the template custom field type to update")
}
