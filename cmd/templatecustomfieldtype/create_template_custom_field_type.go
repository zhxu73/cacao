package templatecustomfieldtype

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// templateCustomFieldTypeCreateParams
var templateCustomFieldTypeCreateParams = struct {
	// ignore empty fields
	service.TemplateCustomFieldTypeModel
	Filename string
}{}

// createCmd ...

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <name> <query-method> <query-target> [flags])",
	Short:                 "Create a template custom field type",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(3),
	RunE:                  templateCustomFieldTypeCreate,
	SilenceUsage:          true,
}

func templateCustomFieldTypeCreate(command *cobra.Command, args []string) error {
	client := getTemplateCustomFieldTypeClient()
	client.PrintDebug("Parsed flags are: %v", templateCustomFieldTypeCreateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(templateCustomFieldTypeCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &templateCustomFieldTypeCreateParams.TemplateCustomFieldTypeModel); err != nil {
			return err
		}
	} else {
		templateCustomFieldTypeCreateParams.Name = args[0]
		templateCustomFieldTypeCreateParams.QueryMethod = service.TemplateCustomFieldTypeQueryMethod(args[1])
		templateCustomFieldTypeCreateParams.QueryTarget = args[2]
	}

	data, err := json.Marshal(templateCustomFieldTypeCreateParams.TemplateCustomFieldTypeModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/templates/customfieldtypes", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&templateCustomFieldTypeCreateParams.Filename, "file", "f", "", "The filename of the template custom field type to create")
	createCmd.Flags().MarkHidden("file")

	createCmd.Flags().StringVarP(&templateCustomFieldTypeCreateParams.Description, "description", "", "", "The description of the template custom field type to create")
	createCmd.Flags().StringVarP(&templateCustomFieldTypeCreateParams.QueryData, "query-data", "", "", "The query data of the template custom field type to create")
	createCmd.Flags().StringVarP(&templateCustomFieldTypeCreateParams.QueryResultJSONPathFilter, "result-filter", "", "", "The query result jsonpath filter of the template custom field type to create")
}
