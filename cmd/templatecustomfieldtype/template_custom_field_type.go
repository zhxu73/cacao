package templatecustomfieldtype

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for template_custon_field_type sub-command
var Cmd = &cobra.Command{
	Use:   "template_custom_field_type",
	Short: "Manage template custom field types",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(deleteCmd)
}

func getTemplateCustomFieldTypeClient() utils.Client {
	client := utils.NewClient()
	return client
}
