package templatecustomfieldtype

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var getCmd = &cobra.Command{
	Use:          "get [name] [flags]",
	Short:        "Get template custom field type(s)",
	Long:         `Gets all template custom field types unless specific name is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         templateCustomFieldTypeGet,
	SilenceUsage: true,
}

// Run ...
func templateCustomFieldTypeGet(command *cobra.Command, args []string) error {
	client := getTemplateCustomFieldTypeClient()
	if len(args) >= 1 {
		return getOneTemplateCustomFieldType(client, args[0])
	}

	req := client.NewRequest("GET", "/templates/customfieldtypes", "")
	return client.DoRequest(req)
}

func getOneTemplateCustomFieldType(client utils.Client, templateCustomFieldTypeName string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", templateCustomFieldTypeName)
	req := client.NewRequest("GET", "/templates/customfieldtypes/"+neturl.PathEscape(templateCustomFieldTypeName), "")
	return client.DoRequest(req)
}
