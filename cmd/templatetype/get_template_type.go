package templatetype

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var getCmd = &cobra.Command{
	Use:          "get [name] [flags]",
	Short:        "Get template type(s)",
	Long:         `Gets all template types unless specific name is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         templateTypeGet,
	SilenceUsage: true,
}

// Run ...
func templateTypeGet(command *cobra.Command, args []string) error {
	client := getTemplateTypeClient()
	if len(args) >= 1 {
		return getOneTemplateType(client, args[0])
	}

	req := client.NewRequest("GET", "/templates/types", "")
	return client.DoRequest(req)
}

func getOneTemplateType(client utils.Client, templateTypeName string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", templateTypeName)
	req := client.NewRequest("GET", "/templates/types/"+neturl.PathEscape(templateTypeName), "")
	return client.DoRequest(req)
}
