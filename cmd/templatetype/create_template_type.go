package templatetype

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// templateTypeCreateParams
var templateTypeCreateParams = struct {
	// ignore empty fields
	service.TemplateTypeModel
	Filename string
}{}

// createCmd ...

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <name> <comma-separated-formats> <engine> <comma-separated-provider-types> [flags])",
	Short:                 "Create a template type",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(4),
	RunE:                  templateTypeCreate,
	SilenceUsage:          true,
}

func templateTypeCreate(command *cobra.Command, args []string) error {
	client := getTemplateTypeClient()
	client.PrintDebug("Parsed flags are: %v", templateTypeCreateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(templateTypeCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &templateTypeCreateParams.TemplateTypeModel); err != nil {
			return err
		}
	} else {
		templateTypeCreateParams.Name = service.TemplateTypeName(args[0])

		// formats
		formatSplits := strings.Split(args[1], ",")
		templateFormats := []service.TemplateFormat{}
		for _, formatField := range formatSplits {
			templateFormats = append(templateFormats, service.TemplateFormat(strings.ToLower(formatField)))
		}
		templateTypeCreateParams.Formats = templateFormats

		// engine
		templateTypeCreateParams.Engine = service.TemplateEngine(strings.ToLower(args[2]))

		// provider tyes
		providerTypeSplits := strings.Split(args[3], ",")
		templateProviderTypes := []service.TemplateProviderType{}
		for _, providerTypeField := range providerTypeSplits {
			templateProviderTypes = append(templateProviderTypes, service.TemplateProviderType(strings.ToLower(providerTypeField)))
		}
		templateTypeCreateParams.ProviderTypes = templateProviderTypes

	}

	data, err := json.Marshal(templateTypeCreateParams.TemplateTypeModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/templates/types", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&templateTypeCreateParams.Filename, "file", "f", "", "The filename of the template type to create")
	createCmd.Flags().MarkHidden("file")
}
