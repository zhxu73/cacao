package templatetype

import (
	"bytes"
	"encoding/json"
	"io"
	neturl "net/url"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// templateTypeUpdateParams ...
var templateTypeUpdateParams = struct {
	service.TemplateTypeModel
	Filename string
}{}

// updateCmd ...
var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <name> <comma-separated-formats> <engine> <comma-separated-provider-types> [flags])",
	Short:                 "Update a template type",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(4),
	RunE:                  templateTypeUpdate,
	SilenceUsage:          true,
}

func templateTypeUpdate(command *cobra.Command, args []string) error {
	client := getTemplateTypeClient()
	client.PrintDebug("Parsed flags are: %v", templateTypeUpdateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(templateTypeUpdateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &templateTypeUpdateParams.TemplateTypeModel); err != nil {
			return err
		}
	} else {
		templateTypeUpdateParams.Name = service.TemplateTypeName(args[0])

		// formats
		formatSplits := strings.Split(args[1], ",")
		templateFormats := []service.TemplateFormat{}
		for _, formatField := range formatSplits {
			templateFormats = append(templateFormats, service.TemplateFormat(strings.ToLower(formatField)))
		}
		templateTypeUpdateParams.Formats = templateFormats

		// engine
		templateTypeUpdateParams.Engine = service.TemplateEngine(strings.ToLower(args[2]))

		// provider tyes
		providerTypeSplits := strings.Split(args[3], ",")
		templateProviderTypes := []service.TemplateProviderType{}
		for _, providerTypeField := range providerTypeSplits {
			templateProviderTypes = append(templateProviderTypes, service.TemplateProviderType(strings.ToLower(providerTypeField)))
		}
		templateTypeUpdateParams.ProviderTypes = templateProviderTypes
	}

	data, err := json.Marshal(templateTypeUpdateParams.TemplateTypeModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("PATCH", "/templates/types/"+neturl.PathEscape(string(templateTypeUpdateParams.Name)), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&templateTypeUpdateParams.Filename, "file", "f", "", "The filename of the template type to update")
	updateCmd.Flags().MarkHidden("file")
}
