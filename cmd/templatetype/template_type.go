package templatetype

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for template_type sub-command
var Cmd = &cobra.Command{
	Use:   "template_type",
	Short: "Manage template types",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(deleteCmd)
}

func getTemplateTypeClient() utils.Client {
	client := utils.NewClient()
	return client
}
