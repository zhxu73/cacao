package js2user

import (
	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get",
	Short:        "Gets JetStream2 user",
	Args:         cobra.NoArgs,
	RunE:         js2userGet,
	SilenceUsage: true,
}

func init() {

}

func js2userGet(command *cobra.Command, args []string) error {
	client := getJS2userClient()
	req := client.NewRequest("GET", "/js2user", "")
	return client.DoRequest(req)
}
