package useraction

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// CreateUserActionCommand ...
var userActionCreateParams = struct {
	service.UserActionModel
	Filename string
	DataType string
}{}

var createCmd = &cobra.Command{
	Use:   "create (-f <filename> | <type> <method> <source> <name> [flags])",
	Short: "Create a user action",
	Example: `cacao user_action create instance_execution script https://gitlab.com/cyverse/cacao/-/raw/main/cmd/useraction/example_script.py test1 --data-type text/x-python -d "short desc123" --public

cacao user_action create instance_execution script ./example_script.py test1 --data-type text/x-python
	`,
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(4),
	RunE:                  userActionCreate,
	SilenceUsage:          true,
}

func init() {
	createCmd.Flags().StringVarP(&userActionCreateParams.Filename, "file", "f", "", "JSON file to read user action info from")
	createCmd.Flags().StringVarP(&userActionCreateParams.Description, "description", "d", "", "Description of the user action")
	createCmd.Flags().BoolVarP(&userActionCreateParams.Public, "public", "p", false, "The visibility of the user action to create. Defaults to private")
	createCmd.Flags().StringVarP(&userActionCreateParams.DataType, "data-type", "", "", "The MIME type of the script")

	createCmd.Flags().MarkHidden("file")
	createCmd.MarkFlagsMutuallyExclusive("file", "description")
	createCmd.MarkFlagsMutuallyExclusive("file", "public")
	createCmd.MarkFlagsMutuallyExclusive("file", "data-type")
}

func getMimeType(filename string, content string) service.UserActionScriptDataType {
	ext := path.Ext(filename)
	switch ext {
	case "py":
		return service.UserActionScriptDataTypePython
	case "sh", "bash":
		return service.UserActionScriptDataTypeShellScript
	case "pl":
		return service.UserActionScriptDataTypePerl
	}

	contentLines := strings.Split(content, "\n")
	if len(contentLines) > 0 {
		if !strings.Contains(contentLines[0], "#!") {
			// does not have shebang line
			return service.UserActionScriptDataTypeUnknown
		}

		if strings.Contains(contentLines[0], "python") {
			return service.UserActionScriptDataTypePython
		}

		if strings.Contains(contentLines[0], "sh") {
			return service.UserActionScriptDataTypeShellScript
		}

		if strings.Contains(contentLines[0], "pl") {
			return service.UserActionScriptDataTypePerl
		}
	}

	return service.UserActionScriptDataTypeUnknown
}

func getScriptUserActionItem(sourceURL string, dataType service.UserActionScriptDataType) (service.UserActionItem, error) {
	sourceType := service.UserActionScriptSourceTypeUnknown
	data := ""
	content := ""

	if strings.HasPrefix(sourceURL, "http://") || strings.HasPrefix(sourceURL, "https://") {
		// url
		sourceType = service.UserActionScriptSourceTypeURL
		data = sourceURL

		response, err := http.Get(sourceURL)
		if err != nil {
			return service.UserActionItem{}, err
		}

		defer response.Body.Close()
		respBody, err := io.ReadAll(response.Body)
		if err != nil {
			return service.UserActionItem{}, err
		}

		content = string(bytes.TrimLeft(respBody, " \t\r\n"))
	} else if strings.HasPrefix(sourceURL, "file://") {
		// file
		sourceType = service.UserActionScriptSourceTypeRaw
		response, err := http.Get(sourceURL)
		if err != nil {
			return service.UserActionItem{}, err
		}

		defer response.Body.Close()
		respBody, err := io.ReadAll(response.Body)
		if err != nil {
			return service.UserActionItem{}, err
		}

		content = string(bytes.TrimLeft(respBody, " \t\r\n"))
		data = content
	} else {
		// file
		sourceType = service.UserActionScriptSourceTypeRaw
		sourceStat, err := os.Stat(sourceURL)
		if err != nil {
			return service.UserActionItem{}, err
		}

		if sourceStat.IsDir() {
			return service.UserActionItem{}, fmt.Errorf("failed to read source script at %s, as it is a dir", sourceURL)
		}

		dataBytes, err := os.ReadFile(sourceURL)
		if err != nil {
			return service.UserActionItem{}, err
		}

		content = string(dataBytes)
		data = content
	}

	// mime type
	filename := path.Base(sourceURL)
	autoDetectedDataType := getMimeType(filename, content)

	if dataType != service.UserActionScriptDataTypeUnknown {
		return service.NewUserActionItemForScriptAction(sourceType, dataType, data), nil
	}

	return service.NewUserActionItemForScriptAction(sourceType, autoDetectedDataType, data), nil
}

func userActionCreate(command *cobra.Command, args []string) error {
	client := getUserActionClient()
	client.PrintDebug("Parsed flags are: %v", userActionCreateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(userActionCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &userActionCreateParams.UserActionModel); err != nil {
			return err
		}
	} else {
		userActionCreateParams.Type = service.UserActionType(args[0])
		uaMethod := service.UserActionMethod(args[1])

		if userActionCreateParams.Type != service.UserActionTypeInstanceExecution {
			return fmt.Errorf("type not supported")
		}

		if uaMethod != service.UserActionMethodScript {
			return fmt.Errorf("method not supported")
		}

		sourceURL := args[2]
		userActionCreateParams.Name = args[3]

		// overwrite data type
		dataType := service.UserActionScriptDataTypeUnknown
		if command.Flags().Changed("data-type") {
			dataType = service.UserActionScriptDataType(userActionCreateParams.DataType)
		}

		userActionItem, err := getScriptUserActionItem(sourceURL, dataType)
		if err != nil {
			return err
		}

		userActionCreateParams.Action = userActionItem
	}

	data, err := json.Marshal(userActionCreateParams.UserActionModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("POST", "/useractions", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}
