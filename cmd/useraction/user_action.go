package useraction

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for user_action sub-command
var Cmd = &cobra.Command{
	Use:   "user_action",
	Short: "Manage user actions",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(updateCmd)
}

func getUserActionClient() utils.Client {
	client := utils.NewClient()
	return client
}
