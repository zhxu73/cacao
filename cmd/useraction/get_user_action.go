package useraction

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get [user-action-id] [flags]",
	Short:        "Get user action(s)",
	Long:         `Gets all user actions unless specific ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         userActionGet,
	SilenceUsage: true,
}

// Run ...
func userActionGet(command *cobra.Command, args []string) error {
	client := getUserActionClient()
	if len(args) >= 1 {
		return getOneUserAction(client, args[0])
	}

	req := client.NewRequest("GET", "/useractions", "")
	return client.DoRequest(req)
}

func getOneUserAction(client utils.Client, userActionID string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", userActionID)
	req := client.NewRequest("GET", "/useractions/"+neturl.PathEscape(userActionID), "")
	return client.DoRequest(req)
}
