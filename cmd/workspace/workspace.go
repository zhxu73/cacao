package workspace

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for workspace sub-command
var Cmd = &cobra.Command{
	Use:   "workspace",
	Short: "Manage workspaces",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(updateCmd)
}

func getWorkspaceClient() utils.Client {
	client := utils.NewClient()
	return client
}
