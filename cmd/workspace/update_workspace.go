package workspace

import (
	"bytes"
	"encoding/json"
	"io"
	neturl "net/url"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// workspaceUpdateParams ...
var workspaceUpdateParams = struct {
	service.WorkspaceModel
	Filename          string
	DefaultProviderID string
}{}

var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <id>) [flags]",
	Short:                 "Update a workspace",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(1),
	RunE:                  workspaceUpdate,
	SilenceUsage:          true,
}

func init() {
	updateCmd.Flags().StringVarP(&workspaceUpdateParams.Filename, "file", "f", "", "JSON file to read workspace info from")

	updateCmd.Flags().StringVarP(&workspaceUpdateParams.Description, "description", "d", "", "Description of the workspace")

	updateCmd.Flags().StringVarP(&workspaceUpdateParams.DefaultProviderID, "default-provider-id", "", "", "Default provider ID to use if none is specified.")

	updateCmd.Flags().StringVarP(&workspaceUpdateParams.Name, "name", "n", "", "Name of the workspace")

	updateCmd.Flags().MarkHidden("file")
	updateCmd.MarkFlagsMutuallyExclusive("file", "description")
	updateCmd.MarkFlagsMutuallyExclusive("file", "default-provider-id")
	updateCmd.MarkFlagsMutuallyExclusive("file", "name")
}

func workspaceUpdate(command *cobra.Command, args []string) error {
	client := getWorkspaceClient()
	if command.Flags().Changed("file") {
		data, err := os.ReadFile(workspaceUpdateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &workspaceUpdateParams.WorkspaceModel); err != nil {
			return err
		}

	} else {
		workspaceUpdateParams.ID = common.ID(args[0])
	}

	if command.Flags().Changed("default-provider-id") {
		workspaceUpdateParams.WorkspaceModel.DefaultProviderID = common.ID(workspaceUpdateParams.DefaultProviderID)
	}

	data, err := json.Marshal(workspaceUpdateParams.WorkspaceModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("PATCH", "/workspaces/"+neturl.PathEscape(workspaceUpdateParams.ID.String()), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}
