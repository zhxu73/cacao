package workspace

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// CreateWorkspaceCommand ...
var workspaceCreateParams = struct {
	service.WorkspaceModel
	Filename          string
	DefaultProviderID string
}{}

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <name> [flags])",
	Short:                 "Create a workspace",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(1),
	RunE:                  workspaceCreate,
	SilenceUsage:          true,
}

func init() {
	createCmd.Flags().StringVarP(&workspaceCreateParams.Filename, "file", "f", "", "JSON file to read workspace info from")
	createCmd.Flags().StringVarP(&workspaceCreateParams.Description, "description", "d", "", "Description of the workspace")
	createCmd.Flags().StringVarP(&workspaceCreateParams.DefaultProviderID, "default-provider-id", "", "", "Default provider ID to use if none is specified")
	createCmd.Flags().MarkHidden("file")
	createCmd.MarkFlagsMutuallyExclusive("file", "description")
	createCmd.MarkFlagsMutuallyExclusive("file", "default-provider-id")
}

func workspaceCreate(command *cobra.Command, args []string) error {
	client := getWorkspaceClient()
	if command.Flags().Changed("file") {
		data, err := os.ReadFile(workspaceCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &workspaceCreateParams.WorkspaceModel); err != nil {
			return err
		}

	} else {
		workspaceCreateParams.Name = args[0]
	}

	if command.Flags().Changed("default-provider-id") {
		workspaceCreateParams.WorkspaceModel.DefaultProviderID = common.ID(workspaceCreateParams.DefaultProviderID)
	}

	data, err := json.Marshal(workspaceCreateParams.WorkspaceModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("POST", "/workspaces", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)

}
