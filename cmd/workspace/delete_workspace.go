package workspace

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

// deleteCmd ...
var deleteCmd = &cobra.Command{
	Use:          "delete <workspace-id>",
	Short:        "Delete a workspace",
	RunE:         workspaceDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// workspaceDelete ...
func workspaceDelete(command *cobra.Command, args []string) error {
	client := getWorkspaceClient()
	workspaceID := args[0]
	req := client.NewRequest("DELETE", "/workspaces/"+neturl.PathEscape(workspaceID), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	// DoRequest should return an error if the request fails...
	return client.DoRequest(req)
}
