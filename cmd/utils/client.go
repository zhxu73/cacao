package utils

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	neturl "net/url"
	"os"
	"strings"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/api-service/constants"

	log "github.com/sirupsen/logrus"
)

// Client ...
type Client struct {
	Output
	cacaoAPI    string
	authToken   string
	username    string
	tokenHeader string
}

// NewClient ...
func NewClient() (c Client) {
	err := c.getOrAskCacaoAPI()
	if err != nil {
		c.PrintError(err.Error())
		panic(err)
	}
	c.Output = NewOutput()
	return
}

// NewClientWithAPIAddr ...
func NewClientWithAPIAddr(cacaoAPI string) (c Client) {
	c.cacaoAPI = cacaoAPI
	c.Output = NewOutput()
	return
}

// GetHTPPClient returns an HTTP Client to be used by the CLI
func (c *Client) GetHTPPClient() *http.Client {
	log.Trace("Client: start")
	return &http.Client{}
}

// DoRequest will either execute a Request or print the curl command
func (c *Client) DoRequest(req *http.Request) error {
	log.Trace("DoRequest: start")
	if c.OutputCurlString {
		c.PrintCurlString(req)
		return nil
	}
	err := c.readAuthToken()
	if err != nil {
		c.PrintError(err.Error())
		return err
	}
	c.PrintDebug("Request struct:\n%v\n", req)
	if len(req.Header.Get(c.tokenHeader)) == 0 {
		// set the auth header again, in case user just logged in
		req.Header.Set(c.tokenHeader, c.authToken)
	}

	resp, err := c.GetHTPPClient().Do(req)
	if err != nil {
		c.PrintError(err.Error())
		return err
	}
	c.PrintDebug("Response struct:\n%v\n", resp)
	c.PrintHTTPResponse(resp)

	if resp.StatusCode >= 400 {
		err = fmt.Errorf("received HTTP error status code %d (%s)", resp.StatusCode, resp.Status)
		c.PrintError(err.Error())
		return err
	}

	return nil
}

// NewRequest creates a new HTTP request with the correct header
func (c *Client) NewRequest(verb, path string, data string) *http.Request {
	log.Trace("NewRequest: start")
	req, err := http.NewRequest(verb, c.cacaoAPI+path, nil)
	if err != nil {
		panic(err) // if error here, then caller is passed bad arguments
	}
	if len(data) > 0 {
		req.Body = io.NopCloser(strings.NewReader(data))
	}
	// see if we can't tell what kind of token we're dealing with
	// and set the header key appropriately
	if c.tokenHeader == "" {
		if checkIfCACAOToken(c.authToken) {
			c.tokenHeader = constants.RequestHeaderCacaoAPIToken
		} else {
			c.tokenHeader = "Authorization"
		}
	}
	req.Header.Add(c.tokenHeader, c.authToken)
	return req
}

// getOrAskCacaoAPI checks for ~/.cacao/api.json or asks users for cacao API address
func (c *Client) getOrAskCacaoAPI() error {
	log.Trace("getOrAskCacaoAPI: start")

	apiEnv, ok := os.LookupEnv("CACAO_API")
	if ok && apiEnv != "" {
		c.cacaoAPI = apiEnv
		return nil
	}

	apiBaseURL, err := c.readAPIFromFile()
	if err == nil {
		c.cacaoAPI = apiBaseURL
		return nil
	}
	apiBaseURL, err = c.askForCacaoAPI()
	if err != nil {
		return err
	}
	c.cacaoAPI = apiBaseURL
	return nil
}

func (c *Client) readAPIFromFile() (string, error) {
	exists, err := fileExists(cacaoAPIJSONPath)
	if err != nil {
		return "", err
	}
	if !exists {
		return "", fmt.Errorf("api is missing")
	}
	var config APIFile
	err = readFileAsJSON(cacaoAPIJSONPath, &config)
	if err != nil {
		return "", err
	}
	if config.APIBaseURL == "" {
		return "", fmt.Errorf("api is empty")
	}
	return config.APIBaseURL, nil
}

// askForCacaoAPI asks for the user to enter the address of the API
func (c *Client) askForCacaoAPI() (string, error) {
	log.Trace("askForCacaoAPI: start")

	//var tries int
	var cacaoAPI string
	fmt.Println("Please provide address of Cacao API.")
	fmt.Println("Format Should be: http://<hostname>:<port>	  or    <hostname>:<port> ")
	fmt.Println("(Developers: this should match the value of API_DOMAIN in install/config.yaml followed by \"/api\", e.g. http://ca.cyverse.local/api)")
	var err error
	cacaoAPI, err = GetInput("Cacao API address (https://cacao.jetstream-cloud.org/api): ",
		ValidateInput(IsHTTPOrHTTPSUrl), Retry(2), DefaultInput("https://cacao.jetstream-cloud.org/api"))
	if err != nil {
		return "", err
	}
	createDotCACAODirectoryIfNotExist()
	err = WriteAPIURLToConfigFile(cacaoAPI)
	if err != nil {
		return "", err
	}
	return cacaoAPI, nil
}

// IsHTTPOrHTTPSUrl check if input string is HTTP or HTTPS URL
func IsHTTPOrHTTPSUrl(input string) error {
	parsed, err := neturl.Parse(input)
	if err != nil {
		return err
	}
	if parsed.Scheme != "http" && parsed.Scheme != "https" {
		return fmt.Errorf("invalid URL scheme, must be http(s)")
	}
	return nil
}

// WriteAPIURLToConfigFile writes the CACAO api URL to a config file in the .cacao directory
func WriteAPIURLToConfigFile(cacaoAPI string) error {
	createDotCACAODirectoryIfNotExist()
	naf := APIFile{cacaoAPI}
	result, err := json.Marshal(naf)
	if err != nil {
		return err
	}
	err = writeToAPIJSONFile(result)
	if err != nil {
		return err
	}
	return nil
}

// readAuthToken read auth token from config file. If not exist then ask user to login.
func (c *Client) readAuthToken() error {
	log.Trace("readAuthTokenOrLogin: start")

	tokenEnv, ok := os.LookupEnv("CACAO_TOKEN")
	if ok && tokenEnv != "" {
		c.authToken = tokenEnv
		return nil
	}

	authToken, err := readAuthTokenFromFile()
	if err != nil {
		c.PrintError(err.Error())
		fmt.Println("You need to login first before using this command.")
		return err
	}
	c.authToken = authToken
	if checkIfCACAOToken(authToken) {
		c.tokenHeader = constants.RequestHeaderCacaoAPIToken
	} else {
		c.tokenHeader = "Authorization"
	}
	return nil
}

func checkIfCACAOToken(token string) bool {
	// CACAO token has a prefix, we will check its presence to determine if the token looks like a CACAO API token
	indexOfUnderscore := strings.IndexRune(token, '_')
	if indexOfUnderscore < 0 {
		return false
	}
	prefix := token[:indexOfUnderscore]
	if prefix == string(common.PersonalTokenPrefix) || prefix == string(common.InternalTokenPrefix) || prefix == string(common.DelegatedTokenPrefix) {
		return true
	}
	return false
}
