package utils

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/mitchellh/cli"
	log "github.com/sirupsen/logrus"
)

type userInputOption struct {
	validate      func(string) error
	maxRetryCount uint
	defaultValue  string
}

// UserInputOptionFunc is the type for options for GetInput()
type UserInputOptionFunc func(option *userInputOption)

// DefaultInput sets a default value for GetInput(), default value is used if user enters empty string (just hit enter).
func DefaultInput(defaultValue string) UserInputOptionFunc {
	return func(option *userInputOption) {
		option.defaultValue = defaultValue
	}
}

// ValidateInput adds validation to GetInput()
func ValidateInput(validate func(string) error) UserInputOptionFunc {
	return func(option *userInputOption) {
		option.validate = validate
	}
}

// Retry adds retry to GetInput()
func Retry(maxRetryCount uint) UserInputOptionFunc {
	return func(option *userInputOption) {
		option.maxRetryCount = maxRetryCount
	}
}

// GetInput asks user for a string input. Additional options can be also specified.
func GetInput(prompt string, options ...UserInputOptionFunc) (string, error) {
	log.Trace("GetInput: start")
	return getInput(prompt, func() (string, error) {
		reader := bufio.NewReader(os.Stdin)
		return reader.ReadString('\n')
	}, options...)
}

// GetSecretInput asks user for a string input. Additional options can be also specified.
// The difference between this function and GetInput is that this function does not print user's input on the screen.
func GetSecretInput(prompt string, options ...UserInputOptionFunc) (string, error) {
	log.Trace("GetSecretInput: start")
	var UI cli.Ui
	UI = &cli.BasicUi{
		Reader:      os.Stdin,
		Writer:      os.Stdout,
		ErrorWriter: os.Stderr,
	}
	return getInput(prompt, func() (string, error) {
		return UI.AskSecret("")
	}, options...)
}

func getInput(prompt string, ask func() (string, error), options ...UserInputOptionFunc) (string, error) {
	var option userInputOption
	for _, optFunc := range options {
		optFunc(&option)
	}
	fmt.Print(prompt)
	var userInput string
	var err error
	for i := uint(0); i < option.maxRetryCount+1; i++ {
		userInput, _ = ask()
		userInput = strings.Trim(userInput, "\n\r")
		if userInput == "" && option.defaultValue != "" {
			userInput = option.defaultValue
			fmt.Printf("use default value %s\n", option.defaultValue)
		}
		if option.validate == nil {
			break
		}
		err = option.validate(userInput)
		if err != nil {
			fmt.Println(err.Error())
			fmt.Print(prompt)
		} else {
			break
		}
	}
	return userInput, err
}
