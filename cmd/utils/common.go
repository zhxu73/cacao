package utils

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
)

// MaximumNArgsOrFileFlag builds on top of cobra.MaximumNArgs.
// It checks if --file flag is set.
// If it is NOT set, then enforce a maximum number of arguments.
// If --file is set and other arguments are still provided then error.
func MaximumNArgsOrFileFlag(n int) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		// if filename is not set then just call the default cobra function
		if !fileFlagChanged(cmd) {
			err := cobra.MaximumNArgs(n)(cmd, args)
			if err != nil {
				return fmt.Errorf("must provide a filename or the correct number of required args (at most %d).\nSee 'cacao %s %s -h' for more information", n, cmd.Parent().Name(), cmd.Name())
			}
		} else if len(args) > 0 {
			return errors.New("cannot use the filename flag and provide arguments at the same time")
		}
		return nil
	}
}

// MinimumNArgsOrFileFlag builds on top of cobra.MinimumNArgs.
// It checks if --file flag is set.
// If it is NOT set, then requires a minimum number of arguments.
// If --file is set and other arguments are still provided then error.
func MinimumNArgsOrFileFlag(n int) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		// if filename is not set then just call the default cobra function
		if !fileFlagChanged(cmd) {
			err := cobra.MinimumNArgs(n)(cmd, args)
			if err != nil {
				return fmt.Errorf("must provide a filename or the correct number of required args (at least %d).\nSee 'cacao %s %s -h' for more information", n, cmd.Parent().Name(), cmd.Name())
			}
		} else if len(args) > 0 {
			return fmt.Errorf("cannot use the filename flag and provide arguments at the same time.\nSee 'cacao %s %s -h' for more information", cmd.Parent().Name(), cmd.Name())
		}
		return nil
	}
}

// ExactNArgsOrFileFlag builds on top of cobra.ExactArgs.
// It checks if --file flag is set.
// If it is NOT set, then requires a minimum number of arguments.
// If --file is set and other arguments are still provided then error.
func ExactNArgsOrFileFlag(n int) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		// if filename is not set then just call the default cobra function
		if !fileFlagChanged(cmd) {
			err := cobra.ExactArgs(n)(cmd, args)
			if err != nil {
				return fmt.Errorf("must provide a filename or the correct number of required args (%d).\nSee 'cacao %s %s -h' for more information", n, cmd.Parent().Name(), cmd.Name())
			}
		} else if len(args) > 0 {
			return fmt.Errorf("cannot use the filename flag and provide arguments at the same time.\nSee 'cacao %s %s -h' for more information", cmd.Parent().Name(), cmd.Name())
		}
		return nil
	}
}

func fileFlagChanged(cmd *cobra.Command) bool {
	return cmd.Flags().Changed("file")
}
