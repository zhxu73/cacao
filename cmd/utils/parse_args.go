package utils

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/http"
	"strings"
)

// ParseKeyValuePairs parse a parameter string into key-value pairs.
// The format of the params is key=value,key=value
func ParseKeyValuePairs(parameters string) ([]http.KeyValue, error) {
	var result []http.KeyValue
	var keySet = map[string]struct{}{} // a set of keys, this is used to de-duplicate keys
	if parameters == "" {
		return []http.KeyValue{}, nil
	}
	commaSeparatedList := strings.Split(parameters, ",")
	if commaSeparatedList[len(commaSeparatedList)-1] == "" {
		commaSeparatedList = commaSeparatedList[:len(commaSeparatedList)-1]
	}
	for _, entry := range commaSeparatedList {
		pair, err := parseOneKeyValuePair(entry)
		if err != nil {
			return []http.KeyValue{}, err
		}
		if _, keyExist := keySet[pair.Key]; keyExist {
			// if duplicate key, use the first pair, skip any later ones
			continue
		}
		keySet[pair.Key] = struct{}{}
		result = append(result, pair)
	}
	return result, nil
}

func parseOneKeyValuePair(onePair string) (http.KeyValue, error) {
	equalCharacterIndex := strings.Index(onePair, "=")
	if equalCharacterIndex < 0 {
		return http.KeyValue{}, fmt.Errorf("bad key-value pair, no equal charater, %s", onePair)
	}
	key := onePair[:equalCharacterIndex]
	value := onePair[equalCharacterIndex+1:]
	if key == "" {
		return http.KeyValue{}, fmt.Errorf("bad key-value pair, empty key, %s", onePair)
	}
	return http.KeyValue{
		Key:   key,
		Value: value,
	}, nil
}
