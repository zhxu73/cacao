package utils

import (
	"fmt"
	"io"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/constants"
)

// CheckToken checks if there is token in the config file, if token is present, then check if it is valid by making an API call.
func (c *Client) CheckToken() error {
	log.Trace("CheckLogin: start")

	var token string
	var ok bool
	token, ok = os.LookupEnv("CACAO_TOKEN")
	if !ok || token != "" {
		// try to read from file and see what format the token is in.
		var err error
		token, err = readAuthTokenFromFile()
		if err != nil {
			return err
		}
	}

	if checkIfCACAOToken(token) {
		// we have a cacao token, use it.
		c.authToken = token
		c.tokenHeader = constants.RequestHeaderCacaoAPIToken
		err := c.checkIfCacaoTokenIsValid()
		if err != nil {
			return err
		}
		return nil
	}

	// try as auth token
	c.authToken = token
	c.tokenHeader = "Authorization"
	err := c.checkIfTokenIsValid()
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) checkIfCacaoTokenIsValid() error {
	req := c.NewRequest("GET", "/tokens/types", "")
	var httpClient http.Client
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode >= 300 {
		respBody, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("HTTP %s. %s", resp.Status, string(respBody))
	}
	c.PrintDebug("token is valid, checked with GET /tokens/types, %s", resp.Status)
	return nil
}

func (c *Client) checkIfTokenIsValid() error {
	req := c.NewRequest("GET", "/users/mine", "")
	var httpClient http.Client
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode >= 300 {
		respBody, err := io.ReadAll(resp.Body)
		if err != nil {
			c.PrintError(err.Error())
		}
		return fmt.Errorf("HTTP %s, %s", resp.Status, string(respBody))
	}
	c.PrintDebug("token is valid, checked with GET /users/mine, %s", resp.Status)
	return nil
}

func readAuthTokenFromFile() (string, error) {
	var config ConfigFile
	err := readFileAsJSON(cacaoConfigJSONPath, &config)
	if err != nil {
		return "", err
	}
	if config.Token == "" {
		return "", fmt.Errorf("token is empty")
	}
	return config.Token, nil
}
