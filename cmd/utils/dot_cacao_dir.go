package utils

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
)

var (
	home                  = os.Getenv("HOME")
	dotCACAODirectory     = path.Join(home, ".cacao")
	cacaoConfigJSONPath   = path.Join(dotCACAODirectory, "config.json")
	cacaoAPIJSONPath      = path.Join(dotCACAODirectory, "api.json")
	cacaoUsernameJSONPath = path.Join(dotCACAODirectory, "username.json")
)

// ConfigFile is schema of ~/.cacao/config.json
type ConfigFile struct {
	Token string `json:"id_token"`
}

// APIFile is schema of ~/.cacao/api.json
type APIFile struct {
	APIBaseURL string `json:"api"`
}

// UsernameFile is schema of ~/.cacao/username.json
type UsernameFile struct {
	Username string `json:"username"`
}

func createDotCACAODirectoryIfNotExist() {
	_, err := os.Stat(dotCACAODirectory)
	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dotCACAODirectory, 0700)
		if errDir != nil {
			panic(err)
		}
	}
}

// ClearDotCACAODirectory removes any files in ~/.cacao used by cli
func ClearDotCACAODirectory() {
	errs := os.Remove(cacaoConfigJSONPath)
	if errs != nil && !os.IsNotExist(errs) {
		fmt.Printf("%s could not be removed\n", cacaoConfigJSONPath)
	}
	errs = os.Remove(cacaoAPIJSONPath)
	if errs != nil && !os.IsNotExist(errs) {
		fmt.Printf("%s could not be removed\n", cacaoAPIJSONPath)
	}
	errs = os.Remove(cacaoUsernameJSONPath)
	if errs != nil && !os.IsNotExist(errs) {
		fmt.Printf("%s could not be removed\n", cacaoUsernameJSONPath)
	}
}

// ~/.cacao/config.json
func writeToConfigJSONFile(data []byte) error {
	return os.WriteFile(cacaoConfigJSONPath, data, 0600)
}

// ~/.cacao/api.json
func writeToAPIJSONFile(data []byte) error {
	return os.WriteFile(cacaoAPIJSONPath, data, 0600)
}

func fileExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func readFileAsJSON(path string, output interface{}) error {
	content, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	err = json.Unmarshal(content, output)
	if err != nil {
		return err
	}
	return nil
}

// SaveAuthTokenToConfigFile saves auth token to ~/.cacao/config.json
func SaveAuthTokenToConfigFile(authToken string) error {
	var result = ConfigFile{
		Token: authToken,
	}
	marshal, err := json.Marshal(result)
	if err != nil {
		return err
	}
	return writeToConfigJSONFile(marshal)
}
