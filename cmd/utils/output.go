package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/fatih/color"
	prettyjson "github.com/hokaccha/go-prettyjson"
	"github.com/mitchellh/cli"
	log "github.com/sirupsen/logrus"
)

var (
	// Curl is global option, if true, then all HTTP request will be NOT be sent and only print as curl command
	Curl bool
	// Verbose is global option, if true, cli will print debug info in addition to the normal output
	Verbose bool
	// MoreVerbose is global option, if true, cli will print even more debug info
	MoreVerbose bool
	// PrintLog is a toggle that indicate that verbose/more-verbose logging is enabled.
	PrintLog bool
)

// Output contain the meta-option that nearly all subcommand inherited.
type Output struct {
	UI               cli.Ui
	OutputFormat     string
	OutputCurlString bool
	PrintLog         bool
}

// PrintCurlString will output a string that is the equivalent curl command for
// the specified request. This will be helpful for debugging. A majority of the
// code in this function comes from Hashicorp Vault's equivalent function:
// https://github.com/hashicorp/vault/blob/528604359c20a32fef628d86abdb7663e81235c3/api/output_string.go#L35-L64
func (m *Output) PrintCurlString(req *http.Request) {
	result := "curl"
	result += fmt.Sprintf(" -X %s", req.Method)
	for k, v := range req.Header {
		for _, h := range v {
			result += fmt.Sprintf(` -H "%s: %s"`, k, h)
		}
	}

	var body []byte
	var err error
	if req.Body != nil {
		body, err = io.ReadAll(req.Body)
		if err != nil {
			m.UI.Error("Unable to parse request body when constructing curl string")
			return
		}
		req.Body = io.NopCloser(bytes.NewBuffer(body))
	}

	if len(body) > 0 {
		removeWhitespace := regexp.MustCompile(`[\t\n]`)
		escapedBody := string(removeWhitespace.ReplaceAll(body, []byte("")))
		escapedBody = strings.Replace(escapedBody, "'", "'\"'\"'", -1)
		result += fmt.Sprintf(" -d '%s'", escapedBody)
	}

	result += fmt.Sprintf(" %s", req.URL.String())
	m.UI.Output(result)
}

// PrintHTTPResponse is used to print the body and error (if it exists) from an HTTP Response
func (m *Output) PrintHTTPResponse(response *http.Response) {
	respBody, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	// Determine if response contains Object or Array
	x := bytes.TrimLeft(respBody, " \t\r\n")
	isObject := len(x) > 0 && x[0] == '{'

	if len(respBody) > 0 {
		if response.StatusCode < 300 {
			if m.OutputFormat == "default_pretty" && isObject {
				var respJSON map[string]interface{}
				json.Unmarshal(respBody, &respJSON)

				formatter := NewFormatter()
				prettyJSON, err := formatter.Marshal(respJSON)
				if err != nil {
					panic(err)
				}
				m.UI.Output(string(prettyJSON))
			} else {
				m.UI.Output(string(respBody))
			}
		} else {
			m.UI.Error(string(respBody))
		}
	}
}

// PrintDebug is used to print some output only when the debug flag is used
func (m *Output) PrintDebug(msg string, extras ...interface{}) {
	if m.PrintLog {
		m.UI.Output(fmt.Sprintf(msg, extras...))
	}
}

// PrintError ...
func (m *Output) PrintError(msg string) {
	m.UI.Error(msg)
}

// NewFormatter returns a new formatter with following default values.
func NewFormatter() *prettyjson.Formatter {
	return &prettyjson.Formatter{
		KeyColor:        color.New(color.FgBlue, color.Bold),
		StringColor:     color.New(color.FgGreen, color.Bold),
		BoolColor:       color.New(color.FgYellow, color.Bold),
		NumberColor:     color.New(color.FgCyan, color.Bold),
		NullColor:       color.New(color.FgBlack, color.Bold),
		StringMaxLength: 0,
		DisabledColor:   false,
		Indent:          4,
		Newline:         "\n",
	}
}

// NewOutput ...
func NewOutput() Output {

	if Verbose {
		PrintLog = true
		log.SetLevel(log.DebugLevel)
	}

	if MoreVerbose {
		PrintLog = true
		log.SetLevel(log.TraceLevel)
	}

	return Output{
		UI: &cli.ColoredUi{
			InfoColor:  cli.UiColorBlue,
			ErrorColor: cli.UiColorRed,
			Ui: &cli.BasicUi{
				Reader:      os.Stdin,
				Writer:      os.Stdout,
				ErrorWriter: os.Stderr,
			},
		},
		OutputFormat:     "default_pretty",
		OutputCurlString: Curl,
		PrintLog:         PrintLog,
	}
}
