package provider

import (
	"github.com/spf13/cobra"

	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for provider sub-command
var Cmd = &cobra.Command{
	Use:   "provider",
	Short: "Manage providers",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(opCmd)

}

func getProviderClient() utils.Client {
	client := utils.NewClient()
	return client
}
