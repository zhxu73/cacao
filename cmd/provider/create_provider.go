package provider

import (
	"bytes"
	"encoding/json"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var providerCreateParams = struct {
	service.ProviderModel
	Filename         string
	MetadataFilename string
	MetadataStr      string
}{}

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <name> <url> <type> [flags])",
	Short:                 "Create a provider",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(3),
	RunE:                  providerCreate,
	SilenceUsage:          true,
}

func providerCreate(command *cobra.Command, args []string) error {
	client := getProviderClient()
	if command.Flags().Changed("file") {
		data, err := os.ReadFile(providerCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &providerCreateParams.ProviderModel); err != nil {
			return err
		}

	} else {
		providerCreateParams.Name = args[0]
		providerCreateParams.URL = args[1]
		providerCreateParams.Type = service.ProviderType(args[2])

	}

	if command.Flags().Changed("metadata") {
		var metadata map[string]interface{}
		if err := json.Unmarshal([]byte(providerCreateParams.MetadataStr), &metadata); err != nil {
			return err
		}
		providerCreateParams.Metadata = metadata

	} else if command.Flags().Changed("metadata-file") {
		metadata, err := os.ReadFile(providerCreateParams.MetadataFilename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(metadata, &providerCreateParams.Metadata); err != nil {
			return err
		}

		client.PrintDebug("read metadata from file: %s and parsed it to: %v", providerCreateParams.MetadataFilename, providerCreateParams.Metadata)
	}

	client.PrintDebug("creating provider with the following parameters: %v", providerCreateParams.ProviderModel)

	// Let's marshal the provider model to json so we can print it

	data, err := json.Marshal(providerCreateParams.ProviderModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("POST", "/providers", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&providerCreateParams.Filename, "file", "f", "", "JSON file containing provider information")
	createCmd.Flags().StringVarP(&providerCreateParams.MetadataFilename, "metadata-file", "M", "", "The file to read the provider metadata from. If provider is created from a file, this will override the metadata in the file")
	createCmd.Flags().StringVarP(&providerCreateParams.MetadataStr, "metadata", "m", "", `
The provider metadata as a JSON string. For example:
'{"key1": "value1", "key2": "value2"}'. If provider is created from a file, this will override the metadata in the file`)
	createCmd.MarkFlagsMutuallyExclusive("metadata-file", "metadata")
	createCmd.Flags().MarkHidden("file")
}
