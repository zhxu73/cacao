package provider

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get [provider-id] [flags]",
	Short:        "Get provider(s)",
	Long:         `Gets all providers unless specific ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         providerGet,
	SilenceUsage: true,
}

func providerGet(command *cobra.Command, args []string) error {
	client := getProviderClient()
	if len(args) >= 1 {
		return getOneProvider(client, args[0])
	}

	req := client.NewRequest("GET", "/providers", "")
	return client.DoRequest(req)
}

func getOneProvider(client utils.Client, providerID string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", providerID)
	req := client.NewRequest("GET", "/providers/"+neturl.PathEscape(providerID), "")
	return client.DoRequest(req)
}
