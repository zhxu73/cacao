package provider

import (
	"fmt"
	"net/http"
	neturl "net/url"

	"github.com/spf13/cobra"
)

var providerOpParams = struct {
	ResourceID   string
	credentialID string
	region       string
}{}

// opCmd represents the get command
var opCmd = &cobra.Command{
	Use:   "op <provider-id> <resource> <operation> (--credential <credential-id> --region <region>)",
	Short: "provider operations",
	Long:  `provider operations`,
	Example: `	cacao provider op provider-openstack-aaaaaaaaaaaaaaaaaaaa flavor list --credential cred-aaaaaaaaaaaaaaaaaaaa --region 'region-name'
	cacao provider op provider-aws-aaaaaaaaaaaaaaaaaaaa region list  --credential cred-aaaaaaaaaaaaaaaaaaaa,
	cacao provider op provider-aws-aaaaaaaaaaaaaaaaaaaa image get  --id ami-aaaaaaaaaaaaaaaaa  --credential cred-aaaaaaaaaaaaaaaaaaaa --region 'us-east-1'`,
	SilenceUsage: true,
	Args:         cobra.ExactArgs(3),
	RunE:         providerOp,
}

func providerOp(command *cobra.Command, args []string) error {

	// handle errors first
	providerID := args[0]
	resource := args[1]
	operation := args[2]
	var needResourceID bool
	if operation == "get" || operation == "update" {
		needResourceID = true
		if !command.Flags().Changed("id") {
			return fmt.Errorf("the --id flag is required for get and update operations")
		}
	}
	if resource[len(resource)-1] != 's' {
		// append 's' at the end of resource if not present
		resource += "s"
	}

	client := getProviderClient()

	var httpMethod string
	switch operation {
	case "delete":
		httpMethod = http.MethodDelete
	case "create":
		httpMethod = http.MethodPost
	default:
		httpMethod = http.MethodGet
	}

	var url *neturl.URL
	var err error
	if needResourceID {
		url, err = neturl.Parse(fmt.Sprintf("/providers/%s/%s/%s", neturl.PathEscape(providerID), neturl.PathEscape(resource), neturl.PathEscape(providerOpParams.ResourceID)))
		if err != nil {
			return err
		}
	} else {
		url, err = neturl.Parse(fmt.Sprintf("/providers/%s/%s", neturl.PathEscape(providerID), neturl.PathEscape(resource)))
		if err != nil {
			return err
		}
	}

	var queryParams = neturl.Values{}
	queryParams.Add("credential", providerOpParams.credentialID)
	queryParams.Add("region", providerOpParams.region)
	url.RawQuery = queryParams.Encode()

	req := client.NewRequest(httpMethod, url.String(), "")
	return client.DoRequest(req)
}
func init() {
	opCmd.Flags().StringVar(&providerOpParams.ResourceID, "id", "", "id of the resource to retrieve (retrieve a single resource vs a list of resources)")
	opCmd.Flags().StringVar(&providerOpParams.credentialID, "credential", "", "credential id to use")
	opCmd.Flags().StringVar(&providerOpParams.region, "region", "", "region to use")
	// credential is required for all provider specific operations (at least for now).
	_ = opCmd.MarkFlagRequired("credential")
}
